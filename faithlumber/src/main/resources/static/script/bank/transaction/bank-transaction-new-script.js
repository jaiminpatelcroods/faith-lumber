/**
 * This File is For Bank Transaction New
 * Bank Transaction Validation
 */

$(document).ready(function(){
	
	//	withdraw form validation	
	$("#withdraw_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savewithdraw",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			transactionDate:{
				validators: {
					notEmpty: {
						message: 'The date is required. '
					}
				}
			},
			"fromBankVo.bankId":{
				validators: {
					notEmpty: {
						message: 'From Bank is required. '
					}
				}
			},
			debitAmount:{
				validators: {
					notEmpty: {
						message: 'Amount is required. '
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid. '
					}
				}
			},
			description:{
				validators: {
					
				}
			}
		}
	});
	
	$('#withdrawTransactionDate').datepicker({
		format: 'dd/mm/yyyy',autoclose: true
	}).on('changeDate', function(e) {
		$('#withdraw_form').formValidation('revalidateField', 'transactionDate'); 
	});
	
	//	deposit form validation	
	$("#deposit_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savedeposit",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			transactionDate:{
				validators: {
					notEmpty: {
						message: 'The date is required. '
					}
				}
			},
			"toBankVo.bankId":{
				validators: {
					notEmpty: {
						message: 'To Bank is required. '
					}
				}
			},
			creditAmount:{
				validators: {
					notEmpty: {
						message: 'Amount is required. '
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid. '
					}
				}
			},
			description:{
				validators: {
					
				}
			}
		}
	});
	
	$('#depositTransactionDate').datepicker({
		format: 'dd/mm/yyyy',autoclose: true
	}).on('changeDate', function(e) {
		$('#deposit_form').formValidation('revalidateField', 'transactionDate'); 
	});
	
	//	transfer form validation	
	$("#transfer_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savetransfer",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			transactionDate:{
				validators: {
					notEmpty: {
						message: 'The date is required. '
					}
				}
			},
			"fromBankVo.bankId":{
				validators: {
					notEmpty: {
						message: 'From Bank is required. '
					}
				}
			},
			"toBankVo.bankId":{
				validators: {
					notEmpty: {
						message: 'To Bank is required. '
					}
				}
			},
			creditAmount:{
				validators: {
					notEmpty: {
						message: 'Amount is required. '
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid. '
					}
				}
			},
			description:{
				validators: {
					
				}
			}
		}
	});
	
	$('#transferTransactionDate').datepicker({
		format: 'dd/mm/yyyy',autoclose: true
	}).on('changeDate', function(e) {
		$('#transfer_form').formValidation('revalidateField', 'transactionDate'); 
	});
});