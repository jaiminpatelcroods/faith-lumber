/**
 * This File is For lead source New Modal
 * lead source Ajax Insert
 * lead source Validation
 */
$(document).ready(function(){
	
	//----------lead source------------

	$("#gate_new_form").formValidation({
		framework : 'bootstrap',
	/*	live:'disabled',*/
		excluded : ":disabled",
		button:{
			selector : "#savegate",
			disabled : "disabled",
		},
		icon : null,
		fields : {
		vehicleno:{
			validators: {
				notEmpty: {
					message: 'The Vehicle No. is required. '
				}
			}
		},
		gateDate:{
			validators: {
				notEmpty: {
					message: 'The Date is reqired. '
				},
				
			}
		},
		"salesVo.salesId":{
			validators: {
				notEmpty: {
					message: 'The Sales Order is reqired. '
				}
			}
		},
		 "contactTransportVo.contactId":{
			 validators: {
					notEmpty: {
						message: 'The Transport is reqired. '
					}
				},
				
			}
			
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#savegate").attr("disabled", true);
		 
		 $.post("/gate/save", {
			 vehicleno: $('#vehicleno').val(),
			 gateDate:$('#gateDate').val(),
			 remark:$('#remark').val(),
			 "salesVo.salesId":$('#salesVo').val(),
			 "contactTransportVo.contactId":$('#contactVo').val(),
			 
		 }, function( data,status ) {
			toastr["success"]("Record Inserted....");
			$('#gatein_new_modal').modal('toggle');
			 location.reload(true);
			
		 });
		 
	});
	
	$('#gatein_new_modal').on('show.bs.modal', function() {
		$("#savegate").attr("disabled", false);
		$('#gate_new_form').formValidation('resetForm', true);
		
	});
	
	$("#savegate").click(function() {
		
		$('#gate_new_form').data('formValidation').validate();
	});

});