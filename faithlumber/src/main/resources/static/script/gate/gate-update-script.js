/**
 * This File is For Gate New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#gate_update_form").formValidation({
		
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updategate",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			vehicleno:{
				validators: {
					notEmpty: {
						message: 'The Vehicle No. is required. '
					}
				}
			},
			gateDate:{
				validators: {
					notEmpty: {
						message: 'The Date is reqired. '
					},
					
				}
			},
			"salesVo.salesId":{
				validators: {
					notEmpty: {
						message: 'The Sales Order is reqired. '
					}
				}
			},
			 "contactTransportVo.contactId":{
				 validators: {
						notEmpty: {
							message: 'The Transport Name is reqired. '
						}
					},
					
				}
			},
		
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#updategate").attr("disabled", false);
		 $.post("/gate/update", {
			 gateId : $('#gateId').val(),
			 vehicleno: $('#updatevehicleno').val(),
			 gateDate:$('#updategateDate').val(),
			 remark:$('#updateremark').val(),
			"salesId":$('#updatesalesVo').val(),
			 "contactId":$('#updatecontactVo').val()
		 }, function( data,status ) {
			 toastr["success"]("Record Updated....");
			 $('#gate_update_modal').modal('toggle');
			 location.reload(true);
		 });
	});
	
	$('#gate_update_modal').on('shown.bs.modal', function() {
	
		$("#contactVo").select2({dropdownParent: $("#updatecontactVo").parent()});	
		$("#salesVo").select2({dropdownParent: $("#updatesalesVo").parent()});	
		
	});
	
	$("#updategate").click(function() {
		$('#gate_update_form').data('formValidation').validate();
	});
	
});

function updategate(row,id)
{  		//console.log(row);
	
	$("#updategate").attr("disabled", false);
	
	$('#gate_update_form').formValidation('resetForm', true);
	
	var crow = $(row).closest('tr');		
	
	var gateDate = $(crow).find('td:eq(1)').text();
	var vehicleno= $(crow).find('td:eq(3)').text();
	var remark = $(crow).find("input[id='remark"+id+"']").val();
	var salesVo = $(crow).find("input[id='salesId"+id+"']").val();
	var contactVo = $(crow).find("input[id='contactId"+id+"']").val();
	var rowindex = $(crow).find('td:eq(0)').text();
	
	$('#gateId').val(id);
	$('#updatevehicleno').val(vehicleno);
	$('#updateremark').val(remark);
	$('#updatecontactVo').val(contactVo).trigger('change');
	$('#updatesalesVo').val(salesVo).trigger('change');
	$('#updategateDate').val(gateDate);
	$("#dataindex").val(rowindex);
	
}
