/**
 * This File is For Department New Modal
 * Department Ajax Insert
 * Department Validation
 */

$(document).ready(function(){
	
	//----------Department------------
	
	$(".department-select2").each(function() {
    	
    	placeholder="Select...";
    	if($(this).attr("placeholder")) {
    		placeholder=$(this).attr("placeholder");
    	}
    	
    	allowClear=0;
    	if($(this).data('allow-clear')) {
    		allowClear=	!0;
    	}
    	$(this).select2({
    		placeholder:placeholder,
    		allowClear:allowClear,
    		escapeMarkup: function (markup) { return markup; },
			 language: {
	            noResults: function () {
	            	return '<div class="m-demo-icon"><div class="m-demo-icon__preview"><span class=""><i class="flaticon-plus m--font-primary"></i></span></div><div class="m-demo-icon__class"><a href="#" data-toggle="modal" class="m-link m--font-boldest add-department-btn">Add Department</a></div></div>';
	            }
	        }
    	});
    });
	
	$(document).on("click",".add-department-btn",function() {
		$('#department_new_form').formValidation('resetForm', true);
		$("#department_new_modal").modal("show")
		$("#departmentNameModal").val($(".select2-search__field").val());
		
		$(".department-select2").select2("close");
	});
	
	$("#department_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savedepartment",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			departmentName:{
				verbose : false,
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/department/department/verify",
						type : 'POST',
						async: true
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		
		e.preventDefault();//stop the from action methods
		$("#savedepartment").attr("disabled",true);
		 $.post("/department/save", {
			 name: $('#departmentNameModal').val(),
		 }, function( data,status ) {
			 toastr["success"]("Category Department....");
			 $('#department_new_modal').modal('toggle');
			 $(".department-select2").prepend($('<option>',{value :data.departmentId, text :data.departmentName}));
			 $(".department-select2").val(data.departmentId).trigger("change");
			
		 });
		 
	});
	
	$('#department_new_modal').on('shown.bs.modal', function() {
		$("#savedepartment").attr("disabled",false);
		$("#departmentNameModal").focus();
	});
	
	$("#savedepartment").click(function() {
		$('#department_new_form').data('formValidation').validate();
	});
	
	//----------End Department------------
	
});