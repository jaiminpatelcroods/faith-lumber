/**
 * This File is For Department New Modal
 * Department Ajax Insert
 * Department Validation
 */

$(document).ready(function(){
	
	//----------Department------------
	$("#department_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savedepartment",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			departmentName:{
				verbose : false,
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/department/department/verify",
						type : 'POST',
						async: true
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		
		e.preventDefault();//stop the from action methods
		$("#savedepartment").attr("disabled",true);
		 $.post("/department/save", {
			 name: $('#departmentNameModal').val(),
		 }, function( data,status ) {
			toastr["success"]("Record Inserted....");
			$('#department_new_modal').modal('toggle');
			 
			location.reload(true);
			
		 });
		 
	});
	
	$('#department_new_modal').on('show.bs.modal', function() {
		$("#savedepartment").attr("disabled",false);
		$('#department_new_form').formValidation('resetForm', true);
	});
	
	$("#savedepartment").click(function() {
		$('#department_new_form').data('formValidation').validate();
	});
	
	//----------End Department------------
	
});