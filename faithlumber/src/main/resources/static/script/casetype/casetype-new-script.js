/**
 * This File is For Case Type New Modal
 * Case Type Ajax Insert
 * Case Type Validation
 */

$(document).ready(function(){
	
	//----------Category------------
	$("#casetype_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savecasetype",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			caseName:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					}
				}
			},
			caseDescription:{
				validators: {
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#savecasetype").attr("disabled", true);
		 
		 $.post("/casetype/save", {
			 caseName: $('#caseNameModal').val(),
			 caseDescription:$('#caseDescriptionModal').val()
		 }, function( data,status ) {
			
			toastr["success"]("Record Inserted....");
			
			$('#casetype_new_modal').modal('toggle');
			
			location.reload(true);
		 });
		 
	});
	
	$('#casetype_new_modal').on('show.bs.modal', function() {
		$("#savecasetype").attr("disabled", false);
		$('#casetype_new_form').formValidation('resetForm', true);
	});
	
	$("#savecasetype").click(function() {
		$('#casetype_new_form').data('formValidation').validate();
	});
	
	//----------End Category------------
});