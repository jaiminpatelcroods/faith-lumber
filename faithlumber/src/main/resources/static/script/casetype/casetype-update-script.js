/**
 * This File is For Case Type Update Modal
 * Case Type Ajax Insert
 * Case Type Validation
 */

$(document).ready(function(){
	
	//----------Case Type------------
	$("#casetype_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updatecasetype",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			catupname:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					}
				}
			},
			catupdesc:{
				validators: {
					
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#updatecasetype").attr("disabled", true);
		 
		 $.post("/casetype/update", {
			 caseName: $('#updateCaseName').val(),
			 caseDescription:$('#updateCaseDescription').val(),
			 caseTypeId:$('#caseTypeId').val()
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			 
			 $('#casetype_update_modal').modal('toggle');
			 
			 location.reload(true);
			
		 });
	});
	
	$("#updatecasetype").click(function() {
		$('#casetype_update_form').data('formValidation').validate();
	});
	//----------End Case Type------------
	
});

//----------Case Type Function------------
function updateCaseType(row,id)
{  		
	$("#updatecasetype").attr("disabled", false);
	
	$('#casetype_update_form').formValidation('resetForm', true);
	
	var crow=$(row).closest('tr');		
	var name=$(crow).find('td:eq(1)').text();
	var description = $(crow).find('td:eq(2)').text();

	$('#updateCaseName').val(name);
	$('#updateCaseDescription').val(description);
	$('#caseTypeId').val(id);

}
//----------End Case Type Function------------