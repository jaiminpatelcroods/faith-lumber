/**
 * This File is For Case Type New Modal
 * Case Type Ajax Insert
 * Case Type Validation
 */

$(document).ready(function(){
	
	//----------Case Type------------
	
	$(".casetype-select2").each(function() {
    	
    	placeholder="Select...";
    	if($(this).attr("placeholder")) {
    		placeholder=$(this).attr("placeholder");
    	}
    	
    	allowClear=0;
    	if($(this).data('allow-clear')) {
    		allowClear=	!0;
    	}
    	$(this).select2({
    		placeholder:placeholder,
    		allowClear:allowClear,
    		escapeMarkup: function (markup) { return markup; },
			 language: {
	            noResults: function () {
	            	return '<div class="m-demo-icon"><div class="m-demo-icon__preview"><span class=""><i class="flaticon-plus m--font-primary"></i></span></div><div class="m-demo-icon__class"><a href="#" data-toggle="modal" class="m-link m--font-boldest add-casetype-btn">Add Case Type</a></div></div>';
	            }
	        }
    	});
    });
	
	$(document).on("click",".add-casetype-btn",function() {
		$('#casetype_new_form').formValidation('resetForm', true);
		$("#casetype_new_modal").modal("show")
		$("#caseNameModal").val($(".select2-search__field").val());
		
		$(".casetype-select2").select2("close");
	});
	
	$("#casetype_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savecasetype",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			caseName:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					}
				}
			},
			caseDescription:{
				validators: {
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#savecasetype").attr("disabled", true);
		 
		 $.post("/casetype/save", {
			 caseName: $('#caseNameModal').val(),
			 caseDescription:$('#caseDescriptionModal').val()
		 }, function( data,status ) {
			
			 toastr["success"]("Case Type Inserted....");
			 $('#casetype_new_modal').modal('toggle');
			 $(".casetype-select2").prepend($('<option>',{value :data.caseTypeId, text :data.caseName}));
			 $(".casetype-select2").val(data.caseTypeId).trigger("change");
		 });
		 
	});
	
	$('#casetype_new_modal').on('shown.bs.modal', function() {
		$("#savecasetype").attr("disabled", false);
		$('#caseNameModal').focus();
	});
	
	$("#savecasetype").click(function() {
		$('#casetype_new_form').data('formValidation').validate();
	});
	
	//----------End Category------------
});