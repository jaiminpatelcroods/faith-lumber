/**
 * This File is For Delivery - Faith Lumber
 * Material Request Validation
 */


	var tableIndex = 0;
	
	var qtyValidator = {
			validators : {
				notEmpty : {
					message : 'The Qty is required. '
				},
				stringLength : {
					max : 20,
					message : 'The Qty must be less than 20 characters long. '
				},
				regexp : {
					regexp:/^((\d+)((\.\d{0,4})?))$/,
					message : 'The Qty is invalid'
				}
			}
		},
		productValidator = {
			validators : {
				notEmpty : {
					message : 'The product is required. '
				}
	          }
	     };
	
	$(document).ready(function(){
		
  		//-------------------- Material Request Validation -------------------------------
		$('#delivery_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			/*live:'disabled', */
			button: {
				selector: "#savedelivery",
				disabled: "disabled"
			},
			icon : null,
			fields : {
				/*"requestedBy.employeeId" : {
					validators : {
						notEmpty : {
							message : 'please select Requested By. '
						}
					}
				},*/
				deliveryDate : {
					validators : {
						notEmpty : {
							message : 'The Request Date is required. '
						}
					}
				},
				
			}
		});
		//-------------------- End Material Request Validation ---------------------------
		
		$("#deliveryDate").change(function() {
			$('#delivery_form').formValidation('revalidateField', 'deliveryDate');
		});
		
		$("#product_table").on("click",'button[data-item-remove]',function(e) {
			
			e.preventDefault();
			var i = $(this).closest("[data-table-item]").attr("data-table-item");
			
			if($("#deliveryItemId"+i).val() != undefined) {
				$("#deleteDeliveryIds").val($("#deleteDeliveryIds").val()+$("#deliveryItemId"+i).val()+ ",");
			}
			
			$('#delivery_form').formValidation('removeField',"deliveryItemVos["+i+"].productVariantVo.productVariantId");
			$('#delivery_form').formValidation('removeField',"deliveryItemVos["+i+"].qty");
			
			$(this).closest("[data-table-item]").remove();
			
			setTableSrNo();
		});
		
		$("#savedelivery").click(function (){
			
			if ($('#delivery_form').data('formValidation').isValid() == null) {
				$('#delivery_form').data('formValidation').validate();
			}
			
			if($("#product_table").find("[data-table-item]").not(".m--hide").length == 0) {
				toastr.options = {
				  "closeButton": true,
				  "debug": false,
				  "newestOnTop": false,
				  "progressBar": false,
				  "positionClass": "toast-top-center",
				  "preventDuplicates": true,
				  "onclick": null,
				  "showDuration": "300",
				  "hideDuration": "1000",
				  "timeOut": "5000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "fadeIn",
				  "hideMethod": "fadeOut"
				};
				toastr.error("","add minimum one product");
				
				return false;
			}
			
			if($('#delivery_form').data('formValidation').isValid() == true) {
				
				if($("#jobworkVo").val() == '') {
					$("#jobworkVo").remove();
				}
				$("#product_table").find("[data-table-item='template']").remove();
				
				checkAllTotal();
			}
			
		});
		
	});
	
	function addTableItem() {
		
		var	$tableItemTemplate = $("#product_table").find("[data-table-item='template']").clone();
		
		$tableItemTemplate.removeClass("m--hide").attr("data-table-item",tableIndex);
		
		$tableItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
			n=$(this).attr("id");
			n ? $(this).attr("id",n.replace(/{index}/g,tableIndex)) : "";
			n=$(this).attr("name");
			n ? $(this).attr("name",n.replace(/{index}/g,tableIndex)) : "";
			
			$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,tableIndex)) : "";
		});
		
		$("#product_table").find("[data-table-list]").append($tableItemTemplate);
		
		$("#productVariantId"+tableIndex).addClass("m-select2");
		$("#productVariantId"+tableIndex).addClass("readonly");
		$("#productVariantId"+tableIndex).select2({placeholder:"Select Product",allowClear:0});
		
		$('#delivery_form').formValidation('addField',"deliveryItemVos["+tableIndex+"].productVariantVo.productVariantId");
		$('#delivery_form').formValidation('addField',"deliveryItemVos["+tableIndex+"].qty",qtyValidator);
		
		/*For Remote Multiple Select2*/
		setProductRemote(tableIndex);
		
		tableIndex++;
		
		setTableSrNo();
	}
	
	function setProductRemote(i) {
		
		$("#productVariantId"+i).select2({
	        placeholder: "Search Product",
	        allowClear: 0,
	        ajax: {
	            url: "/product/variant/select/json",
	            dataType: "json",
	            type: "GET",
	            delay: 250,
	            data: function(e) {
	                return {
	                    q: e.term,
	                    page: e.page
	                }
	            },
	            processResults: function(e, t) {
					return t.page = t.page || 1, {
	                    results: e.items,
	                    pagination: {
	                        more: 30 * t.page < e.total_count
	                    }
	                }
	            },
	            cache: !0
	        },
	        escapeMarkup: function(e) {
	            return e
	        },
	        minimumInputLength: 1,
	        templateResult: function(e) {
	            if (e.loading) return e.text;
	            return e.text 
	        },
	        templateSelection: function(e) {
	            return e.full_name || e.text
	        }
	    });
	}
	function setTableSrNo() {
		
		var $tableItemTemplate=$("#product_table").find("[data-table-item]").not(".m--hide");
		var i = 0;
		$tableItemTemplate.each(function (){
			$(this).find("[data-item-index]").html(++i);
		});
	}
	
	function getProductVariantInfo(id) {
		
		if($("#productVariantId"+id).val() != "") {
			$.post("/product/variant/"+$("#productVariantId"+id).val()+"/json", {
				
			}, function (data, status) {
				if(status == 'success') {
					
					$tableItem=$("#product_table").find('[data-table-item="'+id+'"]');
					
					$("#qty"+id).val("0");
					$tableItem.find("[data-item-uom]").html(data.productVo.unitOfMeasurementVo.measurementCode);
				}
			});
		}
	}
	
	function setTableIndex(id){
		
		tableIndex=id;
		setTableSrNo();
	}
	
	function setDeliveryQty(id) {
		
		var soQty=0.0;
		var soTotalQty = 0.0;
		var deliveredQty = 0.0;
  		var deliveredTotalQty = 0.0;
  		var qty=0;
		var totalQty=0;
  		var remainsQty = 0.0;
  		var remainsTotalQty = 0.0;
  		var oldDeliveredQty = 0.0;
		
  		if($("#soQty"+id).val()=="") {
  			soQty=parseFloat("0.0");
		} else {
			soQty = parseFloat($("#soQty"+id).val());
		}
  		
  		if($("#deliveredQty"+id).val()=="") {
  			deliveredQty=parseFloat("0.0");
		} else {
			deliveredQty = parseFloat($("#deliveredQty"+id).val());
		}
  		
		if($("#qty"+id).val()=="") {
			qty=parseFloat("0.0");
		} else {
			qty = parseFloat($("#qty"+id).val());
		}
		
		if($("#remainsQty"+id).val()=="") {
			remainsQty = parseFloat("0.0");
		} else {
			remainsQty = parseFloat($("#remainsQty"+id).val());
		}
		
		/*if($("#oldProductionQty"+id).val()=="") {
			oldProductionQty = parseFloat("0.0");
		} else {
			oldProductionQty = parseFloat($("#oldProductionQty"+id).val());
		}*/
		
		
		
		if(deliveredQty==0)
			remainsQty = (soQty - qty + oldDeliveredQty);
		else
			remainsQty = ((soQty - deliveredQty)- qty + oldDeliveredQty);
		
		
		
		$tableItemTemplate = $("#product_table").find("[data-table-item='"+id+"']");
		$tableItemTemplate.find("[data-item-remains]").html(remainsQty.toFixed(2));
		
		$("#remainsQty"+id).val(remainsQty.toFixed(2));
		
		//setAllTotal();
	}
	
	function checkAllTotal() {
		
		var totalRemainsQtyCheck = 0.0;
		var $tableItemTemplate=$("#product_table").find("[data-table-item]").not(".m--hide");
		$tableItemTemplate.each(function (){
			totalRemainsQtyCheck = totalRemainsQtyCheck + parseFloat($(this).find("[data-item-remains]").val());
		});
		
		if(totalRemainsQtyCheck==0){
			$("#updateStatus").val("Completed");
		}
		
	}
		