/**
 * This File is For Delivery Challan View Page
 * Delivery Challan Validation
 */

	var rackTableIndex = 0;
	
	var qtyValidator = {
			validators : {
				notEmpty : {
					message : 'The Qty is required. '
				},
				stringLength : {
					max : 20,
					message : 'The Qty must be less than 20 characters long. '
				},
				regexp : {
					regexp:/^((\d+)((\.\d{0,4})?))$/,
					message : 'The Qty is invalid. '
				}
			}
		},
		productValidator = {
			validators : {
				notEmpty : {
					message : 'The Product is required. '
				}
			}
	     },
	     rackValidator = {
			validators : {
				notEmpty : {
					message : 'The Rack is required. '
				}
			}
		};
	
	$(document).ready(function() {
		
		$("#qc_btn").click(function (){
			$("#product_table").find(".qc-td").removeClass("m--hide");
			$("#rack_div").removeClass("m--hide");
			$("#qc_footer").removeClass("m--hide");
			$("#qc_btn").addClass("m--hide");
		});
		$("#qc_cancel").click(function (){
			$("#product_table").find(".qc-td").addClass("m--hide");
			$("#rack_div").addClass("m--hide");
			$("#qc_footer").addClass("m--hide");
			$("#qc_btn").removeClass("m--hide");
		});
		
		//-------------------- Delivery Challan Validation -------------------------------
		$('#deliverychallan_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			/*live:'disabled', */
			button: {
				selector: "#savedeliverychallan",
				disabled: "disabled"
			},
			icon : null,
			fields : {
				"qcBy.employeeId" : {
					validators : {
						notEmpty : {
							message : 'please select Quality Check By'
						}
					}
				},
			}
		});
		//-------------------- End Delivery Challan Validation ---------------------------
		
		var $productItem=$("#product_table").find("[data-table-item]").not(".m--hide");
		$productItem.each(function (){
			
			$('#deliverychallan_form').formValidation('addField',"deliveryChallanItemVos["+$productItem.attr("data-table-item")+"].qcQty", qtyValidator);
			setDifference($productItem.attr("data-table-item"));
		});
		
	});
	
	function setDifference(id) {
		var $productItem=$("#product_table").find("[data-table-item='"+id+"']"), differenceQty = 0;
		
		differenceQty = $productItem.find("[data-item-pack]").text()*1 - $("#qcQty"+id).val()*1;
		
		$productItem.find("[data-item-difference]").text(differenceQty);
		
		if(differenceQty == 0) {
			$productItem.find("[data-item-difference]").removeClass("m--font-danger");
			$productItem.find("[data-item-difference]").addClass("m--font-success");
		} else {
			$productItem.find("[data-item-difference]").removeClass("m--font-success");
			$productItem.find("[data-item-difference]").addClass("m--font-danger");
		}
		
	}
	
	function addRackTableItem() {
		
		var	$tableItemTemplate = $("#rack_table").find("[data-table-item='template']").clone();
		
		$tableItemTemplate.removeClass("m--hide").attr("data-table-item",rackTableIndex);
		
		$tableItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
			n=$(this).attr("id");
			n ? $(this).attr("id",n.replace(/{index}/g,rackTableIndex)) : "";
			n=$(this).attr("name");
			n ? $(this).attr("name",n.replace(/{index}/g,rackTableIndex)) : "";
			
			$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,rackTableIndex)) : "";
		});
		
		$("#rack_table").find("[data-table-list]").append($tableItemTemplate);
		
		$("#productVariantId"+rackTableIndex).addClass("m-select2");
		$("#productVariantId"+rackTableIndex).select2({
			placeholder:"Select Product",
			allowClear:0,
			escapeMarkup: function(e) {return e},
			templateResult: function(e) {
	            if (e.loading) return e.text;
	            return e.text;
	        },
		});
		
		$("#rackId"+rackTableIndex).addClass("m-select2");
		$("#rackId"+rackTableIndex).select2({placeholder:"Select Rack",allowClear:0});
		
		$('#deliverychallan_form').formValidation('addField',"deliveryChallanRackVos["+rackTableIndex+"].productVariantVo.productVariantId", productValidator);
		$('#deliverychallan_form').formValidation('addField',"deliveryChallanRackVos["+rackTableIndex+"].rackVo.rackId",rackValidator);
		$('#deliverychallan_form').formValidation('addField',"deliveryChallanRackVos["+rackTableIndex+"].rackQty",qtyValidator);
		
		rackTableIndex++;
		
		setRackTableSrNo();
	}
	
	function setRackTableSrNo() {
		var $tableItemTemplate=$("#rack_table").find("[data-table-item]").not(".m--hide");
		var i = 0;
		$tableItemTemplate.each(function (){
			$(this).find("[data-item-index]").html(++i);
		});
	}

		