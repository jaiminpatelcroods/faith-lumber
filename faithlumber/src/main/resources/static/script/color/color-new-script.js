/**
 * This File is For Color New Modal
 * Color Ajax Insert
 * Color Validation
 */

$(document).ready(function(){
	$("#color_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savecolor",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			colorName:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					}
				}
			},
			colorCode:{
				validators: {
					notEmpty: {
						message: 'The Code is required. '
					},
					regexp : {
						regexp : /^[a-zA-Z0-9]+$/,
						message : 'The Code can only consist of alphabetical, numberical value. '
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $("#savecolor").attr("disabled", true);
		 $.post("/color/save", {
			 colorName: $('#colorName').val(),
			 colorCode:$('#colorCode').val()
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#color_new_modal').modal('toggle');
			
			location.reload(true);
		 });
		 
	});
	
	$('#color_new_modal').on('show.bs.modal', function() {
		$("#savecolor").attr("disabled", false);
		$('#color_new_form').formValidation('resetForm', true);
	});
	
	$("#savecolor").click(function() {
		$('#color_new_form').data('formValidation').validate();
	});
	
});