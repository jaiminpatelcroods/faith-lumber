/**
 * This File is For Color New Modal
 * Color Ajax Insert
 * Color Validation
 */

$(document).ready(function(){
	$("#color_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updatecolor",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			colorName:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					}
				}
			},
			colorCode:{
				validators: {
					notEmpty: {
						message: 'The Code is required. '
					},
					regexp : {
						regexp : /^[a-zA-Z0-9]+$/,
						message : 'The Code can only consist of alphabetical, numberical value. '
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $("#updatecolor").attr("disabled", true);
		 $.post("/color/update", {
			 colorName: $('#updateColorName').val(),
			 colorCode:$('#updateColorCode').val(),
			 colorId:$('#colorId').val()
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			
			 $('#color_update_modal').modal('toggle');
			
			location.reload(true);
			
		 });
	});
	
	$('#color_update_modal').on('hide.bs.modal', function() {
		$('#color_update_form').formValidation('resetForm', true);
	});
	
	$("#updatecolor").click(function() {
		$('#color_update_form').data('formValidation').validate();
	});
	
});

function updateColor(row,id)
{  		//console.log(row);
	$("#updatecolor").attr("disabled", false);
	$('#color_update_form').formValidation('resetForm', true);
	var crow=$(row).closest('tr');		
	var name=$(crow).find('td:eq(1)').text();
	var code = $(crow).find('td:eq(2)').text();
	var rowindex = $(crow).find('td:eq(0)').text();

	$('#updateColorName').val(name);
	$('#updateColorCode').val(code);
	$('#colorId').val(id);
	
}