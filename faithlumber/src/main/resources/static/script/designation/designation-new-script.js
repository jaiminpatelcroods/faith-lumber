/**
 * This File is For Designation New Modal
 * Designation Ajax Insert
 * Designation Validation
 */

$(document).ready(function(){
	
	//----------Designation------------
	$("#designation_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savedesignation",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			designationName:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/designation/designation/verify",
						type : 'POST'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $("#savedesignation").attr("disabled",true);
		 
		 $.post("/designation/save", {
			 name: $('#designationNameModal').val(),
		 }, function( data,status ) {
			toastr["success"]("Record Inserted....");
			$('#designation_new_modal').modal('toggle');
			 
			location.reload(true);
			
		 });
		 
	});
	
	$('#designation_new_modal').on('show.bs.modal', function() {
		$("#savedesignation").attr("disabled",false);
		$('#designation_new_form').formValidation('resetForm', true);
	});
	
	$("#savedesignation").click(function() {
		$('#designation_new_form').data('formValidation').validate();
	});
	
	//----------End Designation------------
	
});