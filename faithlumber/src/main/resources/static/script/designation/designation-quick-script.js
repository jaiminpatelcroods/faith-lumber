/**
 * This File is For Designation New Modal
 * Designation Ajax Insert
 * Designation Validation
 */

$(document).ready(function(){
	
	//----------Designation------------
	
	$(".designation-select2").each(function() {
    	
    	placeholder="Select...";
    	if($(this).attr("placeholder")) {
    		placeholder=$(this).attr("placeholder");
    	}
    	
    	allowClear=0;
    	if($(this).data('allow-clear')) {
    		allowClear=	!0;
    	}
    	$(this).select2({
    		placeholder:placeholder,
    		allowClear:allowClear,
    		escapeMarkup: function (markup) { return markup; },
			 language: {
	            noResults: function () {
	            	return '<div class="m-demo-icon"><div class="m-demo-icon__preview"><span class=""><i class="flaticon-plus m--font-primary"></i></span></div><div class="m-demo-icon__class"><a href="#" data-toggle="modal" class="m-link m--font-boldest add-designation-btn">Add Designation</a></div></div>';
	            }
	        }
    	});
    });
	
	$(document).on("click",".add-designation-btn",function() {
		$('#designation_new_form').formValidation('resetForm', true);
		$("#designation_new_modal").modal("show")
		$("#designationNameModal").val($(".select2-search__field").val());
		
		$(".designation-select2").select2("close");
	});
	
	$("#designation_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savedesignation",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			designationName:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/designation/designation/verify",
						type : 'POST'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $("#savedesignation").attr("disabled",true);
		 
		 $.post("/designation/save", {
			 name: $('#designationNameModal').val(),
		 }, function( data,status ) {
			 toastr["success"]("Designation Inserted....");
			 $('#designation_new_modal').modal('toggle');
			 $(".designation-select2").prepend($('<option>',{value :data.designationId, text :data.designationName}));
			 $(".designation-select2").val(data.designationId).trigger("change");
			
		 });
		 
	});
	
	$('#designation_new_modal').on('shown.bs.modal', function() {
		$("#savedesignation").attr("disabled",false);
		$('#designationNameModal').focus()
	});
	
	$("#savedesignation").click(function() {
		$('#designation_new_form').data('formValidation').validate();
	});
	
	//----------End Designation------------
	
});