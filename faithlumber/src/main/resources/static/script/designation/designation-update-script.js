/**
 * This File is For Designation Update Modal
 * Designation Ajax Insert
 * Designation Validation
 */

$(document).ready(function(){
	
	//----------Designation------------
	$("#designation_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updatedesignation",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			designationName:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/designation/designation/verify",
						type : 'POST',
						data: function(validator, $field, value) {
	                            return {
	                            	designationId : $('#updateDesignationId').val()
	                            };
	                        }
				}
			}
		}
	  } 
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $("#updatedesignation").attr("disabled",true);
		 $.post("/designation/update", {
			 name: $('#updateDesignationName').val(),
			 id:$('#updateDesignationId').val()
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			 
			 $('#designation_update_modal').modal('toggle');
			 
			 location.reload(true);
			
		 });
	});
	
	$("#updatedesignation").click(function() {
		$('#designation_update_form').data('formValidation').validate();
	});
	//----------End Designation------------
	
});

//----------Designation Function------------
function updateDesignation(row,id)
{  		
	$("#updatedesignation").attr("disabled",false);
	
	$('#designation_update_form').formValidation('resetForm', true);
	
	var crow=$(row).closest('tr');		
	var name=$(crow).find('td:eq(1)').text();

	$('#updateDesignationName').val(name);
	$('#updateDesignationId').val(id);

}
//----------End Designation Function------------

