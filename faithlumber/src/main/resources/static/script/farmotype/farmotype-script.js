/**
 * This File is For Purchase
 * Purchase Validation
 */
	var tableIndex = 0;
	var particularValidator = {
			validators : {
				notEmpty : {message : 'The particular is required. '}
			}
		},measurement = {
			validators: {
				notEmpty: {message: 'Please enter Measurement. '},
				regexp : {
					regexp : /^[0-9.]+$/,
					message : 'Measurement is not valid, can contain only digits. '
				}
			}
		};
	$(document).ready(function(){
		
		$("#product_table").on("click",'button[data-item-remove]',function(e) {
			
			var i=$(this).closest("[data-table-item]").attr("data-table-item");
			$("#particular"+i).val(0);
			$("#farmoParticularMeasurement"+i).val("");
			$("#deleteFarmoTypeIds").val($("#deleteFarmoTypeIds").val()+$("#farmoTypeParticularsId"+i).val()+ ",");
			
			$('#farmotype_form').formValidation('removeField',"farmoTypeParticularVos["+i+"].farmoParticularVo.farmoParticularId");
			$('#farmotype_form').formValidation('removeField',"farmoTypeParticularVos["+i+"].farmoParticularMeasurement");
			$(this).closest("[data-table-item]").remove();
			setSrNo();
			return false;
		});
		
		//-------------------- Form Type Validation -------------------------------
		$('#farmotype_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			button: {
				selector: "#saveFarmoType",
				disabled: "disabled"
			},
			icon : null,
			fields : {
				farmoTypeName : {
					validators : {
						notEmpty : {
							message : 'Please enter Name. '
						},stringLength : {
							max : 20,
							message : 'The Name must be less than 20 characters long. '
						},
						regexp : {
							regexp : /^[a-zA-Z0-9/_-\s-., ]+$/,
							message : 'Name is not valid. '
						}
					}
				},frontLength : {
					validators : {
						notEmpty : {
							message : 'Please enter Front Length. '
						},regexp : {
							regexp : /^[0-9.]+$/,
							message : 'Front Length is not valid, can contain only digits. '
						}
					}
				},frontSeat : {
					validators : {
						notEmpty : {
							message : 'Please enter Front Seat. '
						},regexp : {
							regexp : /^[0-9.]+$/,
							message : 'Front Seat is not valid, can contain only digits. '
						}
					}
				},frontThai : {
					validators : {
						notEmpty : {
							message : 'Please enter Front Thai. '
						},regexp : {
							regexp : /^[0-9.]+$/,
							message : 'Front Thai is not valid, can contain only digits. '
						}
					}
				},frontLangot : {
					validators : {
						notEmpty : {
							message : 'Please enter Front Langot. '
						},regexp : {
							regexp : /^[0-9.]+$/,
							message : 'Front Langot is not valid, can contain only digits. '
						}
					}
				},frontKnee : {
					validators : {
						notEmpty : {
							message : 'Please enter Front Knee. '
						},regexp : {
							regexp : /^[0-9.]+$/,
							message : 'Front Knee is not valid, can contain only digits. '
						}
					}
				},frontMoriMunda : {
					validators : {
						notEmpty : {
							message : 'Please enter Front Mori Munda. '
						},regexp : {
							regexp : /^[0-9.]+$/,
							message : 'Front Mori Munda is not valid, can contain only digits. '
						}
					}
				},frontJati : {
					validators : {
						notEmpty : {
							message : 'Please enter Front Jati. '
						},regexp : {
							regexp : /^[0-9.]+$/,
							message : 'Front Jati is not valid, can contain only digits. '
						}
					}
				},backLength : {
					validators : {
						notEmpty : {
							message : 'Please enter Back Length. '
						},regexp : {
							regexp : /^[0-9.]+$/,
							message : 'Back Length is not valid, can contain only digits. '
						}
					}
				},backSeat : {
					validators : {
						notEmpty : {
							message : 'Please enter Back Seat. '
						},regexp : {
							regexp : /^[0-9.]+$/,
							message : 'Back Seat is not valid, can contain only digits. '
						}
					}
				},backThai : {
					validators : {
						notEmpty : {
							message : 'Please enter Back Thai. '
						},regexp : {
							regexp : /^[0-9.]+$/,
							message : 'Back Thai is not valid, can contain only digits. '
						}
					}
				},backLangot : {
					validators : {
						notEmpty : {
							message : 'Please enter Back Langot. '
						},regexp : {
							regexp : /^[0-9.]+$/,
							message : 'Back Langot is not valid, can contain only digits. '
						}
					}
				},backKnee : {
					validators : {
						notEmpty : {
							message : 'Please enter Back Knee. '
						},regexp : {
							regexp : /^[0-9.]+$/,
							message : 'Back Knee is not valid, can contain only digits. '
						}
					}
				},backMoriMunda : {
					validators : {
						notEmpty : {
							message : 'Please enter Back Mori Munda. '
						},regexp : {
							regexp : /^[0-9.]+$/,
							message : 'Back Mori Munda is not valid, can contain only digits. '
						}
					}
				},backJati : {
					validators : {
						notEmpty : {
							message : 'Please enter Back Jati. '
						},regexp : {
							regexp : /^[0-9.]+$/,
							message : 'Back Jati is not valid, can contain only digits. '
						}
					}
				},
			}
		});
		//-------------------- End Purchase Validation ---------------------------
		
		var $purchaseItem=$("#product_table").find("[data-table-item]").not(".m--hide");
		
		$purchaseItem.each(function (){
			var i=$(this).attr("data-table-item");
			$('#farmotype_form').formValidation('addField',"farmoTypeParticularVos["+i+"].farmoParticularVo.farmoParticularId", particularValidator);
			$('#farmotype_form').formValidation('addField',"farmoTypeParticularVos["+i+"].farmoParticularMeasurement", measurement);
		});
		
		$("#saveFarmoType").click(function (){
			if($("#product_table").find("[data-table-item]").not(".m--hide").length == 0) {
				toastr.options = {
				  "closeButton": true,
				  "debug": false,
				  "newestOnTop": false,
				  "progressBar": false,
				  "positionClass": "toast-top-center",
				  "preventDuplicates": true,
				  "onclick": null,
				  "showDuration": "300",
				  "hideDuration": "1000",
				  "timeOut": "5000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "fadeIn",
				  "hideMethod": "fadeOut"
				};
				toastr.error("","Add minimum one particular.");
				
				return false;
			}
			
			
			
			if ($('#farmotype_form').data('formValidation').isValid() == null) {
				$('#farmotype_form').data('formValidation').validate();
			}
			
			if($('#farmotype_form').data('formValidation').isValid() == true) {
				$("#product_table").find("[data-table-item='template']").remove();
			}
			
		});
		
		$("body").on("change", "input[type='text']", function(){
			if($(this).attr('data-decimal')!=undefined && $(this).attr('data-decimal') != '') {			
				var fix=parseFloat($(this).val()).toFixed($(this).attr('data-decimal'));					
				$(this).val(fix);
			}	
		});
	});
	
	
	function addTableItem() {
		
		var	$tableItemTemplate = $("#product_table").find("[data-table-item='template']").clone();
		
		$tableItemTemplate.removeClass("m--hide").attr("data-table-item",tableIndex);
		
		$tableItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
			n=$(this).attr("id");
			n ? $(this).attr("id",n.replace(/{index}/g,tableIndex)) : "";
			n=$(this).attr("name");
			n ? $(this).attr("name",n.replace(/{index}/g,tableIndex)) : "";
			
			$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,tableIndex)) : "";
		});
		
		$("#product_table").find("[data-table-list]").append($tableItemTemplate);
		
		$("#particular"+tableIndex).addClass("m-select2");
		$("#particular"+tableIndex).select2({placeholder:"Select Particular",allowClear:0});
		
		$('#farmotype_form').formValidation('addField',"farmoTypeParticularVos["+tableIndex+"].farmoParticularVo.farmoParticularId", particularValidator);
		$('#farmotype_form').formValidation('addField',"farmoTypeParticularVos["+tableIndex+"].farmoParticularMeasurement", measurement);
		
		tableIndex++;
		
		setdTableSrNo();
	}
	
	function setdTableSrNo() {
		var $tableItemTemplate=$("#product_table").find("[data-table-item]").not(".m--hide");
		var i = 0;
		$tableItemTemplate.each(function (){
			$(this).find("[data-item-index]").html(++i);
		});
	}
	
	function setSrNo() {
		var $purchaseItem=$("#product_table").find("[data-table-item]").not(".m--hide"), subTotal=0.0;
		var i = 0;
		$purchaseItem.each(function (){
			$(this).find("[data-item-index]").html(++i);
		});
	}
	
	function setBackFields() {
		$("#backLength").val($("#frontLength").val());
		$("#backSeat").val($("#frontSeat").val());
		$("#backThai").val($("#frontThai").val());
		$("#backLangot").val($("#frontLangot").val());
		$("#backKnee").val($("#frontKnee").val());
		$("#backMoriMunda").val($("#frontMoriMunda").val());
		$("#backJati").val($("#frontJati").val());
		
		$('#farmotype_form').formValidation('revalidateField','backLength');
		$('#farmotype_form').formValidation('revalidateField','backSeat');
		$('#farmotype_form').formValidation('revalidateField','backThai');
		$('#farmotype_form').formValidation('revalidateField','backLangot');
		$('#farmotype_form').formValidation('revalidateField','backKnee');
		$('#farmotype_form').formValidation('revalidateField','backMoriMunda');
		$('#farmotype_form').formValidation('revalidateField','backJati');
		
		
	}
	function setTableIndex(id) {
		
		tableIndex=id;
	}