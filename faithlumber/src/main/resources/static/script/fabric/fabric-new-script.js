/**
 * This File is For Fabric New Modal
 * Fabric Ajax Insert
 * Fabric Validation
 */

$(document).ready(function(){
	$("#fabric_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savefabric",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			fabricName:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/fabric/fabric/verify",
						type : 'POST'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $("#savefabric").attr("disabled", true);
		 $.post("/fabric/save", {
			 fabricName: $('#fabricName').val()
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#fabric_new_modal').modal('toggle');
			 
			location.reload(true);
			
		 });
		 
	});
	
	$('#fabric_new_modal').on('show.bs.modal', function() {
		$("#savefabric").attr("disabled", false);
		$('#fabric_new_form').formValidation('resetForm', true);
	});
	
	$("#savefabric").click(function() {
		$('#fabric_new_form').data('formValidation').validate();
	});
	
});