/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#farmoparticular_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updatefarmoparticular",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			name:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					}
				}
			},
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#updatefarmoparticular").attr("disabled", true);
		 
		 var checked=0;
		 if($("#updateIsActive").prop('checked') == true) {
			 checked=1;
		 } 
         var isdefault="No"
		 
		 $.post("/farmoparticular/update", {
			 farmoParticularId:$("#updateFarmoParticularId").val(),
			 name: $('#updateName').val(),
			 isActive:checked
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			
			 $('#farmoparticular_update_modal').modal('toggle');
			
			location.reload(true);
			
		 });
	});
	
	$('#farmoparticular_update_modal').on('hide.bs.modal', function() {
		$('#farmoparticular_update_form').formValidation('resetForm', true);
	});
	
	$("#updatefarmoparticular").click(function() {
		$('#farmoparticular_update_form').data('formValidation').validate();
	});
	
});

function updateFarmoParticular(row,id)
{  		//console.log(row);
	
	$("#updatefarmoparticular").attr("disabled", false);
	$('#farmoparticular_update_form').formValidation('resetForm', true);
	var crow=$(row).closest('tr');		
	var name=$(crow).find('td:eq(1)').text();
	var rowindex = $(crow).find('td:eq(0)').text();
	
	$('#updateName').val(name);
	
	if($(crow).find('td:eq(2)').text()=="Yes"){
		$("#updateIsActive").prop('checked',true)
	} else {
		$("#updateIsActive").prop('checked',false)
	}
	
	$('#updateFarmoParticularId').val(id);
	$("#dataIndex").val(rowindex);
	
}