/**
 * This File is For Terms and condition New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#farmoparticular_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savefarmoparticular",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			name:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					}
				}
			},
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#savefarmoparticular").attr("disabled", true);
		 
		 var checked="0";
		 if($("#newIsActive").prop('checked') == true) {
			 checked="1";
		 } 
		 
         var isdefault="0"
		 $.post("/farmoparticular/save", {
			 name: $('#newName').val(),
			 isActive:checked
		 }, function( data,status ) {
			 
			 toastr["success"]("Record Inserted....");
			 
			 $('#farmoparticular_new_modal').modal('toggle');
			 
			 location.reload(true);
		 });
		 
	});
	
	$('#farmoparticular_new_modal').on('show.bs.modal', function() {
		$("#savefarmoparticular").attr("disabled", false);
		$('#farmoparticular_new_form').formValidation('resetForm', true);
	});
	
	$("#savefarmoparticular").click(function() {
		$('#farmoparticular_new_form').data('formValidation').validate();
	});
	
});