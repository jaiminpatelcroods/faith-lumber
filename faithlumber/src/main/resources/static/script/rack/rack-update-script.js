/**
 * This File is For Rack New Modal
 * Rack Ajax Insert
 * Rack Validation
 */

$(document).ready(function(){
	$("#rack_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updaterack",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			rackCode:{
				validators: {
					notEmpty: {
						message: 'The Code is required. '
					},
					remote : {
						message : 'This Code is already exist. ',
						url : "/rack/verify",
						type : 'POST',
						data: function(validator, $field, value) {
                            return {
                            	rackId : $('#rackId').val(),
                            };
                        },
					} 
				}
			},
			/*noOfRow:{
				validators: {
					notEmpty: {
						message: 'The No. of Row is required. '
					},
					regexp: {
						regexp:  /^[0-9,/d{1}]$/,
						message: 'The No. of Row can only consist numberical value O to 9. '
					}
				}
			},
			noOfColumn:{
				validators: {
					notEmpty: {
						message: 'The No. of Column is required. '
					},
					regexp: {
						regexp:  /^[0-5,/d{1}]$/,
						message: 'The No. of Column can only consist numberical value O to 5. '
					}
				}
			}*/
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#updaterack").attr("disabled", true);
		 
		 $.post("/rack/update", {
			 rackCode:$('#updateRackCode').val(),
			 rackId:$('#rackId').val()
			 /*noOfRow:$('#updateNoOfRow').val(),
			 noOfColumn:$('#updateNoOfColumn').val(),*/
			 
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			
			 $('#rack_update_modal').modal('toggle');
			
			location.reload(true);
			
		 });
	});
	
	$('#rack_update_modal').on('hide.bs.modal', function() {
		$('#rack_update_form').formValidation('resetForm', true);
	});
	
	$("#updaterack").click(function() {
		$('#rack_update_form').data('formValidation').validate();
	});
	
});

function updateRack(row,id)
{
	$("#updaterack").attr("disabled", false);
	$('#rack_update_form').formValidation('resetForm', true);
	var crow=$(row).closest('tr');		
	var code=$(crow).find('td:eq(1)').text();
	/*var noOfRow = $(crow).find('td:eq(2)').text();
	var noOfColumn = $(crow).find('td:eq(3)').text();*/
	var rowindex = $(crow).find('td:eq(0)').text();
	$('#updateRackCode').val(code);
	/*$('#updateNoOfRow').val(noOfRow);
	$('#updateNoOfColumn').val(noOfColumn);*/
	$('#rackId').val(id);
	
}