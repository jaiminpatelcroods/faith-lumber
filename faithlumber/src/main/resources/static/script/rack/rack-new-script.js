/**
 * This File is For Rack New Modal
 * Rack Ajax Insert
 * Rack Validation
 */

$(document).ready(function(){
	$("#rack_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#saverack",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			rackCode:{
				validators: {
					notEmpty: {
						message: 'The Code is required. '
					},
					remote : {
						message : 'This Code is already exist. ',
						url : "/rack/verify",
						type : 'POST'
					}
				}
			},
			/*noOfRow:{
				validators: {
					notEmpty: {
						message: 'The No. of Row is required. '
					},
					regexp: {
						regexp:  /^[0-9,/d{1}]$/,
						message: 'The No. of Row can only consist numberical value O to 9. '
					}
				}
			},
			noOfColumn:{
				validators: {
					notEmpty: {
						message: 'The No. of Column is required. '
					},
					regexp: {
						regexp:  /^[0-5,/d{1}]$/,
						message: 'The No. of Column can only consist numberical value O to 5. '
					}
				}
			}*/
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#saverack").attr("disabled", true);
		 
		 $.post("/rack/save", {
			 rackCode:$('#rackCode').val()
			 /*noOfRow:$('#noOfRow').val(),
			 noOfColumn:$('#noOfColumn').val()*/
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#rack_new_modal').modal('toggle');
			
			location.reload(true);
		 });
		 
	});
	
	$('#rack_new_modal').on('show.bs.modal', function() {
		$("#saverack").attr("disabled", false);
		$('#rack_new_form').formValidation('resetForm', true);
	});
	
	$("#saverack").click(function() {
		$('#rack_new_form').data('formValidation').validate();
	});
	
});