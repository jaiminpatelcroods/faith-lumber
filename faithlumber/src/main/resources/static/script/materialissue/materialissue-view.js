/**
 * This File is For Material Issue View Page
 * Material Issue Validation
 */

	var qtyValidator = {
			validators : {
				notEmpty : {
					message : 'The Qty is required. '
				},
				stringLength : {
					max : 20,
					message : 'The Qty must be less than 20 characters long. '
				},
				regexp : {
					regexp:/^((\d+)((\.\d{0,4})?))$/,
					message : 'The Qty is invalid. '
				}
			}
		};
	
	$(document).ready(function() {
		
		$("#material_return").click(function (){
			$("#product_table").find(".mr-td").removeClass("m--hide");
			$("#mr_footer").removeClass("m--hide");
			$("#material_return	").addClass("m--hide");
		});
		$("#mr_cancel").click(function (){
			$("#product_table").find(".mr-td").addClass("m--hide");
			$("#mr_footer").addClass("m--hide");
			$("#material_return").removeClass("m--hide");
		});
		
		//-------------------- Delivery Challan Validation -------------------------------
		$('#materialissue_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			/*live:'disabled', */
			button: {
				selector: "#savematerialissue",
				disabled: "disabled"
			},
			icon : null,
			fields : {
				
			}
		});
		//-------------------- End Delivery Challan Validation ---------------------------
		
		var $productItem=$("#product_table").find("[data-table-item]").not(".m--hide");
		$productItem.each(function (){
			$('#materialissue_form').formValidation('addField',"materialIssueItemVos["+$productItem.attr("data-table-item")+"].returnQty", qtyValidator);
		});
		
	});
	
		