/**
 * This File is For Material Issue
 * Issue Request Validation
 */

	var tableIndex = 0;
	
	var qtyValidator = {
			validators : {
				notEmpty : {
					message : 'The Qty is required. '
				},
				stringLength : {
					max : 20,
					message : 'The Qty must be less than 20 characters long. '
				},
				regexp : {
					regexp:/^((\d+)((\.\d{0,4})?))$/,
					message : 'The Qty is invalid. '
				}
			}
		},
		productValidator = {
			validators : {
				notEmpty : {
					message : 'The product is required. '
				}
	          }
	     },
		designNoValidator = {
			validators : {
				notEmpty : {
					message : 'The design no is required. '
				}
	          }
	     },
		baleNoValidator = {
			validators : {
				notEmpty : {
					message : 'The bale no is required. '
				}
	          }
	     },
	     materialRequestValidator = {
			validators : {
				notEmpty : {
					message : 'please Material Request. '
				}
			}
 	     };
	
	$(document).ready(function(){
		
  		//-------------------- Material Request Validation -------------------------------
		$('#materialissue_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			live:'disabled', 
			button: {
				selector: "#savematerialissue",
				disabled: "disabled"
			},
			icon : null,
			fields : {
				"receivedBy.employeeId" : {
					validators : {
						notEmpty : {
							message : 'please select Received By. '
						}
					}
				},
				issueDate : {
					validators : {
						notEmpty : {
							message : 'The Issue Date is required. '
						}
					}
				},
			}
		});
		//-------------------- End Material Request Validation ---------------------------
		
		if($("#materialIssueId").val() == undefined) {
			$('#materialissue_form').formValidation('addField',"materialRequestVo.materialRequestId",materialRequestValidator);
		} else {
			var $tableItemTemplate=$("#product_table").find("[data-table-item]").not(".m--hide");
			var i = 0;
			$tableItemTemplate.each(function (){
				i = $(this).attr("data-table-item");
				
				$('#materialissue_form').formValidation('addField',"materialIssueItemVos["+i+"].productVariantVo.productVariantId",productValidator);
				$('#materialissue_form').formValidation('addField',"materialIssueItemVos["+i+"].qty",qtyValidator);
				$('#materialissue_form').formValidation('addField',"materialIssueItemVos["+i+"].designNo",designNoValidator);
				$('#materialissue_form').formValidation('addField',"materialIssueItemVos["+i+"].baleNo",baleNoValidator);
				
				if($("#haveDesignno"+i).val() == 0) {
					$("#designNo"+tableIndex).attr("disabled", true)
					$('#materialissue_form').formValidation('enableFieldValidators', "materialIssueItemVos["+tableIndex+"].designNo", false);
				}
				
				if($("#haveBaleno"+i).val() == 0) {
					$("#baleNo"+tableIndex).attr("disabled", true);
					$('#materialissue_form').formValidation('enableFieldValidators', "materialIssueItemVos["+tableIndex+"].baleNo", false);
				}
			});
		}
		
		$("#issueDate").change(function() {
			$('#materialrequest_form').formValidation('revalidateField', 'requestDate');
		});
		
		$("#product_table").on("click",'button[data-item-remove]',function(e) {
			
			e.preventDefault();
			var i = $(this).closest("[data-table-item]").attr("data-table-item");
			
			if($("#materialIssueItemId"+i).val() != undefined) {
				$("#deleteMaterialIssueIds").val($("#deleteMaterialIssueIds").val()+$("#materialIssueItemId"+i).val()+ ",");
			}
			
			$('#materialissue_form').formValidation('removeField',"materialIssueItemVos["+i+"].productVariantVo.productVariantId");
			$('#materialissue_form').formValidation('removeField',"materialIssueItemVos["+i+"].qty");
			$('#materialissue_form').formValidation('removeField',"materialIssueItemVos["+i+"].designNo");
			$('#materialissue_form').formValidation('removeField',"materialIssueItemVos["+i+"].baleNo");
			
			$(this).closest("[data-table-item]").remove();
			
			setTableSrNo();
		});
		
		$("#savematerialissue").click(function (){
			if ($('#materialissue_form').data('formValidation').isValid() == null) {
				$('#materialissue_form').data('formValidation').validate();
			}
			
			if($('#materialissue_form').data('formValidation').isValid() == true) {
				$("#product_table").find("[data-table-item='template']").remove();
			}
		});
		
	});
	
	function addTableItem() {
		
		var	$tableItemTemplate = $("#product_table").find("[data-table-item='template']").clone();
		
		$tableItemTemplate.removeClass("m--hide").attr("data-table-item",tableIndex);
		
		$tableItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
			n=$(this).attr("id");
			n ? $(this).attr("id",n.replace(/{index}/g,tableIndex)) : "";
			n=$(this).attr("name");
			n ? $(this).attr("name",n.replace(/{index}/g,tableIndex)) : "";
			
			$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,tableIndex)) : "";
		});
		
		$("#product_table").find("[data-table-list]").append($tableItemTemplate);
		
		$("#productVariantId"+tableIndex).addClass("m-select2");
		$("#productVariantId"+tableIndex).select2({placeholder:"Select Product",allowClear:0});
		
		$("#designNo"+tableIndex).addClass("m-select2");
		$("#designNo"+tableIndex).select2({placeholder:"Select Design No.",allowClear:0});
		
		$("#baleNo"+tableIndex).addClass("m-select2");
		$("#baleNo"+tableIndex).select2({placeholder:"Select Bale No.",allowClear:0});
		
		$('#materialissue_form').formValidation('addField',"materialIssueItemVos["+tableIndex+"].productVariantVo.productVariantId",productValidator);
		$('#materialissue_form').formValidation('addField',"materialIssueItemVos["+tableIndex+"].qty",qtyValidator);
		$('#materialissue_form').formValidation('addField',"materialIssueItemVos["+tableIndex+"].designNo",designNoValidator);
		$('#materialissue_form').formValidation('addField',"materialIssueItemVos["+tableIndex+"].baleNo",baleNoValidator);
		
		/*For Remote Multiple Select2*/
		setProductRemote(tableIndex);
		
		tableIndex++;
		
		setTableSrNo();
	}
	
	function setProductRemote(i) {
		
		$("#productVariantId"+i).select2({
	        placeholder: "Search Product",
	        allowClear: 0,
	        ajax: {
	            url: "/product/variant/select/json",
	            dataType: "json",
	            type: "GET",
	            delay: 250,
	            data: function(e) {
	                return {
	                    q: e.term,
	                    page: e.page
	                }
	            },
	            processResults: function(e, t) {
					return t.page = t.page || 1, {
	                    results: e.items,
	                    pagination: {
	                        more: 30 * t.page < e.total_count
	                    }
	                }
	            },
	            cache: !0
	        },
	        escapeMarkup: function(e) {
	            return e
	        },
	        minimumInputLength: 1,
	        templateResult: function(e) {
	            if (e.loading) return e.text;
	            return e.text 
	        },
	        templateSelection: function(e) {
	            return e.full_name || e.text
	        }
	    });
	}
	
	function setTableSrNo() {
		var $tableItemTemplate=$("#product_table").find("[data-table-item]").not(".m--hide");
		var i = 0;
		$tableItemTemplate.each(function (){
			$(this).find("[data-item-index]").html(++i);
		});
	}
	
	function getProductVariantInfo(id) {
		
		if($("#productVariantId"+id).val() != "") {
			$.post("/product/variant/"+$("#productVariantId"+id).val()+"/json", {
				
			}, function (data, status) {
				if(status == 'success') {
					
					$tableItem=$("#product_table").find('[data-table-item="'+id+'"]');
					
					$("#qty"+id).val("0");
					$tableItem.find("[data-item-uom]").html(data.productVo.unitOfMeasurementVo.measurementCode);
					$("#haveDesignno"+id).val(data.productVo.haveDesignno);
					$("#haveBaleno"+id).val(data.productVo.haveBaleno);
					

					if(data.productVo.haveDesignno == 0) {
						$("#designNo"+id).attr("disabled", true)
						$('#materialissue_form').formValidation('enableFieldValidators', "materialIssueItemVos["+id+"].designNo", false);
					}
					
					if(data.productVo.haveBaleno == 0) {
						$("#baleNo"+id).attr("disabled", true);
						$('#materialissue_form').formValidation('enableFieldValidators', "materialIssueItemVos["+id+"].baleNo", false);
					}
					getDesignNo(id);
				}
			});
		}
	}
	
	function getMaterialRequestItem() {
		if($("#materialRequestVo").val() != "") {
			$.post("/materialrequest/"+$("#materialRequestVo").val()+"/item/json", {
				
			}, function (data, status) {
				if(status == 'success') {
					var productName = "";
					
					$("#product_table").find("[data-table-item]").not(".m--hide").remove();
					tableIndex = 0;
					$.each(data, function (key, value) {
						
						var	$tableItemTemplate = $("#product_table").find("[data-table-item='template']").clone();
						
						$tableItemTemplate.removeClass("m--hide").attr("data-table-item",tableIndex);
						
						$tableItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
							n=$(this).attr("id");
							n ? $(this).attr("id",n.replace(/{index}/g,tableIndex)) : "";
							n=$(this).attr("name");
							n ? $(this).attr("name",n.replace(/{index}/g,tableIndex)) : "";
							
							$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,tableIndex)) : "";
						});
						
						$tableItemTemplate.find("[data-item-uom]").html(value.productVariantVo.productVo.unitOfMeasurementVo.measurementCode);
						
						$("#product_table").find("[data-table-list]").append($tableItemTemplate);
						
						
						$("#qty"+tableIndex).val(value.qty);
						
						$("#haveDesignno"+tableIndex).val(value.productVariantVo.productVo.haveDesignno);
						$("#haveBaleno"+tableIndex).val(value.productVariantVo.haveBaleno);
						productName = value.productVariantVo.productVo.categoryVo.categoryName + " " 
							+ value.productVariantVo.productVo.name + " "
							+ value.productVariantVo.variantName;
						
						$("#productVariantId"+tableIndex).append($('<option>',{value : value.productVariantVo.productVariantId, text :productName, selected:'selected'}));
						
						$("#productVariantId"+tableIndex).addClass("m-select2");
						$("#productVariantId"+tableIndex).select2({placeholder:"Select Product",allowClear:0});
						
						$("#designNo"+tableIndex).addClass("m-select2");
						$("#designNo"+tableIndex).select2({placeholder:"Select Design No.",allowClear:0});
						
						$("#baleNo"+tableIndex).addClass("m-select2");
						$("#baleNo"+tableIndex).select2({placeholder:"Select Bale No.",allowClear:0});
						
						$('#materialissue_form').formValidation('addField',"materialIssueItemVos["+tableIndex+"].productVariantVo.productVariantId");
						$('#materialissue_form').formValidation('addField',"materialIssueItemVos["+tableIndex+"].qty",qtyValidator);
						$('#materialissue_form').formValidation('addField',"materialIssueItemVos["+tableIndex+"].designNo",designNoValidator);
						$('#materialissue_form').formValidation('addField',"materialIssueItemVos["+tableIndex+"].baleNo",baleNoValidator);
						
						if(value.productVariantVo.productVo.haveDesignno == 0) {
							$("#designNo"+tableIndex).attr("disabled", true)
							$('#materialissue_form').formValidation('enableFieldValidators', "materialIssueItemVos["+tableIndex+"].designNo", false);
						}
						
						if(value.productVariantVo.productVo.haveBaleno == 0) {
							$("#baleNo"+tableIndex).attr("disabled", true);
							$('#materialissue_form').formValidation('enableFieldValidators', "materialIssueItemVos["+tableIndex+"].baleNo", false);
						}
						
						/*For Remote Multiple Select2*/
						setProductRemote(tableIndex);
						
						tableIndex++;
						
						setAllItemDesignNo();
						
					});
					
					setTableSrNo();
				}
			});
		}
	}
	
	function getDesignNo(id){
		
		if($("#haveDesignno"+id).val() == 1 && $("#productVariantId"+id).val() != "") {
			$.post("/product/"+$("#productVariantId"+id).val()+"/designno/json", {
				
			}, function (data, status) {
				if(status == 'success') {
					$("#designNo"+id).empty();
					$("#designNo"+id).append($('<option>',{value :'', text :"Select Design No."}));
					
					$.each(data.designNoList,function(key,value){
						$("#designNo"+id).append($('<option>',{value :value.designNo, text :value.designNo, name:value.qty}));
					});
				}
			});
		}
	}

	function setAllItemDesignNo() {
		
		var $tableItemTemplate=$("#product_table").find("[data-table-item]").not(".m--hide");
		var i = 0, productVariantIds = "";
		$tableItemTemplate.each(function (){
			i = $(this).attr("data-table-item");
			
			if($("#haveDesignno"+i).val() == 1) {
				productVariantIds += $("#productVariantId"+i).val() + ",";
			}
			
		});
		
		$.post("/product/designno/json", {
			productVariantIds : productVariantIds
		}, function (data, status) {
			if(status == 'success') {
				
				var $tableItemTemplate=$("#product_table").find("[data-table-item]").not(".m--hide");
				var i = 0;
				$tableItemTemplate.each(function (){
					i = $(this).attr("data-table-item");
					
					$.each(data, function(key, value) {
						
						if($("#productVariantId"+i).val() == value.productVariantId) {
							
							$("#designNo"+i).empty();
							$("#designNo"+i).append($('<option>',{value :'', text :"Select Design No."}));
							$.each(value.designNoList, function (k, val){
								
								$("#designNo"+i).append($('<option>',{value :val.designNo, text :val.designNo}));
							});
							
							if($("#designNo"+i).data("default") != undefined) {
								
								if($("#designNo"+i).find("option:contains('" +$("#designNo"+i).data("default")+ "')").length == 0) {
									$("#designNo"+i).append($('<option>',{value :$("#designNo"+i).data("default"), text :$("#designNo"+i).data("default")}));
								}
								$("#designNo"+i).val($("#designNo"+i).data("default")).trigger('change');
							}
								
						}
					})
				});
				
			}
		});
	} 
	function getBaleNo(id) {
		if($("#haveBaleno"+id).val() == 1 && $("#designNo"+id).val() != "") {
			$.post("/product/"+$("#productVariantId"+id).val()+"/"+$("#designNo"+id).val()+"/baleno/json", {
				
			}, function (data, status) {
				if(status == 'success') {
					$("#baleNo"+id).empty();
					$("#baleNo"+id).append($('<option>',{value :'', text :"Select Bale No."}));
					
					$.each(data,function(key,value){
						$("#baleNo"+id).append($('<option>',{value :value.baleNo, text :value.baleNo}));
					});
					
					
					if($("#baleNo"+id).data("default") != undefined) {
						if($("#baleNo"+id).find("option:contains('" +$("#baleNo"+id).data("default")+ "')").length == 0) {
							$("#baleNo"+id).append($('<option>',{value :$("#baleNo"+id).data("default"), text :$("#baleNo"+id).data("default")}));
						}
						$("#baleNo"+id).val($("#baleNo"+id).data("default")).trigger('change');
					}
				}
			});
		}
	}
	function setTableIndex(id){
		
		tableIndex=id;
		setTableSrNo();
	}
		