/**
 * This File is For Payment Term_new_form New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	
	$(".paymentterm-select2").each(function() {
    	
    	placeholder="Select...";
    	if($(this).attr("placeholder")) {
    		placeholder=$(this).attr("placeholder");
    	}
    	
    	allowClear=0;
    	if($(this).data('allow-clear')) {
    		allowClear=	!0;
    	}
    	$(this).select2({
    		placeholder:placeholder,
    		allowClear:allowClear,
    		escapeMarkup: function (markup) { return markup; },
			 language: {
	            noResults: function () {
	            	return '<div class="m-demo-icon"><div class="m-demo-icon__preview"><span class=""><i class="flaticon-plus m--font-primary"></i></span></div><div class="m-demo-icon__class"><a href="#" data-toggle="modal" class="m-link m--font-boldest add-paymentterm-btn">Add Payment Term</a></div></div>';
	            }
	        }
    	});
    });
	
	$(document).on("click",".add-paymentterm-btn",function() {
		$('#paymentterm_new_form').formValidation('resetForm', true);
		$("#paymentterm_new_modal").modal("show")
		$("#paymentTermNameModal").val($(".select2-search__field").val());
		
		$(".paymentterm-select2").select2("close");
	});
	
	
	$("#paymentterm_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savepaymentterm",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			paymentTermName:{
				validators: {
					notEmpty: {
						message: 'The Payment Term Name is reqired. '
					}
				}
			},
			paymentTermDay:{
				validators: {
					notEmpty: {
						message: 'The Payment Term Day is required. '
					},
					regexp : {
						regexp : /^[0-9]+$/,
						message : 'The Payment Term Day can only consist of numeric value. '
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("savepaymentterm").attr("disabled", true);
		 
		 $.post("/paymentterm/save", {
			 name: $('#paymentTermNameModal').val(),
			 day:$('#paymentTermDayModal').val()
		 }, function( data,status ) {
			 
			 toastr["success"]("Payment Term Inserted....");
			 $('#paymentterm_new_modal').modal('toggle');
			 $(".paymentterm-select2").prepend($('<option>',{value :data.paymentTermId, text :data.paymentTermName, name: data.paymentTermDay}));
			 $(".paymentterm-select2").val(data.paymentTermId).trigger("change");
			
		 });
		 
	});
	
	$('#paymentterm_new_modal').on('show.bs.modal', function() {
		$("savepaymentterm").attr("disabled", false);
		$('#paymentTermNameModal').focus();
	});
	
	$("#savepaymentterm").click(function() {
		$('#paymentterm_new_form').data('formValidation').validate();
	});
	
});