/**
 * This File is For Payment Term_new_form New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#paymentterm_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savepaymentterm",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			paymentTermName:{
				validators: {
					notEmpty: {
						message: 'The Payment Term Name is reqired. '
					}
				}
			},
			paymentTermDay:{
				validators: {
					notEmpty: {
						message: 'The Payment Term Day is required. '
					},
					regexp : {
						regexp : /^[0-9]+$/,
						message : 'The Payment Term Day can only consist of numeric value. '
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("savepaymentterm").attr("disabled", true);
		 
		 $.post("/paymentterm/save", {
			 name: $('#paymentTermNameModal').val(),
			 day:$('#paymentTermDayModal').val()
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#paymentterm_new_modal').modal('toggle');
			 
			location.reload(true);
			
		 });
		 
	});
	
	$('#paymentterm_new_modal').on('show.bs.modal', function() {
		$("savepaymentterm").attr("disabled", false);
		$('#paymentterm_new_form').formValidation('resetForm', true);
	});
	
	$("#savepaymentterm").click(function() {
		$('#paymentterm_new_form').data('formValidation').validate();
	});
	
});