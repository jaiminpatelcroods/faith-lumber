/**
 * This File is For Payment Term New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#paymentterm_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updatepaymentterm",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			paymentTermName:{
				validators: {
					notEmpty: {
						message: 'The Payment Term Name is reqired. '
					}
				}
			},
			paymentTermDay:{
				validators: {
					notEmpty: {
						message: 'The Payment Term Day is required. '
					},
					regexp : {
						regexp : /^[0-9]+$/,
						message : 'The Payment Term Day can only consist of numeric value. '
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("updatepaymentterm").attr("disabled", true);
		 
		 $.post("/paymentterm/update", {
			 name: $('#updatePaymentTermName').val(),
			 day:$('#updatePaymentTermDay').val(),
			 id:$('#paymentTermId').val()
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			
			 $('#paymentterm_update_modal').modal('toggle');
			
			location.reload(true);
			
		 });
	});
	
	$('#paymentterm_update_modal').on('hide.bs.modal', function() {
		$('#paymentterm_update_form').formValidation('resetForm', true);
	});
	
	$("#updatepaymentterm").click(function() {
		$('#paymentterm_update_form').data('formValidation').validate();
	});
	
});

function updatePaymentTerm(row,id)
{
	$("updatepaymentterm").attr("disabled", false);
	$('#paymentterm_update_form').formValidation('resetForm', true);
	var crow=$(row).closest('tr');		
	var name=$(crow).find('td:eq(1)').text();
	var day = $(crow).find('td:eq(2)').text();
	var rowindex = $(crow).find('td:eq(0)').text();
	
	$('#updatePaymentTermName').val(name);
	$('#updatePaymentTermDay').val(day);
	$('#paymentTermId').val(id);
	$("#dataindex").val(rowindex);	
	
}