/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	
$(".uom-select2").each(function() {
    	
    	placeholder="Select...";
    	if($(this).attr("placeholder")) {
    		placeholder=$(this).attr("placeholder");
    	}
    	
    	allowClear=0;
    	if($(this).data('allow-clear')) {
    		allowClear=	!0;
    	}
    	$(this).select2({
    		placeholder:placeholder,
    		allowClear:allowClear,
    		escapeMarkup: function (markup) { return markup; },
			 language: {
	            noResults: function () {
	            	return '<div class="m-demo-icon"><div class="m-demo-icon__preview"><span class=""><i class="flaticon-plus m--font-primary"></i></span></div><div class="m-demo-icon__class"><a href="#" data-toggle="modal" class="m-link m--font-boldest add-uom-btn">Add UOM</a></div></div>';
	            }
	        }
    	});
    });
	
	$(document).on("click",".add-uom-btn",function() {
		$('#uom_new_form').formValidation('resetForm', true);
		$("#uom_new_modal").modal("show")
		$("#measurementNameModal").val($(".select2-search__field").val());
		$("#decimalPlacesModal").val(2);
		$(".uom-select2").select2("close");
	});
	
	$("#uom_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#saveuom",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			Uom:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/unitofmeasurement/unitofmeasurement/verify",
						type : 'POST'
					}
				}
			},
			UomCode:{
				validators: {
					notEmpty: {
						message: 'The Code is required. '
					},
				}
			},
			noOfDecimalPlaces:{
				validators: {
					notEmpty: {
						message: 'The No. of Decimal is required. '
					},
					regexp: {
						regexp:  /^[0-4,/d{1}]$/,
						message: 'The No. of Decimal Places can only consist numberical value O tO 4. '
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $("#saveuom").attr("disabled",true);
		 $.post("/unitofmeasurement/save", {
			 name: $('#measurementNameModal').val(),
			 code:$('#measurementCodeModal').val(),
			 decimal:$('#decimalPlacesModal').val(),
		 }, function( data,status ) {
			 
			 toastr["success"]("UOM Inserted....");
			 $('#uom_new_modal').modal('toggle');
			 $(".uom-select2").prepend($('<option>',{value :data.measurementId, text :data.measurementCode}));
			 $(".uom-select2").val(data.measurementId).trigger("change");
		 });
		 
	});
	
	$('#uom_new_modal').on('shown.bs.modal', function() {
		$("#saveuom").attr("disabled",false);
		$('#measurementNameModal').focus();
	});
	
	$("#saveuom").click(function() {
		$('#uom_new_form').data('formValidation').validate();
	});
	
});