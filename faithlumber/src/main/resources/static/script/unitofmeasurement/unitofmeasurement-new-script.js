/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#uom_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#saveuom",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			Uom:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/unitofmeasurement/unitofmeasurement/verify",
						type : 'POST'
					}
				}
			},
			UomCode:{
				validators: {
					notEmpty: {
						message: 'The Code is required. '
					},
				}
			},
			noOfDecimalPlaces:{
				validators: {
					notEmpty: {
						message: 'The No. of Decimal is required. '
					},
					regexp: {
						regexp:  /^[0-4,/d{1}]$/,
						message: 'The No. of Decimal Places can only consist numberical value O tO 4. '
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $("#saveuom").attr("disabled",true);
		 $.post("/unitofmeasurement/save", {
			 name: $('#measurementNameModal').val(),
			 code:$('#measurementCodeModal').val(),
			 decimal:$('#decimalPlacesModal').val(),
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#uom_new_modal').modal('toggle');
			 
			location.reload(true);
			
		 });
		 
	});
	
	$('#uom_new_modal').on('show.bs.modal', function() {
		$("#saveuom").attr("disabled",false);
		$('#uom_new_form').formValidation('resetForm', true);
	});
	
	$("#saveuom").click(function() {
		$('#uom_new_form').data('formValidation').validate();
	});
	
});