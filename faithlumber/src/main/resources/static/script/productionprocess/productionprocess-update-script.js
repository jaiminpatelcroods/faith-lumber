/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#productionprocess_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updatefarmoparticular",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			'productionTypeVo.productionTypeId':{
				validators: {
					notEmpty: {
						message: 'Please Select Production Type. '
					}
				}
			},
			productionProcessName:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					}
				}
			},
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#updatefarmoparticular").attr("disabled", true);
		 
		 $.post("/production/process/update", {
			 productionProcessId:$("#updateProductionProcessId").val(),
			 productionTypeId: $('#updateProductionId').val(),
			 productionProcessName:$('#updateProductionProcessName').val()
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			
			 $('#productionprocess_update_modal').modal('toggle');
			
			location.reload(true);
			
		 });
	});
	
	$('#productionprocess_update_modal').on('shown.bs.modal', function() {
		$("#updateProductionId").select2({dropdownParent: $("#updateProductionId").parent()});
	});
	
	$('#productionprocess_update_modal').on('hide.bs.modal', function() {
		$('#productionprocess_update_form').formValidation('resetForm', true);
	});
	
	$("#updatefarmoparticular").click(function() {
		$('#productionprocess_update_form').data('formValidation').validate();
	});
	
});

function updateFarmoParticular(row,id)
{  
	$("#updatefarmoparticular").attr("disabled", false);
	
	$('#productionprocess_update_form').formValidation('resetForm', true);
	var crow=$(row).closest('tr');		
	var rowindex = $(crow).find('td:eq(0)').text();
	var productionTypeId=$(crow).find('td:eq(1)').attr('data-production-type-id');
	var productionProcessName=$(crow).find('td:eq(2)').text();
	$("#updateProductionProcessId").val(id)
	$('#updateProductionId').val(productionTypeId);
	$("#updateProductionProcessName").val(productionProcessName)
	$("#dataIndex").val(rowindex);
	$('#productionprocess_update_modal').modal('toggle');
	
}