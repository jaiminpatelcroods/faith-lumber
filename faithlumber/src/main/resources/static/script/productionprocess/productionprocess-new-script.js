/**
 * This File is For Terms and condition New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#productionprocess_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#saveProductionProcess",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			'productionTypeVo.productionTypeId':{
				validators: {
					notEmpty: {
						message: 'Please Select Production Type. '
					}
				}
			},
			productionProcessName:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					}
				}
			},
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#saveProductionProcess").attr("disabled", true);
		 
		 $.post("/production/process/save", {
			 productionTypeId: $('#newProductionId').val(),
			 productionProcessName:$('#newProductionProcessName').val()
		 }, function( data,status ) {
			 
			 toastr["success"]("Record Inserted....");
			 
			 $('#productionprocess_new_modal').modal('toggle');
			 
			 location.reload(); 
			 
			
			
		 });
		 
	});
	
	$('#productionprocess_new_modal').on('shown.bs.modal', function() {
		$("#saveProductionProcess").attr("disabled", false);
		$("#newProductionId").select2({dropdownParent: $("#newProductionId").parent()});
		$('#productionprocess_new_form').formValidation('resetForm', true);
	});
	
	
	$("#saveProductionProcess").click(function() {
		$('#productionprocess_new_form').data('formValidation').validate();
	});
	
});