/**
 * This File is For Material Request
 * Material Request Validation
 */


	var tableIndex = 0;
	
	var qtyValidator = {
			validators : {
				notEmpty : {
					message : 'The Qty is required. '
				},
				stringLength : {
					max : 20,
					message : 'The Qty must be less than 20 characters long. '
				},
				regexp : {
					regexp:/^((\d+)((\.\d{0,4})?))$/,
					message : 'The Qty is invalid'
				}
			}
		},
		productValidator = {
			validators : {
				notEmpty : {
					message : 'The product is required. '
				}
	          }
	     };
	
	$(document).ready(function(){
		
  		//-------------------- Material Request Validation -------------------------------
		$('#materialrequest_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			/*live:'disabled', */
			button: {
				selector: "#savematerialrequest",
				disabled: "disabled"
			},
			icon : null,
			fields : {
				"requestedBy.employeeId" : {
					validators : {
						notEmpty : {
							message : 'please select Requested By. '
						}
					}
				},
				requestDate : {
					validators : {
						notEmpty : {
							message : 'The Request Date is required. '
						}
					}
				},
				requireDate : {
					validators : {
						notEmpty : {
							message : 'The Require Date is required. '
						}
					}
				},
			}
		});
		//-------------------- End Material Request Validation ---------------------------
		
		$("#requestDate").change(function() {
			$('#materialrequest_form').formValidation('revalidateField', 'requestDate');
		});
		
		$("#requireDate").change(function() {
			$('#materialrequest_form').formValidation('revalidateField', 'requireDate');
		});
		
		$("#product_table").on("click",'button[data-item-remove]',function(e) {
			
			e.preventDefault();
			var i = $(this).closest("[data-table-item]").attr("data-table-item");
			
			if($("#materialRequestItemId"+i).val() != undefined) {
				$("#deleteMaterialRequestIds").val($("#deleteMaterialRequestIds").val()+$("#materialRequestItemId"+i).val()+ ",");
			}
			
			$('#materialrequest_form').formValidation('removeField',"materialRequestItemVos["+i+"].productVariantVo.productVariantId");
			$('#materialrequest_form').formValidation('removeField',"materialRequestItemVos["+i+"].qty");
			
			$(this).closest("[data-table-item]").remove();
			
			setTableSrNo();
		});
		
		$("#savematerialrequest").click(function (){
			
			if ($('#materialrequest_form').data('formValidation').isValid() == null) {
				$('#materialrequest_form').data('formValidation').validate();
			}
			
			if($("#product_table").find("[data-table-item]").not(".m--hide").length == 0) {
				toastr.options = {
				  "closeButton": true,
				  "debug": false,
				  "newestOnTop": false,
				  "progressBar": false,
				  "positionClass": "toast-top-center",
				  "preventDuplicates": true,
				  "onclick": null,
				  "showDuration": "300",
				  "hideDuration": "1000",
				  "timeOut": "5000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "fadeIn",
				  "hideMethod": "fadeOut"
				};
				toastr.error("","add minimum one product");
				
				return false;
			}
			
			if($('#materialrequest_form').data('formValidation').isValid() == true) {
				
				if($("#jobworkVo").val() == '') {
					$("#jobworkVo").remove();
				}
				$("#product_table").find("[data-table-item='template']").remove();
			}
		});
		
	});
	
	function addTableItem() {
		
		var	$tableItemTemplate = $("#product_table").find("[data-table-item='template']").clone();
		
		$tableItemTemplate.removeClass("m--hide").attr("data-table-item",tableIndex);
		
		$tableItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
			n=$(this).attr("id");
			n ? $(this).attr("id",n.replace(/{index}/g,tableIndex)) : "";
			n=$(this).attr("name");
			n ? $(this).attr("name",n.replace(/{index}/g,tableIndex)) : "";
			
			$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,tableIndex)) : "";
		});
		
		$("#product_table").find("[data-table-list]").append($tableItemTemplate);
		
		$("#productVariantId"+tableIndex).addClass("m-select2");
		$("#productVariantId"+tableIndex).select2({placeholder:"Select Product",allowClear:0});
		
		$('#materialrequest_form').formValidation('addField',"materialRequestItemVos["+tableIndex+"].productVariantVo.productVariantId");
		$('#materialrequest_form').formValidation('addField',"materialRequestItemVos["+tableIndex+"].qty",qtyValidator);
		
		/*For Remote Multiple Select2*/
		setProductRemote(tableIndex);
		
		tableIndex++;
		
		setTableSrNo();
	}
	
	function setProductRemote(i) {
		
		$("#productVariantId"+i).select2({
	        placeholder: "Search Product",
	        allowClear: 0,
	        ajax: {
	            url: "/product/variant/select/json",
	            dataType: "json",
	            type: "GET",
	            delay: 250,
	            data: function(e) {
	                return {
	                    q: e.term,
	                    page: e.page
	                }
	            },
	            processResults: function(e, t) {
					return t.page = t.page || 1, {
	                    results: e.items,
	                    pagination: {
	                        more: 30 * t.page < e.total_count
	                    }
	                }
	            },
	            cache: !0
	        },
	        escapeMarkup: function(e) {
	            return e
	        },
	        minimumInputLength: 1,
	        templateResult: function(e) {
	            if (e.loading) return e.text;
	            return e.text 
	        },
	        templateSelection: function(e) {
	            return e.full_name || e.text
	        }
	    });
	}
	function setTableSrNo() {
		var $tableItemTemplate=$("#product_table").find("[data-table-item]").not(".m--hide");
		var i = 0;
		$tableItemTemplate.each(function (){
			$(this).find("[data-item-index]").html(++i);
		});
	}
	
	function getProductVariantInfo(id) {
		
		if($("#productVariantId"+id).val() != "") {
			$.post("/product/variant/"+$("#productVariantId"+id).val()+"/json", {
				
			}, function (data, status) {
				if(status == 'success') {
					
					$tableItem=$("#product_table").find('[data-table-item="'+id+'"]');
					
					$("#qty"+id).val("0");
					$tableItem.find("[data-item-uom]").html(data.productVo.unitOfMeasurementVo.measurementCode);
				}
			});
		}
	}
	
	function setTableIndex(id){
		
		tableIndex=id;
		setTableSrNo();
	}
		