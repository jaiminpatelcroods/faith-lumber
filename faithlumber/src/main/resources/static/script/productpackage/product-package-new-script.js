/**
 * This File is For Contact New
 * Contact Validation
 */
var productVariant;	
var variantTableIndex=0;

var priceValidator = {
		validators : {
			notEmpty : {
				message : 'The price is required. '
			},
			stringLength : {
				max : 20,
				message : 'The price must be less than 20 characters long. '
			},
			regexp : {
				regexp:/^((\d+)((\.\d{0,2})?))$/,
				message : 'The price is invalid. '
			}
		}
	},qtyValidator = {
		validators : {
			notEmpty : {
				message : 'The qty is required. '
			},
			stringLength : {
				max : 20,
				message : 'The qty must be less than 20 characters long. '
			},
			regexp : {
				regexp:/^((\d+)((\.\d{0,2})?))$/,
				message : 'The qty is invalid. '
			}
		}
	},
	itemCodeValidator = {
		validators : {
			notEmpty : {
				message : 'The Item Code is required. '
			},
			stringLength : {
				max : 20,
				message : 'The Item Code must be less than 20 characters long. '
			},
			regexp : {
				regexp : /^[a-zA-Z0-9]+$/,
				message : 'The Item Code can only consist of alphabetical, number. '
			}
			
		}
	},
	variationName = {
		validators : {
			notEmpty : {
				message : 'The Variant Name is required. '
			},
			stringLength : {
				max : 20,
				message : 'The Variant Name must be less than 20 characters long. '
			},
			regexp : {
				regexp : /^[a-zA-Z0-9,-/]+$/,
				message : 'The Variant Name can only consist of alphabetical, number. '
			}
			
		}
	}, 
	productValidator = {
		validators : {
			notEmpty : {
				message : 'Product is required. '
			}
		}
	};
	
	$(document).ready(function(){
		
		$("#product_form").formValidation({
			framework : 'bootstrap',
			live:'disabled',
			excluded : ":disabled",
			button:{
				selector : "#save_product",
				disabled : "disabled",
			},
			icon : null,
			fields : {
				name : {
					validators : {
						notEmpty: {
							message: 'Name is required. '
						},
						stringLength : {
							max : 50,
							message : 'The Name can not exceed 50 character. '
						},
					}
				},
				
				
			}
		});
		
		if($("#packageId").val() == undefined) {
			$('#product_form').formValidation('addField',"productVo.productId", productValidator);
		}
		/**
		 * Variant Repeater 
		 */
		
		$("#variation_ids_repeater").repeater({
			initEmpty: false,
	        isFirstItemUndeletable: true,
			show: function() {
	            if($("#productId").val() == undefined || $("#productId").val() == ''){
	            	toastr["error"]("Please select the product.");
	            	return false;
	            }
	        	index=$(this).index();
	        	var $variantTable = $(this).find("[data-variant-table]").attr("data-variant-table",variantTableIndex);
	        	var $variantTemplate= $variantTable.find('[data-table-item="template"]');
	        	var tableRowIndex = 0;
	        	
				$.each(productVariant, function(k,v){
					$variant = $variantTemplate.clone();
					$variant.removeClass("m--hide").attr("data-table-item",tableRowIndex);
					$variant.find("[data-item-name]").html(v.variantName).end()
					$variant.find("[id*=parentVariantId]").val(v.productVariantId).end()
					$variantTable.find('[data-table-list]').append($variant);
					$('#product_form').formValidation('addField',"productVariantVos["+variantTableIndex+"].parentVariantQuantites", qtyValidator);
					tableRowIndex++
				});
				
	        	$(this).slideDown();
	        	
	        	$('#product_form').formValidation('addField',"productVariantVos["+variantTableIndex+"].variantName", variationName);
				$('#product_form').formValidation('addField',"productVariantVos["+variantTableIndex+"].itemCode", itemCodeValidator);
				$('#product_form').formValidation('addField',"productVariantVos["+variantTableIndex+"].retailerPrice", priceValidator);
				$('#product_form').formValidation('addField',"productVariantVos["+variantTableIndex+"].wholesalerPrice", priceValidator);
				$('#product_form').formValidation('addField',"productVariantVos["+variantTableIndex+"].otherPrice", priceValidator);
				
				variantTableIndex++;
				
			},
	        hide: function(e) {
	        	index =$(this).index();
	        	
	        	$('#product_form').formValidation('removeField',"productVariantVos["+index+"].variantName");
				$('#product_form').formValidation('removeField',"productVariantVos["+index+"].itemCode");
				$('#product_form').formValidation('removeField',"productVariantVos["+index+"].retailerPrice");
				$('#product_form').formValidation('removeField',"productVariantVos["+index+"].wholesalerPrice");
				$('#product_form').formValidation('removeField',"productVariantVos["+index+"].otherPrice");
				
	        	//$('#contact_form').formValidation('removeField',"contactAddressVos["+index+"].pinCode");
	        	
	        	/*if($("#deleted-variants").val() != undefined) {
	        		if($("#productVariantId"+index).val() != '') {
	        			$("#deleted-address").val($("#deleted-address").val() + $("#productVariantId"+index).val() + ",");
	        		}
	        	}*/
	        	
	            $(this).remove()
	        },
	        
	        ready: function (setIndexes) {
	        	
	        },
	        
	    });
		
		$("#variation_ids_repeater").find("[data-repeater-item]").each(function() {
			index=$(this).index();
	    });
		
		$("#productId").on('change',function() {
			
			if($("#productId").val() != '') {
				$("#variation_ids_repeater").find("[data-repeater-item]").remove();
				variantTableIndex = 0;
				getProductInfo();
			}
		});
		
		$("#save_product").click(function(){
			
			if($('#product_form').data('formValidation').isValid() == null) {
				$('#product_form').data('formValidation').validate();
			}
			
			if($("#variation_ids_repeater").find("[data-repeater-item]").length == 0) {
				
				toastr.error("","add minimum one package");
				
				return false;
				
			} else if($('#product_form').data('formValidation').isValid() == true) {
				
			}
			
		});
		
	});
	function variantChange(checkbox){
		var rownumber = $(checkbox).closest("[data-table-item]").attr("data-table-item")
		
		if ($(checkbox).is(':checked')) {
			$(checkbox).closest("[data-table-item]").find("input[id*='variantQtys']").removeAttr("disabled")
			$(checkbox).closest("[data-table-item]").find("input[id*='parentVariantIds']").removeAttr("disabled")
		}else{
			$(checkbox).closest("[data-table-item]").find("input[id*='variantQtys']").attr("disabled","disabled")
			$(checkbox).closest("[data-table-item]").find("input[id*='parentVariantIds']").attr("disabled","disabled")
		}
	}
	
	function getProductInfo() {
		
		$.post("/product/"+$("#productId").val()+"/json", {
			
		}, function (data, status) {
			if(status == 'success') {
				productVariant = data.productVariantVos;
				
			} else {
				console.log("status: "+status);
			}
		});
	}