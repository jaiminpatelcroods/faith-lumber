/**
 * This File is For Terms and condition New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#termsandcondition_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savetermsandcondition",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			termsCondition:{
				validators: {
					notEmpty: {
						message: 'The Terms And Condition is required. '
					}
				}
			},
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#savetermsandcondition").attr("disabled", true);
		 
		 var isDefault = 0;
		 
		 if($("#isDefault").prop("checked") == true) {
			 isDefault = 1
		 }
		 $.post("/termsandcondition/save", {
			 termsCondition: $('#termsCondition').val(),
			 modules:$('#modules').val().join(","),
			 isDefault:isDefault
		 }, function( data,status ) {
			 
			 toastr["success"]("Record Inserted....");
			 
			 $('#termsandcondition_new_modal').modal('toggle');
			 
			 location.reload(true);
		 });
		 
	});
	
	$('#termsandcondition_new_modal').on('show.bs.modal', function() {
		$("#savetermsandcondition").attr("disabled", false);
		$('#termsandcondition_new_form').formValidation('resetForm', true);
	});
	
	$("#savetermsandcondition").click(function() {
		$('#termsandcondition_new_form').data('formValidation').validate();
	});
	
});