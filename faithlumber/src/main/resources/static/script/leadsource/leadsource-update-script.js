/**
 * This File is For lead source Update Modal
 * lead source Ajax Insert
 * lead source Validation
 */

$(document).ready(function(){
	
	//----------lead source------------
	$("#leadsource_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updateleadsource",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			sourceName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/leadsource/verify",
						type : 'POST',
						data: function(validator, $field, value) {
	                            return {
	                            	leadSourceId : $('#updateLeadSourceId').val()
	                            };
	                        }
				       }
			 }
		}
	  } 
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#updateleadsource").attr("disabled", true);
		 
		 $.post("/leadsource/update", {
			 sourceName: $('#updateSourceName').val(),
			 id:$('#updateLeadSourceId').val()
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			 
			 $('#leadsource_update_modal').modal('toggle');
			 
			 location.reload(true);
			
		 });
	});
	
	$("#updateleadsource").click(function() {
		$('#leadsource_update_form').data('formValidation').validate();
	});
	//----------End lead source------------
	
});

//----------lead source Function------------
function updateLeadSource(row,id)
{  		
	$("#updateleadsource").attr("disabled", false);
	
	$('#leadsource_update_form').formValidation('resetForm', true);
	
	var crow=$(row).closest('tr');		
	var name=$(crow).find('td:eq(1)').text();

	$('#updateSourceName').val(name);
	$('#updateLeadSourceId').val(id);

}
//----------End lead source Function------------

