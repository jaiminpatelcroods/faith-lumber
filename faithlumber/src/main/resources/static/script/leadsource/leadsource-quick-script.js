/**
 * This File is For lead source New Modal
 * lead source Ajax Insert
 * lead source Validation
 */
$(document).ready(function(){
	
	$(".leadsource-select2").each(function() {
    	
    	placeholder="Select...";
    	if($(this).attr("placeholder")) {
    		placeholder=$(this).attr("placeholder");
    	}
    	
    	allowClear=0;
    	if($(this).data('allow-clear')) {
    		allowClear=	!0;
    	}
    	$(this).select2({
    		placeholder:placeholder,
    		allowClear:allowClear,
    		escapeMarkup: function (markup) { return markup; },
			 language: {
	            noResults: function () {
	            	return '<div class="m-demo-icon"><div class="m-demo-icon__preview"><span class=""><i class="flaticon-plus m--font-primary"></i></span></div><div class="m-demo-icon__class"><a href="#" data-toggle="modal" class="m-link m--font-boldest add-leadsource-btn">Add Lead Source</a></div></div>';
	            }
	        }
    	});
    });
	
	$(document).on("click",".add-leadsource-btn",function() {
		
		$('#leadsource_new_form').formValidation('resetForm', true);
		$("#leadsource_new_modal").modal("show")
		$("#leadSourceNameModal").val($(".select2-search--dropdown").find(".select2-search__field").val());
		
		$(".leadsource-select2").select2("close");
	});
	
	//----------lead source------------
	$("#leadsource_new_form").formValidation({
		framework : 'bootstrap',
	/*	live:'disabled',*/
		excluded : ":disabled",
		button:{
			selector : "#saveleadsource",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			sourceName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/leadsource/verify",
						type : 'POST'
					}
					
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#saveleadsource").attr("disabled", true);
		 
		 $.post("/leadsource/save", {
			 sourceName: $('#leadSourceNameModal').val(),
		 }, function( data,status ) {
			 toastr["success"]("Lead Source Inserted....");
			 $('#leadsource_new_modal').modal('toggle');
			 $(".leadsource-select2").append($('<option>',{value :data.leadSourceId, text :data.sourceName}));
			 $(".leadsource-select2").val(data.leadSourceId).trigger("change");
			
		 });
		 
	});
	
	$('#leadsource_new_modal').on('shown.bs.modal', function() {
		$("#saveleadsource").attr("disabled", false);
		$('#leadSourceNameModal').focus()
	});
	
	$("#saveleadsource").click(function() {
		
		$('#leadsource_new_form').data('formValidation').validate();
	});
	
	//----------End lead source------------
	
});