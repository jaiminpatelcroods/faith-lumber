/**
 * This File is For lead source New Modal
 * lead source Ajax Insert
 * lead source Validation
 */
$(document).ready(function(){
	
	//----------lead source------------
	$("#leadsource_new_form").formValidation({
		framework : 'bootstrap',
	/*	live:'disabled',*/
		excluded : ":disabled",
		button:{
			selector : "#saveleadsource",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			sourceName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/leadsource/verify",
						type : 'POST'
					}
					
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#saveleadsource").attr("disabled", true);
		 
		 $.post("/leadsource/save", {
			 sourceName: $('#leadSourceNameModal').val(),
		 }, function( data,status ) {
			toastr["success"]("Record Inserted....");
			$('#leadsource_new_modal').modal('toggle');
			 location.reload();
			
		 });
		 
	});
	
	$('#leadsource_new_modal').on('show.bs.modal', function() {
		$("#saveleadsource").attr("disabled", false);
		$('#leadsource_new_form').formValidation('resetForm', true);
	});
	
	$("#saveleadsource").click(function() {
		
		$('#leadsource_new_form').data('formValidation').validate();
	});
	
	//----------End lead source------------
	
});