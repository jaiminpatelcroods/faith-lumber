	$('#assignedemployee_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			live:'disabled', 
			button: {
				selector: "#saveassignedemployee",
				disabled: "disabled"
			},
			icon : null,
			fields : {
				assignedEmployee : {
						validators : {
							notEmpty : {
							message : 'please select Employee. '
						}
					}
				},
			}
		});
		
		$('#assignedemployee_new_modal').on('shown.bs.modal', function() {
			$('#assignedemployee_form').formValidation('resetForm', true);
			$("#employeeId").select2({dropdownParent: $("#employeeId").parent()});
		});
		
		$("#saveassignedemployee").click(function() {
			$('#assignedemployee_form').data('formValidation').validate();
		});
		
	/*--------------------------------------------------Assigned Employee Modal--------------------------*/	
	
		$('#closecomplain_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			live:'disabled', 

			button : {
				selector : "#saveclosecomplain",
				disabled : "disabled"
			},
			icon : null,
			fields : {
				completedDate : {
					validators : {
						notEmpty : {
							message : 'The Completed Date is required. '
						}
					}
				},
				remark : {
					validators : {
						notEmpty : {
							message : 'The Remark is required. '
						}
					}
				},
				correctiveActions : {
					validators : {
						notEmpty : {
							message : 'The Corrective Action is required. '
						}
					}
				},
				preventiveAction : {
					validators : {
						notEmpty : {
							message : 'The Preventive Action is required. '
						}
					}
				},
			}   	
		});	
				

		$('#closecomplain_new_modal').on('show.bs.modal', function() {
			$('#closecomplain_form').formValidation('resetForm', true);
		});
		
		$("#saveclosecomplain").click(function() {
			
			$('#closecomplain_form').data('formValidation').validate();
		});
		
		/*--------------------------------------------Close complain modal----------------------------------*/
		
