/**
 * This File is For Additional Charge New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#additionalcharge_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#saveadditionalcharge",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			additionalCharge:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					}
				}
			},
			defaultAmount:{
				validators: {
					notEmpty: {
						message: 'The Amount is reqired. '
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid. '
					},
				}
			},
			"taxVo.taxId":{
				validators: {
					notEmpty: {
						message: 'The Tax is reqired. '
					}
				}
			},
			hsnCode:{
				validators : {
					stringLength : {
						max : 8,
						message : 'The hsn code must be 8 characters long. '
					},
					regexp : {
						regexp : /^[0-9]+$/,
						message : 'The hsn can only consist of number. '
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("saveadditionalcharge").attr("disabled", true);
		 
		 $.post("/additionalcharge/save", {
			 additionalCharge: $('#additionalCharge').val(),
			 defaultAmount:$('#defaultAmount').val(),    
			 hsnCode:$('#hsnCode').val(),
			 tax:$('#taxVo').val()
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#additionalcharge_new_modal').modal('toggle');
			 
			location.reload(true);
			
		 });
		 
	});
	
	$('#additionalcharge_new_modal').on('shown.bs.modal', function() {
		
		$("saveadditionalcharge").attr("disabled", false);
		
		$('#additionalcharge_new_form').formValidation('resetForm', true);
		$("#taxVo").select2({dropdownParent: $("#taxVo").parent()});
		$("#defaultAmount").val(0);
		$("#additionalCharge").focus();
	});
	
	$("#saveadditionalcharge").click(function() {
		$('#additionalcharge_new_form').data('formValidation').validate();
	});
	
});