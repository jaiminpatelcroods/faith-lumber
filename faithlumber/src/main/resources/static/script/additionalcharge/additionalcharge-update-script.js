/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#additionalcharge_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updateadditionalcharge",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			additionalCharge:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					}
				}
			},
			defaultAmount:{
				validators: {
					notEmpty: {
						message: 'The Amount Is reqired. '
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid. '
					},
				}
			},
			"taxVo.taxId":{
				validators: {
					notEmpty: {
						message: 'The Tax is reqired. '
					}
				}
			},
			hsnCode:{
				validators : {
					stringLength : {
						max : 8,
						message : 'The hsn code must be 8 characters long. '
					},
					regexp : {
						regexp : /^[0-9]+$/,
						message : 'The hsn can only consist of number. '
					}
				}
			},
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("updateadditionalcharge").attr("disabled", false);
				 
		 $.post("/additionalcharge/update", {
			 additionalCharge: $('#updateAdditionalCharge').val(),
			 defaultAmount:$('#updateDefaultAmount').val(),
			 additionalChargeId:$('#additionalChargeId').val(),
			 tax:$('#updateTaxVo').val()	,
			 hsnCode:$('#updateHsnCode').val(),
		 }, function( data,status ) {
			
			 toastr["success"]("Record Updated....");
			
			 $('#additionalcharge_update_modal').modal('toggle');
			
			location.reload(true);
			
		 });
	});
	
	$('#additionalcharge_update_modal').on('shown.bs.modal', function() {
		$("#updateTaxVo").select2({dropdownParent: $("#updateTaxVo").parent()});
	});
	
	$("#updateadditionalcharge").click(function() {
		$('#additionalcharge_update_form').data('formValidation').validate();
	});
	
});

function updateAdditionalCharge(row,id)
{  		//console.log(row);
	
	$("updateadditionalcharge").attr("disabled", false);
	
	$('#additionalcharge_update_form').formValidation('resetForm', true);
	
	var crow=$(row).closest('tr');		
	
	var name=$(crow).find('td:eq(1)').text();
	var amount = $(crow).find('td:eq(2)').text();
	var tax = $(crow).find("input[name='taxId"+id+"']").val();
	var hsn = $(crow).find('td:eq(4)').text();
	var rowindex = $(crow).find('td:eq(0)').text();
	$('#updateAdditionalCharge').val(name);
	$('#updateDefaultAmount').val(amount);
	$('#updateHsnCode').val(hsn);
	$('#updateTaxVo').val(tax).trigger('change');
	$('#additionalChargeId').val(id);
	$("#dataindex").val(rowindex);
	
}