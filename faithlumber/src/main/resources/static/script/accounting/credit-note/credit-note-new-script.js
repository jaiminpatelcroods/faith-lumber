/**
 * This File is For Credit Note New
 * Credit Note Validation
 */

$(document).ready(function(){
	
	$("#credit_note_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savecreditnote",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			transactionDate:{
				validators: {
					notEmpty: {
						message: 'Transaction Date is required. '
					}
				}
			},
			"fromAccountCustomVo.accountCustomId":{
				validators: {
					notEmpty: {
						message: 'From Account is required. '
					}
				}
			},
			"toAccountCustomVo.accountCustomId":{
				validators: {
					notEmpty: {
						message: 'To Account is required. '
					}
				}
			},
			amount:{
				validators: {
					notEmpty: {
						message: 'Amount is required. '
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid. '
					}
				}
			},
			description:{
				validators: {
					
				}
			}
		}
	});
	
	$('#transactionDate').datepicker({
		format: 'dd/mm/yyyy',autoclose: true
	}).on('changeDate', function(e) {
		$('#credit_note_new_form').formValidation('revalidateField', 'transactionDate'); 
	});
});