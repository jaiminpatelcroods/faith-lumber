/**
 * This File is For Account New Modal
 * Account Ajax Insert
 * Account Validation
 */

$(document).ready(function(){
	
	//----------Department------------
	$("#account_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#saveaccount",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			accountName:{
				validators: {
					notEmpty: {
						message: 'The Name is Required. '
					}
				}
			},
			"group.accountGroupId":{
				validators: {
					notEmpty: {
						message: 'Account Group is Required. '
					}
				}
			},
			debit:{
				validators: {
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid. '
					}
				}
			},
			credit:{
				validators: {
					regexp : {
						regexp:/^((\d+)((\.\d{0,2})?))$/,
						message : 'The Amount is invalid. '
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 var $form = $(e.target);
		 fv    = $form.data('formValidation');
		 
		 $.post("/account/save", {
			 accountName: $("#accountName").val(),
			 group: $("#accountGroup").val(),
			 debit: $("#debit").val(),
			 credit: $("#credit").val(),
		 }, function( data,status ) {
			toastr["success"]("Record Inserted....");
			
			location.reload(true);
			
		 });
		 
	});
	
	$('#account_new_modal').on('shown.bs.modal', function() {
		$('#account_new_form').formValidation('resetForm', true);
		$("#accountGroup").select2({dropdownParent: $("#accountGroup").parent(),placeholder:"Select Account Group"});
	});
	
	$("#saveaccount").click(function() {
		$('#account_new_form').data('formValidation').validate();
	});
	
	//----------End Department------------
	
});