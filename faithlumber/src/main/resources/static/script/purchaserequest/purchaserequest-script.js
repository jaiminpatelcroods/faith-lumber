/**
 * This File is For Purchase Request
 * Purchase RequestValidation
 */

	var index = 0;
	
	var priceValidator = {
			validators : {
				notEmpty : {
					message : 'The price is required. '
				},
				stringLength : {
					max : 20,
					message : 'The price must be less than 20 characters long. '
				},
				regexp : {
					regexp:/^((\d+)((\.\d{0,2})?))$/,
					message : 'The price is invalid. '
				}
			}
		};
	
	$(document).ready(function(){
		
		
		//-------------------- Purchase Validation -------------------------------
		$('#purchaserequest_new_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			/*live:'disabled', */
			button: {
				selector: "#savepurchaserequest",
				disabled: "disabled"
			},
			icon : null,
			fields : {
				purchaseDate : {
					validators : {
						notEmpty : {
							message : 'The Request Date is required. '
						}
					}
				},
				prefix : {
					validators : {
						notEmpty : {
							message : 'The Prefix is required. '
						},
						stringLength : {
							max : 20,
							message : 'The Prefix must be less than 20 characters long. '
						},
						regexp : {
							regexp : /^[a-zA-Z0-9/_-\s-., ]+$/,
							message : 'Prefix is not valid. '
						}
					}
				},
				requestNo : {
					verbose : false,
					validators : {
						stringLength : {
							max : 8,
							message : 'The Request No is required. '
						},
						regexp : {
							regexp : /^[0-9]+$/,
							message : 'The Request No only consist of number. '
						}
					}
				},
				"requestedBy.employeeId" : {
					validators : {
						notEmpty : {
							message : 'please select Requested By. '
						}
					}
				},
			}
		});
		//-------------------- End Purchase Validation ---------------------------
		
		$("#productId").change(function(){
			if($("#productId").val() != "") {
				$("#addProductModal").modal("show");
				$("#productIdModal").val($("#productId").val()).trigger('change');
				$("#productId").val("").trigger('change');
			}
		});
		
		$(document).on("click",'a[data-purchase-item-toggle]',function(e) {
			
			if($(this).closest('[data-table-item]').hasClass("m-table__row--brand")) {
				$(this).closest('[data-table-item]').removeClass("m-table__row--brand");
			} else {
				$(this).closest('[data-table-item]').addClass("m-table__row--brand");
			}
			e.stopPropagation();
		});
		
		$(document).on("click",'tr[data-table-item]',function() {
			
			var obj = new Object();
			var mainArray=[];
			obj.productId = $(this).find("input[id*='productId']" ).val();
			obj.haveVariation=$(this).find("input[id*='haveVariant']" ).val();
			obj.name=$(this).find("[data-table-name]").html();
			obj.purchaseItemIndex = $(this).attr("data-table-item");
			var unitOfMeasurementVo = new Object(); 
			unitOfMeasurementVo.measurementCode = $(this).find("[data-table-uom]").html();
			obj.unitOfMeasurementVo = unitOfMeasurementVo;
			
			var productVariantVos =[]
			
			$variantItem=$("#product_table").find("[data-variant-row='"+$(this).attr("data-table-item")+"']").find("[data-variant-item]");
			
			$variantItem.each(function (){	
				
				/* discountType = $(this).find("input[id*='discountType']" ).val(); */
				
				if($(this).index() == 0) {
					obj.productId=$(this).find("input[id*='productId']" ).val();;
				}
				var variant=new Object();
				variant.productVariantId=$(this).find("input[id*='productVariantId']" ).val();
				variant.variantName=$(this).find("[data-variant-name]" ).html();
				variant.qty=$(this).find("input[id*='qty']" ).val();
				variant.variantItemIndex= $(this).attr("data-variant-item");
				productVariantVos.push(variant);
				
			});
			
			obj.productVariantVos = productVariantVos;
			
			mainArray.push(obj);
			console.log(JSON.stringify(mainArray));
			$("#addProductModal").modal("show");
			setProductOnModal(obj);
			
		});
		
		$("#product_table").on("click",'button[data-item-remove]',function(e) {
			var i=$(this).closest("[data-table-item]").attr("data-table-item");
			
			$("#product_table").find("[data-variant-row='"+i+"']").find("input[id*='purchaseRequestItemId']").each(function() {
				$("#deletePurchaseRequestItemIds").val($("#deletePurchaseRequestItemIds").val() + $(this).val() + ",");
			});
			
			$(this).closest("[data-table-item]").remove();
			$("#product_table").find("[data-variant-row='"+i+"']").remove();
			
			setSrNo();
			return false;
		});
		
		$("#savepurchaserequest").click(function (){
			
			if($('#purchaserequest_new_form').data('formValidation').isValid() == null) {
				$('#purchaserequest_new_form').data('formValidation').validate();
			}
			
			if($("#product_table").find("[data-table-item]").not(".m--hide").length == 0) {
				toastr.options = {
				  "closeButton": true,
				  "debug": false,
				  "newestOnTop": false,
				  "progressBar": false,
				  "positionClass": "toast-top-center",
				  "preventDuplicates": true,
				  "onclick": null,
				  "showDuration": "300",
				  "hideDuration": "1000",
				  "timeOut": "5000",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "fadeIn",
				  "hideMethod": "fadeOut"
				};
				toastr.error("","add minimum one product");
				
				return false;
			} else if($('#purchaserequest_new_form').data('formValidation').isValid() == true) {
				$("#product_table").find("[data-table-item='template']").remove();
				$("#product_table").find("[data-variant-row='template']").remove();
				
				if($("#requireDate").val() == "") {
					$("#requireDate").remove();
				}
			}
			
		});
	});
	
	function getProductInfo() {
		
		var id = $("#productIdModal").val();
		
		if(id != "") {
			$.post("/product/"+id+"/json", {
				
			}, function (data, status) {
				if(status == 'success') {
					setProductOnModal(data);
				}
			});
		}
	}
	
	function setProductOnModal(data) {
		
		$("#variant_table").find("[data-variant-item]").not(".m--hide").remove();
		var $template = $("#variant_table").find("[data-variant-list]").clone();
		
		var haveVariant = $("#modalHaveVariant");
		
		haveVariant.val(data.haveVariation);
		$("#purchaseItemIndex").val(data.purchaseItemIndex ? data.purchaseItemIndex : "-1");
		$("#modalUOM").val(data.unitOfMeasurementVo.measurementCode);
		$.each(data.productVariantVos, function( key, value ) {
			
			$clone = $template.clone();
			$clone.find("[data-variant-item]").removeClass('m--hide');
			$clone.find("[data-variant-item]").attr("data-variant-item", value.variantItemIndex ? value.variantItemIndex : "");
			
			//$clone.find("input[id*='modalQty]'").val(value.qty ? value.qty : "");
			if(data.haveVariation == 1) {
				
				//$clone.find("[data-variant-item]").find("[id='modalQty{index}']").val(value.qty ? value.qty : "0");
				$clone.find("[data-variant-item]").find("[id='modalProductVariantId{index}']").val(value.productVariantId);
				
				$clone.find("input[type='text'],input[type='hidden']").each(function (){
					n=$(this).attr("id");
					n ? $(this).attr("id",n.replace(/{index}/g,key)) : "";
				});
				
				$clone.find("[data-variant-name]").html(value.variantName);
				
				$("#variant_table").find("[data-variant-list]").append($clone.html());
				$("#modalQty"+key).val(value.qty ? value.qty : "")
			}
			
			if(key == 0) {
				$("#modalProductVariantId").val(value.productVariantId);
				$("#modalQty").val(value.qty ? value.qty : "1");
				
			}
		});
		
		if(data.purchaseItemIndex) {
			$("#productIdModal").val(data.productId);
			$("#productIdModal").select2();
		}
		
		if(haveVariant.val() == 1) {
			$("#qty_div").addClass("m--hide");
			$("#variant_div").removeClass("m--hide");
		} else {
			$("#variant_div").addClass("m--hide");
			$("#qty_div").removeClass("m--hide");
			
		}
		
	}
	
	function setProduct() {
		//modalDescription
		
		
		var	purchaseItemIndex = $("#purchaseItemIndex").val(), haveVariant = $("#modalHaveVariant").val(),
			productVariantId=$("#modalProductVariantId").val(), productId=$("#productIdModal").val();
		
		
		var	$purchaseItemTemplate, $variantTableTemplate;
		var mainIndex=index;
		var selector = purchaseItemIndex==-1 ? "{index}" : purchaseItemIndex;
		
		if(purchaseItemIndex == -1) {
			$purchaseItemTemplate=$("#product_table").find("[data-table-item='template']").clone();
			$variantTableTemplate=$("#product_table").find("[data-variant-row='template']").clone();
		} else {
			$purchaseItemTemplate=$("#product_table").find("[data-table-item='"+purchaseItemIndex+"']");
			$variantTableTemplate=$("#product_table").find("[data-variant-row='"+purchaseItemIndex+"']");
			mainIndex = purchaseItemIndex;
		}
		
		//$purchaseItemTemplate.not("[data-purchase-item='template']").remove();
		$variantTableTemplate.attr("data-variant-row",mainIndex)
		
		if(haveVariant == 0) {
			
			$variantTableTemplate.attr("id", $variantTableTemplate.attr("id").replace(/{index}/g,index));
			qty = $("#modalQty").val();
			
			$variantTableTemplate.find("[data-variant-name]").html("").end()
				
				.find("[data-variant-qty]").html(qty).end()
				.find("[name='purchaseRequestItemVos["+selector+"].product.productId']").val(productId).end()
				.find("[name='purchaseRequestItemVos["+selector+"].productVariantVo.productVariantId']").val(productVariantId).end()
				.find("[name='purchaseRequestItemVos["+selector+"].qty']").val(qty).end();
				
			$variantTableTemplate.find("input[type='hidden']").each(function (){
				n=$(this).attr("id");
				n ? $(this).attr("id",n.replace(/{index}/g,index)) : "";
				
				n=$(this).attr("name");
				n ? $(this).attr("name",n.replace(/{index}/g,index)) : "";
			});
			
		} else {
			//data-variant-item
			
			$variantTableTemplate.attr("id", $variantTableTemplate.attr("id").replace(/{index}/g,index));
			qty = 0.0;
			
			if(purchaseItemIndex == -1) {
				$variantItemTemplate = $variantTableTemplate.find("[data-variant-item]").clone();
				$variantTableTemplate.find("[data-variant-table]").children('tbody').html("");
			} else {
				$variantItemTemplate = $variantTableTemplate.find("[data-variant-table]").children('tbody');
			}
			
			$("#variant_table").find("[data-variant-item]").not(".m--hide").each(function () {
				//var variantItemIndex = $variantItem.attr("data-variant-item") == ""? $variantItem.attr("data-variant-item");
				var variantSelector = "{index}";
				if(purchaseItemIndex == -1)	{
					$variantItem = $variantItemTemplate.clone();
					$variantItem.attr("data-variant-item",index);
				} else {
					$variantItem = $variantItemTemplate.find("[data-variant-item='"+$(this).attr("data-variant-item")+"']");
					variantSelector = $(this).attr("data-variant-item");
				}
				var i=($(this).index())-1;
				productVariantId = $("#modalProductVariantId"+i).val();
				qty += parseFloat($("#modalQty"+($(this).index()-1)).val());
				
				$variantItem.find("[data-variant-name]").html($(this).find("[data-variant-name]").html()).end()
					.find("[data-variant-qty]").html($("#modalQty"+($(this).index()-1)).val()).end()
					.find("[name='purchaseRequestItemVos["+variantSelector+"].product.productId']").val(productId).end()
					.find("[name='purchaseRequestItemVos["+variantSelector+"].productVariantVo.productVariantId']").val(productVariantId).end()
					.find("[name='purchaseRequestItemVos["+variantSelector+"].qty']").val($("#modalQty"+($(this).index()-1)).val()).end();
				
				$variantItem.find("input[type='hidden']").each(function (){
					n=$(this).attr("id");
					n ? $(this).attr("id",n.replace(/{index}/g,index)) : "";
					
					n=$(this).attr("name");
					n ? $(this).attr("name",n.replace(/{index}/g,index)) : "";
				});
				
				if(purchaseItemIndex == -1)	{
					$variantTableTemplate.find("[data-variant-table]").children('tbody').append($variantItem);
					index++;
				}
				
				
			});
		}
		
		var varIcon = '';
		
		if(haveVariant == 0) {
			$purchaseItemTemplate.find("[data-purchase-item-toggle]").removeAttr("data-purchase-item-toggle");
			index++;
		} else {
			varIcon = '  <i class="fa fa-clone m--regular-font-size-sm2 "></i>';
			$purchaseItemTemplate.find("[data-purchase-item-toggle]").attr("href",$purchaseItemTemplate.find("[data-purchase-item-toggle]").attr("href").replace(/{index}/g,mainIndex)).end();
		}
		
		$purchaseItemTemplate.attr("data-table-item",mainIndex)
			.removeClass("m--hide");
		$purchaseItemTemplate.find("[data-table-name]").html($('#productIdModal option[value="'+$('#productIdModal').val()+'"]').html()+ varIcon).end()
			.find("[data-table-uom]").html($("#modalUOM").val()).end()
			.find("[data-table-qty]").html(qty).end()
			.find("[id='haveVariant"+selector+"']").val(haveVariant).end()
			.find("[id='haveVariant"+selector+"']").attr("id",$purchaseItemTemplate.find("[id='haveVariant"+selector+"']").attr("id").replace(/{index}/g,mainIndex)).end()
		
		if(purchaseItemIndex == -1) {
			$("#product_table").find("[data-table-list]").append($purchaseItemTemplate).append($variantTableTemplate);
		}
		
		setSrNo();
		
		$("#addProductModal").modal("hide");
	}
	
	function setSrNo() {
		var $purchaseItem=$("#product_table").find("[data-table-item]").not(".m--hide");
		var i = 0;
		$purchaseItem.each(function (){
			$(this).find("[data-table-index]").html(++i);
		});
	}
	
	function setIndex(i) {
		index = i
	}
	
	function alpha(e) {
	    var k;
	    document.all ? k = e.keyCode : k = e.which;
	    /*46 key code for '.' */
	    /*32 key code for ' ' */
	    /*(k > 64 && k < 91) || (k > 96 && k < 123) ||*/
	    return ( k == 0 || k == 46 || k == 8 || (k >= 48 && k <= 57));
	}