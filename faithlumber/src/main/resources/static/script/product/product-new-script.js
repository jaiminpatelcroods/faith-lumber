/**
 * This File is For Product New
 * Product Validation
 */

	var e = new Array();
	var tagInputColorFlag=1;
	var tagInputColor=["label-danger","label-success"];
	
	var priceValidator = {
			validators : {
				notEmpty : {
					message : 'The price is required. '
				},
				stringLength : {
					max : 20,
					message : 'The price must be less than 20 characters long. '
				},
				regexp : {
					regexp:/^((\d+)((\.\d{0,2})?))$/,
					message : 'The price is invalid. '
				}
			}
		},
		itemCodeValidator = {
				validators : {
					notEmpty : {
						message : 'The Item Code is required. '
					},
					remote : {
						message : 'This Item Code is already exist. ',
						url : "/product/itemcode/verify",
						type : 'POST',
						data: function(validator, $field, value) {
	                        var id = $field.closest("tr").find("[id*=productVariantId]").val();
	                        
	                      //  alert(id ? id : '0')
							return {
	                        	productVariantId : id ? id : '0'
	                        };
	                    },
					},
					stringLength : {
						max : 20,
						message : 'The Item Code must be less than 20 characters long. '
					},
					regexp : {
						regexp : /^[a-zA-Z0-9_-]+$/,
						message : 'The Item Code can only consist of alphabetical, number and underscore. '
					}
					
				}
			};
	
	$(document).ready(function(){
		$('#product_form').formValidation({
			framework: 'bootstrap',
			excluded: ":disabled",
			/*live:'disabled', */
			button: {
				selector: "#save_product",
				disabled: "disabled"
			},
			icon : null,
			fields : {
				name : {
					validators : {
						notEmpty : {
							message : 'The product name is required. '
						},
						stringLength : {
							max : 50,
							message : 'The product name must be less than 50 characters long. '
						},
						regexp : {
							regexp : /^[a-zA-Z0-9_-\s-., ]+$/,
							message : 'The product name can only consist of alphabetical, number and underscore. '
						}
					}
				},
				displayName : {
					validators : {
						notEmpty : {
							message : 'The print name is required. '
						},
						stringLength : {
							max : 50,
							message : 'The print name must be less than 50 characters long. '
						},
						regexp : {
							regexp : /^[a-zA-Z0-9_-\s-., ]+$/,
							message : 'The print name can only consist of alphabetical, number and underscore. '
						}
					}
				},
				'unitOfMeasurementVo.measurementId' : {
					validators : {
						notEmpty : {
							message : 'select Unit of Measurement. '
						}
					}
				},
				'salesTaxVo.taxId' : {
					validators : {
						notEmpty : {
							message : 'select sales tax. '
						}
					}
				},
				/*'purchaseTaxVo.taxId' : {
					validators : {
						notEmpty : {
							message : 'select purchase tax. '
						}
					}
				},*/
				/*hsnCode : {
					validators : {
						stringLength : {
							max : 8,
							message : 'The hsn code must be 8 characters long. '
						},
						regexp : {
							regexp : /^[0-9]+$/,
							message : 'The hsn can only consist of number. '
						}
					}
				},
				'categoryVo.categoryId' : {
					validators : {
						notEmpty : {
							message : 'select Category. '
						}
					}
				},
				'brandVo.brandId' : {
					validators : {
						notEmpty : {
							message : 'select Brand. '
						}
					}
				b}*/
			}
		});
		
		$("#name").change(function() {
			$('#displayName').val($("#name").val());
			$('#product_form').formValidation('revalidateField', 'displayName');
		});
		
		/*
		* whene click on Tax Included checkbox value is change 0 or 1
		*/
		$('input[name="salesTaxIncluded"],input[name="purchaseTaxIncluded"]').click(function(){
	        if($(this).prop("checked") == true){
	            $(this).val(1);
	        }
	        else if($(this).prop("checked") == false){
	        	  $(this).val(0);
	        }
	    });
		
		/*
		* whene click on haveDesignno checkbox value is change 0 or 1
		*/
		$('input[name="haveDesignno"],input[name="haveBaleno"]').click(function(){
	        if($(this).prop("checked") == true){
	            $(this).val(1);
	        }
	        else if($(this).prop("checked") == false){
	        	  $(this).val(0);
	        }
	    });
		
		$("#variant_yes").click(function() {
			
			
			$("#haveVariation").val("1");
			$("#variant_option").removeClass("m--hide");
			$("#variant_button").addClass("m--hide");
			
			$("#single_variant").addClass("m--hide");
			
			if($("#variant_option_repeater div[data-repeater-item]:visible").length == 0) {
				$("#variant_option_repeater button[data-repeater-create]").click();
			}
			if( $("#variant_option_repeater div[data-repeater-item]:visible").length < 2) {
				 $("#variant_option_repeater button[data-repeater-delete]:visible").addClass("m--hide");
			}
			
			$('#product_form').formValidation('removeField',"productVariantVos[0].salesPrice");
		/*	$('#product_form').formValidation('removeField',"productVariantVos[0].retailerPrice");
			$('#product_form').formValidation('removeField',"productVariantVos[0].wholesalerPrice");
			$('#product_form').formValidation('removeField',"productVariantVos[0].otherPrice");*/
			$('#product_form').formValidation('removeField',"productVariantVos[0].itemCode");
			
			$("#salesPrice").attr("name","");
			/*$("#retailerPrice").attr("name","");
			$("#wholesalerPrice").attr("name","");
			$("#otherPrice").attr("name","");*/
			$("#position").attr("name","");
			$("#variantName").attr("name","");
			$("#itemCode").attr("name","");
			
			setVariant();
		});
		
		$("#variant_no").click(function() {
			
			$("#variant_repeater").find('[data-repeater-list]').empty();
			
			$("#haveVariation").val("0");
			$("#variant_option").addClass("m--hide");
			$("#variant_button").removeClass("m--hide");
			$("#single_variant").removeClass("m--hide");
			
			$("#purchasePrice").attr("name","productVariantVos[0].salesPrice");
			/*$("#retailerPrice").attr("name","productVariantVos[0].retailerPrice");
			$("#wholesalerPrice").attr("name","productVariantVos[0].wholesalerPrice");
			$("#otherPrice").attr("name","productVariantVos[0].purchasePrice");*/
			$("#variantName").attr("name","productVariantVos[0].variantName");
			$("#position").attr("name","productVariantVos[0].position");
			$("#itemCode").attr("name","productVariantVos[0].itemCode");
			
			$('#product_form').formValidation('addField',"productVariantVos[0].salesPrice", priceValidator);
		/*	$('#product_form').formValidation('addField',"productVariantVos[0].retailerPrice", priceValidator);
			$('#product_form').formValidation('addField',"productVariantVos[0].wholesalerPrice", priceValidator);
			$('#product_form').formValidation('addField',"productVariantVos[0].otherPrice", priceValidator);*/
			$('#product_form').formValidation('addField',"productVariantVos[0].itemCode", itemCodeValidator);
			
		});
		
		$("#variant_option_repeater").repeater({
			initEmpty: false,	
	        isFirstItemUndeletable: false,
			/* defaultValues: {
	            "text-input": "foo"
	        }, */
	        show: function() {
	           
	        	//$(this).parent().remove();
	        	//$(this).closest('.tagsinput').remove();
	        	
	        	if( $("#variant_option_repeater div[data-repeater-item]:visible").length < 3){
	        		
	        		index=$(this).index();
	        		$(this).find(".bootstrap-tagsinput").remove();
	        		
	        		$(this).find('input[ data-role="tagsinput"]').tagsinput({
        				tagClass:'label '+tagInputColor[0]
        			});
	        		$(this).find('input[ data-role="tagsinput"]').attr("data-tag-color",tagInputColor[0]);
	      			tagInputColor.shift();
	      			
	        		var a, n, o, t, i, s;
	        		
					$('#optionName'+index).typeahead({
		                hint: !0,
		                highlight: !0,
		                minLength: 0	,
		                showHintOnFocus: true
		            }, {
		                name: "Option",
		                source: (a = e, function(e, n) {
		                    var o;
		                    o = [], substrRegex = new RegExp(e, "i"), $.each(a, function(e, a) {
		                        substrRegex.test(a) && o.push(a)
		                    }), n(o)
		                })
		            });
	        		//uv end
					$(this).find('input[ data-role="tagsinput"]').on('itemAdded', function(event) {
						setVariant();
					});
	        		$(this).find('input[ data-role="tagsinput"]').on('itemRemoved', function(event) {
	        			setVariant();
	        		});
	        		
		            $(this).slideDown();
	        		$("#variant_option_repeater button[data-repeater-delete]:hidden").removeClass("m--hide");
	        		
	        		$("#productAttributePosition"+index).val(index);
	        	} else {
	        		$(this).remove();
	        	}
	        	
	        	if( $("#variant_option_repeater div[data-repeater-item]:visible").length == 3) {
	        		$("#variant_option_repeater button[data-repeater-create]").addClass("m--hide");
	        	}
	
	        },
	        hide: function(deleteElement) {
	        	
	        	
	        	tagInputColor.push($(this).find('input[ data-role="tagsinput"]').attr("data-tag-color"));
	        	index=$(this).index();
	        	
	        	$('#product_form').formValidation('removeField',"productAttributeVos["+index+"].optionName");
        		$('#product_form').formValidation('removeField',"productAttributeVos["+index+"].optionValues");
        		
	            $(this).slideUp(function (){
	            	setVariant();
	            });
	            
	            
	            if( $("#variant_option_repeater div[data-repeater-item]:visible").length <= 3) {
	            	$("#variant_option_repeater button[data-repeater-create]").removeClass("m--hide");
	        	}

	            if( $("#variant_option_repeater div[data-repeater-item]:visible").length <= 2) {
	            	$("#variant_option_repeater button[data-repeater-delete]:visible").addClass("m--hide");
	        	}
	            //$(this).remove();
	            
	            //;
	        },
	        
	        ready: function (setIndexes) {
	        	$("#variant_option_repeater").find('input[ data-role="tagsinput"]').on('itemAdded', function(event) {
					//setVariant();
				});
	        	$("#variant_option_repeater").find('input[ data-role="tagsinput"]').on('itemRemoved', function(event) {
        			//setVariant();
        		});
	        },	
	        
	    });
		
		$("#variant_repeater").repeater({
			initEmpty: false,	
	        isFirstItemUndeletable: false,
			/* defaultValues: {
	            "text-input": "foo"
	        }, */
	        show: function() {
	        	$(this).slideDown();
	        	
	        	index = $(this).index();
	        	
	        	if(index == 0) {
	        		$("#purchasePrice"+index).val($("#salesPrice").val());
					/*$("#retailerPrice"+index).val($("#retailerPrice").val());
					$("#wholesalerPrice"+index).val($("#wholesalerPrice").val());
					$("#otherPrice"+index).val($("#otherPrice").val());*/
	        	}
	        	
	        	$("#position"+index).val(index);
	        	
	        	$('#attributeValue1'+index).val($('#optionName0').val());
				$('#attributeValue2'+index).val($('#optionName1').val());
				$('#attributeValue3'+index).val($('#optionName2').val());
				
				$('#product_form').formValidation('addField',"productVariantVos["+index+"].salesPrice", priceValidator);
				/*$('#product_form').formValidation('addField',"productVariantVos["+index+"].retailerPrice", priceValidator);
				$('#product_form').formValidation('addField',"productVariantVos["+index+"].wholesalerPrice", priceValidator);
				$('#product_form').formValidation('addField',"productVariantVos["+index+"].otherPrice", priceValidator);*/
				$('#product_form').formValidation('addField',"productVariantVos["+index+"].itemCode", itemCodeValidator);
				
	        },
	        hide: function(e) {
	        	$(this).slideUp()
	        },
	        ready: function (setIndexes) {
	        	
	        },
	    });
	});
	
	function setVariant() {
		
		var totalVariant=1;
		var variantOption=[];
		
		$("#variant_option_repeater div[data-repeater-item]:visible").find('input[ data-role="tagsinput"]').each(function() {
			
			if($(this).tagsinput('items').length!=0) {
				
				totalVariant = totalVariant * $(this).tagsinput('items').length;
				variantOption.push($(this).tagsinput('items'));
			}
			
		});
		
		$("#variant_repeater tr[data-repeater-item]:visible").each(function() {
			index=$(this).index();
			
			$('#product_form').formValidation('removeField',"productVariantVos["+index+"].salesPrice");
		/*	$('#product_form').formValidation('removeField',"productVariantVos["+index+"].retailerPrice");
			$('#product_form').formValidation('removeField',"productVariantVos["+index+"].wholesalerPrice");
			$('#product_form').formValidation('removeField',"productVariantVos["+index+"].otherPrice");*/
			$('#product_form').formValidation('removeField',"productVariantVos["+index+"].itemCode");
		});
		
		$("#variant_repeater").find('[data-repeater-list]').empty();
		
		for(i=0 ; i< totalVariant ;i++) {
			
			$("#variant_repeater button[data-repeater-create]").click();
		}
		
		var r=allPossibleCases(variantOption);
		var inputs =$("#variant_repeater").children('tbody').find($("tr"));
		
   		for (var j =0 ;j<inputs.length;  j++) {
   			
   			$(inputs[j]).find('td').eq(0).text(r[j]);
   			$('#variantName'+j).val(r[j]);
   			var x = r[j] ? r[j].split("/") : {};
   			
   			$('#attributeValue1'+j).val(x[0] ? x[0].trim() : "");
   			$('#attributeValue2'+j).val(x[1] ? x[1].trim() : "");
   			$('#attributeValue3'+j).val(x[2] ? x[2].trim() : "");
	   	}
   		 
   		if(r.length<1) {
			$('#variant_container').hide();
			$("#variant_option").addClass("col-lg-9 col-md-9");
			$("#variant_option").removeClass("col-lg-12 col-md-12");
		}
		else {
			$('#variant_container').show();
			$("#variant_option").removeClass("col-lg-9 col-md-9");
			$("#variant_option").addClass("col-lg-12 col-md-12");
		}
		
	}
	
	function allPossibleCases(arr) {
		if (arr.length === 0) {
			return [];
		} else if (arr.length ===1) {
			return arr[0];
		} else {
			var result = [];
			var allCasesOfRest = allPossibleCases(arr.slice(1));  // recur with the rest of array
			
			for (var c in allCasesOfRest) {
				for (var i = 0; i < arr[0].length; i++) {
					result.push(arr[0][i] + " / " + allCasesOfRest[c]);
				}
			}
			
			return result;
		}
	}
	
	function copyVariantDetails() {
		var $variantItem = $("#variant_repeater tr[data-repeater-item]:visible").eq(0);
		
		$("#variant_repeater tr[data-repeater-item]:visible").each(function() {
			
			$(this).find("input[id*='salesPrice']" ).val($variantItem.find("input[id*='salesPrice']" ).val())
			/*$(this).find("input[id*='retailerPrice']" ).val($variantItem.find("input[id*='retailerPrice']" ).val())
			$(this).find("input[id*='wholesalerPrice']" ).val($variantItem.find("input[id*='wholesalerPrice']" ).val())
			$(this).find("input[id*='otherPrice']" ).val($variantItem.find("input[id*='otherPrice']" ).val())*/
			
			$('#product_form').formValidation('revalidateField',$(this).find("input[id*='salesPrice']" ));
			/*$('#product_form').formValidation('revalidateField',$(this).find("input[id*='retailerPrice']" ));
			$('#product_form').formValidation('revalidateField',$(this).find("input[id*='wholesalerPrice']" ));
			$('#product_form').formValidation('revalidateField',$(this).find("input[id*='otherPrice']" ));*/
			
		});
	}
	
	$(document).ready(function() {

		/*
		*only in Product New click #variant_no button on load
		*/
		//$("#variant_yes").click();
		//
		
		/*
		*Product New always hide #variant_container DIV on page lode
		*Product Edit if product have varaint don't hide otherwise hide #variant_container DIV on page lode
		*/
		//$('#variant_container').hide();
		if(!$("#productId").val()) {
			$("#variant_no").click();
			$("#variant_option_repeater").find('[data-repeater-list]').empty();
			$("#variant_repeater").find('[data-repeater-list]').empty();
		}else {
			
			$("#variant_option_repeater").find("[data-repeater-create],[data-repeater-delete]").addClass("m--hide");
			
			$(".bootstrap-tagsinput").find("input").attr("readonly", "readonly")
			$(".bootstrap-tagsinput").find('.tag [data-role="remove"]').css("display", "none");
		}
	});
	