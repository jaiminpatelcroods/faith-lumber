/**
 * This File is For Contact New
 * Contact Validation
 */

var companyNameValidator = {
		validators : {
			notEmpty : {
				message: 'The Company Name is required. '
			},
			stringLength : {
				max : 100,
				message : 'The Company Name Not More than 100 characters long. '
			},
			regexp : {
				regexp : /^[a-zA-Z0-9_-\s-.,()& ]+$/,
				message : 'The Company Name can only consist of alphabetical, numberical value. '
			}
		}
	},
	addressValidator = {
		validators : {
			stringLength : {
				max : 100,
				message : 'The Address Line Not More than 100 characters long. '
			},
			regexp : {
				regexp : /^[a-zA-Z0-9_-\s-.,()&/ ]+$/,
				message : 'The Address Line can only consist of alphabetical, numberical value. '
			}
		}
	},
	countryValidator = {
		validators : {
			notEmpty : {
				message: 'The Country is required. '
			},
		}
	},
	stateValidator = {
		validators : {
			notEmpty : {
				message: 'The State is required. '
			},
		}
	},
	cityValidator = {
		validators : {
			notEmpty : {
				message: 'The City is required. '
			},
		}
	},
	pincodeValidator = {
		validators : {
			stringLength : {
				min : 6,
				max : 6,
				message : 'The Pincode must be 6 digit long. '
			},
			regexp : {
				regexp : /^\d{6}$/,
				message : 'The Pincode can only consist of numerical value. '
			}
		}
	},
	nameValidator = {
		validators: {
			stringLength : {
				max : 100,
				message : 'The Name Not More  than 100 characters long. '
			},
			regexp : {
				regexp : /^[a-zA-Z0-9_-\s-.,()& ]+$/,
				message : 'The Name can only consist of alphabetical, numberical value. '
			},
		}
	},
	mobilenoValidator = {
		validators : {
			stringLength : {
				max : 15,
				message : 'The Mobile no. must be 15 digit long. '
			},
			regexp : {
				regexp : /^[0-9+]+$/,
				message : 'The Mobile no. can only consist numerical value. '
			}
		}
	},
	emailValidator = {
		validators : {
			regexp : {
				regexp : /^[a-z|A-Z|]+([.|-]?[a-z|A-Z|0-9|_]+)*@[a-z|A-Z]+[a-z|A-Z|0-9]*[\\.]+[a-z|A-Z]+([.]?[a-z|A-Z]+)$/,
				message : 'The Email address is not valid. '
			},
		}
	};
			
$(document).ready(function(){
	$(".contact-select2").each(function() {
	    	
		var type = $(this).attr("data-type"),
		id = $(this).attr("id");
    	placeholder="Select...";
    	if($(this).attr("placeholder")) {
    		placeholder=$(this).attr("placeholder");
    	}
    	
    	allowClear=0;
    	if($(this).data('allow-clear')) {
    		allowClear=	!0;
    	}
    	$(this).select2({
    		placeholder:placeholder,
    		allowClear:allowClear,
    		escapeMarkup: function (markup) { return markup; },
			 language: {
	            noResults: function () {
	            	return '<div class="m-demo-icon"><div class="m-demo-icon__preview"><span class=""><i class="flaticon-plus m--font-primary"></i></span></div><div class="m-demo-icon__class"><a href="#" data-toggle="modal" class="m-link m--font-boldest add-contact-btn" data-type="'+type+'" data-response="'+id+'">Add Contact</a></div></div>';
	            }
	        }
    	});
    });

	
	$("#contact_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savecontact",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			companyName:{
				validators: {
					notEmpty: {
						message: 'The Company Name is required. '
					},
					stringLength : {
						max : 100,
						message : 'The Company Name Not More than 100 characters long. '
					},
					regexp : {
						regexp : /^[a-zA-Z0-9_-\s-.,()& ]+$/,
						message : 'The Company Name can only consist of alphabetical, numberical value. '
					},
				}
			}, 
			companyMobileno : {
				validators : {
					stringLength : {
						max : 15,
						message : 'The Mobile no. must be 15 digit long. '
					},
					regexp : {
						regexp : /^[0-9+]+$/,
						message : 'The Mobile no. can only consist numerical value. '
					}
				}
			},
			companyTelephone : {
				validators : {
					stringLength : {
						
						max : 17,
						message : 'The Telehphone No must be 17 digit long. '
					},
					regexp : {
						regexp : /^[0-9]+$/,
						message : 'The Telephone No consist only numeric digit. '
					}
				}
			},
			companyEmail : {
				verbose : false,
				validators : {
					regexp : {
						regexp : /^[a-z|A-Z|]+([.|-]?[a-z|A-Z|0-9|_]+)*@[a-z|A-Z]+[a-z|A-Z|0-9]*[\\.]+[a-z|A-Z]+([.]?[a-z|A-Z]+)$/,
						message : 'The Email address is not valid. '
					},
				}
			},
			ownerName:{
				validators: {
					stringLength : {
						max : 100,
						message : 'The Owner Not More  than 100 characters long. '
					},
					regexp : {
						regexp : /^[a-zA-Z0-9_-\s-.,()& ]+$/,
						message : 'The Owner Name can only consist of alphabetical, numberical value. '
					},
				}
			},
			ownerMobileno : {
				validators : {
					stringLength : {
						max : 15,
						message : 'The Mobile no. must be 15 digit long. '
					},
					regexp : {
						regexp : /^[0-9+]+$/,
						message : 'The Mobile no. can only consist numerical value. '
					}
				}
			},
			concernPersonName:{
				validators: {
					stringLength : {
						max : 100,
						message : 'The Concern Person Not More  than 100 characters long. '
					},
					regexp : {
						regexp : /^[a-zA-Z0-9_-\s-.,()& ]+$/,
						message : 'The Concern Person Name can only consist of alphabetical, numberical value. '
					},
				}
			},
			concernPersonMobileno : {
				validators : {
					stringLength : {
						max : 15,
						message : 'The Mobile no. must be 15 digit long. '
					},
					regexp : {
						regexp : /^[0-9+]+$/,
						message : 'The Mobile no. can only consist numerical value. '
					}
				}
			},
			gstType : {
				validators : {
					notEmpty : {
						message : 'The GST Type is required. '
					},

				}
			},
			gstin : {
				validators : {
					notEmpty : {
						message : 'GSTIN/UIN is required. '
					},
					stringLength : {
						max : 15,
						message : 'GSTIN/UIN is not more than 15 digit long. '
					},
					regexp : {

						regexp : /^\d{2}[A-Z]{5}\d{4}[A-Z]{1}\d{1}[A-Z]{1}[A-Z0-9]{1}$/,
						message : 'The GSTIN/UIN is not valid. '
					}
				}
			},
			panNo : {
				validators : {
					stringLength : {
						max : 10,
						message : 'The Pan No not more than 10 digit long. '
					},
					regexp : {

						regexp : /^[A-Z]{5}\d{4}[A-Z]{1}$/,
						message : 'The Pan No can only consist 5 capital alphabatic  value , 4 digit and 1 capital alphabate. '
					}
				}
			},
			noOfShops : {
				validators : {
					stringLength : {
						max : 4,
						message : 'The No. of Shops must be 4 digit long. '
					},
					regexp : {
						regexp : /^[0-9+]+$/,
						message : 'The No. of Shops can only consist numerical value. '
					}
				}
			},
			agentCommission : {
				validators : {
					stringLength : {
						max : 5,
						message : 'The Agent Commission must be 5 digit long. '
					},
					regexp : {
						regexp:/^((\d{0,3})((\.\d{0,2})?))$/,
						message: 'The Agent Commission can contain only numeric value and not more than two decimal value. '
					},
				}
			},
			workingTime : {
				validators : {
					stringLength : {
						max : 50,
						message : 'The Working Time not more than 50 characters long. '
					},
				}
			},
		}
	}).on('success.form.fv', function(e) {
		
		e.preventDefault();//stop the from action methods
		$("#savecontact").attr("disabled",true);
		$.fn.serializeObject = function() {
		    var o = {};
		    var a = this.serializeArray();
		    $.each(a, function() {
		        if (o[this.name] !== undefined) {
		            if (!o[this.name].push) {
		                o[this.name] = [o[this.name]];
		            }
		            o[this.name].push(this.value || '');
		        } else {
		            o[this.name] = this.value || '';
		        }
		    });
		    return o;
		};
		
		
		$.ajax({
			url: $('#contact_form').attr("action"),
		    type: "POST",
		    dataType: "json",
		    data: $.parseJSON(JSON.stringify($('#contact_form').serialize())),
		    success: function(data){
		    	 $('#'+$("#responseField").val())
	             .prepend($("<option></option>")
	                        .attr("value",data.contactId)
	                        .text(data.companyName)); 
		    	 $('#'+$("#responseField").val()).val(data.contactId).trigger("change");
		    	 
		    	 $("#contact_new_modal").modal("hide");
		    },
		    error: function(error){
		         console.log("Error:");
		         console.log(error);
		    }
		});
		
	});
	
	$('#contact_form').formValidation('addField',"contactAddressVos[0].addressLine1", addressValidator);
	$('#contact_form').formValidation('addField',"contactAddressVos[0].addressLine2", addressValidator);
	$('#contact_form').formValidation('addField',"contactAddressVos[0].countriesCode", countryValidator);
	$('#contact_form').formValidation('addField',"contactAddressVos[0].stateCode", stateValidator);
	$('#contact_form').formValidation('addField',"contactAddressVos[0].cityCode", cityValidator);
	$('#contact_form').formValidation('addField',"contactAddressVos[0].pinCode", pincodeValidator);
	
	$(document).on("click",".add-contact-btn",function() {
		$('#contact_form').formValidation('resetForm', true);
		var type = $(this).attr("data-type"),
		id = $(this).attr("data-response");
		
		$('#contact_form').attr("action","/contact/"+type+"/save/json");
		
		$("#responseField").val(id);
		$("#agent-commission-div").addClass("m--hide");
		$("#date-div").addClass("m--hide");
		$("#transport-div").addClass("m--hide");
		$("#total-shops-div").addClass("m--hide");
		
		if(type == 'customers') {
			$("#date-div").removeClass("m--hide");
			$("#total-shops-div").addClass("m--hide");
			$("#contact-modal-title").text("New Customer");
		} else if(type == 'suppliers') {
			$("#date-div").removeClass("m--hide");
			$("#contact-modal-title").text("New Supplier");
		} else if(type == 'transport') {
			$("#transport-div").removeClass("m--hide");
			$("#contact-modal-title").text("New Transport");
		} else if(type == 'agent') {
			$("#agent-commission-div").removeClass("m--hide");
			$("#date-div").removeClass("m--hide");
			$("#contact-modal-title").text("New Agent");
		}
		
		$("#companyName").val($(".select2-search--dropdown").find(".select2-search__field").val());
		
		$("#gstType").val("UnRegistered").trigger("change");
		$("#countriesCode0").val($("#countriesCode0").attr("data-default")).trigger("change");
		
		$("#serviceAvailable").tagsinput('removeAll');
		
		$("#dateOfBirth").val("");
		$("#anniversaryDate").val("");
		$("#savecontact").attr("disabled",false);
		$("#contact_new_modal").modal("show");
		$(".contact-select2").select2("close");
	});
	
	if($('#serviceAvailable').val() != undefined) {
		$('#serviceAvailable').tagsinput({
			tagClass:'labeldanger',
			confirmKeys: [13, 188]
		});
		
		$('.bootstrap-tagsinput').on("keypress", "input", function(event) {
			if (event.keyCode == 13) {
				event.preventDefault();
			}
		});
	}
	
	//$('#contact_form').formValidation('enableFieldValidators', 'agentCommission', false);
	// Agent 
	
	$("#gstType").change(function() {
		
		if($("#gstType").val()=="UnRegistered") {
			$('#contact_form').formValidation('resetField','gstin');
			$("#gstin").val();
			$("#gstin").prop('disabled', true);	
			$('#contact_form').formValidation('enableFieldValidators', 'gstin', false);
  		} else {
  			$('#contact_form').formValidation('resetField','gstin');
  			$("#gstin").prop('disabled', false);
  			$('#contact_form').formValidation('enableFieldValidators', 'gstin', true);
  		 }
	});
	
	$('#contact_new_modal').on('shown.bs.modal', function() {
		$("#savecontact").attr("disabled",false);
		$("#companyName").focus();
		$("#countriesCode0").select2({dropdownParent: $("#countriesCode0").parent(),placeholder:"Select Country"});
		$("#stateCode0").select2({dropdownParent: $("#stateCode0").parent(),placeholder:"Select State"});
		$("#cityCode0").select2({dropdownParent: $("#cityCode0").parent(),placeholder:"Select City"});
		$("#gstType").select2({dropdownParent: $("#gstType").parent(),placeholder:"Select GST Type"});
	});
	
	getAllCountryAjax("countriesCode0");
	
	$("#savecontact").click(function() {
		$('#contact_form').data('formValidation').validate();
	});
});