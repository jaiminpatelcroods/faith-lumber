/**
 * This File is For Category and Brand New Modal
 * Category and Brand Ajax Insert
 * Category and Brand Validation
 */

$(document).ready(function(){
	
	//----------Category------------
	$("#category_new_form").formValidation({
		framework : 'bootstrap',
		/*live:'disabled',*/
		excluded : ":disabled",
		button:{
			selector : "#savecategory",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			category:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/categorybrand/category/verify",
						type : 'POST'
					}
				}
			},
			description:{
				validators: {
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $("#savecategory").attr("disabled",true);
		 $.post("/categorybrand/category/save", {
			 name: $('#categoryNameModal').val(),
			 description:$('#categoryDescriptionModal').val()
		 }, function( data,status ) {
			 toastr["success"]("Record Inserted....");
			 $('#category_new_modal').modal('toggle');
			 location.reload(true);
		 });
		 
	});
	
	$('#category_new_modal').on('show.bs.modal', function() {
		$("#savecategory").attr("disabled",false);
		$('#category_new_form').formValidation('resetForm', true);
	});
	
	$("#savecategory").click(function() {
		$('#category_new_form').data('formValidation').validate();
	});
	
	//----------End Category------------
	
	//----------Brand------------
	$("#brand_new_form").formValidation({
		framework : 'bootstrap',
		/*live:'disabled',*/
		excluded : ":disabled",
		button:{
			selector : "#savebrand",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			brandname:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/categorybrand/brand/verify",
						type : 'POST'
					}
				}
			},
			branddesc:{
				validators: {
					
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $("#savebrand").attr("disabled",true);
		 $.post("categorybrand/brand/save", {
			 name: $('#brandNameModal').val(),
			 description:$('#brandDescriptionModal').val()
		 }, function( data,status ) {
			
			toastr["success"]("Record Inserted....");
			
			$('#brand_new_modal').modal('toggle');
			 
			location.reload(true);
			
		 });
		 
	});
	
	$('#brand_new_modal').on('show.bs.modal', function() {
		$("#savebrand").attr("disabled",false);
		$('#brand_new_form').formValidation('resetForm', true);
	});
	
	$("#savebrand").click(function() {
		$('#brand_new_form').data('formValidation').validate();
	});
	
	//----------End Brand------------
});