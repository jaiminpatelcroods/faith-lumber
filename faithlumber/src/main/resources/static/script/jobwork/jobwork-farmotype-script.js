/**
 * This File is For Jobwork FarmoType
 */
	var tableIndex = 0;
	
	$(document).ready(function(){
		
		$("#farmoTypeVo").on("change", function(){
			$.ajax({
			  	method: "GET",
			  	url:"/farmotype/json",
			  	data: { farmoTypeId: $('#farmoTypeVo').val() }
			}).done(function( data ) {
				
				$("#frontLength").val(data.frontLength)
				$("#frontSeat").val(data.frontSeat)
				$("#frontThai").val(data.frontThai)
				$("#frontLangot").val(data.frontLangot)
				$("#frontKnee").val(data.frontKnee)
				$("#frontMoriMunda").val(data.frontMoriMunda)
				$("#frontJati").val(data.frontJati)
				
				$("#backLength").val(data.backLength);
				$("#backSeat").val(data.backSeat);
				$("#backThai").val(data.backThai);
				$("#backLangot").val(data.backLangot);
				$("#backKnee").val(data.backKnee);
				$("#backMoriMunda").val(data.backMoriMunda);
				$("#backJati").val(data.backJati);
				$("#particularDiv").html('');
				$.each(data.farmoTypeParticularVos, function(key,value){
					var id = "#particularTempletDiv"
					$(id).find("[data-particular-label]").html(value.farmoParticularVo.name+" :").end()
						 .find("[data-particular-input-text]").attr('value',value.farmoParticularMeasurement).end()
					
					$("#particularDiv").html($("#particularDiv").html()+$("#particularTempletDiv").html());
					
				});
				
				
			});
			
		});
		
		//End Document.ready
	});
	
	
	function addTableItem() {
		
		var	$tableItemTemplate = $("#product_table").find("[data-table-item='template']").clone();
		
		$tableItemTemplate.removeClass("m--hide").attr("data-table-item",tableIndex);
		
		$tableItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
			n=$(this).attr("id");
			n ? $(this).attr("id",n.replace(/{index}/g,tableIndex)) : "";
			n=$(this).attr("name");
			n ? $(this).attr("name",n.replace(/{index}/g,tableIndex)) : "";
			
			$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,tableIndex)) : "";
		});
		
		$("#product_table").find("[data-table-list]").append($tableItemTemplate);
		
		$("#particular"+tableIndex).addClass("m-select2");
		$("#particular"+tableIndex).select2({placeholder:"Select Particular",allowClear:0});
		
		$('#farmotype_form').formValidation('addField',"farmoTypeParticularVos["+tableIndex+"].farmoParticularVo.farmoParticularId", particularValidator);
		$('#farmotype_form').formValidation('addField',"farmoTypeParticularVos["+tableIndex+"].farmoParticularMeasurement", measurement);
		
		tableIndex++;
		
		setdTableSrNo();
	}
	
	function setdTableSrNo() {
		var $tableItemTemplate=$("#product_table").find("[data-table-item]").not(".m--hide");
		var i = 0;
		$tableItemTemplate.each(function (){
			$(this).find("[data-item-index]").html(++i);
		});
	}
	
	function setSrNo() {
		var $purchaseItem=$("#product_table").find("[data-table-item]").not(".m--hide"), subTotal=0.0;
		var i = 0;
		$purchaseItem.each(function (){
			$(this).find("[data-item-index]").html(++i);
		});
	}
	
	function setBackFields() {
		$("#backLength").val($("#frontLength").val());
		$("#backSeat").val($("#frontSeat").val());
		$("#backThai").val($("#frontThai").val());
		$("#backLangot").val($("#frontLangot").val());
		$("#backKnee").val($("#frontKnee").val());
		$("#backMoriMunda").val($("#frontMoriMunda").val());
		$("#backJati").val($("#frontJati").val());
		
		$('#farmotype_form').formValidation('revalidateField','backLength');
		$('#farmotype_form').formValidation('revalidateField','backSeat');
		$('#farmotype_form').formValidation('revalidateField','backThai');
		$('#farmotype_form').formValidation('revalidateField','backLangot');
		$('#farmotype_form').formValidation('revalidateField','backKnee');
		$('#farmotype_form').formValidation('revalidateField','backMoriMunda');
		$('#farmotype_form').formValidation('revalidateField','backJati');
		
		
	}
	function setTableIndex(id){
		
		tableIndex=id;
	}