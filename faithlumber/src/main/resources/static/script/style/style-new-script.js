/**
 * This File is For Style New Modal
 * Style Ajax Insert
 * Style Validation
 */

$(document).ready(function(){
	$("#style_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savestyle",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			styleName:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/style/style/verify",
						type : 'POST'
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 $("#savestyle").attr("disabled", true);
		 $.post("/style/save", {
			 styleName: $('#styleName').val()
		 }, function( data,status ) {
			 
			toastr["success"]("Record Inserted....");
			 
			$('#style_new_modal').modal('toggle');
			
			location.reload(true);
			
		 });
		 
	});
	
	$('#style_new_modal').on('show.bs.modal', function() {
		$("#savestyle").attr("disabled", false);
		$('#style_new_form').formValidation('resetForm', true);
	});
	
	$("#savestyle").click(function() {
		$('#style_new_form').data('formValidation').validate();
	});
	
});