/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#tax_update_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#updatetax",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			tax:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/tax/tax/verify",
						type : 'POST',
						data: function(validator, $field, value) {
	                            return {
	                            	taxId : $('#uptaxid').val()
	                            };
	                        },
					} 
				}
			},
			taxuprate:{
				validators: {
					notEmpty: {
						message: 'The Rate is required. '
					},
					regexp: {
						regexp:/^((\d{0,3})((\.\d{0,2})?))$/,
						message: 'The Tax Rate can contain only numeric value and not more than two decimal value. '
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#updatetax").attr("disabled", true);
		 
		 $.post("/tax/update", {
			 name: $('#uptaxname').val(),
			 rate:$('#uptaxrate').val(),
			 id:$('#uptaxid').val()
		 }, function( data,status ) {
			toastr["success"]("Record Updated....");
			$('#tax_update_modal').modal('toggle');
			
			location.reload(true);
			
		 });
	});
	
	$("#updatetax").click(function() {
		$('#tax_update_form').data('formValidation').validate();
	});
	
});

function updatetax(row,id)
{  		//console.log(row);
	$("#updatetax").attr("disabled", false);
	$('#tax_update_form').formValidation('resetForm', true);
	var crow=$(row).closest('tr');		
	var name=$(crow).find('td:eq(1)').text();
	var rate = $(crow).find('td:eq(2)').text();
	var rowindex = $(crow).find('td:eq(0)').text();

	$('#uptaxname').val(name);
	$('#uptaxrate').val(rate);
	$('#uptaxid').val(id);
	$("#dataindex").val(rowindex);
	
}