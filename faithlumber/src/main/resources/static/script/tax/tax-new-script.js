/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	$("#tax_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savetaxnew",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			tax:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/tax/tax/verify",
						type : 'POST'
					}
				}
			},
			taxrate:{
				validators: {
					notEmpty: {
						message: 'The Rate is required. '
					},
					regexp: {
						regexp:/^((\d{0,3})((\.\d{0,2})?))$/,
						message: 'The Tax Rate can contain only numeric value and not more than two decimal value. '
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#savetaxnew").attr("disabled", true);
		 
		 $.post("/tax/create", {
			 name: $('#taxNameModal').val(),
			 rate:$('#taxRateModal').val()
		 }, function( data,status ) {
			toastr["success"]("Record Inserted....");
			$('#tax_new_modal').modal('toggle');
			
			location.reload(true);
			
		 });
		 
		 /*$('#tax_new_form').formValidation('resetForm', true);
		 $('#tax').val("");
		 $('#rate').val("");*/
	});
	
	$('#tax_new_modal').on('show.bs.modal', function() {
		
		$("#savetaxnew").attr("disabled", false);
		
		$('#tax_new_form').formValidation('resetForm', true);
	});
	
	$("#savetaxnew").click(function() {
		$('#tax_new_form').data('formValidation').validate();
	});
	
});