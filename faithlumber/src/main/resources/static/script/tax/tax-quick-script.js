/**
 * This File is For Tax New Modal
 * Tax Ajax Insert
 * Tax Validation
 */

$(document).ready(function(){
	
	$(".tax-select2").each(function() {
    	
    	placeholder="Select...";
    	if($(this).attr("placeholder")) {
    		placeholder=$(this).attr("placeholder");
    	}
    	
    	allowClear=0;
    	if($(this).data('allow-clear')) {
    		allowClear=	!0;
    	}
    	$(this).select2({
    		placeholder:placeholder,
    		allowClear:allowClear,
    		escapeMarkup: function (markup) { return markup; },
			 language: {
	            noResults: function () {
	            	return '<div class="m-demo-icon"><div class="m-demo-icon__preview"><span class=""><i class="flaticon-plus m--font-primary"></i></span></div><div class="m-demo-icon__class"><a href="#" data-toggle="modal" class="m-link m--font-boldest add-tax-btn">Add Tax</a></div></div>';
	            }
	        }
    	});
    });
	
	$(document).on("click",".add-tax-btn",function() {
		$('#tax_new_form').formValidation('resetForm', true);
		$("#tax_new_modal").modal("show")
		$("#taxNameModal").val($(".select2-search__field").val());
		
		$(".tax-select2").select2("close");
	});
	
	$("#tax_new_form").formValidation({
		framework : 'bootstrap',
		live:'disabled',
		excluded : ":disabled",
		button:{
			selector : "#savetaxnew",
			disabled : "disabled",
		},
		icon : null,
		fields : {
			tax:{
				validators: {
					notEmpty: {
						message: 'The Name is required. '
					},
					remote : {
						message : 'This Name is already exist. ',
						url : "/tax/tax/verify",
						type : 'POST'
					}
				}
			},
			taxrate:{
				validators: {
					notEmpty: {
						message: 'The Rate is required. '
					},
					regexp: {
						regexp:/^((\d{0,3})((\.\d{0,2})?))$/,
						message: 'The Tax Rate can contain only numeric value and not more than two decimal value. '
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		 e.preventDefault();//stop the from action methods
		 
		 $("#savetaxnew").attr("disabled", true);
		 
		 $.post("/tax/create", {
			 name: $('#taxNameModal').val(),
			 rate:$('#taxRateModal').val()
		 }, function( data,status ) {
			 toastr["success"]("Tax Inserted....");
			 $('#tax_new_modal').modal('toggle');
			 $(".tax-select2").prepend($('<option>',{value :data.taxId, text :data.taxName}));
			 $(".tax-select2").val(data.taxId).trigger("change");
			
		 });
		 
	});
	
	$('#tax_new_modal').on('shown.bs.modal', function() {
		
		$("#savetaxnew").attr("disabled", false);
		$('#taxNameModal').focus()
	});
	
	$("#savetaxnew").click(function() {
		$('#tax_new_form').data('formValidation').validate();
	});
	
});