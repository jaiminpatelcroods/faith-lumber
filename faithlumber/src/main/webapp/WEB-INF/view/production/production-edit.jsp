<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Edit | ${productionVo.prefix}${productionVo.productionNo} | Production</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		select[readonly].select2-hidden-accessible + .select2-container {
		  pointer-events: none;
		  touch-action: none;
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		input::-webkit-outer-spin-button,
		input::-webkit-inner-spin-button {
		    /* display: none; <- Crashes Chrome on hover */
		    -webkit-appearance: none;
		    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
		}
		
		input[type=number] {
		    -moz-appearance:textfield; /* Firefox */
		}
		/* table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		} */
		
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${productionVo.prefix}${productionVo.productionNo}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/production" class="m-nav__link">
										<span class="m-nav__link-text">Production</span>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/production/${productionVo.productionId}" class="m-nav__link">
										<span class="m-nav__link-text">${productionVo.prefix}${productionVo.productionNo}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="production_form" action="/production/save" method="post">	
						<input type="hidden" name="prefix" id="prefix" value="${productionVo.prefix}"/>
						<input type="hidden" name="productionNo" id="productionNo" value="${productionVo.productionNo}"/>
						<input type="hidden" name="productionId" id="productionId" value="${productionVo.productionId}"/>
						
						<input type="hidden" name="total" id="total" value="0"/>
						
						<c:if test="${not empty productionVo.salesVo.salesId}">
							<input type="hidden" id="parentId" name="salesVo.salesId" value="${productionVo.salesVo.salesId}"/>
						</c:if>
						<input type="hidden" id="updateStatus" name="updateStatus" value="In Progress"/>
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">Production</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">	
												
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Against SO :</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<span>
															${productionVo.salesVo.prefix}${productionVo.salesVo.salesNo}
														</span>
													</div>
												</div>
												
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Production Date:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input todaybtn-datepicker" id="productionDate"  name="productionDate" readonly placeholder="Production Date" data-date-format="dd/mm/yyyy"
																data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'
																value='<fmt:formatDate pattern="dd/MM/yyyy" value="${productionVo.productionDate}"/>'/>
															<div class="input-group-append">
																<span class="input-group-text"><i class="la la-calendar"></i></span>
															</div>
														</div>
													</div>
												</div>
												
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Production No.:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<span>
															${productionVo.prefix}${productionVo.productionNo}
														</span>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Notes:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<textarea class="form-control m-input" placeholder="Notes" name="note">${productionVo.note}</textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="table-responsive m--margin-top-20">
													<table class="table m-table table-bordered table-hover" id="product_table">
														<thead>
															<tr>
																<th>#</th>
																<th>Product</th>
																<th class="text-right">SO Qty</th>
																<th class="text-right">Total Produced Qty</th>
																<th class="text-right">Edit Production Qty</th>
																<th class="text-right">Remains Qty</th>
																<th></th>
															</tr>
														</thead>
													    <tbody data-table-list="">
													    	<c:set scope="page" var="index" value="0"/>
													    	<c:forEach items="${productionVo.productionItemVos}" var="productionItemVo" varStatus="status">
													    		<tr data-table-item="${index}" class="">
														    		<td class="" style="width: 50px;">
														    			<span data-item-index>${index+1}</span>
														    		</td>
															        <td class="p-0" style="width: 370px;">
															        	<div class="form-group m-form__group p-0">
																		<select class="form-control m-input form-control-sm m-input1 m-select2" name="productionItemVos[${index}].productVariantVo.productVariantId" id="productVariantId${index}" onchange="getProductVariantInfo(${index})" placeholder="Select Product" readonly="readonly">
																			<option value="${productionItemVo.productVariantVo.productVariantId}" selected="selected">${productionItemVo.productVariantVo.productVo.name} ${productionItemVo.productVariantVo.variantName}</option>
																		</select>
																		</div>
															        </td>
															        <td class="p-0" style="width: 270px;">
															        	<div class="form-group m-form__group p-0">
															        		<div class="m-input-icon m-input-icon--right">
																				<input type="text" class="form-control form-control m-input1 text-right" id="soQty${index}" name="productionItemVos[${index}].soQty" placeholder="Qty" value="${productionItemVo.soTotalQty}" readonly="readonly">
																				<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																			</div>
															        	</div>
															        </td>
															        <td class="p-0" style="width: 270px;">
															        	<div class="form-group m-form__group p-0">
															        		<div class="m-input-icon m-input-icon--right">
																				<input type="text" class="form-control form-control m-input1 text-right" id="producedQty${index}" name="productionItemVos[${index}].producedQty" placeholder="Qty" value="${productionItemVo.producedTotalQty-productionItemVo.qty}" readonly="readonly">
																				<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																			</div>
															        	</div>
															        </td>
																        
															        <td class="p-0" style="width: 270px;">
															        	<div class="form-group m-form__group p-0">
															        		<div class="m-input-icon m-input-icon--right">
																				<%-- Jaimin <input type="text" class="form-control form-control m-input1 text-right" id="qty${index}" name="productionItemVos[${index}].qty" placeholder="Qty" value="${productionItemVo.qty}"/> --%>
																				<input type="number" class="form-control form-control m-input1 text-right" id="qty${index}" name="productionItemVos[${index}].qty" placeholder="Qty" value="${productionItemVo.qty}" onchange="setProductionQty(${index})" step="Any" max="${productionItemVo.soTotalQty}" required="required" min="0"/>
																				
																				<span class="m-input-icon__icon m-input-icon__icon--right">
																					<span>
																						<i class="" data-item-uom>
																							${productionItemVo.productVariantVo.productVo.unitOfMeasurementVo.measurementCode}
																						</i>
																					</span>
																				</span>
																			</div>
															        	</div>
															        </td>
															        
															        <td class="p-0" style="width: 270px;">
															        	<div class="form-group m-form__group p-0">
															        		<div class="m-input-icon m-input-icon--right">
																				<%-- Jaimin <input type="text" class="form-control form-control m-input1 text-right" id="remainsQty${index}" placeholder="Qty" readonly="readonly" value="${productionItemVo.soTotalQty - productionItemVo.producedTotalQty}" data-item-remains> --%>
																				 <input type="text" class="form-control form-control m-input1 text-right" id="remainsQty${index}" placeholder="Qty" readonly="readonly" value="${(productionItemVo.soTotalQty - productionItemVo.producedTotalQty)}" data-item-remains>
																				<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																			</div>
															        	</div>
															        </td>
															        <td class="p-0" style="width: 40px;">
															        	<!-- <button type="button" data-item-remove="" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill" title="Delete"> <i class="fa fa-times"></i></button> -->
															        	<input type="hidden" name="productionItemVos[${index}].productionItemId" id="productionItemId${index}" value="${productionItemVo.productionItemId}">
															        	<input type="hidden" id="oldProductionQty${index}"  name="productionItemVos[${index}].oldProductionQty" value="${productionItemVo.qty}"/>
															        	<%-- <input type="hidden" id="soTotalQty${index}"  name="productionItemVos[${index}].soQty" value="${productionItemVo.soTotalQty}"/>
															        	<input type="hidden" id="producedTotalQty${ind}"  name="productionItemVos[${index}].producedQty" value="${productionItemVo.producedTotalQty}"/> --%>
															        	<input type="hidden" id="salesItemId${index}"  name="productionItemVos[${index}].salesItemId" value="${productionItemVo.salesItemId}"/>
															        </td>
														    	</tr>
														    	<c:set scope="page" var="index" value="${index+1}"/>
													    	</c:forEach>
													    	<tr data-table-item="template" class="m--hide">
													    		<td class="" style="width: 50px;">
													    			<span data-item-index></span>
													    		</td>
														        <td class="p-0" style="width: 370px;">
														        	<div class="form-group m-form__group p-0">
																	<select class="form-control m-input form-control-sm m-input1" name="productionItemVos[{index}].productVariantVo.productVariantId" id="productVariantId{index}" onchange="getProductVariantInfo({index})" placeholder="Select Product">
																		<option value="">Select Product</option>
																		<c:forEach items="${productVariantVos}" var="productVariantVo">
																			<option value="${productVariantVo.productVariantId}">${productVariantVo.productVo.categoryVo.categoryName} ${productVariantVo.productVo.name} ${productVariantVo.variantName}</option>
																		</c:forEach>
																	</select>
																	</div>
														        </td>
														        <td class="p-0" style="width: 270px;">
														        	<div class="form-group m-form__group p-0">
														        		<div class="m-input-icon m-input-icon--right">
																			<input type="text" class="form-control form-control m-input1 text-right" id="qty{index}" name="productionItemVos[{index}].qty" placeholder="Qty" value="">
																			<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																		</div>
														        	</div>
														        </td>
														        <td class="p-0" style="width: 40px;">
														        	<button type="button" data-item-remove="" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill" title="Delete"> <i class="fa fa-times"></i></button>
														        </td>
													    	</tr>
														</tbody>
														<tfoot>
													      <tr>
													        <th></th>
													        <th>
													        	<!-- Jaimin <div class="m-demo-icon mb-0">
																	<div class="m-demo-icon__preview">
																		<span class=""><i class="flaticon-plus m--font-primary"></i></span>
																	</div>
																	<div class="m-demo-icon__class">
																	<a href="JavaScript:void(0)" data-toggel="modal" class="m-link m--font-boldest" onclick="addTableItem()"> Add More</a>
																	</div>
																</div> -->
													        </th>
													        <th><span class="m--font-boldest float-right" id=""></span></th>
													        <th><span class="m--font-boldest float-right" id=""></span></th>
													        <th><span class="m--font-boldest float-right" id=""></span></th>
													        <th><span class="m--font-boldest float-right" id=""></span></th>
													        <th></th>
													      </tr>
													    </tfoot>
												  	</table>
												</div>
											</div>
										</div>
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions m-form__actions--solid m-form__actions--right">
											<button type="submit" class="btn btn-brand" id="saveproduction">
												Submit
											</button>
											
											<a href="/production" class="btn btn-secondary">
												Cancel
											</a>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
		
	<script src="<%=request.getContextPath()%>/script/production/production-script.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			
			setTableIndex(${index});
			
			var $tableItemTemplate=$("#product_table").find("[data-table-item]").not(".m--hide");
			var i = 0;
			$tableItemTemplate.each(function () {
				
				i = $(this).attr("data-table-item");
				$('#production_form').formValidation('addField',"productionItemVos["+i+"].productVariantVo.productVariantId");
				$('#production_form').formValidation('addField',"productionItemVos["+i+"].qty",qtyValidator);
				
				setProductRemote(i);
			});
		});
	</script>
	
</body>
<!-- end::Body -->
</html>