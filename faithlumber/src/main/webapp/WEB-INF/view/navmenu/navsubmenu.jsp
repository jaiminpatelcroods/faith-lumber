<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Color</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Nav Menu</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
						
					<div class="row">
					
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
												
									</div>
								</div>
								<div class="m-portlet__body" >
									
									Insert Sub Menu
									<form action="navsubmenu/save" method="post">
										<table border="2">
											<thead>
												<tr>
													<th>Menu</th>
													<th>Title</th>
													<th>Menu Url</th>
													<th>Ordering</th>
													<th>Action</th>
													<th>Type</th>
												</tr>
											</thead>
											<tr>
												<td>
													<select name="menuId0">
														<option value="khali">Select</option>
														<c:forEach items="${navMenuVos}" var="navMenuVo">
															<option value="${navMenuVo.navMenuId}">${navMenuVo.title}</option>
														</c:forEach>
													</select>
												</td>
												<td>
													<input type="text" name="title0"/>
												</td>
												<td>
													<input type="text" name="menuURL0"/>
												</td>
												<td>
													<input type="text" name="ordering0"/>
												</td>
												<td>
													<input type="text" name="action0"/>
												</td>
												<td>
													<input type="text" name="type0"/>
												</td>
											</tr>
											<tr>
												<td>
													<select name="menuId1">
														<option value="khali">Select</option>
														<c:forEach items="${navMenuVos}" var="navMenuVo">
															<option value="${navMenuVo.navMenuId}">${navMenuVo.title}</option>
														</c:forEach>
													</select>
												</td>
												<td>
													<input type="text" name="title1"/>
												</td>
												<td>
													<input type="text" name="menuURL1"/>
												</td>
												<td>
													<input type="text" name="ordering1"/>
												</td>
												<td>
													<input type="text" name="action1"/>
												</td>
												<td>
													<input type="text" name="type1"/>
												</td>
											</tr>
											<tr>
												<td>
													<select name="menuId2">
														<option value="khali">Select</option>
														<c:forEach items="${navMenuVos}" var="navMenuVo">
															<option value="${navMenuVo.navMenuId}">${navMenuVo.title}</option>
														</c:forEach>
													</select>
												</td>
												<td>
													<input type="text" name="title2"/>
												</td>
												<td>
													<input type="text" name="menuURL2"/>
												</td>
												<td>
													<input type="text" name="ordering2"/>
												</td>
												<td>
													<input type="text" name="action2"/>
												</td>
												<td>
													<input type="text" name="type2"/>
												</td>
											</tr>
											<tr>
												<td>
													<select name="menuId3">
														<option value="khali">Select</option>
														<c:forEach items="${navMenuVos}" var="navMenuVo">
															<option value="${navMenuVo.navMenuId}">${navMenuVo.title}</option>
														</c:forEach>
													</select>
												</td>
												<td>
													<input type="text" name="title3"/>
												</td>
												<td>
													<input type="text" name="menuURL3"/>
												</td>
												<td>
													<input type="text" name="ordering3"/>
												</td>
												<td>
													<input type="text" name="action3"/>
												</td>
												<td>
													<input type="text" name="type3"/>
												</td>
											</tr>
											<tr>
												<td>
													<select name="menuId4">
														<option value="khali">Select</option>
														<c:forEach items="${navMenuVos}" var="navMenuVo">
															<option value="${navMenuVo.navMenuId}">${navMenuVo.title}</option>
														</c:forEach>
													</select>
												</td>
												<td>
													<input type="text" name="title4"/>
												</td>
												<td>
													<input type="text" name="menuURL4"/>
												</td>
												<td>
													<input type="text" name="ordering4"/>
												</td>
												<td>
													<input type="text" name="action4"/>
												</td>
												<td>
													<input type="text" name="type4"/>
												</td>
											</tr>
											<tr>
												<td>
													<select name="menuId5">
														<option value="khali">Select</option>
														<c:forEach items="${navMenuVos}" var="navMenuVo">
															<option value="${navMenuVo.navMenuId}">${navMenuVo.title}</option>
														</c:forEach>
													</select>
												</td>
												<td>
													<input type="text" name="title5"/>
												</td>
												<td>
													<input type="text" name="menuURL5"/>
												</td>
												<td>
													<input type="text" name="ordering5"/>
												</td>
												<td>
													<input type="text" name="action5"/>
												</td>
												<td>
													<input type="text" name="type5"/>
												</td>
											</tr>
											<tr>
												<td>
													<select name="menuId6">
														<option value="khali">Select</option>
														<c:forEach items="${navMenuVos}" var="navMenuVo">
															<option value="${navMenuVo.navMenuId}">${navMenuVo.title}</option>
														</c:forEach>
													</select>
												</td>
												<td>
													<input type="text" name="title6"/>
												</td>
												<td>
													<input type="text" name="menuURL6"/>
												</td>
												<td>
													<input type="text" name="ordering6"/>
												</td>
												<td>
													<input type="text" name="action6"/>
												</td>
												<td>
													<input type="text" name="type6"/>
												</td>
											</tr>
											<tr>
												<td>
													<select name="menuId7">
														<option value="khali">Select</option>
														<c:forEach items="${navMenuVos}" var="navMenuVo">
															
															<option value="${navMenuVo.navMenuId}">${navMenuVo.title}</option>
														</c:forEach>
													</select>
												</td>
												<td>
													<input type="text" name="title7"/>
												</td>
												<td>
													<input type="text" name="menuURL7"/>
												</td>
												<td>
													<input type="text" name="ordering7"/>
												</td>
												<td>
													<input type="text" name="action7"/>
												</td>
												<td>
													<input type="text" name="type7"/>
												</td>
											</tr>
											<tr>
												<td>
													<select name="menuId8">
														<option value="khali">Select</option>
														<c:forEach items="${navMenuVos}" var="navMenuVo">
															
															<option value="${navMenuVo.navMenuId}">${navMenuVo.title}</option>
														</c:forEach>
													</select>
												</td>
												<td>
													<input type="text" name="title8"/>
												</td>
												<td>
													<input type="text" name="menuURL8"/>
												</td>
												<td>
													<input type="text" name="ordering8"/>
												</td>
												<td>
													<input type="text" name="action8"/>
												</td>
												<td>
													<input type="text" name="type8"/>
												</td>
											</tr>
											<tr>
												<td>
													<select name="menuId9">
														<option value="khali">Select</option>
														<c:forEach items="${navMenuVos}" var="navMenuVo">
															
															<option value="${navMenuVo.navMenuId}">${navMenuVo.title}</option>
														</c:forEach>
													</select>
												</td>
												<td>
													<input type="text" name="title9"/>
												</td>
												<td>
													<input type="text" name="menuURL9"/>
												</td>
												<td>
													<input type="text" name="ordering9"/>
												</td>
												<td>
													<input type="text" name="action9"/>
												</td>
												<td>
													<input type="text" name="type9"/>
												</td>
											</tr>
											<tr>
												<td>
													
												</td>
												<td>
													<button type="submit">Submit</button>
												</td>
											</tr>
										</table>
										
									</form>
									
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
				
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
		
	<script src="<%=request.getContextPath()%>/script/color/color-new-script.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/script/color/color-update-script.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	
	var DatatablesDataSourceHtml = {
	    init: function() {
	        $("#color_table").DataTable({
	            responsive: !0,
	            
	        })  
	    }
	};
	
	$(document).ready(function() {
		DatatablesDataSourceHtml.init();	
	});
	
	function deleteColor(rows,id)
    {  		
	 	
		swal({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!"
        }).then(function(e) {
        	
        	if(e.value) {
        		$.post("/color/delete", {
   		         id:id
   		        }, function( data,status ) {
   		        	location.reload();
   		        	t.on( 'order.dt search.dt', function () {
   					        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
   					            cell.innerHTML = i+1;
   					        } );
   					}).draw();
   		        	 
   					toastr["success"]("Record Deleted....");
   				
   				
   				});            		
        	}
        	
        });
		
    }
	</script>	
</body>
<!-- end::Body -->
</html>