<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
	
	<%@include file="../../header/head.jsp" %>
	
	<title>${journalVoucherVo.prefix}${journalVoucherVo.voucherNo} | Journal Voucher</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${journalVoucherVo.prefix}${journalVoucherVo.voucherNo}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/account/journalvoucher" class="m-nav__link">
										<span class="m-nav__link-text">Journal Voucher</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-12"></div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<span class="m-portlet__head-icon">
												<i class="flaticon-cogwheel-2"></i>
											</span>
											<h3 class="m-portlet__head-text m--font-brand">Journal Voucher</h3>
										</div>
									</div>
									<div class="m-portlet__head-tools">
										<ul class="m-portlet__nav">
											<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
												<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl  m-dropdown__toggle">
													<!-- <i class="la la-ellipsis-v"></i> -->
													<i class="la la-ellipsis-h m--font-brand"></i>
												</a>
						                        <div class="m-dropdown__wrapper">
						                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
						                            <div class="m-dropdown__inner">
					                                    <div class="m-dropdown__body">              
					                                        <div class="m-dropdown__content">
					                                            <ul class="m-nav">
																	<li class="m-nav__item">
					                                                    <a href="/account/journalvoucher/${journalVoucherVo.journalVoucherId}/pdf" target="_blank" class="m-nav__link">
					                                                        <i class="m-nav__link-icon fa fa-file-pdf"></i>
					                                                        <span class="m-nav__link-text">PDF</span>
					                                                    </a>
					                                                </li>
					                                                <li class="m-nav__separator m-nav__separator--fit"></li>
					                                                <li class="m-nav__item">
					                                                    <a href="/account/journalvoucher/${journalVoucherVo.journalVoucherId}/edit" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																			<span><i class="flaticon-edit"></i><span>Edit</span></span>
																		</a>
					                                                    <button data-url="/account/journalvoucher/${journalVoucherVo.journalVoucherId}/delete" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right delete-btn">
																			<span><i class="flaticon-delete-1"></i><span>Delete</span></span>
																		</button>
					                                                </li>
					                                            </ul>
					                                        </div>
					                                    </div>
						                            </div>
						                        </div>
											</li>
										</ul>
									</div>
								</div>
								<div class="m-portlet__body" >
									<div class="m-widget28">
										<div class="m-widget28__container" >	
											<!-- Start:: Content -->
											<div class="m-widget28__tab tab-content">
												<div class="m-widget28__tab-container tab-pane active">
												    <div class="m-widget28__tab-items">
														<div class="m-widget28__tab-item">
															<span class="m--regular-font-size-">Voucher No:</span>
															<span>${journalVoucherVo.prefix} ${journalVoucherVo.voucherNo}</span>
														</div>
														<div class="m-widget28__tab-item">
															<span class="m--regular-font-size-">Transaction Date:</span>
															<span><fmt:formatDate pattern="dd/MM/yyyy" value="${journalVoucherVo.voucherDate}"/></span>
														</div>
														<div class="m-widget28__tab-item">
															<span class="m--regular-font-size-">Description:</span>
															<span><c:if test="${empty journalVoucherVo.description}">N/A</c:if>${journalVoucherVo.description}</span>
														</div>
													</div>				      	 		      	
												</div>
											</div>
											<!-- end:: Content --> 	
										</div>				 	 
									</div>
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12">																					
											<div class="table-responsive m--margin-top-20">
												<table class="table m-table table-bordered table-hover" id="journalvoucher_table">
													<thead>
														<tr>
															<th>#</th>
															<th>From Account</th>
															<th>Debit</th>
															<th>Credit</th>
															<th></th>
														</tr>
												    </thead>
												    <tbody data-table-list="">
												    	<c:forEach items="${journalVoucherVo.journalVoucherAccountVos}" var="journalVoucherAccountVo" varStatus="status">
													    	<tr data-table-item="${status.index}" class="">
													    		<td class="" style="width: 50px;">
													    			<span data-item-index>${status.index+1}</span>
													    		</td>
														        <td class="" style="width: 280px;">
														        	${journalVoucherAccountVo.fromAccountCustomVo.accountName}
														        </td>
														        <td class="text-right" style="width: 180px;">
														        	${journalVoucherAccountVo.debit}
														        </td>
														        <td class="text-right" style="width: 180px;">
														        	${journalVoucherAccountVo.credit}
														        </td>
														        <td class="p-0" style="width: 40px;">
														        </td>
													    	</tr>
													    </c:forEach>
													</tbody>
													<tfoot>
												      <tr>
												        <th></th>
												        <th>
												        </th>
												        <th><span class="m--font-boldest float-right" id="debit_total">${journalVoucherVo.total}</span></th>
												        <th><span class="m--font-boldest float-right" id="credit_total">${journalVoucherVo.total}</span></th>
												        <th></th>
												      </tr>
												    </tfoot>
											  	</table>
											</div>
										</div>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script type="text/javascript">
		$(document).ready(function() {
			$(".delete-btn").click(function(e) {
	            
	            var u = $(this).data("url") ? $(this).data("url") : '';
	    		swal({
	                title: "Are you sure?",
	                text: "You won't be able to revert this!",
	                type: "warning",
	                showCancelButton: !0,
	                confirmButtonText: "Yes, delete it!"
	            }).then(function(e) {
	            	e.value && u != ''? window.location.href=u : console.error("CROODS: data-url undefined ")
	            })
	        });
		});
	</script>
		
</body>
<!-- end::Body -->
</html>