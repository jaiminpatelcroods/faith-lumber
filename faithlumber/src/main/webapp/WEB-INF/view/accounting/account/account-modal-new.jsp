	<div class="modal fade" id="account_new_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					New Account
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="account_new_form">
					<div class="form-group">
						<label class="form-control-label">
							Account Name:
						</label>
						<input type="text" name="accountName" class="form-control" id="accountName">
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Account Group:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<select class="form-control m-select2"  placeholder="Select Account Group" id="accountGroup" name="group.accountGroupId">
								<c:forEach var="accountGroupVo" items="${accountGroupVos}" varStatus="index">	
									<option value="${accountGroupVo.accountGroupId}">${accountGroupVo.accountGroupName}</option>
								</c:forEach>
							</select>	
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Opening Balance:</label>
						<div class="col-lg-6 col-md-6 col-sm-12"><input type="text" class="form-control m-input" placeholder="Debit" name="debit" id="debit"/> </div>	
						<div class="col-lg-6 col-md-6 col-sm-12	"><input type="text" class="form-control m-input" placeholder="Credit" name="credit" id="credit"/></div>
					</div> 
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button"id="saveaccount" class="btn btn-primary">Save</button>
			</div>
		</div>
	</div>
</div>