<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
	
	<%@include file="../../header/head.jsp" %>
	
	<title>Opening Balance</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Opening Balance</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/account" class="m-nav__link">
										<span class="m-nav__link-text">Chart of Account</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
						
					<div class="row">
						
						<!-- Department -->
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<span class="m-portlet__head-icon">
												<i class="flaticon-cogwheel-2"></i>
											</span>
											<h3 class="m-portlet__head-text m--font-brand">
												Setup Opening Balance
											</h3>
										</div>
									</div>
								</div>
								<div class="m-portlet__body" >
									<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
										<div class="row align-items-center">
											<div class="col-xl-8 order-2 order-xl-6">
												<div class="form-group m-form__group row align-items-center">
													<div class="col-lg-4 col-md-4 col-sm-12">
														<div class="m-form__group">
															<form method="get">
																<div class="m-form__control">
																	 <select class="form-control m-select2" name="financialYearVo" id="financialYearVo" data-col-index="0">
																		<c:forEach var="financialYearVo" items="${financialYearVos}" varStatus="index">
																			
																			<option  value="${financialYearVo.yearInterval}" 
																				<c:if test="${sessionScope.financialYear==financialYearVo.yearInterval}">selected="selected"</c:if>>${financialYearVo.yearInterval}</option>	
																		</c:forEach>					
																	</select> 
																</div>
															</form>
														</div>
														<div class="d-md-none m--margin-bottom-10"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div  class="m_datatable"  >
										<table class="table table-striped- table-bordered table-hover table-checkable" id="opening_balance_table">
											<thead>
						  						<tr>
						  							<th>#</th>
						  							<th>Account Name</th>
						  							<th>Account Group</th>
						  							<th>Debit</th>
						  							<th>Credit</th>
							  					</tr>
											</thead>
										</table>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
						<!-- End Department -->
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../../footer/footer.jsp" %>
		
		<%-- <%@include file="account-modal-update.jsp" %> --%>
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/datatable/jquery.spring-friendly.js" type="text/javascript"></script>
	
	<script>
	
	var DatatablesDataSourceHtml = {
		init: function() {
	        $("#opening_balance_table").DataTable({
	            responsive: !0,
	            pageLength: 10,
	            searchDelay: 500,
				processing: !0,
				serverSide: true,
				ajax: {
					url: "<%=request.getContextPath()%>/account/openingbalance/datatable",
					type: "POST",
					"data": function ( d ) {
						return $.extend( {}, d, {
							"yearInterval": $('#financialYearVo').val(),
						});
					}
				},
				lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
				
				columns: [{
					data: "accountCustomId"
				},{
					data: "accountName"
				},{
					data: "group.accountGroupName"
				},{
					data: "accountCustomId"
				},{
					data: "accountCustomId"
				}],
				
				columnDefs: [
					{
						targets: 4,
						render: function(a, e, t, n) {
							var a = '<input type="hidden" name="accountCustomId" value="'+t.accountCustomId+'">';
							a += '<input type="text" class="form-control m-input" onChange="changeAmount('+t.accountCustomId+',this)" name="creditAmount"  value="'+t.creditAmount+'" placeholder="Credit Amount" />';
							return a;
						}
					},{
						targets: 3,
						render: function(a, e, t, n) {
							return '<input type="text" class="form-control m-input" onChange="changeAmount('+t.accountCustomId+',this)" name="debitAmount" value="'+t.debitAmount+'" placeholder="Debit Amount" />'
						}
					},{
						targets: 0,
						orderable: !1,
						render: function(a, e, t, n) {
							return (n.row+n.settings._iDisplayStart+1);
						}
					}],
	            
	            //lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
	            fixedHeader: {
	                header: false,
	                footer: false
	            },
	        }),$("#financialYearVo").on("change", function(t) {
	        	$('#opening_balance_table').DataTable().draw()
            })
	    }
	};
	
	
	jQuery(document).ready(function() {
		DatatablesDataSourceHtml.init();
		//$("#financialYearVo").val("${sessionScope.financialYear}").trigger('change');
	});
	
	function changeAmount(accountCustomId,text){
		
		if($(text).attr('name') == 'debitAmount') {
			$(text).closest('tr').find("input[name='creditAmount']" ).val(0)
		} else {
			$(text).closest('tr').find("input[name='debitAmount']" ).val(0)
		}
		
		$.post("/account/openingbalance/update", {
			yearInterval:$("#financialYearVo").val(),
			accountCustomId: $(text).closest('tr').find("input[name='accountCustomId']" ).val(),
			creditAmount:$(text).closest('tr').find("input[name='creditAmount']" ).val(),
			debitAmount:$(text).closest('tr').find("input[name='debitAmount']" ).val() 
		}).done(function(data) {
			toastr.error("Opening Balance updated successfully");
		});  
	}
	
	/*function debitChange(accountcustom,debit) {
		//alert("DEBITSSS"+accountcustom+"::"+openingbalnce+"::"+debit.value)
		//alert($(debit).closest('tr').find("input[id*='openingbalance']").val())
		
		$(debit).closest('tr').find("input[name='creditAmount']").val(0)
		var credit=0;
		var openingbalnce=$(debit).closest('tr').find("input[id*='openingbalance']").val();
		
		$.post("/account/openingbalance/update", {
			year:$("#yearInterval").val(),
			accountcustom: accountcustom,
			openingbalnce: openingbalnce,
			credit:credit,
			debit:$(debit).val()
		}).done(function(data) {
			$(debit).closest('tr').find("input[id*='openingbalance']").val(data.openingBalanceId)
		}); 
	} */
	</script>
</body>
<!-- end::Body -->
</html>