<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${materialRequestVo.prefix}${materialRequestVo.requestNo} | Material Request</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		/* table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		} */
		
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${materialRequestVo.prefix}${materialRequestVo.requestNo}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/materialrequest" class="m-nav__link">
										<span class="m-nav__link-text">Material Request</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="materialrequest_form" method="post">	
						<input type="hidden" name="total" id="total" value="0"/>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">Material Request</h3>
											</div>
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
																
												<%-- <li class="m-portlet__nav-item">
													<a href="#" class="btn btn-metal m-btn m-btn--icon m-btn--icon-only" onclick="printSalesReport()"
														data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i></a>
												</li>
												<li class="m-portlet__nav-item">
													<a href="/purchaserequest/${purchaseRequestVo.purchaseRequestId}/pdf" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only" target="_blank"	
														data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="PDF"><i class="fa fa-file-pdf"></i></a>	
												</li> --%>
												<li class="m-portlet__nav-item">
													<!--begin: Dropdown-->
					                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
					                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-primary m-btn m-btn--icon m-btn--icon-only">
					                                        <i class="la la-ellipsis-h"></i>
					                                    </a>
					                                    <div class="m-dropdown__wrapper">
				                                            <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
				                                            <div class="m-dropdown__inner">
				                                                <div class="m-dropdown__body">              
				                                                    <div class="m-dropdown__content">
				                                                        <ul class="m-nav">
				                                                            <li class="m-nav__section m-nav__section--first">
				                                                                <span class="m-nav__section-text">Quick Actions</span>
				                                                            </li>
				                                                             
							                                                <li class="m-nav__separator m-nav__separator--fit">
							                                                </li>
							                                                <li class="m-nav__item">
							                                                    <a href="<%=request.getContextPath() %>/materialrequest/${materialRequestVo.materialRequestId}/edit" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																					<span><i class="flaticon-edit"></i><span>Edit</span></span>
																				</a>
							                                                    <button type="button" data-url="<%=request.getContextPath() %>/materialrequest/${materialRequestVo.materialRequestId}/delete" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right delete-btn">
																					<span><i class="flaticon-delete-1"></i><span>Delete</span></span>
																				</button>
							                                                </li>
				                                                        </ul>
				                                                    </div>
				                                                </div>
				                                            </div>
					                                    </div>
					                                </div>
					                                <!--end: Dropdown-->
					                            </li>
												
											</ul>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<!-- <div class="m-widget28__pic m-portlet-fit--sides"></div> -->			  
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Request No.:</span>
																		<span>
																			${materialRequestVo.prefix}${materialRequestVo.requestNo}
																		</span>
																	</div>
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Request Date:</span>
																		<span>
																			<c:if test="${empty materialRequestVo.requestDate}">N/A</c:if> ${materialRequestVo.requestDate}
																		</span>
																	</div>
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Required Date:</span>
																		<span>
																			<c:if test="${empty materialRequestVo.requireDate}">N/A</c:if> ${materialRequestVo.requireDate}
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<!-- <div class="m-widget28__pic m-portlet-fit--sides"></div> -->			  
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Requested By:</span>
																		<span>
																			<c:if test="${empty materialRequestVo.requestedBy}">N/A</c:if> ${materialRequestVo.requestedBy.employeeName}
																		</span>
																	</div>
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Jobwork:</span>
																		<span>
																			<c:if test="${empty materialRequestVo.jobworkVo}">N/A</c:if> ${materialRequestVo.jobworkVo.prefix}${materialRequestVo.jobworkVo.jobworkNo}
																		</span>
																	</div>
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Notes:</span>
																		<span><c:if test="${empty materialRequestVo.note}">N/A</c:if>${materialRequestVo.note}</span>
																	</div>																	
																</div>					      	 		      	
															</div>
														</div>
														
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="table-responsive m--margin-top-30">
													<table class="table m-table table-bordered table-hover" id="product_table">
														<thead>
															<tr>
																<th>#</th>
																<th>Product</th>
																<th class="text-right">Qty</th>
																<th></th>
															</tr>
														</thead>
													    <tbody data-table-list="">
													    	<c:forEach items="${materialRequestVo.materialRequestItemVos}" var="materialRequestItemVo" varStatus="status">
													    	
														    	<tr data-table-item="${status.index}" class="">
														    		<td class="" style="width: 50px;">
														    			<span data-item-index>${status.index+1}</span>
														    		</td>
															        <td class="" style="width: 370px;">
															        	<div class="form-group m-form__group p-0">
																			${materialRequestItemVo.productVariantVo.productVo.categoryVo.categoryName} 
																			${materialRequestItemVo.productVariantVo.productVo.name} 
																			${materialRequestItemVo.productVariantVo.variantName}
																		</div>
															        </td>
															        <td class="text-right" style="width: 270px;">
															        	<div class="form-group m-form__group p-0">
															        		<span>${materialRequestItemVo.qty}</span>
																			<small data-item-uom>${materialRequestItemVo.productVariantVo.productVo.unitOfMeasurementVo.measurementCode}</small>
															        	</div>
															        </td>
															        <td class="p-0" style="width: 40px;">
															        </td>
														    	</tr>
														    </c:forEach>
														</tbody>
												  	</table>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
		
	<script src="<%=request.getContextPath()%>/script/materialrequest/materialrequest-script.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".delete-btn").click(function(e) {
	            
	            var u = $(this).data("url") ? $(this).data("url") : '';
	    		swal({
	                title: "Are you sure?",
	                text: "You won't be able to revert this!",
	                type: "warning",
	                showCancelButton: !0,
	                confirmButtonText: "Yes, delete it!"
	            }).then(function(e) {
	            	e.value && u != ''? window.location.href=u : console.error("CROODS: data-url undefined ")
	            })
	        });
		});
	</script>
	
</body>
<!-- end::Body -->
</html>