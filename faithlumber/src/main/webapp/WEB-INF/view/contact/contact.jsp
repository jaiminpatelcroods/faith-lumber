<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${displayContactType}</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${displayContactType}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
						
					<div class="row">
						
						<!-- Department -->
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<a href="<%=request.getContextPath() %>/contact/${type}/new" class="btn btn-primary m-btn m-btn--icon m-btn--air">
												<span><i class="la la-plus"></i><span>${displayContactType}</span></span>
											</a>
										</div>
									</div>
									<div class="m-portlet__head-tools">
										<a href="#" id="export_print" class="btn btn-metal m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i></a>
										<a href="#" id="export_excel" class="btn btn-success m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Excel"> <i class="fa fa-file-excel"></i>
										</a>
										<a href="#" id="export_pdf" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="PDF"> <i class="fa fa-file-pdf"></i>
										</a>
									</div>
								</div>
								<div class="m-portlet__body" >
									<div  class="m_datatable">
										<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 collapse" id="m_datatable_group_action_form">
											<div class="row align-items-center">
												<div class="col-xl-12">
													<div class="m-form__group m-form__group--inline">
														<div class="m-form__label m-form__label-no-wrap">
															<label class="m--font-bold m--font-danger-">
																Selected <span id="m_datatable_selected_number"></span>
																records:
															</label>
														</div>
														<div class="m-form__control">
															<div class="btn-toolbar">
																<button class="btn btn-info m-btn btn-sm m-btn m-btn--icon" type="button" id="m_datatable_check_all" onclick="sendSMS()" data-target="#modal_send_msg"  data-toggle="modal"><i class="fa fa-send"></i>&nbsp;Send SMS</button>
																&nbsp;&nbsp;&nbsp;	
																<!-- <button class="btn btn-info m-btn btn-sm m-btn m-btn--icon " type="button" id="m_datatable_check_all" onclick="sendMail()" data-target="#modal_send_mail"  data-toggle="modal"><i class="fa fa-envelope"></i>&nbsp;Send Mail</button> -->	
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										
										<table class="table table-striped- table-bordered table-hover table-checkable" id="contact_table">
											<thead>
						  						<tr>
						  						<c:if test="${type == Constant.CONTACT_TRANSPORT}">
				  									<th><label class="m-checkbox m-checkbox--bold m-checkbox--state-brand m--margin-top-0 m--margin-bottom-0" style="display:initial">
			  										<input type="checkbox" class="checkbox" id="select-all" name="">
			  											<span></span>
			  										</label></th>
			  									</c:if> 
			  									<c:if test="${type == Constant.CONTACT_CUSTOMER}">
				  									<th>#</th>
				  									</c:if>
				  									<th>Company Name</th>
				  									<th>Mobile No.</th>
				  									<c:if test="${type == Constant.CONTACT_AGENT || type == Constant.CONTACT_TRANSPORT}">
				  										<th>Email</th>
				  									</c:if>
				  									<th>GSTIN</th>
				  									<th>City</th>
				  									<%-- <c:if test="${type == Constant.CONTACT_CUSTOMER || type == Constant.CONTACT_SUPPLIER}">
				  										<th>Due Amount</th>
				  									</c:if> --%>
				  									<th>Actions</th>
							  					</tr>
											</thead>
											<tbody>
												<c:forEach var="contactVo" items="${contactVos}" varStatus="index">
													<tr>
														<td>
															<c:if test="${type == Constant.CONTACT_TRANSPORT}">
																<span class="m-checkbox-inline">
																	<label class="m-checkbox m-checkbox--bold m-checkbox--state-brand m--margin-top-0 m--margin-bottom-0" style="display:initial"><input type="checkbox" class="checkbox" id="" name="" data-mobileno="${contactVo.companyMobileno}" value="${contactVo.companyMobileno}">
																	<span></span>
																</label>
															</span>
															</c:if>
														 	${index.index+1}
														</td>
														<td><a
															href="${pageContext.request.contextPath}/contact/${type}/${contactVo.contactId}"
															class="m-link m--font-bolder">${contactVo.companyName}</a> <br>
										
														</td>
														<td>${contactVo.companyMobileno}</td>
														<c:if test="${type == Constant.CONTACT_AGENT || type == Constant.CONTACT_TRANSPORT}">
															<td>${contactVo.companyEmail}</td>
														</c:if>
														<td>${contactVo.gstin}</td>
														<td>${contactVo.contactAddressVos.get(0).cityName}</td>
														<%-- <c:if test="${type == Constant.CONTACT_CUSTOMER || type == Constant.CONTACT_SUPPLIER}">
															<td>${contactVo.dueAmount}</td>
														</c:if> --%>
														<td>
															<a href="${pageContext.request.contextPath}/contact/${type}/${contactVo.contactId}/edit"  class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill"
																title="Edit"> <i class="fa fa-edit"></i></a>
															<button data-url="${pageContext.request.contextPath}/contact/${type}/${contactVo.contactId}/delete" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill delete-btn"
																title="Delete"> <i class="fa fa-trash"></i></button>
														</td>
													</tr>
												</c:forEach>
											</tbody>			
										</table>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
						<!-- End Department -->
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		<div class="modal fade" id="modal_send_msg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog " role="document">
				<input type="hidden" name="companyMobileno" id="phoneno" value=""/>
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							Send SMS
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
					</div>
					<div class="modal-body">
							<p class="col-12 " id="errorMessageCategory" style="color: red;"></p>
							<div class="form-group m-form__group row">
		            			<label class="col-12 col-form-label">Message <span class="required">*</span></label>
		              			<div class="col-12 ">
		               				<textarea name="message" id="offer_message" class="form-control m-input" maxlength="480" rows="4" placeholder="Message" ></textarea>
		               			</div>
		             		</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="send_sms_btn">Send</button>
	          		    	<button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
		
	<script src="<%=request.getContextPath()%>/script/accounting/account/account-new-script.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/script/accounting/account/account-update-script.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	
	var DatatablesDataSourceHtml = {
	    init: function() {
	        $("#contact_table").DataTable({
	        	responsive: !0,
	        	buttons: [
		            {
		            	extend: 'print',
		            	exportOptions: {
		                    columns: [0,1,2,3,4]
		                },
		                title :"${sessionScope.name}",
		                messageTop: function () {
	                    	return '<span class="sub-heading"><b>${displayContactType} List</b></span>';
	                    },
	                    autoPrint: true,
			            customize: function ( win ) {
					    	 $(win.document.body).find('h1').css('text-align', 'center');
					    	$( win.document.body).find("div").css('text-align', 'center');
					    	
					    	var css = '@page { size: auto;margin-top:5mm;margin-left:5mm;margin-right:5mm; }',
		                    head = win.document.head || win.document.getElementsByTagName('head')[0],
		                    style = win.document.createElement('style');
			                style.type = 'text/css';
			                style.media = 'print';
			 
			                if (style.styleSheet)
			                {
			                  style.styleSheet.cssText = css;
			                }
			                else
			                {
			                  style.appendChild(win.document.createTextNode(css));
			                }
			 
			                head.appendChild(style);
					    	
		                
	
	              		 } 
		            },
		            {
		            	extend: 'pdfHtml5',
		            	//extension: '.pdf'
		            	exportOptions: {
		                    columns: [0,1,2,3,4]
		                },
		                pageSize: 'LEGAL',
		                title :"${sessionScope.name}",
		                messageTop: function () {
	                    	return '${displayContactType} List';
	                    },
	                    customize: function(doc) {
	                    	doc.styles.tableHeader = {
	                            	fillColor:"#88898c",
	                            	color: '#ffffff',
	                            	bold: 'true',
	                        }
	                    
	                    	doc.styles.title = {
	                          fontSize: '18',
	                          alignment: 'center',
	                          bold: 'true'
                        	}
	                        doc.pageMargins = [ 10, 20, 10, 10 ];
                            doc.content.forEach(function(item) {
		                        if (item.table) {
		                        	item.table.widths = [ 65, 170, 95, 100, 100];
		                        	item.table.body[1][3].alignment = 'right';
		                        	
		                        	for(i = 0; i < item.table.body.length; i++) {
		                        		item.table.body[i][0].alignment = 'center' ;
		                        		item.table.body[i][1].alignment = 'left' ;
		                        		item.table.body[i][2].alignment = 'left' ;
		                        		item.table.body[i][3].alignment = 'left' ;
		                        		item.table.body[i][4].alignment = 'left' ;
		                        		//item.table.body[i][5].alignment = 'right' ;
		                        		//item.table.body[i][6].alignment = 'right' ;
		                        		item.table.body[i][4].margin = [ 0, 0, 5, 0 ];
		                        	}
		                        	
		                        }
                            });
                           
	                        doc.styles.message = {
	  	                          fontSize: '12',
		                          alignment: 'center'
	                        	}
	                    }
		            },
		            {
		            	extend: 'excelHtml5',
		            	//extension: '.pdf'
		            	exportOptions: {
		                    columns: [0,1,2,3,4]
		                },
		                title :"${sessionScope.name}",
		                messageTop: function () {
	                    	return '${displayContactType} List';
	                    },
	                     customize: function(doc) {
	                    	var sheet = doc.xl.worksheets['sheet1.xml'];
	                    }
		            }
		        ],
	        }), $("#export_print").on("click", function(e) {
                e.preventDefault(), $("#contact_table").DataTable().button(0).trigger()
            }), $("#export_excel").on("click", function(e) {
                e.preventDefault(), $("#contact_table").DataTable().button(2).trigger()
            }), $("#export_pdf").on("click", function(e) {
                e.preventDefault(), $("#contact_table").DataTable().button(1).trigger()
            });
	    }
	};
	
	$(document).ready(function() {
		
		DatatablesDataSourceHtml.init();
		$(".dt-buttons").addClass("m--hide");
		
		$(".delete-btn").click(function(e) {
            
            var u = $(this).data("url") ? $(this).data("url") : '';
    		swal({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, delete it!"
            }).then(function(e) {
            	e.value && u != ''? window.location.href=u : console.error("CROODS: data-url undefined ")
            })
        });

		$("#contact_table").on("click", ".checkbox", function() {
	    	
			var total=$('.checkbox:checked').length;
			var tt=$('.checkbox').length;
			
			if(total>0){
				$('#m_datatable_group_action_form').collapse("show");
				if(total==tt){total=total-1;}
				$('#m_datatable_selected_number').html(total);
			}else{
				$('#m_datatable_group_action_form').collapse("hide");
			}
				
  		});
		
		$("#select-all" ).click(function() {
  			if($("#select-all").prop('checked') == true) {
  				$("#contact_table").find(".checkbox").prop('checked',true);
  			}
  			else{
  				$("#contact_table").find(".checkbox").prop('checked',false);
  			}
  		});
		
		$("#send_sms_btn" ).click(function() {
	    	
	    	if($("#offer_message").val()!=""){
	    		$.post("/contact/transport/sendsms", { phoneno: $("#phoneno").val(), message: $("#offer_message").val() })
	    		  .done(function( data ) {
	    			  toastr.options = {
	    					  "closeButton": true,
	    					  "debug": false,
	    					  "newestOnTop": false,
	    					  "progressBar": false,
	    					  "positionClass": "toast-top-center",
	    					  "preventDuplicates": true,
	    					  "onclick": null,
	    					  "showDuration": "300",
	    					  "hideDuration": "1000",
	    					  "timeOut": "5000",
	    					  "extendedTimeOut": "1000",
	    					  "showEasing": "swing",
	    					  "hideEasing": "linear",
	    					  "showMethod": "fadeIn",
	    					  "hideMethod": "fadeOut"
	    					  
	    					};
	    				toastr.success("","Message send successfully");
	    				$('#modal_send_msg').modal('hide');
	    		  });
	    	}else{
	    		toastr.options = {
						  "closeButton": true,
						  "debug": false,
						  "newestOnTop": false,
						  "progressBar": false,
						  "positionClass": "toast-top-center",
						  "preventDuplicates": true,
						  "onclick": null,
						  "showDuration": "300",
						  "hideDuration": "1000",
						  "timeOut": "5000",
						  "extendedTimeOut": "1000",
						  "showEasing": "swing",
						  "hideEasing": "linear",
						  "showMethod": "fadeIn",
						  "hideMethod": "fadeOut"
						};
						toastr.error("","Please Write Message");
			    	}
			    			    	
			    });
		
			});
	
			function sendSMS() {
				var phoneNo=""
					$("#contact_table").find("tbody").find(".checkbox").each(function(){
					if($(this).prop('checked') == true) {
						if($(this).val()!=""){
							phoneNo=phoneNo+$(this).val()+",";
							}
		  			   }
					
		  		  });
				$("#phoneno").val(phoneNo);
				$("#offer_message").html("Dear Transporter ,"
						+"\nWe require a truck as per below given details :"
						+"\nItem : "
						+"\nQty : "
						+"\nTons : "
						+"\nDestination :" 
						+"\nType : "   
						+"\nTruck required for ."
				); 
			}
		
	</script>	
</body>
<!-- end::Body -->
</html>