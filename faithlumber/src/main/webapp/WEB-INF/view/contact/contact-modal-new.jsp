<!--begin::Modal-->
<div class="modal fade" id="contact_new_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document" style="max-width: 1000px;">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="contact-modal-title">New Contact</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<input type="hidden" id="responseField" value="" />
			<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="contact_form" action="/contact/save" method="post">
				<input type="hidden" name="paymentType" value="COD" />
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="form-group m-form__group row">
								<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Company Name:</label>
								<div class="col-lg-12 col-md-12 col-sm-12">
									<input type="text" class="form-control m-input" name="companyName" id="companyName" placeholder="Company Name"/>
								</div>
							</div>
							<div class="form-group m-form__group row">
								<div class="col-lg-6 m-form__group-sub">
									<label class="form-control-label">Mobile No.:</label>
									<input type="text" class="form-control m-input" name="companyMobileno" placeholder="Mobile No." value="">
								</div>
								<div class="col-lg-6 m-form__group-sub">
									<label class="form-control-label">Telephone No.:</label>
									<input type="text" class="form-control m-input" name=companyTelephone  placeholder="Telephone No." value="">
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Email:</label>
								<div class="col-lg-12 col-md-12 col-sm-12">
									<input type="text" class="form-control m-input" name="companyEmail" placeholder="Email"/>
								</div>
							</div>
							<div class="form-group m-form__group row">
								<div class="col-lg-6 m-form__group-sub">
									<label class="form-control-label">Owner Name:</label>
									<input type="text" class="form-control m-input" name="ownerName" placeholder="Name" value="">
								</div>
								<div class="col-lg-6 m-form__group-sub">
									<label class="form-control-label">Owner Mobile No.:</label>
									<input type="text" class="form-control m-input" name="ownerMobileno"  placeholder="Mobile No." value="">
								</div>
							</div>
							<div class="form-group m-form__group row">
								<div class="col-lg-6 m-form__group-sub">
									<label class="form-control-label">Concern Person:</label>
									<input type="text" class="form-control m-input" name="concernPersonName" placeholder="Name" value="">
								</div>
								<div class="col-lg-6 m-form__group-sub">
									<label class="form-control-label">Concern Person Mobile No.:</label>
									<input type="text" class="form-control m-input" name="concernPersonMobileno"  placeholder="Mobile No." value="">
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="form-group m-form__group row">
								<label class="form-control-label col-lg-12 col-md-12 col-sm-12">GST Type:</label>
								<div class="col-lg-12 col-md-12 col-sm-12">
									<select class="form-control m-select2" id="gstType" name="gstType" placeholder="Select GST Type">
										<option value="UnRegistered">UnRegistered</option>
										<option value="Registered">Registered</option>
										<option value="Composition Scheme">Composition Scheme</option>
										<option value="Input Service Distributor">Input Service Distributor</option>
										<option value="E-Commerce Operator">E-Commerce Operator</option>
									</select>
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="form-control-label col-lg-12 col-md-12 col-sm-12">GSTIN:</label>
								<div class="col-lg-12 col-md-12 col-sm-12">
									<input type="text" class="form-control m-input" id="gstin"  name="gstin" placeholder="GSTIN"/>
								</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Pan No.:</label>
								<div class="col-lg-12 col-md-12 col-sm-12">
									<input type="text" class="form-control m-input" name="panNo" placeholder="Pan No."/>
								</div>
							</div>
							<div id="date-div" class="m--hide">
								<div class="form-group m-form__group row">
									<div class="col-lg-6 m-form__group-sub">
										<label class="form-control-label">Date of Birth:</label>
										<div class="input-group date" >
											<input type="text" class="form-control m-input clearbtn-datepicker" name="dateOfBirth" id="dateOfBirth" readonly placeholder="Date Of Birth"/>
											<div class="input-group-append">
												<span class="input-group-text">
													<i class="la la-calendar"></i>
												</span>
											</div>
										</div>
									</div>
									<div class="col-lg-6 m-form__group-sub">
										<label class="form-control-label">Anniversary Date:</label>
										<div class="input-group date" >
											<input type="text" class="form-control m-input clearbtn-datepicker" readonly name="anniversaryDate" id="anniversaryDate" placeholder="Anniversary Date"/>
											<div class="input-group-append">
												<span class="input-group-text">
													<i class="la la-calendar"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group m-form__group row m--hide" id="total-shops-div">
								<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Total No. of Shops:</label>
								<div class="col-lg-12 col-md-12 col-sm-12">
									<input type="text" class="form-control m-input" name="noOfShops" placeholder="Total No. of Shops"/>
								</div>
							</div>
							<div class="form-group m-form__group row m--hide" id="agent-commission-div">
								<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Agent Commission:</label>
								<div class="col-lg-12 col-md-12 col-sm-12">
									<input type="text" class="form-control m-input" id="agentCommission"  name="agentCommission" placeholder="Agent Commission"/>
									<span class="m-form__help">agent commission is in percentage only</span>
								</div>
							</div>
							<div class="m--hide" id="transport-div">
								<div class="form-group m-form__group row">
									<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Working Time:</label>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<input type="text" class="form-control m-input" id="workingTime"  name="workingTime" placeholder="Working Time"/>
									</div>
								</div>
								<div class="form-group m-form__group row">
									<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Available Location:</label>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<input type="text" class="form-control m-input" id="serviceAvailable" data-role="tagsinput" data-tag-color="label-info" name="serviceAvailable" placeholder="Available Location"/>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
					<input type="hidden" name="contactAddressVos[0].isDefault" value="1" />
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="form-group m-form__group row ">
								<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Address Line 1:</label>
								<div class="col-lg-12 col-md-12 col-sm-12">
									<input type="text" class="form-control m-input" name="contactAddressVos[0].addressLine1" placeholder="Address Line 1" value="">
								</div>
							</div>
							<div class="form-group m-form__group row ">
								<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Address Line 2:</label>
								<div class="col-lg-12 col-md-12 col-sm-12">
									<input type="text" class="form-control m-input" name="contactAddressVos[0].addressLine2" placeholder="Address Line 2" value="">
								</div>
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="form-group m-form__group row">
								<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
									<label class="form-control-label">Select Country:</label>
									<select class="form-control " id="countriesCode0" name="contactAddressVos[0].countriesCode" onchange="getAllStateAjax('countriesCode0','stateCode0')" data-default="${sessionScope.countryCode}" placeholder="Select Country" data-allow-clear="true">
									</select>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
									<label class="form-control-label">Select State:</label>
									<select class="form-control" id="stateCode0" name="contactAddressVos[0].stateCode" onchange="getAllCityAjax('stateCode0','cityCode0')" data-allow-clear="false" placeholder="Select State">
									</select>
								</div>
							</div>
							<div class="form-group m-form__group row">
								<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
									<label class="form-control-label">Select City:</label>
									<select class="form-control" id="cityCode0" name="contactAddressVos[0].cityCode" placeholder="Select City" data-allow-clear="true">
									</select>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
									<label class="form-control-label">ZIP/Postal code:</label>
									<input type="text" class="form-control m-input" name="contactAddressVos[0].pinCode" placeholder="ZIP/Postal code" value="">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary" id="savecontact">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!--end::Modal-->