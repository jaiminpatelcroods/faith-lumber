<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Edit | Profile</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
		}
		.input-group .select2-container {
			position: relative;
			-webkit-box-flex: 1;
			-ms-flex: 1 1 auto;
			flex: 1 1 auto;
			width: 1% !important;
			
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		.rotate {
		    -moz-transition: all .5s linear;
		    -webkit-transition: all .5s linear;
		    transition: all .5s linear;
		}
		.rotate.down {
		    -moz-transform:rotate(45deg);
		    -webkit-transform:rotate(45deg);
		    transform:rotate(45deg);
		}
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Edit Profile</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/setting" class="m-nav__link">
										<span class="m-nav__link-text">Settings</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="user_form" action="/profile/update" method="post">	
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">Basic Details</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Company Name:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="name" placeholder="Company Name" value="${userFrontVo.name}"/>
													</div>
												</div>
												
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Email:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="email" placeholder="Email" value="${userFrontVo.email}"/>
													</div>
												</div>
												
												<div class="form-group m-form__group row">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">Contact Name:</label>
														<input type="text" class="form-control m-input" name=contactName value="${userFrontVo.contactName}" placeholder="Contact Name" />
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">Mobile No.:</label>
														<input type="text" class="form-control m-input" name="contactNo" value="${userFrontVo.contactNo}" placeholder="Mobile No." />
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Telephone No.:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="telephone" value="${userFrontVo.telephone}" placeholder="Telephone No."/>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">GST Type:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<select class="form-control m-select2" id="gstType" name="gstRegistrationType" placeholder="Select GST Type">
															<option value="UnRegistered">UnRegistered</option>
															<option value="Registered">Registered</option>
															<option value="Composition Scheme">Composition Scheme</option>
															<option value="Input Service Distributor">Input Service Distributor</option>
															<option value="E-Commerce Operator">E-Commerce Operator</option>
														</select>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">GSTIN:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" id="gst"  name="gst" value="${userFrontVo.gst}" placeholder="GSTIN"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Pan No.:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="panNo" value="${userFrontVo.panNo}" placeholder="Pan No"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Website:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="website" value="${userFrontVo.website}" placeholder="Website"/>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions m-form__actions--solid m--padding-20">
	                                        <div class="form-group m-form__group row">
												<div class="col-lg-4 m-form__group-sub">
													<label class="form-control-label">User Name:</label>
													<input type="text" class="form-control form-control-danger" name="userName" id="userName" value="${userFrontVo.userName}" placeholder="User Name"/>
												</div>
												<div class="col-lg-4 m-form__group-sub">
													<label class="form-control-label">Financial Year:</label>
													<select class="form-control m-select2" id="defaultYearInterval" name="defaultYearInterval" placeholder="Select Financial Year">
														<option value="">Select Financial Year</option>
														<c:forEach items="${financialYearVos}" var="financialYearVo">
															<option value="${financialYearVo.yearInterval}"
															 <c:if test="${userFrontVo.defaultYearInterval == financialYearVo.yearInterval}">selected="selected"</c:if>>
															 	${financialYearVo.yearInterval}</option>
														</c:forEach>
													</select>
												</div>
												<div class="col-lg-4 m-form__group-sub">
													<label class="form-control-label">Month Interval:</label>
													<select class="form-control m-select2" id="monthInterval" name="monthInterval" placeholder="Select Month Interval">
														<option value="">Select Month Interval</option>
														<c:forEach items="${financialMonthVos}" var="financialMonthVos">
															<option value="${financialMonthVos.monthInterval}"
															 <c:if test="${userFrontVo.monthInterval == financialMonthVos.monthInterval}">selected="selected"</c:if>>
															 	${financialMonthVos.monthInterval}</option>
														</c:forEach>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
								
								<!--begin::Portlet-->
								<div class="m-portlet" data-repeater-item>
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Address Details
												</h3>
											</div>			
										</div>
									</div>
									<div class="m-portlet__body">
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row ">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Address:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<textarea rows="6" class="form-control m-input" name="address">${userFrontVo.address}</textarea>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
														<label class="form-control-label">Select Country:</label>
														<select class="form-control m-select2" id="countriesCode" name="countriesCode" data-default="${userFrontVo.countriesCode}" onchange="getAllStateAjax('countriesCode','stateCode')" placeholder="Select Country" data-allow-clear="true">
														</select>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
														<label class="form-control-label">Select State:</label>
														<select class="form-control m-select2" id="stateCode" name="stateCode" data-default="${userFrontVo.stateCode}" onchange="getAllCityAjax('stateCode','cityCode')" data-allow-clear="false" placeholder="Select State">
														</select>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
														<label class="form-control-label">Select City:</label>
														<select class="form-control m-select2" id="cityCode" name="cityCode" data-default="${userFrontVo.cityCode}" placeholder="Select City" data-allow-clear="true">
														</select>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
														<label class="form-control-label">ZIP/Postal code:</label>
														<input type="text" class="form-control m-input" name="pincode" value="${userFrontVo.pincode}" placeholder="ZIP/Postal code" />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end::Portlet-->
								<!--begin::Portlet-->
								<div class="m-portlet" data-repeater-item>
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Bank Details
												</h3>
											</div>			
										</div>
									</div>
									<div class="m-portlet__body">
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row m--padding-left-0">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Bank Name:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="bankName" value="${userFrontVo.bankName}" placeholder="Bank Name"/>
													</div>
												</div>
												
												<div class="form-group m-form__group row m--padding-left-0">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Branch Name:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="bankBranch" value="${userFrontVo.bankBranch}" placeholder="Branch Name"/>
													</div>
												</div>
												<div class="form-group m-form__group row m--padding-left-0">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Account Holder Name:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="bankAcholderName" value="${userFrontVo.bankAcholderName}" placeholder="Account Holder Name"/>
													</div>
												</div>
												<div class="form-group m-form__group row m--padding-left-0">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Account No.:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="bankAcno" value="${userFrontVo.bankAcno}" placeholder="Account No."/>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row m--padding-left-0">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">IBAN No:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="ibanNo" value="${userFrontVo.ibanNo}" placeholder="IBAN No"/>
													</div>
												</div>
												<div class="form-group m-form__group row m--padding-left-0">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">IFSC Code:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="bankIFSC" value="${userFrontVo.bankIFSC}" placeholder="IFSC Code"/>
													</div>
												</div>
												
												<div class="form-group m-form__group row m--padding-left-0">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">SWIFT Code:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="bankSwiftCode" value="${userFrontVo.bankSwiftCode}" placeholder="SWIFT Code"/>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions--solid m-form__actions--right">
								<button type="submit" class="btn btn-brand" id="save_profile">
									Submit
								</button>
								
								<a href="/setting" class="btn btn-secondary">
									Cancel
								</a>
							</div>
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	<%-- <script src="<%=request.getContextPath()%>/script/employee/employee-script.js" type="text/javascript"></script> --%>
	<%@include file="../global/location-ajax.jsp" %>
	
	<script type="text/javascript">
		$(document).ready(function (){
			getAllCountryAjax("countriesCode");
			
			$("#user_form").formValidation({
				framework : 'bootstrap',
				live:'disabled',
				excluded : ":disabled",
				button:{
					selector : "#save_profile",
					disabled : "disabled",
				},
				icon : null,
				fields : {
					name:{
						validators: {
							notEmpty: {
								message: 'The Name is required. '
							},
							stringLength : {
								max : 100,
								message : 'The Name Not More than 100 characters long. '
							},
							regexp : {
								regexp : /^[a-zA-Z0-9_-\s-.,()& ]+$/,
								message : 'The Name can only consist of alphabetical, numberical value. '
							},
						}
					}, 
					contactNo : {
						validators : {
							stringLength : {
								max : 15,
								message : 'The Mobile no. must be 15 digit long. '
							},
							regexp : {
								regexp : /^[0-9+]+$/,
								message : 'The Mobile no. can only consist numerical value. '
							}
						}
					},
					telephone : {
						validators : {
							stringLength : {
								
								max : 17,
								message : 'The Telehphone No must be 17 digit long. '
							},
							regexp : {
								regexp : /^[0-9]+$/,
								message : 'The Telephone No consist only numeric digit. '
							}
						}
					},
					email : {
						verbose : false,
						validators : {
							regexp : {
								regexp : /^[a-z|A-Z|]+([.|-]?[a-z|A-Z|0-9|_]+)*@[a-z|A-Z]+[a-z|A-Z|0-9]*[\\.]+[a-z|A-Z]+([.]?[a-z|A-Z]+)$/,
								message : 'The Email address is not valid. '
							},
						}
					},
					contactName:{
						validators: {
							stringLength : {
								max : 100,
								message : 'The Contact Not More  than 100 characters long. '
							},
							regexp : {
								regexp : /^[a-zA-Z0-9_-\s-.,()& ]+$/,
								message : 'The Contact Name can only consist of alphabetical, numberical value. '
							},
						}
					},
					gstRegistrationType : {
						validators : {
							notEmpty : {
								message : 'The GST Type is required. '
							},

						}
					},
					gst : {
						validators : {
							stringLength : {
								max : 15,
								message : 'GSTIN/UIN is not more than 15 digit long. '
							},
							regexp : {

								regexp : /^\d{2}[A-Z]{5}\d{4}[A-Z]{1}\d{1}[A-Z]{1}[A-Z0-9]{1}$/,
								message : 'The GSTIN/UIN is not valid. '
							}
						}
					},
					panNo : {
						validators : {
							stringLength : {
								max : 10,
								message : 'The Pan No not more than 10 digit long. '
							},
							regexp : {

								regexp : /^[A-Z]{5}\d{4}[A-Z]{1}$/,
								message : 'The Pan No can only consist 5 capital alphabatic  value , 4 digit and 1 capital alphabate. '
							}
						}
					},
					userName:{
						validators: {
						 notEmpty : {
							message : 'The UserName name is required. '
					           },
						stringLength : {
							max : 50,
							message : 'The UserName must be less than 50 characters long. '
						       },
						regexp : {
							regexp : /^[a-zA-Z0-9_-\s-., ]+$/,
							message : 'The UserName can only consist of alphabetical, number and underscore. '
						       }
					    }
					},
					defaultYearInterval : {
						validators : {
							notEmpty : {
								message : 'The Financial Year is required. '
							},

						}
					},
					monthInterval : {
						validators : {
							notEmpty : {
								message : 'The Month Interval is required. '
							},

						}
					},
					countriesCode : {
						validators : {
							notEmpty : {
								message : 'The Country is required. '
							},

						}
					},
					stateCode : {
						validators : {
							notEmpty : {
								message : 'The State is required. '
							},

						}
					},
					cityCode : {
						validators : {
							notEmpty : {
								message : 'The City is required. '
							},

						}
					},
					pincode : {
						validators : {
							stringLength : {
								min : 6,
								max : 6,
								message : 'The Pincode must be 6 digit long. '
							},
							regexp : {
								regexp : /^\d{6}$/,
								message : 'The Pincode can only consist of numerical value. '
							}
						}
					},
					bankName : {
						validators : {
							stringLength : {
								max : 50,
								message : 'The Bank Name not more than 50 characters long. '
							},
							regexp : {
								regexp : /^[a-zA-Z0-9_-\s-, ]+$/,
								message : 'The Bank Name can only consist of alphabetical and numerical value. '
							}
						}
					},
					bankBranch : {
						validators : {
							stringLength : {
								max : 50,
								message : 'The Branch Name not more than 50 characters long. '
							},
							regexp : {
								regexp : /^[a-zA-Z0-9_-\s-, ]+$/,
								message : 'The Branch Name can only consist of alphabetical and numerical value. '
							}
						}
					},
					bankAcholderName : {
						validators : {
							stringLength : {
								max : 50,
								message : 'The Bank Account Holder not more than 50 characters long. '
							},
							regexp : {
								regexp : /^[a-zA-Z0-9_-\s-, ]+$/,
								message : 'The Bank Account Holder can only consist of alphabetical and numerical value. '
							}
						}
					},
					bankAcno : {
						validators : {
							stringLength : {
								max : 20,
								message : 'The Account No. not more than 20 digit long. '
							},
							regexp : {
								regexp : /^[0-9]+$/,
								message : 'The Account No can only consist of numerical value. '
							}
						}
					},
					bankIFSC : {
						validators : {
							stringLength : {
								max : 20,
								message : 'The IFSC code must be 20 character long. '
							},
							regexp : {
								regexp : /[A-Z0-9]$/,
								message : 'The IFSC code can only consist of alphabetical and numerical. '
							}
						}
					},
				}
			});
		});
	</script>
</body>
<!-- end::Body -->
</html>