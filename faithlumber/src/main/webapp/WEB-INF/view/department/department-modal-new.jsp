	<div class="modal fade" id="department_new_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					New Department
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="department_new_form">
					<div class="form-group">
						<label class="form-control-label">
							Department Name:
						</label>
						<input type="text" name="departmentName" class="form-control" id="departmentNameModal">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" id="savedepartment" class="btn btn-primary">Save</button>
			</div>
		</div>
	</div>
</div>