<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html >

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Gate</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Gate</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
						
					<div class="row">
						
						<!-- Designation -->
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<a href="#" data-toggle="modal" data-target="#gatein_new_modal" class="btn btn-primary m-btn m-btn--icon m-btn--air">
												<span><i class="la la-plus"></i><span>Gate</span></span>
											</a>
										</div>			
									</div>
								</div>
								<div class="m-portlet__body" >
									<div  class="m_datatable"  >
										<table class="table table-striped- table-bordered table-hover table-checkable" id="gate_table">
											<thead>
						  						<tr>
			  									<th>#</th>
			  									<th>Gate Date</th>
			  									<th>Company Name</th>
			  									<th>Vehicle No</th>
			  									<th>Sales Order</th>
			  									<th>Actions</th>
			  									<th>Remark</th>
							  					</tr>
											</thead>
											<tbody>
												
											</tbody>				
										</table>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
						<!-- End Designation -->
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
		<%@include file="gate-modal-new.jsp" %>
		<%@include file="gate-modal-update.jsp" %>
		
		<!-- send msg modal start -->
		<div class="modal fade" id="modal_send_msg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog " role="document">
				<input type="hidden" name="contactVo.companyMobileno" id="phoneno" value=""/>
			
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							Send SMS
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
					</div>
					<div class="modal-body">
							<p class="col-12 " id="errorMessageCategory" style="color: red;"></p>
							<div class="form-group m-form__group row">
		            			<label class="col-12 col-form-label">Message <span class="required">*</span></label>
		              			<div class="col-12 ">
		               				<textarea name="message" id="offer_message" onkeyup="" class="form-control m-input" maxlength="500" rows="4" placeholder="Message" ></textarea>
		               					<div id="charNum"></div>
		               			</div>
		             		</div>
						</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="send_sms_btn">Send</button>
	          		    	<button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/assets/datatable/jquery.spring-friendly.js" type="text/javascript"></script> 
	
	<script src="<%=request.getContextPath()%>/script/gate/gate-new-script.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/script/gate/gate-update-script.js" type="text/javascript"></script>
	<script type="text/javascript">
	
	$(document).ready(function() {
		$("#contactVo").select2();	
		$("#salesVo").select2();	
		
		try {
			 $('#gateDate').datepicker('setDate','today');
				
			} catch(e) {
				$('#gateDate').datepicker('setDate','<%=session.getAttribute("firstDateFinancialYear")%>');
			}
			
		$("#updatecontactVo").select2();	
		$("#updatesalesVo").select2();
		
		$('#offer_message').keyup(function () {
			  var max = 500;
			  var len = $(this).val().length;
			  if (len >= max) {
			    $('#charNum').text(' you have reached the limit');
			  } else {
			    var char = max - len;
			    $('#charNum').text(char + ' characters left');
			  }
			});	
		
		});
			var DatatablesDataSourceHtml = {
			 	init: function() {
	      		  $("#gate_table").DataTable({
	        		responsive: !0,
       		 		responsive: !0,
       			 	pageLength: 10,
		            searchDelay: 500,
					processing: !0,
					serverSide: true,
					ajax:  {
						url: "<%=request.getContextPath()%>/gate/datatable",
						type: "POST",
					},
					lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
					  columns: [{
			                data: "gateId"
			            }, {
			                data: "gateDate"
			            }, {
			                data: "contactTransportVo.contactId"
			            }, {
			                data: "vehicleno"
			            },{
			                data: "salesVo.salesId"
			            },{
			                data: "gateId"
			            },{
			                data: "remark"
			            }],
			            columnDefs: [{
							targets: 5,
							orderable: !1,
							render: function(a, e, t, n) {
								var action = "";
								action += '<button data-toggle="modal" onclick="updategate(this,'+t.gateId+')" data-target="#gate_update_modal" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></button>'
								action += '<button onclick="deleteGate(this,'+t.gateId+')" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></button>'
								action += '<input type="hidden" value="'+t.remark+'" name="remark" id="remark'+t.gateId+'">'
								action += '<input type="hidden" name="contactId${contactVo.contactId}" id="contactId${contactVo.contactId}'+t.gateId+'"  value="'+t.contactTransportVo.contactId+'" />'
								action += '<input type="hidden" name="salesId${salesVo.salesId}" id="salesId${salesVo.salesId}'+t.gateId+'"  value="'+t.salesVo.salesId+'" />'
								action +='<button data-toggle="modal" onclick="sendmessage('+t.gateId+')" class="btn btn-info m-btn btn-sm m-btn m-btn--icon" type="button" id="smsbutton" data-target="#modal_send_msg"><i class="fa fa-send"></i>&nbsp;Send SMS</button>'
								return action;
							}
			            },{
							targets: 0,
							orderable: !1,
							render: function(a, e, t, n) {
								return (n.row+n.settings._iDisplayStart+1);
								//return (n.row+1);	
								
							}
						},{
							targets: 2,
							orderable: !1,
							render: function(a, e, t, n) {
								return '\n  <a href="/contact/transport/'+t.contactTransportVo.contactId+'" class="m-link m--font-bolder" aria-expanded="true">\n '+t.contactTransportVo.companyName+'</a>\n '
								}
						},{
							targets: 3,
							render: function(a, e, t, n) {
								return t.vehicleno;
							}
						},{
							targets:1,
			                render: function(a, e, t, n) {
							return moment(a).format('DD/MM/YYYY');
							}
					},{
						targets: 4,
						orderable: !1,
						render: function(a, e, t, n) {
							return '\n  <a href="/sales/order/'+t.salesVo.salesId+'" class="m-link m--font-bolder" aria-expanded="true">\n '+t.salesVo.prefix+t.salesVo.salesNo+'</a>\n '
							}
					},{
						visible:false,
						targets: 6,
						render: function(a, e, t, n) {
						return t.remark;
							}
					}],
		            fixedHeader: {
		                header: false,
		                footer: false
		            } 
		        })
		    }
		};
	
	 $(document).ready(function() {
		DatatablesDataSourceHtml.init();	
		});
	
		function deleteGate(rows,id)
   		 	{  		
			swal({
            title: "Are you sure?",
            text: "You won't be able to revert this!",
            type: "warning",
            showCancelButton: !0,
            confirmButtonText: "Yes, delete it!"
        }).then(function(e) {
        	
        	if(e.value) {
        		$.post("/gate/delete", {
   		        	id:id
       		    }, function( data,status ) {
       	        	var t = $('#gate_table').DataTable();
       	        	var crow=$(rows).closest('tr');		
       				var index=$(crow).find('td:eq(0)').text();
       	        	var b= t.row(index-1).remove().draw(false);
       	        	t.on( 'order.dt search.dt', function () {
       				        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
       				            cell.innerHTML = i+1;
       				        } );
       				    } ).draw();
       	        	 
       				toastr["success"]("Record Deleted....");
       			});      		
        	}
        })
		
    } 
		function submitSendSms(id){
			if($("#offer_message").val()!=""){
	    		$.post('/gate/'+id+'/sendsms', { phoneno: $("#phoneno").val(), message: $("#offer_message").val() })
	    		  .done(function( data ) {
	    			  toastr.options = {
	    					  "closeButton": true,
	    					  "debug": false,
	    					  "newestOnTop": false,
	    					  "progressBar": false,
	    					  "positionClass": "toast-top-center",
	    					  "preventDuplicates": true,
	    					  "onclick": null,
	    					  "showDuration": "300",
	    					  "hideDuration": "1000",
	    					  "timeOut": "5000",
	    					  "extendedTimeOut": "1000",
	    					  "showEasing": "swing",
	    					  "hideEasing": "linear",
	    					  "showMethod": "fadeIn",
	    					  "hideMethod": "fadeOut"
	    					  
	    					};
	    				toastr.success("","Message send successfully");
	    				$('#modal_send_msg').modal('hide');
	    		  });
	    	}else{
	    		toastr.options = {
						  "closeButton": true,
						  "debug": false,
						  "newestOnTop": false,
						  "progressBar": false,
						  "positionClass": "toast-top-center",
						  "preventDuplicates": true,
						  "onclick": null,
						  "showDuration": "300",
						  "hideDuration": "1000",
						  "timeOut": "5000",
						  "extendedTimeOut": "1000",
						  "showEasing": "swing",
						  "hideEasing": "linear",
						  "showMethod": "fadeIn",
						  "hideMethod": "fadeOut"
						};
						toastr.error("","Please Write Message");
			    	}
		}
		
		function sendmessage(id){
			$("#send_sms_btn").attr("onclick", "submitSendSms("+id+")")	
			$.ajax({
	            url: '/gate/'+id+'/sendsms/gatein',
	            type: 'post',
	            success: function (data) {
	            $('#offer_message').html(data);
	            }, 
      		 });
	  	  }	
				
	
	</script>	
</body>
<!-- end::Body -->
</html>