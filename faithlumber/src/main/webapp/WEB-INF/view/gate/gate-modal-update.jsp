<div class="modal fade" id="gate_update_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					Edit Gate
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
				</button>
			</div>
		<div class="modal-body">
			<form id="gate_update_form">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 p-0">
						<div class="form-group ">
						  <label class="col-form-label col-lg-12 col-md-12 col-sm-12">Date:</label>
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="input-group date" >
									<input type="text" class="form-control m-input todaybtn-datepicker" name="gateDate" readonly id="updategateDate"  data-date-format="dd/mm/yyyy"
										data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'
										value='<fmt:formatDate pattern="dd/MM/yyyy" value="${gateVo.gateDate}" />'/>
										<input type="hidden" id="gateId" name="gateId" value="${gateVo.gateId}"/>
										<div class="input-group-append">
											<span class="input-group-text">
												<i class="la la-calendar"></i>
												</span>
											</div>
										</div>
									</div>
								<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Transport:</label>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<select style="width: 100%;" class="form-control m-select2 contact-select2" id="updatecontactVo" name="contactTransportVo.contactId"  placeholder="Select Transport">
											<option value="">Select Transport</option>
											<c:forEach items="${contactVos}" var="contactVo" varStatus="index">
												<option value="${contactVo.contactId}" name="${contactVo.companyName}"
													<c:if test="${gateVo.contactVo.contactId == contactVo.contactId }" >selected="selected" </c:if>>
													${contactVo.companyName}
												</option>
										</c:forEach>
									</select>
								</div>
								<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Vehicle No:</label>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<input type="text" class="form-control m-input" name="vehicleno" id="updatevehicleno" value="${gateVo.vehicleno}" placeholder="Vehicle No.">
									</div>
								<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Sales Order:</label>  
									<div class="col-lg-12 col-md-12 col-sm-12">
										<select style="width: 100%;" class="form-control m-select2 sales-select2" id="updatesalesVo" name="salesVo.salesId" value="${salesVo.salesId}" placeholder="Select Sales Order" >
											<option value="">Select Sales Order</option>
												<c:forEach items="${salesVos}" var="salesVo">
													<option value="${salesVo.salesId}" name="${salesVo.prefix}${salesVo.salesNo}"
													<c:if test="${gateVo.salesVo.salesId == salesVo.salesId }">selected="selected" </c:if>>
														${salesVo.prefix}${salesVo.salesNo}</option>
												</c:forEach>
										</select>
									</div>
								<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Remark:</label>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<textarea class="form-control m-input" name="remark" id="updateremark" placeholder="Remark">${gateVo.remark}</textarea>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
				<button type="button"id="updategate" class="btn btn-primary">
						Save
				</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">
					Close
				</button>
				
				</div>
			</div>
		</div>
	</div>
	
		