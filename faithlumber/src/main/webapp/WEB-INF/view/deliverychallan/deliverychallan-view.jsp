<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.croods.bssgroup.constant.Constant" %>

<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${deliveryChallanVo.prefix}${deliveryChallanVo.deliveryChallanNo} | Delivery Challan</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
				
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${deliveryChallanVo.prefix}${deliveryChallanVo.deliveryChallanNo}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/deliverychallan" class="m-nav__link">
										<span class="m-nav__link-text">Delivery Challan</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="deliverychallan_form" action="/deliverychallan/${deliveryChallanVo.deliveryChallanId}/qc/save" method="post">	
						<div class="row">
							<div class="col-lg-5 col-md-5 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet m-portlet--bordered-semi">
									<div class="m-portlet__body" >
										
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Suppplier:</span>
																		<span>
																			<a href="<%=request.getContextPath() %>/contact/${deliveryChallanVo.contactVo.type}/${deliveryChallanVo.contactVo.contactId}" class="m-link m--font-boldest" target="_blank">
																				${deliveryChallanVo.contactVo.companyName}
																			</a>
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">GSTIN:</span>
																		<span><c:if test="${empty deliveryChallanVo.contactVo.gstin}">N/A</c:if>${deliveryChallanVo.contactVo.gstin}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<c:set var="contactAddressVo" value="${deliveryChallanVo.contactVo.contactAddressVos.get(0)}"/>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply:</span>
																		<span id="lblPlaceofSupply">${contactAddressVo.stateName}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="m-section mt-3 m--margin-bottom-15">
													<%-- <c:if test="${contactAddress.isDeleted==0}"> --%>
													<h3 class="m-section__heading">Address Details</h3>
													<!-- <div class="m-divider"><span></span></div> -->
													<h5 class=""><small class="text-muted m--hide" data-address-message="">Address is not provided</small></h5>
													<div class="m-section__content" id="purchase_billing_address">
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12">																
																<h5 class=""><small class="text-muted" data-address-name="">${contactAddressVo.companyName}</small></h5>
																<p class="mb-0">
																	<span data-address-line-1="">${contactAddressVo.addressLine1}</span>
																</p>
																<p class="mb-0">
																	<span data-address-line-2="">${contactAddressVo.addressLine2}</span>
																</p>
																<p class="mb-0">
																	<span data-address-pincode="">${contactAddressVo.pinCode}</span>
																	<span data-address-city="">${contactAddressVo.cityName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																</p>
																<p class="mb-0">
																	<span data-address-state="">${contactAddressVo.stateName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																	<span data-address-country="">${contactAddressVo.countriesName}</span>
																</p>
																<p class="mb-0">
																	<span class="">
																		<i class="la la-phone align-middle"></i> 
																		<span class="" data-address-phoneno="">
																			<c:if test="${empty deliveryChallanVo.contactVo.companyMobileno}">Mobile no. is not provided</c:if>${deliveryChallanVo.contactVo.companyMobileno}
																		</span>
																	</span>
																</p>
															</div>
														</div>
													</div>
													
												<%-- </c:if> --%>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-7 col-md-7 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<c:if test="${deliveryChallanVo.status == 'Pending'}"><span class="m-badge m-badge--warning m-badge--wide">Pending</span></c:if>
												<c:if test="${deliveryChallanVo.status == 'Complete'}"><span class="m-badge m-badge--success m-badge--wide">Complete</span></c:if>
											</div>			
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item">
													<button type="button" class="btn btn-info btn-sm m-btn m-btn--custom" id="qc_btn"><span class="m--font-boldest">Quality Check</span></button>
												</li>
												<c:if test="${deliveryChallanVo.status == 'Pending'}">
												
													<li class="m-portlet__nav-item">
														<!--begin: Dropdown-->
						                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
						                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-primary m-btn m-btn--icon m-btn--icon-only">
						                                        <i class="la la-ellipsis-h"></i>
						                                    </a>
						                                    <div class="m-dropdown__wrapper">
						                                    	<span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
					                                            <div class="m-dropdown__inner">
					                                                <div class="m-dropdown__body">              
					                                                    <div class="m-dropdown__content">
					                                                        <ul class="m-nav">
					                                                            <li class="m-nav__section m-nav__section--first">
					                                                                <span class="m-nav__section-text">Quick Actions</span>
					                                                            </li>
								                                                <li class="m-nav__separator m-nav__separator--fit">
								                                                </li>
								                                                <li class="m-nav__item">
								                                                    <a href="<%=request.getContextPath() %>/deliverychallan/${deliveryChallanVo.deliveryChallanId}/edit" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																						<span><i class="flaticon-edit"></i><span>Edit</span></span>
																					</a>
								                                                    <button type="button" data-url="<%=request.getContextPath() %>/deliverychallan/${deliveryChallanVo.deliveryChallanId}/delete" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right delete-btn">
																						<span><i class="flaticon-delete-1"></i><span>Delete</span></span>
																					</button>
								                                                </li>
					                                                        </ul>
					                                                    </div>
					                                                </div>
					                                            </div>
						                                    </div>
						                                </div>
						                                <!--end: Dropdown-->
						                            </li>
						                       	</c:if>
											</ul>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Delivery Challan Date:</span>
																		<span>
																			<fmt:formatDate pattern="dd/MM/yyyy" value="${deliveryChallanVo.deliveryChallanDate}" />
																		</span>
																	</div>
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Delivery Challan No:</span>
																		<span>${deliveryChallanVo.prefix}${deliveryChallanVo.deliveryChallanNo}</span>
																	</div>
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Purchase Order No:</span>
																		<span>
																			<c:if test="${empty purchaseNo}">N/A</c:if>
																			<c:if test="${not empty purchaseNo}">
																				<a href="<%=request.getContextPath() %>/purchase/${Constant.PURCHASE_ORDER}/${deliveryChallanVo.purchaseId}" class="m-link m--font-boldest" target="_blank">
																					${purchaseNo}
																				</a>
																			</c:if>
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Reference No:</span>
																		<span>
																			${deliveryChallanVo.referenceNo}
																		</span>
																	</div>
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Received By:</span>
																		<span>
																			${deliveryChallanVo.receivedBy.employeeName}
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
										</div>
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions m-form__actions--solid p-0">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-12">
													<div class="m-widget4">
														<div class="m-widget4__item">
															<div class="m-widget4__info">
																<span class="m-widget4__title">
																	Note:
																</span><br/>
																<span class="m-widget4__ext">
																	<span class="m-widget4__number m--font-danger">
																		${deliveryChallanVo.note}
																	</span>
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12">
										
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<h3 class="m-portlet__head-text m--font-brand">
													Product Details
												</h3>
											</div>			
										</div>
									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<div class="tab-pane active" id="m_tabs_7_1" role="tabpanel">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														
														<div class="table-responsive m--margin-top-20">
															<table class="table m-table table-bordered table-hover" id="product_table">
															    
															    <thead>
															      <tr>
															        <!-- <th>#</th> -->
															        <th>#</th>
															        <th>Product</th>
															        <th>Design No.</th>
															        <th>Bale No.</th>
															        <th class="text-right">Pack</th>
															        <th class="text-right">Qty</th>
															        <th class="text-right qc-td m--hide">QC Qty</th>
															        <th class="text-right qc-td m--hide">Difference Qty</th>
															        
															      </tr>
															    </thead>
															    <tbody data-table-list="">
															    	<c:forEach items="${deliveryChallanVo.deliveryChallanItemVos}" var="deliveryChallanItemVo" varStatus="status">
																    	<tr data-table-item="${status.index}" class="">
																    		<td class="" style="width: 50px;">
																    			<span data-item-index>${status.index+1}</span>
																    		</td>
																	        <td class="" style="width: 370px;">
																	        	<div class="form-group m-form__group p-0">
																					${deliveryChallanItemVo.productVariantVo.productVo.categoryVo.categoryName} 
																					${deliveryChallanItemVo.productVariantVo.productVo.name} 
																					${deliveryChallanItemVo.productVariantVo.variantName}																					
																				</div>
																	        </td>
																	        <td class="" style="width: 270px;">
																	        	<div class="form-group m-form__group p-0">
																	        		<c:if test="${deliveryChallanItemVo.productVariantVo.productVo.haveDesignno==0}">-</c:if>
																	        		${deliveryChallanItemVo.designNo}
																	        	</div>
																	        </td>
																	        <td class="" style="width: 270px;">
																	        	<div class="form-group m-form__group p-0">
																	        		<c:if test="${deliveryChallanItemVo.productVariantVo.productVo.haveBaleno==0}">-</c:if>
																	        		${deliveryChallanItemVo.baleNo}
																	        	</div>
																	        </td>
																	         <td class="" style="width: 270px;">
																	        	<div class="form-group m-form__group p-0 text-right">
																	        		<span class="" data-item-pack="">${deliveryChallanItemVo.pack}</span>
																	        	</div>
																	        </td>
																	        <td class="" style="width: 270px;">
																	        	<div class="form-group m-form__group p-0 text-right">
																	        		<span class="" data-item-qty="">${deliveryChallanItemVo.qty}</span>
																	        		<span class="" data-item-uom="">${deliveryChallanItemVo.productVariantVo.productVo.unitOfMeasurementVo.measurementCode}</span>
																	        	</div>
																	        </td>
																	        <td class="p-0 qc-td m--hide" style="width: 270px;">
																	        	<div class="form-group m-form__group p-0">
																	        		<input type="text" class="form-control form-control m-input1 text-right" id="qcQty${status.index}" name="deliveryChallanItemVos[${status.index}].qcQty" placeholder="Pack" value="${deliveryChallanItemVo.qcQty}" onchange="setDifference(${status.index})"/>
																	        		<input type="hidden" name="deliveryChallanItemVos[${status.index}].deliveryChallanItemId" value="${deliveryChallanItemVo.deliveryChallanItemId}"/>
																	        	</div>
																	        </td>
																	        <td class="qc-td m--hide" style="width: 270px;">
																	        	<div class="form-group m-form__group p-0 text-right">
																	        		<span class="m--font-bolder" data-item-difference>0</span>
																	        	</div>
																	        </td>
																    	</tr>
																    </c:forEach>
																</tbody>
														  	</table>
														</div>
														
													</div>
													<div class="col-lg-7 col-md-7 col-sm-12 m--hide" id="rack_div">
														
														<div class="table-responsive m--margin-top-20">
															<table class="table m-table table-bordered table-hover" id="rack_table">
															    
															    <thead>
															      <tr style="background-color:#ebedf2;border-color:#ebedf2">
															        <th colspan="5">Rack Management</th>
															      </tr>
															      <tr>
															        <!-- <th>#</th> -->
															        <th>#</th>
															        <th>Product</th>
															        <th class="">Rack</th>
															        <th class="">Rack Qty</th>
															        <th></th>
															      </tr>
															    </thead>
															    <tbody data-table-list="">
															    	<tr data-table-item="template" class="m--hide">
															    		<td class="" style="width: 50px;">
															    			<span data-item-index></span>
															    		</td>
																        <td class="p-0" style="width: 370px;">
																        	<div class="form-group m-form__group p-0">
																				<select class="form-control m-input form-control-sm m-input1" name="deliveryChallanRackVos[{index}].productVariantVo.productVariantId" id="productVariantId{index}" placeholder="Select Product">
																					<option value="">Select Product</option>
																					<c:forEach items="${deliveryChallanVo.deliveryChallanItemVos}" var="deliveryChallanItemVo" varStatus="status">
																						<option value="${deliveryChallanItemVo.productVariantVo.productVariantId}">
																							${deliveryChallanItemVo.productVariantVo.productVo.categoryVo.categoryName} 
																							${deliveryChallanItemVo.productVariantVo.productVo.name} 
																							${deliveryChallanItemVo.productVariantVo.variantName}	
																							<c:if test="${deliveryChallanItemVo.productVariantVo.productVo.haveDesignno == 1}">
																								<span class="m-badge m-badge--success m-badge--wide m-badge--rounded mt-1" data-item-designno> - ${deliveryChallanItemVo.designNo}</span>
																							</c:if>
																							
																							<c:if test="${deliveryChallanItemVo.productVariantVo.productVo.haveBaleno == 1}">
																								<span class="m-badge m-badge--info m-badge--wide m-badge--rounded mt-1" data-item-baleno> - ${deliveryChallanItemVo.baleNo}</span>
																							</c:if>
																						</option>
																					</c:forEach>
																				</select>
																			</div>
																        </td>
																       	<td class="p-0" style="width: 370px;">
																        	<div class="form-group m-form__group p-0">
																			<select class="form-control m-input form-control-sm m-input1" name="deliveryChallanRackVos[{index}].rackVo.rackId" id="rackId${status.index}{index}" placeholder="Select Rack">
																				<option value="">Select Rack</option>
																				<c:forEach items="${rackVos}" var="rackVo">
																					<option value="${rackVo.rackId}">${rackVo.rackCode}</option>
																				</c:forEach>
																			</select>
																			</div>
																        </td>
																        <td class="p-0 qc-td" style="width: 270px;">
																        	<div class="form-group m-form__group p-0">
																        		<input type="text" class="form-control form-control m-input1 text-right" id="rackQty{index}" name="deliveryChallanRackVos[{index}].rackQty" placeholder="Rack Qty" value="${deliveryChallanRackVo.rackQty}"/>
																        		<input type="hidden" name="deliveryChallanRackVos[{index}].deliveryChallanRackId" value="${deliveryChallanRackVo.deliveryChallanRackId}"/>
																        	</div>
																        </td>
																        <td class="p-0" style="width: 40px;">
																        	<button type="button" data-item-remove="" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill" title="Delete"> <i class="fa fa-times"></i></button>
																        </td>
															    	</tr>
																</tbody>
																<tfoot>
																	<tr>
																		<th></th>
																		<th>
																			<div class="m-demo-icon mb-0">
																				<div class="m-demo-icon__preview">
																					<span class=""><i
																						class="flaticon-plus m--font-primary"></i></span>
																				</div>
																				<div class="m-demo-icon__class">
																					<a href="JavaScript:void(0)" data-toggel="modal" class="m-link m--font-boldest" onclick="addRackTableItem()"> Add More</a>
																				</div>
																			</div>
																		</th>
																		<th></th>
																		<th></th>
																		<th></th>
																	</tr>
																</tfoot>
															</table>
														</div>
														
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit m--hide" id="qc_footer">
										
										<div class="m-form__actions m-form__actions--solid">
											<div class="row">
												<div class="col-lg-5 col-md-5 col-sm-12">
													<div class="form-group m-form__group row">
														<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Quality Check By:</label>
														<div class="col-lg-12 col-md-12 col-sm-12">
															<div class="input-group" >
																<select class="form-control m-select2" id="qcBy" name="qcBy.employeeId" placeholder="Select Quality Check By">
																	<option value="">Select Quality Check By</option>
																	<c:forEach items="${employeeVos}" var="employeeVo">
																		<option value="${employeeVo.employeeId}" 
																			<c:if test="${employeeVo.employeeId == deliveryChallanVo.qcBy.employeeId}">selected="selected"</c:if>>
																			${employeeVo.employeeName}
																		</option>
																	</c:forEach>
																</select>
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-7 col-md-7 col-sm-12 text-right">
													<div class="form-group m-form__group row">
														<label class="col-form-label col-lg-12 col-md-12 col-sm-12">&nbsp;</label>
														<div class="col-lg-12 col-md-12 col-sm-12">
															<button type="submit" class="btn btn-brand" id="savedeliverychallan">Submit</button>
															<button class="btn btn-secondary" type="button" id="qc_cancel">Cancel</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end::Portlet-->
								
							</div>
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/script/deliverychallan/deliverychallan-view.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".delete-btn").click(function(e) {
	            
	            var u = $(this).data("url") ? $(this).data("url") : '';
	    		swal({
	                title: "Are you sure?",
	                text: "You won't be able to revert this!",
	                type: "warning",
	                showCancelButton: !0,
	                confirmButtonText: "Yes, delete it!"
	            }).then(function(e) {
	            	e.value && u != ''? window.location.href=u : console.error("CROODS: data-url undefined ")
	            })
	        });
		});
	</script>
	
</body>
<!-- end::Body -->
</html>