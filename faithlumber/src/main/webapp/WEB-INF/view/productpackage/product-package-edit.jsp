<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Edit | ${productVo.name} | Product Package</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.css">
	
	<style type="text/css">
		.select2-container{display: block;}
		/* .select2-container {
			width: 100% !important;
			
		} */
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1 {
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Edit Product Package</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/productpackage" class="m-nav__link">
										<span class="m-nav__link-text">Product Package</span>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="/productpackage/${productVo.productId}" class="m-nav__link">
										<span class="m-nav__link-text">${productVo.name}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="product_form" action="/productpackage/save" method="post">	
						<input type="hidden" name="productId" id="packageId"  value="${productVo.productId}"/>
						<input type="hidden" name="productVo.productId" id="productId" value="${productVo.productVo.productId}"/>
						<input type="hidden" name="deleted-variants" id="deleted-variants"/>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">General Details</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item" style="border: none">
																		<span class="m--regular-font-size-">Product:</span>
																		<span>${productVo.productVo.displayName}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Package Name:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="name" placeholder="Package Name" value="${productVo.name}">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
						</div>
						
						<div class="" id="variation_ids_repeater">
							<div data-repeater-list="">
								<!--begin::Portlet-->
								<div class="m-portlet" data-repeater-item>
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Variation Details
												</h3>
											</div>			
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item">
													<button type="button" data-repeater-delete="" data-toggle="modal" class="m-portlet__nav-link btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air">
														<i class="la la-close"></i>
													</button>
												</li>
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">
										<div class="row m--margin-bottom-20">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row ">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Variant Name:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="productVariantVos[{index}].variantName" id="variantName{index}" placeholder="Variant Name" value="">
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row ">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Item Code:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="productVariantVos[{index}].itemCode" id="itemCode{index}" placeholder="Item Code" value="">
													</div>
												</div>
											</div>
										</div>
										<div class="row m--margin-bottom-20">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Retailer Price:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="productVariantVos[{index}].retailerPrice" id="retailerPrice{index}" placeholder="Retailer Price" value="">
													</div>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="form-group m-form__group row ">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Wholesaler Price:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="productVariantVos[{index}].wholesalerPrice" id="wholesalerPrice{index}" placeholder="Wholesaler Price" value="">
													</div>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="form-group m-form__group row ">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Other Price:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="productVariantVos[{index}].otherPrice" id="otherPrice{index}" placeholder="Other Price" value="">
													</div>
												</div>
											</div>
										</div>
										<div class="" id="multipleProductVariantDiv" >
											<table class="table m-table table-bordered table-hover" data-variant-table="">
												<thead>
											      <tr>
											        <th>#</th>
											        <th>Product Variant</th>
											        <th>Qty</th>
											      </tr>
											    </thead>
											    <tbody data-table-list="">
											    	<tr data-table-item="template" class="m--hide">
											    		<td class="" style="width: 25px;">
											    			<label class="m-checkbox m-checkbox--bold m-checkbox--state-brand">
															<input type="checkbox" onchange="variantChange(this)">
															<span></span>
															</label>
											    		</td>
												        <td class="" style="width: 280px;">
												        	<span data-item-name=""></span>
												        	<input type="hidden" class="form-control form-control m-input1" id="parentVariantIds{index}" name="productVariantVos[{index}].parentVariantIds" disabled="disabled">
												        </td>
												        <td class=" p-0" style="width: 180px;">
												        	<div class="form-group m-form__group p-0">
												        		<div class="m-input-icon m-input-icon--right">
																<input type="text" class="form-control form-control m-input1 text-right" id="variantQtys{index}" name="productVariantVos[{index}].parentVariantQuantites" disabled="disabled">
																	<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																</div>
												        	</div>
												        </td>
												    </tr>
											    </tbody>
										  	</table>
										</div>
									</div>
								</div>
								<!--end::Portlet-->
								<c:set scope="page" var="variantIndex" value="0"/>
								<c:forEach items="${productVo.productVariantVos}" var="productVariantVo">
									<div class="m-portlet" data-repeater-item>
										<input type="hidden" name="productVariantVos[${variantIndex}].productVariantId" id="productVariantId${variantIndex}" value="${productVariantVo.productVariantId}"/>
										<div class="m-portlet__head">
											<div class="m-portlet__head-caption">
												<div class="m-portlet__head-title">
													<span class="m-portlet__head-icon">
														<i class="flaticon-cogwheel-2"></i>
													</span>
													<h3 class="m-portlet__head-text m--font-brand">
														Variation Details
													</h3>
												</div>			
											</div>
											<div class="m-portlet__head-tools">
												<!-- <ul class="m-portlet__nav">
													<li class="m-portlet__nav-item">
														<a href="#" data-repeater-delete="" data-toggle="modal" class="m-portlet__nav-link btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill m-btn--air">
															<i class="la la-close"></i>
														</a>
													</li>
												</ul> -->
											</div>
										</div>
										<div class="m-portlet__body">
											<div class="row m--margin-bottom-20">
												<div class="col-lg-6 col-md-6 col-sm-12">
													<div class="form-group m-form__group row ">
														<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Variant Name:</label>
														<div class="col-lg-12 col-md-12 col-sm-12">
															<input type="text" class="form-control m-input" name="productVariantVos[${variantIndex}].variantName" id="variantName${variantIndex}" placeholder="Variant Name" value="${productVariantVo.variantName}">
														</div>
													</div>
												</div>
												<div class="col-lg-6 col-md-6 col-sm-12">
													<div class="form-group m-form__group row ">
														<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Item Code:</label>
														<div class="col-lg-12 col-md-12 col-sm-12">
															<input type="text" class="form-control m-input" name="productVariantVos[${variantIndex}].itemCode" id="itemCode${variantIndex}" placeholder="Item Code" value="${productVariantVo.itemCode}">
														</div>
													</div>
												</div>
											</div>
											<div class="row m--margin-bottom-20">
												<div class="col-lg-4 col-md-4 col-sm-12">
													<div class="form-group m-form__group row">
														<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Retailer Price:</label>
														<div class="col-lg-12 col-md-12 col-sm-12">
															<input type="text" class="form-control m-input" name="productVariantVos[${variantIndex}].retailerPrice" id="retailerPrice${variantIndex}" placeholder="Retailer Price" value="${productVariantVo.retailerPrice}">
														</div>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12">
													<div class="form-group m-form__group row ">
														<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Wholesaler Price:</label>
														<div class="col-lg-12 col-md-12 col-sm-12">
															<input type="text" class="form-control m-input" name="productVariantVos[${variantIndex}].wholesalerPrice" id="wholesalerPrice${variantIndex}" placeholder="Wholesaler Price" value="${productVariantVo.wholesalerPrice}">
														</div>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12">
													<div class="form-group m-form__group row ">
														<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Other Price:</label>
														<div class="col-lg-12 col-md-12 col-sm-12">
															<input type="text" class="form-control m-input" name="productVariantVos[${variantIndex}].otherPrice" id="otherPrice${variantIndex}" placeholder="Other Price" value="${productVariantVo.otherPrice}">
														</div>
													</div>
												</div>
											</div>
											<div class="" id="multipleProductVariantDiv" >
												<table class="table m-table table-bordered table-hover" data-variant-table="">
													<thead>
												      <tr>
												        <th>#</th>
												        <th>Product Variant</th>
												        <th>Qty</th>
												      </tr>
												    </thead>
												    <tbody data-table-list="">
												    	<c:set scope="page" var="parentVariantQuantites" value="${fn:split(productVariantVo.parentVariantQuantites, ',')}"/>
														<c:set scope="page" var="parentVariantIndex" value="0"/>
												    	<c:forTokens var="productVariantId" items="${productVariantVo.parentVariantIds}" delims=",">
														    <tr data-table-item="${parentVariantIndex}" class="">
													    		<td class="" style="width: 25px;">
													    			<label class="m-checkbox m-checkbox--bold m-checkbox--state-brand">
																	<input type="checkbox" onchange="variantChange(this)" checked="checked">
																	<span></span>
																	</label>
													    		</td>
														        <td class="" style="width: 280px;">
														        	<span data-item-name="">
															        	<c:forEach items="${productVo.productVo.productVariantVos}" var="proVar2">
																			<c:if test="${proVar2.productVariantId == productVariantId}">${proVar2.variantName}</c:if>
																		</c:forEach>
														        	</span>
														        	<input type="hidden" class="form-control form-control m-input1" id="parentVariantIds${parentVariantIndex}" name="productVariantVos[${variantIndex}].parentVariantIds" value="${productVariantId}">
														        </td>
														        <td class=" p-0" style="width: 180px;">
														        	<div class="form-group m-form__group p-0">
														        		<div class="m-input-icon m-input-icon--right">
																		<input type="text" class="form-control form-control m-input1 text-right" id="variantQtys${parentVariantIndex}" name="productVariantVos[${variantIndex}].parentVariantQuantites" value="${parentVariantQuantites[parentVariantIndex]}" >
																			<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																		</div>
														        	</div>
														        </td>
														    </tr>
														    <c:set scope="page" var="parentVariantIndex" value="${parentVariantIndex+1}"/>
													    </c:forTokens>
												    </tbody>
											  	</table>
											</div>
										</div>
									</div>
									<c:set scope="page" var="variantIndex" value="${variantIndex+1}"/>
								</c:forEach>
							</div>
							<div class="m-demo-icon">
								<div class="m-demo-icon__preview">
									<span class=""><i class="flaticon-plus m--font-primary"></i></span>
								</div>
								<div class="m-demo-icon__class">
									<a href="#" data-toggle="modal"  data-toggel="modal" data-repeater-create="" class="m-link m--font-boldest">Add More Variation</a>
								</div>
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions--solid m-form__actions--right">
								<button type="submit" class="btn btn-brand" id="save_product">
									Submit
								</button>
								
								<a href="/productpackage/${productVo.productId}" class="btn btn-secondary">
									Cancel
								</a>
							</div>
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/script/productpackage/product-package-new-script.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			getProductInfo();
			
			$("#variation_ids_repeater").find("[data-repeater-item]").first().remove();
			//data-repeater-delete=""
		});
	</script>
	
</body>
<!-- end::Body -->
</html>