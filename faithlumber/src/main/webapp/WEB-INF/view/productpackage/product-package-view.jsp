<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${productVo.name} | Product Package</title>
	
	<style type="text/css">
		.select2-container{display: block;}
		
		.variants_table td {
			vertical-align: middle;
		}
		.variants_table img {
			width: 4rem;
			border-radius: 10%;
		}
		.active_img{
			border: 5px solid #716aca
		}
		
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${productVo.name}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/productpackage" class="m-nav__link">
										<span class="m-nav__link-text">Product Package</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">				
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<span class="m-portlet__head-icon">
												<i class="flaticon-cogwheel-2"></i>
											</span>
											<h3 class="m-portlet__head-text m--font-brand">General Details</h3>
										</div>
									</div>
									<div class="m-portlet__head-tools">
										<ul class="m-portlet__nav">
											<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
												<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl  m-dropdown__toggle">
													<!-- <i class="la la-ellipsis-v"></i> -->
													<i class="la la-ellipsis-h m--font-brand"></i>
												</a>
						                        <div class="m-dropdown__wrapper">
						                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
						                            <div class="m-dropdown__inner">
					                                    <div class="m-dropdown__body">              
					                                        <div class="m-dropdown__content">
					                                            <ul class="m-nav">
					                                                
					                                                <li class="m-nav__separator m-nav__separator--fit">
					                                                </li>
					                                                <li class="m-nav__item">
					                                                    <a href="/productpackage/${productVo.productId}/edit" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																			<span><i class="flaticon-edit"></i><span>Edit</span></span>
																		</a>
					                                                    <button data-url="/productpackage/${productVo.productId}/delete" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right delete-btn">
																			<span><i class="flaticon-delete-1"></i><span>Delete</span></span>
																		</button>
					                                                </li>
					                                            </ul>
					                                        </div>
					                                    </div>
						                            </div>
						                        </div>
											</li>
										</ul>
									</div>
								</div>
								<div class="m-portlet__body" >
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12">
											<div class="media">							 		 
											  	<div class="media-body m--padding-top-10">
											    	<span class="text-mute">Product Name:</span><br>
													<span class="display-5">${productVo.name}</span>		 
											  	</div>
											</div>
										</div>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
					</div>
					<c:forEach items="${productVo.productVariantVos}" var="productVariantVo">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Variants Details
												</h3>
											</div>			
										</div>
									</div>
									<div class="m-portlet__body">
										<div class="row  m--margin-top-20">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item" style="border: none">
																		<span class="m--regular-font-size-">Variant:</span>
																		<span>${productVariantVo.variantName}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item" style="border: none">
																		<span class="m--regular-font-size-">Item Code:</span>
																		<span>${productVariantVo.itemCode}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
										</div>
										<div class="row  m--margin-top-20">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item" style="border: none">
																		<span class="m--regular-font-size-">Retailer Price:</span>
																		<span>${productVariantVo.retailerPrice}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item" style="border: none">
																		<span class="m--regular-font-size-">Wholesaler Price:</span>
																		<span>${productVariantVo.wholesalerPrice}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item" style="border: none">
																		<span class="m--regular-font-size-">Other Price:</span>
																		<span>${productVariantVo.otherPrice}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-6 scrollmenu" >
												<table class="table m-table m-table--head-no-border variants_table">
													<c:set scope="page" var="parentVariantQuantites" value="${fn:split(productVariantVo.parentVariantQuantites, ',')}"/>
													<c:set scope="page" var="parentVariantIndex" value="0"/>
													<thead>
													  <tr>
														<th>Variant</th>
														<th>Qty</th>
													  </tr>
													</thead>
													<tbody>
														<c:forTokens var="productVariantId" items="${productVariantVo.parentVariantIds}" delims=",">
												           <tr>												        
															<td>
																<c:forEach items="${productVo.productVo.productVariantVos}" var="proVar2">
																	<c:if test="${proVar2.productVariantId == productVariantId}">${proVar2.variantName}</c:if>
																</c:forEach>
															</td>
															<td>${parentVariantQuantites[parentVariantIndex]}</td>
														  </tr> 
														  <c:set scope="page" var="parentVariantIndex" value="${parentVariantIndex+1}"/>
												       	</c:forTokens>
													</tbody>
												</table>
											</div>
										</div>
										
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
						</div>
					</c:forEach>
					
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/script/product/product-new-script.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$(document).on("click",".delete-btn",function(e) {
	            
	         	var u = $(this).data("url") ? $(this).data("url") : '';
	    	  	swal({
	                title: "Are you sure?",
	                text: "You won't be able to revert this!",
	                type: "warning",
	                showCancelButton: !0,
	                confirmButtonText: "Yes, delete it!"
	               }).then(function(e) {
	            	e.value && u != ''? window.location.href=u : console.error("CROODS: data-url undefined ")
	            			
	            })
	        });
		});
	</script>
	
</body>
<!-- end::Body -->
</html>