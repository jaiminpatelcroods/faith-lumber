<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${complainVo.prefix}${complainVo.complainNo} | Complain</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		/* .m-table tr.m-table__row--brand span {
			color: #fff !important;
		} */
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		/* table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		} */
		
		
		
		.card-container {
		  cursor: pointer;
		  height: 100%;
		  perspective: 600;
		  position: relative;
		  width: 55px;
		}
		.card {
		  height: 100%;
		  position: absolute;
		  transform-style: preserve-3d;
		  transition: all 1s ease-in-out;
		  width: 100%;
		}
		.card-hover {
		  transform: rotateY(180deg);
		}
		.card-hover .card-btn {
			/* display: none; */		
		}
		.card .side {
		  backface-visibility: hidden;
		  /* border-radius: 6px; */
		  height: 100%;
		  position: absolute;
		  overflow: hidden;
		  width: 100%;
		}
		.card .back {
		   background: #eaeaed !important;
		  /*color: #0087cc;
		  line-height: 150px; */
		  /* text-align: center; */
		  transform: rotateY(180deg);
		}
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${complainVo.prefix}${complainVo.complainNo}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/complain" class="m-nav__link">
										<span class="m-nav__link-text">Complain</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="complain_form" action="/complain/save" method="post">	
						
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet" style="min-height: 484.05px">
									<div class="m-portlet__body" >
									<!-- <div class="card-container">
											  <div class="card" id="card">
											    <div class="side"><button class="btn btn-success card-btn" type="button" id="btn_per" style="border-top-right-radius: 0;border-bottom-right-radius: 0">Go!</button></div>
											    <div class="side back"><button class="btn btn-danger" type="button">GO!</button></div>
											  </div>
											</div> -->
									
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Customer:</span>
																		<span>
																			<a href="<%=request.getContextPath() %>/contact/${complainVo.contactVo.type}/${complainVo.contactVo.contactId}" class="m-link m--font-boldest" target="_blank">
																				${complainVo.contactVo.companyName}
																			</a>
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">GSTIN:</span>
																		<span id="lblContactGSTIN"><c:if test="${empty complainVo.contactVo.gstin}">N/A</c:if>${complainVo.contactVo.gstin}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="m-section mt-3 m--margin-bottom-15">
													<%-- <c:if test="${contactAddress.isDeleted==0}"> --%>
													<h3 class="m-section__heading">Address</h3>
													<!-- <div class="m-divider"><span></span></div> -->
													<h5 class=""><small class="text-muted m--hide" data-address-message="">Address is not provided</small></h5>
													<c:set var="contactAddressVo" value="${complainVo.contactVo.contactAddressVos.get(0)}"/>
													<div class="m-section__content" id="purchase_billing_address">
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12">																
																<h5 class=""><small class="text-muted" data-address-name="">${contactAddressVo.companyName}</small></h5>
																<p class="mb-0">
																	<span data-address-line-1="">${contactAddressVo.addressLine1}</span>
																</p>
																<p class="mb-0">
																	<span data-address-line-2="">${contactAddressVo.addressLine2}</span>
																</p>
																<p class="mb-0">
																	<span data-address-pincode="">${contactAddressVo.pinCode}</span>
																	<span data-address-city="">${contactAddressVo.cityName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																</p>
																<p class="mb-0">
																	<span data-address-state="">${contactAddressVo.stateName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																	<span data-address-country="">${contactAddressVo.countriesName}</span>
																</p>
																<p class="mb-0">
																	<span class="">
																		<i class="la la-phone align-middle"></i> 
																		<span class="" data-address-phoneno="">
																			<c:if test="${empty complainVo.contactVo.companyMobileno}">Mobile no. is not provided</c:if>${complainVo.contactVo.companyMobileno}
																		</span>
																	</span>
																</p>
																
															</div>
														</div>
													</div>
													
												<%-- </c:if> --%>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-8 col-md-8 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet" style="min-height: 484.05px">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
											<c:if test="${complainVo.status == 'pending'}"><span class="m-badge m-badge--warning m-badge--wide">Pending</span></c:if>
											<c:if test="${complainVo.status == 'complete'}"><span class="m-badge m-badge--success m-badge--wide">Complete</span></c:if>
											<c:if test="${complainVo.status == 'inprogress'}"><span class="m-badge m-badge--info m-badge--wide">In Progress</span></c:if>
											</div>			
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item">
													<c:if test="${complainVo.status =='inprogress'}">
														<button type="button" class="btn btn-info btn-sm m-btn m-btn--custom" id="qc_btn" data-toggle="modal" data-target="#closecomplain_new_modal"><span class="m--font-boldest">Close Complain</span></button>
													</c:if>
												</li>
												<li class="m-portlet__nav-item">
													<!--begin: Dropdown-->
					                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
					                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-primary m-btn m-btn--icon m-btn--icon-only">
					                                        <i class="la la-ellipsis-h"></i>
					                                    </a>
					                                    <div class="m-dropdown__wrapper">
					                                            <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
					                                            <div class="m-dropdown__inner">
					                                                <div class="m-dropdown__body">              
					                                                    <div class="m-dropdown__content">
					                                                        <ul class="m-nav">
					                                                        	<c:if test="${complainVo.status== 'pending'}">
					                                                        		<li class="m-nav__item">
						                                                           		<a href="#" data-toggle="modal" data-target="#assignedemployee_new_modal" class="m-nav__link">
						                                                           			<i class="m-nav__link-icon fa fa-file-assigneEmployee"></i>
						                                                           			<span class="m-nav__link-text">Assigne Employee</span>
						                                                           		</a>
						                                                           	</li>
					                                                        	</c:if>
								                                                <li class="m-nav__separator m-nav__separator--fit">
								                                                </li>
								                                                <li class="m-nav__item">
								                                                    <a href="<%=request.getContextPath() %>/complain/${complainVo.complainId}/edit" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																						<span><i class="flaticon-edit"></i><span>Edit</span></span>
																					</a>
								                                                    <button type="button" data-url="<%=request.getContextPath() %>/complain/${complainVo.complainId}/delete" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right delete-btn">
																						<span><i class="flaticon-delete-1"></i><span>Delete</span></span>
																					</button>
								                                                </li>
					                                                           
					                                                        </ul>
					                                                    </div>
					                                                </div>
					                                            </div>
					                                    </div>
					                                </div>
					                                <!--end: Dropdown-->
					                            </li>
												
											</ul>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Complain Date:</span>
																		<span>
																			<fmt:formatDate pattern="dd/MM/yyyy" value="${complainVo.complainDate}" />
																		</span>
																	</div>
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Complain No:</span>
																		<span>${complainVo.prefix}${complainVo.complainNo}</span>
																	</div>
																	
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Case Channel:</span>
																		<span>
																			${complainVo.caseChannel}
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Due Date:</span>
																		<span>
																			<c:if test="${empty complainVo.dueDate}">N/A</c:if>
																			<c:if test="${not empty complainVo.dueDate}"><fmt:formatDate pattern="dd/MM/yyyy" value="${complainVo.dueDate}" /></c:if>
																		</span>
																	</div>
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Case Type:</span>
																		<span>${complainVo.caseTypeVo.caseName}</span>
																	</div>
																	<c:if test="${not empty complainVo.assignedEmployee}">
																		<div class="m-widget28__tab-item">
																			<span class="m--regular-font-size-">Assigned Employee:</span>
																			<span>
																				${complainVo.assignedEmployee.employeeName}
																			</span>
																		</div>
																	</c:if>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<c:if test="${not empty complainVo.caseTypeVo.caseDescription}">
												<div class="col-lg-12 col-md-12 col-sm-12" id="case_type_div">
													<div class="alert m-alert--default " role="alert">
												  		<strong>Case Type:</strong><br/>
												  		<span>${complainVo.caseTypeVo.caseDescription}</span>
													</div>
												</div>
											</c:if>
											<c:if test="${complainVo.status == 'complete'}">
												<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-10">
													<!--begin::Section-->
													<div class="m-section">
														<h3 class="m-section__heading">Close Complain Details</h3>
														<div class="m-section__content">
															<div class="row">
																<div class="col-lg-6 col-md-6 col-sm-12">
																	<table class="table m-table">
																	  	<tbody>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Completed Date:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12"><c:if test="${not empty complainVo.completedDate}"><fmt:formatDate pattern="dd/MM/yyyy" value="${complainVo.completedDate}" /></c:if></td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Remark:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${complainVo.remark}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Corrective Action:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${complainVo.correctiveActions}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Preventive Action:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${complainVo.preventiveAction}</td>
																	    	</tr>
																	  	</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div>
													<!--end::Section-->
												</div>
											</c:if>
										</div>
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions m-form__actions--solid p-0">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-12">
													<div class="m-widget4">
														<div class="m-widget4__item">
															<div class="m-widget4__info">
																<span class="m-widget4__title">
																	Remark
																</span><br/>
																<span class="m-widget4__ext">
																	<span class="m-widget4__number m--font-danger">
																		${complainVo.remark}
																	</span>
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<div class="modal fade" id="assignedemployee_new_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							 Assigned Employee
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
					</div>
					<form id="assignedemployee_form" action="/complain/${complainVo.complainId}/update/assignedemployee" method="post">
						<div class="modal-body">
							<div class="form-group m-form__group row">
								<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Employee:</label>
								<div class="col-lg-12 col-md-12 col-sm-12">
									<div class="input-group" >
										<select class="form-control m-select2" id="employeeId" name="assignedEmployee" placeholder="Select Employee">
											<option value="">Select Employee</option>
											<c:forEach items="${employeeVos}" var="employeeVo">
												<option value="${employeeVo.employeeId}">${employeeVo.employeeName}</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit"id="saveassignedemployee" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!---------END OF ASSIGNED NEW MODAL--------------->
		
		<div class="modal fade" id="closecomplain_new_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							 Close Complain
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
					</div>
					<form id="closecomplain_form" action="/complain/${complainVo.complainId}/update/closecomplain" method="post">
						<div class="modal-body">
							<div class="input-group" >
								<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Completed Date:</label>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<div class="input-group date" >
												<input type="text" class="form-control m-input todaybtn-datepicker" name="completedDate" readonly id="completedDate" data-date-format="dd/mm/yyyy"
													data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'/>
												<div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar"></i>
													</span>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Remark:</label>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<textarea class="form-control m-input" name="remark" placeholder="Remark"></textarea>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Corrective Action:</label>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<textarea class="form-control m-input" name="correctiveActions" placeholder="Corrective Action"></textarea>
										</div>
									</div>
									<div class="form-group m-form__group row">
										<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Preventive Action:</label>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<textarea class="form-control m-input" name="preventiveAction" placeholder="Preventive Action"></textarea>
										</div>
									</div>
 								</div>
							</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit"id="saveclosecomplain" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/script/complain/complain-view-script.js" type="text/javascript"></script>
	<script type="text/javascript">
		
		$(document).ready(function(){
			
			$(".delete-btn").click(function(e) {
	            
	            var u = $(this).data("url") ? $(this).data("url") : '';
	    		swal({
	                title: "Are you sure?",
	                text: "You won't be able to revert this!",
	                type: "warning",
	                showCancelButton: !0,
	                confirmButtonText: "Yes, delete it!"
	            }).then(function(e) {
	            	e.value && u != ''? window.location.href=u : console.error("CROODS: data-url undefined ")
	            })
	        });
		});
		
	</script>
	
</body>
<!-- end::Body -->
</html>