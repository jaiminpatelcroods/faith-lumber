<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${jobworkVo.prefix}${jobworkVo.jobworkNo} | Jobwork</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		/* .m-table tr.m-table__row--brand span {
			color: #fff !important;
		} */
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
		.m-wizard.m-wizard--3 .m-wizard__head {
		    padding: 2rem 1rem !important;
		}
		
		.m-wizard.m-wizard--3 .m-wizard__form {
			padding: 2rem 4rem 3rem 4rem;
		}
		
	</style>
	
	<c:if test="${!isEditable}">
		<style type="text/css">
			select[readonly].select2-hidden-accessible + .select2-container {
			  pointer-events: none;
			  touch-action: none;
			}
		</style>
	</c:if>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${jobworkVo.prefix}${jobworkVo.jobworkNo}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/jobwork" class="m-nav__link">
										<span class="m-nav__link-text">Jobwork</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<!-- <form class="m-form m-form--state m-form--fit m-form--label-align-left" id="jobwork_form" action="/jobwork/save" method="post"> -->	
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									
									<div class="m-portlet__body" >
										<!--begin: Form Wizard-->
										<div class="m-wizard m-wizard--3 m-wizard--success" id="m_wizard">
								
											<!--begin: Message container -->
										    <div class="m-portlet__padding-x">
										        <!-- Here you can put a message or alert -->
										    </div>
										    <!--end: Message container -->
								
											<div class="row m-row--no-padding">
												<div class="col-xl-3 col-lg-12">
													<!--begin: Form Wizard Head -->
													<div class="m-wizard__head">
								
														<!--begin: Form Wizard Progress -->  		
														<div class="m-wizard__progress">	
															<div class="progress">		 
																<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>						 	
															</div>			 
														</div> 
											            <!--end: Form Wizard Progress --> 
								
											            <!--begin: Form Wizard Nav -->
														<%@include file="jobwork-step-nav.jsp" %>
														<div class="m-wizard__nav">
															<div class="m-wizard__steps ">
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_1">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step1_form').submit()">							 
																			<span><span>1</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Antri And Larring
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_2" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step2_form').submit()">							 
																			<span><span>2</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Farmo Type
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_3" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step3_form').submit()">							 
																			<span><span>3</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Stitching
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_4" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step4_form').submit()">							 
																			<span><span>4</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Measurement QC
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_5" > 
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step5_form').submit()">							 
																			<span><span>5</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Bal / Gaj
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_6" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step6_form').submit()">							 
																			<span><span>6</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Washing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_7">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number"  onclick="$('#goto_step7_form').submit()">							 
																			<span><span>7</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Measurement QC
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_8" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step8_form').submit()">							 
																			<span><span>8</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Pressing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_9" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step9_form').submit()">							 
																			<span><span>9</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Labeling / Finishing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_10">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step10_form').submit()">							 
																			<span><span>10</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Store
																		</div>
																	</div>
																</div>
															</div>
														</div>	
														<!--end: Form Wizard Nav -->
													</div>
													<!--end: Form Wizard Head -->	
												</div>
												<div class="col-xl-9 col-lg-12">
													<!--begin: Form Wizard Form-->
													<div class="m-wizard__form">
														<!--
															1) Use m-form--label-align-left class to alight the form input lables to the right
															2) Use m-form--state class to highlight input control borders on form validation
														-->
														<form class="m-form m-form--label-align-left- m-form--state-" id="form_step9" method="post" action="/jobwork/save">
															<input type="hidden" id="jobworkId" name="jobworkId" value="${jobworkVo.jobworkId}"/>
															<input type="hidden" id="deleteJobworkProcessVos" name="deleteJobworkProcessVos" value=""/>
															<input type="hidden" id="process" name="process" value="${Constant.JOBWORK_PROCESS_LABELING_FINISHING}"/>
															
															<!--begin: Form Body -->
															<div class="m-portlet__body m-portlet__body--no-padding">
																<!--begin: Form Wizard Step 3-->
																<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_3">
																	<div class="m-form__section m-form__section--first">
																		<div class="m-form__heading">
																			<h3 class="m-form__heading-title">Labeling Finishing Process</h3>
																		</div>
																		
																		<div class="table-responsive m--margin-top-20">
																			<table class="table m-table table-bordered table-hover" id="labeling_process_table">
																			    <thead>
																			      <tr>
																			        <!-- <th>#</th> -->
																			        <th>#</th>
																			        <th>Labeling Finishing Particular</th>
																			        <th>Employee </th>
																			        <th class="text-right">Qty</th>
																			        <th class="text-right">Rate</th>
																			        <th></th>
																			        
																			      </tr>
																			    </thead>
																			    <tbody data-table-list="">
																			    	<tr data-table-item="template" class="m--hide">
																			    		<td class="" style="width: 50px;">
																			    			<span data-item-index></span>
																			    		</td>
																				        <td class="p-0" style="width: 370px;">
																				        	<div class="form-group m-form__group p-0">
																								<select class="form-control m-input form-control-sm m-input1" name="jobworkProcessVos[{index}].productionProcessVo.productionProcessId" id="productionProcessId{index}" placeholder="Select Stitching Particular">
																									<option value="">Select Labeling Finishing Particular</option>
																									<c:forEach items="${productionProcessVos}" var="productionProcessVo">
																										<option value="${productionProcessVo.productionProcessId}">${productionProcessVo.productionProcessName}</option>
																									</c:forEach>
																								</select>
																							</div>
																				        </td>
																				        <td class="p-0" style="width: 270px;">
																				        	<div class="form-group m-form__group p-0">
																								<select class="form-control m-input form-control-sm m-input1" name="jobworkProcessVos[{index}].employeeVo.employeeId" id="employeeId{index}" placeholder="Select Employee">
																									<option value="">Select Employee</option>
																									<c:forEach items="${employeeVos}" var="employeeVo">
																										<option value="${employeeVo.employeeId}">
																											${employeeVo.employeeName}
																										</option>
																									</c:forEach>
																								</select>
																							</div>
																				        </td>
																				        <td class="p-0" style="width: 270px;">
																				        	<div class="form-group m-form__group p-0">
																				        		<div class="m-input-icon m-input-icon--right">
																								<input type="text" class="form-control form-control m-input1 text-right" id="qty{index}" name="jobworkProcessVos[{index}].qty" placeholder="Qty" value="">
																									<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																								</div>
																				        	</div>
																				        </td>
																				        <td class="p-0" style="width: 270px;">
																				        	<div class="form-group m-form__group p-0">
																				        		<div class="m-input-icon m-input-icon--right">
																									<input type="text" class="form-control form-control m-input1 text-right" id="rate{index}" name="jobworkProcessVos[{index}].rate" placeholder="Rate" value="">
																									<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																								</div>
																				        	</div>
																				        </td>
																				        <td class="p-0" style="width: 40px;">
																				        	<button type="button" data-item-remove="" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill" title="Delete"> <i class="fa fa-times"></i></button>
																				        </td>
																			    	</tr>
																			    	<c:set scope="page" var="tableIndex" value="0"/>
																				    <c:forEach items="${jobworkVo.jobworkProcessVos}" var="jobworkProcessVo">
																				    
																				    	<tr data-table-item="${tableIndex}" class="">
																				    		<td class="" style="width: 50px;">
																				    			<span data-item-index></span>
																				    		</td>
																					        <td class="p-0" style="width: 370px;">
																					        	<div class="form-group m-form__group p-0">
																									<select class="form-control m-input form-control-sm m-input1 m-select2" name="jobworkProcessVos[${tableIndex}].productionProcessVo.productionProcessId" id="productionProcessId${tableIndex}" placeholder="Select Stitching Particular">
																										<option value="">Select Labeling Finishing Particular</option>
																										<c:forEach items="${productionProcessVos}" var="productionProcessVo">
																											<option value="${productionProcessVo.productionProcessId}"
																												<c:if test="${jobworkProcessVo.productionProcessVo.productionProcessId == productionProcessVo.productionProcessId}">selected="selected"</c:if>>
																												${productionProcessVo.productionProcessName}
																											</option>
																										</c:forEach>
																									</select>
																								</div>
																					        </td>
																					        <td class="p-0" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																									<select class="form-control m-input form-control-sm m-input1 m-select2" name="jobworkProcessVos[${tableIndex}].employeeVo.employeeId" id="employeeId${tableIndex}" placeholder="Select Employee">
																										<option value="">Select Employee</option>
																										<c:forEach items="${employeeVos}" var="employeeVo">
																											<option value="${employeeVo.employeeId}"
																												<c:if test="${jobworkProcessVo.employeeVo.employeeId == employeeVo.employeeId}">selected="selected"</c:if>>
																												${employeeVo.employeeName}
																											</option>
																										</c:forEach>
																									</select>
																								</div>
																					        </td>
																					        <td class="p-0" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																					        		<div class="m-input-icon m-input-icon--right">
																									<input type="text" class="form-control form-control m-input1 text-right" id="qty${tableIndex}" name="jobworkProcessVos[${tableIndex}].qty" placeholder="Qty" value="${jobworkProcessVo.qty}">
																										<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																									</div>
																					        	</div>
																					        </td>
																					        <td class="p-0" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																					        		<div class="m-input-icon m-input-icon--right">
																										<input type="text" class="form-control form-control m-input1 text-right" id="rate${tableIndex}" name="jobworkProcessVos[${tableIndex}].rate" placeholder="Rate" value="${jobworkProcessVo.rate}">
																										<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																									</div>
																					        	</div>
																					        </td>
																					        <td class="p-0" style="width: 40px;">
																					        	<button type="button" data-item-remove="" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill not-view" title="Delete"> <i class="fa fa-times"></i></button>
																					        	<input type="hidden" name="jobworkProcessVos[${tableIndex}].jobworkProcessId" id="jobworkProcessId${tableIndex}" value="${jobworkProcessVo.jobworkProcessId}"/>
																					        </td>
																				    	</tr>
																				    	<c:set scope="page" var="tableIndex" value="${tableIndex+1}"/>
																				    </c:forEach>
																				</tbody>
																				<tfoot class="not-view">
																			      <tr>
																			        <th></th>
																			        <th>
																			        	<div class="m-demo-icon mb-0">
																							<div class="m-demo-icon__preview">
																								<span class=""><i class="flaticon-plus m--font-primary"></i></span>
																							</div>
																							<div class="m-demo-icon__class">
																							<a href="JavaScript:void(0)" data-toggel="modal" class="m-link m--font-boldest" onclick="addTableItem()"> Add More</a>
																							</div>
																						</div>
																			        </th>
																			        <th></th>
																			        <th></th>
																			        <th><span class="m--font-boldest float-right" id=""></span></th>
																			        <th></th>
																			      </tr>
																			    </tfoot>
																		  	</table>
																		</div>
																	</div>
																</div>
																<!--end: Form Wizard Step 3-->
															</div>
															<!--end: Form Body -->
															<!--begin: Form Actions -->
															<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
																<div class="m-form__actions">
																	<div class="row">
																		<div class="col-lg-6 m--align-left">
																			<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" onclick="$('#previous_form').submit()">
																				<span>
																				<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																				<span>Back</span>
																				</span>
																			</button>
																		</div>
																		<div class="col-lg-6 m--align-right">
																			<c:if test='${not empty jobworkVo.labelingFinishingQC}'>
																				<button type="button" class="btn btn-primary m-btn m-btn--custom m-btn--icon" id="quality_check">
																					<span><i class="la la-check"></i>&nbsp;&nbsp;<span>Quality Check</span></span>
																				</button>
																			</c:if>
																			<c:if test='${empty jobworkVo.labelingFinishingQC || jobworkVo.labelingFinishingQC == "pending"}'>
																			<button type="submit" id="form_save_step9" class="btn btn-success m-btn m-btn--custom m-btn--icon not-view" data-wizard-action="next">
																				<span><span>Save & Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																			</button>
																			</c:if>
																			<c:if test="${isEditable == false}">
																				<a href="/jobwork/${jobworkVo.jobworkId}" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
																					<span>
																						<span>Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																				</a>
																			</c:if>
																		</div>
																	</div>
																</div>
															</div>
															<!--end: Form Actions -->
														</form>
														<form class="m-form m-form--label-align-left- m-form--state- m--hide" id="form_step9_qc" method="post" action="/jobwork/save">
															
															<input type="hidden" id="jobworkId" name="jobworkId" value="${jobworkVo.jobworkId}"/>
															<input type="hidden" id="process" name="process" value="${Constant.JOBWORK_PROCESS_LABELING_FINISHING}"/>
															<input type="hidden" id="labelingFinishingQC" name="labelingFinishingQC" value="${jobworkVo.labelingFinishingQC}"/>
															
															<!--begin: Form Body -->
															<div class="m-portlet__body m-portlet__body--no-padding">
																<!--begin: Form Wizard Step 3-->
																<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_3">
																	<div class="m-form__section m-form__section--first">
																		<div class="m-form__heading">
																			<h3 class="m-form__heading-title">Labeling Finishing Process Quality Check</h3>
																		</div>
																		
																		<div class="table-responsive m--margin-top-20" >
																			<table class="table m-table table-bordered table-hover" style="display: inline-block; width: 1400px" id="stitching_quality_check_table">
																			    <thead>
																			      <tr>
																			        <!-- <th>#</th> -->
																			        <th>#</th>
																			        <th>Labeling Finishing Particular</th>
																			        <th>Employee </th>
																			        <th>Qty</th>
																			        <th class="text-right">Rate</th>
																			        <th class="text-right">Qty</th>
																			        <th class="text-right qc-td">Pass Qty</th>
																			        <th class="text-right qc-td">Fail Qty</th>
																			        <th class="qc-td">Remark</th>
																			        <th class="qc-td">QC By</th>
																			      </tr>
																			    </thead>
																			    <tbody data-table-list="">
																			    	<c:set scope="page" var="qcTableIndex" value="0"/>
																				    <c:forEach items="${jobworkVo.jobworkProcessVos}" var="jobworkProcessVo">
																				    	<tr data-table-item="${qcTableIndex}" class="">
																				    		<td class="" style="width: 50px;">
																				    			<span data-item-index>${qcTableIndex+1}</span>
																				    		</td>
																					        <td class="" style="width: 370px;">
																					        	<div class="form-group m-form__group p-0">
																									${jobworkProcessVo.productionProcessVo.productionProcessName}
																								</div>
																					        </td>
																					        <td class="" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																					        		${jobworkProcessVo.employeeVo.employeeName}
																					        	</div>
																					        </td>
																					        <td class="" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																					        		${jobworkProcessVo.qty}
																					        	</div>
																					        </td>
																					         <td class="" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0 text-right">
																					        		${jobworkProcessVo.rate}
																					        	</div>
																					        </td>
																					       <td class="p-0 qc-td" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																					        		<input type="text" class="form-control form-control m-input1 text-right" id="qcQty${qcTableIndex}" name="jobworkProcessVos[${qcTableIndex}].qcQty" placeholder="QC Qty" value="${jobworkProcessVo.qcQty}" onchange="changeQty(${qcTableIndex})"/>
																					        	</div>
																					        </td>
																					        <td class="p-0 qc-td" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																					        		<input type="text" class="form-control form-control m-input1 text-right" id="qcPassQty${qcTableIndex}" name="jobworkProcessVos[${qcTableIndex}].qcPassQty" placeholder="Pass Qty" value="${jobworkProcessVo.qcPassQty}" onchange="changeQty(${qcTableIndex})"/>
																					        	</div>
																					        </td>
																					        <td class="p-0 qc-td" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																					        		<input type="text" class="form-control form-control m-input1 text-right" id="qcFailQty${qcTableIndex}" name="jobworkProcessVos[${qcTableIndex}].qcFailQty" placeholder="Fail Qty" value="${jobworkProcessVo.qcFailQty}" onchange="changeQty(${qcTableIndex})"/>
																					        	</div>
																					        </td>
																					        <td class="p-0 qc-td" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																					        		<input type="text" class="form-control form-control m-input1 text-right" id="remark${qcTableIndex}" name="jobworkProcessVos[${qcTableIndex}].remark" placeholder="Remark" value="${jobworkProcessVo.remark}" />
																					        	</div>
																					        </td>
																					        <td class="p-0 qc-td" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																									<select class="form-control m-input form-control-sm m-select2 m-input1" name="jobworkProcessVos[${qcTableIndex}].qcBy.employeeId" id="qcBy${qcTableIndex}" placeholder="Select Employee">
																										<option value="">Select Employee</option>
																										<c:forEach items="${employeeVos}" var="employeeVo">
																											<option value="${employeeVo.employeeId}"
																												<c:if test="${employeeVo.employeeId == jobworkProcessVo.qcBy.employeeId}">selected="selected"</c:if>>
																												${employeeVo.employeeName}
																											</option>
																										</c:forEach>
																									</select>
																								</div>
																								<input type="hidden" name="jobworkProcessVos[${qcTableIndex}].jobworkProcessId" value="${jobworkProcessVo.jobworkProcessId}"/>
																								<input type="hidden" name="jobworkProcessVos[${qcTableIndex}].productionProcessVo.productionProcessId" value="${jobworkProcessVo.productionProcessVo.productionProcessId}"/>
																								<input type="hidden" name="jobworkProcessVos[${qcTableIndex}].process" value="${jobworkProcessVo.process}"/>
																								<input type="hidden" name="jobworkProcessVos[${qcTableIndex}].employeeVo.employeeId" value="${jobworkProcessVo.employeeVo.employeeId}"/>
																								<input type="hidden" name="jobworkProcessVos[${qcTableIndex}].qty" value="${jobworkProcessVo.qty}"/>
																								<input type="hidden" name="jobworkProcessVos[${qcTableIndex}].rate" value="${jobworkProcessVo.rate}"/>
																					        </td>
																					        <c:set scope="page" var="qcTableIndex" value="${qcTableIndex+1}"/>
																				    	</tr>
																				    </c:forEach>
																				</tbody>
																		  	</table>
																		</div>
																	</div>
																</div>
																<!--end: Form Wizard Step 3-->
															</div>
															<!--end: Form Body -->
															<!--begin: Form Actions -->
															<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
																<div class="m-form__actions">
																	<div class="row">
																		<div class="col-lg-6 m--align-left">
																			<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" onclick="$('#previous_form').submit()">
																				<span>
																				<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																				<span>Back</span>
																				</span>
																			</button>
																		</div>
																		<div class="col-lg-6 m--align-right">
																			<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" id="quality_check_cancel">
																				<span>Cancel</span>
																			</button>
																			<c:if test='${jobworkVo.labelingFinishingQC != "complete"}'>
																				<button type="submit" id="form_save_step9_qc" class="btn btn-success m-btn m-btn--custom m-btn--icon not-view" data-wizard-action="next">
																					<span><span>Save & Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																				</button>
																			</c:if>
																		</div>
																	</div>
																</div>
															</div>
															<!--end: Form Actions -->
														</form>
														<form action="/jobwork/${jobworkVo.jobworkId}" method="post" id="previous_form">
															<input type="hidden" name="previousProcess" value="${Constant.JOBWORK_PROCESS_PRESSING}"/>
														</form>
													</div>
													<!--end: Form Wizard Form-->
								
												</div>
											</div>
										</div>
										<!--end: Form Wizard-->
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
						</div>
						
					<!-- </form> -->
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	<script type="text/javascript">
	
		if(!${isEditable}) {
			$(".not-view").remove();
			$(".default-datetimepicker").removeClass("default-datetimepicker");
			$.each($('input, select ,textarea', '#form_step9,#form_step9_qc'),function(k){
				$(this).attr("readonly", "true")
			});
		}
	</script>
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	<%-- <script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/wizard/wizard.js" type="text/javascript"></script> --%>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<%-- <script src="<%=request.getContextPath()%>/script/Jobwork/Jobwork-script.js" type="text/javascript"></script> --%>
	
	<script type="text/javascript">
		
		var qtyValidator = {
			validators : {
				notEmpty : {
					message : 'The Qty is required'
				},
				stringLength : {
					max : 20,
					message : 'The Qty must be less than 20 characters long'
				},
				regexp : {
					regexp:/^((\d+)((\.\d{0,4})?))$/,
					message : 'The Qty is invalid'
				}
			}
		},
		passQtyValidator = {
			validators : {
				notEmpty : {
					message : 'The Qty is required'
				},
				stringLength : {
					max : 20,
					message : 'The Qty must be less than 20 characters long'
				},
				regexp : {
					regexp:/^((\d+)((\.\d{0,4})?))$/,
					message : 'The Qty is invalid'
				},
				callback: {
 						message: 'The sum of Pass and Fail Qty should be equals to Qty',
 						callback: function(value, validator, $field) {
 							
 							var id = $field.closest("tr").attr("data-table-item");
 							
 							if ($("#qcQty"+id).val() != (parseFloat($("#qcPassQty"+id).val())+parseFloat($("#qcFailQty"+id).val())))
 							{
 								return false
 							}
 							return true
 						}
 					},
			}
		},
		processValidator = {
			validators : {
				notEmpty : {
					message : 'Process is required'
				},
			}
		},
		employeeValidator = {
			validators : {
				notEmpty : {
					message : 'Employee is required'
				}
			}
		},rateValidator = {
			validators : {
				notEmpty : {message : 'The rate is required'},
				stringLength : {max : 20,message : 'The rate must be less than 20 characters long'},
				regexp : {regexp:/^((\d+)((\.\d{0,2})?))$/,message : 'The rate is invalid'}
			}
		};
	
		
		
		var tableIndex = 0;
		
		$(document).ready(function(){
			
			setTableIndex(${tableIndex});
			
			$("#form_step9").formValidation({
				framework : 'bootstrap',
				live:'disabled',
				excluded : ":disabled",
				icon : null,
				button: {
					selector: "#form_save_step9",
					disabled: "disabled"
				},
				fields : {
					
				}
			});
			
			$("#labeling_process_table").on("click",'button[data-item-remove]',function(e) {
			
				e.preventDefault();
				var i = $(this).closest("[data-table-item]").attr("data-table-item");
				
				if($("#jobworkProcessId"+i).val() != undefined ) {
					$("#deleteJobworkProcessVos").val($("#deleteJobworkProcessVos").val()+$("#jobworkProcessId"+i).val()+ ",");
				}
				
				$('#form_step9').formValidation('removeField',"jobworkProcessVos["+i+"].productionProcessVo.productionProcessId");
				$('#form_step9').formValidation('removeField',"jobworkProcessVos["+i+"].employeeVo.employeeId");
				$('#form_step9').formValidation('removeField',"jobworkProcessVos["+i+"].qty");
				$('#form_step9').formValidation('removeField',"jobworkProcessVos["+i+"].rate"); 
				$(this).closest("[data-table-item]").remove();
				
				setSrNo();
			});
			
			$("#form_save_step9").on("click", function() {
				
				if ($('#form_step9').data('formValidation').isValid() == null) {
					$('#form_step9').data('formValidation').validate();
				}
				
				if($("#labeling_process_table").find("[data-table-item]").not(".m--hide").length == 0) {
					toastr.error("","add minimum one Labeling/Finishing Process");
					return false;
				}
				
				if($('#form_step9').data('formValidation').isValid() == true) {
					$("#labeling_process_table").find("[data-table-item='template']").remove();
					
					$.each($('input, select ,textarea', '#form_step9'),function(k){
					    if($("#"+$(this).attr('id')).val() == "" || $("#"+$(this).attr('id')).val() == "undefined"){
							$("#"+$(this).attr('id')).attr("disabled", "disabled")
						}
					});
					
					$('#form_step9_qc').remove();
				} else {
					return false;
				}
				
			});
			
			var $tableItem=$("#labeling_process_table").find("[data-table-item]").not(".m--hide");
			
			$tableItem.each(function () {
				var i = $(this).attr("data-table-item");
				$('#form_step9').formValidation('addField',"jobworkProcessVos["+i+"].productionProcessVo.productionProcessId", processValidator);
				$('#form_step9').formValidation('addField',"jobworkProcessVos["+i+"].employeeVo.employeeId", employeeValidator);
				$('#form_step9').formValidation('addField',"jobworkProcessVos["+i+"].qty", qtyValidator);
				$('#form_step9').formValidation('addField',"jobworkProcessVos["+i+"].rate", rateValidator);
				
			});
			
			setSrNo();
	  		
		});
		
		function addTableItem() {
			
			var	$tableItemTemplate = $("#labeling_process_table").find("[data-table-item='template']").clone();
			
			$tableItemTemplate.removeClass("m--hide").attr("data-table-item",tableIndex);
			
			$tableItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
				n=$(this).attr("id");
				n ? $(this).attr("id",n.replace(/{index}/g,tableIndex)) : "";
				n=$(this).attr("name");
				n ? $(this).attr("name",n.replace(/{index}/g,tableIndex)) : "";
				
				$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,tableIndex)) : "";
			});
			
			$("#labeling_process_table").find("[data-table-list]").append($tableItemTemplate);
			
			$("#productionProcessId"+tableIndex).addClass("m-select2");
			$("#productionProcessId"+tableIndex).select2({placeholder:"Select Process",allowClear:0});
			
			$("#employeeId"+tableIndex).addClass("m-select2");
			$("#employeeId"+tableIndex).select2({placeholder:"Select Employee",allowClear:0});
			
			$('#form_step9').formValidation('addField',"jobworkProcessVos["+tableIndex+"].productionProcessVo.productionProcessId", processValidator);
			$('#form_step9').formValidation('addField',"jobworkProcessVos["+tableIndex+"].employeeVo.employeeId", employeeValidator);
			$('#form_step9').formValidation('addField',"jobworkProcessVos["+tableIndex+"].qty", qtyValidator);
			$('#form_step9').formValidation('addField',"jobworkProcessVos["+tableIndex+"].rate", rateValidator); 
			
			tableIndex++;
			
			setSrNo();
		}
		
		function setSrNo() {
			var $tableItemTemplate=$("#labeling_process_table").find("[data-table-item]").not(".m--hide");
			var i = 0;
			$tableItemTemplate.each(function (){
				$(this).find("[data-item-index]").html(++i);
			});
		}
		
		function setTableIndex(i) {
			tableIndex = i*1;
		}
		
		function changeQty(id) {
			$('#form_step9_qc').formValidation('revalidateField',"jobworkProcessVos["+id+"].qcQty");
			$('#form_step9_qc').formValidation('revalidateField',"jobworkProcessVos["+id+"].qcPassQty");
			$('#form_step9_qc').formValidation('revalidateField',"jobworkProcessVos["+id+"].qcFailQty");
		}
		/**
		*** QUALITY CHECK
		*/
		
		$(document).ready(function(){
			
			$("#form_step9_qc").formValidation({
				framework : 'bootstrap',
				live:'disabled',
				excluded : ":disabled",
				icon : null,
				button: {
					selector: "#form_save_step9_qc",
					disabled: "disabled"
				},
				fields : {
					
				}
			});
			
			$("#quality_check").click(function(){
				$("#form_step9_qc").removeClass("m--hide");
				$("#form_step9").addClass("m--hide");
			});
			
			$("#quality_check_cancel").click(function(){
				$("#form_step9").removeClass("m--hide");
				$("#form_step9_qc").addClass("m--hide");
			});
			
			$("#form_save_step9_qc").on("click", function() {
				
				if ($('#form_step9_qc').data('formValidation').isValid() == null) {
					$('#form_step9_qc').data('formValidation').validate();
				}
				
				if($('#form_step9_qc').data('formValidation').isValid() == true) {
					$('#form_step9').remove();
				} else {
					return false;
				}
				
			});
			
			var $tableItem=$("#stitching_quality_check_table").find("[data-table-item]").not(".m--hide");
			
			$tableItem.each(function () {
				var i = $(this).attr("data-table-item");
				$('#form_step9_qc').formValidation('addField',"jobworkProcessVos["+i+"].qcQty", qtyValidator);
				$('#form_step9_qc').formValidation('addField',"jobworkProcessVos["+i+"].qcPassQty", passQtyValidator);
				$('#form_step9_qc').formValidation('addField',"jobworkProcessVos["+i+"].qcFailQty", passQtyValidator);
				$('#form_step9_qc').formValidation('addField',"jobworkProcessVos["+i+"].qcBy.employeeId", employeeValidator);
			});
		});
	</script>
</body>
<!-- end::Body -->
</html>