<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${jobworkVo.jobworkId == 0 ? "New" : jobworkVo.prefix.concat(jobworkVo.jobworkNo)} | Jobwork</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		/* .m-table tr.m-table__row--brand span {
			color: #fff !important;
		} */
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
		.m-wizard.m-wizard--3 .m-wizard__head {
		    padding: 2rem 1rem !important;
		}
		
		.m-wizard.m-wizard--3 .m-wizard__form {
			padding: 2rem 4rem 3rem 4rem;
		}
		
	</style>
	
	<c:if test="${!isEditable}">
		<style type="text/css">
			select[readonly].select2-hidden-accessible + .select2-container {
			  pointer-events: none;
			  touch-action: none;
			}
		</style>
	</c:if>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${jobworkVo.jobworkId == 0 ? "New Jobwork" : jobworkVo.prefix.concat(jobworkVo.jobworkNo)}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/jobwork" class="m-nav__link">
										<span class="m-nav__link-text">Jobwork</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<!-- <form class="m-form m-form--state m-form--fit m-form--label-align-left" id="jobwork_form" action="/jobwork/save" method="post"> -->	
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									
									<div class="m-portlet__body" >
										<!--begin: Form Wizard-->
										<div class="m-wizard m-wizard--3 m-wizard--success" id="m_wizard">
								
											<!--begin: Message container -->
										    <div class="m-portlet__padding-x">
										        <!-- Here you can put a message or alert -->
										    </div>
										    <!--end: Message container -->
								
											<div class="row m-row--no-padding">
												<div class="col-xl-3 col-lg-12">
													<!--begin: Form Wizard Head -->
													<div class="m-wizard__head">
								
														<!--begin: Form Wizard Progress -->  		
														<div class="m-wizard__progress">	
															<div class="progress">		 
																<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>						 	
															</div>			 
														</div> 
											            <!--end: Form Wizard Progress --> 
								
											            <!--begin: Form Wizard Nav -->
														<%@include file="jobwork-step-nav.jsp" %>
														<div class="m-wizard__nav">
															<div class="m-wizard__steps ">
																<div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_1">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step1_form').submit()">							 
																			<span><span>1</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Antri And Larring
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step " m-wizard-target="m_wizard_form_step_2" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step2_form').submit()">							 
																			<span><span>2</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Farmo Type
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step3_form').submit()">							 
																			<span><span>3</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Stitching
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_4" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step4_form').submit()">							 
																			<span><span>4</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Measurement QC
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_5" > 
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step5_form').submit()">							 
																			<span><span>5</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Bal / Gaj
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_6" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step6_form').submit()">							 
																			<span><span>6</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Washing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_7">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number"  onclick="$('#goto_step7_form').submit()">							 
																			<span><span>7</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Measurement QC
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_8" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step8_form').submit()">							 
																			<span><span>8</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Pressing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_9" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step9_form').submit()">							 
																			<span><span>9</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Labeling / Finishing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_10">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step10_form').submit()">							 
																			<span><span>10</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Store
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!--end: Form Wizard Nav -->
													</div>
													<!--end: Form Wizard Head -->	
												</div>
												<div class="col-xl-9 col-lg-12">
													<!--begin: Form Wizard Form-->
													<div class="m-wizard__form">
														<!--
															1) Use m-form--label-align-left class to alight the form input lables to the right
															2) Use m-form--state class to highlight input control borders on form validation
														-->
														<form class="m-form m-form--label-align-left- m-form--state-" id="form_step1" method="post" action="/jobwork/save">
															<c:if test="${jobworkVo.jobworkId != 0}">
																<input type="hidden" id="jobworkId" name="jobworkId" value="${jobworkVo.jobworkId}"/>
															</c:if>
															<input type="hidden" id="deleteJobworkInputIdIds" name="deleteJobworkInputIdIds" />
															<input type="hidden" id="deleteJobworkOutputIdIds" name="deleteJobworkOutputIdIds" />
															<input type="hidden" id="process" name="process" value="${Constant.JOBWORK_PROCESS_ANTRI_AND_LARRING}"/>
															<!--begin: Form Body -->
															<div class="m-portlet__body m-portlet__body--no-padding">
																<!--begin: Form Wizard Step 1-->
																<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
																	<div class="m-form__section m-form__section--first">
																		<div class="row">
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Design No.:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<input type="text" class="form-control m-input" name="designNo" placeholder="Design No." value="${jobworkVo.designNo}">
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Jobwork Date:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<div class="input-group date" >
																							<input type="text" class="form-control m-input todaybtn-datepicker" name="jobworkDate" readonly id="jobworkDate" data-date-format="dd/mm/yyyy"
																								data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'
																								value='<fmt:formatDate pattern="dd/MM/yyyy" value="${jobworkVo.jobworkDate}"/>'/>
																							<div class="input-group-append">
																								<span class="input-group-text">
																									<i class="la la-calendar"></i>
																								</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<div class="m-separator m-separator--dashed m-separator--lg m-1"></div>
																	<div class="m-form__section">
																		<div class="m-form__heading">
																			<h3 class="m-form__heading-title">Input Product</h3>
																		</div>
																		<div class="table-responsive m--margin-top-20">
																			<table class="table m-table table-bordered table-hover" id="input_product">
																			    <thead>
																			      <tr>
																			        <!-- <th>#</th> -->
																			        <th>#</th>
																			        <th>Product</th>
																			        <th>Bale No.</th>
																			        <th class="text-right">Qty</th>
																			        <th class="text-right">Jobwork Qty</th>
																			        <th class="text-right">Faulty</th>
																			        <th></th>
																			        
																			      </tr>
																			    </thead>
																			    <tbody data-table-list="">
																			    	<tr data-table-item="template" class="m--hide not-view">
																			    		<td class="" style="width: 50px;">
																			    			<span data-item-index></span>
																			    		</td>
																				        <td class="p-0" style="width: 370px;">
																				        	<div class="form-group m-form__group p-0">
																								<select class="form-control m-input form-control-sm m-input1" name="jobworkInputVos[{index}].productVariantVo.productVariantId" id="productVariantId{index}" onchange="getBalenoList({index})" placeholder="Select Product">
																									<option value="">Select Product</option>
																									<c:forEach items="${productVariantVos}" var="productVariantVo">
																										<option value="${productVariantVo.productVariantId}">${productVariantVo.productVo.categoryVo.categoryName} ${productVariantVo.productVo.name} ${productVariantVo.variantName}</option>
																									</c:forEach>
																								</select>
																							</div>
																				        </td>
																				        <td class="p-0" style="width: 270px;">
																				        	<div class="form-group m-form__group p-0">
																								<select class="form-control m-input form-control-sm m-input1" name="jobworkInputVos[{index}].productVariantVo.productVariantId" id="baleNo{index}" onchange="setBalenoQty({index})" placeholder="Select Bale No.">
																									<option value="">Select Bale No.</option>
																								</select>
																							</div>
																				        </td>
																				        <td class="" style="width: 270px;">
																				        	<div class="form-group m-form__group p-0 text-right">
																				        		<span class="" data-item-qty="">0</span>
																				        		<span class="" data-item-uom=""></span>
																				        	</div>
																				        </td>
																				        <td class="p-0" style="width: 270px;">
																				        	<div class="form-group m-form__group p-0">
																				        		<div class="m-input-icon m-input-icon--right">
																								<input type="text" class="form-control form-control m-input1 text-right" id="jobworkQty{index}" name="jobworkInputVos[{index}].jobworkQty" placeholder="Jobwork Qty" value="">
																									<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																								</div>
																				        	</div>
																				        </td>
																				        <td class="p-0" style="width: 270px;">
																				        	<div class="form-group m-form__group p-0">
																				        		<div class="m-input-icon m-input-icon--right">
																									<input type="text" class="form-control form-control m-input1 text-right" id="faultyQty{index}" name="jobworkInputVos[{index}].faultyQty" placeholder="Faulty" value="">
																									<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																								</div>
																				        	</div>
																				        </td>
																				        <td class="p-0" style="width: 40px;">
																				        	<button type="button" data-item-remove="" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill" title="Delete"> <i class="fa fa-times"></i></button>
																				        	<input type="hidden" name="jobworkInputVos[{index}].designNo" id="designNo{index}"/>
																				        </td>
																			    	</tr>
																			    	<c:set scope="page" var="inputProductIndex" value="0"/>
																				    <c:forEach items="${jobworkVo.jobworkInputVos}" varStatus="status" var="jobworkInputVo">
																				    	<tr data-table-item="${inputProductIndex}" class="">
																				    		<td class="" style="width: 50px;">
																				    			<span data-item-index></span>
																				    		</td>
																					        <td class="p-0" style="width: 370px;">
																					        	<div class="form-group m-form__group p-0">
																									<select class="form-control m-input form-control-sm m-input1 m-select2" name="jobworkInputVos[${inputProductIndex}].productVariantVo.productVariantId" id="productVariantId${inputProductIndex}" onchange="getBalenoList(${inputProductIndex})" placeholder="Select Product">
																										<option value="">Select Product</option>
																										<c:forEach items="${productVariantVos}" var="productVariantVo">
																											<option value="${productVariantVo.productVariantId}"
																												<c:if test="${jobworkInputVo.productVariantVo.productVariantId == productVariantVo.productVariantId}">selected="selected"</c:if>>
																													${productVariantVo.productVo.categoryVo.categoryName} ${productVariantVo.productVo.name} ${productVariantVo.variantName}
																												</option>
																										</c:forEach>
																									</select>
																								</div>
																					        </td>
																					        <td class="p-0" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																									<select class="form-control m-input form-control-sm m-input1 m-select2" name="jobworkInputVos[${inputProductIndex}].baleNo" data-default="${jobworkInputVo.baleNo}" id="baleNo${inputProductIndex}" onchange="setBalenoQty(${inputProductIndex})" placeholder="Select Bale No.">
																										<option value="">Select Bale No.</option>
																									</select>
																								</div>
																					        </td>
																					        <td class="" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0 text-right">
																					        		<span class="" data-item-qty="">0</span>
																					        		<span class="" data-item-uom=""></span>
																					        	</div>
																					        </td>
																					        <td class="p-0" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																					        		<div class="m-input-icon m-input-icon--right">
																									<input type="text" class="form-control form-control m-input1 text-right" id="jobworkQty${inputProductIndex}" name="jobworkInputVos[${inputProductIndex}].jobworkQty" placeholder="Jobwork Qty" value="${jobworkInputVo.jobworkQty}">
																										<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																									</div>
																					        	</div>
																					        </td>
																					        <td class="p-0" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																					        		<div class="m-input-icon m-input-icon--right">
																										<input type="text" class="form-control form-control m-input1 text-right" id="faultyQty${inputProductIndex}" name="jobworkInputVos[${inputProductIndex}].faultyQty" placeholder="Faulty" value="${jobworkInputVo.faultyQty}">
																										<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																									</div>
																					        	</div>
																					        </td>
																					        <td class="p-0" style="width: 40px;">
																					        	<button type="button" data-item-remove="" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill not-view" title="Delete"> <i class="fa fa-times"></i></button>
																					        	<input type="hidden" id="jobworkInputId${inputProductIndex}" name="jobworkInputVos[${inputProductIndex}].jobworkInputId" value="${jobworkInputVo.jobworkInputId}"/>
																					        	<input type="hidden" id="designNo${inputProductIndex}" name="jobworkInputVos[${inputProductIndex}].designNo" value="${jobworkInputVo.designNo}"/>
																					        </td>
																				    	</tr>
																				    	<c:set scope="page" var="inputProductIndex" value="${inputProductIndex+1}"/>
																				    </c:forEach>
																				</tbody>
																				<tfoot class="not-view">
																			      <tr>
																			        <th></th>
																			        <th>
																			        	<div class="m-demo-icon mb-0">
																							<div class="m-demo-icon__preview">
																								<span class=""><i class="flaticon-plus m--font-primary"></i></span>
																							</div>
																							<div class="m-demo-icon__class">
																							<a href="JavaScript:void(0)" data-toggel="modal" class="m-link m--font-boldest" onclick="addTableItemInputProduct()"> Add More</a>
																							</div>
																						</div>
																			        </th>
																			        <th></th>
																			        <th></th>
																			        <th></th>
																			        <th><span class="m--font-boldest float-right" id=""></span></th>
																			        <th></th>
																			      </tr>
																			    </tfoot>
																		  	</table>
																		</div>
																	</div>
																	<div class="m-separator m-separator--dashed m-separator--lg m-1"></div>
																	<div class="m-form__section">
																		<div class="m-form__heading">
																			<h3 class="m-form__heading-title">Output Product</h3>
																		</div>
																		
																		<div class="row">
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Product:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<select class="m-select2 form-control m-input form-control-sm" name="productVo.productId" id="productId" placeholder="Select Product" onchange="getVariantList()">
																							<option value="">Select Product$</option>
																							<c:forEach items="${productVos}" var="productVo">
																								<option value="${productVo.productId}"
																								<c:if test="${productVo.productId == jobworkVo.productVo.productId}">selected="selected"</c:if>>
																									${productVo.categoryVo.categoryName} ${productVo.name}
																								</option>
																							</c:forEach>
																						</select>
																					</div>
																				</div>
																			</div>
																		</div>
																		
																		<div class="table-responsive m--margin-top-20">
																			<table class="table m-table table-bordered table-hover" id="output_product">
																			    
																			    <thead>
																			      <tr>
																			        <!-- <th>#</th> -->
																			        <th>#</th>
																			        <th>Product</th>
																			        <th class="text-right">Expected Qty</th>
																			        
																			      </tr>
																			    </thead>
																			    <tbody data-table-list="">
																			    	<tr data-table-item="template" class="m--hide not-view">
																			    		<td class="" style="width: 50px;">
																			    			<span data-item-index></span>
																			    		</td>
																				        <td class="" style="width: 370px;">
																				        	<div class="form-group m-form__group p-0" data-variant-name="">
																								
																							</div>
																				        </td>
																				        <td class="p-0" style="width: 270px;">
																				        	<div class="form-group m-form__group p-0">
																				        		<div class="m-input-icon m-input-icon--right">
																									<input type="text" class="form-control form-control m-input1 text-right" id="expectedQty{index}" name="jobworkOutputVos[{index}].expectedQty" placeholder="Expected Qty" value="">
																									<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																									<input type="hidden" name="jobworkOutputVos[{index}].productVariantVo.productVariantId" id="variantId{index}"/>
																								</div>
																				        	</div>
																				        </td>
																			    	</tr>
																			    	<c:set scope="page" var="outputProductIndex" value="0"/>
																			    	<c:forEach items="${jobworkVo.jobworkOutputVos}" varStatus="status" var="jobworkOutputVo">
																				    	<tr data-table-item="${outputProductIndex}" class="">
																				    		<td class="" style="width: 50px;">
																				    			<span data-item-index></span>
																				    		</td>
																					        <td class="" style="width: 370px;">
																					        	<div class="form-group m-form__group p-0">
																									${jobworkOutputVo.productVariantVo.variantName}
																								</div>
																					        </td>
																					        <td class="p-0" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																					        		<div class="m-input-icon m-input-icon--right">
																										<input type="text" class="form-control form-control m-input1 text-right" id="expectedQty${outputProductIndex}" name="jobworkOutputVos[${outputProductIndex}].expectedQty" placeholder="Expected Qty" value="${jobworkOutputVo.expectedQty}" />
																										<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																										<input type="hidden" id="jobworkOutputId${outputProductIndex}" name="jobworkOutputVos[${outputProductIndex}].jobworkOutputId" value="${jobworkOutputVo.jobworkOutputId}"/>
																										<input type="hidden" name="jobworkOutputVos[${outputProductIndex}].productVariantVo.productVariantId" id="variantId${outputProductIndex}" value="${jobworkOutputVo.productVariantVo.productVariantId}"/>
																									</div>
																					        	</div>
																					        </td>
																				    	</tr>
																				    	<c:set scope="page" var="outputProductIndex" value="${outputProductIndex+1}"/>
																				    </c:forEach>
																				</tbody>
																		  	</table>
																		</div>
																	</div>
																	<div class="m-separator m-separator--dashed m-separator--lg m-1"></div>
																	<div class="m-form__section">
																		<div class="row">
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Antri And Larring By:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<select class="form-control m-select2" id="antriLarringBy" name="antriLarringBy.employeeId" placeholder="Select Antri And Larring By" tab-index="-1">
																							<option value="">Select Antri And Larring By</option>
																							<c:forEach items="${employeeVos}" var="employeeVo">
																								<option value="${employeeVo.employeeId}"
																									<c:if test="${employeeVo.employeeId == jobworkVo.antriLarringBy.employeeId}">selected="selected"</c:if>>
																									${employeeVo.employeeName}
																								</option>
																							</c:forEach>
																						</select>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Antri And Larring Date:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<div class="input-group date" >
																							<input type="text" class="form-control m-input default-datetimepicker" name="antriLarringDate" placeholder="Select date and time" id="antriLarringDate"
																								value='<fmt:formatDate pattern="dd/MM/yyyy hh:mm a" value="${jobworkVo.antriLarringDate}" />'/>
																							<div class="input-group-append">
																								<span class="input-group-text">
																								<i class="la la-calendar glyphicon-th"></i>
																								</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Antri And Larring Approve By:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<select class="form-control m-select2" id="antriLarringApproveBy" name="antriLarringApproveBy.employeeId" placeholder="Select Antri And Larring Approve By">
																							<option value="">Select Antri And Larring Approve By</option>
																							<c:forEach items="${employeeVos}" var="employeeVo">
																								<option value="${employeeVo.employeeId}"
																									<c:if test="${employeeVo.employeeId == jobworkVo.antriLarringApproveBy.employeeId}">selected="selected"</c:if>>
																									${employeeVo.employeeName}
																								</option>
																							</c:forEach>
																						</select>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Antri And Larring Approve Date:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<c:if test="${not empty jobworkVo.antriLarringApproveDate}"></c:if>
																						<div class="input-group date" >
																							<input type="text" class="form-control m-input default-datetimepicker" name="antriLarringApproveDate" placeholder="Select date and time" id="antriLarringApproveDate" value='<fmt:formatDate pattern="dd/MM/yyyy hh:mm a" value="${jobworkVo.antriLarringApproveDate}" />'/>
																							<div class="input-group-append">
																								<span class="input-group-text">
																								<i class="la la-calendar glyphicon-th"></i>
																								</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<!--end: Form Wizard Step 1-->
															</div>
															<!--end: Form Body -->
															<!--begin: Form Actions -->
															<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
																<div class="m-form__actions">
																	<div class="row">
																		<div class="col-lg-6 m--align-left">
																			<a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
																			<span>
																			<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																			<span>Back</span>
																			</span>
																			</a>
																		</div>
																		<div class="col-lg-6 m--align-right">
																			
																			<c:if test="${isEditable == true}">
																				<button type="submit" id="form_save_step1" class="btn btn-success m-btn m-btn--custom m-btn--icon not-view" data-wizard-action="next">
																					<span><span>Save & Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																				</button>
																			</c:if>
																			<c:if test="${isEditable == false}">
																				<a href="/jobwork/${jobworkVo.jobworkId}" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
																					<span>
																						<span>Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																				</a>
																			</c:if>
																		</div>
																	</div>
																</div>
															</div>
															<!--end: Form Actions -->
														</form>
													</div>
													<!--end: Form Wizard Form-->
								
												</div>
											</div>
										</div>
										<!--end: Form Wizard-->
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
						</div>
						
					<!-- </form> -->
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	<script type="text/javascript">
	
		if(!${isEditable}) {
			$(".not-view").remove();
			$(".default-datetimepicker").removeClass("default-datetimepicker");
			$(".todaybtn-datepicker").removeClass("todaybtn-datepicker");
			$.each($('input, select ,textarea', '#form_step1'),function(k){
				$(this).attr("readonly", "true")
			});
		}
	</script>
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	<%-- <script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/wizard/wizard.js" type="text/javascript"></script> --%>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<%-- <script src="<%=request.getContextPath()%>/script/Jobwork/Jobwork-script.js" type="text/javascript"></script> --%>
	
	<script type="text/javascript">
		
		var qtyValidator = {
			validators : {
				notEmpty : {
					message : 'The Qty is required'
				},
				stringLength : {
					max : 20,
					message : 'The Qty must be less than 20 characters long'
				},
				regexp : {
					regexp:/^((\d+)((\.\d{0,4})?))$/,
					message : 'The Qty is invalid'
				}
			}
		},
		baleNoValidator = {
				validators : {
					notEmpty : {
						message : 'Bale No. is required'
					},
					stringLength : {
						max : 20,
						message : 'The Qty must be less than 20 characters long'
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,4})?))$/,
						message : 'The Qty is invalid'
					}
				}
			},
			productValidator = {
					validators : {
						notEmpty : {
							message : 'Product is required'
						}
					}
				};
	
		function getBalGajForm(formType) {
			$("#btnBalGajProcessDiv").addClass("m--hide");
			if(formType == 0){
				
				$("#inHouseTable").removeClass("m--hide");
				$("#outHouseTable").addClass("m--hide");
			}else{
				$("#outHouseTable").removeClass("m--hide");
				$("#inHouseTable").addClass("m--hide");
			}
			
		}
		
		var inputProductTableIndex = 0, outputProductTableIndex = 0;
		
		$(document).ready(function(){
			
			setInputProductTableIndex(${inputProductIndex});
			setOutputProductTableIndex(${outputProductIndex});
			
			$("#form_step1").formValidation({
				framework : 'bootstrap',
				live:'disabled',
				excluded : ":disabled",
				icon : null,
				button: {
					selector: "#form_save_step1",
					disabled: "disabled"
				},
				fields : {
					designNo:{
						validators: {
							notEmpty: {
								message: 'The Design No. is required'
							},
							stringLength : {
								max : 100,
								message : 'The Design No. Not More than 100 characters long'
							},
							regexp : {
								regexp : /^[a-zA-Z0-9_-\s-.,()& ]+$/,
								message : 'The Design No. can only consist of alphabetical, numberical value'
							},
						}
					},
					"productVo.productId":{
						validators: {
							notEmpty: {
								message: 'The Output Product is required'
							},
						}
					},
					"antriLarringBy.employeeId":{
						validators: {
							notEmpty: {
								message: 'The Antri Larring By is required'
							},
						}
					},
					"antriLarringApproveBy.employeeId":{
						validators: {
							notEmpty: {
								message: 'The Antri Larring Approve By is required'
							},
						}
					},
					antriLarringDate:{
						validators: {
							notEmpty: {
								message: 'The Antri Larring Date is required'
							},
						}
					},
					antriLarringApproveDate:{
						validators: {
							notEmpty: {
								message: 'The Antri Larring Approve Date is required'
							},
						}
					},
					/* companyMobileno : {
						validators : {
							stringLength : {
								max : 15,
								message : 'The Mobile no. must be 15 digit long'
							},
							regexp : {
								regexp : /^[0-9+]+$/,
								message : 'The Mobile no. can only consist numerical value'
							}
						}
					}, */
				}
			});
			
			$("#antriLarringDate").change(function() {
				$('#form_step1').formValidation('revalidateField', 'antriLarringDate');
			});
			
			$("#antriLarringApproveDate").change(function() {
				$('#form_step1').formValidation('revalidateField', 'antriLarringApproveDate');
			});
			
			
			$("#input_product").on("click",'button[data-item-remove]',function(e) {
			
				e.preventDefault();
				var i = $(this).closest("[data-table-item]").attr("data-table-item");
				
				if($("#jobworkInputId"+i).val() != undefined ) {
				
					$("#deleteJobworkInputIdIds").val($("#deleteJobworkInputIdIds").val()+$("#jobworkInputId"+i).val()+ ",");
				}
				
				$('#form_step1').formValidation('removeField',"jobworkInputVos["+i+"].productVariantVo.productVariantId");
				$('#form_step1').formValidation('removeField',"jobworkInputVos["+i+"].jobworkQty");
				$('#form_step1').formValidation('removeField',"jobworkInputVos["+i+"].baleNo");
				$('#form_step1').formValidation('removeField',"jobworkInputVos["+i+"].faultyQty"); 
				$(this).closest("[data-table-item]").remove();
				
				setInputProductSrNo();
			});
			
			$("#output_product").on("click",'button[data-item-remove]',function(e) {
			
				e.preventDefault();
				var i = $(this).closest("[data-table-item]").attr("data-table-item");
				
				if($("#jobworkOutputId"+i).val() != undefined ) {
				
					$("#deleteJobworkOutputIdIds").val($("#deleteJobworkOutputIdIds").val()+$("#jobworkOutputId"+i).val()+ ",");
				}
				
				$('#form_step1').formValidation('removeField',"jobworkOutputVos["+i+"].productVariantVo.productVariantId");
				$('#form_step1').formValidation('removeField',"jobworkOutputVos["+i+"].expectedQty");
				$(this).closest("[data-table-item]").remove();
				
				setOutputProductSrNo();
			});
			
			$("#form_save_step1").on("click", function(){
				
				if ($('#form_step1').data('formValidation').isValid() == null) {
					$('#form_step1').data('formValidation').validate();
				}
				
				if($("#input_product").find("[data-table-item]").not(".m--hide").length == 0) {
					toastr.error("","add minimum one input product");
					return false;
				} else if($("#output_product").find("[data-table-item]").not(".m--hide").length == 0) {
					toastr.error("","add minimum one output product");
					return false;
				}
				
				if($('#form_step1').data('formValidation').isValid() == true) {
					$("#input_product").find("[data-table-item='template']").remove();
					$("#output_product").find("[data-table-item='template']").remove();
					
					$.each($('input, select ,textarea', '#form_step1'),function(k){
					    if($("#"+$(this).attr('id')).val() == "" || $("#"+$(this).attr('id')).val() == "undefined"){
							$("#"+$(this).attr('id')).attr("disabled", "disabled")
						}
					});
				} else {
					return false;
				}
				
			});
			
			var $tableItem=$("#input_product").find("[data-table-item]").not(".m--hide");
			
			$tableItem.each(function () {
				var i = $(this).attr("data-table-item");
				
				getBalenoList(i);
				$('#form_step1').formValidation('addField',"jobworkInputVos["+i+"].productVariantVo.productVariantId", productValidator);
				$('#form_step1').formValidation('addField',"jobworkInputVos["+i+"].jobworkQty", qtyValidator);
				$('#form_step1').formValidation('addField',"jobworkInputVos["+i+"].baleNo", baleNoValidator);
				$('#form_step1').formValidation('addField',"jobworkInputVos["+i+"].faultyQty", qtyValidator);
				
			});
			
			$tableItem=$("#output_product").find("[data-table-item]").not(".m--hide");
			$tableItem.each(function () {
				var i = $(this).attr("data-table-item");
				$('#form_step1').formValidation('addField',"jobworkOutputVos["+i+"].productVariantVo.productVariantId", productValidator);
				$('#form_step1').formValidation('addField',"jobworkOutputVos["+i+"].expectedQty", qtyValidator); 
			});
			
			setOutputProductSrNo();
			setInputProductSrNo();
	  		
		});
		
		function addTableItemInputProduct() {
			
			var	$tableItemTemplate = $("#input_product").find("[data-table-item='template']").clone();
			
			$tableItemTemplate.removeClass("m--hide").attr("data-table-item",inputProductTableIndex);
			
			$tableItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
				n=$(this).attr("id");
				n ? $(this).attr("id",n.replace(/{index}/g,inputProductTableIndex)) : "";
				n=$(this).attr("name");
				n ? $(this).attr("name",n.replace(/{index}/g,inputProductTableIndex)) : "";
				
				$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,inputProductTableIndex)) : "";
			});
			
			$("#input_product").find("[data-table-list]").append($tableItemTemplate);
			
			$("#productVariantId"+inputProductTableIndex).addClass("m-select2");
			$("#productVariantId"+inputProductTableIndex).select2({placeholder:"Select Product",allowClear:0});
			
			$("#baleNo"+inputProductTableIndex).addClass("m-select2");
			$("#baleNo"+inputProductTableIndex).select2({placeholder:"Select Bale No.",allowClear:0});
			
			$('#form_step1').formValidation('addField',"jobworkInputVos["+inputProductTableIndex+"].productVariantVo.productVariantId", productValidator);
			$('#form_step1').formValidation('addField',"jobworkInputVos["+inputProductTableIndex+"].jobworkQty", qtyValidator);
			$('#form_step1').formValidation('addField',"jobworkInputVos["+inputProductTableIndex+"].baleNo", baleNoValidator);
			$('#form_step1').formValidation('addField',"jobworkInputVos["+inputProductTableIndex+"].faultyQty", qtyValidator); 
			
			inputProductTableIndex++;
			
			setInputProductSrNo();
		}
		
		function setInputProductSrNo() {
			var $tableItemTemplate=$("#input_product").find("[data-table-item]").not(".m--hide");
			var i = 0;
			$tableItemTemplate.each(function (){
				$(this).find("[data-item-index]").html(++i);
			});
		}
		
		function setInputProductTableIndex(i) {
			inputProductTableIndex = i*1;
		}
		
		function getBalenoList(id) {
			if($("#productVariantId"+id).val() != "") {
				$.post("/product/"+$("#productVariantId"+id).val()+"/baleno/json", {
					
				}, function (data, status) {
					if(status == 'success') {
						
						$tableItem=$("#input_product").find('[data-table-item="'+id+'"]');
						
						$tableItem.find("[data-item-uom]").html(data.measurementCode);
						
						$("#baleNo"+id).empty();
						$("#baleNo"+id).append($('<option>',{value :'', text :"Select Bale No."}));
						$.each(data.balenoList,function(key,value){
							$("#baleNo"+id).append($('<option>',{value :value.baleNo, text :value.baleNo, name:value.qty, designno:value.designNo}));
						});
						
						if($("#baleNo"+id).data("default") != undefined) {
							if($("#baleNo"+id).find("option:contains('" +$("#baleNo"+id).data("default")+ "')").length == 0) {
								$("#baleNo"+id).append($('<option>',{value :$("#baleNo"+id).data("default"), text :$("#baleNo"+id).data("default"), designno: $("#designNo"+id).val()}));
							}
							$("#baleNo"+id).val($("#baleNo"+id).data("default")).trigger('change') ;
						}
						
					}
				});
			}
		}
		
		function setBalenoQty(id) {
			$tableItem=$("#input_product").find('[data-table-item="'+id+'"]');
			$tableItem.find("[data-item-qty]").html($('#baleNo'+id+' option[value="'+$('#baleNo'+id).val()+'"]').attr("name"));
			$tableItem.find('[name="jobworkInputVos['+id+'].designNo"]').val($('#baleNo'+id+' option[value="'+$('#baleNo'+id).val()+'"]').attr("designno"));
			
			//designNo{index}
		}
		
		function getVariantList() {
			
			if($("#productId").val() != "") {
				$.post("/product/"+$("#productId").val()+"/json", {
					
				}, function (data, status) {
					if(status == 'success') {
						
						var $tableItem=$("#output_product").find("[data-table-item]").not(".m--hide");
						$tableItem.each(function () {
							var i = $(this).attr("data-table-item");
							
							if($("#jobworkOutputId"+i).val() != undefined ) {
								$("#deleteJobworkOutputIdIds").val($("#deleteJobworkOutputIdIds").val()+$("#jobworkOutputId"+i).val()+ ",");
							}
							
							$('#form_step1').formValidation('addField',"jobworkOutputVos["+i+"].expectedQty", qtyValidator);
							
						});
						
						$("#output_product").find("[data-table-item]").not(".m--hide").remove();
						outputProductTableIndex = 0;
						
						$.each(data.productVariantVos, function (key, value) {
							
							var	$tableItemTemplate = $("#output_product").find("[data-table-item='template']").clone();
							
							$tableItemTemplate.removeClass("m--hide").attr("data-table-item",outputProductTableIndex);
							
							$tableItemTemplate.find("[data-variant-name]").html(value.variantName);
							
							$tableItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
								n=$(this).attr("id");
								n ? $(this).attr("id",n.replace(/{index}/g,outputProductTableIndex)) : "";
								n=$(this).attr("name");
								n ? $(this).attr("name",n.replace(/{index}/g,outputProductTableIndex)) : "";
								
								$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,outputProductTableIndex)) : "";
							});
							
							$("#output_product").find("[data-table-list]").append($tableItemTemplate);
							
							$("#variantId"+outputProductTableIndex).val(value.productVariantId);
							
							$('#form_step1').formValidation('addField',"jobworkOutputVos["+outputProductTableIndex+"].expectedQty", qtyValidator);
							
							outputProductTableIndex++;
							
							setOutputProductSrNo();
						});
					}
				});
			}
		}
		function getInputProductInfo(id) {
			if($("#productVariantId"+id).val() != "") {
				$.post("/product/variant/"+$("#productVariantId"+id).val()+"/json", {
					
				}, function (data, status) {
					if(status == 'success') {
						
						$tableItem=$("#input_product").find('[data-table-item="'+id+'"]');
						
						$tableItem.find("[data-item-uom]").html(data.productVo.unitOfMeasurementVo.measurementCode);
					}
				});
			}
		}
		
		function addTableItemOutputProduct() {
			
			var	$tableItemTemplate = $("#output_product").find("[data-table-item='template']").clone();
			
			$tableItemTemplate.removeClass("m--hide").attr("data-table-item",outputProductTableIndex);
			
			$tableItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
				n=$(this).attr("id");
				n ? $(this).attr("id",n.replace(/{index}/g,outputProductTableIndex)) : "";
				n=$(this).attr("name");
				n ? $(this).attr("name",n.replace(/{index}/g,outputProductTableIndex)) : "";
				
				$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,outputProductTableIndex)) : "";
			});
			
			$("#output_product").find("[data-table-list]").append($tableItemTemplate);
			
			$("#variantId"+outputProductTableIndex).addClass("m-select2");
			$("#variantId"+outputProductTableIndex).select2({placeholder:"Select Product",allowClear:0});
			
			$('#form_step1').formValidation('addField',"jobworkOutputVos["+outputProductTableIndex+"].productVariantVo.productVariantId", productValidator);
			$('#form_step1').formValidation('addField',"jobworkOutputVos["+outputProductTableIndex+"].expectedQty", qtyValidator);
			
			outputProductTableIndex++;
			
			setOutputProductSrNo();
		}
		
		function setOutputProductSrNo() {
			var $tableItemTemplate=$("#output_product").find("[data-table-item]").not(".m--hide");
			var i = 0;
			$tableItemTemplate.each(function (){
				$(this).find("[data-item-index]").html(++i);
			});
		}
		
		function setOutputProductTableIndex(i) {
			outputProductTableIndex = i*1
		}
	</script>
</body>
<!-- end::Body -->
</html>