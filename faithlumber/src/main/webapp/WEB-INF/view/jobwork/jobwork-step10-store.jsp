<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${jobworkVo.prefix}${jobworkVo.jobworkNo} | Jobwork</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		/* .m-table tr.m-table__row--brand span {
			color: #fff !important;
		} */
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
		.m-wizard.m-wizard--3 .m-wizard__head {
		    padding: 2rem 1rem !important;
		}
		
		.m-wizard.m-wizard--3 .m-wizard__form {
			padding: 2rem 4rem 3rem 4rem;
		}
		
	</style>
	
	<c:if test="${!isEditable}">
		<style type="text/css">
			select[readonly].select2-hidden-accessible + .select2-container {
			  pointer-events: none;
			  touch-action: none;
			}
		</style>
	</c:if>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${jobworkVo.prefix}${jobworkVo.jobworkNo}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/jobwork" class="m-nav__link">
										<span class="m-nav__link-text">Jobwork</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<!-- <form class="m-form m-form--state m-form--fit m-form--label-align-left" id="jobwork_form" action="/jobwork/save" method="post"> -->	
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									
									<div class="m-portlet__body" >
										<!--begin: Form Wizard-->
										<div class="m-wizard m-wizard--3 m-wizard--success" id="m_wizard">
								
											<!--begin: Message container -->
										    <div class="m-portlet__padding-x">
										        <!-- Here you can put a message or alert -->
										    </div>
										    <!--end: Message container -->
								
											<div class="row m-row--no-padding">
												<div class="col-xl-3 col-lg-12">
													<!--begin: Form Wizard Head -->
													<div class="m-wizard__head">
								
														<!--begin: Form Wizard Progress -->  		
														<div class="m-wizard__progress">	
															<div class="progress">		 
																<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>						 	
															</div>			 
														</div> 
											            <!--end: Form Wizard Progress --> 
								
											            <!--begin: Form Wizard Nav -->
														<%@include file="jobwork-step-nav.jsp" %>
														<div class="m-wizard__nav">
															<div class="m-wizard__steps ">
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_1">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step1_form').submit()">							 
																			<span><span>1</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Antri And Larring
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_2" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step2_form').submit()">							 
																			<span><span>2</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Farmo Type
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_3" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step3_form').submit()">							 
																			<span><span>3</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Stitching
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_4" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step4_form').submit()">							 
																			<span><span>4</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Measurement QC
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_5" > 
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step5_form').submit()">							 
																			<span><span>5</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Bal / Gaj
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_6" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step6_form').submit()">							 
																			<span><span>6</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Washing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_7">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number"  onclick="$('#goto_step7_form').submit()">							 
																			<span><span>7</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Measurement QC
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_8" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step8_form').submit()">							 
																			<span><span>8</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Pressing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_9" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step9_form').submit()">							 
																			<span><span>9</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Labeling / Finishing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_10">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step10_form').submit()">							 
																			<span><span>10</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Store
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!--end: Form Wizard Nav -->
													</div>
													<!--end: Form Wizard Head -->	
												</div>
												<div class="col-xl-9 col-lg-12">
													<!--begin: Form Wizard Form-->
													<div class="m-wizard__form">
														<!--
															1) Use m-form--label-align-left class to alight the form input lables to the right
															2) Use m-form--state class to highlight input control borders on form validation
														-->
														<form class="m-form m-form--label-align-left- m-form--state-" id="form_step10" method="post" action="/jobwork/save">
															<input type="hidden" id="jobworkId" name="jobworkId" value="${jobworkVo.jobworkId}"/>
															<input type="hidden" id="process" name="process" value="${Constant.JOBWORK_PROCESS_STORE}"/>
															<input type="hidden" id="storeStatus" name="storeStatus" value="pending"/>
															
															<!--begin: Form Body -->
															<div class="m-portlet__body m-portlet__body--no-padding">
																<!--begin: Form Wizard Step 3-->
																<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_6">
																	<div class="m-form__section m-form__section--first">
																		<div class="m-form__heading">
																			<h3 class="m-form__heading-title">Store</h3>
																		</div>
																	</div>
																	<div class="m-form__section">
																		<div class="m-form__heading">
																			<h3 class="m-form__heading-title">Output Product</h3>
																		</div>
																		
																		<div class="table-responsive m--margin-top-20">
																			<table class="table m-table table-bordered table-hover" id="output_product">
																			    
																			    <thead>
																			      <tr>
																			        <!-- <th>#</th> -->
																			        <th>#</th>
																			        <th>Product</th>
																			        <th class="text-right">Expected Qty</th>
																			        <th class="text-right">Production Qty</th>
																			      </tr>
																			    </thead>
																			    <tbody data-table-list="">
																			    	<c:set scope="page" var="outputProductIndex" value="0"/>
																			    	<c:forEach items="${jobworkVo.jobworkOutputVos}" varStatus="status" var="jobworkOutputVo">
																				    	<tr data-table-item="${outputProductIndex}" class="">
																				    		<td class="" style="width: 50px;">
																				    			<span data-item-index>${status.index+1}</span>
																				    		</td>
																					        <td class="" style="width: 370px;">
																					        	<div class="form-group m-form__group p-0">
																									${jobworkOutputVo.productVariantVo.productVo.categoryVo.categoryName} ${jobworkOutputVo.productVariantVo.productVo.name} ${jobworkOutputVo.productVariantVo.variantName}
																								</div>
																					        </td>
																					        <td class="text-right" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																					        		<span>${jobworkOutputVo.expectedQty}</span>
																					        		<span>${jobworkOutputVo.productVariantVo.productVo.unitOfMeasurementVo.measurementCode}</span>
																					        	</div>
																					        </td>
																					        <td class="p-0" style="width: 270px;">
																					        	<div class="form-group m-form__group p-0">
																					        		<div class="m-input-icon m-input-icon--right">
																									<input type="text" class="form-control form-control m-input1 text-right" id="productionQty${outputProductIndex}" name="jobworkOutputVos[${outputProductIndex}].productionQty" placeholder="Production Qty" value="${jobworkOutputVo.productionQty}" />
																										<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom>${jobworkOutputVo.productVariantVo.productVo.unitOfMeasurementVo.measurementCode}</i></span></span>
																										<input type="hidden" id="jobworkOutputId${outputProductIndex}" name="jobworkOutputVos[${outputProductIndex}].jobworkOutputId" value="${jobworkOutputVo.jobworkOutputId}"/>
																										<input type="hidden" id="expectedQty${outputProductIndex}" name="jobworkOutputVos[${outputProductIndex}].expectedQty" value="${jobworkOutputVo.expectedQty}"/>
																										<input type="hidden" id="productVariantVo${outputProductIndex}" name="jobworkOutputVos[${outputProductIndex}].productVariantVo.productVariantId" value="${jobworkOutputVo.productVariantVo.productVariantId}"/>
																									</div>
																					        	</div>
																					        </td>
																				    	</tr>
																				    	<c:set scope="page" var="outputProductIndex" value="${outputProductIndex+1}"/>
																				    </c:forEach>
																				</tbody>
																		  	</table>
																		</div>
																	</div>
																</div>
																<!--end: Form Wizard Step 3-->
															</div>
															<!--end: Form Body -->
															<!--begin: Form Actions -->
															<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
																<div class="m-form__actions">
																	<div class="row">
																		<div class="col-lg-6 m--align-left">
																			<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" onclick="$('#previous_form').submit()">
																				<span>
																				<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																				<span>Back</span>
																				</span>
																			</button>
																		</div>
																		<div class="col-lg-6 m--align-right">
																			<c:if test='${not empty jobworkVo.storeStatus || jobworkVo.storeStatus == "pending"}'>
																				<button type="button" class="btn btn-primary m-btn m-btn--custom m-btn--icon" id="generate_package">
																					<span><i class="la la-check"></i>&nbsp;&nbsp;<span>Generate Package</span></span>
																				</button>
																			</c:if>
																			<c:if test='${empty jobworkVo.storeStatus || jobworkVo.storeStatus == "pending"}'>
																				<button type="submit" id="form_save_step10" class="btn btn-success m-btn m-btn--custom m-btn--icon not-view" data-wizard-action="next">
																					<span><span>Save & Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																				</button>
																			</c:if>
																			<c:if test="${isEditable == false}">
																				<a href="/jobwork/${jobworkVo.jobworkId}" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
																					<span>
																						<span>Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																				</a>
																			</c:if>
																		</div>
																	</div>
																</div>
															</div>
															<!--end: Form Actions -->
														</form>
														<form class="m-form m-form--label-align-left- m-form--state- m--hide" id="form_step10_package" method="post" action="/jobwork/save">
															<input type="hidden" id="jobworkId" name="jobworkId" value="${jobworkVo.jobworkId}"/>
															<input type="hidden" id="process" name="process" value="${Constant.JOBWORK_PROCESS_STORE}"/>
															<input type="hidden" id="storeStatus" name="storeStatus" value="package"/>
															<input type="hidden" id="productId" value="${jobworkVo.productVo.productId}"/>
															
															<!--begin: Form Body -->
															<div class="m-portlet__body m-portlet__body--no-padding">
																<!--begin: Form Wizard Step 3-->
																<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_6">
																	<div class="m-form__section m-form__section--first">
																		<div class="m-form__heading">
																			<h3 class="m-form__heading-title">Generate Package</h3>
																		</div>
																		<div class="row">
																			<div class="col-lg-4 col-md-4 col-sm-12">
																				<div class="m-widget28">
																					<div class="m-widget28__container" >	
																						<!-- Start:: Content -->
																						<div class="m-widget28__tab tab-content">
																							<div class="m-widget28__tab-container tab-pane active">
																							    <div class="m-widget28__tab-items">
																									<div class="m-widget28__tab-item">
																										<span class="m--regular-font-size-">Output Product Name:</span>
																										<span>${jobworkVo.productVo.name}</span>
																									</div>
																								</div>					      	 		      	
																							</div>
																						</div>
																						<!-- end:: Content --> 	
																					</div>				 	 
																				</div>
																			</div>
																			<c:forEach items="${jobworkVo.jobworkOutputVos}" varStatus="status" var="jobworkOutputVo">
																				<input type="hidden" name="jobworkOutputVos[${status.index}].jobworkOutputId" value="${jobworkOutputVo.jobworkOutputId}"/>
																				<input type="hidden" name="jobworkOutputVos[${status.index}].productVariantVo.productVariantId" value="${jobworkOutputVo.productVariantVo.productVariantId}"/>
																				<input type="hidden" name="jobworkOutputVos[${status.index}].expectedQty" value="${jobworkOutputVo.expectedQty}"/>
																				<input type="hidden" name="jobworkOutputVos[${status.index}].productionQty" value="${jobworkOutputVo.productionQty}"/>
																				<input type="hidden" name="jobworkOutputVos[${status.index}].finalQty" id="finalQty${jobworkOutputVo.productVariantVo.productVariantId}" value="${jobworkOutputVo.finalQty}"/>
																				<div class="col-lg-2 col-md-2 col-sm-12">
																					<div class="m-widget28">
																						<div class="m-widget28__container" >	
																							<!-- Start:: Content -->
																							<div class="m-widget28__tab tab-content">
																								<div class="m-widget28__tab-container tab-pane active">
																								    <div class="m-widget28__tab-items">
																										<div class="m-widget28__tab-item">
																											<span class="m--regular-font-size-">${jobworkOutputVo.productVariantVo.variantName}:</span>
																											<span>${jobworkOutputVo.productionQty}<small>&nbsp;${jobworkOutputVo.productVariantVo.productVo.unitOfMeasurementVo.measurementCode}</small></span>
																										</div>
																									</div>					      	 		      	
																								</div>
																							</div>
																							<!-- end:: Content --> 	
																						</div>				 	 
																					</div>
																				</div>
																			</c:forEach>
																		</div>
																	</div>
																	<div class="m-separator m-separator--dashed m-separator--lg m-1"></div>
																	<div class="m-form__section" id="package_list">
																		<div class="m--hide" data-package-item="template">
																			<div class="row">
																				<div class="form-group m-form__group col-lg-4 row pt-0 pb-0">
																					<div class="col-lg-6 m-form__group-sub content-center">
																						<label class="form-control-label" data-package-name="">28-38</label>
																					</div>
																					<div class="col-lg-6 m-form__group-sub">
																						<label class="form-control-label">No. of Pack:</label>
																						<input type="text" class="form-control m-input" name="jobworkPackageVos[{index}].noOfPack" onchange="changePackageQty({index},this);this.oldValue = this.value" onkeypress="return alpha(event)" value="">
																						<input type="hidden" name="jobworkPackageVos[{index}].productVariantVo.productVariantId" value=""/>
																					</div>
																				</div>
																				<div class="form-group m-form__group col-lg-8 row pt-0 pb-0" data-package-variant="">
																					<div class="col-lg-2 m-form__group-sub m--hide" data-variant-item="template">
																						<label class="form-control-label" data-variant-name="">28:</label>
																						<input type="text" class="form-control m-input" name="jobworkPackageVos[{index}].variantQuantites" placeholder="" value="" readonly="readonly" />
																						<input type="hidden" name="variant-qty" value="0"/>
																						<input type="hidden" name="jobworkPackageVos[{index}].parentVariantIds" value=""/>
																						
																					</div>
																				</div>
																			</div>
																			<div class="m-divider p-4"><span></span></div>
																		</div>
																	</div>
																	<div class="m-form__section" style="background-color: #f7f8fa; padding: 20px">
																		<div class="m-form__heading">
																			<h3 class="m-form__heading-title">Single Product</h3>
																		</div>
																		<div class="row">
																			<c:forEach items="${jobworkVo.jobworkOutputVos}" varStatus="status" var="jobworkOutputVo">
																				<div class="col-lg-2 col-md-2 col-sm-12">
																					<div class="m-widget28">
																						<div class="m-widget28__container" >	
																							<!-- Start:: Content -->
																							<div class="m-widget28__tab tab-content">
																								<div class="m-widget28__tab-container tab-pane active">
																								    <div class="m-widget28__tab-items">
																										<div class="m-widget28__tab-item">
																											<span class="m--regular-font-size-">${jobworkOutputVo.productVariantVo.variantName}:</span>
																											<span>
																												<span id="available-qty-${jobworkOutputVo.productVariantVo.productVariantId}">${jobworkOutputVo.productionQty}</span>
																												<small>&nbsp;${jobworkOutputVo.productVariantVo.productVo.unitOfMeasurementVo.measurementCode}</small>
																											</span>
																										</div>
																									</div>					      	 		      	
																								</div>
																							</div>
																							<!-- end:: Content --> 	
																						</div>				 	 
																					</div>
																				</div>
																			</c:forEach>
																		</div>
																	</div>
																</div>
																<!--end: Form Wizard Step 3-->
															</div>
															<!--end: Form Body -->
															<!--begin: Form Actions -->
															<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
																<div class="m-form__actions">
																	<div class="row">
																		<div class="col-lg-6 m--align-left">
																			<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" onclick="$('#previous_form').submit()">
																				<span>
																				<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																				<span>Back</span>
																				</span>
																			</button>
																		</div>
																		<div class="col-lg-6 m--align-right">
																			<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" id="generate_package_cancel">
																				<span>Cancel</span>
																			</button>
																			<c:if test='${empty jobworkVo.storeStatus || jobworkVo.storeStatus == "pending"}'>
																				<button type="submit" id="form_save_step10_package" class="btn btn-success m-btn m-btn--custom m-btn--icon not-view" data-wizard-action="next">
																					<span><span>Save & Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																				</button>
																			</c:if>
																		</div>
																	</div>
																</div>
															</div>
															<!--end: Form Actions -->
														</form>
														<form action="/jobwork/${jobworkVo.jobworkId}" method="post" id="previous_form">
															<input type="hidden" name="previousProcess" value="${Constant.JOBWORK_PROCESS_LABELING_FINISHING}"/>
														</form>
													</div>
													<!--end: Form Wizard Form-->
								
												</div>
											</div>
										</div>
										<!--end: Form Wizard-->
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
						</div>
					<!-- </form> -->
				</div>
			</div>
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	<script type="text/javascript">
	
		if(!${isEditable}) {
			$(".not-view").remove();
			$(".default-datetimepicker").removeClass("default-datetimepicker");
			$.each($('input, select ,textarea', '#form_step8,#form_step8_receive'),function(k){
				$(this).attr("readonly", "true")
			});
		}
	</script>
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	<%-- <script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/wizard/wizard.js" type="text/javascript"></script> --%>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<%-- <script src="<%=request.getContextPath()%>/script/Jobwork/Jobwork-script.js" type="text/javascript"></script> --%>
	
	<script type="text/javascript">
		
		var packageIndex = 0;
		
		var qtyValidator = {
				validators : {
					notEmpty : {
						message : 'The Qty is required'
					},
					stringLength : {
						max : 20,
						message : 'The Qty must be less than 20 characters long'
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,4})?))$/,
						message : 'The Qty is invalid'
					}
				}
			};
		$(document).ready(function(){
			
			$("#form_step10").formValidation({
				framework : 'bootstrap',
				live:'disabled',
				excluded : ":disabled",
				icon : null,
				button: {
					selector: "#form_save_step10",
					disabled: "disabled"
				},
				fields : {
				}
			});
			
			
			$("#form_save_step10").on('click', function(){
				
				if($('#form_step10').data('formValidation').isValid() == null) {
					$('#form_step10').data('formValidation').validate();
				}
				
				if($('#form_step10').data('formValidation').isValid() == true) {
					$('#form_step10_package').remove();
				} else {
					return false;
				}
			});
			
			var $tableItem=$("#output_product").find("[data-table-item]").not(".m--hide");
			
			$tableItem.each(function () {
				var i = $(this).attr("data-table-item");
				$('#form_step10').formValidation('addField',"jobworkOutputVos["+i+"].productionQty", qtyValidator);
			});
			
			$("#generate_package").click(function(){
				
				if($("#package_list").find('[data-package-item]').not(".m--hide").length == 0) {
					mApp.blockPage({overlayColor:"chocolate",type:"loader",state:"danger",message:"Loading Packages"});
					
					var URL;
					if(${not empty jobworkVo.jobworkOutputVos}) {
						URL = "/jobwork/package/"+$("#jobworkId").val()+"/"+$("#productId").val()+"/json";
					} else {
						URL = "/productpackage/"+$("#productId").val()+"/json";
					}
					
					$.post(URL, {
						
					}, function (data, status) {
						if(status == 'success') {
							
							if(data.error != undefined) {
								swal({
							        title: "No packages created!",
							        text: "Do you want to create new package ?",
							        icon: "success",
							        confirmButtonText: "<span><i class='la la-thumbs-up'></i><span>Yes</span></span>",
							        confirmButtonClass: "btn btn-primary m-btn m-btn--pill m-btn--air m-btn--icon bg-primary",
							        showCancelButton: !0,
							        cancelButtonText: "<span><i class='la la-thumbs-down'></i><span>No</span></span>",
							        cancelButtonClass: "btn btn-secondary m-btn m-btn--pill m-btn--icon"
							    }).then(function(e) {
					                
					                if(e.value) {
					                	window.location.href  = "/productpackage/new"
					                }
							    	
					            })
								return false;
							} else {
								var jobworkPackageVos;
								
								if(${not empty jobworkVo.jobworkOutputVos}) {
									jobworkPackageVos = data.jobworkPackageVos;
									data = data.productVo;
								}
								
								$.each(data.productVariantVos,function(key, value){
									$packageItemTemplate = $("#package_list").find('[data-package-item="template"]').clone();
									$packageItemTemplate.attr("data-package-item",packageIndex).removeClass("m--hide");
									
									$packageItemTemplate.find("[data-package-name]").text(value.variantName)
									
									$packageItemTemplate.find('[name="jobworkPackageVos[{index}].productVariantVo.productVariantId"]').val(value.productVariantId);
									var variantIds =  value.parentVariantIds.split(",");
									var variantQuantites =  value.parentVariantQuantites.split(",");
									
									$.each(variantIds,function(k, val){
										
										$.each(data.productVo.productVariantVos,function(j, v){
											if(val==v.productVariantId) {
												
												$variantTemplate = $packageItemTemplate.find('[data-variant-item="template"]').clone();
												
												$variantTemplate.attr("data-variant-item","").removeClass("m--hide");
												
												$variantTemplate.find("[data-variant-name]").text(v.variantName);
												$variantTemplate.find('[name="variant-qty"]').val(variantQuantites[k]);
												$variantTemplate.find('[name="jobworkPackageVos[{index}].parentVariantIds"]').val(val);
												
												$packageItemTemplate.find('[data-package-variant]').append($variantTemplate);
											}
										});
									});
									
									$packageItemTemplate.find("input[type='text'],input[type='hidden'],select").each(function (){
										n=$(this).attr("id");
										n ? $(this).attr("id",n.replace(/{index}/g,packageIndex)) : "";
										n=$(this).attr("name");
										n ? $(this).attr("name",n.replace(/{index}/g,packageIndex)) : "";
										
										$(this).attr("onchange") ? $(this).attr("onchange",$(this).attr("onchange").replace(/{index}/g,packageIndex)) : "";
									});
									
									$("#package_list").append($packageItemTemplate);
									
									packageIndex++;
								});
								
								$packageItem = $("#package_list").find('[data-package-item]').not(".m--hide");
								
								$.each($packageItem, function(key, $value) {
									var $item = $(this), id = $(this).attr("data-package-item");
									var productVariantId =$item.find('[name="jobworkPackageVos['+id+'].productVariantVo.productVariantId"]').val();
									
									$.each(jobworkPackageVos, function(k,val) {
										if(productVariantId == val.productVariantVo.productVariantId) {
											$item.find('[name="jobworkPackageVos['+id+'].noOfPack"]').val(val.noOfPack).trigger("change");	
										}
									});
									
									$('#form_step10_package').formValidation('addField',"jobworkPackageVos["+id+"].noOfPack", qtyValidator);
								});
								
								$("#form_step10_package").removeClass("m--hide");
								$("#form_step10").addClass("m--hide");
							}
							
							mApp.unblockPage();
							
						}
					});
					
				} else {
					$("#form_step10_package").removeClass("m--hide");
					$("#form_step10").addClass("m--hide");
				}
			});
		});
		
		
		$(document).ready(function(){
			
			$("#form_step10_package").formValidation({
				framework : 'bootstrap',
				live:'disabled',
				excluded : ":disabled",
				icon : null,
				button: {
					selector: "#form_save_step10_package",
					disabled: "disabled"
				},
				fields : {
				}
			});
			
			$("#generate_package_cancel").click(function(){
				
				$("#form_step10").removeClass("m--hide");
				$("#form_step10_package").addClass("m--hide");
			});
			
			$("#form_save_step10_package").on('click', function(){
				
				if($('#form_step10_package').data('formValidation').isValid() == null) {
					$('#form_step10_package').data('formValidation').validate();
				}
				
				if($('#form_step10_package').data('formValidation').isValid() == true) {
					$('#form_step10').remove();			
					$("#package_list").find('[data-package-item="template"]').remove();
					$("#package_list").find('[data-variant-item="template"]').remove();
				} else {
					return false;
				}
			});
		});
		
		function changePackageQty(id,now) {
			
			$packageItem = $("#package_list").find('[data-package-item="'+id+'"]');
			$variant = $packageItem.find('[data-variant-item]').not(".m--hide");
			var flag= false;
			var finalQuantites = [];
			var finalIds = [];
			$noOfPack = $packageItem.find('[name="jobworkPackageVos['+id+'].noOfPack"]');
			$.each($variant,function(key, value) {
				var variantId = $(this).find('[name="jobworkPackageVos['+id+'].parentVariantIds"]').val();
				$variantQuantites = $(this).find('[name="jobworkPackageVos['+id+'].variantQuantites"]');
				var variantQty = $("#available-qty-"+variantId).text() * 1 + $variantQuantites.val() * 1;
				
				var perPackQty = ($noOfPack.val()*1)*($(this).find('[name="variant-qty"]').val()*1);
				
				if((variantQty-perPackQty) < 0) {
					flag= false;
					return;
				} else {					
					flag= true;
					finalQuantites.push(perPackQty);
					finalIds.push(variantId);
				}
				
			});
			
			if(flag==true) {
				$.each(finalIds,function(key, value){
					
					$.each($variant,function(k, val) {
						var variantId = $(this).find('[name="jobworkPackageVos['+id+'].parentVariantIds"]').val();
						$variantQuantites = $(this).find('[name="jobworkPackageVos['+id+'].variantQuantites"]');
						
						if(variantId == value) {
							
							var variantQty = $("#available-qty-"+variantId).text() * 1 + $variantQuantites.val() * 1;
							
							$(this).find('[name="jobworkPackageVos['+id+'].variantQuantites"]').val(finalQuantites[key]);
							$("#available-qty-"+value).text(variantQty - finalQuantites[key]);
							$("#finalQty"+value).val(variantQty - finalQuantites[key]);
						}
					});
				});
			} else {
				
				toastr.error("","To make pack of "+$noOfPack.val()+" is not possible with current variants");
				$noOfPack.val(now.oldValue);
			}
			//$("#available-qty-"+variantId).text(finalQty)
			
		}
		
		function alpha(e) {
		    var k;
		    document.all ? k = e.keyCode : k = e.which;
		    /*46 key code for '.' */
		    /*32 key code for ' ' */
		    /*(k > 64 && k < 91) || (k > 96 && k < 123) ||*/
		    return ( k == 0 || k == 8 || (k >= 48 && k <= 57));
		}
	</script>
</body>
<!-- end::Body -->
</html>