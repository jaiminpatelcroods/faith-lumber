<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Jobwork</title>
	
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Jobwork</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="#" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
						
					<div class="row">
					
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<a href="<%=request.getContextPath()%>/jobwork/new" class="btn btn-primary m-btn m-btn--icon m-btn--air">
												<span><i class="la la-plus"></i><span>Jobwork</span></span>
											</a>
										</div>			
									</div>
									
									<!-- <div class="m-portlet__head-tools">
										<a href="#" id="export_print" class="btn btn-metal m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i></a>
										<a href="#" id="export_excel" class="btn btn-success m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top"
											title="Excel"> <i class="fa fa-file-excel"></i>
										</a>
										<a href="#" id="export_pdf" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top"
											title="PDF"> <i class="fa fa-file-pdf"></i>
										</a>
									</div> -->
								</div>
								<div class="m-portlet__body" >
									<div  class="m_datatable"  >
										<table class="table table-striped- table-bordered table-hover table-checkable" id="jobwork_table">
											<thead>
						  						<tr>
				  									<th>#</th>
				  									<th>Jobwork No.</th>
				  									<th>Jobwork Date</th>
				  									<th>Design No.</th>
				  									<th>Current Process</th>
				  									<th>Status</th>
							  					</tr>
											</thead>			
										</table>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
							
						</div>
				
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/datatable/jquery.spring-friendly.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		var DatatablesDataSourceHtml = {
		    init: function() {
		        $("#jobwork_table").DataTable({
		            responsive: !0,
		            pageLength: 10,
		            searchDelay: 500,
					processing: !0,
					serverSide: true,
					ajax: {
						url: "<%=request.getContextPath()%>/jobwork/datatable",
						type: "POST",
					},
					lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
					  columns: [{
			                data: "jobworkId"
			            }, {
			                data: "jobworkNo"
			            }, {
			                data: "jobworkDate"
			            }, {
			                data: "designNo"
			            },{
			                data: "currentProcess"
			            },{
			            	data: "status"
			            }],
					
					columnDefs: [{
							targets: 0,
							orderable: !1,
							render: function(a, e, t, n) {
								return (n.row+n.settings._iDisplayStart+1);
							}
						},{
							targets: 2,
							render: function(a, e, t, n) {
								return moment(a).format('DD/MM/YYYY');
							}
						},{
							targets: 1,
							render: function(a, e, t, n) {
								return '\n  <a href="/jobwork/'+t.jobworkId+'" class="m-link m--font-bolder" aria-expanded="true">\n '+t.prefix+t.jobworkNo+'</a>\n '
							}
						},{
							targets: 5,
							render: function(a, e, t, n) {
								if(a === 'progress') {
									return '<span class="m-badge m-badge--warning m-badge--wide">progress</span>';
								} else {
									return '<span class="m-badge m-badge--success m-badge--wide">Complete</span>';
								}
							}
					}],
		            fixedHeader: {
		                header: false,
		                footer: false
		            }
		            
		        })
		    }
		};
		
		jQuery(document).ready(function() {
			
			DatatablesDataSourceHtml.init();
		});
	</script>
	
</body>
<!-- end::Body -->
</html>