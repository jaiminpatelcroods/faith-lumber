<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${jobworkVo.prefix}${jobworkVo.jobworkNo} | Jobwork</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		/* .m-table tr.m-table__row--brand span {
			color: #fff !important;
		} */
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
		.m-wizard.m-wizard--3 .m-wizard__head {
		    padding: 2rem 1rem !important;
		}
		
		.m-wizard.m-wizard--3 .m-wizard__form {
			padding: 2rem 4rem 3rem 4rem;
		}
		
	</style>
	
	<c:if test="${!isEditable}">
		<style type="text/css">
			select[readonly].select2-hidden-accessible + .select2-container {
			  pointer-events: none;
			  touch-action: none;
			}
		</style>
	</c:if>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${jobworkVo.prefix}${jobworkVo.jobworkNo}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/jobwork" class="m-nav__link">
										<span class="m-nav__link-text">Jobwork</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<!-- <form class="m-form m-form--state m-form--fit m-form--label-align-left" id="jobwork_form" action="/jobwork/save" method="post"> -->	
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									
									<div class="m-portlet__body" >
										<!--begin: Form Wizard-->
										<div class="m-wizard m-wizard--3 m-wizard--success" id="m_wizard">
								
											<!--begin: Message container -->
										    <div class="m-portlet__padding-x">
										        <!-- Here you can put a message or alert -->
										    </div>
										    <!--end: Message container -->
								
											<div class="row m-row--no-padding">
												<div class="col-xl-3 col-lg-12">
													<!--begin: Form Wizard Head -->
													<div class="m-wizard__head">
								
														<!--begin: Form Wizard Progress -->  		
														<div class="m-wizard__progress">	
															<div class="progress">		 
																<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>						 	
															</div>			 
														</div> 
											            <!--end: Form Wizard Progress --> 
								
											            <!--begin: Form Wizard Nav -->
														<%@include file="jobwork-step-nav.jsp" %>
														<div class="m-wizard__nav">
															<div class="m-wizard__steps ">
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_1">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step1_form').submit()">							 
																			<span><span>1</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Antri And Larring
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_2" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step2_form').submit()">							 
																			<span><span>2</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Farmo Type
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_3" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step3_form').submit()">							 
																			<span><span>3</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Stitching
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_4" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step4_form').submit()">							 
																			<span><span>4</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Measurement QC
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_5" > 
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step5_form').submit()">							 
																			<span><span>5</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Bal / Gaj
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_6" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step6_form').submit()">							 
																			<span><span>6</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Washing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_7">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number"  onclick="$('#goto_step7_form').submit()">							 
																			<span><span>7</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Measurement QC
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_8" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step8_form').submit()">							 
																			<span><span>8</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Pressing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_9" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step9_form').submit()">							 
																			<span><span>9</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Labeling / Finishing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_10">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step10_form').submit()">							 
																			<span><span>10</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Store
																		</div>
																	</div>
																</div>
															</div>
														</div>	
														<!--end: Form Wizard Nav -->
													</div>
													<!--end: Form Wizard Head -->	
												</div>
												<div class="col-xl-9 col-lg-12">
													<!--begin: Form Wizard Form-->
													<div class="m-wizard__form">
														<!--
															1) Use m-form--label-align-left class to alight the form input lables to the right
															2) Use m-form--state class to highlight input control borders on form validation
														-->
														<form class="m-form m-form--label-align-left- m-form--state-" id="form_step8" method="post" action="/jobwork/save">
															<input type="hidden" id="jobworkId" name="jobworkId" value="${jobworkVo.jobworkId}"/>
															<input type="hidden" id="process" name="process" value="${Constant.JOBWORK_PROCESS_PRESSING}"/>
															<input type="hidden" id="pressingReceiveStatus" name="pressingReceiveStatus" value="pending"/>
															<!--begin: Form Body -->
															<div class="m-portlet__body m-portlet__body--no-padding">
																<!--begin: Form Wizard Step 3-->
																<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_6">
																	<div class="m-form__section m-form__section--first">
																		<div class="m-form__heading">
																			<h3 class="m-form__heading-title">Pressing</h3>
																		</div>
																		<div class="row">
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Handover Quantity:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<input type="text" class="form-control m-input" name="pressingQty" id="pressingQty" placeholder="Pressing Qty" value="${empty jobworkVo.pressingReceiveStatus ? jobworkVo.washingQty: jobworkVo.pressingQty}"/>
																					</div>
																				</div>
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Handover By:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<select class="form-control m-select2" id="pressingHandoverBy" name="pressingHandoverBy.employeeId" placeholder="Select Handover By">
																							<option value="">Select Handover By</option>
																							<c:forEach items="${employeeVos}" var="employeeVo">
																								<option value="${employeeVo.employeeId}" <c:if test="${employeeVo.employeeId eq jobworkVo.pressingHandoverBy.employeeId}">selected='selected'</c:if>>
																									${employeeVo.employeeName}
																								</option>
																							</c:forEach>
																						</select>
																					</div>
																				</div>
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Handover Date:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<div class="input-group date" >
																							<input type="text" class="form-control m-input default-datetimepicker" placeholder="Select date and time" name="pressingHandoverDate" id="pressingHandoverDate" value="<fmt:formatDate pattern='dd/MM/yyyy hh:mm aa' value='${jobworkVo.pressingHandoverDate}' />"/>
																							<div class="input-group-append">
																								<span class="input-group-text">
																								<i class="la la-calendar glyphicon-th"></i>
																								</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Supplier:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<select class="form-control m-select2" id="contactVo" name="pressingContactVo.contactId" onchange="getContactInfo()" placeholder="Select Supplier">
																							<option value="">Select Supplier</option>
																							<c:forEach items="${contactVos}" var="contactVo">
																								<option value="${contactVo.contactId}" <c:if test="${contactVo.contactId eq jobworkVo.pressingContactVo.contactId}">selected='selected'</c:if> >
																									${contactVo.companyName}
																								</option>
																							</c:forEach>
																						</select>
																					</div>
																				</div>
																				<div class="m-section mt-3 m--margin-bottom-15 m--margin-left-15">
																					<%-- <c:if test="${contactAddress.isDeleted==0}"> --%>
																					<h3 class="m-section__heading">Address Details</h3>
																					<!-- <div class="m-divider"><span></span></div> -->
																					<h5 class=""><small class="text-muted" data-address-message="">Address is not provided</small></h5>
																					<div class="m-section__content m--hide" id="purchase_billing_address">
																						<div class="row">
																							<div class="col-lg-12 col-md-12 col-sm-12">																
																								<h5 class=""><small class="text-muted" data-address-name=""></small></h5>
																								<p class="mb-0">
																									<span data-address-line-1=""></span>
																								</p>
																								<p class="mb-0">
																									<span data-address-line-2=""></span>
																								</p>
																								<p class="mb-0">
																									<span data-address-pincode=""></span>
																									<span data-address-city=""></span>
																									<span class="m--font-boldest">,&nbsp;</span>
																								</p>
																								<p class="mb-0">
																									<span data-address-state=""></span>
																									<span class="m--font-boldest">,&nbsp;</span>
																									<span data-address-country=""></span>
																								</p>
																								<p class="mb-0">
																									<span class=""><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno=""></span></span>
																								</p>
																							</div>
																						</div>
																					</div>																				
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<!--end: Form Wizard Step 3-->
															</div>
															<!--end: Form Body -->
															<!--begin: Form Actions -->
															<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
																<div class="m-form__actions">
																	<div class="row">
																		<div class="col-lg-6 m--align-left">
																			<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" onclick="$('#previous_form').submit()">
																				<span>
																				<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																				<span>Back</span>
																				</span>
																			</button>
																		</div>
																		<div class="col-lg-6 m--align-right">
																			<c:if test='${not empty jobworkVo.pressingReceiveStatus}'>
																				<button type="button" class="btn btn-primary m-btn m-btn--custom m-btn--icon" id="pressing_receive">
																					<span><i class="la la-check"></i>&nbsp;&nbsp;<span>Pressing Receive</span></span>
																				</button>
																			</c:if>
																			<c:if test='${empty jobworkVo.pressingReceiveStatus || jobworkVo.pressingReceiveStatus == "pending"}'>
																				<button type="submit" id="form_save_step8" class="btn btn-success m-btn m-btn--custom m-btn--icon not-view" data-wizard-action="next">
																					<span><span>Save & Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																				</button>
																			</c:if>
																			<c:if test="${isEditable == false}">
																				<a href="/jobwork/${jobworkVo.jobworkId}" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
																					<span>
																						<span>Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																				</a>
																			</c:if>
																		</div>
																	</div>
																</div>
															</div>
															<!--end: Form Actions -->
														</form>
														<form class="m-form m-form--label-align-left- m-form--state- m--hide" id="form_step8_receive" method="post" action="/jobwork/save">
															<input type="hidden" id="jobworkId" name="jobworkId" value="${jobworkVo.jobworkId}"/>
															<input type="hidden" id="process" name="process" value="${Constant.JOBWORK_PROCESS_PRESSING}"/>
															<input type="hidden" id="pressingReceiveStatus" name="pressingReceiveStatus" value="complete"/>
															<!--begin: Form Body -->
															<div class="m-portlet__body m-portlet__body--no-padding">
																<!--begin: Form Wizard Step 3-->
																<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_6">
																	<div class="m-form__section m-form__section--first">
																		<div class="m-form__heading">
																			<h3 class="m-form__heading-title">Pressing Receive</h3>
																		</div>
																		
																		<div class="row">
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Receive Quantity:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<input type="text" class="form-control m-input" name="pressingReceivedQty" id="pressingReceivedQty" placeholder="Receive Quantity" value="${jobworkVo.pressingReceivedQty == 0 ? '' : jobworkVo.pressingReceivedQty}"/>
																					</div>
																				</div>
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Receive By:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<select class="form-control m-select2" id="receivedBy" name="pressingReceivedBy.employeeId" placeholder="Select Receive By">
																							<option value="">Select Handover By</option>
																							<c:forEach items="${employeeVos}" var="employeeVo">
																								<option value="${employeeVo.employeeId}" <c:if test="${employeeVo.employeeId eq jobworkVo.pressingReceivedBy.employeeId}">selected='selected'</c:if>>
																									${employeeVo.employeeName}
																								</option>
																							</c:forEach>
																						</select>
																					</div>
																				</div>
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Receive Date:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<div class="input-group date" >
																							<input type="text" class="form-control m-input default-datetimepicker" placeholder="Select date and time" id="pressingReceivedDate" name="pressingReceivedDate" value="<fmt:formatDate pattern='dd/MM/yyyy hh:mm aa' value='${jobworkVo.pressingReceivedDate}' />"/>
																							<div class="input-group-append">
																								<span class="input-group-text">
																								<i class="la la-calendar glyphicon-th"></i>
																								</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="m-widget28 m--margin-left-15">
																					<div class="m-widget28__container" >	
																						<!-- Start:: Content -->
																						<div class="m-widget28__tab tab-content">
																							<div class="m-widget28__tab-container tab-pane active">
																							    <div class="m-widget28__tab-items">
																									<div class="m-widget28__tab-item">
																										<span class="m--regular-font-size-">Supplier Name:</span>
																										<span>
																											<a href="<%=request.getContextPath() %>/contact/${jobworkVo.pressingContactVo.type}/${jobworkVo.pressingContactVo.contactId}" class="m-link m--font-boldest" target="_blank">
																												${jobworkVo.pressingContactVo.companyName}
																											</a>
																										</span>
																									</div>
																								</div>					      	 		      	
																							</div>
																						</div>
																						<!-- end:: Content --> 	
																					</div>				 	 
																				</div>
																				<c:set var="contactAddressVo" value="${jobworkVo.pressingContactVo.contactAddressVos.get(0)}"/>
																				<div class="m-section mt-3 m--margin-top-20 m--margin-left-15">
																					<%-- <c:if test="${contactAddress.isDeleted==0}"> --%>
																					<h3 class="m-section__heading">Address Details</h3>
																					<!-- <div class="m-divider"><span></span></div> -->
																					<h5 class=""><small class="text-muted m--hide" data-address-message="">Address is not provided</small></h5>
																					<div class="m-section__content">
																						<div class="row">
																							<div class="col-lg-12 col-md-12 col-sm-12">																
																								<h5 class=""><small class="text-muted" data-address-name="">${contactAddressVo.companyName}</small></h5>
																								<p class="mb-0">
																									<span data-address-line-1="">${contactAddressVo.addressLine1}</span>
																								</p>
																								<p class="mb-0">
																									<span data-address-line-2="">${contactAddressVo.addressLine2}</span>
																								</p>
																								<p class="mb-0">
																									<span data-address-pincode="">${contactAddressVo.pinCode}</span>
																									<span data-address-city="">${contactAddressVo.cityName}</span>
																									<span class="m--font-boldest">,&nbsp;</span>
																								</p>
																								<p class="mb-0">
																									<span data-address-state="">${contactAddressVo.stateName}</span>
																									<span class="m--font-boldest">,&nbsp;</span>
																									<span data-address-country="">${contactAddressVo.countriesName}</span>
																								</p>
																								<p class="mb-0">
																									<span class="">
																										<i class="la la-phone align-middle"></i> 
																										<span class="" data-address-phoneno="">
																											<c:if test="${empty jobworkVo.pressingContactVo.companyMobileno}">Mobile no. is not provided</c:if>${jobworkVo.pressingContactVo.companyMobileno}
																										</span>
																									</span>
																								</p>
																							</div>
																						</div>
																					</div>																				
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<!--end: Form Wizard Step 3-->
															</div>
															<!--end: Form Body -->
															<!--begin: Form Actions -->
															<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
																<div class="m-form__actions">
																	<div class="row">
																		<div class="col-lg-6 m--align-left">
																			<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" onclick="$('#previous_form').submit()">
																				<span>
																				<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																				<span>Back</span>
																				</span>
																			</button>
																		</div>
																		<div class="col-lg-6 m--align-right">
																			
																			<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" id="pressing_receive_cancel">
																				<span>Cancel</span>
																			</button>
																			
																			<c:if test='${jobworkVo.pressingReceiveStatus != "complete"}'>
																				<button type="submit" id="form_save_step8_receive" class="btn btn-success m-btn m-btn--custom m-btn--icon not-view" data-wizard-action="next">
																					<span><span>Save & Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																				</button>
																			</c:if>
																		</div>
																	</div>
																</div>
															</div>
															<!--end: Form Actions -->
														</form>
														<form action="/jobwork/${jobworkVo.jobworkId}" method="post" id="previous_form">
															<input type="hidden" name="previousProcess" value="${Constant.JOBWORK_PROCESS_MEASUREMENT_QC2}"/>
														</form>
													</div>
													<!--end: Form Wizard Form-->
								
												</div>
											</div>
										</div>
										<!--end: Form Wizard-->
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
						</div>
					<!-- </form> -->
				</div>
			</div>
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	<script type="text/javascript">
	
		if(!${isEditable}) {
			$(".not-view").remove();
			$(".default-datetimepicker").removeClass("default-datetimepicker");
			$.each($('input, select ,textarea', '#form_step8,#form_step8_receive'),function(k){
				$(this).attr("readonly", "true")
			});
		}
	</script>
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	<%-- <script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/wizard/wizard.js" type="text/javascript"></script> --%>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<%-- <script src="<%=request.getContextPath()%>/script/Jobwork/Jobwork-script.js" type="text/javascript"></script> --%>
	
	<script type="text/javascript">
		
			
		$(document).ready(function(){
			getContactInfo();
			
			$("#form_step8").formValidation({
				framework : 'bootstrap',
				live:'disabled',
				excluded : ":disabled",
				icon : null,
				button: {
					selector: "#form_save_step8",
					disabled: "disabled"
				},
				fields : {
					pressingQty : {
						validators : {
							notEmpty : {
								message : 'The Qty is required'
							},
							stringLength : {
								max : 20,
								message : 'The Qty must be less than 20 characters long'
							},
							regexp : {
								regexp:/^((\d+)((\.\d{0,4})?))$/,
								message : 'The Qty is invalid'
							}
						}
					},
					"pressingHandoverBy.employeeId":{
						validators: {
							notEmpty: {
								message: 'The Handover By is required'
							},
						}
					},
					"pressingContactVo.contactId":{
						validators: {
							notEmpty: {
								message: 'The Supplier is required'
							},
						}
					},
					pressingHandoverDate:{
						validators: {
							notEmpty: {
								message: 'The Pressing Handover Date is required'
							},
						}
					},
					
				}
			});
			
			$("#pressingHandoverDate").change(function() {
				$('#form_step8').formValidation('revalidateField', 'pressingHandoverDate');
			});
			
			$("#form_save_step8").on('click', function(){
				
				if ($('#form_step8').data('formValidation').isValid() == null) {
					$('#form_step8').data('formValidation').validate();
				}
				
				if($('#form_step8').data('formValidation').isValid() == true) {
					$('#form_step8_receive').remove();
				} else {
					return false;
				}
			});
			
			$("#pressing_receive").click(function(){
				$("#form_step8_receive").removeClass("m--hide");
				$("#form_step8").addClass("m--hide");
			});
			
		});
		
		function getContactInfo() {
			if($("#contactVo").val() != ''){
				var id=$("#contactVo").val();
				
				$.post("/contact/suppliers/"+id+"/json", {
					
				}, function (data, status) {
					
					
					if(status == 'success') {
						
						address=data.contactAddressVos;
						
						
						$.each(data.contactAddressVos, function( key, value ) {
							
							if(key == 0) {
								
								$("#purchase_billing_address").find("[data-address-name]").html(value.companyName).end()
									.find("[data-address-line-1]").html(value.addressLine1).end()
									.find("[data-address-line-2]").html(value.addressLine2).end()
									.find("[data-address-pincode]").html(value.pinCode).end()
									.find("[data-address-city]").html(value.cityName).end()
									.find("[data-address-state]").html(value.stateName).end()
									.find("[data-address-country]").html(value.countriesName).end()
									.find("[data-address-city]").attr("data-address-city",value.cityCode).end()
									.find("[data-address-state]").attr("data-address-state",value.stateCode).end()
									.find("[data-address-country]").attr("data-address-country",value.countriesCode).end()
									.find("[data-address-phoneNo]").html(data.companyMobileno == "" ? "Mobile no. is not provided": data.companyMobileno).end()
									.removeClass("m--hide").end()
									.find("[data-address-message]").addClass("m--hide").end();
								
								$("#purchase_billing_address").parent().find("[data-address-message]").addClass("m--hide").end();
							}
						});
						
					} else {
						console.log("status: "+status);
					}
				});
			}
		}
		/**
		*** QUALITY CHECK
		*/
		
		$(document).ready(function(){
			
			$("#form_step8_receive").formValidation({
				framework : 'bootstrap',
				live:'disabled',
				button:{
					selector : "#form_save_step8_receive",
					disabled : "disabled",
				},
				excluded : ":disabled",
				icon : null,
				fields : {
					pressingReceivedQty : {
						validators : {
							notEmpty : {
								message : 'The Qty is required. '
							},
							stringLength : {
								max : 20,
								message : 'The Qty must be less than 20 characters long. '
							},
							regexp : {
								regexp:/^((\d+)((\.\d{0,4})?))$/,
								message : 'The Qty is invalid. '
							}
						}
					}, "pressingReceivedBy.employeeId":{
						validators: {
							notEmpty: {
								message: 'The Received By is required. '
							},
						}
					}, pressingReceivedDate:{
						validators: {
							notEmpty: {
								message: 'The Pressing Received Date is required. '
							},
						}
					},
				}
			});
			
			$("#pressing_receive_cancel").click(function(){
				$("#form_step8_receive").addClass("m--hide");
				$("#form_step8").removeClass("m--hide");
			});
			
			$("#pressingReceivedDate").change(function() {
				$('#form_step8_receive').formValidation('revalidateField', 'pressingReceivedDate');
			});
			
			$("#form_save_step8_receive").on("click", function() {
				
				if ($('#form_step8_receive').data('formValidation').isValid() == null) {
					$('#form_step8_receive').data('formValidation').validate();
				}
				
				if($('#form_step8_receive').data('formValidation').isValid() == true) {
					$('#form_step8').remove();
				} else {
					return false;
				}
				
			});
		});
	</script>
</body>
<!-- end::Body -->
</html>