<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${jobworkVo.prefix}${jobworkVo.jobworkNo} | Jobwork</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		/* .m-table tr.m-table__row--brand span {
			color: #fff !important;
		} */
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
		.m-wizard.m-wizard--3 .m-wizard__head {
		    padding: 2rem 1rem !important;
		}
		
		.m-wizard.m-wizard--3 .m-wizard__form {
			padding: 2rem 4rem 3rem 4rem;
		}
		
	</style>
	
	<c:if test="${!isEditable}">
		<style type="text/css">
			select[readonly].select2-hidden-accessible + .select2-container {
			  pointer-events: none;
			  touch-action: none;
			}
		</style>
	</c:if>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${jobworkVo.prefix}${jobworkVo.jobworkNo}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/jobwork" class="m-nav__link">
										<span class="m-nav__link-text">Jobwork</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<!-- <form class="m-form m-form--state m-form--fit m-form--label-align-left" id="jobwork_form" action="/jobwork/save" method="post"> -->	
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									
									<div class="m-portlet__body" >
										<!--begin: Form Wizard-->
										<div class="m-wizard m-wizard--3 m-wizard--success" id="m_wizard">
								
											<!--begin: Message container -->
										    <div class="m-portlet__padding-x">
										        <!-- Here you can put a message or alert -->
										    </div>
										    <!--end: Message container -->
								
											<div class="row m-row--no-padding">
												<div class="col-xl-3 col-lg-12">
													<!--begin: Form Wizard Head -->
													<div class="m-wizard__head">
								
														<!--begin: Form Wizard Progress -->  		
														<div class="m-wizard__progress">	
															<div class="progress">		 
																<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>						 	
															</div>			 
														</div> 
											            <!--end: Form Wizard Progress --> 
								
											            <!--begin: Form Wizard Nav -->
														<%@include file="jobwork-step-nav.jsp" %>
														<div class="m-wizard__nav">
															<div class="m-wizard__steps ">
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_1">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step1_form').submit()">							 
																			<span><span>1</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Antri And Larring
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_2" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step2_form').submit()">							 
																			<span><span>2</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Farmo Type
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_3" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step3_form').submit()">							 
																			<span><span>3</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Stitching
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_4" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step4_form').submit()">							 
																			<span><span>4</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Measurement QC
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_5" > 
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step5_form').submit()">							 
																			<span><span>5</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Bal / Gaj
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_6" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step6_form').submit()">							 
																			<span><span>6</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Washing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_7">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number"  onclick="$('#goto_step7_form').submit()">							 
																			<span><span>7</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Measurement QC
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_8" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step8_form').submit()">							 
																			<span><span>8</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Pressing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_9" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step9_form').submit()">							 
																			<span><span>9</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Labeling / Finishing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_10">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step10_form').submit()">							 
																			<span><span>10</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Store
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!--end: Form Wizard Nav -->
													</div>
													<!--end: Form Wizard Head -->	
												</div>
												<div class="col-xl-9 col-lg-12">
													<!--begin: Form Wizard Form-->
													<div class="m-wizard__form">
														<form class="m-form m-form--label-align-left- m-form--state-" method="POST" action="/jobwork/save" id="form_step2">
															
															<input type="hidden" id="jobworkId" name="jobworkId" value="${jobworkVo.jobworkId}"/>
															<input type="hidden" id="jobworkFarmoTypeVos.process" name="jobworkFarmoTypeVos[0].process" value="${Constant.JOBWORK_PROCESS_FARMO_TYPE}"/> <!-- process to identify which jobwork farmotype it is. Eg. For farmotype Or measurment qc1 Or measurment qc2      -->
															<c:if test="${not empty jobworkVo.jobworkFarmoTypeVos}">
																<c:set scope="page" var="jobworkFarmoTypeVosFarmoType" value="${jobworkVo.jobworkFarmoTypeVos.stream().filter(p->p.process == Constant.JOBWORK_PROCESS_FARMO_TYPE).toList().get(0)}"></c:set>
																<input type="hidden" id="jobworkFarmoTypeVo.jobworkFarmoTypeId" name="jobworkFarmoTypeVos[0].jobworkFarmoTypeId" value="${jobworkFarmoTypeVosFarmoType.jobworkFarmoTypeId}"/>
															</c:if>
															<%-- <c:forEach items="${jobworkVo.jobworkFarmoTypeParticularVos}" var="jobworkFarmoTypeParticularVo" >
																<input type="hidden" id="jobworkFarmoTypeVo.jobworkFarmoTypeId" name="jobworkVo.jobworkFarmoTypeParticularVos[].jobworkFarmoTypeParticularsId" value="${jobworkFarmoTypeParticularVo.jobworkFarmoTypeParticularsId}"/>
															</c:forEach> --%>
															<input type="hidden" id="process" name="process" value="${Constant.JOBWORK_PROCESS_FARMO_TYPE}"/>
															<!--begin: Form Body -->
															<div class="m-portlet__body m-portlet__body--no-padding">
																<!--begin: Form Wizard Step 2-->
																<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_2">
																	<div class="m-form__section m-form__section--first">
																		
																		<div class="m-form__heading">
																			<h3 class="m-form__heading-title">Farmo Details</h3>
																		</div>
																		<div class="form-group m-form__group row">
																			<div class="col-lg-6">
																				<label class="form-control-label">Farmo Type:</label>
																				<select class="form-control m-select2" id="farmoTypeVo" name="jobworkFarmoTypeVos[0].farmoTypeVo.farmoTypeId" placeholder="Select Farmo Type">
																					<option value="">Select Farmo Type</option>
																					<c:forEach items="${farmoTypeVos}" var="farmoTypeVos">
																						<option value="${farmoTypeVos.farmoTypeId}" <c:if test="${farmoTypeVos.farmoTypeId eq jobworkFarmoTypeVosFarmoType.farmoTypeVo.farmoTypeId}">selected="selected"</c:if>>${farmoTypeVos.farmoTypeName}</option>
																					</c:forEach>
																				</select>
																			</div>
																		</div>
																		<div class="m-separator m-separator--dashed m-separator--lg"></div>
																		<div class="form-group m-form__group row">
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="m-form__heading">
																					<h3 class="m-form__heading-title">Front Details</h3>
																				</div>
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Front length :</label>
																							<div class="col-lg-7 col-md-7 col-sm-12">
																								<div class="input-group">
																									<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].frontLength" id="frontLength" value="${jobworkFarmoTypeVosFarmoType.frontLength}"/>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Front seat :</label>
																							<div class="col-lg-7 col-md-7 col-sm-12">
																								<div class="input-group">
																									<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].frontSeat" id="frontSeat" value="${jobworkFarmoTypeVosFarmoType.frontSeat}"/>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Front thai :</label>
																							<div class="col-lg-7 col-md-7 col-sm-12">
																								<div class="input-group">
																									<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].frontThai" id="frontThai" value="${jobworkFarmoTypeVosFarmoType.frontThai}"/>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Front langot :</label>
																							<div class="col-lg-7 col-md-7 col-sm-12">
																								<div class="input-group">
																									<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].frontLangot" id="frontLangot" value="${jobworkFarmoTypeVosFarmoType.frontLangot}"/>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Front knee :</label>
																							<div class="col-lg-7 col-md-7 col-sm-12">
																								<div class="input-group">
																									<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].frontKnee" id="frontKnee" value="${jobworkFarmoTypeVosFarmoType.frontKnee}"/>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Front mori-munda :</label>
																							<div class="col-lg-7 col-md-7 col-sm-12">
																								<div class="input-group">
																									<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].frontMoriMunda" id="frontMoriMunda" value="${jobworkFarmoTypeVosFarmoType.frontMoriMunda}"/>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Front jati :</label>
																							<div class="col-lg-7 col-md-7 col-sm-12">
																								<div class="input-group">
																									<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].frontJati" id="frontJati" value="${jobworkFarmoTypeVosFarmoType.frontJati}"/>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="m-form__heading">
																					<h3 class="m-form__heading-title">Back Details</h3>
																				</div>
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Back length :</label>
																							<div class="col-lg-7 col-md-7 col-sm-12">
																								<div class="input-group">
																									<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].backLength" id="backLength" value="${jobworkFarmoTypeVosFarmoType.backLength}"/>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Back seat :</label>
																							<div class="col-lg-7 col-md-7 col-sm-12">
																								<div class="input-group">
																									<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].backSeat" id="backSeat" value="${jobworkFarmoTypeVosFarmoType.backSeat}"/>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Back thai :</label>
																							<div class="col-lg-7 col-md-7 col-sm-12">
																								<div class="input-group">
																									<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].backThai" id="backThai" value="${jobworkFarmoTypeVosFarmoType.backThai}"/>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Back langot :</label>
																							<div class="col-lg-7 col-md-7 col-sm-12">
																								<div class="input-group">
																									<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].backLangot" id="backLangot" value="${jobworkFarmoTypeVosFarmoType.backLangot}"/>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Back knee :</label>
																							<div class="col-lg-7 col-md-7 col-sm-12">
																								<div class="input-group">
																									<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].backKnee" id="backKnee" value="${jobworkFarmoTypeVosFarmoType.backKnee}"/>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Back mori-munda :</label>
																							<div class="col-lg-7 col-md-7 col-sm-12">
																								<div class="input-group">
																									<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].backMoriMunda" id="backMoriMunda" value="${jobworkFarmoTypeVosFarmoType.backMoriMunda}"/>
																								</div>
																							</div>
																						</div>
																					</div>
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Back jati :</label>
																							<div class="col-lg-7 col-md-7 col-sm-12">
																								<div class="input-group">
																									<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].backJati" id="backJati" value="${jobworkFarmoTypeVosFarmoType.backJati}"/>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="m-separator m-separator--dashed m-separator--lg"></div>
																		<div class="m-form__heading">
																			<h3 class="m-form__heading-title">Farmo Particular Details</h3>
																		</div>
																		
																		<div class="form-group m-form__group row" id="particularDiv" >
																			<div class="col-lg-12 col-md-12 col-sm-12 <c:if test="${not empty jobworkVo.jobworkFarmoTypeParticularVos}">m--hide</c:if>" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12" data-particular-label="">No farmo type selected</label>
																						</div>
																					</div>
																				</div>
																			</div>
																			<c:set scope="page" var="farmoParticularIndex" value="0"></c:set>
																			<c:forEach items="${jobworkVo.jobworkFarmoTypeParticularVos.stream().filter(aa -> aa.process == Constant.JOBWORK_PROCESS_FARMO_TYPE).toList()}" var="jobworkFarmoTypeParticularVo">
																				<div class="col-lg-6 col-md-6 col-sm-12" >
																					<div class="row">
																						<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																							<div class="form-group m-form__group row">
																								<label class="col-form-label col-lg-5 col-md-5 col-sm-12">${jobworkFarmoTypeParticularVo.farmoParticularVo.name}</label>
																								<div class="col-lg-7 col-md-7 col-sm-12">
																									<div class="input-group">
																										<input type="hidden" id="jobworkFarmoTypeVo.jobworkFarmoTypeId" name="jobworkFarmoTypeParticularVos[${farmoParticularIndex}].process" value="${jobworkFarmoTypeParticularVo.process}"/>
																										<input type="hidden" id="jobworkFarmoTypeVo.jobworkFarmoTypeId" name="jobworkFarmoTypeParticularVos[${farmoParticularIndex}].jobworkFarmoTypeParticularsId" value="${jobworkFarmoTypeParticularVo.jobworkFarmoTypeParticularsId}"/>
																										<input type="hidden" id="jobworkFarmoTypeVo.jobworkFarmoTypeId" name="jobworkFarmoTypeParticularVos[${farmoParticularIndex}].farmoParticularVo.farmoParticularId" value="${jobworkFarmoTypeParticularVo.farmoParticularVo.farmoParticularId}"/>
																										<input type="text" class="form-control m-input" name="jobworkFarmoTypeParticularVos[${farmoParticularIndex}].farmoParticularMeasurement" id="backJati" value="${jobworkFarmoTypeParticularVo.farmoParticularMeasurement}"/>
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			<c:set scope="page" var="farmoParticularIndex" value="${farmoParticularIndex+1}"></c:set>
																			</c:forEach>
																		</div>
																		<div class="m-separator m-separator--dashed m-separator--lg"></div>
																		<div class="row">
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Drawing By:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<select class="form-control m-select2" id="drawingBy" name="drawingBy.employeeId" placeholder="Select Design By">
																							<option value="">Select Drawing By</option>
																							<c:forEach items="${employeeVos}" var="employeeVo">
																								<option value="${employeeVo.employeeId}" <c:if test="${employeeVo.employeeId eq jobworkVo.drawingBy.employeeId}">selected="selected"</c:if>>
																									${employeeVo.employeeName}
																								</option>
																							</c:forEach>
																						</select>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Drawing Date:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<div class="input-group date" >
																							<input type="text" class="form-control m-input default-datetimepicker" value="<fmt:formatDate pattern='dd/MM/yyyy hh:mm aa' value='${jobworkVo.drawingDate}'/>" name="drawingDate" placeholder="Select date and time" id="drawingDate"/>
																							<div class="input-group-append">
																								<span class="input-group-text">
																								<i class="la la-calendar glyphicon-th"></i>
																								</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Drawing Approve By:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<select class="form-control m-select2" id="drawingApproveBy" name="drawingApproveBy.employeeId" placeholder="Select Design Approve By">
																							<option value="">Select Drawing Approve By</option>
																							<c:forEach items="${employeeVos}" var="employeeVo">
																								<option value="${employeeVo.employeeId}" <c:if test="${employeeVo.employeeId eq jobworkVo.drawingApproveBy.employeeId}">selected="selected"</c:if>>
																									${employeeVo.employeeName}
																								</option>
																							</c:forEach>
																						</select>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Drawing Approve Date:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<div class="input-group date" >
																							<input type="text" class="form-control m-input default-datetimepicker" name="drawingApproveDate" value="<fmt:formatDate pattern='dd/MM/yyyy hh:mm aa' value='${jobworkVo.drawingApproveDate}'/>" placeholder="Select date and time" id="drawingApproveDate"/>
																							<div class="input-group-append">
																								<span class="input-group-text">
																								<i class="la la-calendar glyphicon-th"></i>
																								</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Cutting By:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<select class="form-control m-select2" id="cuttingBy" name="cuttingBy.employeeId" placeholder="Select Cutting By">
																							<option value="">Select Cutting By</option>
																							<c:forEach items="${employeeVos}" var="employeeVo">
																								<option value="${employeeVo.employeeId}" <c:if test="${employeeVo.employeeId eq jobworkVo.cuttingBy.employeeId}">selected="selected"</c:if>>
																									${employeeVo.employeeName}
																								</option>
																							</c:forEach>
																						</select>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Cutting Date:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<div class="input-group date" >
																							<input type="text" class="form-control m-input default-datetimepicker" name="cuttingDate" placeholder="Select date and time" id="cuttingDate"
																								value="<fmt:formatDate pattern='dd/MM/yyyy hh:mm aa' value='${jobworkVo.cuttingDate}' />"/>
																							<div class="input-group-append">
																								<span class="input-group-text">
																								<i class="la la-calendar glyphicon-th"></i>
																								</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Cutting Approve By:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<select class="form-control m-select2" id="cuttingApproveBy" name="cuttingApproveBy.employeeId" placeholder="Select Cutting Approve By">
																							<option value="">Select Cutting Approve By</option>
																							<c:forEach items="${employeeVos}" var="employeeVo">
																								<option value="${employeeVo.employeeId}" <c:if test="${employeeVo.employeeId eq jobworkVo.cuttingApproveBy.employeeId}">selected="selected"</c:if>>
																									${employeeVo.employeeName}
																								</option>
																							</c:forEach>
																						</select>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Cutting Approve Date:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<div class="input-group date" >
																							<input type="text" class="form-control m-input default-datetimepicker" name="cuttingApproveDate" value="<fmt:formatDate pattern='dd/MM/yyyy hh:mm aa' value='${jobworkVo.cuttingApproveDate}' />" placeholder="Select date and time" id="cuttingApproveDate"/>
																							<div class="input-group-append">
																								<span class="input-group-text">
																								<i class="la la-calendar glyphicon-th"></i>
																								</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Sort By:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<select class="form-control m-select2" id="sortBy" name="sortBy.employeeId" placeholder="Select Sort By">
																							<option value="">Select Sort By</option>
																							<c:forEach items="${employeeVos}" var="employeeVo">
																								<option value="${employeeVo.employeeId}" <c:if test="${employeeVo.employeeId eq jobworkVo.sortBy.employeeId}">selected="selected"</c:if>>
																									${employeeVo.employeeName}
																								</option>
																							</c:forEach>
																						</select>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Sort Date:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<div class="input-group date" >
																							<input type="text" class="form-control m-input default-datetimepicker" name="sortDate" value="<fmt:formatDate pattern='dd/MM/yyyy hh:mm aa' value='${jobworkVo.sortDate}' />" placeholder="Select date and time" id="sortDate"/>
																							<div class="input-group-append">
																								<span class="input-group-text">
																								<i class="la la-calendar glyphicon-th"></i>
																								</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Sort Approve By:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<select class="form-control m-select2" id="sortApproveBy" name="sortApproveBy.employeeId" placeholder="Select Sort Approve By">
																							<option value="">Select Cutting Approve By</option>
																							<c:forEach items="${employeeVos}" var="employeeVo">
																								<option value="${employeeVo.employeeId}" <c:if test="${employeeVo.employeeId eq jobworkVo.sortApproveBy.employeeId}">selected="selected"</c:if>>
																									${employeeVo.employeeName}
																								</option>
																							</c:forEach>
																						</select>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-6 col-md-6 col-sm-12">
																				<div class="form-group m-form__group row">
																					<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Sort Approve Date:</label>
																					<div class="col-lg-12 col-md-12 col-sm-12">
																						<div class="input-group date" >
																							<input type="text" class="form-control m-input default-datetimepicker" name="sortApproveDate" value="<fmt:formatDate pattern='dd/MM/yyyy hh:mm aa' value='${jobworkVo.sortApproveDate}' />" placeholder="Select date and time" id="sortApproveDate"/>
																							<div class="input-group-append">
																								<span class="input-group-text">
																								<i class="la la-calendar glyphicon-th"></i>
																								</span>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<!--end: Form Wizard Step 2-->
															</div>
															<!--end: Form Body -->
															<!--begin: Form Actions -->
															<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
																<div class="m-form__actions">
																	<div class="row">
																		<div class="col-lg-6 m--align-left">
																			<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" onclick="$('#previous_form').submit()">
																				<span>
																				<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																				<span>Back</span>
																				</span>
																			</button>
																		</div>
																		<div class="col-lg-6 m--align-right">
																			<a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit">
																				<span>
																				<i class="la la-check"></i>&nbsp;&nbsp;
																				<span>Submit</span>
																				</span>
																			</a>
																			<c:if test="${isEditable == true}">
																				<button type="submit" id="form_save_step2" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
																					<span>
																					<span>Save & Continue</span>&nbsp;&nbsp;
																					<i class="la la-arrow-right"></i>
																					</span>
																				</button>
																			</c:if>
																			<c:if test="${isEditable == false}">
																				<a href="/jobwork/${jobworkVo.jobworkId}" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
																					<span>
																						<span>Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																				</a>
																			</c:if>
																		</div>
																	</div>
																</div>
															</div>
															<!--end: Form Actions -->
														</form>
														<form action="/jobwork/${jobworkVo.jobworkId}" method="post" id="previous_form">
															<input type="hidden" name="previousProcess" value="${Constant.JOBWORK_PROCESS_ANTRI_AND_LARRING}"/>
														</form>
													</div>
													<!--end: Form Wizard Form-->
								
												</div>
											</div>
										</div>
										<!--end: Form Wizard-->
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
						</div>
						<%-- <div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions--solid m-form__actions--right">
								<button type="submit" class="btn btn-brand" id="savedeliverychallan">
									Submit
								</button>
								
								<a href="<%=request.getContextPath()%>/deliverychallan" class="btn btn-secondary">
									Cancel
								</a>
							</div>
						</div> --%>
					<!-- </form> -->
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		<div class="form-group m-form__group row m--hide" id="particularTempletDiv" >
			<div class="col-lg-6 col-md-6 col-sm-12" >
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
						<div class="form-group m-form__group row">
							<label class="col-form-label col-lg-5 col-md-5 col-sm-12" data-particular-label="">Back jati :</label>
							<div class="col-lg-7 col-md-7 col-sm-12">
								<div class="input-group">
									<input type="hidden" class="form-control m-input" data-particular-input-hidden-id="" />
									<input type="hidden" class="form-control m-input" data-particular-input-hidden-process="" />
									<input type="text" class="form-control m-input" data-particular-input-text="" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	<script type="text/javascript">
		if(${!isEditable}) {
			$(".not-view").remove();
			$(".default-datetimepicker").removeClass("default-datetimepicker");
			$.each($('input, select ,textarea', '#form_step2'),function(k){
				$(this).attr("readonly", "true")
			});
		}
	</script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<script type="text/javascript">
		
		var measurementValidator = {
			validators : {
				notEmpty : {
					message : 'Please enter measurement. '
				},regexp : {
					regexp : /^[0-9.]+$/,
					message : 'Measurement is not valid, can contain only digits. '
				}
			}
		};
		var allField = 0 ;
		var k = ${farmoParticularIndex};
		$(document).ready(function() {
			
			$("#form_step2").formValidation({
				framework : 'bootstrap',
				live:'disabled',
				excluded : ":disabled",
				icon : null,
				button: {
					selector: "#form_save_step2",
					disabled: "disabled"
				},
				fields : {
					"jobworkFarmoTypeVos[0].farmoTypeVo.farmoTypeId":{
						validators: {
							notEmpty: {
								message: 'The Farmo Type is required'
							},
						}
					},
				}
			});
			
			$('#form_step2').formValidation('addField',"jobworkFarmoTypeVos[0].frontLength", measurementValidator);
			$('#form_step2').formValidation('addField',"jobworkFarmoTypeVos[0].frontSeat", measurementValidator);
			$('#form_step2').formValidation('addField',"jobworkFarmoTypeVos[0].frontThai", measurementValidator);
			$('#form_step2').formValidation('addField',"jobworkFarmoTypeVos[0].frontLangot", measurementValidator);
			$('#form_step2').formValidation('addField',"jobworkFarmoTypeVos[0].frontKnee", measurementValidator);
			$('#form_step2').formValidation('addField',"jobworkFarmoTypeVos[0].frontMoriMunda", measurementValidator);
			$('#form_step2').formValidation('addField',"jobworkFarmoTypeVos[0].frontJati", measurementValidator);
			$('#form_step2').formValidation('addField',"jobworkFarmoTypeVos[0].backLength", measurementValidator);
			$('#form_step2').formValidation('addField',"jobworkFarmoTypeVos[0].backSeat", measurementValidator);
			$('#form_step2').formValidation('addField',"jobworkFarmoTypeVos[0].backThai", measurementValidator);
			$('#form_step2').formValidation('addField',"jobworkFarmoTypeVos[0].backLangot", measurementValidator);
			$('#form_step2').formValidation('addField',"jobworkFarmoTypeVos[0].backKnee", measurementValidator);
			$('#form_step2').formValidation('addField',"jobworkFarmoTypeVos[0].backMoriMunda", measurementValidator);
			$('#form_step2').formValidation('addField',"jobworkFarmoTypeVos[0].backJati", measurementValidator);
			
			for(var i = 0; i<k; i++) {
				$('#form_step2').formValidation('addField',"jobworkFarmoTypeParticularVos["+i+"].farmoParticularMeasurement", measurementValidator);
			}
			$("#farmoTypeVo").on("change", function(){
				$.ajax({
				  	method: "GET",
				  	url:"/farmotype/json",
				  	data: { farmoTypeId: $('#farmoTypeVo').val() }
				}).done(function( data ) {
					$("#frontLength").val(data.frontLength)
					$("#frontSeat").val(data.frontSeat)
					$("#frontThai").val(data.frontThai)
					$("#frontLangot").val(data.frontLangot)
					$("#frontKnee").val(data.frontKnee)
					$("#frontMoriMunda").val(data.frontMoriMunda)
					$("#frontJati").val(data.frontJati)
					
					$("#backLength").val(data.backLength);
					$("#backSeat").val(data.backSeat);
					$("#backThai").val(data.backThai);
					$("#backLangot").val(data.backLangot);
					$("#backKnee").val(data.backKnee);
					$("#backMoriMunda").val(data.backMoriMunda);
					$("#backJati").val(data.backJati);
					
					for(var i = 0; i<k; i++) {
						$('#form_step2').formValidation('removeField',"jobworkFarmoTypeParticularVos["+i+"].farmoParticularMeasurement");
					}
					
					$("#particularDiv").html('');
					
					k=0;
					
					$.each(data.farmoTypeParticularVos, function(key,value){
						var id = "#particularTempletDiv"
						$(id).find("[data-particular-label]").html(value.farmoParticularVo.name+" :").end()
							 .find("[data-particular-input-text]").attr('value',value.farmoParticularMeasurement).end()
							 .find("[data-particular-input-text]").attr('name',"jobworkFarmoTypeParticularVos["+k+"].farmoParticularMeasurement").end()
							 .find("[data-particular-input-hidden-id]").attr('value',value.farmoParticularVo.farmoParticularId).end()
							 .find("[data-particular-input-hidden-id]").attr('name',"jobworkFarmoTypeParticularVos["+k+"].farmoParticularVo.farmoParticularId").end()
							 .find("[data-particular-input-hidden-process]").attr('value','${Constant.JOBWORK_PROCESS_FARMO_TYPE}').end()
							 .find("[data-particular-input-hidden-process]").attr('name',"jobworkFarmoTypeParticularVos["+k+"].process").end()
							 
						
						$("#particularDiv").html($("#particularDiv").html()+$("#particularTempletDiv").html());
						
						k++;
					});
					
					for(var i = 0; i<k; i++) {
						$('#form_step2').formValidation('addField',"jobworkFarmoTypeParticularVos["+i+"].farmoParticularMeasurement", measurementValidator);
					}
					$('#form_step2').formValidation('revalidateField',"jobworkFarmoTypeVos[0].frontLength");
					$('#form_step2').formValidation('revalidateField',"jobworkFarmoTypeVos[0].frontSeat");
					$('#form_step2').formValidation('revalidateField',"jobworkFarmoTypeVos[0].frontThai");
					$('#form_step2').formValidation('revalidateField',"jobworkFarmoTypeVos[0].frontLangot");
					$('#form_step2').formValidation('revalidateField',"jobworkFarmoTypeVos[0].frontKnee");
					$('#form_step2').formValidation('revalidateField',"jobworkFarmoTypeVos[0].frontMoriMunda");
					$('#form_step2').formValidation('revalidateField',"jobworkFarmoTypeVos[0].frontJati");
					$('#form_step2').formValidation('revalidateField',"jobworkFarmoTypeVos[0].backLength");
					$('#form_step2').formValidation('revalidateField',"jobworkFarmoTypeVos[0].backSeat");
					$('#form_step2').formValidation('revalidateField',"jobworkFarmoTypeVos[0].backThai");
					$('#form_step2').formValidation('revalidateField',"jobworkFarmoTypeVos[0].backLangot");
					$('#form_step2').formValidation('revalidateField',"jobworkFarmoTypeVos[0].backKnee");
					$('#form_step2').formValidation('revalidateField',"jobworkFarmoTypeVos[0].backMoriMunda");
					$('#form_step2').formValidation('revalidateField',"jobworkFarmoTypeVos[0].backJati");
				});
			});
			
			$("#form_save_step2").on("click", function(){
				
				if ($('#form_step2').data('formValidation').isValid() == null) {
					$('#form_step2').data('formValidation').validate();
				}
				
				if ($('#form_step2').data('formValidation').isValid() == true) {
					$("#particularTempletDiv").html('')
					$.each($('input, select ,textarea', '#form_step2'),function(k){
						if($("#"+$(this).attr('id')).val() == "" || $("#"+$(this).attr('id')).val() == "undefined"){
							$("#"+$(this).attr('id')).attr("disabled", "disabled")
						}
					});
				} else {
					return false;
				}
				
			});
			
			
		});//End Document Ready
		
	</script>
</body>
<!-- end::Body -->
</html>