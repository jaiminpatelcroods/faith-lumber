<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${jobworkVo.prefix}${jobworkVo.jobworkNo} | Jobwork</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		/* .m-table tr.m-table__row--brand span {
			color: #fff !important;
		} */
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
		.m-wizard.m-wizard--3 .m-wizard__head {
		    padding: 2rem 1rem !important;
		}
		
		.m-wizard.m-wizard--3 .m-wizard__form {
			padding: 2rem 4rem 3rem 4rem;
		}
		
	</style>
	
	<c:if test="${!isEditable}">
		<style type="text/css">
			select[readonly].select2-hidden-accessible + .select2-container {
			  pointer-events: none;
			  touch-action: none;
			}
		</style>
	</c:if>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${jobworkVo.prefix}${jobworkVo.jobworkNo}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/jobwork" class="m-nav__link">
										<span class="m-nav__link-text">Jobwork</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<!-- <form class="m-form m-form--state m-form--fit m-form--label-align-left" id="jobwork_form" action="/jobwork/save" method="post"> -->	
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									
									<div class="m-portlet__body" >
										
										<!--begin: Form Wizard-->
										<div class="m-wizard m-wizard--3 m-wizard--success" id="m_wizard">
								
											<!--begin: Message container -->
										    <div class="m-portlet__padding-x">
										        <!-- Here you can put a message or alert -->
										    </div>
										    <!--end: Message container -->
								
											<div class="row m-row--no-padding">
												<div class="col-xl-3 col-lg-12">
													<!--begin: Form Wizard Head -->
													<div class="m-wizard__head">
								
														<!--begin: Form Wizard Progress -->  		
														<div class="m-wizard__progress">	
															<div class="progress">		 
																<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>						 	
															</div>			 
														</div> 
											            <!--end: Form Wizard Progress --> 
								
											            <!--begin: Form Wizard Nav -->
														<%@include file="jobwork-step-nav.jsp" %>
														<div class="m-wizard__nav">
															<div class="m-wizard__steps ">
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_1">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step1_form').submit()">							 
																			<span><span>1</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Antri And Larring
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_2" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step2_form').submit()">							 
																			<span><span>2</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Farmo Type
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_3" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step3_form').submit()">							 
																			<span><span>3</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Stitching
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_4" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step4_form').submit()">							 
																			<span><span>4</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Measurement QC
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_5" > 
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step5_form').submit()">							 
																			<span><span>5</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Bal / Gaj
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_6" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step6_form').submit()">							 
																			<span><span>6</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Washing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_7">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number"  onclick="$('#goto_step7_form').submit()">							 
																			<span><span>7</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Measurement QC
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_8" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step8_form').submit()">							 
																			<span><span>8</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Pressing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_9" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step9_form').submit()">							 
																			<span><span>9</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Labeling / Finishing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_10">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step10_form').submit()">							 
																			<span><span>10</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Store
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!--end: Form Wizard Nav -->
													</div>
													<!--end: Form Wizard Head -->	
												</div>
												<div class="col-xl-9 col-lg-12">
													<!--begin: Form Wizard Form-->
													<div class="m-wizard__form">
														<!--
															1) Use m-form--label-align-left class to alight the form input lables to the right
															2) Use m-form--state class to highlight input control borders on form validation
														-->
														<c:if test="${empty jobworkVo.balGajLocation}">
															<div class="m-section" id="bal_gaj_location">
																<h3 class="m-section__heading">Select Bal/Gaj Process Location</h3>
																<div class="m-section__content">
																	<div class="m-demo">
																		<div class="m-demo__preview m-demo__preview--btn">
																			<div class="row">
																				<div class="col-lg-6 col-md-6 col-sm-12 text-center">
																					<button type="button" class="btn btn-brand btn-lg" onclick="setBalGajLocation(0)">In House</button>
																				</div>
																				<div class="col-lg-6 col-md-6 col-sm-12 text-center">
																					<button type="button" class="btn btn-brand btn-lg" onclick="setBalGajLocation(1)">Out Source</button>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</c:if>
														<c:if test="${empty jobworkVo.balGajLocation || jobworkVo.balGajLocation == 'in_house'}">
														
															<form class="m-form m-form--label-align-left- m-form--state-" id="form_step5" method="post" action="/jobwork/save">
																
																<input type="hidden" id="jobworkId" name="jobworkId" value="${jobworkVo.jobworkId}"/>
																<input type="hidden" id="deleteJobworkProcessVos" name="deleteJobworkProcessVos" value=""/>
																<input type="hidden" id="process" name="process" value="${Constant.JOBWORK_PROCESS_BAL_GAJ}"/>
																<input type="hidden" name="balGajLocation" value="in_house"/>
																<!--begin: Form Body -->
																<div class="m-portlet__body m-portlet__body--no-padding">
																	<!--begin: Form Wizard Step 3-->
																	<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_5">
																		<div class="m-form__section m-form__section--first">
																			<div class="m-form__heading">
																				<h3 class="m-form__heading-title">Bal/Gaj Process</h3>
																			</div>
																			
																			<c:if test='${jobworkVo.balGajQC == "complete"}'>
																				<div class="row">
																					<div class="col-lg-6 col-md-6 col-sm-12">
																						<div class="m-widget28">
																							<div class="m-widget28__container" >	
																								<!-- Start:: Content -->
																								<div class="m-widget28__tab tab-content">
																									<div class="m-widget28__tab-container tab-pane active">
																									    <div class="m-widget28__tab-items">
																											<div class="m-widget28__tab-item">
																												<span class="m--regular-font-size-">Total Qty:</span>
																												<span>${jobworkVo.balGajQty}</span>
																											</div>
																										</div>					      	 		      	
																									</div>
																								</div>
																								<!-- end:: Content --> 	
																							</div>				 	 
																						</div>
																					</div>
																				</div>
																			</c:if>
																			
																			<div class="table-responsive m--margin-top-20">
																				<table class="table m-table table-bordered table-hover" id="bal_gaj_process_table">
																				    <thead>
																				      <tr>
																				        <!-- <th>#</th> -->
																				        <th>#</th>
																				        <th>Bal/Gaj Particular</th>
																				        <th>Employee </th>
																				        <th class="text-right">Qty</th>
																				        <th class="text-right">Rate</th>
																				        <th></th>
																				        
																				      </tr>
																				    </thead>
																				    <tbody data-table-list="">
																				    	<c:set scope="page" var="tableIndex" value="0"/>
																					    <c:if test="${jobworkVo.jobworkProcessVos.size()==0}">
																					    	<c:forEach items="${productionProcessVos}" var="productionProcessVo">
																						    	<tr data-table-item="${tableIndex}" class="">
																						    		<td class="" style="width: 50px;">
																						    			<span data-item-index></span>
																						    		</td>
																							        <td class="" style="width: 370px;">
																							        	<div class="form-group m-form__group p-0">
																											${productionProcessVo.productionProcessName}
																											<input type="hidden" name="jobworkProcessVos[${tableIndex}].productionProcessVo.productionProcessId" value="${productionProcessVo.productionProcessId}"/>
																										</div>
																							        </td>
																							        <td class="p-0" style="width: 270px;">
																							        	<div class="form-group m-form__group p-0">
																											<select class="form-control m-input form-control-sm m-input1 m-select2" name="jobworkProcessVos[${tableIndex}].employeeVo.employeeId" id="employeeId${tableIndex}" placeholder="Select Employee">
																												<option value="">Select Employee</option>
																												<c:forEach items="${employeeVos}" var="employeeVo">
																													<option value="${employeeVo.employeeId}">
																														${employeeVo.employeeName}
																													</option>
																												</c:forEach>
																											</select>
																										</div>
																							        </td>
																							        <td class="p-0" style="width: 270px;">
																							        	<div class="form-group m-form__group p-0">
																							        		<div class="m-input-icon m-input-icon--right">
																											<input type="text" class="form-control form-control m-input1 text-right" id="qty${tableIndex}" name="jobworkProcessVos[${tableIndex}].qty" placeholder="Qty" value="">
																												<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																											</div>
																							        	</div>
																							        </td>
																							        <td class="p-0" style="width: 270px;">
																							        	<div class="form-group m-form__group p-0">
																							        		<div class="m-input-icon m-input-icon--right">
																												<input type="text" class="form-control form-control m-input1 text-right" id="rate${tableIndex}" name="jobworkProcessVos[${tableIndex}].rate" placeholder="Rate" value="">
																												<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																											</div>
																							        	</div>
																							        </td>
																							        <td class="p-0" style="width: 40px;">
																							        	
																							        </td>
																						    	</tr>
																						    	<c:set scope="page" var="tableIndex" value="${tableIndex+1}"/>
																						    </c:forEach>
																					    </c:if>
																					    
																					    <c:forEach items="${jobworkVo.jobworkProcessVos}" var="jobworkProcessVo">
																					    
																					    	<tr data-table-item="${tableIndex}" class="">
																					    		<td class="" style="width: 50px;">
																					    			<span data-item-index></span>
																					    		</td>
																						        <td class="" style="width: 370px;">
																						        	<div class="form-group m-form__group p-0">
																										${jobworkProcessVo.productionProcessVo.productionProcessName}
																										<input type="hidden" name="jobworkProcessVos[${tableIndex}].productionProcessVo.productionProcessId" value="${jobworkProcessVo.productionProcessVo.productionProcessId}"/>
																									</div>
																						        </td>
																						        <td class="p-0" style="width: 270px;">
																						        	<div class="form-group m-form__group p-0">
																										<select class="form-control m-input form-control-sm m-input1 m-select2" name="jobworkProcessVos[${tableIndex}].employeeVo.employeeId" id="employeeId${tableIndex}" placeholder="Select Employee">
																											<option value="">Select Employee</option>
																											<c:forEach items="${employeeVos}" var="employeeVo">
																												<option value="${employeeVo.employeeId}"
																													<c:if test="${jobworkProcessVo.employeeVo.employeeId == employeeVo.employeeId}">selected="selected"</c:if>>
																													${employeeVo.employeeName}
																												</option>
																											</c:forEach>
																										</select>
																									</div>
																						        </td>
																						        <td class="p-0" style="width: 270px;">
																						        	<div class="form-group m-form__group p-0">
																						        		<div class="m-input-icon m-input-icon--right">
																										<input type="text" class="form-control form-control m-input1 text-right" id="qty${tableIndex}" name="jobworkProcessVos[${tableIndex}].qty" placeholder="Qty" value="${jobworkProcessVo.qty}">
																											<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																										</div>
																						        	</div>
																						        </td>
																						        <td class="p-0" style="width: 270px;">
																						        	<div class="form-group m-form__group p-0">
																						        		<div class="m-input-icon m-input-icon--right">
																											<input type="text" class="form-control form-control m-input1 text-right" id="rate${tableIndex}" name="jobworkProcessVos[${tableIndex}].rate" placeholder="Rate" value="${jobworkProcessVo.rate}">
																											<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																										</div>
																						        	</div>
																						        </td>
																						        <td class="p-0" style="width: 40px;">
																						        	<c:if test="${jobworkProcessVo.jobworkProcessId != 0}">
																						        		<input type="hidden" name="jobworkProcessVos[${tableIndex}].jobworkProcessId" id="jobworkProcessId${tableIndex}" value="${jobworkProcessVo.jobworkProcessId}"/>
																						        	</c:if>
																						        </td>
																					    	</tr>
																					    	<c:set scope="page" var="tableIndex" value="${tableIndex+1}"/>
																					    </c:forEach>
																					</tbody>
																			  	</table>
																			</div>
																		</div>
																	</div>
																	<!--end: Form Wizard Step 3-->
																</div>
																<!--end: Form Body -->
																<!--begin: Form Actions -->
																<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
																	<div class="m-form__actions">
																		<div class="row">
																			<div class="col-lg-6 m--align-left">
																				<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" onclick="$('#previous_form').submit()">
																					<span>
																					<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																					<span>Back</span>
																					</span>
																				</button>
																			</div>
																			<div class="col-lg-6 m--align-right">
																				<c:if test='${empty jobworkVo.balGajLocation}'>
																					<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" onclick="changeBalGajLocation()">
																						<span>
																						<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																						<span>Change Bal/Gaj Location</span>
																						</span>
																					</button>
																				</c:if>
																				<c:if test='${not empty jobworkVo.balGajQC}'>
																					<button type="button" class="btn btn-primary m-btn m-btn--custom m-btn--icon" id="quality_check">
																						<span><i class="la la-check"></i>&nbsp;&nbsp;<span>Quality Check</span></span>
																					</button>
																				</c:if>
																				<c:if test='${empty jobworkVo.balGajQC || jobworkVo.balGajQC == "pending"}'>
																				<button type="submit" id="form_save_step5" class="btn btn-success m-btn m-btn--custom m-btn--icon not-view" data-wizard-action="next">
																					<span><span>Save & Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																				</button>
																				</c:if>
																				<c:if test="${isEditable == false}">
																					<a href="/jobwork/${jobworkVo.jobworkId}" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
																						<span>
																							<span>Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																					</a>
																				</c:if>
																			</div>
																		</div>
																	</div>
																</div>
																<!--end: Form Actions -->
															</form>
															<form class="m-form m-form--label-align-left- m-form--state- m--hide" id="form_step5_qc" method="post" action="/jobwork/save">
																
																<input type="hidden" id="jobworkId" name="jobworkId" value="${jobworkVo.jobworkId}"/>
																<input type="hidden" id="process" name="process" value="${Constant.JOBWORK_PROCESS_BAL_GAJ}"/>
																<input type="hidden" id="balGajQC" name="balGajQC" value="${jobworkVo.balGajQC}"/>
																
																<!--begin: Form Body -->
																<div class="m-portlet__body m-portlet__body--no-padding">
																	<!--begin: Form Wizard Step 3-->
																	<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_5">
																		<div class="m-form__section m-form__section--first">
																			<div class="row">
																				<div class="col-lg-6 col-md-6 col-sm-12">
																					<div class="form-group m-form__group row">
																						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Total Qty:</label>
																						<div class="col-lg-12 col-md-12 col-sm-12">
																							<input type="text" class="form-control m-input" name="balGajQty" placeholder="Total Qty" value='<c:if test="${jobworkVo.balGajQty != 0}">${jobworkVo.balGajQty}</c:if>' />
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="m-separator m-separator--dashed m-separator--lg m-1"></div>
																		<div class="m-form__section">
																			<div class="m-form__heading">
																				<h3 class="m-form__heading-title">Bal/Gaj Process Quality Check</h3>
																			</div>
																			
																			<div class="table-responsive m--margin-top-20" id="bal_gaj_quality_check_table">
																				<table class="table m-table table-bordered table-hover" style="display: inline-block; width: 1400px">
																				    <thead>
																				      <tr>
																				        <!-- <th>#</th> -->
																				        <th>#</th>
																				        <th>Bal/Gaj Particular</th>
																				        <th>Employee </th>
																				        <th>Qty</th>
																				        <th class="text-right">Rate</th>
																				        <th class="text-right">Qty</th>
																				        <th class="text-right qc-td">Pass Qty</th>
																				        <th class="text-right qc-td">Fail Qty</th>
																				        <th class="qc-td">Remark</th>
																				        <th class="qc-td">QC By</th>
																				      </tr>
																				    </thead>
																				    <tbody data-table-list="">
																				    	<c:set scope="page" var="qcTableIndex" value="0"/>
																					    <c:forEach items="${jobworkVo.jobworkProcessVos}" var="jobworkProcessVo">
																					    	<tr data-table-item="${qcTableIndex}" class="">
																					    		<td class="" style="width: 50px;">
																					    			<span data-item-index>${qcTableIndex+1}</span>
																					    		</td>
																						        <td class="" style="width: 370px;">
																						        	<div class="form-group m-form__group p-0">
																										${jobworkProcessVo.productionProcessVo.productionProcessName}
																									</div>
																						        </td>
																						        <td class="" style="width: 270px;">
																						        	<div class="form-group m-form__group p-0">
																						        		${jobworkProcessVo.employeeVo.employeeName}
																						        	</div>
																						        </td>
																						        <td class="" style="width: 270px;">
																						        	<div class="form-group m-form__group p-0">
																						        		${jobworkProcessVo.qty}
																						        	</div>
																						        </td>
																						         <td class="" style="width: 270px;">
																						        	<div class="form-group m-form__group p-0 text-right">
																						        		${jobworkProcessVo.rate}
																						        	</div>
																						        </td>
																						       <td class="p-0 qc-td" style="width: 270px;">
																						        	<div class="form-group m-form__group p-0">
																						        		<input type="text" class="form-control form-control m-input1 text-right" id="qcQty${qcTableIndex}" name="jobworkProcessVos[${qcTableIndex}].qcQty" placeholder="QC Qty" value="${jobworkProcessVo.qcQty}" onchange="changeQty(${qcTableIndex})"/>
																						        	</div>
																						        </td>
																						        <td class="p-0 qc-td" style="width: 270px;">
																						        	<div class="form-group m-form__group p-0">
																						        		<input type="text" class="form-control form-control m-input1 text-right" id="qcPassQty${qcTableIndex}" name="jobworkProcessVos[${qcTableIndex}].qcPassQty" placeholder="Pass Qty" value="${jobworkProcessVo.qcPassQty}" onchange="changeQty(${qcTableIndex})"/>
																						        	</div>
																						        </td>
																						        <td class="p-0 qc-td" style="width: 270px;">
																						        	<div class="form-group m-form__group p-0">
																						        		<input type="text" class="form-control form-control m-input1 text-right" id="qcFailQty${qcTableIndex}" name="jobworkProcessVos[${qcTableIndex}].qcFailQty" placeholder="Fail Qty" value="${jobworkProcessVo.qcFailQty}" onchange="changeQty(${qcTableIndex})"/>
																						        	</div>
																						        </td>
																						        <td class="p-0 qc-td" style="width: 270px;">
																						        	<div class="form-group m-form__group p-0">
																						        		<input type="text" class="form-control form-control m-input1 text-right" id="remark${qcTableIndex}" name="jobworkProcessVos[${qcTableIndex}].remark" placeholder="Remark" value="${jobworkProcessVo.remark}" />
																						        	</div>
																						        </td>
																						        <td class="p-0 qc-td" style="width: 270px;">
																						        	<div class="form-group m-form__group p-0">
																										<select class="form-control m-input form-control-sm m-select2 m-input1" name="jobworkProcessVos[${qcTableIndex}].qcBy.employeeId" id="qcBy${qcTableIndex}" placeholder="Select Employee">
																											<option value="">Select Employee</option>
																											<c:forEach items="${employeeVos}" var="employeeVo">
																												<option value="${employeeVo.employeeId}"
																													<c:if test="${employeeVo.employeeId == jobworkProcessVo.qcBy.employeeId}">selected="selected"</c:if>>
																													${employeeVo.employeeName}
																												</option>
																											</c:forEach>
																										</select>
																									</div>
																									<input type="hidden" name="jobworkProcessVos[${qcTableIndex}].jobworkProcessId" value="${jobworkProcessVo.jobworkProcessId}"/>
																									<input type="hidden" name="jobworkProcessVos[${qcTableIndex}].productionProcessVo.productionProcessId" value="${jobworkProcessVo.productionProcessVo.productionProcessId}"/>
																									<input type="hidden" name="jobworkProcessVos[${qcTableIndex}].process" value="${jobworkProcessVo.process}"/>
																									<input type="hidden" name="jobworkProcessVos[${qcTableIndex}].employeeVo.employeeId" value="${jobworkProcessVo.employeeVo.employeeId}"/>
																									<input type="hidden" name="jobworkProcessVos[${qcTableIndex}].qty" value="${jobworkProcessVo.qty}"/>
																									<input type="hidden" name="jobworkProcessVos[${qcTableIndex}].rate" value="${jobworkProcessVo.rate}"/>
																						        </td>
																						        <c:set scope="page" var="qcTableIndex" value="${qcTableIndex+1}"/>
																					    	</tr>
																					    </c:forEach>
																					</tbody>
																			  	</table>
																			</div>
																		</div>
																	</div>
																	<!--end: Form Wizard Step 3-->
																</div>
																<!--end: Form Body -->
																<!--begin: Form Actions -->
																<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
																	<div class="m-form__actions">
																		<div class="row">
																			<div class="col-lg-6 m--align-left">
																				<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" onclick="$('#previous_form').submit()">
																					<span>
																					<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																					<span>Back</span>
																					</span>
																				</button>
																			</div>
																			<div class="col-lg-6 m--align-right">
																				<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" id="quality_check_cancel">
																					<span>Cancel</span>
																				</button>
																				<c:if test='${jobworkVo.balGajQC != "complete"}'>
																					<button type="submit" id="form_save_step5_qc" class="btn btn-success m-btn m-btn--custom m-btn--icon not-view" data-wizard-action="next">
																						<span><span>Save & Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																					</button>
																				</c:if>
																			</div>
																		</div>
																	</div>
																</div>
																<!--end: Form Actions -->
															</form>
														</c:if>
														<c:if test="${empty jobworkVo.balGajLocation || jobworkVo.balGajLocation == 'out_source'}">
															<form class="m-form m-form--label-align-left- m-form--state- " id="form_step5_out_source" method="post" action="/jobwork/save">
																<input type="hidden" id="jobworkId" name="jobworkId" value="${jobworkVo.jobworkId}"/>
																<input type="hidden" id="process" name="process" value="${Constant.JOBWORK_PROCESS_BAL_GAJ}"/>
																<input type="hidden" name="balGajLocation" value="out_source"/>
																<input type="hidden" id="balGajQC" name="balGajQC" value="pending"/>
																<!--begin: Form Body -->
																<div class="m-portlet__body m-portlet__body--no-padding">
																	<!--begin: Form Wizard Step 3-->
																	<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_6">
																		<div class="m-form__section m-form__section--first">
																			<div class="m-form__heading">
																				<h3 class="m-form__heading-title">Bal/Gaj</h3>
																			</div>
																			<div class="row">
																				<div class="col-lg-6 col-md-6 col-sm-12">
																					<div class="form-group m-form__group row">
																						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Total / Handover Quantity:</label>
																						<div class="col-lg-12 col-md-12 col-sm-12">
																							<input type="text" class="form-control m-input" name="balGajQty" id="balGajQty" placeholder="Bal/Gaj Qty" value="${jobworkVo.balGajQty != 0 ? jobworkVo.balGajQty: ''}"/>
																						</div>
																					</div>
																					<div class="form-group m-form__group row">
																						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Handover By:</label>
																						<div class="col-lg-12 col-md-12 col-sm-12">
																							<select class="form-control m-select2" id="balGajHandoverBy" name="balGajHandoverBy.employeeId" placeholder="Select Handover By">
																								<option value="">Select Handover By</option>
																								<c:forEach items="${employeeVos}" var="employeeVo">
																									<option value="${employeeVo.employeeId}" <c:if test="${employeeVo.employeeId eq jobworkVo.balGajHandoverBy.employeeId}">selected='selected'</c:if>>
																										${employeeVo.employeeName}
																									</option>
																								</c:forEach>
																							</select>
																						</div>
																					</div>
																					<div class="form-group m-form__group row">
																						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Handover Date:</label>
																						<div class="col-lg-12 col-md-12 col-sm-12">
																							<div class="input-group date" >
																								<input type="text" class="form-control m-input default-datetimepicker" placeholder="Select date and time" name="balGajHandoverDate" id="balGajHandoverDate" value="<fmt:formatDate pattern='dd/MM/yyyy hh:mm aa' value='${jobworkVo.balGajHandoverDate}' />"/>
																								<div class="input-group-append">
																									<span class="input-group-text">
																									<i class="la la-calendar glyphicon-th"></i>
																									</span>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="col-lg-6 col-md-6 col-sm-12">
																					<div class="form-group m-form__group row">
																						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Supplier:</label>
																						<div class="col-lg-12 col-md-12 col-sm-12">
																							<select class="form-control m-select2" id="contactVo" name="balGajContactVo.contactId" onchange="getContactInfo()" placeholder="Select Supplier">
																								<option value="">Select Supplier</option>
																								<c:forEach items="${contactVos}" var="contactVo">
																									<option value="${contactVo.contactId}" <c:if test="${contactVo.contactId eq jobworkVo.balGajContactVo.contactId}">selected='selected'</c:if> >
																										${contactVo.companyName}
																									</option>
																								</c:forEach>
																							</select>
																						</div>
																					</div>
																					<div class="m-section mt-3 m--margin-bottom-15 m--margin-left-15">
																						<%-- <c:if test="${contactAddress.isDeleted==0}"> --%>
																						<h3 class="m-section__heading">Address Details</h3>
																						<!-- <div class="m-divider"><span></span></div> -->
																						<h5 class=""><small class="text-muted" data-address-message="">Address is not provided</small></h5>
																						<div class="m-section__content m--hide" id="purchase_billing_address">
																							<div class="row">
																								<div class="col-lg-12 col-md-12 col-sm-12">																
																									<h5 class=""><small class="text-muted" data-address-name=""></small></h5>
																									<p class="mb-0">
																										<span data-address-line-1=""></span>
																									</p>
																									<p class="mb-0">
																										<span data-address-line-2=""></span>
																									</p>
																									<p class="mb-0">
																										<span data-address-pincode=""></span>
																										<span data-address-city=""></span>
																										<span class="m--font-boldest">,&nbsp;</span>
																									</p>
																									<p class="mb-0">
																										<span data-address-state=""></span>
																										<span class="m--font-boldest">,&nbsp;</span>
																										<span data-address-country=""></span>
																									</p>
																									<p class="mb-0">
																										<span class=""><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno=""></span></span>
																									</p>
																								</div>
																							</div>
																						</div>																				
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<!--end: Form Wizard Step 3-->
																</div>
																<!--end: Form Body -->
																<!--begin: Form Actions -->
																<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
																	<div class="m-form__actions">
																		<div class="row">
																			<div class="col-lg-6 m--align-left">
																				<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" onclick="$('#previous_form').submit()">
																					<span>
																					<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																					<span>Back</span>
																					</span>
																				</button>
																			</div>
																			<div class="col-lg-6 m--align-right">
																				<c:if test='${empty jobworkVo.balGajLocation}'>
																					<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" onclick="changeBalGajLocation()">
																						<span>
																						<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																						<span>Change Bal/Gaj Location</span>
																						</span>
																					</button>
																				</c:if>
																				<c:if test='${not empty jobworkVo.balGajQC}'>
																					<button type="button" class="btn btn-primary m-btn m-btn--custom m-btn--icon" id="bal_gaj_receive">
																						<span><i class="la la-check"></i>&nbsp;&nbsp;<span>Bal/Gaj Receive</span></span>
																					</button>
																				</c:if>
																				<c:if test='${empty jobworkVo.balGajQC || jobworkVo.balGajQC == "pending"}'>
																					<button type="submit" id="form_save_step5_out_source" class="btn btn-success m-btn m-btn--custom m-btn--icon not-view" data-wizard-action="next">
																						<span><span>Save & Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																					</button>
																				</c:if>
																				<c:if test="${isEditable == false}">
																					<a href="/jobwork/${jobworkVo.jobworkId}" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
																						<span>
																							<span>Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																					</a>
																				</c:if>
																			</div>
																		</div>
																	</div>
																</div>
																<!--end: Form Actions -->
															</form>
															<form class="m-form m-form--label-align-left- m-form--state- m--hide" id="form_step5_receive" method="post" action="/jobwork/save">
																<input type="hidden" id="jobworkId" name="jobworkId" value="${jobworkVo.jobworkId}"/>
																<input type="hidden" id="process" name="process" value="${Constant.JOBWORK_PROCESS_BAL_GAJ}"/>
																<input type="hidden" id="balGajQC" name="balGajQC" value="complete"/>
																<!--begin: Form Body -->
																<div class="m-portlet__body m-portlet__body--no-padding">
																	<!--begin: Form Wizard Step 3-->
																	<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_6">
																		<div class="m-form__section m-form__section--first">
																			<div class="m-form__heading">
																				<h3 class="m-form__heading-title">Bal / Gaj Receive</h3>
																			</div>
																			
																			<div class="row">
																				<div class="col-lg-6 col-md-6 col-sm-12">
																					<div class="form-group m-form__group row">
																						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Receive Quantity:</label>
																						<div class="col-lg-12 col-md-12 col-sm-12">
																							<input type="text" class="form-control m-input" name="balGajReceivedQty" id="balGajReceivedQty" placeholder="Receive Quantity" value="${jobworkVo.balGajReceivedQty == 0 ? '' : jobworkVo.balGajReceivedQty}"/>
																						</div>
																					</div>
																					<div class="form-group m-form__group row">
																						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Receive By:</label>
																						<div class="col-lg-12 col-md-12 col-sm-12">
																							<select class="form-control m-select2" id="balGajReceivedBy" name="balGajReceivedBy.employeeId" placeholder="Select Receive By">
																								<option value="">Select Handover By</option>
																								<c:forEach items="${employeeVos}" var="employeeVo">
																									<option value="${employeeVo.employeeId}" <c:if test="${employeeVo.employeeId eq jobworkVo.balGajReceivedBy.employeeId}">selected='selected'</c:if>>
																										${employeeVo.employeeName}
																									</option>
																								</c:forEach>
																							</select>
																						</div>
																					</div>
																					<div class="form-group m-form__group row">
																						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Receive Date:</label>
																						<div class="col-lg-12 col-md-12 col-sm-12">
																							<div class="input-group date" >
																								<input type="text" class="form-control m-input default-datetimepicker" placeholder="Select date and time" id="balGajReceivedDate" name="balGajReceivedDate" value="<fmt:formatDate pattern='dd/MM/yyyy hh:mm aa' value='${jobworkVo.balGajReceivedDate}' />"/>
																								<div class="input-group-append">
																									<span class="input-group-text">
																									<i class="la la-calendar glyphicon-th"></i>
																									</span>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																				<div class="col-lg-6 col-md-6 col-sm-12">
																					<div class="m-widget28 m--margin-left-15">
																						<div class="m-widget28__container" >	
																							<!-- Start:: Content -->
																							<div class="m-widget28__tab tab-content">
																								<div class="m-widget28__tab-container tab-pane active">
																								    <div class="m-widget28__tab-items">
																										<div class="m-widget28__tab-item">
																											<span class="m--regular-font-size-">Supplier Name:</span>
																											<span>
																												<a href="<%=request.getContextPath() %>/contact/${jobworkVo.balGajContactVo.type}/${jobworkVo.balGajContactVo.contactId}" class="m-link m--font-boldest" target="_blank">
																													${jobworkVo.balGajContactVo.companyName}
																												</a>
																											</span>
																										</div>
																									</div>					      	 		      	
																								</div>
																							</div>
																							<!-- end:: Content --> 	
																						</div>				 	 
																					</div>
																					<c:set var="contactAddressVo" value="${jobworkVo.balGajContactVo.contactAddressVos.get(0)}"/>
																					<div class="m-section mt-3 m--margin-top-20 m--margin-left-15">
																						<%-- <c:if test="${contactAddress.isDeleted==0}"> --%>
																						<h3 class="m-section__heading">Address Details</h3>
																						<!-- <div class="m-divider"><span></span></div> -->
																						<h5 class=""><small class="text-muted m--hide" data-address-message="">Address is not provided</small></h5>
																						<div class="m-section__content">
																							<div class="row">
																								<div class="col-lg-12 col-md-12 col-sm-12">																
																									<h5 class=""><small class="text-muted" data-address-name="">${contactAddressVo.companyName}</small></h5>
																									<p class="mb-0">
																										<span data-address-line-1="">${contactAddressVo.addressLine1}</span>
																									</p>
																									<p class="mb-0">
																										<span data-address-line-2="">${contactAddressVo.addressLine2}</span>
																									</p>
																									<p class="mb-0">
																										<span data-address-pincode="">${contactAddressVo.pinCode}</span>
																										<span data-address-city="">${contactAddressVo.cityName}</span>
																										<span class="m--font-boldest">,&nbsp;</span>
																									</p>
																									<p class="mb-0">
																										<span data-address-state="">${contactAddressVo.stateName}</span>
																										<span class="m--font-boldest">,&nbsp;</span>
																										<span data-address-country="">${contactAddressVo.countriesName}</span>
																									</p>
																									<p class="mb-0">
																										<span class="">
																											<i class="la la-phone align-middle"></i> 
																											<span class="" data-address-phoneno="">
																												<c:if test="${empty jobworkVo.balGajContactVo.companyMobileno}">Mobile no. is not provided</c:if>${jobworkVo.balGajContactVo.companyMobileno}
																											</span>
																										</span>
																									</p>
																								</div>
																							</div>
																						</div>																				
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																	<!--end: Form Wizard Step 3-->
																</div>
																<!--end: Form Body -->
																<!--begin: Form Actions -->
																<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
																	<div class="m-form__actions">
																		<div class="row">
																			<div class="col-lg-6 m--align-left">
																				<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" onclick="$('#previous_form').submit()">
																					<span>
																					<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																					<span>Back</span>
																					</span>
																				</button>
																			</div>
																			<div class="col-lg-6 m--align-right">
																				
																				<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" id="bal_gaj_receive_cancel">
																					<span>Cancel</span>
																				</button>
																				
																				<c:if test='${jobworkVo.balGajQC != "complete"}'>
																					<button type="submit" id="form_save_step5_receive" class="btn btn-success m-btn m-btn--custom m-btn--icon not-view" data-wizard-action="next">
																						<span><span>Save & Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																					</button>
																				</c:if>
																			</div>
																		</div>
																	</div>
																</div>
																<!--end: Form Actions -->
															</form>
														</c:if>
														<form action="/jobwork/${jobworkVo.jobworkId}" method="post" id="previous_form">
															<input type="hidden" name="previousProcess" value="${Constant.JOBWORK_PROCESS_MEASUREMENT_QC1}"/>
														</form>
													</div>
													<!--end: Form Wizard Form-->
								
												</div>
											</div>
										</div>
										<!--end: Form Wizard-->
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
						</div>
						
					<!-- </form> -->
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	<script type="text/javascript">
	
		if(!${isEditable}) {
			$(".not-view").remove();
			$(".default-datetimepicker").removeClass("default-datetimepicker");
			$.each($('input, select ,textarea', '#form_step5,#form_step5_qc,#form_step5_out_source,#form_step5_receive'),function(k){
				$(this).attr("readonly", "true")
			});
		}
	</script>
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	<%-- <script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/wizard/wizard.js" type="text/javascript"></script> --%>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<%-- <script src="<%=request.getContextPath()%>/script/Jobwork/Jobwork-script.js" type="text/javascript"></script> --%>
	
	<script type="text/javascript">
		
		<c:if test="${empty jobworkVo.balGajLocation}">
		
			$("#form_step5").addClass("m--hide");
			$("#form_step5_out_source").addClass("m--hide");
			
			function setBalGajLocation(balGajLocation) {
				$("#bal_gaj_location").addClass("m--hide");
				if(balGajLocation == 1){
					$("#form_step5_out_source").removeClass("m--hide");
					$("#form_step5").addClass("m--hide");
				}else{
					$("#form_step5").removeClass("m--hide");
					$("#form_step5_out_source").addClass("m--hide");
				}
				
			}
			
			function changeBalGajLocation() {
				$("#bal_gaj_location").removeClass("m--hide");
				
				if(!$("#form_step5").hasClass("m--hide")) {
					$("#form_step5").addClass("m--hide");
				}
				
				if(!$("#form_step5_out_source").hasClass("m--hide")) {
					$("#form_step5_out_source").addClass("m--hide");
				}
				
			}
		
		</c:if>
		
		<c:if test="${empty jobworkVo.balGajLocation || jobworkVo.balGajLocation == 'in_house'}">
			var qtyValidator = {
				validators : {
					notEmpty : {
						message : 'The Qty is required'
					},
					stringLength : {
						max : 20,
						message : 'The Qty must be less than 20 characters long'
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,4})?))$/,
						message : 'The Qty is invalid'
					}
				}
			},
			passQtyValidator = {
				validators : {
					notEmpty : {
						message : 'The Qty is required'
					},
					stringLength : {
						max : 20,
						message : 'The Qty must be less than 20 characters long'
					},
					regexp : {
						regexp:/^((\d+)((\.\d{0,4})?))$/,
						message : 'The Qty is invalid'
					},
					callback: {
  						message: 'The sum of Pass and Fail Qty should be equals to Qty',
  						callback: function(value, validator, $field) {
  							
  							var id = $field.closest("tr").attr("data-table-item");
  							
  							if ($("#qcQty"+id).val() != (parseFloat($("#qcPassQty"+id).val())+parseFloat($("#qcFailQty"+id).val())))
  							{
  								return false
  							}
  							return true
  						}
  					},
				}
			},
			processValidator = {
				validators : {
					notEmpty : {
						message : 'Process is required'
					},
				}
			},
			employeeValidator = {
				validators : {
					notEmpty : {
						message : 'Employee is required'
					}
				}
			},rateValidator = {
				validators : {
					notEmpty : {message : 'The rate is required'},
					stringLength : {max : 20,message : 'The rate must be less than 20 characters long'},
					regexp : {regexp:/^((\d+)((\.\d{0,2})?))$/,message : 'The rate is invalid'}
				}
			};
		
			
			
			var tableIndex = 0;
			
			$(document).ready(function(){
				
				setTableIndex(${tableIndex});
				
				$("#form_step5").formValidation({
					framework : 'bootstrap',
					live:'disabled',
					excluded : ":disabled",
					icon : null,
					button: {
						selector: "#form_save_step5",
						disabled: "disabled"
					},
					fields : {
						
					}
				});
				
				$("#form_save_step5").on("click", function() {
					
					if ($('#form_step5').data('formValidation').isValid() == null) {
						$('#form_step5').data('formValidation').validate();
					}
					
					if($("#bal_gaj_process_table").find("[data-table-item]").not(".m--hide").length == 0) {
						toastr.error("","add minimum one Bal/Gaj Process");
						return false;
					}
					
					if($('#form_step5').data('formValidation').isValid() == true) {
						$("#bal_gaj_process_table").find("[data-table-item='template']").remove();
						
						$.each($('input, select ,textarea', '#form_step5'),function(k){
						    if($("#"+$(this).attr('id')).val() == "" || $("#"+$(this).attr('id')).val() == "undefined"){
								$("#"+$(this).attr('id')).attr("disabled", "disabled")
							}
						});
						
						$('#form_step5_qc').remove();
					} else {
						return false;
					}
					
				});
				
				var $tableItem=$("#bal_gaj_process_table").find("[data-table-item]").not(".m--hide");
				
				$tableItem.each(function () {
					var i = $(this).attr("data-table-item");
					$('#form_step5').formValidation('addField',"jobworkProcessVos["+i+"].productionProcessVo.productionProcessId", processValidator);
					$('#form_step5').formValidation('addField',"jobworkProcessVos["+i+"].employeeVo.employeeId", employeeValidator);
					$('#form_step5').formValidation('addField',"jobworkProcessVos["+i+"].qty", qtyValidator);
					$('#form_step5').formValidation('addField',"jobworkProcessVos["+i+"].rate", rateValidator);
					
				});
				
				setSrNo();
		  		
			});
			
			function setSrNo() {
				var $tableItemTemplate=$("#bal_gaj_process_table").find("[data-table-item]").not(".m--hide");
				var i = 0;
				$tableItemTemplate.each(function (){
					$(this).find("[data-item-index]").html(++i);
				});
			}
			
			function setTableIndex(i) {
				tableIndex = i*1;
			}
			
			function changeQty(id) {
				$('#form_step5_qc').formValidation('revalidateField',"jobworkProcessVos["+id+"].qcQty");
				$('#form_step5_qc').formValidation('revalidateField',"jobworkProcessVos["+id+"].qcPassQty");
				$('#form_step5_qc').formValidation('revalidateField',"jobworkProcessVos["+id+"].qcFailQty");
			}
			
			/**
			*** QUALITY CHECK
			*/
			
			$(document).ready(function(){
				
				$("#form_step5_qc").formValidation({
					framework : 'bootstrap',
					live:'disabled',
					button:{
						selector : "#form_save_step5_qc",
						disabled : "disabled",
					},
					excluded : ":disabled",
					icon : null,
					fields : {
						balGajQty : {
							validators : {
								notEmpty : {
									message : 'The Qty is required'
								},
								stringLength : {
									max : 20,
									message : 'The Qty must be less than 20 characters long'
								},
								regexp : {
									regexp:/^((\d+)((\.\d{0,4})?))$/,
									message : 'The Qty is invalid'
								}
							}
						},
					}
				});
				
				$("#quality_check").click(function(){
					$("#form_step5_qc").removeClass("m--hide");
					$("#form_step5").addClass("m--hide");
				});
				
				$("#quality_check_cancel").click(function(){
					$("#form_step5").removeClass("m--hide");
					$("#form_step5_qc").addClass("m--hide");
				});
				
				$("#form_save_step5_qc").on("click", function() {
					
					if ($('#form_step5_qc').data('formValidation').isValid() == null) {
						$('#form_step5_qc').data('formValidation').validate();
					}
					
					if($('#form_step5_qc').data('formValidation').isValid() == true) {
						$('#form_step5').remove();
					} else {
						return false;
					}
					
				});
				
				var $tableItem=$("#bal_gaj_quality_check_table").find("[data-table-item]").not(".m--hide");
				
				$tableItem.each(function () {
					var i = $(this).attr("data-table-item");
					$('#form_step5_qc').formValidation('addField',"jobworkProcessVos["+i+"].qcQty", qtyValidator);
					$('#form_step5_qc').formValidation('addField',"jobworkProcessVos["+i+"].qcPassQty", passQtyValidator);
					$('#form_step5_qc').formValidation('addField',"jobworkProcessVos["+i+"].qcFailQty", passQtyValidator);
					$('#form_step5_qc').formValidation('addField',"jobworkProcessVos["+i+"].qcBy.employeeId", employeeValidator);
					
				});
			});
		</c:if>
		
		<c:if test="${empty jobworkVo.balGajLocation || jobworkVo.balGajLocation == 'out_source'}">
			function getContactInfo() {
				if($("#contactVo").val() != ''){
					var id=$("#contactVo").val();
					
					$.post("/contact/suppliers/"+id+"/json", {
						
					}, function (data, status) {
						
						
						if(status == 'success') {
							
							address=data.contactAddressVos;
							
							
							$.each(data.contactAddressVos, function( key, value ) {
								
								if(key == 0) {
									
									$("#purchase_billing_address").find("[data-address-name]").html(value.companyName).end()
										.find("[data-address-line-1]").html(value.addressLine1).end()
										.find("[data-address-line-2]").html(value.addressLine2).end()
										.find("[data-address-pincode]").html(value.pinCode).end()
										.find("[data-address-city]").html(value.cityName).end()
										.find("[data-address-state]").html(value.stateName).end()
										.find("[data-address-country]").html(value.countriesName).end()
										.find("[data-address-city]").attr("data-address-city",value.cityCode).end()
										.find("[data-address-state]").attr("data-address-state",value.stateCode).end()
										.find("[data-address-country]").attr("data-address-country",value.countriesCode).end()
										.find("[data-address-phoneNo]").html(data.companyMobileno == "" ? "Mobile no. is not provided": data.companyMobileno).end()
										.removeClass("m--hide").end()
										.find("[data-address-message]").addClass("m--hide").end();
									
									$("#purchase_billing_address").parent().find("[data-address-message]").addClass("m--hide").end();
								}
							});
							
						} else {
							console.log("status: "+status);
						}
					});
				}
			}
			
			$(document).ready(function(){
				getContactInfo();
				
				$("#form_step5_out_source").formValidation({
					framework : 'bootstrap',
					live:'disabled',
					excluded : ":disabled",
					icon : null,
					fields : {
						balGajQty : {
							validators : {
								notEmpty : {
									message : 'The Qty is required'
								},
								stringLength : {
									max : 20,
									message : 'The Qty must be less than 20 characters long'
								},
								regexp : {
									regexp:/^((\d+)((\.\d{0,4})?))$/,
									message : 'The Qty is invalid'
								}
							}
						},
						"balGajHandoverBy.employeeId":{
							validators: {
								notEmpty: {
									message: 'The Handover By is required'
								},
							}
						},
						"balGajContactVo.contactId":{
							validators: {
								notEmpty: {
									message: 'The Supplier is required'
								},
							}
						},
						balGajHandoverDate:{
							validators: {
								notEmpty: {
									message: 'The Pressing Handover Date is required'
								},
							}
						},
						
					}
				});
				
				$("#balGajHandoverDate").change(function() {
					$('#form_step5_out_source').formValidation('revalidateField', 'balGajHandoverDate');
				});
				
				$("#form_save_step5_out_source").on('click', function(){
					
					if ($('#form_step5_out_source').data('formValidation').isValid() == null) {
						$('#form_step5_out_source').data('formValidation').validate();
					}
					
					if($('#form_step5_out_source').data('formValidation').isValid() == true) {
						$('#form_step5_receive').remove();
					} else {
						return false;
					}
				});
				
				$("#bal_gaj_receive").click(function(){
					$("#form_step5_receive").removeClass("m--hide");
					$("#form_step5_out_source").addClass("m--hide");
				});
				
			});
			
			$(document).ready(function(){
				
				$("#form_step5_receive").formValidation({
					framework : 'bootstrap',
					live:'disabled',
					button:{
						selector : "#form_save_step5_receive",
						disabled : "disabled",
					},
					excluded : ":disabled",
					icon : null,
					fields : {
						balGajReceivedQty : {
							validators : {
								notEmpty : {
									message : 'The Qty is required. '
								},
								stringLength : {
									max : 20,
									message : 'The Qty must be less than 20 characters long. '
								},
								regexp : {
									regexp:/^((\d+)((\.\d{0,4})?))$/,
									message : 'The Qty is invalid. '
								}
							}
						}, 
						"balGajReceivedBy.employeeId":{
							validators: {
								notEmpty: {
									message: 'The Received By is required. '
								},
							}
						}, 
						balGajReceivedDate:{
							validators: {
								notEmpty: {
									message: 'The Received Date is required. '
								},
							}
						},
					}
				});
				
				$("#bal_gaj_receive_cancel").click(function(){
					$("#form_step5_receive").addClass("m--hide");
					$("#form_step5_out_source").removeClass("m--hide");
				});
				
				$("#balGajReceivedDate").change(function() {
					$('#form_step5_receive').formValidation('revalidateField', 'balGajReceivedDate');
				});
				
				$("#form_save_step5_receive").on("click", function() {
					
					if ($('#form_step5_receive').data('formValidation').isValid() == null) {
						$('#form_step5_receive').data('formValidation').validate();
					}
					
					if($('#form_step5_receive').data('formValidation').isValid() == true) {
						$('#form_step5_out_source').remove();
					} else {
						return false;
					}
					
				});
			});
		</c:if>
	</script>
</body>
<!-- end::Body -->
</html>