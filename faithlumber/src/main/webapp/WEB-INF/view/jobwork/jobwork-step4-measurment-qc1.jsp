<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${jobworkVo.prefix}${jobworkVo.jobworkNo} | Jobwork</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		/* .m-table tr.m-table__row--brand span {
			color: #fff !important;
		} */
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
		.m-wizard.m-wizard--3 .m-wizard__head {
		    padding: 2rem 1rem !important;
		}
		
		.m-wizard.m-wizard--3 .m-wizard__form {
			padding: 2rem 4rem 3rem 4rem;
		}
		
	</style>
	
	<c:if test="${!isEditable}">
		<style type="text/css">
			select[readonly].select2-hidden-accessible + .select2-container {
			  pointer-events: none;
			  touch-action: none;
			}
		</style>
	</c:if>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${jobworkVo.prefix}${jobworkVo.jobworkNo}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/jobwork" class="m-nav__link">
										<span class="m-nav__link-text">Jobwork</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<!-- <form class="m-form m-form--state m-form--fit m-form--label-align-left" id="jobwork_form" action="/jobwork/save" method="post"> -->	
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									
									<div class="m-portlet__body" >
										<!--begin: Form Wizard-->
										<div class="m-wizard m-wizard--3 m-wizard--success" id="m_wizard">
								
											<!--begin: Message container -->
										    <div class="m-portlet__padding-x">
										        <!-- Here you can put a message or alert -->
										    </div>
										    <!--end: Message container -->
								
											<div class="row m-row--no-padding">
												<div class="col-xl-3 col-lg-12">
													<!--begin: Form Wizard Head -->
													<div class="m-wizard__head">
								
														<!--begin: Form Wizard Progress -->  		
														<div class="m-wizard__progress">	
															<div class="progress">		 
																<div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>						 	
															</div>			 
														</div> 
											            <!--end: Form Wizard Progress --> 
								
											            <!--begin: Form Wizard Nav -->
														<%@include file="jobwork-step-nav.jsp" %>
														<div class="m-wizard__nav">
															<div class="m-wizard__steps ">
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_1">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step1_form').submit()">							 
																			<span><span>1</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Antri And Larring
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_2" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step2_form').submit()">							 
																			<span><span>2</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Farmo Type
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--done" m-wizard-target="m_wizard_form_step_3" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step3_form').submit()">							 
																			<span><span>3</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Stitching
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step m-wizard__step--current" m-wizard-target="m_wizard_form_step_4" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step4_form').submit()">							 
																			<span><span>4</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Measurement QC
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_5" > 
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step5_form').submit()">							 
																			<span><span>5</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Bal / Gaj
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_6" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step6_form').submit()">							 
																			<span><span>6</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Washing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_7">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number"  onclick="$('#goto_step7_form').submit()">							 
																			<span><span>7</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Measurement QC
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_8" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step8_form').submit()">							 
																			<span><span>8</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Pressing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_9" >
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step9_form').submit()">							 
																			<span><span>9</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Labeling / Finishing
																		</div>
																	</div>
																</div>
																<div class="m-wizard__step" m-wizard-target="m_wizard_form_step_10">
																	<div class="m-wizard__step-info">
																		<a href="#" class="m-wizard__step-number" onclick="$('#goto_step10_form').submit()">							 
																			<span><span>10</span></span>							 
																		</a>
																		<div class="m-wizard__step-line">
																			<span></span>
																		</div>
																		<div class="m-wizard__step-label">
																			Store
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<!--end: Form Wizard Nav -->
													</div>
													<!--end: Form Wizard Head -->	
												</div>
												<div class="col-xl-9 col-lg-12">
													<!--begin: Form Wizard Form-->
													<div class="m-wizard__form">
														<form class="m-form m-form--label-align-left- m-form--state-" method="POST" action="/jobwork/save" id="form_step4">
															<c:set scope="page" var="jobworkFarmoTypeFarmoType" value="${jobworkVo.jobworkFarmoTypeVos.stream().filter(p->p.process == Constant.JOBWORK_PROCESS_FARMO_TYPE).toList().get(0)}"/>
															<c:set scope="page" var="jobworkFarmoTypeMeasurmentQC" value="${jobworkVo.jobworkFarmoTypeVos.stream().filter(p->p.process == Constant.JOBWORK_PROCESS_MEASUREMENT_QC1).toList().get(0)}"/>
															<input type="hidden" id="jobworkId" name="jobworkId" value="${jobworkVo.jobworkId}"/>
															<input type="hidden" id="jobworkFarmoTypeVo.process" name="jobworkFarmoTypeVos[0].process" value="${Constant.JOBWORK_PROCESS_MEASUREMENT_QC1}"/> <!-- process to identify which jobwork farmotype it is. Eg. For farmotype Or measurment qc1 Or measurment qc2      -->
															<c:if test="${not empty jobworkFarmoTypeMeasurmentQC}">
																<input type="hidden" id="jobworkFarmoTypeVos.jobworkFarmoTypeId" name="jobworkFarmoTypeVos[0].jobworkFarmoTypeId" value="${jobworkFarmoTypeMeasurmentQC.jobworkFarmoTypeId}"/>
															</c:if>
															<input type="hidden" id="process" name="process" value="${Constant.JOBWORK_PROCESS_MEASUREMENT_QC1}"/>
															<!--begin: Form Body -->
															<div class="m-portlet__body m-portlet__body--no-padding">
																<!--begin: Form Wizard Step 2-->
																<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_2">
																	<div class="m-form__heading">
																		<h3 class="m-form__heading-title">Measurement QC 1</h3>
																	</div>
																	<div class="m-form__section m-form__section--first">
																		
																		<div class="form-group m-form__group row" id="particularTempletDiv" >
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<label class="col-form-label">&nbsp;</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 text-center">
																						<label class="col-form-label">Measurement</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 text-center">
																						<label class="col-form-label">New Measurement</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 text-center">
																						<label class="col-form-label">Diffrance</label>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="form-group m-form__group row" id="particularTempletDiv" >
																			<div class="m-form__heading m--padding-left-15">
																				<h3 class="m-form__heading-title">Front Details</h3>
																			</div>
																		</div>
																		<div class="form-group m-form__group row" id="particularTempletDiv" >
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<label class="col-form-label">Front length :</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos.frontLength" id="oldFrontLength" value="${jobworkFarmoTypeFarmoType.frontLength}" disabled="disabled"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input new-measurement-input" name="jobworkFarmoTypeVos[0].frontLength" id="FrontLength" value="${jobworkFarmoTypeMeasurmentQC.frontLength}"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" id="diffranceFrontLength" readonly="readonly"  />
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<label class="col-form-label" >Front seat :</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos.frontSeat" id="oldFrontSeat" value="${jobworkFarmoTypeFarmoType.frontSeat}" disabled="disabled"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input new-measurement-input"  name="jobworkFarmoTypeVos[0].frontSeat" id="FrontSeat" value="${jobworkFarmoTypeMeasurmentQC.frontSeat}"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" id="diffranceFrontSeat" readonly="readonly"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<label class="col-form-label" >Front thai :</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].frontThai" id="oldFrontThai" value="${jobworkFarmoTypeFarmoType.frontThai}" disabled="disabled"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input new-measurement-input"  name="jobworkFarmoTypeVos[0].frontThai" id="FrontThai" value="${jobworkFarmoTypeMeasurmentQC.frontThai}"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" id="diffranceFrontThai" readonly="readonly"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<label class="col-form-label" >Front langot :</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].frontLangot" id="oldFrontLangot" value="${jobworkFarmoTypeFarmoType.frontLangot}" disabled="disabled"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input new-measurement-input"  name="jobworkFarmoTypeVos[0].frontLangot" id="FrontLangot" value="${jobworkFarmoTypeMeasurmentQC.frontLangot}"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input"  id="diffranceFrontLangot" readonly="readonly"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<label class="col-form-label" >Front knee :</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].frontKnee" id="oldFrontKnee" value="${jobworkFarmoTypeFarmoType.frontKnee}" disabled="disabled"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input new-measurement-input"  name="jobworkFarmoTypeVos[0].frontKnee" id="FrontKnee" value="${jobworkFarmoTypeMeasurmentQC.frontKnee}"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input"  id="diffranceFrontKnee" readonly="readonly"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<label class="col-form-label" >Front mori-munda :</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].frontMoriMunda" id="oldFrontMoriMunda" value="${jobworkFarmoTypeFarmoType.frontMoriMunda}" disabled="disabled"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input new-measurement-input"  name="jobworkFarmoTypeVos[0].frontMoriMunda" id="FrontMoriMunda" value="${jobworkFarmoTypeMeasurmentQC.frontMoriMunda}"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input"  id="diffranceFrontMoriMunda" readonly="readonly"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<label class="col-form-label" >Front jati :</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].frontJati" id="oldFrontJati" value="${jobworkFarmoTypeFarmoType.frontJati}" disabled="disabled"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input new-measurement-input"  name="jobworkFarmoTypeVos[0].frontJati" id="FrontJati" value="${jobworkFarmoTypeMeasurmentQC.frontJati}"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input"  id="diffranceFrontJati" readonly="readonly"/>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="form-group m-form__group row" id="particularTempletDiv" >
																			<div class="m-form__heading m--padding-left-15 m--padding-top-10">
																				<h3 class="m-form__heading-title">Back Details</h3>
																			</div>
																		</div>
																		<div class="form-group m-form__group row p-0" id="particularTempletDiv" >
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<label class="col-form-label" >Back length :</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].backLength" id="oldBackLength" value="${jobworkFarmoTypeFarmoType.backLength}" disabled="disabled"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input new-measurement-input"  name="jobworkFarmoTypeVos[0].backLength" id="BackLength" value="${jobworkFarmoTypeMeasurmentQC.backLength}"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input"  id="diffranceBackLength" readonly="readonly"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<label class="col-form-label" >Back seat :</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].backSeat" id="oldBackSeat" value="${jobworkFarmoTypeFarmoType.backSeat}" disabled="disabled"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input new-measurement-input"  name="jobworkFarmoTypeVos[0].backSeat" id="BackSeat" value="${jobworkFarmoTypeMeasurmentQC.backSeat}"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input"  id="diffranceBackSeat" readonly="readonly"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<label class="col-form-label" >Back thai :</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].backThai" id="oldBackThai" value="${jobworkFarmoTypeFarmoType.backThai}" disabled="disabled"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input new-measurement-input"  name="jobworkFarmoTypeVos[0].backThai" id="BackThai" value="${jobworkFarmoTypeMeasurmentQC.backThai}"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input"  id="diffranceBackThai" readonly="readonly"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<label class="col-form-label" >Back langot :</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].backLangot" id="oldBackLangot" value="${jobworkFarmoTypeFarmoType.backLangot}" disabled="disabled"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input new-measurement-input"  name="jobworkFarmoTypeVos[0].backLangot" id="BackLangot" value="${jobworkFarmoTypeMeasurmentQC.backLangot}"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input"  id="diffranceBackLangot" readonly="readonly"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<label class="col-form-label" >Back knee :</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].backKnee" id="oldBackKnee" value="${jobworkFarmoTypeFarmoType.backKnee}" disabled="disabled"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input new-measurement-input"  name="jobworkFarmoTypeVos[0].backKnee" id="BackKnee" value="${jobworkFarmoTypeMeasurmentQC.backKnee}"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input"  id="diffranceBackKnee" readonly="readonly"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<label class="col-form-label" >Back mori-munda :</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].backMoriMunda" id="oldBackMoriMunda" value="${jobworkFarmoTypeFarmoType.backMoriMunda}" disabled="disabled"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input new-measurement-input"  name="jobworkFarmoTypeVos[0].backMoriMunda" id="BackMoriMunda" value="${jobworkFarmoTypeMeasurmentQC.backMoriMunda}"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input"  id="diffranceBackMoriMunda" readonly="readonly"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<label class="col-form-label" >Back jati :</label>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input" name="jobworkFarmoTypeVos[0].backJati" id="oldBackJati" value="${jobworkFarmoTypeFarmoType.backJati}" disabled="disabled"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input new-measurement-input"  name="jobworkFarmoTypeVos[0].backJati" id="BackJati" value="${jobworkFarmoTypeMeasurmentQC.backJati}"/>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-3 col-md-3 col-sm-12" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="input-group">
																							<input type="text" class="form-control m-input"  id="diffranceBackJati" readonly="readonly"/>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div class="m-separator m-separator--dashed m-separator--lg"></div>
																		<div class="m-form__heading">
																			<h3 class="m-form__heading-title">Farmo Particular Details</h3>
																		</div>
																		<div class="form-group m-form__group row" id="particularDiv" >
																			<div class="col-lg-12 col-md-12 col-sm-12 <c:if test="${not empty jobworkVo.jobworkFarmoTypeParticularVos}">m--hide</c:if>" >
																				<div class="row">
																					<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																						<div class="form-group m-form__group row">
																							<label class="col-form-label col-lg-5 col-md-5 col-sm-12" data-particular-label="">No farmo type selected</label>
																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="col-lg-12 col-md-12 col-sm-12 <c:if test="${empty jobworkVo.jobworkFarmoTypeParticularVos}">m--hide</c:if>" >
																				<div class="row">
																					<c:set scope="page" var="farmoParticularOldIndex" value="0"></c:set>
																					<c:set scope="page" var="farmoParticularNewIndex" value="0"></c:set>
																					<c:set scope="page" var="farmoParticularDiffranceIndex" value="0"></c:set>
																					<div class="col-lg-3 col-md-3 col-sm-12" >
																						<c:forEach items="${jobworkVo.jobworkFarmoTypeParticularVos.stream().filter(aa->aa.process == Constant.JOBWORK_PROCESS_FARMO_TYPE).toList()}" var="jobworkFarmoTypeParticularVo">
																							<div class="row">
																								<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																									<div class="form-group m-form__group">
																										<label class="col-form-label">${jobworkFarmoTypeParticularVo.farmoParticularVo.name}&nbsp;:</label>
																									</div>
																								</div>
																							</div>
																						</c:forEach>
																					</div>
																					<div class="col-lg-3 col-md-3 col-sm-12" >
																						<c:forEach items="${jobworkVo.jobworkFarmoTypeParticularVos.stream().filter(bb->bb.process == Constant.JOBWORK_PROCESS_FARMO_TYPE).toList()}" var="jobworkFarmoTypeParticularVo">
																							<div class="row">
																								<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																									<div class="form-group m-form__group">
																										<div class="">
																											<div class="input-group">
																												<input type="text" class="form-control m-input"  id="oldFarmoParticularMeasurement${farmoParticularOldIndex}" value="${jobworkFarmoTypeParticularVo.farmoParticularMeasurement}" disabled="disabled"/>
																												<input type="hidden" class="form-control m-input new-measurement-input" name="jobworkFarmoTypeParticularVos[${farmoParticularOldIndex}].farmoParticularVo.farmoParticularId" value="${jobworkFarmoTypeParticularVo.farmoParticularVo.farmoParticularId}"/>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>
																							<c:set scope="page" var="farmoParticularOldIndex" value="${farmoParticularOldIndex+1}"></c:set>
																						</c:forEach>
																					</div>
																					<div class="col-lg-3 col-md-3 col-sm-12" >
																						<c:forEach items="${jobworkVo.jobworkFarmoTypeParticularVos.stream().filter(cc->cc.process == Constant.JOBWORK_PROCESS_MEASUREMENT_QC1).toList()}" var="jobworkFarmoTypeParticularVo">
																							<div class="row">
																								<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																									<div class="form-group m-form__group">
																										<div class="">
																											<div class="input-group">
																												<input type="text" class="form-control m-input new-measurement-input" name="jobworkFarmoTypeParticularVos[${farmoParticularNewIndex}].farmoParticularMeasurement" id="FarmoParticularMeasurement${farmoParticularNewIndex}" value="${jobworkFarmoTypeParticularVo.farmoParticularMeasurement}"/>
																												<c:if test="${jobworkFarmoTypeParticularVo.jobworkFarmoTypeParticularsId !=0}">
																													<input type="hidden" class="form-control m-input new-measurement-input" name="jobworkFarmoTypeParticularVos[${farmoParticularNewIndex}].jobworkFarmoTypeParticularsId" id="jobworkFarmoTypeParticularsId${farmoParticularNewIndex}" value="${jobworkFarmoTypeParticularVo.jobworkFarmoTypeParticularsId}"/>
																												</c:if>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>
																							<c:set scope="page" var="farmoParticularNewIndex" value="${farmoParticularNewIndex+1}"></c:set>
																						</c:forEach>
																					</div>
																					<div class="col-lg-3 col-md-3 col-sm-12" >
																						<c:forEach items="${jobworkVo.jobworkFarmoTypeParticularVos.stream().filter(dd->dd.process == Constant.JOBWORK_PROCESS_FARMO_TYPE).toList()}" var="jobworkFarmoTypeParticularVo">	
																							<div class="row">
																								<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 ">
																									<div class="form-group m-form__group">
																										<div class="">
																											<div class="input-group">
																												<input type="text" class="form-control m-input " name="jobworkFarmoTypeParticularVos[${farmoParticularDiffranceIndex}].farmoParticularMeasurement" id="diffranceFarmoParticularMeasurement${farmoParticularDiffranceIndex}" readonly="readonly"/>
																											</div>
																										</div>
																									</div>
																								</div>
																							</div>
																							<c:set scope="page" var="farmoParticularDiffranceIndex" value="${farmoParticularDiffranceIndex+1}"></c:set>
																						</c:forEach>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
																<!--end: Form Wizard Step 2-->
															</div>
															<!--end: Form Body -->
															<!--begin: Form Actions -->
															<div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
																<div class="m-form__actions">
																	<div class="row">
																		<div class="col-lg-6 m--align-left">
																			<button type="button" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" onclick="$('#previous_form').submit()">
																				<span>
																				<i class="la la-arrow-left"></i>&nbsp;&nbsp;
																				<span>Back</span>
																				</span>
																			</button>
																		</div>
																		<div class="col-lg-6 m--align-right">
																			<c:if test="${isEditable == true}">
																				<button type="submit" id="form_save_step4" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
																					<span>
																					<span>Save & Continue</span>&nbsp;&nbsp;
																					<i class="la la-arrow-right"></i>
																					</span>
																				</button>
																			</c:if>
																			<c:if test="${isEditable == false}">
																				<a href="/jobwork/${jobworkVo.jobworkId}" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
																					<span>
																						<span>Continue</span>&nbsp;&nbsp;<i class="la la-arrow-right"></i></span>
																				</a>
																			</c:if>
																		</div>
																	</div>
																</div>
															</div>
															<!--end: Form Actions -->
														</form>
														<form action="/jobwork/${jobworkVo.jobworkId}" method="post" id="previous_form">
															<input type="hidden" name="previousProcess" value="${Constant.JOBWORK_PROCESS_STITCHING}"/>
														</form>
													</div>
													<!--end: Form Wizard Form-->
								
												</div>
											</div>
										</div>
										<!--end: Form Wizard-->
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
						</div>
						<%-- <div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions--solid m-form__actions--right">
								<button type="submit" class="btn btn-brand" id="savedeliverychallan">
									Submit
								</button>
								
								<a href="<%=request.getContextPath()%>/deliverychallan" class="btn btn-secondary">
									Cancel
								</a>
							</div>
						</div> --%>
					<!-- </form> -->
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	<script type="text/javascript">
	
		if(!${isEditable}) {
			$(".not-view").remove();
			$(".default-datetimepicker").removeClass("default-datetimepicker");
			$.each($('input, select ,textarea', '#form_step4'),function(k){
				$(this).attr("readonly", "true")
			});
		}
	</script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<%-- <script src="<%=request.getContextPath()%>/script/Jobwork/Jobwork-script.js" type="text/javascript"></script> --%>
	
	<script type="text/javascript">

		var allField = 0 ;
		var measurementValidator = {
				validators : {
					notEmpty : {
						message : 'Please enter measurement. '
					},regexp : {
						regexp : /^[0-9.]+$/,
						message : 'Measurement is not valid, can contain only digits. '
					}
				}
			};
		$(document).ready(function() {
			
			$("#form_step4").formValidation({
				framework : 'bootstrap',
				live:'disabled',
				excluded : ":disabled",
				button: {
					selector: "#form_save_step4",
					disabled: "disabled"
				},
				icon : null,
				fields : {
				}
			});
			
			$('#form_step4').formValidation('addField',"jobworkFarmoTypeVos[0].frontLength", measurementValidator);
			$('#form_step4').formValidation('addField',"jobworkFarmoTypeVos[0].frontSeat", measurementValidator);
			$('#form_step4').formValidation('addField',"jobworkFarmoTypeVos[0].frontThai", measurementValidator);
			$('#form_step4').formValidation('addField',"jobworkFarmoTypeVos[0].frontLangot", measurementValidator);
			$('#form_step4').formValidation('addField',"jobworkFarmoTypeVos[0].frontKnee", measurementValidator);
			$('#form_step4').formValidation('addField',"jobworkFarmoTypeVos[0].frontMoriMunda", measurementValidator);
			$('#form_step4').formValidation('addField',"jobworkFarmoTypeVos[0].frontJati", measurementValidator);
			$('#form_step4').formValidation('addField',"jobworkFarmoTypeVos[0].backLength", measurementValidator);
			$('#form_step4').formValidation('addField',"jobworkFarmoTypeVos[0].backSeat", measurementValidator);
			$('#form_step4').formValidation('addField',"jobworkFarmoTypeVos[0].backThai", measurementValidator);
			$('#form_step4').formValidation('addField',"jobworkFarmoTypeVos[0].backLangot", measurementValidator);
			$('#form_step4').formValidation('addField',"jobworkFarmoTypeVos[0].backKnee", measurementValidator);
			$('#form_step4').formValidation('addField',"jobworkFarmoTypeVos[0].backMoriMunda", measurementValidator);
			$('#form_step4').formValidation('addField',"jobworkFarmoTypeVos[0].backJati", measurementValidator);
			
			var k = ${farmoParticularNewIndex};
			
			for(var i = 0; i<k; i++) {
				$('#form_step4').formValidation('addField',"jobworkFarmoTypeParticularVos["+i+"].farmoParticularMeasurement", measurementValidator);
			}
			
			$(".new-measurement-input").each(function(){ 
				var id = $(this).attr('id');
				$("#diffrance"+id).val(($("#old"+id).val()*1)-($("#"+id).val()*1))
			});
			
			$("#form_save_step4").on("click", function(){	
				
				if ($('#form_step4').data('formValidation').isValid() == null) {
					$('#form_step4').data('formValidation').validate();
				}
				
				if ($('#form_step4').data('formValidation').isValid() == true) {
					$.each($('input, select ,textarea', '#form_step4'),function(k){
						if($("#"+$(this).attr('id')).val() == "" || $("#"+$(this).attr('id')).val() == "undefined"){
							$("#"+$(this).attr('id')).attr("disabled", "disabled")
						}
					});
				} else {
					return false;
				}
				
			});
			
			$(".new-measurement-input").on('change', function(){ 
				var id = $(this).attr('id');
				$("#diffrance"+id).val(($("#old"+id).val()*1)-($("#"+id).val()*1))
			});
			
			
		});//End Document Ready
		
	</script>
</body>
<!-- end::Body -->
</html>