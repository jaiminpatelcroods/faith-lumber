<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${leadVo.companyName} | Lead</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${leadVo.companyName}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/lead" class="m-nav__link">
										<span class="m-nav__link-text">Lead</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<div class="row">
						<div class="col-lg-3 col-md-3 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<span class="m--font-boldest m--font-primary" data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="" data-html="true" data-original-title="<b>Customer Type</b>">
												${leadVo.leadType}
											</span>
										</div>
									</div>
									<div class="m-portlet__head-tools">
										<ul class="m-portlet__nav">
											<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
												<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl  m-dropdown__toggle">
													<!-- <i class="la la-ellipsis-v"></i> -->
													<i class="la la-ellipsis-h m--font-brand"></i>
												</a>
						                        <div class="m-dropdown__wrapper">
						                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
						                            <div class="m-dropdown__inner">
					                                    <div class="m-dropdown__body">              
					                                        <div class="m-dropdown__content">
					                                            <ul class="m-nav">
					                                                <li class="m-nav__separator m-nav__separator--fit">
					                                                </li>
					                                                <li class="m-nav__item">
					                                                    <a href="/lead/${leadVo.leadId}/edit" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																			<span><i class="flaticon-edit"></i><span>Edit</span></span>
																		</a>
					                                                    <button id="lead_delete" data-url="/lead/${leadVo.leadId}/delete" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right delete-btn">
																			<span><i class="flaticon-delete-1"></i><span>Delete</span></span>
																		</button>
					                                                </li>
					                                            </ul>
					                                        </div>
					                                    </div>
						                            </div>
						                        </div>
											</li>
										</ul>
									</div>
								</div>
								<div class="m-portlet__body m--padding-bottom-0 m--padding-right-10 m--padding-left-10">
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12">
											<h5 class="display-5 text-center">${leadVo.companyName}</h5>
											<p class="text-center mb-0">
												<span class="m--font-info">
													<i class="fa fa-mobile-alt align-middle"></i> <c:if test="${empty leadVo.companyMobileno}">Mobile number is not provided</c:if>${leadVo.companyMobileno}
												</span>
											</p>
											<p class="text-center mb-0" style=" word-break: break-all">
												<span class="m--font-bolder ">
													<i class="la la-phone align-middle"></i>&nbsp; <c:if test="${empty leadVo.companyTelephone}">Telephone no is not provided</c:if>${leadVo.companyTelephone}
												</span>
											</p>
											<p class="text-center " style=" word-break: break-all">
												<span class="m--font-bolder ">
													<i class="flaticon-mail-1 align-middle"></i>&nbsp; <c:if test="${empty leadVo.companyEmail}">Email is not provided</c:if>${leadVo.companyEmail}
												</span>
											</p>
											<h3 class="text-center mb-0">
												<small class="text-muted"><c:if test="${empty leadVo.ownerName}">Owner Name is not provided</c:if>${leadVo.ownerName}</small>
											</h3>
											<p class="text-center">
												<span class="m--font-brand">
													<i class="fa fa-mobile-alt align-middle"></i> <c:if test="${empty leadVo.ownerMobileno}">Mobile number is not provided</c:if>${leadVo.ownerMobileno}
												</span>
											</p>
											<div class="m-divider"><span></span></div>
											<div class="m-widget4">
												<div class="m-widget4__item">
													<!-- <div class="m-widget4__img m-widget4__img--logo">
														<img src="../../assets/app/media/img/client-logos/logo5.png" alt="">
													</div> -->
													<div class="m-widget4__info">
														<span class="m-widget4__title">
															Lead Source
														</span>
														<br>
														<span class="m-widget4__sub"></span>
													</div>
													<span class="m-widget4__ext">
														<span class="m-widget4__number m--font-danger">
															${leadVo.leadSource}
														</span>
													</span>
												</div>
												<div class="m-widget4__item">
													<div class="m-widget4__info">
														<span class="m-widget4__title">
															Total No. of Shops
														</span>
														<br>
														<span class="m-widget4__sub"></span>
													</div>
													<span class="m-widget4__ext">
														<span class="m-widget4__number m--font-info">
															<c:if test="${empty leadVo.noOfShops}">N/A</c:if>${leadVo.noOfShops}
														</span>
													</span>
												</div>
											</div>
											
										</div>
									</div>
									<div class="row m--margin-top-10">
										<div class="col-lg-12 col-md-12 col-sm-12" style="padding-right: 0.3rem !important;padding-left: 0.3rem !important">
											<!--begin::Section-->                                            
							                <div class="m-accordion m-accordion--bordered" id="m_accordion_2" role="tablist">   
							
							                    <!--begin::Item-->              
							                    <div class="m-accordion__item">
							                        <div class="m-accordion__item-head"  role="tab" id="gst_accordion_item_head" data-toggle="collapse" href="#gst_accordion_item_body" aria-expanded="false">
							                            <!-- <span class="m-accordion__item-icon"><i class="fa flaticon-user-ok"></i></span> -->
							                            <span class="m-accordion__item-title m--font-bolder">GST Details</span>  
							                            <span class="m-accordion__item-mode"></span>
							                        </div>
							
							                        <div class="m-accordion__item-body m-accordion__item-body-background collapse show" id="gst_accordion_item_body" role="tabpanel" aria-labelledby="gst_accordion_item_head" data-parent="#m_accordion_2"> 
							                            <div class="m-accordion__item-content">
							                                <div class="m-widget28">
																<!-- <div class="m-widget28__pic m-portlet-fit--sides"></div> -->			  
																<div class="m-widget28__container" >	
																	<!-- Start:: Content -->
																	<div class="m-widget28__tab tab-content">
																		<div class="m-widget28__tab-container tab-pane active">
																		    <div class="m-widget28__tab-items">
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">GST Type:</span>
																					<span><c:if test="${empty leadVo.gstType}">N/A</c:if>${leadVo.gstType}</span>
																				</div>
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">GSTIN</span>
																					<span><c:if test="${empty leadVo.gstin}">N/A</c:if>${leadVo.gstin}</span>
																				</div>
																				
																			</div>					      	 		      	
																		</div>
																	</div>
																	<!-- end:: Content --> 	
																</div>				 	 
															</div>
							                            </div>
							                        </div>
							                    </div>
							                    <!--end::Item--> 
							                    
							                    <!--begin::Item--> 
							                    <div class="m-accordion__item">
							                        <div class="m-accordion__item-head collapsed" role="tab" id="other_accordion_item_head" data-toggle="collapse" href="#other_accordion_item_body" aria-expanded="    false">
							                        	<span class="m-accordion__item-title m--font-bolder">Other Details</span>
							                              
							                            <span class="m-accordion__item-mode"></span>     
							                        </div>
							
							                        <div class="m-accordion__item-body m-accordion__item-body-background collapse" id="other_accordion_item_body" role="tabpanel" aria-labelledby="other_accordion_item_head" data-parent="#m_accordion_2"> 
							                            <div class="m-accordion__item-content">
							                            	<div class="m-widget28">
																<div class="m-widget28__container" >	
																	<!-- Start:: Content -->
																	<div class="m-widget28__tab tab-content">
																		<div class="m-widget28__tab-container tab-pane active">
																		    <div class="m-widget28__tab-items">
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">Pan No.:</span>
																					<span><c:if test="${empty leadVo.panNo}">N/A</c:if>${leadVo.panNo}</span>
																				</div>
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">Date Of Birth:</span>
																					<span><c:if test="${empty leadVo.dateOfBirth}">N/A</c:if>${leadVo.dateOfBirth}</span>
																				</div>
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">Anniversary Date:</span>
																					<span><c:if test="${empty leadVo.anniversaryDate}">N/A</c:if>${leadVo.anniversaryDate}</span>
																				</div>
																			</div>					      	 		      	
																		</div>
																	</div>
																	<!-- end:: Content --> 	
																</div>				 	 
															</div>
							                            </div>
							                        </div>                       
							                    </div>
							                    <!--end::Item-->                     
							
							                </div>
							                <!--end::Section-->
										</div>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
						<div class="col-lg-9 col-md-9 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--tabs m-portlet--head-solid-bg m-portlet--head-sm">
								<div class="m-portlet__head">
									<div class="m-portlet__head-tools">
										<ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
											<li class="nav-item m-tabs__item">
												<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_7_1" role="tab">
													Address Details
												</a>
											</li>
											<li class="nav-item m-tabs__item">
												<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_Activity" role="tab">
													Activity
												</a>
											</li>
										</ul>
									</div>
									
									<div class="m-portlet__head-tools">
										<ul class="m-portlet__nav">
											<li class="m-portlet__nav-item">
												<c:if test="${leadVo.status == 'Pending'}"><span class="m-badge m-badge--warning m-badge--wide">Pending</span></c:if>
												<c:if test="${leadVo.status == 'In Progress'}"><span class="m-badge m-badge--info m-badge--wide">In Progress</span></c:if>
												<c:if test="${leadVo.status == 'Completed'}"><span class="m-badge m-badge--success m-badge--wide">Completed</span></c:if>
											</li>
											<li class="m-portlet__nav-item">
												<c:if test="${leadVo.status == 'In Progress'}">
													<button type="button" class="btn btn-success btn-sm m-btn m-btn--icon" data-toggle="modal" data-target="#lead_complete_modal">
														<span class="m--font-boldest"><i class="la la-plus"></i> &nbsp;Complete Enquiry</span>
													</button>
												</c:if>
												<c:if test="${leadVo.status != 'Completed'}">
												<button type="button" class="btn btn-primary btn-sm m-btn m-btn--icon m--margin-left-5" data-toggle="modal" data-target="#new_activity_modal">
													<span class="m--font-boldest"><i class="la la-plus"></i> &nbsp;Add Activity</span>
												</button>
												</c:if>
											</li>
										</ul>
									</div>
								</div>
								<div class="m-portlet__body">									
									<div class="tab-content">
										<div class="tab-pane" id="m_tabs_Activity" role="tabpanel">
											<div class="m-timeline-1 m-timeline-1--fixed">

												<div class="m-timeline-1__items">	
													<div class="m-timeline-1__marker"></div>
														<c:if test="${leadVo.status == 'Completed' }">
															<div class="m-timeline-1__item m-timeline-1__item--right m-timeline-1__item--first">
			
																<div class="m-timeline-1__item-circle">
																	<div class="m--bg-danger"></div>
																</div>
																<div class="m-timeline-1__item-arrow"></div>
																<span class="m-timeline-1__item-time m--font-brand">
																<fmt:formatDate pattern="dd/MM/yyyy" value="${leadVo.completedDate}" />
																<span></span></span>
																<div class="m-timeline-1__item-content" style="background-color: #34bfa3; color : white !important;">
																	<div class="m-timeline-1__item-body_custom row"
																		style="margin-left: 0px;">
																		
																		<div class="m-timeline-1_item-body-col-9"><!--asign to  -->
																			</div>
																		<div class="m--align-right col-3 m-timeline-1_badge_div">
																					
																			</div>
																	</div>
																	<div class="m-timeline-1__item-title">														
																		 
																	Completed Date - <span class="">
																		<fmt:formatDate pattern="dd/MM/yyyy" value="${leadVo.completedDate}" />
																	</span>
																	</div>
																	<div class="m-timeline-1__item-body_custom m--margin-top-15"
																		style="word-wrap: break-word;">
																		<span class="m--font-bolder" id="">Remark: </span>
																		<span >${leadVo.remark}</span>
																		</br>
																		<div class="m-timeline-1__item-body row m--margin-0">
																			
																			<div class="col-6 m--padding-0 m--align-right">
																				<span class="m--font-bolder">&nbsp;</span>
																				<span class="m--font-danger m--font-bold">
																					&nbsp; </span>
																			</div>
																			 <div class="col-6 m--padding-0 m--align-right">
																				<span class="m--font-bolder">Assigned Employee:</span>
																				<span class=" m--font-bold">
																					${leadVo.assignedEmployee.employeeName}</span>
																			</div> 
																		</div>
																	</div>
																	
																</div>
			
															</div>
														</c:if>
														<c:forEach items="${leadActivityVos}" var="leadActivityVo">
															<div class="m-timeline-1__item m-timeline-1__item--right m-timeline-1__item--first">
			
																<div class="m-timeline-1__item-circle">
																	<div class="m--bg-danger"></div>
																</div>
																<div class="m-timeline-1__item-arrow"></div>
																<span class="m-timeline-1__item-time m--font-brand">
																
																<%-- <fmt:parseDate pattern="dd/MM/yyyy" value="${ActivityDetails.createdOn}" var="date" />
																	${date}
																<fmt:formatDate pattern="dd/MM/yyyy" value="" /> --%>
																<fmt:parseDate value="${leadActivityVo.createdOn}" pattern="dd-MM-yyyy" var="myDate"/>
																<fmt:formatDate value="${myDate}" var="startFormat" pattern="dd/MM/yyyy"/>
																${startFormat}
																<span></span></span>
																<div class="m-timeline-1__item-content">
																	<div class="m-timeline-1__item-body_custom row"
																		style="margin-left: 0px;">
																		
																		<div class="m-timeline-1_item-body-col-9"><!--asign to  -->
																			</div>
																		<div class="m--align-right col-3 m-timeline-1_badge_div">
																					
																			</div>
																	</div>
																	<div class="m-timeline-1__item-title">														
																		 
																	Mode of Communication - <span class="m--font-primary">${leadActivityVo.modeOfCommunication}</span>
																	</div>
																	<div class="m-timeline-1__item-body_custom m--margin-top-15"
																		style="word-wrap: break-word;">
																		<span class="m--font-bolder" id="">Remark: </span>
																		<span >${leadActivityVo.remark}</span>
																		</br>
																		<span class="m--font-bolder">Next Schedule Date: </span>
																		<span id="">
																			<fmt:formatDate pattern="dd/MM/yyyy" value="${leadActivityVo.nextScheduleDate}" />
																		</span>
																		
																		<div class="m-timeline-1__item-body row m--margin-0">
																			
																			<div class="col-6 m--padding-0 m--align-right">
																				<span class="m--font-bolder">&nbsp;</span>
																				<span class="m--font-danger m--font-bold">
																					&nbsp; </span>
																			</div>
																			 <div class="col-6 m--padding-0 m--align-right">
																				<span class="m--font-bolder">Assigned Employee:</span>
																				<span class="m--font-danger m--font-bold">
																					${leadVo.assignedEmployee.employeeName}</span>
																			</div> 
																		</div>
																	</div>
																	
																</div>
			
															</div>
														</c:forEach>
														<br />
												</div>
											</div>
										</div>
										<div class="tab-pane active" id="m_tabs_7_1" role="tabpanel">
											<c:forEach items="${leadVo.leadAddressVos}" var="leadAddress" varStatus="status">
												<!--begin::Section-->
												<div class="m-section">
													<h3 class="m-section__heading">
														<c:if test="${leadAddress.isDefault==1}">Default Address</c:if>
														<c:if test="${leadAddress.isDefault==0}">
															Address Details
															<label class="m-checkbox m-checkbox--solid m-checkbox--brand float-right">
																<input type="checkbox" onchange="setAsDefaultAddress(${leadAddress.leadAddressId})"  id="setAsDefault" name="setAsDefault" value="" > Set As Default
																<span></span>
															</label>
														</c:if>
													</h3>
													<div class="m-section__content">
														<div class="row">
															<div class="col-lg-6 col-md-6 col-sm-12">
																<table class="table m-table">
																  	<tbody>
																    	<tr class="row">
																      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Company Name:</th>
																	      	<td class="col-lg-8 col-md-8 col-sm-12">${leadAddress.companyName}</td>
																    	</tr>
																    	<tr class="row">
																      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Address Line 1:</th>
																	      	<td class="col-lg-8 col-md-8 col-sm-12">${leadAddress.addressLine1}</td>
																    	</tr>
																    	<tr class="row">
																      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Address Line 2:</th>
																	      	<td class="col-lg-8 col-md-8 col-sm-12">${leadAddress.addressLine2}</td>
																    	</tr>
																    	<tr class="row">
																      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">ZIP/Postal code:</th>
																	      	<td class="col-lg-8 col-md-8 col-sm-12">${leadAddress.pinCode}</td>
																    	</tr>
																  	</tbody>
																</table>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12 m--padding-left-30">
																<table class="table m-table">
																  	<tbody>
																    	<tr class="row">
																	      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Country:</th>
																		    <td class="col-lg-8 col-md-8 col-sm-12">${leadAddress.countriesName}</td>
																    	</tr>
																    	<tr class="row">
																      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">State:</th>
																      		<td class="col-lg-8 col-md-8 col-sm-12">${leadAddress.stateName}</td>
																    	</tr>
																    	<tr class="row">
																      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">City:</th>
																      		<td class="col-lg-8 col-md-8 col-sm-12">${leadAddress.cityName}</td>
																    	</tr>
																  	</tbody>
																</table>
															</div>
														</div>
													</div>
												</div>
											</c:forEach>
											<!--end::Section-->
										</div>
									</div>
								</div>
								<div class="m-portlet__foot m-portlet__foot--fit">
									<div class="m-accordion m-accordion--bordered" id="m_accordion_1" role="tablist">   
					                    <!--begin::Item-->              
					                    <div class="m-accordion__item" style="border-top: 5px solid #ebedf2">
					                        <div class="m-accordion__item-head collapsed"  role="tab" id="contact_accordion_item_head" data-toggle="collapse" href="#contact_accordion_item_body" aria-expanded="false">
					                            <span class="m-accordion__item-title m--font-bolder">Contact Details</span>  
					                            <span class="m-accordion__item-mode"></span>
					                        </div>
					
					                        <div class="m-accordion__item-body m-accordion__item-body-background collapse" id="contact_accordion_item_body" class="" role="tabpanel" aria-labelledby="contact_accordion_item_head" data-parent="#m_accordion_1" style="background-color: #fff"> 
					                            <div class="m-accordion__item-content">
					                            	<div class="row">
														<div class="col-lg-4 col-md-4 col-sm-12">
															<div class="m-widget28">
																<div class="m-widget28__container" >	
																	<!-- Start:: Content -->
																	<div class="m-widget28__tab tab-content">
																		<div class="m-widget28__tab-container tab-pane active">
																		    <div class="m-widget28__tab-items">
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">Concern Person:</span>
																					<span><c:if test="${empty leadVo.concernPersonName}">N/A</c:if>${leadVo.concernPersonName}</span>
																				</div>
																			</div>					      	 		      	
																		</div>
																	</div>
																	<!-- end:: Content --> 	
																</div>				 	 
															</div>
														</div>
														<div class="col-lg-4 col-md-4 col-sm-12">
															<div class="m-widget28">
																<div class="m-widget28__container" >	
																	<!-- Start:: Content -->
																	<div class="m-widget28__tab tab-content">
																		<div class="m-widget28__tab-container tab-pane active">
																		    <div class="m-widget28__tab-items">
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">Concern Person Mobile No.:</span>
																					<span><c:if test="${empty leadVo.concernPersonMobileno}">N/A</c:if>${leadVo.concernPersonMobileno}</span>
																				</div>
																			</div>					      	 		      	
																		</div>
																	</div>
																	<!-- end:: Content --> 	
																</div>				 	 
															</div>
														</div>
													</div>
													<c:forEach items="${leadVo.leadContactVos}" var="leadContactVo" varStatus="status">
														<div class="row">
															<div class="col-lg-4 col-md-4 col-sm-12">
																<div class="m-widget28">
																	<div class="m-widget28__container" >	
																		<!-- Start:: Content -->
																		<div class="m-widget28__tab tab-content">
																			<div class="m-widget28__tab-container tab-pane active">
																			    <div class="m-widget28__tab-items">
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Contact Name:</span>
																						<span><c:if test="${empty leadContactVo.name}">N/A</c:if>${leadContactVo.name}</span>
																					</div>
																				</div>					      	 		      	
																			</div>
																		</div>
																		<!-- end:: Content --> 	
																	</div>				 	 
																</div>
															</div>
															<div class="col-lg-4 col-md-4 col-sm-12">
																<div class="m-widget28">
																	<div class="m-widget28__container" >	
																		<!-- Start:: Content -->
																		<div class="m-widget28__tab tab-content">
																			<div class="m-widget28__tab-container tab-pane active">
																			    <div class="m-widget28__tab-items">
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Contact Mobile No.:</span>
																						<span><c:if test="${empty leadContactVo.mobileno}">N/A</c:if>${leadContactVo.mobileno}</span>
																					</div>
																				</div>					      	 		      	
																			</div>
																		</div>
																		<!-- end:: Content --> 	
																	</div>				 	 
																</div>
															</div>
															<div class="col-lg-4 col-md-4 col-sm-12">
																<div class="m-widget28">
																	<div class="m-widget28__container" >	
																		<!-- Start:: Content -->
																		<div class="m-widget28__tab tab-content">
																			<div class="m-widget28__tab-container tab-pane active">
																			    <div class="m-widget28__tab-items">
																					<div class="m-widget28__tab-item">
																						<span class="m--regular-font-size-">Contact Email:</span>
																						<span><c:if test="${empty leadContactVo.email}">N/A</c:if>${leadContactVo.email}</span>
																					</div>
																				</div>					      	 		      	
																			</div>
																		</div>
																		<!-- end:: Content --> 	
																	</div>				 	 
																</div>
															</div>
														</div>
													</c:forEach>
					                            </div>
					                        </div>
					                    </div>
					                    <!--end::Item-->
					                    
					                    <!--begin::Item-->              
					                    <div class="m-accordion__item" style="border-top: 5px solid #ebedf2">
					                        <div class="m-accordion__item-head collapsed"  role="tab" id="bank__payment__accordion_item_head" data-toggle="collapse" href="#bank__payment_accordion_item_body" aria-expanded="false">
					                            <span class="m-accordion__item-title m--font-bolder">Bank & Payment Details</span>  
					                            <span class="m-accordion__item-mode"></span>
					                        </div>
					
					                        <div class="m-accordion__item-body m-accordion__item-body-background collapse" id="bank__payment_accordion_item_body" role="tabpanel" aria-labelledby="bank__payment_accordion_item_head" data-parent="#m_accordion_1" style="background-color: #fff"> 
					                            <div class="m-accordion__item-content">
					                            	<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-12">
															<div class="m-section">
																<h3 class="m-section__heading">
																	Bank Details
																</h3>
																<div class="m-section__content p-2">
																	<table class="table m-table">
																	  	<tbody>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Bank Name:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${leadVo.bankName}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Branch Name:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${leadVo.bankBranch}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Account No.:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${leadVo.bankAccountNo}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">IFSC Code:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${leadVo.bankIfsc}</td>
																	    	</tr>
																	  	</tbody>
																	</table>
																</div>
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-12">
															<div class="m-section">
																<h3 class="m-section__heading">
																	Payment Details
																</h3>
																<div class="m-section__content p-2">
																	<table class="table m-table">
																	  	<tbody>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Payment Type:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${leadVo.paymentType}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">No. of Credit Days:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${leadVo.creditDays}</td>
																	    	</tr>
																	    	<tr class="row">
																	      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Max. Credit Limit:</th>
																		      	<td class="col-lg-8 col-md-8 col-sm-12">${leadVo.maximumCreditLimit}</td>
																	    	</tr>
																	  	</tbody>
																	</table>
																</div>
															</div>
														</div>
													</div>
					                            </div>
					                        </div>
					                    </div>
					                    <!--end::Item-->
					                    <!--begin::Item-->              
					                    <div class="m-accordion__item" style="border-top: 5px solid #ebedf2">
					                        <div class="m-accordion__item-head collapsed"  role="tab" id="reference_accordion_item_head" data-toggle="collapse" href="#reference_accordion_item_body" aria-expanded="false">
					                            <span class="m-accordion__item-title m--font-bolder">Reference Details</span>  
					                            <span class="m-accordion__item-mode"></span>
					                        </div>
					
					                        <div class="m-accordion__item-body m-accordion__item-body-background collapse" id="reference_accordion_item_body" class="" role="tabpanel" aria-labelledby="reference_accordion_item_head" data-parent="#m_accordion_1" style="background-color: #fff"> 
					                            <div class="m-accordion__item-content">
					                            	<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-12">
															<div class="m-widget28">
																<div class="m-widget28__container" >	
																	<!-- Start:: Content -->
																	<div class="m-widget28__tab tab-content">
																		<div class="m-widget28__tab-container tab-pane active">
																		    <div class="m-widget28__tab-items">
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">Reference Name:</span>
																					<span><c:if test="${empty leadVo.referenceName}">N/A</c:if>${leadVo.referenceName}</span>
																				</div>
																			</div>					      	 		      	
																		</div>
																	</div>
																	<!-- end:: Content --> 	
																</div>				 	 
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-12">
															<div class="m-widget28">
																<div class="m-widget28__container" >	
																	<!-- Start:: Content -->
																	<div class="m-widget28__tab tab-content">
																		<div class="m-widget28__tab-container tab-pane active">
																		    <div class="m-widget28__tab-items">
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">Reference Mobile no.:</span>
																					<span><c:if test="${empty leadVo.referenceMobileno}">N/A</c:if>${leadVo.referenceMobileno}</span>
																				</div>
																			</div>					      	 		      	
																		</div>
																	</div>
																	<!-- end:: Content --> 	
																</div>				 	 
															</div>
														</div>
													</div>
													<div class="m-divider"><span></span></div>
					                            	<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-12">
															<div class="m-widget28">
																<div class="m-widget28__container" >	
																	<!-- Start:: Content -->
																	<div class="m-widget28__tab tab-content">
																		<div class="m-widget28__tab-container tab-pane active">
																		    <div class="m-widget28__tab-items">
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">Agent:</span>
																					<span>
																						<c:if test="${leadVo.agentId ==0}">N/A</c:if>
																						<c:if test="${leadVo.agentId !=0}">${leadVo.agentId}</c:if>
																					</span>
																				</div>
																			</div>					      	 		      	
																		</div>
																	</div>
																	<!-- end:: Content --> 	
																</div>				 	 
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-12">
															<div class="m-widget28">
																<div class="m-widget28__container" >	
																	<!-- Start:: Content -->
																	<div class="m-widget28__tab tab-content">
																		<div class="m-widget28__tab-container tab-pane active">
																		    <div class="m-widget28__tab-items">
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">Agent Commission:</span>
																					<span>${leadVo.agentCommission}</span>
																				</div>
																			</div>					      	 		      	
																		</div>
																	</div>
																	<!-- end:: Content --> 	
																</div>				 	 
															</div>
														</div>
													</div>
					                            </div>
					                        </div>
					                    </div>
					                    <!--end::Item-->
					                    <!--begin::Item-->
					                    <div class="m-accordion__item" style="border-top: 5px solid #ebedf2">
					                        <div class="m-accordion__item-head collapsed"  role="tab" id="interest_accordion_item_head" data-toggle="collapse" href="#interest_accordion_item_body" aria-expanded="false">
					                            <span class="m-accordion__item-title m--font-bolder">Interest Details</span>  
					                            <span class="m-accordion__item-mode"></span>
					                        </div>
					
					                        <div class="m-accordion__item-body m-accordion__item-body-background collapse" id="interest_accordion_item_body" class="" role="tabpanel" aria-labelledby="interest_accordion_item_head" data-parent="#m_accordion_1" style="background-color: #fff"> 
					                            <div class="m-accordion__item-content">
					                                <div class="row">
														<div class="col-lg-6 col-md-6 col-sm-12">
															<div class="m-widget28">
																<div class="m-widget28__container" >	
																	<!-- Start:: Content -->
																	<div class="m-widget28__tab tab-content">
																		<div class="m-widget28__tab-container tab-pane active">
																		    <div class="m-widget28__tab-items">
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">Monthly Requirement:</span>
																					<span>${leadVo.interestedProductRequirements}</span>
																				</div>
																			</div>					      	 		      	
																		</div>
																	</div>
																	<!-- end:: Content --> 	
																</div>				 	 
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-12">
															<div class="m-widget28">
																<div class="m-widget28__container" >	
																	<!-- Start:: Content -->
																	<div class="m-widget28__tab tab-content">
																		<div class="m-widget28__tab-container tab-pane active">
																		    <div class="m-widget28__tab-items">
																				<div class="m-widget28__tab-item">
																					<span class="m--regular-font-size-">Monthly Requirement Value:</span>
																					<span><c:if test="${empty leadVo.interestedProductValue}">N/A</c:if>${leadVo.interestedProductValue}</span>
																				</div>
																			</div>					      	 		      	
																		</div>
																	</div>
																	<!-- end:: Content --> 	
																</div>				 	 
															</div>
														</div>
													</div>
													<c:if test="${not empty leadVo.interestedProductIds}">
														<div class="m-divider mb-4 mt-4"><span></span></div>
														<div class="row">
															<div class="col-lg-8 col-md-8 col-sm-12">
																<c:set var="interestedProductIdList" scope="page" value="" />
																<table class="table table-sm m-table table-striped m--margin-left-15" id="interested_product_table">
																  	<thead>
																  		<tr class="row">
																	      	<th scope="row" class="col-lg-1 col-md-1 col-sm-12">#</th>
																    		<th scope="row" class="col-lg-11 col-md-11 col-sm-12">
																    			<span>Intrested Products</span>
																    			<span class="float-right">
																    				<!-- <a href="#" data-toggle="modal" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill"
																						data-target="#terms_and_condition" id="terms_condition_button" title="Edit"> <i class="fa fa-edit"></i></a> -->
																				</span>
																    		</th>
																    	</tr>
																  	</thead>
																  	<tbody>
																		<c:forEach items="${productVariantVos}" var="productVariantVo" varStatus="status">
																			<tr class="row">
																		      	<td class="col-lg-1 col-md-1 col-sm-12">
																		      		${status.index+1}
																		      	</td>
																		      	<td class="col-lg-11 col-md-11 col-sm-12">
																		      		${productVariantVo.productVo.categoryVo.categoryName} ${productVariantVo.productVo.name}&nbsp;${productVariantVo.variantName}
																	      		</td>
																	    	</tr>
																	    </c:forEach>
																  	</tbody>
																</table>
															</div>
														</div>
													</c:if>
					                            </div>
					                        </div>
					                    </div>
					                    <!--end::Item-->
					                 </div>
								</div>
							</div>
							<!--end::Portlet-->
						</div>
					</div>
				</div>
		
			</div>
			
			<div class="modal fade" id="new_activity_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">New Activity</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="activity_form" action="/lead/${leadVo.leadId}/activity/save" method="post">
							<div class="modal-body">
							
								<c:if test="${not empty leadVo.assignedEmployee }">
									<div class="m-widget28 m--padding-left-10">
										<!-- <div class="m-widget28__pic m-portlet-fit--sides"></div> -->			  
										<div class="m-widget28__container" >	
											<!-- Start:: Content -->
											<div class="m-widget28__tab tab-content">
												<div class="m-widget28__tab-container tab-pane active">
												    <div class="m-widget28__tab-items">
														<div class="m-widget28__tab-item">
															<span class="m--regular-font-size-">Assigned Employee:</span>
															<span>${leadVo.assignedEmployee.employeeName}</span>
														</div>
													</div>					      	 		      	
												</div>
											</div>
											<!-- end:: Content --> 	
										</div>				 	 
									</div>
								</c:if>
								<c:if test="${empty leadVo.assignedEmployee }">
									<div class="form-group m-form__group row">
										<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Select Employee:</label>
										<div class="col-lg-12 col-md-12 col-sm-12">
											<select class="form-control m-select2" id="employeeId" name="assignedEmployee.employeeId" placeholder="Select Employee">
												<option value="">Select Employee</option>
												<c:forEach items="${employeeVos}" var="employeeVo">																			
													<option value="${employeeVo.employeeId}">
													    ${employeeVo.employeeName}
													</option>
												</c:forEach>	
											</select>
										</div>
									</div>
								</c:if>
								<div class="form-group m-form__group row">
									<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Select Mode of Communication:</label>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<select class="form-control m-select2" id="modeOfCommunication" name="modeOfCommunication" placeholder="Select Mode of Communication">
											<option value="">Select Mode of Communication</option>
											<option value="Showroom">Showroom</option>
											<option value="On Site">On Site</option>
											<option value="Telecaller">Telecaller</option>
											<option value="Email">Email</option>
											<option value="SMS">SMS</option>
										</select>
									</div>
								</div>
								<div class="form-group m-form__group row ">
									<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Remark:</label>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<textarea class="form-control m-input"  maxlength="150" rows="3" name="remark" id="remark" placeholder="Remark"></textarea>
									</div>
								</div>
								<div class="form-group m-form__group row">
									<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Next Schedule Date:</label>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<div class="input-group date" >
											<input type="text" class="form-control m-input todaybtn-datepicker" name="nextScheduleDate" readonly id="nextScheduleDate" data-date-format="dd/mm/yyyy"/>
											<div class="input-group-append">
												<span class="input-group-text">
													<i class="la la-calendar"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary"  id="saveactivity">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			
			<div class="modal fade" id="lead_complete_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Complete Lead</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="lead_complete_form" action="/lead/${leadVo.leadId}/completed/save" method="post">
							<div class="modal-body">
							
								<c:if test="${not empty leadVo.assignedEmployee }">
									<div class="m-widget28 m--padding-left-10">
										<!-- <div class="m-widget28__pic m-portlet-fit--sides"></div> -->			  
										<div class="m-widget28__container" >	
											<!-- Start:: Content -->
											<div class="m-widget28__tab tab-content">
												<div class="m-widget28__tab-container tab-pane active">
												    <div class="m-widget28__tab-items">
														<div class="m-widget28__tab-item">
															<span class="m--regular-font-size-">Assigned Employee:</span>
															<span>${leadVo.assignedEmployee.employeeName}</span>
														</div>
													</div>					      	 		      	
												</div>
											</div>
											<!-- end:: Content --> 	
										</div>				 	 
									</div>
								</c:if>
								<div class="form-group m-form__group row">
									<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Completed Date:</label>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<div class="input-group date" >
											<input type="text" class="form-control m-input todaybtn-datepicker" name="completedDate" readonly id="completedDate" data-date-format="dd/mm/yyyy"/>
											<div class="input-group-append">
												<span class="input-group-text">
													<i class="la la-calendar"></i>
												</span>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group m-form__group row ">
									<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Remark:</label>
									<div class="col-lg-12 col-md-12 col-sm-12">
										<textarea class="form-control m-input"  maxlength="150" rows="3" name="remark" id="remark" placeholder="Remark"></textarea>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary"  id="saveleadcomplete">Save</button>
							</div>
						</form>
					</div>
				</div>
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$(".delete-btn").click(function(e) {
	            
	            var u = $(this).data("url") ? $(this).data("url") : '';
	    		swal({
	                title: "Are you sure?",
	                text: "You won't be able to revert this!",
	                type: "warning",
	                showCancelButton: !0,
	                confirmButtonText: "Yes, delete it!"
	            }).then(function(e) {
	            	e.value && u != ''? window.location.href=u : console.error("CROODS: data-url undefined ")
	            })
	        });
		});
	</script>
	
	<script type="text/javascript">
		
		var employeeValidator = {
				validators: {
					notEmpty: {message: 'please select Employee. '},
				}
			};
	
		$(document).ready(function (){
			
			//-------------------- Activity Validation -------------------------------
			$('#activity_form').formValidation({
				framework: 'bootstrap',
				excluded: ":disabled",
				/*live:'disabled', */
				button: {
					selector: "#saveactivity",
					disabled: "disabled"
				},
				icon : null,
				fields : {
					"modeOfCommunication" : {
						validators : {
							notEmpty : {
								message : 'please select Mode of Communication. '
							}
						}
					},
					nextScheduleDate : {
						validators : {
							notEmpty : {
								message : 'The Date is required. '
							}
						}
					},
					
				}
			});
			//-------------------- End Activity Validation ---------------------------
			
			if($("#employeeId").val() != undefined) {
				$('#activity_form').formValidation('addField',"assignedEmployee.employeeId",employeeValidator);
			}
			
			$("#nextScheduleDate").change(function() {
				$('#activity_form').formValidation('revalidateField', 'nextScheduleDate');
			});
			
			$('#new_activity_modal').on('shown.bs.modal', function() {
				
				$('#activity_form').formValidation('resetForm', true);
				
				$("#modeOfCommunication").val("");
				$("#modeOfCommunication").select2({dropdownParent: $("#modeOfCommunication").parent()});
				
				if($("#employeeId").val() != undefined) {
					$("#employeeId").val("");
					$("#employeeId").select2({dropdownParent: $("#employeeId").parent()});
				}
			});
			
			//-------------------- Activity Validation -------------------------------
			$('#lead_complete_form').formValidation({
				framework: 'bootstrap',
				excluded: ":disabled",
				/*live:'disabled', */
				button: {
					selector: "#saveleadcomplete",
					disabled: "disabled"
				},
				icon : null,
				fields : {
					completedDate : {
						validators : {
							notEmpty : {
								message : 'The Date is required. '
							}
						}
					},
					
				}
			});
			//-------------------- End Activity Validation ---------------------------
			
			$("#completedDate").change(function() {
				$('#lead_complete_form').formValidation('revalidateField', 'completedDate');
			});
			
			$('#lead_complete_modal').on('shown.bs.modal', function() {
				
				$('#lead_complete_form').formValidation('resetForm', true);
				
			});
		});
	</script>
</body>
<!-- end::Body -->
</html>