<div class="modal fade" id="farmoparticular_new_modal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog " role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">New Farmo Particular</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true"> &times; </span>
				</button>
			</div>
			<div class="modal-body">
				<form id="farmoparticular_new_form">
					<div class="form-group m-form__group row ">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Name :</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<input class="form-control m-input" maxlength="50" name="name" id="newName" placeholder="Enter Name">
						</div>
					</div>
					<div class="form-group m-form__group row ">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="m-checkbox-inline">
								<label class="m-checkbox m-checkbox--solid m-checkbox--brand">
									<input type="checkbox" name="isActive" id="newIsActive" value="true" checked="checked">Active <span></span>
								</label><i data-toggle="m-tooltip" data-width="auto"
									class="m-form__heading-help-icon flaticon-info"
									title="If Active then only visible in Farmo Type."></i>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">
					Close</button>
				<button type="button" id="savefarmoparticular" class="btn btn-primary">
					Save</button>
			</div>
		</div>
	</div>
</div>
