<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Delivery</title>
	
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Delivery</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="#" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
						
					<div class="row">
					
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<%-- Jaimin <a href="<%=request.getContextPath()%>/delivery/new" class="btn btn-primary m-btn m-btn--icon m-btn--air">
												<span><i class="la la-plus"></i><span>Material Request</span></span>
											</a> --%>
										</div>			
									</div>
									
									<div class="m-portlet__head-tools">
										<a href="#" id="export_print" class="btn btn-metal m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i></a>
										<a href="#" id="export_excel" class="btn btn-success m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top"
											title="Excel"> <i class="fa fa-file-excel"></i>
										</a>
										<a href="#" id="export_pdf" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top"
											title="PDF"> <i class="fa fa-file-pdf"></i>
										</a>
									</div>
								</div>
								<div class="m-portlet__body" >
									<div  class="m_datatable"  >
										<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 collapse" id="m_datatable_group_action_form">
											<div class="row align-items-center">
												<div class="col-xl-12">
													<div class="m-form__group m-form__group--inline">
														<div class="m-form__label m-form__label-no-wrap">
															<label class="m--font-bold m--font-danger-">
																Selected <span id="m_datatable_selected_number"></span>
																records:
															</label>
														</div>
														<div class="m-form__control">
															<div class="btn-toolbar">
																<button class="btn btn-info m-btn btn-sm m-btn m-btn--icon" type="button" id="m_datatable_check_all" onclick="sendSMS()" data-target="#modal_send_msg"  data-toggle="modal"><i class="fa fa-send"></i>&nbsp;Send SMS</button>
																&nbsp;&nbsp;&nbsp;	
																<!-- <button class="btn btn-info m-btn btn-sm m-btn m-btn--icon " type="button" id="m_datatable_check_all" onclick="sendMail()" data-target="#modal_send_mail"  data-toggle="modal"><i class="fa fa-envelope"></i>&nbsp;Send Mail</button> -->	
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<table class="table table-striped- table-bordered table-hover table-checkable" id="delivery_table">
											<thead>
						  						<tr>
				  									<th>
				  									<label class="m-checkbox m-checkbox--bold m-checkbox--state-brand m--margin-top-0 m--margin-bottom-0" style="display:initial">
			  										<input type="checkbox" class="checkbox" id="select-all" name="">
			  											<span></span>
			  										</label></th>
				  									<th>Delivery No.</th>
				  									<th>Delivery Date</th>
				  									
				  									<th>Sales Order</th>			  									
				  									<th>Actions</th>
							  					</tr>
											</thead>			
										</table>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
							
						</div>
				
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		<div class="modal fade" id="modal_send_msg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog " role="document">
				<input type="hidden" name="salesVo.contactVo.companyMobileno" id="phoneno" value=""/>
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							Send SMS
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
					</div>
					<div class="modal-body">
							<p class="col-12 " id="errorMessageCategory" style="color: red;"></p>
							<div class="form-group m-form__group row">
		            			<label class="col-12 col-form-label">Message <span class="required">*</span></label>
		              			<div class="col-12 ">
		               				<textarea name="message" id="offer_message" class="form-control m-input" maxlength="160" rows="4" placeholder="Message" ></textarea>
		               			</div>
		             		</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="send_sms_btn">Send</button>
	          		    	<button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/datatable/jquery.spring-friendly.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		var DatatablesDataSourceHtml = {
		    init: function() {
		        $("#delivery_table").DataTable({
		            responsive: !0,
		            pageLength: 10,
		            searchDelay: 500,
					processing: !0,
					serverSide: true,
					ajax: {
						url: "<%=request.getContextPath()%>/delivery/datatable",
						type: "POST",
					},
					lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
					  columns: [{
			                data: "deliveryId"
			            }, {
			                data: "deliveryNo"
			            }, {
			                data: "deliveryDate"
			            }, {
			                data: "salesVo"
			            },{
			                data: "deliveryId"
			            }],
			            
					columnDefs: [{
							targets: 4,
							orderable: !1,
							render: function(a, e, t, n) {
								var action = "";
								if(t.salesVo.status!='Completed')
									{
									action += '<a href="${pageContext.request.contextPath}/delivery/'+t.deliveryId+'/edit" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'
									action += '<button type="button" data-url="${pageContext.request.contextPath}/delivery/'+t.deliveryId+'/delete" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill delete-btn" title="Delete"> <i class="fa fa-trash"></i></button>'
									}
								return action;
							}
			            },{
							targets: 0,
							orderable: !1,
							render: function(a, e, t, n) {
								var  c ="";
								c+= '<span class="m-checkbox-inline"><label class="m-checkbox m-checkbox--bold m-checkbox--state-brand m--margin-top-0 m--margin-bottom-0" style="display:initial"><input type="checkbox" class="checkbox" id="" name="" data-mobileno="'+t.salesVo.contactVo.companyMobileno+'" value="'+t.salesVo.contactVo.companyMobileno+'"><span></span></label></span>';
								c+= '<span class="pull-right">'+(n.row+n.settings._iDisplayStart+1)+'</span>';
								return c;
							}
						},{
							targets: 2,
							render: function(a, e, t, n) {
								return moment(a).format('DD/MM/YYYY');
							}
						},{
							targets: 3,
							render: function(a, e, t, n) {
								return t.salesVo.prefix + t.salesVo.salesNo;
							}
						},{
							targets:1,
			                orderable: !1,
			               	render: function(a, e, t, n) {
			                  return '\n  <a href="/delivery/'+t.deliveryId+'" class="m-link m--font-bolder" aria-expanded="true">\n '+t.prefix+t.deliveryNo+'</a>\n '
			                }
					}],
		            fixedHeader: {
		                header: false,
		                footer: false
		            }
		            
		        })
		    }
		};
		
		jQuery(document).ready(function() {
			
			DatatablesDataSourceHtml.init();
			
			$(document).on("click",".delete-btn",function(e) {
	            
	         var u = $(this).data("url") ? $(this).data("url") : '';
	    	  swal({
	                title: "Are you sure?",
	                text: "You won't be able to revert this!",
	                type: "warning",
	                showCancelButton: !0,
	                confirmButtonText: "Yes, delete it!"
	               }).then(function(e) {
	            	e.value && u != ''? window.location.href=u : console.error("CROODS: data-url undefined ")
	            			
	            })
	        });
			$("#delivery_table").on("click", ".checkbox", function() {
				var total=$('.checkbox:checked').length;
				var tt=$('.checkbox').length;
				
				if(total>0){
					$('#m_datatable_group_action_form').collapse("show");
					if(total==tt){total=total-1;}
					$('#m_datatable_selected_number').html(total);
				}else{
					$('#m_datatable_group_action_form').collapse("hide");
				}
					
	  		});
			
			$("#select-all" ).click(function() {
	  			if($("#select-all").prop('checked') == true) {
	  				$("#delivery_table").find(".checkbox").prop('checked',true);
	  			}
	  			else{
	  				$("#delivery_table").find(".checkbox").prop('checked',false);
	  			}
	  		});
				
			$("#send_sms_btn" ).click(function() {
		    	
		    	if($("#offer_message").val()!=""){
		    		$.post("/delivery/sendsms", { phoneno: $("#phoneno").val(), message: $("#offer_message").val() })
		    		  .done(function( data ) {
		    			  toastr.options = {
		    					  "closeButton": true,
		    					  "debug": false,
		    					  "newestOnTop": false,
		    					  "progressBar": false,
		    					  "positionClass": "toast-top-center",
		    					  "preventDuplicates": true,
		    					  "onclick": null,
		    					  "showDuration": "300",
		    					  "hideDuration": "1000",
		    					  "timeOut": "5000",
		    					  "extendedTimeOut": "1000",
		    					  "showEasing": "swing",
		    					  "hideEasing": "linear",
		    					  "showMethod": "fadeIn",
		    					  "hideMethod": "fadeOut"
		    					  
		    					};
		    				toastr.success("","Message send successfully");
		    				$('#modal_send_msg').modal('hide');
		    		  });
		    	}else{
		    		toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "newestOnTop": false,
							  "progressBar": false,
							  "positionClass": "toast-top-center",
							  "preventDuplicates": true,
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};
							toastr.error("","Please Write Message");
				    	}
				    			    	
				    });
				});
		
			function sendSMS() {
				
				var phoneNo=""
					$("#delivery_table").find("tbody").find(".checkbox").each(function(){
					if($(this).prop('checked') == true) {
						if($(this).val()!=""){
							phoneNo=phoneNo+$(this).val()+",";
						}
		  			}
				});
				$("#phoneno").val(phoneNo);
				 $("#offer_message").html("Hello world!"); 
			}
	</script>
	
</body>
<!-- end::Body -->
</html>