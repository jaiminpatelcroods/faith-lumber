<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Edit | ${deliveryVo.prefix}${deliveryVo.deliveryNo} | Delivery</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		select[readonly].select2-hidden-accessible + .select2-container {
		  pointer-events: none;
		  touch-action: none;
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		input::-webkit-outer-spin-button,
		input::-webkit-inner-spin-button {
		    /* display: none; <- Crashes Chrome on hover */
		    -webkit-appearance: none;
		    margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
		}
		
		input[type=number] {
		    -moz-appearance:textfield; /* Firefox */
		}
		/* table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		} */
		
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${deliveryVo.prefix}${deliveryVo.deliveryNo}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/delivery" class="m-nav__link">
										<span class="m-nav__link-text">Delivery</span>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/delivery/${deliveryVo.deliveryId}" class="m-nav__link">
										<span class="m-nav__link-text">${deliveryVo.prefix}${deliveryVo.deliveryNo}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="delivery_form" action="/delivery/save" method="post">	
						<input type="hidden" name="prefix" id="prefix" value="${deliveryVo.prefix}"/>
						<input type="hidden" name="deliveryNo" id="deliveryNo" value="${deliveryVo.deliveryNo}"/>
						<input type="hidden" name="deliveryId" id="deliveryId" value="${deliveryVo.deliveryId}"/>
						
						<input type="hidden" name="total" id="total" value="0"/>
						
						<c:if test="${not empty deliveryVo.salesVo.salesId}">
							<input type="hidden" id="parentId" name="salesVo.salesId" value="${deliveryVo.salesVo.salesId}"/>
						</c:if>
						<c:if test="${not empty deliveryVo.contactVo.contactId}">
							<input type="hidden" name="contactVo.contactId" value="${deliveryVo.contactVo.contactId}"/>
						</c:if>
						
						<input type="hidden" id="updateStatus" name="updateStatus" value="${deliveryVo.salesVo.status}"/>
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">Delivery</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">	
												
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Against SO :</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<span>
															${deliveryVo.salesVo.prefix}${deliveryVo.salesVo.salesNo}
														</span>
													</div>
												</div>
												
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Delivery Date:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input todaybtn-datepicker" id="deliveryDate"  name="deliveryDate" readonly placeholder="Delivery Date" data-date-format="dd/mm/yyyy"
																data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'
																value='<fmt:formatDate pattern="dd/MM/yyyy" value="${deliveryVo.deliveryDate}"/>'/>
															<div class="input-group-append">
																<span class="input-group-text"><i class="la la-calendar"></i></span>
															</div>
														</div>
													</div>
												</div>
												
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Transport:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group" >
															<select class="form-control m-select2 contact-select2" id="transportId" name="contactTransportVo.contactId" data-type="${Constant.CONTACT_TRANSPORT}" placeholder="Select Transport">
																<option value="">Select Transport</option>
																<c:forEach items="${contactTransportVos}" var="contactVo">
																	<option value="${contactVo.contactId}"
																		<c:if test="${deliveryVo.contactTransportVo.contactId == contactVo.contactId}">selected="selected"</c:if>>
																		${contactVo.companyName}
																	</option>
																</c:forEach>
															</select>
														</div>
													</div>
												</div>
												
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Delivery No.:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<span>
															${deliveryVo.prefix}${deliveryVo.deliveryNo}
														</span>
													</div>
												</div>
												
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Vehicle No / LR No:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="vehicleNo" placeholder="Vehicle No / LR No" value="${deliveryVo.vehicleNo}">
													</div>
												</div>
												
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Notes:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<textarea class="form-control m-input" placeholder="Notes" name="note">${deliveryVo.note}</textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="table-responsive m--margin-top-20">
													<table class="table m-table table-bordered table-hover" id="product_table">
														<thead>
															<tr>
																<th>#</th>
																<th>Product</th>
																<th class="text-right">SO Qty</th>
																<th class="text-right">Total Delivered Qty</th>
																<th class="text-right">Edit Delivery Qty</th>
																<th class="text-right">Remains Qty</th>
																<th></th>
															</tr>
														</thead>
													    <tbody data-table-list="">
													    	<c:set scope="page" var="index" value="0"/>
													    	<c:forEach items="${deliveryVo.deliveryItemVos}" var="deliveryItemVo" varStatus="status">
													    		<tr data-table-item="${index}" class="">
														    		<td class="" style="width: 50px;">
														    			<span data-item-index>${index+1}</span>
														    		</td>
															        <td class="p-0" style="width: 370px;">
															        	<div class="form-group m-form__group p-0">
																		<select class="form-control m-input form-control-sm m-input1 m-select2" name="deliveryItemVos[${index}].productVariantVo.productVariantId" id="productVariantId${index}" onchange="getProductVariantInfo(${index})" placeholder="Select Product" readonly="readonly">
																			<option value="">Select Product</option>
																			<option value="${deliveryItemVo.productVariantVo.productVariantId}" selected="selected">${deliveryItemVo.productVariantVo.productVo.name} ${deliveryItemVo.productVariantVo.variantName}</option>
																		</select>
																		</div>
															        </td>
															        <td class="p-0" style="width: 270px;">
															        	<div class="form-group m-form__group p-0">
															        		<div class="m-input-icon m-input-icon--right">
																				<input type="text" class="form-control form-control m-input1 text-right" id="soQty${index}" name="deliveryItemVos[${index}].soQty" placeholder="Qty" value="${deliveryItemVo.soTotalQty}" readonly="readonly">
																				<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																			</div>
															        	</div>
															        </td>
															        <td class="p-0" style="width: 270px;">
															        	<div class="form-group m-form__group p-0">
															        		<div class="m-input-icon m-input-icon--right">
																				<input type="text" class="form-control form-control m-input1 text-right" id="deliveredQty${index}" name="deliveryItemVos[${index}].deliveredQty" placeholder="Qty" value="${deliveryItemVo.deliveredTotalQty-deliveryItemVo.qty}" readonly="readonly">
																				<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																			</div>
															        	</div>
															        </td>
															        
															        <td class="p-0" style="width: 270px;">
															        	<div class="form-group m-form__group p-0">
															        		<div class="m-input-icon m-input-icon--right">
																				<input type="text" class="form-control form-control m-input1 text-right" id="qty${index}" name="deliveryItemVos[${index}].qty" placeholder="Qty" value="${deliveryItemVo.qty}" onchange="setDeliveryQty(${index})" step="Any" max="${deliveryItemVo.soTotalQty}" required="required"/>
																				<span class="m-input-icon__icon m-input-icon__icon--right">
																					<span>
																						<i class="" data-item-uom>
																							${deliveryItemVo.productVariantVo.productVo.unitOfMeasurementVo.measurementCode}
																						</i>
																					</span>
																				</span>
																			</div>
															        	</div>
															        </td>
															        
															        <td class="p-0" style="width: 270px;">
															        	<div class="form-group m-form__group p-0">
															        		<div class="m-input-icon m-input-icon--right">
																				<%-- Jaimin <input type="text" class="form-control form-control m-input1 text-right" id="remainsQty${index}" placeholder="Qty" readonly="readonly" value="${productionItemVo.soTotalQty - productionItemVo.producedTotalQty}" data-item-remains> --%>
																				 <input type="text" class="form-control form-control m-input1 text-right" id="remainsQty${index}" placeholder="Qty" readonly="readonly" value="${(deliveryItemVo.soTotalQty - deliveryItemVo.deliveredTotalQty)}" data-item-remains>
																				<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																			</div>
															        	</div>
															        </td>
															        <td class="p-0" style="width: 40px;">
															        	<!-- <button type="button" data-item-remove="" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill" title="Delete"> <i class="fa fa-times"></i></button> -->
															        	<input type="hidden"  id="deliveryItemId${index}" name="deliveryItemVos[${index}].deliveryItemId" value="${deliveryItemVo.deliveryItemId}">
															        	<input type="hidden" id="salesItemId${index}"  name="deliveryItemVos[${index}].salesItemId" value="${deliveryItemVo.salesItemId}"/>
															        	<input type="hidden" id="oldDeliveredQty${index}"  name="deliveryItemVos[${index}].oldDeliveredQty" value="${deliveryItemVo.qty}"/>
															        </td>
														    	</tr>
														    	<c:set scope="page" var="index" value="${index+1}"/>
													    	</c:forEach>
													    	<tr data-table-item="template" class="m--hide">
													    		<td class="" style="width: 50px;">
													    			<span data-item-index></span>
													    		</td>
														        <td class="p-0" style="width: 370px;">
														        	<div class="form-group m-form__group p-0">
																	<select class="form-control m-input form-control-sm m-input1" name="deliveryItemVos[{index}].productVariantVo.productVariantId" id="productVariantId{index}" onchange="getProductVariantInfo({index})" placeholder="Select Product">
																		<option value="">Select Product</option>
																		<c:forEach items="${productVariantVos}" var="productVariantVo">
																			<option value="${productVariantVo.productVariantId}">${productVariantVo.productVo.categoryVo.categoryName} ${productVariantVo.productVo.name} ${productVariantVo.variantName}</option>
																		</c:forEach>
																	</select>
																	</div>
														        </td>
														        <td class="p-0" style="width: 270px;">
														        	<div class="form-group m-form__group p-0">
														        		<div class="m-input-icon m-input-icon--right">
																			<input type="text" class="form-control form-control m-input1 text-right" id="qty{index}" name="deliveryItemVos[{index}].qty" placeholder="Qty" value="">
																			<span class="m-input-icon__icon m-input-icon__icon--right"><span><i class="" data-item-uom></i></span></span>
																		</div>
														        	</div>
														        </td>
														        <td class="p-0" style="width: 40px;">
														        	<button type="button" data-item-remove="" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill" title="Delete"> <i class="fa fa-times"></i></button>
														        </td>
													    	</tr>
														</tbody>
														<tfoot>
													      <tr>
													        <th></th>
													        <th>
													        	<!-- Jaimin <div class="m-demo-icon mb-0">
																	<div class="m-demo-icon__preview">
																		<span class=""><i class="flaticon-plus m--font-primary"></i></span>
																	</div>
																	<div class="m-demo-icon__class">
																	<a href="JavaScript:void(0)" data-toggel="modal" class="m-link m--font-boldest" onclick="addTableItem()"> Add More</a>
																	</div>
																</div> -->
													        </th>
													        <th><span class="m--font-boldest float-right" id=""></span></th>
													        <th><span class="m--font-boldest float-right" id=""></span></th>
													        <th><span class="m--font-boldest float-right" id=""></span></th>
													        <th><span class="m--font-boldest float-right" id=""></span></th>
													        <th></th>
													      </tr>
													    </tfoot>
												  	</table>
												</div>
											</div>
										</div>
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions m-form__actions--solid m-form__actions--right">
											<button type="submit" class="btn btn-brand" id="savedelivery">
												Submit
											</button>
											
											<a href="/delivery" class="btn btn-secondary">
												Cancel
											</a>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
		
	<script src="<%=request.getContextPath()%>/script/delivery/delivery-script.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			
			setTableIndex(${index});
			
			var $tableItemTemplate=$("#product_table").find("[data-table-item]").not(".m--hide");
			var i = 0;
			$tableItemTemplate.each(function () {
				
				i = $(this).attr("data-table-item");
				$('#delivery_form').formValidation('addField',"deliveryItemVos["+i+"].productVariantVo.productVariantId");
				$('#delivery_form').formValidation('addField',"deliveryItemVos["+i+"].qty",qtyValidator);
				
				setProductRemote(i);
			});
		});
	</script>
	
</body>
<!-- end::Body -->
</html>