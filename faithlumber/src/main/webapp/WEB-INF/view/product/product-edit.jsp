<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Edit | ${productVo.name} | Product</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.css">
	
	<style type="text/css">
		.select2-container{display: block;}
		/* .select2-container {
			width: 100% !important;
			
		} */
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1 {
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Edit Product</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/product" class="m-nav__link">
										<span class="m-nav__link-text">Product</span>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="/product/${productVo.productId}" class="m-nav__link">
										<span class="m-nav__link-text">${productVo.name}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="product_form" action="/product/save" method="post">	
						<input type="hidden" name="productId" id="productId" value="${productVo.productId}"/>
						<input type="hidden" name="haveVariation" id="haveVariation" value="${productVo.haveVariation}"/>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">General Details</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Product Name:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="name" id="name" value="${productVo.name}" placeholder="Product Name"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Print Name:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="displayName" id="displayName" value="${productVo.displayName}" placeholder="Print Name"/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Description:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<textarea class="form-control m-input" name="description" placeholder="Enter Description">${productVo.description}</textarea>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Unit of Measurement:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<select class="form-control m-select2 uom-select2" id="measurementId" name="unitOfMeasurementVo.measurementId" placeholder="Unit of Measurements">
															<option value="">Select Unit Of Measurement</option>
															<c:forEach items="${unitOfMeasurementVos}" var="unitOfMeasurementVo">																			
															    <option value="${unitOfMeasurementVo.measurementId}"
															    <c:if test="${unitOfMeasurementVo.measurementId == productVo.unitOfMeasurementVo.measurementId}">selected="selected"</c:if>>
															    	${unitOfMeasurementVo.measurementName}
															    </option>
															</c:forEach>
														</select>
													</div>
												</div>
												
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Sales Tax:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<select class="form-control m-select2 tax-select2" id="salesTaxId" name="salesTaxVo.taxId" placeholder="Select Sales Tax">
															<option value="">Select Tax</option>	
															<c:forEach items="${taxVos}" var="taxVo">																			
															    <option value="${taxVo.taxId}"
															    <c:if test="${taxVo.taxId == productVo.salesTaxVo.taxId}">selected="selected"</c:if>>
															    	${taxVo.taxName}
															    </option>
															</c:forEach>
														</select>
													</div>
												</div>
												
												<div class="form-group m-form__group row">
												
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="m-checkbox-inline">
															<label class="m-checkbox m-checkbox--solid m-checkbox--brand">
															<input type="checkbox" name="salesTaxIncluded" value="${productVo.salesTaxIncluded}" <c:if test="${productVo.salesTaxIncluded==1}">checked="checked"</c:if>>Sales Tax Included 
															<span></span>
															</label>
														</div>
													</div>
													
												</div>
												
								<c:forEach items="${productVo.productVariantVos}" var="productVariantVo">
									<c:if test="${productVo.haveVariation==0}">
									<div class="col-lg-12 col-md-12 col-sm-12 m-form__group-sub  id="single_variant">
										<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Sales Price:</label>
										<input type="text" class="form-control m-input" id="salesPrice" name="productVariantVos[0].salesPrice" value="${productVariantVo.salesPrice}" placeholder="Sales Price" />
										<input type="hidden" name="productVariantVos[0].variantName" id="variantName" value="${productVariantVo.variantName}"/>
										<input type="hidden" name="productVariantVos[0].position" id="position" value="${productVariantVo.position}"/>
										<input type="hidden" name="productVariantVos[0].productVariantId" id="productVariantId" value="${productVariantVo.productVariantId}"/>
										</div>
									</c:if>
								</c:forEach>
							  </div>
							</div>
						</div>
					</div>	
					<!--end::Portlet-->
				</div>
							
						</div>
						<%-- <div class="row">
							
								<div class="col-lg-12 col-md-12 col-sm-12" id="single_variant">
									<!--begin::Portlet-->
									<div class="m-portlet">
										<div class="m-portlet__body">
											<c:forEach items="${productVo.productVariantVos}" var="productVariantVo">
												<input type="hidden" name="productVariantVos[0].variantName" id="variantName" value="${productVariantVo.variantName}"/>
												<input type="hidden" name="productVariantVos[0].position" id="position" value="${productVariantVo.position}"/>
												<input type="hidden" name="productVariantVos[0].productVariantId" id="productVariantId" value="${productVariantVo.productVariantId}"/>
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														
														<div class="form-group m-form__group row ">
															<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
																<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Sales Price:</label>
																<input type="text" class="form-control m-input" id="salesPrice" name="productVariantVos[0].salesPrice" value="${productVariantVo.salesPrice}" placeholder="Sales Price" />
															</div>
															
															<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
																<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Item Code:</label>
																<input type="text" class="form-control m-input" id="itemCode" name="productVariantVos[0].itemCode" value="${productVariantVo.itemCode}" placeholder="Item Code" />
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub ">
																<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Retailer Price:</label>
																<input type="text" class="form-control m-input" id="retailerPrice" name="productVariantVos[0].retailerPrice" value="${productVariantVo.retailerPrice}" placeholder="Retailer Price" />
															</div>
														</div>
													</div>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="form-group m-form__group row ">
															<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
																<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Wholesaler Price:</label>
																<input type="text" class="form-control m-input" id="wholesalerPrice" name="productVariantVos[0].wholesalerPrice" value="${productVariantVo.wholesalerPrice}" placeholder="Wholesaler Price" />
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub ">
																<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Other Price:</label>
																<input type="text" class="form-control m-input" id="otherPrice" name="productVariantVos[0].otherPrice" value="${productVariantVo.otherPrice}" placeholder="Other Price" />
															</div>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="form-group m-form__group row ">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Item Code:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<input type="text" class="form-control m-input" id="itemCode" name="productVariantVos[0].itemCode" placeholder="Item Code" value="${productVariantVo.itemCode}"/>
															</div>
														</div>
													</div>
												</div>
											</c:forEach>
										</div>
									</div>
									<!--end::Portlet-->
								</div>  --%>
								<div class="col-lg-12 col-md-12 col-sm-12 m--hide" id="variant_button">
									<!--begin::Portlet-->
									<div class="m-portlet">
										<div class="m-portlet__body p-0">
											<div class="m-section__content">
												<div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
													<div class="m-demo__preview" style="border: none">
														<div class="m-stack m-stack--ver m-stack--general m-stack--demo">
															
															<div class="m-stack__item m-stack__item--center m-stack__item--middle m--bg-light" style="border: none">
																<button type="button" class="btn btn-primary" id="variant_yes">Add variant</button>
																<h4></h4>
																<small class="m--font-bolder m--regular-font-size-lg1">Add variants if this product comes in multiple versions, like different sizes or colors.</small>
															</div>
															
														</div>
													</div>
												</div>
											</div>
											
										</div>
									</div>
									<!--end::Portlet-->
								</div>
							
							<c:if test="${productVo.haveVariation==1}">
								<div class="col-lg-12 col-md-12 col-sm-12" id="variant_option">
									<!--begin::Portlet-->
									<div class="m-portlet">
										<div class="m-portlet__head">
											<div class="m-portlet__head-caption">
												<div class="m-portlet__head-title">
													<h3 class="m-portlet__head-text m--font-brand">
														Variants
														<small>product comes in multiple versions, like different sizes or colors.</small>
													</h3>
												</div>			
											</div>
											<!-- <div class="m-portlet__head-tools">
												<ul class="m-portlet__nav">
													<li class="m-portlet__nav-item">
														<button type="button" class="btn btn-link" id="variant_no">Cancel</button>
													</li>
												</ul>
											</div> -->
										</div>
										<div class="m-portlet__body " >
										
											<div class="row" id="variant_option_repeater">
												<div class="col-lg-12 col-md-12 col-sm-12" data-repeater-list="" >
													<c:forEach items="${productVo.productAttributeVos}"  var="productAttributeVo">
														<div class="form-group m-form__group row "  data-repeater-item >
															<input type="hidden" name="productAttributeVos[{index}].position" id="productAttributePosition{index}" value="0"/>
															<input type="hidden" name="productAttributeVos[{index}].productAttributeId" value="${productAttributeVo.productAttributeId}"/>
															<div class="col-lg-2 col-md-2 col-sm-2 m-form__group-sub">
																<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">Option name</label>
																<div class="m-typeahead">
																	<input type="text" class="form-control m-input" name="productAttributeVos[{index}].optionName" value="${productAttributeVo.optionName}" placeholder="eg. Size, Color" />
																</div>
															</div>
															<div class="col-lg-5 col-md-5 col-sm-12 m-form__group-sub ">
																<label class="col-form-label col-lg-12 col-md-12 col-sm-12 ">Option values</label>
																<div class="col-lg-12 col-md-12 col-sm-12">
																	<input type="text" class="form-control m-input" name="productAttributeVos[{index}].optionValues" data-role="tagsinput" value="${productAttributeVo.optionValues}" data-tag-color="label-info" />
																</div>
															</div>
															<div class="col-lg-3 col-md-3 col-sm-3 m-form__group-sub ">
																<label class="col-form-label col-lg-12 col-md-12 col-sm-12 m--padding-left-0">&nbsp;</label>
																<button class="btn btn-outline-primary m--margin-left-20" data-repeater-delete="" onclick="" type="button"><i class="flaticon-delete-1"></i></button>																
															</div>
														</div>
													</c:forEach>
												</div>
												
												<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-10">
													<div class="form-group m-form__group row ">
														<div class="col-lg-3 col-md-3 col-sm-12 m-form__group-sub">
															<button type="button" data-repeater-create="" class="btn btn-secondary m-btn m-btn--custom m-btn--icon m-btn--label-brand">Add another option</button>
														</div>
													</div>
												</div>
											</div>
											
											<div class="row m--margin-top-20" id="variant_container">
												
												<div class="col-lg-12 col-md-12 col-sm-12 ">
													<span class="m--font-bolder" >Modify the variants to be created:</span>
													
													<div class="table-responsive m--margin-top-20">
														<table class="table m-table m-table--head-no-border" id="variant_repeater">
														    
														    <thead>
														      <tr>
														        <!-- <th>#</th> -->
														        <th>Variant</th>
														        <th>Sales Price</th>
														        <!-- <th>Retailer Price</th>
														        <th>Wholesaler Price</th>
														        <th>Other Price</th> -->
														       <!--  <th>
														        	Item Code
														        	<button type="button" style="display: none" data-repeater-create id="variant_repeater-create"></button>
														        	<button onclick="copyVariantDetails()" type="button" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill float-right" title="Delete"> <i class="fa fa-copy"></i></button>
														        </th> -->
														      </tr>
														    </thead>
														    <tbody data-repeater-list="">
														    	<c:forEach items="${productVo.productVariantVos}" var="productVariantVo">
															    	<tr data-repeater-item>
															    		<td>${productVariantVo.variantName}</td>
																        <td>
																        	<div class="form-group m-form__group">
																        		<input type="text" class="form-control m-input" id="salesPrice{index}" name="productVariantVos[{index}].salesPrice" value="${productVariantVo.salesPrice}" placeholder="Sales Price" />
																        	</div>
																        </td>
																        
																        
																        <%-- <td>
																        	<div class="form-group m-form__group">
																        		<input type="text" class="form-control m-input" id="retailerPrice{index}" name="productVariantVos[{index}].retailerPrice" value="${productVariantVo.retailerPrice}" placeholder="Retailer Price" />
																        	</div>
																        </td>
																        <td>
																        	<div class="form-group m-form__group">
																        		<input type="text" class="form-control m-input" id="wholesalerPrice{index}" name="productVariantVos[{index}].wholesalerPrice" value="${productVariantVo.wholesalerPrice}" placeholder="Wholesaler Price" />
																        	</div>
																        </td> --%>
																         <td>
																        	<%-- <div class="form-group m-form__group">
																        		<input type="text" class="form-control m-input" id="otherPrice{index}" name="productVariantVos[{index}].otherPrice" value="${productVariantVo.otherPrice}" placeholder="Other Price" />
																        	</div> --%>
																        	<input type="hidden" name="productVariantVos[{index}].productVariantId" value="${productVariantVo.productVariantId}"/>
																        	<input type="hidden" name="productVariantVos[{index}].variantName" id="variantName{index}" value="${productVariantVo.variantName}"/>
																        	<input type="hidden" name="productVariantVos[{index}].position" id="position{index}" value="${productVariantVo.position}"/>
																        	<input type="hidden" name="productVariantVos[{index}].attributeValue1" id="attributeValue1{index}" value="${productVariantVo.attributeValue1}"/>
																        	<input type="hidden" name="productVariantVos[{index}].attributeValue2" id="attributeValue2{index}" value="${productVariantVo.attributeValue2}"/>
																        	<input type="hidden" name="productVariantVos[{index}].attributeValue3" id="attributeValue3{index}" value="${productVariantVo.attributeValue3}"/>
																        </td>  
																        <%-- <td>
																        	<div class="form-group m-form__group">
																        		<input type="text" class="form-control m-input" id="itemCode{index}" name="productVariantVos[{index}].itemCode" placeholder="Item Code" value="${productVariantVo.itemCode}" />
																        	</div>
																        </td> --%>
															    	</tr>
															    </c:forEach>
															</tbody>
													  	</table>
													</div>
												</div>
											</div>
											
										</div>
									</div>
									<!--end::Portlet-->
									
								</div>
							</c:if>
						
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions--solid m-form__actions--right">
								<button type="submit" class="btn btn-brand" id="save_product">
									Submit
								</button>
								
								<a href="/product" class="btn btn-secondary">
									Cancel
								</a>
							</div>
						</div>
					</form>
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<%@include file="../category-brand/category-modal-new.jsp" %>
		<%@include file="../category-brand/brand-modal-new.jsp" %>
		<%@include file="../unitofmeasurement/unitofmeasurement-modal-new.jsp" %>
		<%@include file="../tax/tax-modal-new.jsp" %>
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/script/product/product-new-script.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/script/category-brand/category-brand-quick-script.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/script/unitofmeasurement/unitofmeasurement-quick-script.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/script/tax/tax-quick-script.js" type="text/javascript"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			
		});
	</script>
	
</body>
<!-- end::Body -->
</html>