<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${farmoTypeVo.farmoTypeName} | Farmo Type</title>
	
	<style type="text/css">
		.select2-container{display: block;}
		
		.variants_table td {
			vertical-align: middle;
		}
		.variants_table img {
			width: 4rem;
			border-radius: 10%;
		}
		.active_img{
			border: 5px solid #716aca
		}
		
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${farmoTypeVo.farmoTypeName}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/setting" class="m-nav__link">
										<span class="m-nav__link-text">Settings</span>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/farmotype" class="m-nav__link">
										<span class="m-nav__link-text">Farmo Type</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">				
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<span class="m-portlet__head-icon">
												<i class="flaticon-cogwheel-2"></i>
											</span>
											<h3 class="m-portlet__head-text m--font-brand">General Details</h3>
										</div>
									</div>
									<div class="m-portlet__head-tools">
										<ul class="m-portlet__nav">
											<li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover">
												<a href="#" class="m-portlet__nav-link m-portlet__nav-link--icon m-portlet__nav-link--icon-xl  m-dropdown__toggle">
													<!-- <i class="la la-ellipsis-v"></i> -->
													<i class="la la-ellipsis-h m--font-brand"></i>
												</a>
						                        <div class="m-dropdown__wrapper">
						                            <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
						                            <div class="m-dropdown__inner">
					                                    <div class="m-dropdown__body">              
					                                        <div class="m-dropdown__content">
					                                            <ul class="m-nav">
					                                                <li class="m-nav__item">
					                                                    <a href="/farmotype/${farmoTypeVo.farmoTypeId}/edit" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																			<span><i class="flaticon-edit"></i><span>Edit</span></span>
																		</a>
					                                                    <button data-url="/farmotype/${farmoTypeVo.farmoTypeId}/delete" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right delete-btn">
																			<span><i class="flaticon-delete-1"></i><span>Delete</span></span>
																		</button>
					                                                </li>
					                                            </ul>
					                                        </div>
					                                    </div>
						                            </div>
						                        </div>
											</li>
										</ul>
									</div>
								</div>
								<div class="m-portlet__body" >
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12">
											<div class="media">							 		 
											  	<div class="media-body m--padding-top-10">
											    	<span class="text-mute">Farmo Type Name:</span><br>
													<span class="display-5">${farmoTypeVo.farmoTypeName}</span>		 
											  	</div>
											</div>
										</div>
									</div>
									<div class="row m--margin-top-20">
										<div class="col-lg-6 col-md-6 col-sm-12">
											<div class="m-section">
												<h3 class="m-section__heading">Front Details</h3>
												<div class="m-section__content">
													<div class="row">
														<div class="col-lg-12 col-md-12 col-sm-12">
															<table class="table m-table">
															  	<tbody>
															    	<tr class="row">
																      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Front length :</th>
																      	<td class="col-lg-8 col-md-8 col-sm-12">${farmoTypeVo.frontLength}</td>
															    	</tr>
															    	<tr class="row">
															      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Front seat :</th>
																      	<td class="col-lg-8 col-md-8 col-sm-12">${farmoTypeVo.frontSeat}</td>
															    	</tr>
															    	<tr class="row">
															      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Front thai :</th>
																      	<td class="col-lg-8 col-md-8 col-sm-12">${farmoTypeVo.frontThai}</td>
															    	</tr>
															    	<tr class="row">
															      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Front langot :</th>
																      	<td class="col-lg-8 col-md-8 col-sm-12">${farmoTypeVo.frontLangot}</td>
															    	</tr>
															    	<tr class="row">
															      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Front knee :</th>
																      	<td class="col-lg-8 col-md-8 col-sm-12">${farmoTypeVo.frontKnee}</td>
															    	</tr>
															    	<tr class="row">
															      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Front mori-munda :</th>
																      	<td class="col-lg-8 col-md-8 col-sm-12">${farmoTypeVo.frontMoriMunda}</td>
															    	</tr>
															    	<tr class="row">
															      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Front jati :</th>
																      	<td class="col-lg-8 col-md-8 col-sm-12">${farmoTypeVo.frontJati}</td>
															    	</tr>
															    </tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-12">
											<div class="m-section">
												<h3 class="m-section__heading">Back Details</h3>
												<div class="m-section__content">
													<div class="row">
														<div class="col-lg-12 col-md-12 col-sm-12">
															<table class="table m-table">
															  	<tbody>
															    	<tr class="row">
																      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Back length :</th>
																      	<td class="col-lg-8 col-md-8 col-sm-12">${farmoTypeVo.backLength}</td>
															    	</tr>
															    	<tr class="row">
															      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Back seat :</th>
																      	<td class="col-lg-8 col-md-8 col-sm-12">${farmoTypeVo.backSeat}</td>
															    	</tr>
															    	<tr class="row">
															      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Back thai :</th>
																      	<td class="col-lg-8 col-md-8 col-sm-12">${farmoTypeVo.backThai}</td>
															    	</tr>
															    	<tr class="row">
															      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Back langot :</th>
																      	<td class="col-lg-8 col-md-8 col-sm-12">${farmoTypeVo.backLangot}</td>
															    	</tr>
															    	<tr class="row">
															      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Back knee :</th>
																      	<td class="col-lg-8 col-md-8 col-sm-12">${farmoTypeVo.backKnee}</td>
															    	</tr>
															    	<tr class="row">
															      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Back mori-munda :</th>
																      	<td class="col-lg-8 col-md-8 col-sm-12">${farmoTypeVo.backMoriMunda}</td>
															    	</tr>
															    	<tr class="row">
															      		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Back jati :</th>
																      	<td class="col-lg-8 col-md-8 col-sm-12">${farmoTypeVo.backJati}</td>
															    	</tr>
															    </tbody>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row  m--margin-top-20">
										<div class="col-lg-12 col-md-12 col-sm-12">
											<div class="scrollmenu" >
												<table class="table m-table m-table--head-no-border variants_table" id="variant_repeater">
														    
												   <thead>
												      <tr>
												        <th>Sr. No.</th>
												        <th>Particular Name</th>
												        <th>Measurement</th>
												      </tr>
												    </thead>
												    <tbody>
												    <c:set var="index" scope="page" value="1" />
												    <c:forEach items="${farmoTypeVo.farmoTypeParticularVos}" var="farmoTypeParticularVo">	
												      <tr>												        
												        <td>${index}</td>
												        <td>
												        	${farmoTypeParticularVo.farmoParticularVo.name}
												        </td>
												        <td>${farmoTypeParticularVo.farmoParticularMeasurement}</td>
												      </tr>
												      <c:set var="index" scope="page" value="${index+1}" />
												     </c:forEach>
												    </tbody>
											  	</table>
											  </div>
										</div>
										
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.min.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$(".delete-btn").click(function(e) {
		        var u = $(this).data("url") ? $(this).data("url") : '';
				swal({
		            title: "Are you sure?",
		            text: "You won't be able to revert this!",
		            type: "warning",
		            showCancelButton: !0,
		            confirmButtonText: "Yes, delete it!"
		        }).then(function(e) {
		        	e.value && u != ''? window.location.href=u : console.error("CROODS: data-url undefined ")
		        })
		    });
		});
	</script>
	
</body>
<!-- end::Body -->
</html>