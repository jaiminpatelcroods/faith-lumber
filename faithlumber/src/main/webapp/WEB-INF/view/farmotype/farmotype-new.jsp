<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>New Farmo Type</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">New Farmo Type</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/setting" class="m-nav__link">
										<span class="m-nav__link-text">Settings</span>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/farmotype" class="m-nav__link">
										<span class="m-nav__link-text">Farmo Type</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="farmotype_form" action="/farmotype/save" method="post">	
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
										
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">Farmo Type Details</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<div class="tab-pane active" id="m_tabs_7_1" role="tabpanel">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="form-group m-form__group row">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Farmo Type Name :</label>
															<div class="col-lg-5 col-md-5 col-sm-12">
																<div class="input-group">
																	<input type="text" class="form-control m-input" name="farmoTypeName" id="farmoTypeName"/>
																</div>
															</div>
														</div>
													</div>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="table-responsive m--margin-top-20">
															<table class="table m-table table-bordered table-hover" id="product_table">
															    <thead>
															      <tr>
															        <th>Sr. No.</th>
															        <th>Particular</th>
															        <th>Measurement</th>
															        <th></th>
															      </tr>
															    </thead>
															    <tbody data-table-list="">
															    	<tr data-table-item="template" class="m--hide">
															    		<td class="">
															    			<span data-item-index></span>
															    		</td>
																        <td class="p-0">
																        	<div class="form-group m-form__group p-0">
																			<select class="form-control" id="particular{index}" name="farmoTypeParticularVos[{index}].farmoParticularVo.farmoParticularId" placeholder="Select Particular">
																				<option value="">Select Particular</option>
																				<c:forEach items="${farmoPerticularVos}" var="farmoPerticularVos">
																					<option value="${farmoPerticularVos.farmoParticularId}">${farmoPerticularVos.name}</option>
																				</c:forEach>
																			</select>
																			</div>
																        </td>
																        <td class="p-0">
																        	<div class="form-group m-form__group p-0">
																        		<div class="m-input-icon m-input-icon--right">
																					<input type="text" class="form-control form-control m-input1" id="farmoParticularMeasurement{index}" name="farmoTypeParticularVos[{index}].farmoParticularMeasurement" placeholder="Measurement" value="">
																				</div>
																        	</div>
																        </td>
																        <td class="p-0">
																        	<button type="button" data-item-remove="" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill" title="Delete"> <i class="fa fa-times"></i></button>
																        </td>
															    	</tr>
																</tbody>
																<tfoot>
															      <tr>
															        <th></th>
															        <th>
															        	<div class="m-demo-icon mb-0">
																			<div class="m-demo-icon__preview">
																				<span class=""><i class="flaticon-plus m--font-primary"></i></span>
																			</div>
																			<div class="m-demo-icon__class">
																			<a href="JavaScript:void(0)" data-toggel="modal" class="m-link m--font-boldest" onclick="addTableItem()"> Add More</a>
																			</div>
																		</div>
															        </th>
															        
															        <th></th>
															        <th></th>
															      </tr>
															    </tfoot>
														  	</table>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
							<div class="col-lg-5 col-md-5 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Front length :</label>
													<div class="col-lg-7 col-md-7 col-sm-12">
														<div class="input-group">
															<input type="text" class="form-control m-input" name="frontLength" id="frontLength"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Front seat :</label>
													<div class="col-lg-7 col-md-7 col-sm-12">
														<div class="input-group">
															<input type="text" class="form-control m-input" name="frontSeat" id="frontSeat"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Front thai :</label>
													<div class="col-lg-7 col-md-7 col-sm-12">
														<div class="input-group">
															<input type="text" class="form-control m-input" name="frontThai" id="frontThai"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Front langot :</label>
													<div class="col-lg-7 col-md-7 col-sm-12">
														<div class="input-group">
															<input type="text" class="form-control m-input" name="frontLangot" id="frontLangot"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Front knee :</label>
													<div class="col-lg-7 col-md-7 col-sm-12">
														<div class="input-group">
															<input type="text" class="form-control m-input" name="frontKnee" id="frontKnee"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Front mori-munda :</label>
													<div class="col-lg-7 col-md-7 col-sm-12">
														<div class="input-group">
															<input type="text" class="form-control m-input" name="frontMoriMunda" id="frontMoriMunda"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Front jati :</label>
													<div class="col-lg-7 col-md-7 col-sm-12">
														<div class="input-group">
															<input type="text" class="form-control m-input" name="frontJati" id="frontJati"/>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-2 col-md-2 col-sm-12">
								<div class="col-lg-12 col-md-12 col-sm-12 p-0 text-center" style="margin-top: 170px;">
									<button type="button" class="btn btn-brand m-btn m-btn--icon btn-lg m-btn--icon-only m-btn--pill m-btn--air" onclick="setBackFields()">
										<i class="fa fa-angle-double-right"></i>
									</button>
								</div>	
							</div>
							<div class="col-lg-5 col-md-5 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Back length :</label>
													<div class="col-lg-7 col-md-7 col-sm-12">
														<div class="input-group">
															<input type="text" class="form-control m-input" name="backLength" id="backLength"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Back seat :</label>
													<div class="col-lg-7 col-md-7 col-sm-12">
														<div class="input-group">
															<input type="text" class="form-control m-input" name="backSeat" id="backSeat"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Back thai :</label>
													<div class="col-lg-7 col-md-7 col-sm-12">
														<div class="input-group">
															<input type="text" class="form-control m-input" name="backThai" id="backThai"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Back langot :</label>
													<div class="col-lg-7 col-md-7 col-sm-12">
														<div class="input-group">
															<input type="text" class="form-control m-input" name="backLangot" id="backLangot"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Back knee :</label>
													<div class="col-lg-7 col-md-7 col-sm-12">
														<div class="input-group">
															<input type="text" class="form-control m-input" name="backKnee" id="backKnee"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Back mori-munda :</label>
													<div class="col-lg-7 col-md-7 col-sm-12">
														<div class="input-group">
															<input type="text" class="form-control m-input" name="backMoriMunda" id="backMoriMunda"/>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-5 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-5 col-md-5 col-sm-12">Back jati :</label>
													<div class="col-lg-7 col-md-7 col-sm-12">
														<div class="input-group">
															<input type="text" class="form-control m-input" name="backJati" id="backJati"/>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							
							
							
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions--solid m-form__actions--right">
								<button type="submit" class="btn btn-brand" id="saveFarmoType">
									Submit
								</button>
								<a href="<%=request.getContextPath()%>/farmotype" class="btn btn-secondary">
									Cancel
								</a>
							</div>
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<div class="m-section m--hide" id="roundoff_section">
			<div class="m-section__content">
				<div class="form-group m-form__group row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<input type="text" class="form-control m-input text-right" name="roundoff" id="" placeholder="Roundoff" value="">
					</div>
				</div>
				<div class="form-group m-form__group row">
					<div class="col-lg-6 col-md-6 col-sm-6">
					<a type="button" class="btn btn-secondary" data-popover-close=''>Cancel</a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6">
					<button type="button" class="btn btn-brand float-right" id="" onclick="setRoundoff()">Set</button>
					</div>
				</div>
			</div>
		</div>
		
		<div class="m-section m--hide" id="address_template">
			<div class="m-section__content" data-address-list="">
				<div class="row" data-address-item="">
					<div class="col-lg-12 col-md-12 col-sm-12 mt-3 mb-3"><div class="m-divider"><span></span></div></div>
					<div class="col-lg-12 col-md-12 col-sm-12">
						<h4 class=""><small class="text-muted" data-address-name="">Nilesh Desai</small> <button type="button" data-change-address="" class="btn btn-outline-brand btn-sm float-right ml-5">Select</button></h4>
						<p class="mb-0">
							<span class="" data-address-line-1="">Shapath X</span>
						</p>
						<p class="mb-0">
							<span class="" data-address-line-2="">Shamal Cross Road,</span>
						</p>
						<p class="mb-0">
							<span class="" data-address-pincode="">380001</span>
							<span class="" data-address-city="">Ahmedavad,</span>
						</p>
						<p class="mb-0">
							<span class="" data-address-state="">Gujarat,</span>
							<span class="" data-address-country="">India</span>
						</p>
						<p class="mb-0">
							<span class="m--font-info"><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno="">9714866160</span></span>
						</p>
					</div>
				</div>
			</div>
		</div>
		
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/script/farmotype/farmotype-script.js" type="text/javascript"></script>
	<script type="text/javascript">
		
	</script>
	
</body>
<!-- end::Body -->
</html>