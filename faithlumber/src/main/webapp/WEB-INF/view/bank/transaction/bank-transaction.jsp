<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="com.croods.bssgroup.constant.Constant"%>
<html>
<head>
	
	<%@include file="../../header/head.jsp" %>
	
	<title>Bank Transaction</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Bank Transaction</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
						
					<div class="row">
						
						<!-- Department -->
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<a href="<%=request.getContextPath() %>/bank/transaction/new" class="btn btn-primary m-btn m-btn--icon m-btn--air">
												<span><i class="la la-plus"></i><span>Bank Transaction</span></span>
											</a>
										</div>
									</div>
								</div>
								<div class="m-portlet__body" >

									<ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
										<li class="nav-item m-tabs__item">
											<a class="nav-link m-tabs__link active" data-toggle="tab" href="#withdraw_tab" role="tab">Withdraw Cash</a>
										</li>
										<li class="nav-item m-tabs__item">
											<a class="nav-link m-tabs__link" data-toggle="tab" href="#deposit_tab" role="tab">Deposit Cash</a>
										</li>
										<li class="nav-item m-tabs__item">
											<a class="nav-link m-tabs__link" data-toggle="tab" href="#transfer_tab" role="tab">Transfer</a>
										</li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="withdraw_tab" role="tabpanel">
											<div  class="m_datatable"  >
												<table class="table table-striped- table-bordered table-hover table-checkable" id="withdraw_table">
													<thead>
								  						<tr>
						  									<th>#</th>
						  									<th>Voucher No.</th>
						  									<th>Transaction Date</th>
															<th>From Account</th>
															<th>Amount</th>
															<th>Actions</th>
									  					</tr>
													</thead>
													<tbody>
														<c:set var="bankTransactionVoType" scope="page" value="${bankTransactionVos.stream().filter( ppp -> ppp.transactionType==Constant.BANK_WITHDRAW).toList()}" />
														<c:if test="${bankTransactionVoType.size()!=0}">
															<c:forEach items="${bankTransactionVoType}" var="bankTransactionVo" varStatus="status">
																<tr>
																	<td>${status.index+1}</td>
																	<td>
																		<a href="${pageContext.request.contextPath}/bank/transaction/${bankTransactionVo.bankTransactionId}"
																			class="m-link m--font-bolder">${bankTransactionVo.prefix}${bankTransactionVo.voucherNo}</a>
																	</td>
																	<td><fmt:formatDate pattern="dd/MM/yyyy" value="${bankTransactionVo.transactionDate}"/></td>
																	<td>${bankTransactionVo.fromBankVo.bankName}</td>
																	<td>${bankTransactionVo.debitAmount}</td>
																	<td>
																		<a href="${pageContext.request.contextPath}/bank/transaction/${bankTransactionVo.bankTransactionId}/edit"  class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill"
																			title="Edit"> <i class="fa fa-edit"></i></a>
																		<button data-url="${pageContext.request.contextPath}/bank/transaction/${bankTransactionVo.bankTransactionId}/delete" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill delete-btn"
																			title="Delete"> <i class="fa fa-trash"></i></button>
																	</td>
																</tr>
															</c:forEach>
														</c:if>
													</tbody>			
												</table>
											</div>
										</div>
										<div class="tab-pane" id="deposit_tab" role="tabpanel">
											<div  class="m_datatable"  >
												<table class="table table-striped- table-bordered table-hover table-checkable" id="deposit_table">
													<thead>
								  						<tr>
						  									<th>#</th>
						  									<th>Voucher No.</th>
						  									<th>Transaction Date</th>
															<th>To Account</th>
															<th>Amount</th>
															<th>Actions</th>	
									  					</tr>
													</thead>
													<tbody>
														<c:set var="bankTransactionVoType" scope="page" value="${bankTransactionVos.stream().filter( ppp -> ppp.transactionType==Constant.BANK_DEPOSIT).toList()}" />
														<c:if test="${bankTransactionVoType.size()!=0}">
															<c:forEach items="${bankTransactionVoType}" var="bankTransactionVo" varStatus="status">
																<tr>
																	<td>${status.index+1}</td>
																	<td>
																		<a href="${pageContext.request.contextPath}/bank/transaction/${bankTransactionVo.bankTransactionId}"
																			class="m-link m--font-bolder">${bankTransactionVo.prefix}${bankTransactionVo.voucherNo}</a>
																	</td>
																	<td><fmt:formatDate pattern="dd/MM/yyyy" value="${bankTransactionVo.transactionDate}"/></td>
																	<td>${bankTransactionVo.toBankVo.bankName}</td>
																	<td>${bankTransactionVo.creditAmount}</td>
																	<td>
																		<a href="${pageContext.request.contextPath}/bank/transaction/${bankTransactionVo.bankTransactionId}/edit"  class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill"
																			title="Edit"> <i class="fa fa-edit"></i></a>
																		<button data-url="${pageContext.request.contextPath}/bank/transaction/${bankTransactionVo.bankTransactionId}/delete" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill delete-btn"
																			title="Delete"> <i class="fa fa-trash"></i></button>
																	</td>
																</tr>
															</c:forEach>
														</c:if>
													</tbody>			
												</table>
											</div>
										</div>
										<div class="tab-pane" id="transfer_tab" role="tabpanel">
											<div  class="m_datatable"  >
												<table class="table table-striped- table-bordered table-hover table-checkable" id="transfer_table">
													<thead>
								  						<tr>
						  									<th>#</th>
						  									<th>Voucher No.</th>
						  									<th>Transaction Date</th>
															<th>From Account</th>
															<th>To Account</th>
															<th>Amount</th>
															<th>Actions</th>
									  					</tr>
													</thead>
													<tbody>
														<c:set var="bankTransactionVoType" scope="page" value="${bankTransactionVos.stream().filter( ppp -> ppp.transactionType==Constant.BANK_TRANSFER).toList()}" />
														<c:if test="${bankTransactionVoType.size()!=0}">
															<c:forEach items="${bankTransactionVoType}" var="bankTransactionVo" varStatus="status">
																<tr>
																	<td>${status.index+1}</td>
																	<td>
																		<a href="${pageContext.request.contextPath}/bank/transaction/${bankTransactionVo.bankTransactionId}"
																			class="m-link m--font-bolder">${bankTransactionVo.prefix}${bankTransactionVo.voucherNo}</a>
																	</td>
																	<td><fmt:formatDate pattern="dd/MM/yyyy" value="${bankTransactionVo.transactionDate}"/></td>
																	<td>${bankTransactionVo.fromBankVo.bankName}</td>
																	<td>${bankTransactionVo.toBankVo.bankName}</td>
																	<td>${bankTransactionVo.creditAmount}</td>
																	<td>
																		<a href="${pageContext.request.contextPath}/bank/transaction/${bankTransactionVo.bankTransactionId}/edit"  class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill"
																			title="Edit"> <i class="fa fa-edit"></i></a>
																		<button data-url="${pageContext.request.contextPath}/bank/transaction/${bankTransactionVo.bankTransactionId}/delete" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill delete-btn"
																			title="Delete"> <i class="fa fa-trash"></i></button>
																	</td>
																</tr>
															</c:forEach>
														</c:if>
													</tbody>			
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
						<!-- End Department -->
					</div>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	
	var DatatablesDataSourceHtml = {
	    init: function() {
	        $("#withdraw_table").DataTable({
	        	responsive: !0,
	        });
	        $("#deposit_table").DataTable({
	        	responsive: !0,
	        });
	        $("#transfer_table").DataTable({
	        	responsive: !0,
	        });
	    }
	};
	
	$(document).ready(function() {
		
		DatatablesDataSourceHtml.init();
		
		$(".delete-btn").click(function(e) {
            
            var u = $(this).data("url") ? $(this).data("url") : '';
    		swal({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, delete it!"
            }).then(function(e) {
            	e.value && u != ''? window.location.href=u : console.error("CROODS: data-url undefined ")
            })
        });
		
	});
	
	</script>	
</body>
<!-- end::Body -->
</html>