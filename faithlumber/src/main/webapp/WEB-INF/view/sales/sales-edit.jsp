<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Edit | ${salesVo.prefix}${salesVo.salesNo} | ${displayType} | Sales</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.css"/>
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		
		.card-container {
		  cursor: pointer;
		  height: 100%;
		  perspective: 600;
		  position: relative;
		  width: 55px;
		}
		.card {
		  height: 100%;
		  position: absolute;
		  transform-style: preserve-3d;
		  transition: all 1s ease-in-out;
		  width: 100%;
		}
		.card-hover {
		  transform: rotateY(180deg);
		}
		.card-hover .card-btn {
			/* display: none; */		
		}
		.card .side {
		  backface-visibility: hidden;
		  /* border-radius: 6px; */
		  height: 100%;
		  position: absolute;
		  overflow: hidden;
		  width: 100%;
		}
		.card .back {
		   background: #eaeaed !important;
		  /*color: #0087cc;
		  line-height: 150px; */
		  /* text-align: center; */
		  transform: rotateY(180deg);
		}
		.row_custom_input_box_td{
			padding-left: 5px;
			padding-right: 5px;
			width: 100%;
			text-align: right;
		}
		
		select[readonly].select2-hidden-accessible + .select2-container {
		  pointer-events: none;
		  touch-action: none;
		}
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Edit ${displayType}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/sales/${type}" class="m-nav__link">
										<span class="m-nav__link-text">${displayType}</span>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/sales/${type}/${salesVo.salesId}" class="m-nav__link">
										<span class="m-nav__link-text">${salesVo.prefix}${salesVo.salesNo}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="sales_form" action="/sales/${type}/save" method="post">	
						
						<input type="hidden" name="salesId" id="salesCheckId" value="${salesVo.salesId}"/>
						<input type="hidden" name="taxType" id="taxType" value="${salesVo.taxType}"/>
						<input type="hidden" name="total" id="total" value="${salesVo.total}"/>
						<input type="hidden" id="state_code" value="${sessionScope.stateCode}"/>
						<input type="hidden" id="roundoff" name="roundoff" value="${salesVo.roundoff}"/>
						<input type="hidden" id="paidAmount" name="paidAmount" value="${salesVo.paidAmount}"/>
						<input type="hidden" name="termsAndConditionIds" id="termsAndConditionIds" value="${salesVo.termsAndConditionIds}"/>
						<input type="hidden" name="deleteSalesItemIds" id="deleteSalesItemIds" value=""/>
						<input type="hidden" name="deleteAdditionalChargeIds" id="deleteAdditionalChargeIds" value=""/>
						
						
						<div class="row">
							<div class="col-lg-5 col-md-5 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12 p-0">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Customer:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<select class="form-control m-select2 contact-select2" id="contactVo" name="contactVo.contactId" <c:if test="${type==Constant.SALES_CREDIT_NOTE}">readonly="readonly"</c:if> onchange="getContactInfo(0)" data-type="${Constant.CONTACT_CUSTOMER}" placeholder="Select Customer">
															<option value="">Select Customer</option>
															<c:forEach items="${contactVos}" var="contactVo">
																<option value="${contactVo.contactId}" 
																	<c:if test="${contactVo.contactId == salesVo.contactVo.contactId}">selected="selected"</c:if>>
																	${contactVo.code}
																</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">GSTIN:</span>
																		<span id="lblContactGSTIN"><c:if test="${empty salesVo.contactVo.gstin}">-</c:if>${salesVo.contactVo.gstin}</span>
																	</div>
																	<!-- <div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply</span>
																		<span>F Gear</span>
																	</div> -->
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply:</span>
																		<span id="lblPlaceofSupply">${salesVo.shippingStateName}</span>
																	</div>
																	<!-- <div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply</span>
																		<span>F Gear</span>
																	</div> -->
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-section mt-3 m--margin-bottom-15">
													<h3 class="m-section__heading">Billing Address</h3>
													<h5 class=""><small class="text-muted m--hide" data-address-message="">Billing Address is not provided</small></h5>
													<div class="m-section__content" id="sales_billing_address">
														<input type="hidden" name="billingAddressId" id="billingAddressId" value="0"/>
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12">																
																<h5 class=""><small class="text-muted" data-address-name="">${salesVo.billingCompanyName}</small></h5>
																<p class="mb-0">
																	<span data-address-line-1="">${salesVo.billingAddressLine1}</span>
																</p>
																<p class="mb-0">
																	<span data-address-line-2="">${salesVo.billingAddressLine2}</span>
																</p>
																<p class="mb-0">
																	<span data-address-pincode="">${salesVo.billingPinCode}</span>
																	<span data-address-city="">${salesVo.billingCityName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																</p>
																<p class="mb-0">
																	<span data-address-state="">${salesVo.billingStateName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																	<span data-address-country="">${salesVo.billingCountriesName}</span>
																</p>
																<p class="mb-0">
																	<span class="">
																		<i class="la la-phone align-middle"></i> 
																		<span class="" data-address-phoneno="">
																			<c:if test="${empty salesVo.contactVo.companyMobileno}">Mobile no. is not provided</c:if>${salesVo.contactVo.companyMobileno}
																		</span>
																	</span>
																</p>
																<button type="button" class="btn btn-link" data-toggle="m-popover" data-trigger="click" 
																	title="Billing Address <a href='#' data-popover-close='' class='m--font-bolder m-link m-link--state  m-link--danger float-right'>Cancel</a>" data-html="true" data-content="" id="billing_address_btn">Change Address</button>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-section mt-3 m--margin-bottom-15">
													<h3 class="m-section__heading">Shipping Address</h3>
													<!-- <div class="m-divider"><span></span></div> -->
													<h5 class=""><small class="text-muted m--hide" data-address-message="">Shipping Address is not provided</small></h5>
													<div class="m-section__content" id="sales_shipping_address">
														<input type="hidden" name="shippingAddressId" id="shippingAddressId" value="0"/>
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12">
																<h5 class=""><small class="text-muted" data-address-name="">${salesVo.shippingCompanyName}</small></h5>
																<p class="mb-0">
																	<span data-address-line-1="">${salesVo.shippingAddressLine1}</span>
																</p>
																<p class="mb-0">
																	<span data-address-line-2="">${salesVo.shippingAddressLine2}</span>
																</p>
																<p class="mb-0">
																	<span data-address-pincode="">${salesVo.shippingPinCode}</span>
																	<span data-address-city="">${salesVo.shippingCityName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																</p>
																<p class="mb-0">
																	<span data-address-state="">${salesVo.shippingStateName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																	<span data-address-country="">${salesVo.shippingCountriesName}</span>
																</p>
																<p class="mb-0">
																	<span class="">
																		<i class="la la-phone align-middle"></i> 
																		<span class="" data-address-phoneno="">
																			<c:if test="${empty salesVo.contactVo.companyMobileno}">Mobile no. is not provided</c:if>${salesVo.contactVo.companyMobileno}
																		</span>
																	</span>
																</p>
																<button type="button" class="btn btn-link" data-toggle="m-popover" data-trigger="click" 
																	title="Shipping Address <a href='#' data-popover-close='' class='m--font-bolder m-link m-link--state  m-link--danger float-right'>Cancel</a>" data-html="true" data-content="" id="shipping_address_btn">Change Address</button>
															</div>
														</div>
													</div>
												</div>
											</div>
											<%-- Jaimin <div class="col-lg-12 col-md-12 col-sm-12">
												<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
										
												<div class="form-group m-form__group row">
													<!-- <label class="col-form-label col-lg-12 col-sm-12">&nbsp;</label> -->
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="m-checkbox-inline">
															<label class="m-checkbox m-checkbox--solid m-checkbox--brand">
															<input type="checkbox" name="sez" value="${salesVo.sez}" <c:if test="${salesVo.sez==1}">checked="checked"</c:if>> Export / SEZ
															<span></span>
															</label>
														</div>
													</div>
												</div>
											</div> --%>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-7 col-md-7 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">${displayType} Date:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input todaybtn-datepicker" name="salesDate" readonly id="salesDate" onchange="changeDueDate()" data-date-format="dd/mm/yyyy"
																data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'
																value="<fmt:formatDate pattern='dd/MM/yyyy' value='${salesVo.salesDate}' />"/>
															<div class="input-group-append">
																<span class="input-group-text">
																	<i class="la la-calendar"></i>
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<%-- <c:if test="${type==Constant.SALES_INVOICE && not empty salesVo.salesVo}">
																		<input type="hidden"  name="salesVo.salesId" value="${salesVo.salesVo.salesId}"/>
																		<div class="m-widget28__tab-item">
																			<span class="m--regular-font-size-">Sales Order No.:</span>
																			<span>
																				<span class="m-badge m-badge--metal m-badge--wide">${salesVo.salesVo.prefix}${salesVo.salesVo.salesNo}</span>
																			</span>
																		</div>
																	</c:if> --%>
																	
																	<c:if test="${type==Constant.SALES_ORDER && not empty salesVo.salesVo}">
																		<input type="hidden"  name="salesVo.salesId" value="${salesVo.salesVo.salesId}"/>
																		<div class="m-widget28__tab-item">
																			<span class="m--regular-font-size-">Sales Quotation No.:</span>
																			<span>
																				<span class="m-badge m-badge--metal m-badge--wide">${salesVo.salesVo.prefix}${salesVo.salesVo.salesNo}</span>
																			</span>
																		</div>
																	</c:if>
																	
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">${displayType} No :</label>
													<div class="col-lg-6 m-form__group-sub">
														<input type="text" class="form-control m-input" name="prefix" placeholder="Prefix" value="${salesVo.prefix}">
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<input type="text" class="form-control m-input" name="salesNo" placeholder="Sales No" value="${salesVo.salesNo}">
													</div>
												</div>
											</div>
											<c:choose>
												<c:when test="${type==Constant.SALES_CREDIT_NOTE}">
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="form-group m-form__group row">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Sales:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<select class="form-control m-select2" id="salesId" name="salesVo.salesId" readonly="readonly" placeholder="Select Sales" onchange="getSalesInfo()">
																	<option value="">Select Sales</option>
																	<option value="${salesVo.salesVo.salesId}" selected="selected">${salesVo.salesVo.prefix}${salesVo.salesVo.salesNo}</option>
																</select>																	
															</div>
														</div>
													</div>
													
												</c:when>
												<c:otherwise>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="form-group m-form__group row">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Payment Term:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<div class="input-group" >
																	<select class="form-control m-select2 paymentterm-select2" id="paymentTermVo" name="paymentTermsVo.paymentTermId" placeholder="Select Payment Term" onchange="changeDueDate()" required="required">
																		<option value="">Select Payment Term</option>
																		<c:forEach items="${paymentTermVos}" var="paymentTermsVo">
																			<option value="${paymentTermsVo.paymentTermId}" name="${paymentTermsVo.paymentTermDay}"
																				<c:if test="${salesVo.paymentTermsVo.paymentTermId == paymentTermsVo.paymentTermId }" >selected="selected"</c:if>>
																				${paymentTermsVo.paymentTermName} 
																			</option>
																		</c:forEach>
																	</select>
																</div>
															</div>
														</div>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="form-group m-form__group row">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Due Date:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<div class="input-group date" >
																	<input type="text" class="form-control m-input clearbtn-datepicker" data-date-format="dd/mm/yyyy" name="dueDate" id="salesDueDate"
																				value='<c:if test="${not empty salesVo.dueDate}"><fmt:formatDate pattern="dd/MM/yyyy" value="${salesVo.dueDate}" /></c:if>'/>
																	<div class="input-group-append">
																		<span class="input-group-text">
																			<i class="la la-calendar"></i>
																		</span>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</c:otherwise>
											</c:choose>	
											
											<div class="col-lg-6 col-md-6 col-sm-12">
												
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12">
										
								<!--begin::Portlet-->
								<div class="m-portlet m-portlet--tabs m-portlet--head-solid-bg m-portlet--head-sm">
									<div class="m-portlet__head">
										<div class="m-portlet__head-tools">
											<ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_7_1" role="tab">
														Product Details
													</a>
												</li>
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_7_2" role="tab">
														Terms & Condition / Note
													</a>
												</li>
												
											</ul>
										</div>
										
									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<div class="tab-pane active" id="m_tabs_7_1" role="tabpanel">
												<div class="row">
													<c:if test="${type==Constant.SALES_INVOICE}">
														<div class="col-lg-4 col-md-4 col-sm-12">
															<div class="form-group m-form__group input-group-append">
												        		<input type="text" class="form-control m-input" id="searchBarcode" placeholder="Barcode" >
												        	</div>
														</div>
													</c:if>
													
													<div class="col-lg-12 col-md-12 col-sm-12">
														<c:if test="${type==Constant.SALES_CREDIT_NOTE}">
											        		<button type="button" data-toggle="modal" data-target="#product_list_modal" id="browse_product" class="btn btn-secondary m-btn m-btn--icon m-btn--wide"><span><i class="fa fa-sort"></i><span>Browse Product</span></span></button>
											        	</c:if>
														<div class="table-responsive m--margin-top-20">
															<table class="table m-table table-bordered table-hover" id="product_table">
															    <thead>
															      <tr>
															        <th>#</th>
															        <th>Product</th>
															        <th style="text-align: right;">Qty&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
															        <th style="text-align: right;">Price</th>
															        <th style="text-align: right;">Discount</th>
															        <th style="text-align: right;">Tax</th>
															        <th style="text-align: right;">Total</th>
															        <th></th>
															      </tr>
															    </thead>
															    <tbody data-sales-list="">
															    	<tr data-sales-item="template" class="m--hide">
															    		<td class="" style="width: 50px;" data-item-index=""></td>
																        <td class="" style="width: 200px;">
																        	<div class="form-group m-form__group p-0" id="templateProductDiv">
																				<select class="form-control" id="productId{index}" onchange="getProductInfo({index})" name="salesItemVos[{index}].productVariantVo.productVariantId" placeholder="Product">
																					<option value="">Select Product</option>
																					
																				</select>
																			</div>
																			<!-- Jaimin <div class="form-group m-form__group p-0 m--margin-top-10 ">
																				<select class="form-control" id="designNo{index}" name="salesItemVos[{index}].designNo" disabled="disabled">
																					<option value="">Select Design No.</option>
																				</select>
																			</div> -->
																        </td>
																        <td class="" style="width: 100px;">
																        	<div class="form-group m-form__group p-0">
																        		<input type="text" class="form-control m-input row_custom_input_box_td" id="qty{index}" data-decimal="2" name="salesItemVos[{index}].qty" onchange="setAmount({index})" placeholder="Qty" value="1">
																        		<span class="m--regular-font-size-sm1" data-item-uom="">pcs</span>
																        	</div>
																        </td>
																        <td class="" style="width: 180px;">
																        	<div class="form-group m-form__group p-0">
																        		<input type="text" class="form-control m-input row_custom_input_box_td" id="price{index}" name="salesItemVos[{index}].price" onchange="setAmount({index})" placeholder="Price" value="0">
																        	</div>
																        </td>
																        <td class="" style="width: 180px;">
																        	<div class="form-group m-form__group p-0">
																        		<input type="text" class="form-control m-input row_custom_input_box_td" id="discount{index}" name="salesItemVos[{index}].discount" onchange="setAmount({index})" placeholder="Discount" value="0">
																        	</div>
																        </td>
																        <td class="" style="width: 180px;">
																        	<div class="p-0 m--font-bolder" >
																        		
																        		<span class="m--font-info" data-item-tax-name=""></span>
																        		<span class="float-right">Rs. <span data-item-tax-amount="">0</span></span>
																        	</div>
																        </td> 
																         <td class="" style="width: 180px;">
																        	<div class="form-group m-form__group p-0">
																        		<input type="hidden" class="form-control m-input" readonly="readonly" id="total{index}" name="salesItemVos[{index}].total" onchange="setProductTotal({index})" placeholder="Total" value="0">
																        		<div class="p-0 text-right m--font-bolder" data-item-amount="">0</div>
																        	</div>
																        </td>
																        <td class="" style="width: 40px;">
																        	<a href="#" data-item-remove="" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-times"></i></a>
																        	<input type="hidden" id="taxVo{index}" name="salesItemVos[{index}].taxVo.taxId" value=""/>
																        	<input type="hidden" id="taxAmount{index}"  name="salesItemVos[{index}].taxAmount" value=""/>
																        	<input type="hidden" id="taxRate{index}" name="salesItemVos[{index}].taxRate"  value=""/> 
																        	<input type="hidden"  id="discountType{index}" name="salesItemVos[{index}].discountType" value="percentage"/>
																        	<input type="hidden" name="" id="haveVariant{index}" value=""/>
																        	<input type="hidden" name="" id="haveDesignno{index}" value=""/>
																        	<input type="hidden" name="" id="taxIncluded{index}" value="0"/>
																        	<input type="hidden" name="" id="noOfDecimalPlaces{index}" value="0"/>
																        	<input type="hidden" name="" id="haveBaleno{index}" value="0"/>
																        </td>
															    	</tr>
															    	
															    	<c:set var="index" scope="page" value="0"/>
															    	<c:forEach items="${salesVo.salesItemVos}" var="salesItemVos"> 
															    		<tr data-sales-item="${index}" class="${index}">
																    		<td class="" style="width: 50px;" data-item-index=""></td>
																	        <td class="" style="width: 200px;">
																	        	<div class="form-group m-form__group p-0" id="templateProductDiv">
																					<select class="form-control" id="productId${index}" onchange="getProductInfo(${index})" name="salesItemVos[${index}].productVariantVo.productVariantId" placeholder="Product"
																						<%-- Jaimin <c:if test="${type==Constant.SALES_CREDIT_NOTE}">readonly="readonly"</c:if>> --%>
																						readonly="readonly">
																						<option value="${salesItemVos.productVariantVo.productVariantId}" selected="selected">${salesItemVos.productVariantVo.productVo.name} ${salesItemVos.productVariantVo.variantName}</option>
																					</select>
																				</div>
																				<%-- Jaimin <div class="form-group m-form__group p-0 m--margin-top-10">
																					<c:choose>
																						<c:when test="${salesItemVos.productVariantVo.productVo.haveDesignno == 1}">
																							<select class="form-control" id="designNo${index}" name="salesItemVos[${index}].designNo" 
																								<c:if test="${type==Constant.SALES_CREDIT_NOTE}">readonly="readonly"</c:if>>
																								<option value="">Select Design No.</option>
																								<option value="${salesItemVos.designNo}" selected="selected">${salesItemVos.designNo}</option>
																							</select>
																						</c:when>
																						<c:otherwise>
																							<select class="form-control" id="designNo${index}" name="salesItemVos[${index}].designNo" disabled="disabled">
																								<option value="">Select Design No.</option>
																							</select>
																						</c:otherwise>
																					</c:choose>
																				</div> --%>
																	        </td>
																	        <td class="" style="width: 100px;">
																	        	<div class="form-group m-form__group p-0">
																	        		<input type="text" class="form-control m-input row_custom_input_box_td" id="qty${index}" data-decimal="${salesItemVos.productVariantVo.productVo.unitOfMeasurementVo.noOfDecimalPlaces}" name="salesItemVos[${index}].qty" onchange="setAmount(${index})" placeholder="Qty" value="${salesItemVos.qty}">
																	        		<span class="m--regular-font-size-sm1" data-item-uom="">pcs</span>
																	        	</div>
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="form-group m-form__group p-0">
																	        		<input type="text" class="form-control m-input row_custom_input_box_td" id="price${index}" name="salesItemVos[${index}].price" onchange="setAmount(${index})" placeholder="Price" value="${salesItemVos.price}">
																	        	</div>
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="form-group m-form__group p-0">
																	        		<input type="text" class="form-control m-input row_custom_input_box_td" id="discount${index}" name="salesItemVos[${index}].discount" onchange="setAmount(${index})" placeholder="Discount" value="${salesItemVos.discount}">
																	        	</div>
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 m--font-bolder" >
																	        		
																	        		<span class="m--font-info" data-item-tax-name="">
																	        		${salesItemVos.taxVo.taxName} (${salesItemVos.taxVo.taxRate} %)
																	        		</span>
																	        		<span class="float-right">Rs. <span data-item-tax-amount="">
																	        		${salesItemVos.taxAmount}
																	        		</span></span>
																	        	</div>
																	        </td> 
																	         <td class="" style="width: 180px;">
																	        	<div class="form-group m-form__group p-0">
																	        		<input type="hidden" class="form-control m-input" readonly="readonly" id="total${index}" name="salesItemVos[${index}].total" onchange="setProductTotal(${index})" placeholder="Total" value="0">
																	        		<div class="p-0 text-right m--font-bolder" data-item-amount="">0</div>
																	        	</div>
																	        </td>
																	        <td class="" style="width: 40px;">
																	        	<a href="#" data-item-remove="" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-times"></i></a>
																	        	
																	        	<input type="hidden" name="salesItemVos[${index}].salesItemId" id="salesItemId${index}" value="${salesItemVos.salesItemId}"/>
																	        	
																	        	<input type="hidden" name="" id="haveVariant${index}" value="${salesItemVos.productVariantVo.productVo.haveVariation}"/>
																	        	<input type="hidden" name="" id="haveDesignno${index}" value="${salesItemVos.productVariantVo.productVo.haveDesignno}"/>
																	        	<input type="hidden" name="" id="taxIncluded${index}" value="${salesItemVos.productVariantVo.productVo.salesTaxIncluded}"/>
																	        	<input type="hidden" name="" id="noOfDecimalPlaces${index}" value="${salesItemVos.productVariantVo.productVo.unitOfMeasurementVo.noOfDecimalPlaces}"/>
																	        	<input type="hidden" name="" id="haveBaleno{index}" value="${salesItemVos.productVariantVo.productVo.haveBaleno}"/>
																	        	
																	        	<input type="hidden" name="salesItemVos[${index}].taxAmount" id="taxAmount${index}" value="${salesItemVos.taxAmount}"/>
																	        	<input type="hidden" name="salesItemVos[${index}].taxRate" id="taxRate${index}" value="${salesItemVos.taxRate}"/>
																	        	<input type="hidden" name="salesItemVos[${index}].taxVo.taxId" id="taxVo${index}" value="${salesItemVos.taxVo.taxId}"/>
																	        	<input type="hidden" name="salesItemVos[${index}].discountType" id="discountType${index}" value="${salesItemVos.discountType}"/>
																	        	
																	        </td>
																    	</tr>
																    	<c:set var="index" scope="page" value="${index+1}"/>
															    	</c:forEach>
															    		
																</tbody>
																<tfoot>
															      <tr>
															        <th></th>
															        <th>
															        	<%-- Jaimin <c:if test="${type != Constant.SALES_CREDIT_NOTE}"> --%>
															        	<c:if test="${salesVo.status=='Pending'}">
																			<div class="m-demo-icon mb-0">
																				<div class="m-demo-icon__preview">
																					<span class=""><i class="flaticon-plus m--font-primary"></i></span>
																				</div>
																				<div class="m-demo-icon__class">
																				<a href="#" data-toggel="modal" class="m-link m--font-boldest" id="add_product"> Add More Product</a>
																				</div>
																			</div>
																		</c:if>
															        </th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th><span class="m--font-boldest float-right" id="product_sub_total">0</span></th>
															        <th></th>
															      </tr>
															    </tfoot>
														  	</table>
														</div>
														
													</div>
													
												</div>
											</div>
											<div class="tab-pane" id="m_tabs_7_2" role="tabpanel">
												<%-- Jaimin <div class="row">
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="form-group m-form__group row">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Transport:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<div class="input-group" >
																	<select class="form-control m-select2 contact-select2" id="transportId" name="contactTransportVo.contactId" data-type="${Constant.CONTACT_TRANSPORT}" placeholder="Select Transport">
																		<option value="">Select Transport</option>
																		<c:forEach items="${contactTransportVos}" var="contactVo">
																			<option value="${contactVo.contactId}"
																				<c:if test="${salesVo.contactTransportVo.contactId == contactVo.contactId}">selected="selected"</c:if>>
																				${contactVo.companyName}
																			</option>
																		</c:forEach>
																	</select>
																</div>
															</div>
														</div>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="form-group m-form__group row">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Vehicle No / LR No:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<input type="text" class="form-control m-input" name="vehicleNo" placeholder="Vehicle No / LR No" value="${salesVo.vehicleNo}">
															</div>
														</div>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="form-group m-form__group row">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Agent:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<select class="form-control m-select2 contact-select2" id="agentId" name="contactAgentVo.contactId" data-type="${Constant.CONTACT_AGENT}" placeholder="Select Agent">
																	<option value="">Select Agent</option>
																	<c:forEach items="${contactAgentVos}" var="contactVo">
																		<option value="${contactVo.contactId}"
																			<c:if test="${salesVo.contactAgentVo.contactId == contactVo.contactId}">selected="selected"</c:if>>
																			${contactVo.companyName}
																		</option>
																	</c:forEach>
																</select>
															</div>
														</div>
													</div>
												</div>
												<!-- <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div> -->
												<div class="m-divider mb-5 mt-5"><span></span></div>  --%>
												<div class="row">
													<div class="col-lg-6 col-md-6 col-sm-12">
														<c:set var="termsAndConditionIds" scope="page" value="" />
														<table class="table table-sm m-table table-striped" id="terms_and_condition_table">
														  	<thead>
														  		<tr class="row">
															      	<th scope="row" class="col-lg-1 col-md-1 col-sm-12">#</th>
														    		<th scope="row" class="col-lg-11 col-md-11 col-sm-12">
														    			<span>Terms & Condition</span>
														    			<span class="float-right">
														    				<a href="#" data-toggle="modal" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill"
																				data-target="#terms_and_condition" id="terms_condition_button" title="Edit"> <i class="fa fa-edit"></i></a>
																		</span>
														    		</th>
														    	</tr>
														  	</thead>
														  	<tbody>
																<c:set var="termsAndConditionIds" scope="page" value=""/>
																<c:forEach items="${termsAndConditionVos}" var="termsAndConditionVo" varStatus="status">
																	<c:set var="termsAndConditionIds" scope="page" value="${termsAndConditionIds}${termsAndConditionVo.termsandConditionId},"/>
																	<tr class="row" data-terms-item="">
																      	<td class="col-lg-1 col-md-1 col-sm-12" data-terms-index="">
																      		${status.index+1}
																      	</td>
																      	<td class="col-lg-11 col-md-11 col-sm-12" data-terms-name="">${termsAndConditionVo.termsCondition}</td>
															    	</tr>
																</c:forEach>
																
																<tr class="row m--hide" data-terms-item="template">
															      	<td class="col-lg-1 col-md-1 col-sm-12" data-terms-index=""></td>
															      	<td class="col-lg-11 col-md-11 col-sm-12" data-terms-name=""></td>
														    	</tr>
														  	</tbody>
														</table>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="form-group m-form__group row">
															<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Note:</label>
															<div class="col-lg-12 col-md-12 col-sm-12">
																<textarea class="form-control m-input" name="note" placeholder="Enter a note">${salesVo.note}</textarea>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									
								</div>	
								<!--end::Portlet-->
								
							</div>
							
							<div class="col-lg-12 col-md-12 col-sm-12 collapse <c:if test='${salesVo.salesAdditionalChargeVos.size() > 0}'>show</c:if>" id="additional_charge_div">
										
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Additional Charge
												</h3>
											</div>			
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
												<li class="m-portlet__nav-item">
													<button type="button" class="btn btn-link" href="#additional_charge_div" data-toggle="collapse" id="additional_charge_cancel">Cancel</button>
												</li>
											</ul>
										</div>
									</div>
									<div class="m-portlet__body">
										
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												
												<div class="table-responsive m--margin-top-20">
													<table class="table m-table table-bordered table-hover" id="additional_charge_table">
													    
													    <thead>
													      <tr>
													        <!-- <th>#</th> -->
													        <th style="width: 50px;">#</th>
													        <th style="width: 280px;">Additional Charge</th>
													        <th style="width: 180px;text-align: right;">Amount</th>
													        <th style="width: 180px;text-align: right;">Tax</th>
													        <th style="width: 180px;text-align: right;">Total</th>
													        <th style="width: 40px;text-align: right;"></th>
													      </tr>
													    </thead>
													    <tbody data-charge-list="">
													    	<c:set var="additionalChargeIndex" scope="page" value="0"/>
													    	<c:forEach items="${salesVo.salesAdditionalChargeVos}" var="salesAdditionalChargeVos">
														    	<tr data-charge-item="${additionalChargeIndex}">
														    		<td class="" style="width: 50px;" data-item-index=""></td>
															        <td class="" style="width: 280px;">
															        	<div class="form-group m-form__group p-0">
																			${salesAdditionalChargeVos.additionalChargeVo.additionalCharge}
																		</div>
															        </td>
															        <td class="" style="width: 180px;">
															        	<div class="p-0 text-right m--font-bolder">
															        		<%-- Jaimin ${salesAdditionalChargeVos.amount}
															        		<input type="hidden" id="additionalChargeAmount${additionalChargeIndex}" value="${salesAdditionalChargeVos.amount}" /> --%>
															        		<input type="text" class="form-control m-input text-right" id="additionalChargeAmount${additionalChargeIndex}" name="salesAdditionalChargeVos[${additionalChargeIndex}].amount" onchange="setAdditionalChargeAmount(${additionalChargeIndex})" placeholder="Amount" value="${salesAdditionalChargeVos.amount}">
															        	</div>
															        </td>
															        <td class="" style="width: 180px;">
															        	<div class=" m--font-bolder">
															        		<span class="m--font-info" data-item-tax-name="">${salesAdditionalChargeVos.taxVo.taxName} (${salesAdditionalChargeVos.taxRate} %)</span>
															        		<span class="float-right">Rs. <span data-item-tax-amount="">${salesAdditionalChargeVos.taxAmount}</span></span>
															        	</div>
															        </td>
															        <td class="" style="width: 180px;">
															        	<div class="text-right m--font-bolder" data-item-amount="">
															        	</div>
															        </td>
															        <td class="" style="width: 40px;">
															        	<a href="#" data-item-remove="" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-times"></i></a>
															        	<input type="hidden" name="salesAdditionalChargeVos[${additionalChargeIndex}].salesAdditionalChargeId" id="salesAdditionalChargeId${additionalChargeIndex}" value="${salesAdditionalChargeVos.salesAdditionalChargeId}"/>
															        	<input type="hidden" name="salesAdditionalChargeVos[${additionalChargeIndex}].taxAmount" id="additionalChargeTaxAmount${additionalChargeIndex}" value="${salesAdditionalChargeVos.taxAmount}"/>
															        	<input type="hidden" name="salesAdditionalChargeVos[${additionalChargeIndex}].taxRate" id="additionalChargeTaxRate${additionalChargeIndex}" value="${salesAdditionalChargeVos.taxRate}"/>
															        	<input type="hidden" name="salesAdditionalChargeVos[${additionalChargeIndex}].taxVo.taxId" id="additionalChargeTaxId${additionalChargeIndex}" value="${salesAdditionalChargeVos.taxVo.taxId}"/>
															        </td>
														    	</tr>
														    	<c:set var="additionalChargeIndex" scope="page" value="${additionalChargeIndex+1}"/>
														    </c:forEach>
													    	<tr data-charge-item="template" class="m--hide">
													    		<td class="" style="width: 50px;" data-item-index=""></td>
														        <td class="" style="width: 280px;">
														        	<div class="form-group m-form__group p-0">
																		<select class="form-control" id="additionalCharge{index}" onchange="getAdditionalChargeInfo({index})" name="salesAdditionalChargeVos[{index}].additionalChargeVo.additionalChargeId" placeholder="Additional Charge">
																			<option value="">Select Additional Charge</option>
																		</select>
																	</div>
														        </td>
														        <td class="" style="width: 180px;">
														        	<div class="form-group m-form__group p-0">
														        		<input type="text" class="form-control m-input text-right" id="additionalChargeAmount{index}" name="salesAdditionalChargeVos[{index}].amount" onchange="setAdditionalChargeAmount({index})" placeholder="Amount" value="0">
														        	</div>
														        </td>
														        <td class="" style="width: 180px;">
														        	<div class=" m--font-bolder">
														        		<span class="m--font-info" data-item-tax-name=""></span>
														        		<span class="float-right">Rs. <span data-item-tax-amount="">0</span></span>
														        	</div>
														        	
														        </td>
														        <td class="" style="width: 180px;">
														        	<div class="text-right m--font-bolder" data-item-amount="">
														        		
														        	</div>
														        </td>
														        <td class="" style="width: 40px;">
														        	<a href="#" data-item-remove="" class="btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--pill"><i class="fa fa-times"></i></a>
														        	<input type="hidden" name="salesAdditionalChargeVos[{index}].taxAmount" id="additionalChargeTaxAmount{index}" value=""/>
														        	<input type="hidden" name="salesAdditionalChargeVos[{index}].taxRate" id="additionalChargeTaxRate{index}" value=""/>
														        	<input type="hidden" name="salesAdditionalChargeVos[{index}].taxVo.taxId" id="additionalChargeTaxId{index}" value=""/>
														        </td>
													    	</tr>
														</tbody>
														<tfoot>
															<tr>
																<th></th>
																<th>
																	
																	<div class="m-demo-icon mb-0">
																		<div class="m-demo-icon__preview">
																			<span class=""><i class="flaticon-plus m--font-primary"></i></span>
																		</div>
																		<div class="m-demo-icon__class">
																		<a href="#" data-toggel="modal" class="m-link m--font-boldest" id="add_additional_charge"> Add More Additional Charge</a>
																		</div>
																	</div>
																	
																</th>
																<th></th>
																<th></th>
																<th><span class="m--font-boldest float-right" id="additional_charge_sub_total">0.0</span></th>
																<th></th>
													      	</tr>
													   </tfoot>
												  	</table>
												</div>
												
											</div>
											
										</div>
										
									</div>
									
								</div>
								<!--end::Portlet-->
								
							</div>
							
							
							<div class="col-lg-12 col-md-12 col-sm-12">
										
								<!--begin::Portlet-->
								<div class="m-portlet" style="margin-top: -2.2rem">
									
									<div class="m-portlet__foot m-portlet__foot--fit">
										
										<div class="m-form__actions m-form__actions--solid m--padding-bottom-15">
											<div class="row m--padding-top-20 m--padding-left-20 m--padding-bottom-0 m--padding-right-20">
										
												<div class="col-lg-4 col-md-4 col-sm-12 ">
													
													
													<div class="m-demo-icon">
														<div class="m-demo-icon__preview " >
															<span class=""><i class="flaticon-plus m--font-primary"></i></span>
														</div>
														<div class="m-demo-icon__class">
															<a href="#additional_charge_div" id="additional_charge_button" data-toggle="collapse" class="m-link m--font-boldest">Add Additional Charges</a>
														</div>
													</div>
													
													<table class="table table-sm m-table table-striped mb-0 m--margin-left-20 collapse" id="tax_summary_table">
													  	<thead>
													  		<tr class="row">
														      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax</th>
													    		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax Rate</th>
														      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax Amount</th>
														      	
													    	</tr>
													  	</thead>
													  	<tbody>
													    	
													    	<tr class="row m--hide" data-tax-item="template">
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
													    	</tr>
													  	</tbody>
													</table>
													
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12">
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12">
													<table class="table m-table text-right mb-0">
													  	<tbody>
													    	<tr class="row ">
														      	<th scope="row" class="col-lg-8 col-md-8 col-sm-12 m--font-info"><a href="#tax_summary_table" data-toggle="collapse" class="m-link m-link--info m-link--state">Tax Amount</a></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="tax_amount">0</h6></td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12"><h6>Total Amount</h6></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="total_amount">0</h6></td>
													    	</tr>
													    	<tr class="row"> 
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12 ">
													      			<a href="JavaScript:void(0);" data-toggle="m-popover" data-trigger="click" title="Roundoff" data-html="true" id="roundoff_edit" data-content="" class="m-link m-link--info m-link--state" >Roundoff</a>
													      		</th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="round_off">0.0</h6></td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12"><h4>Net Amount</h4></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h3 id="net_amount">0</h3></td>
													    	</tr>
													  	</tbody>
													</table>
												</div>
												
											</div>
											<!-- <a href="#" data-toggle="modal"  data-toggel="modal" data-repeater-create="" class="m-link m--font-boldest m--margin-left-30">Show Tax Details</a> -->
										</div>
									</div>
								</div>
								<!--end::Portlet-->
								
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions--solid m-form__actions--right">
								<button type="submit" class="btn btn-brand" id="savesales">
									Submit
								</button>
								
								<a href="<%=request.getContextPath()%>/sales/${type}/${salesVo.salesId}" class="btn btn-secondary">
									Cancel
								</a>
							</div>
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<div class="m-section m--hide" id="roundoff_section">
			<div class="m-section__content">
				<div class="form-group m-form__group row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<input type="text" class="form-control m-input text-right" name="roundoff" id="" placeholder="Roundoff" value="">
					</div>
				</div>
				<div class="form-group m-form__group row">
					<div class="col-lg-6 col-md-6 col-sm-6">
					<a type="button" class="btn btn-secondary" data-popover-close=''>Cancel</a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6">
					<button type="button" class="btn btn-brand float-right" id="" onclick="setRoundoff()">Set</button>
					</div>
				</div>
			</div>
		</div>
		
		<div class="m-section m--hide" id="address_template">
			<div class="m-section__content" data-address-list="">
				<div class="row" data-address-item="">
					<div class="col-lg-12 col-md-12 col-sm-12 mt-3 mb-3"><div class="m-divider"><span></span></div></div>
					<div class="col-lg-12 col-md-12 col-sm-12">
						<h4 class=""><small class="text-muted" data-address-name="">Nilesh Desai</small> <button type="button" data-change-address="" class="btn btn-outline-brand btn-sm float-right ml-5">Select</button></h4>
						<p class="mb-0">
							<span class="" data-address-line-1="">Shapath X</span>
						</p>
						<p class="mb-0">
							<span class="" data-address-line-2="">Shamal Cross Road,</span>
						</p>
						<p class="mb-0">
							<span class="" data-address-pincode="">380001</span>
							<span class="" data-address-city="">Ahmedavad,</span>
						</p>
						<p class="mb-0">
							<span class="" data-address-state="">Gujarat,</span>
							<span class="" data-address-country="">India</span>
						</p>
						<p class="mb-0">
							<span class="m--font-info"><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno="">9714866160</span></span>
						</p>
					</div>
				</div>
			</div>
		</div>
		
		<!--begin::Modal-->
		<div class="modal fade" id="terms_and_condition" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<!-- <div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div> -->
					<div class="modal-body">
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<table class="table m-table" id="term_condition_edit_table">
								  	<thead>
								  		<tr class="row">
									      	<th scope="row" class="col-lg-1 col-md-1 col-sm-12">#</th>
								    		<th scope="row" class="col-lg-11 col-md-11 col-sm-12">
								    			<span>Terms & Condition</span>
								    			<!-- <span class="float-right">
								    				<a href="#" data-toggle="modal" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill"
														data-target="#terms_and_condition" title="Edit"> <i class="fa fa-edit"></i></a>
												</span> -->
								    		</th>
								    	</tr>
								  	</thead>
								  	<tbody>
								    	<tr class="row m--hide" data-terms-item="template">
									      	<td class="col-lg-1 col-md-1 col-sm-12">
									      		<div class="m-checkbox-inline">
													<label class="m-checkbox m-checkbox--solid m-checkbox--brand">
													<input type="checkbox" name="{index}" value="0">
													<span></span>
													</label>
												</div>
									      	</td>
									      	<td class="col-lg-11 col-md-11 col-sm-12" data-terms-name=""></td>
								    	</tr>
									
								  	</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary" onclick="setTermsAndCondition()">Add</button>
					</div>
				</div>
			</div>
		</div>
		<!--end::Modal-->
		
		<!--begin::Modal-->
		<div class="modal fade" id="product_list_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<!-- <div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div> -->
					<div class="modal-body">
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<table class="table m-table m-table--head-separator-primary" id="product_credit_table">
								  	<thead>
								    	<tr>
								      		<th>#</th>
								      		<th>Product</th>
								      		<th class="text-right">Sales Qty</th>
								      		<th class="text-right">Sales Price</th>
								    	</tr>
								  	</thead>
								  	<tbody>
								    	<tr data-product-item="template" class="m--hide">
									      	<th scope="row">
									      		<div class="m-checkbox-inline">
													<label class="m-checkbox m-checkbox--solid m-checkbox--brand">
													<input type="checkbox" name="{index}" value="0">
													<span></span>
													</label>
												</div>
									      	</th>
									      	<td style="width: 280px;">
									      		<span data-product-name=""></span>
									      		<br/>
									      		<span class="m-badge m-badge--success m-badge--wide m-badge--rounded mt-1 m--hide" data-item-designno></span>
									      		<span class="m-badge m-badge--info m-badge--wide m-badge--rounded mt-1 m--hide" data-item-baleno></span>
									      		
									      	</td>
									      	<td class="text-right">
									      		<span class="m--regular-font-size-sm1" data-product-qty=""></span>
									      		<span class="m--regular-font-size-sm1" data-product-uom=""></span>
									      	</td>
									      	<td class="text-right">
									      		<span class="m--regular-font-size-sm1" data-product-price=""></span>
									      		<input type="hidden" name="" id="haveDesignnoCredit{index}" value=""/>
									        	<input type="hidden" name="" id="taxIncludedCredit{index}" value="0"/>
									        	<input type="hidden" name="" id="noOfDecimalPlacesCredit{index}" value="0"/>
									        	<input type="hidden" name="" id="haveBalenoCredit{index}" value="0"/>
									        	<input type="hidden" name="salesItemVos[{index}].productVariantVo.productVariantId" id="productVariantIdCredit{index}" value=""/>
									        	<input type="hidden" name="salesItemVos[{index}].qty" id="qtyCredit{index}" value=""/>
									        	<input type="hidden" name="salesItemVos[{index}].price" id="priceCredit{index}" value=""/>
									        	<input type="hidden" name="salesItemVos[{index}].mrp" id="mrpCredit{index}" value=""/>
									        	<input type="hidden" id="taxNameCredit{index}" value=""/>
									        	<input type="hidden" name="salesItemVos[{index}].taxAmount" id="taxAmountCredit{index}" value=""/>
									        	<input type="hidden" name="salesItemVos[{index}].taxRate" id="taxRateCredit{index}" value=""/>
									        	<input type="hidden" name="salesItemVos[{index}].taxVo.taxId" id="taxIdCredit{index}" value=""/>
									        	<input type="hidden" name="salesItemVos[{index}].discount" id="discountCredit{index}" value=""/>
									        	<input type="hidden" name="salesItemVos[{index}].discountType" id="discountTypeCredit{index}" value=""/>
									        	<input type="hidden" name="salesItemVos[{index}].designNo" id="designNoCredit{index}" value=""/>
									        	<input type="hidden" name="salesItemVos[{index}].baleNo" id="baleNoCredit{index}" value=""/>
									      	</td>
								    	</tr>
								  	</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						<button type="button" class="btn btn-primary" onclick="setProductFromModal()">Add</button>
					</div>
				</div>
			</div>
		</div>
		<!--end::Modal-->
		
		<c:if test="${type != Constant.SALES_CREDIT_NOTE}">
		<%@include file="../contact/contact-modal-new.jsp" %>
		</c:if>
		<%@include file="../paymentterm/paymentterm-modal-new.jsp" %>
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/dist/bootstrap-tagsinput.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/script/sales/sales-script.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/script/paymentterm/paymentterm-quick-script.js" type="text/javascript"></script>
	<c:if test="${type != Constant.SALES_CREDIT_NOTE}">
	<script src="<%=request.getContextPath()%>/script/contact/contact-quick-script.js" type="text/javascript"></script>
	</c:if>
	
	<%@include file="../global/location-ajax.jsp" %>
	
	<script type="text/javascript">
		setSalesNoVerifyURL("<%=request.getContextPath()%>/sales/${type}/salesno/verify");
		
		setSalesType('${type}');
		setConstantType('${Constant.SALES_INVOICE}' , '${Constant.SALES_CREDIT_NOTE}');
		setCustomerTypeConstant('${Constant.CONTACT_CATEGORY_OTHER}', '${Constant.CONTACT_CATEGORY_WHOLESALER}', '${Constant.CONTACT_CATEGORY_RETAILER}');
		
		var $salesItem=$("#product_table").find("[data-sales-item]").not(".m--hide");
		
		$salesItem.each(function (){
			var id = $(this).attr("data-sales-item");
			
			$('#sales_form').formValidation('addField',"salesItemVos["+id+"].productVariantVo.productVariantId",productValidator);
			$('#sales_form').formValidation('addField',"salesItemVos["+id+"].qty",qtyValidator);
			$('#sales_form').formValidation('addField',"salesItemVos["+id+"].price",priceValidator);
			$('#sales_form').formValidation('addField',"salesItemVos["+id+"].discount",discountValidator);
			
			/* if($("#haveDesignno"+id).val() == 1){
				$('#sales_form').formValidation('addField',"salesItemVos["+id+"].designNo",productValidator);
				$("#designNo"+id).select2();
			} */

			$("#productId"+id).addClass("m-select2 product-search");

			var URL ="";
			if(salesType == CONSTANT_CREDITNOTE){
				salesId = $("#salesId").val();
				URL ="sales/"+salesType+"/"+salesId+"/productvariant/json";
			}
			else{
				URL ="/product/variant/json";
			}
			
			/*For Remote Multiple Select2*/
			$("#productId"+id).select2({
		        placeholder: "Search Product",
		        allowClear: 0,
		        ajax: {
		            url: URL,
		            dataType: "json",
		            type: "GET",
		            delay: 250,
		            data: function(e) {
		                return {
		                    q: e.term,
		                    page: e.page
		                }
		            },
		            processResults: function(e, t) {
						return t.page = t.page || 1, {
		                    results: e.items,
		                    pagination: {
		                        more: 30 * t.page < e.total_count
		                    }
		                }
		            },
		            cache: !0
		        },
		        escapeMarkup: function(e) {
		            return e
		        },
		        minimumInputLength: 1,
		        templateResult: function(e) {
		            if (e.loading) return e.text;
		            return e.text 
		        },
		        templateSelection: function(e) {
		            return e.full_name || e.text
		        }
		    });
			/*For Remote Multiple Select2*/
				
			setAmount(id);
		});
  		setSrNo();
  		
		var $additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide");
		$additionalChargeItem.each(function (){
			var additionalChargeId = $(this).attr("data-charge-item");
			// Jaimin $('#sales_form').formValidation('addField',"salesAdditionalChargeVos["+additionalChargeId+"].additionalChargeVo.additionalChargeId", additionalChargeValidator);
			$('#sales_form').formValidation('addField',"salesAdditionalChargeVos["+additionalChargeId+"].amount", additionalChargeAmountValidator);
			setAdditionalChargeAmount(additionalChargeId);
		});
		setAdditionalChargeSrNo();

		$("#round_off").text(${salesVo.roundoff});
  		$("#net_amount").text((parseFloat($("#total_amount").text())+parseFloat(${salesVo.roundoff})).toFixed(2));
  		setIndex(${index});
		
		setAdditionalChargeIndex(${additionalChargeIndex});
		
		getAdditionalCharge();
		getContactInfo(1);
		<c:if test="${salesVo.salesAdditionalChargeVos.size() == 0}">
			$("#additional_charge_button").closest(".m-demo-icon").removeClass("m--hide");
		</c:if>
	</script>
	
</body>
<!-- end::Body -->
</html>