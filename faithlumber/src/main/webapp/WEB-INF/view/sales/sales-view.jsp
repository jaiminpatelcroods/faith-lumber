<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.croods.bssgroup.constant.Constant" %>

<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${salesVo.prefix}${salesVo.salesNo} | ${displayType} | Sales</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		/* table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		} */
		
		
		
		.card-container {
		  cursor: pointer;
		  height: 100%;
		  perspective: 600;
		  position: relative;
		  width: 55px;
		}
		.card {
		  height: 100%;
		  position: absolute;
		  transform-style: preserve-3d;
		  transition: all 1s ease-in-out;
		  width: 100%;
		}
		.card-hover {
		  transform: rotateY(180deg);
		}
		.card-hover .card-btn {
			/* display: none; */		
		}
		.card .side {
		  backface-visibility: hidden;
		  /* border-radius: 6px; */
		  height: 100%;
		  position: absolute;
		  overflow: hidden;
		  width: 100%;
		}
		.card .back {
		   background: #eaeaed !important;
		  /*color: #0087cc;
		  line-height: 150px; */
		  /* text-align: center; */
		  transform: rotateY(180deg);
		}
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${salesVo.prefix}${salesVo.salesNo}</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/sales/${type}" class="m-nav__link">
										<span class="m-nav__link-text">${displayType}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="sales_form" action="/sales/${type}/save" method="post">
						<input type="hidden" name="taxType" id="taxType" value="${salesVo.taxType}"/>
						<input type="hidden" id="state_code" value="${sessionScope.stateCode}"/>
						<div class="row">
							<div class="col-lg-5 col-md-5 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__body" >
									<!-- <div class="card-container">
											  <div class="card" id="card">
											    <div class="side"><button class="btn btn-success card-btn" type="button" id="btn_per" style="border-top-right-radius: 0;border-bottom-right-radius: 0">Go!</button></div>
											    <div class="side back"><button class="btn btn-danger" type="button">GO!</button></div>
											  </div>
											</div> -->
									
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Customer:</span>
																		<span>
																			<a href="<%=request.getContextPath() %>/contact/${salesVo.contactVo.type}/${salesVo.contactVo.contactId}" class="m-link m--font-boldest" target="_blank">
																				${salesVo.contactVo.code}
																			</a>
																		</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">GSTIN:</span>
																		<span><c:if test="${empty salesVo.contactVo.gstin}">N/A</c:if>${salesVo.contactVo.gstin}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Place of Supply:</span>
																		<span id="lblPlaceofSupply">${salesVo.shippingStateName}</span>
																	</div>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-section mt-3 m--margin-bottom-15">
													<%-- <c:if test="${contactAddress.isDeleted==0}"> --%>
													<h3 class="m-section__heading">Billing Address</h3>
													<!-- <div class="m-divider"><span></span></div> -->
													<h5 class=""><small class="text-muted m--hide" data-address-message="">Billing Address is not provided</small></h5>
													<div class="m-section__content " id="sales_billing_address">
														<input type="hidden" name="billingAddressId" id="billingAddressId" value="0"/>
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12">																
																<h5 class=""><small class="text-muted" data-address-name="">${salesVo.billingCompanyName}</small></h5>
																<p class="mb-0">
																	<span data-address-line-1="">${salesVo.billingAddressLine1}</span>
																</p>
																<p class="mb-0">
																	<span data-address-line-2="">${salesVo.billingAddressLine2}</span>
																</p>
																<p class="mb-0">
																	<span data-address-pincode="">${salesVo.billingPinCode}</span>
																	<span data-address-city="">${salesVo.billingCityName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																</p>
																<p class="mb-0">
																	<span data-address-state="">${salesVo.billingStateName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																	<span data-address-country="">${salesVo.billingCountriesName}</span>
																</p>
																<p class="mb-0">
																	<span class=""><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno=""><c:if test="${empty salesVo.contactVo.companyMobileno}">Mobile no. is not provided</c:if>${salesVo.contactVo.companyMobileno}</span></span>
																</p>
															</div>
														</div>
													</div>
													
												<%-- </c:if> --%>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-section mt-3 m--margin-bottom-15">
													<h3 class="m-section__heading">Shipping Address</h3>
													<!-- <div class="m-divider"><span></span></div> -->
													<h5 class=""><small class="text-muted m--hide" data-address-message="">Shipping Address is not provided</small></h5>
													<div class="m-section__content" id="sales_shipping_address">
														<input type="hidden" name="shippingAddressId" id="shippingAddressId" value="0"/>
														<div class="row">
															<div class="col-lg-12 col-md-12 col-sm-12">
																<h5 class=""><small class="text-muted" data-address-name="">${salesVo.shippingCompanyName}</small></h5>
																<p class="mb-0">
																	<span data-address-line-1="">${salesVo.shippingAddressLine1}</span>
																</p>
																<p class="mb-0">
																	<span data-address-line-2="">${salesVo.shippingAddressLine2}</span>
																</p>
																<p class="mb-0">
																	<span data-address-pincode="">${salesVo.shippingPinCode}</span>
																	<span data-address-city="">${salesVo.shippingCityName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																</p>
																<p class="mb-0">
																	<span data-address-state="">${salesVo.shippingStateName}</span>
																	<span class="m--font-boldest">,&nbsp;</span>
																	<span data-address-country="">${salesVo.shippingCountriesName}</span>
																</p>
																<p class="mb-0">
																	<span class=""><i class="la la-phone align-middle"></i> <span class="" data-address-phoneno=""><c:if test="${empty salesVo.contactVo.companyMobileno}">Mobile no. is not provided</c:if>${salesVo.contactVo.companyMobileno}</span></span>
																</p>
															</div>
														</div>
													</div>
												</div>
											</div>
											<%-- <div class="col-lg-12 col-md-12 col-sm-12">
												<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
												<div class="form-group m-form__group row">
													<!-- <label class="col-form-label col-lg-12 col-sm-12">&nbsp;</label> -->
													<div class="col-lg-12 col-md-12 col-sm-12">
														<span class="m--font-bolder m--regular-font-size-">Export / SEZ : </span>
														<span>
															<c:if test="${salesVo.sez==0}">No</c:if>
															<c:if test="${salesVo.sez==1}">Yes</c:if>
														</span>
													</div>
												</div>
											</div> --%>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-7 col-md-7 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<c:if test="${type==Constant.SALES_QUOTATION || type==Constant.SALES_ORDER}">
													<c:if test="${salesVo.status == 'Pending'}"><span class="m-badge m-badge--warning m-badge--wide">Pending</span></c:if>
													<c:if test="${salesVo.status == 'SO Generated'}"><span class="m-badge m-badge--primary m-badge--wide">SO Generated</span></c:if>
													<c:if test="${salesVo.status == 'In Progress'}"><span class="m-badge m-badge--info m-badge--wide">In Progress</span></c:if>
													<c:if test="${salesVo.status == 'Production Completed'}"><span class="m-badge m-badge--metal m-badge--wide">Production Completed</span></c:if>
													<c:if test="${salesVo.status == 'Completed'}"><span class="m-badge m-badge--success m-badge--wide">Completed</span></c:if>
													<%-- Jaimin <c:if test="${salesVo.status == 'Employee Assigned'}"><span class="m-badge m-badge--primary m-badge--wide">Employee Assigned</span></c:if>
													<c:if test="${salesVo.status == 'Cancel'}"><span class="m-badge m-badge--danger m-badge--wide">Cancel</span></c:if>
													<c:if test="${salesVo.status == 'Hold'}"><span class="m-badge m-badge--info m-badge--wide">Hold</span></c:if> --%>
												</c:if>
											</div>			
										</div>
										<div class="m-portlet__head-tools">
											<ul class="m-portlet__nav">
											<c:if test="${(type==Constant.SALES_QUOTATION && (salesVo.status == 'SO Generated')) || (type==Constant.SALES_ORDER) }">
											<li class="m-portlet__nav-item">
												<div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
				                                    <button class="m-dropdown__toggle btn btn-info m-btn btn-sm m-btn m-btn--icon" type="button"><i class="fa fa-send"></i>&nbsp;Send SMS</button>
				                                    <div class="m-dropdown__wrapper">
			                                            <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
			                                            <div class="m-dropdown__inner">
			                                                <div class="m-dropdown__body">              
			                                                    <div class="m-dropdown__content">
			                                                        <ul class="m-nav">
			                                                           <li class="m-nav__section m-nav__section--first">
			                                                                <span class="m-nav__section-text">Quick Actions</span>
			                                                            </li>
			                                                            <li class="m-nav__item">
			                                                                <a href="#" class="m-nav__link" id="smsbuttonforcustomer" data-target="#modal_send_msg"  data-toggle="modal">
			                                                                    <i class="m-nav__link-icon fa fa-share"></i>
			                                                                    <span class="m-nav__link-text">Send sms for Customers</span>
			                                                                </a>
				                                                        </li>
				                                                        <c:if test="${type==Constant.SALES_ORDER && salesVo.status == 'Production Completed'}">
		                                                        		<li class="m-nav__item">
			                                                                <a href="#" class="m-nav__link" id="smsbuttonfortransporter" data-target="#modal_send_msg"  data-toggle="modal">
			                                                                    <i class="m-nav__link-icon fa fa-share"></i>
			                                                                    <span class="m-nav__link-text">Send sms for Transporter</span>
			                                                                </a>
				                                                        </li>
				                                                        </c:if>
				                                                        
				                                                     </ul>
				                                                    
			                                                    </div>
			                                                </div>
			                                            </div>
				                                    </div>
				                                </div>
				                              </li>
			                                </c:if>
			                              </ul>
			                                <ul class="m-portlet__nav">
											
												<c:if test="${type==Constant.SALES_QUOTATION}">
													<li class="m-portlet__nav-item">
														<button href="#" class="btn btn-metal m-btn m-btn--icon m-btn--icon-only"
															data-skin="dark" data-toggle="m-tooltip" data-placement="top" onclick="printSalesReport()" title="Print"><i class="fa fa-print"></i></button>
													</li>
													<li class="m-portlet__nav-item">
														<a href="/sales/${type}/${salesVo.salesId}/pdf" target="_blank" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"
															data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="PDF"><i class="fa fa-file-pdf"></i></a>	
													</li>
												</c:if>
												
												<c:if test="${salesVo.status == 'Pending' || salesVo.status == 'In Progress' || salesVo.status == 'Production Completed'}">
												<li class="m-portlet__nav-item">
													<!--begin: Dropdown-->
					                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
					                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-primary m-btn m-btn--icon m-btn--icon-only">
					                                        <i class="la la-ellipsis-h"></i>
					                                    </a>
					                                    <div class="m-dropdown__wrapper">
				                                            <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
				                                            <div class="m-dropdown__inner">
				                                                <div class="m-dropdown__body">              
				                                                    <div class="m-dropdown__content">
				                                                        <ul class="m-nav">
				                                                        	
				                                                        	<li class="m-nav__section m-nav__section--first">
				                                                                <span class="m-nav__section-text">Quick Actions</span>
				                                                            </li>
				                                                        	<c:if test="${type==Constant.SALES_QUOTATION}">
				                                                        		<li class="m-nav__item">
					                                                                <a href="#" class="m-nav__link" id="sales_invoice_link" onclick="$('#sales_order_form').submit()">
					                                                                    <i class="m-nav__link-icon fa fa-share"></i>
					                                                                    <span class="m-nav__link-text">Generate Sales Order</span>
					                                                                </a>
						                                                        </li>
						                                                    </c:if>
				                                                        	<c:if test="${type==Constant.SALES_ORDER && (salesVo.status == 'Pending' || salesVo.status == 'In Progress')}">
				                                                        		<li class="m-nav__item">
					                                                                <a href="#" class="m-nav__link" id="production_link" onclick="$('#production_form').submit()">
					                                                                    <i class="m-nav__link-icon fa fa-share"></i>
					                                                                    <span class="m-nav__link-text">Generate Production</span>
					                                                                </a>
						                                                        </li>
						                                                    </c:if>   
						                                                    <c:if test="${type==Constant.SALES_ORDER && (salesVo.status == 'Pending' || salesVo.status == 'In Progress' || salesVo.status == 'Production Completed')}">    
						                                                        <li class="m-nav__item">
					                                                                <a href="#" class="m-nav__link" id="delivery_link" onclick="$('#delivery_form').submit()">
					                                                                    <i class="m-nav__link-icon fa fa-share"></i>
					                                                                    <span class="m-nav__link-text">Generate Delivery</span>
					                                                                </a>
						                                                        </li>
						                                                    </c:if>
				                                                            
				                                                            <c:if test="${salesVo.status == 'Pending'}">
				                                                            <li class="m-nav__separator m-nav__separator--fit">
				                                                            </li>
				                                                            <li class="m-nav__item">
							                                                    <a href="<%=request.getContextPath() %>/sales/${type}/${salesVo.salesId}/edit" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																					<span><i class="flaticon-edit"></i><span>Edit</span></span>
																				</a>
							                                                    <button id="sales_delete" type="button" data-url="<%=request.getContextPath() %>/sales/${type}/${salesVo.salesId}/delete" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right">
																					<span><i class="flaticon-delete-1"></i><span>Delete</span></span>
																				</button>
							                                                </li>
							                                                </c:if>
							                                            </ul>
				                                                    </div>
				                                                </div>
				                                            </div>
					                                    </div>
					                                </div>
					                                <!--end: Dropdown-->
					                            </li>
					                            </c:if>
												
												<%-- <li class="m-portlet__nav-item">
													<!--begin: Dropdown-->
					                                <div class="m-dropdown m-dropdown--inline  m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
					                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle btn btn-primary m-btn m-btn--icon m-btn--icon-only">
					                                        <i class="la la-ellipsis-h"></i>
					                                    </a>
					                                    <div class="m-dropdown__wrapper">
				                                            <span class="m-dropdown__arrow m-dropdown__arrow--left m-dropdown__arrow--adjust"></span>
				                                            <div class="m-dropdown__inner">
				                                                <div class="m-dropdown__body">              
				                                                    <div class="m-dropdown__content">
				                                                        <ul class="m-nav">
				                                                        
				                                                        	<c:if test="${type==Constant.SALES_QUOTATION}">
				                                                        		<li class="m-nav__section m-nav__section--first">
					                                                                <span class="m-nav__section-text">Quick Actions</span>
					                                                            </li>
					                                                            <c:if test="${salesVo.status != 'SO Generated'}">
					                                                        		<li class="m-nav__item">
						                                                                <a href="#" class="m-nav__link" id="sales_invoice_link" onclick="$('#sales_order_form').submit()">
						                                                                    <i class="m-nav__link-icon fa fa-share"></i>
						                                                                    <span class="m-nav__link-text">Generate Sales Order</span>
						                                                                </a>
							                                                        </li>
						                                                        </c:if>
						                                                        <c:if test="${salesVo.status == 'SO Generated'}">
					                                                        		<li class="m-nav__item">
						                                                               <span class="m-badge m-badge--primary m-badge--wide">Sales Order Generated</span>
						                                                            </li>
						                                                        </c:if>
				                                                        	</c:if>
				                                                        	
				                                                        	Jaimin <li class="m-nav__section m-nav__section--first">
				                                                                <span class="m-nav__section-text">Quick Actions</span>
				                                                            </li>
				                                                            <c:if test="${type==Constant.SALES_ORDER}">
					                                                            <c:if test="${salesVo.status== 'Pending' || salesVo.status== 'Employee Assigned'}">
						                                                        	<li class="m-nav__item">
						                                                           		<a href="#" data-toggle="modal" class="m-nav__link" onclick="getAllEmployee()">
						                                                           			<i class="m-nav__link-icon fa fa-user"></i>
						                                                           			<span class="m-nav__link-text">Assigne Employee</span>
						                                                           		</a>
						                                                           	</li>
						                                                           	<li class="m-nav__item">
						                                                                <a href="#" class="m-nav__link" id="sales_invoice_link" onclick="$('#sales_invoice_form').submit()">
						                                                                    <i class="m-nav__link-icon fa fa-share"></i>
						                                                                    <span class="m-nav__link-text">Generate Sales Invoice</span>
						                                                                </a>
						                                                            </li>
						                                                            <li class="m-nav__separator m-nav__separator--fit"></li>
						                                                            <li class="m-nav__item">
						                                                                <a href="#" class="m-nav__link" id="sales_hold_link">
						                                                                    <i class="m-nav__link-icon fa fa-pause m--font-info"></i>
						                                                                    <span class="m-nav__link-text m--font-info">Hold Order</span>
						                                                                </a>
						                                                            </li>
						                                                            <li class="m-nav__item">
						                                                                <a href="#" class="m-nav__link" id="sales_cancel_link">
						                                                                    <i class="m-nav__link-icon fa fa-times	m--font-danger"></i>
						                                                                    <span class="m-nav__link-text m--font-danger">Cancel Order</span>
						                                                                </a>
						                                                            </li>
					                                                        	</c:if>
				                                                            </c:if>
				                                                            <li class="m-nav__separator m-nav__separator--fit"></li>
				                                                            
				                                                            <c:if test="${type==Constant.SALES_QUOTATION && salesVo.status != 'SO Generated'}">
								                                                <li class="m-nav__item">
								                                                    <a href="<%=request.getContextPath() %>/sales/${type}/${salesVo.salesId}/edit" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																						<span><i class="flaticon-edit"></i><span>Edit</span></span>
																					</a>
								                                                    <button id="sales_delete" type="button" data-url="<%=request.getContextPath() %>/sales/${type}/${salesVo.salesId}/delete" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right">
																						<span><i class="flaticon-delete-1"></i><span>Delete</span></span>
																					</button>
								                                                </li>
							                                                </c:if>
							                                                <c:if test="${type==Constant.SALES_ORDER && salesVo.status == 'Pending'}">
								                                                <li class="m-nav__item">
								                                                    <a href="<%=request.getContextPath() %>/sales/${type}/${salesVo.salesId}/edit" class="btn btn-outline-info m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm">
																						<span><i class="flaticon-edit"></i><span>Edit</span></span>
																					</a>
								                                                    <button id="sales_delete" type="button" data-url="<%=request.getContextPath() %>/sales/${type}/${salesVo.salesId}/delete" class="btn btn-outline-danger m-btn m-btn--custom m-btn--icon m-btn--pill btn-sm float-right">
																						<span><i class="flaticon-delete-1"></i><span>Delete</span></span>
																					</button>
								                                                </li>
							                                                </c:if>
				                                                           
				                                                        </ul>
				                                                    </div>
				                                                </div>
				                                            </div>
					                                    </div>
					                                </div>
					                                <!--end: Dropdown-->
					                            </li> --%>
												
											</ul>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">${displayType} Date:</span>
																		<span>
																			<fmt:formatDate pattern="dd/MM/yyyy" value="${salesVo.salesDate}" />
																		</span>
																	</div>
																	<div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">${displayType} No:</span>
																		<span>${salesVo.prefix}${salesVo.salesNo}</span>
																	</div>
																	<%-- Jaimin <div class="m-widget28__tab-item">
																		<span class="m--regular-font-size-">Export / SEZ :</span>
																		<span>
																			<c:if test="${salesVo.sez==0}">No</c:if>
																			<c:if test="${salesVo.sez==1}">Yes</c:if>
																		</span>
																	</div> --%>
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="m-widget28">
													<div class="m-widget28__container" >	
														<!-- Start:: Content -->
														<div class="m-widget28__tab tab-content">
															<div class="m-widget28__tab-container tab-pane active">
															    <div class="m-widget28__tab-items">
																	<c:if test="${type==Constant.SALES_INVOICE || type==Constant.SALES_ORDER}">
																		<div class="m-widget28__tab-item">
																			<span class="m--regular-font-size-">Payment Term:</span>
																			<span>
																				<c:if test="${empty salesVo.paymentTermsVo}">N/A</c:if>
																				<c:if test="${not empty salesVo.paymentTermsVo}">${salesVo.paymentTermsVo.paymentTermName}</c:if>
																			</span>
																		</div>
																		<div class="m-widget28__tab-item">
																			<span class="m--regular-font-size-">Due Date:</span>
																			<span>
																				<c:if test="${empty salesVo.dueDate}">N/A</c:if>
																				<c:if test="${not empty salesVo.dueDate}"><fmt:formatDate pattern="dd/MM/yyyy" value="${salesVo.dueDate}" /></c:if>
																			</span>
																		</div>
																	</c:if>
																	<c:if test="${type==Constant.SALES_ORDER && not empty salesVo.assignedEmployee}">
																		<div class="m-widget28__tab-item">
																			<span class="m--regular-font-size-">Assigned Employee:</span>
																			<span>${salesVo.assignedEmployee.employeeName}</span>
																		</div>
																	</c:if>
																	<c:if test="${type==Constant.SALES_CREDIT_NOTE}">
																		<div class="m-widget28__tab-item">
																			<span class="m--regular-font-size-">Sales No.:</span>
																			<span>
																				<span class="m-badge m-badge--metal m-badge--wide">${salesVo.salesVo.prefix}${salesVo.salesVo.salesNo}</span>
																			</span>
																		</div>
																		<div class="m-widget28__tab-item">
																			
																		</div>
																	</c:if>
																	<c:if test="${type==Constant.SALES_INVOICE && not empty salesVo.salesVo}">
																		<div class="m-widget28__tab-item">
																			<span class="m--regular-font-size-">Sales Order No.:</span>
																			<span>
																				<span class="m-badge m-badge--metal m-badge--wide">${salesVo.salesVo.prefix}${salesVo.salesVo.salesNo}</span>
																			</span>
																		</div>
																	</c:if>
																	
																	<c:if test="${not empty salesVo.salesVo && type==Constant.SALES_ORDER}">
																		<div class="m-widget28__tab-item">
																			<span class="m--regular-font-size-">From Sales Quotation No.:</span>
																			<span>
																				${salesVo.salesVo.prefix}${salesVo.salesVo.salesNo}
																			</span>
																		</div>
																	</c:if>
																	
																</div>					      	 		      	
															</div>
														</div>
														<!-- end:: Content --> 	
													</div>				 	 
												</div>
											</div>
										</div>
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions m-form__actions--solid p-0">
											<div class="row">
												<c:if test='${type == Constant.SALES_INVOICE}'>
													<div class="col-lg-4 col-md-4 col-sm-12">
														<div class="m-widget4">
															<div class="m-widget4__item">
																<div class="m-widget4__info">
																	<span class="m-widget4__title">
																		Due Amount
																	</span><br/>
																	<span class="m-widget4__ext">
																		<span class="m-widget4__number m--font-danger">
																			${salesVo.total - salesVo.paidAmount}
																		</span>
																	</span>
																</div>
																
															</div>
														</div>
													</div>
												
													<div class="col-lg-4 col-md-4 col-sm-12">
														<div class="m-widget4">
															<div class="m-widget4__item">
																<div class="m-widget4__info">
																	<span class="m-widget4__title">
																		Paid Amount
																	</span>
																	<br>
																	<span class="m-widget4__ext">
																		<span class="m-widget4__number m--font-success">
																			${salesVo.paidAmount}
																		</span>
																	</span>
																</div>
																
															</div>
														</div>
													</div>
												</c:if>
												<div class="col-lg-4 col-md-4 col-sm-12">
													<div class="m-widget4">
														<div class="m-widget4__item">
															<div class="m-widget4__info">
																<span class="m-widget4__title">
																	Total Amount
																</span>
																<br>
																<span class="m-widget4__ext">
																	<span class="m-widget4__number m--font-info">
																		${salesVo.total}
																	</span>
																</span>
															</div>
															
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12">
										
								<!--begin::Portlet-->
								<div class="m-portlet m-portlet--tabs m-portlet--head-solid-bg m-portlet--head-sm">
									<div class="m-portlet__head">
										<div class="m-portlet__head-tools">
											<ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_7_1" role="tab">
														Product Details
													</a>
												</li>
												<li class="nav-item m-tabs__item">
													<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_7_2" role="tab">
														Terms & Condition / Note
													</a>
												</li>
												
												<c:if test="${salesVo.status != 'Pending'}">
													<li class="nav-item m-tabs__item">
														<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_7_3" role="tab">
															Production Details
														</a>
													</li>
													<li class="nav-item m-tabs__item">
														<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_7_4" role="tab">
															Delivery Details
														</a>
													</li>
													<li class="nav-item m-tabs__item">
														<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_7_5" role="tab">
															Process Details
														</a>
													</li>
												</c:if>
												
											</ul>
										</div>
										
									</div>
									<div class="m-portlet__body">
										<div class="tab-content">
											<div class="tab-pane active" id="m_tabs_7_1" role="tabpanel">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														
														<div class="table-responsive m--margin-top-20">
															<table class="table m-table table-bordered table-hover" id="product_table">
															    
															    <thead>
															      <tr>
															        <!-- <th>#</th> -->
															        <th>#</th>
															        <th>Product</th>
															        <th style="text-align: right;">Qty</th>
															        <th style="text-align: right;">Price</th>
															        <th style="text-align: right;">Discount</th>
															        <th style="text-align: right;">Tax</th>
															        <th style="text-align: right;">Total</th>
															        <th></th>
															        
															      </tr>
															    </thead>
															    <tbody data-sales-list="">
															    	<c:set var="index" scope="page" value="0"/>
															    	<c:forEach items="${salesVo.salesItemVos}" var="salesItemVos">
															    		<tr data-sales-item="${index}">
																    		<td class="" style="width: 50px;">
																    			<span data-item-index></span>
																    		</td>
																	        <td class="" style="width: 280px;">
																	        	<a href="#sales-item-variant${index}" data-sales-item-toggle="" data-item-name aria-expanded="true" data-toggle="collapse" class="m-link m--font-bolder collapsed">
																					<%-- Jaimin ${salesItemVos.productVariantVo.productVo.categoryVo.categoryName} --%>
																					${salesItemVos.productVariantVo.productVo.name}
																					${salesItemVos.productVariantVo.variantName}
																				</a>
																				
																				<br/>
																				<span class="m--font-bold" data-item-description="">${salesItemVos.productDescription}</span>
																				<br/>
																				
																				<%-- Jaimin <c:if test="${salesItemVos.productVariantVo.productVo.haveDesignno == 1}">
																					<span class="m-badge m-badge--success m-badge--wide m-badge--rounded mt-1" data-item-designno>${salesItemVos.designNo}</span>
																				</c:if> --%>
																				
																				
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 text-right m--font-bolder">
																	        		<span class="m--regular-font-size-sm1" data-item-qty="">${salesItemVos.qty}</span>
																	        		<span class="m--regular-font-size-sm1" data-item-uom="">${salesItemVos.productVariantVo.productVo.unitOfMeasurementVo.measurementCode}</span>
																	        	</div>
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 text-right m--font-bolder" data-item-price="">${salesItemVos.price}</div>
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 text-right m--font-bolder" data-item-discount="">${salesItemVos.discount}</div>
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 m--font-bolder" >
																	        		
																	        		<span class="m--font-info" data-item-tax-name="">
																	        			${salesItemVos.taxVo.taxName} (${salesItemVos.taxVo.taxRate} %)
																	        		</span>
																	        		<span class="float-right">Rs. <span data-item-tax-amount="">${salesItemVos.taxAmount}</span></span>
																	        	</div>
																	        	
																	        </td>
																	        <td class="" style="width: 180px;">
																	        	<div class="p-0 text-right m--font-bolder" data-item-amount="">0</div>
																	        </td>
																	        <td class="" style="width: 40px;">
																	        	<input type="hidden" name="" id="haveVariant${index}" value="${salesItemVos.productVariantVo.productVo.haveVariation}"/>
																	        	<input type="hidden" name="" id="haveDesignno${index}" value="${salesItemVos.productVariantVo.productVo.haveDesignno}"/>
																	        	<input type="hidden" name="" id="taxIncluded${index}" value="${salesItemVos.productVariantVo.productVo.salesTaxIncluded}"/>
																	        	<input type="hidden" name="" id="noOfDecimalPlaces${index}" value="${salesItemVos.productVariantVo.productVo.unitOfMeasurementVo.noOfDecimalPlaces}"/>
																	        	<input type="hidden" name="" id="haveBaleno{index}" value="${salesItemVos.productVariantVo.productVo.haveBaleno}"/>
																	        	<input type="hidden" name="salesItemVos[${index}].product.productId" id="productId${index}" value="${salesItemVos.productVariantVo.productVo.productId}"/>
																	        	<input type="hidden" name="salesItemVos[${index}].productVariantVo.productVariantId" id="productVariantId${index}" value="${salesItemVos.productVariantVo.productVariantId}"/>
																	        	<input type="hidden" name="salesItemVos[${index}].qty" id="qty${index}" value="${salesItemVos.qty}"/>
																	        	<input type="hidden" name="salesItemVos[${index}].price" id="price${index}" value="${salesItemVos.price}"/>
																	        	<input type="hidden" name="salesItemVos[${index}].productDescription" id="productDescription${index}" value="${salesItemVos.productDescription}"/>
																	        	<input type="hidden" name="salesItemVos[${index}].taxAmount" id="taxAmount${index}" value="${salesItemVos.taxAmount}"/>
																	        	<input type="hidden" name="salesItemVos[${index}].taxRate" id="taxRate${index}" value="${salesItemVos.taxRate}"/>
																	        	<input type="hidden" name="salesItemVos[${index}].taxVo.taxId" id="taxVo${index}" value="${salesItemVos.taxVo.taxId}"/>
																	        	<input type="hidden" name="salesItemVos[${index}].discount" id="discount${index}" value="${salesItemVos.discount}"/>
																	        	<input type="hidden" name="salesItemVos[${index}].discountType" id="discountType${index}" value="${salesItemVos.discountType}"/>
																	        	<input type="hidden" name="salesItemVos[${index}].designNo" id="designNo${index}" value="${salesItemVos.designNo}"/>
																        		<input type="hidden" name="salesItemVos[${index}].baleNo" id="baleNo${index}" value="${salesItemVos.baleNo}"/>
																	        </td>
																    	</tr>
																    	<c:set var="index" scope="page" value="${index+1}"/>
																    </c:forEach>
																</tbody>
																<tfoot>
															      <tr>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th></th>
															        <th><span class="m--font-boldest float-right" id="product_sub_total">0</span></th>
															        <th></th>
															      </tr>
															    </tfoot>
														  	</table>
														</div>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="m_tabs_7_2" role="tabpanel">
												<%-- Jaimin <div class="row">
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="m-widget28">
															<div class="m-widget28__container" >	
																<!-- Start:: Content -->
																<div class="m-widget28__tab tab-content">
																	<div class="m-widget28__tab-container tab-pane active">
																	    <div class="m-widget28__tab-items">
																			<div class="m-widget28__tab-item">
																				<span class="m--regular-font-size-">Transport:</span>
																				<span>
																					<c:if test="${empty salesVo.contactTransportVo}">N/A</c:if>
																					<c:if test="${not empty salesVo.contactTransportVo}">${salesVo.contactTransportVo.companyName}</c:if>
																				</span>
																			</div>
																		</div>					      	 		      	
																	</div>
																</div>
																<!-- end:: Content --> 	
															</div>				 	 
														</div>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="m-widget28">
															<div class="m-widget28__container" >	
																<!-- Start:: Content -->
																<div class="m-widget28__tab tab-content">
																	<div class="m-widget28__tab-container tab-pane active">
																	    <div class="m-widget28__tab-items">
																			<div class="m-widget28__tab-item">
																				<span class="m--regular-font-size-">Vehicle No / LR No:</span>
																				<span>
																					<c:if test="${empty salesVo.vehicleNo}">N/A</c:if>
																					<c:if test="${not empty salesVo.vehicleNo}">${salesVo.vehicleNo}</c:if>
																				</span>
																			</div>
																		</div>					      	 		      	
																	</div>
																</div>
																<!-- end:: Content --> 	
															</div>				 	 
														</div>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<div class="m-widget28">
															<div class="m-widget28__container" >	
																<!-- Start:: Content -->
																<div class="m-widget28__tab tab-content">
																	<div class="m-widget28__tab-container tab-pane active">
																	    <div class="m-widget28__tab-items">
																			<div class="m-widget28__tab-item">
																				<span class="m--regular-font-size-">Agent:</span>
																				<span>
																					<c:if test="${empty salesVo.contactAgentVo}">N/A</c:if>
																					<c:if test="${not empty salesVo.contactAgentVo}">${salesVo.contactAgentVo.companyName}</c:if>
																				</span>
																			</div>
																		</div>					      	 		      	
																	</div>
																</div>
																<!-- end:: Content --> 	
															</div>				 	 
														</div>
													</div> 
												</div>
												<!-- <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div> -->
												<div class="m-divider mb-4 mt-4"><span></span></div> --%>
												
												<div class="row">
													<div class="col-lg-6 col-md-6 col-sm-12">
														<c:set var="termsAndConditionIds" scope="page" value="" />
														<table class="table table-sm m-table table-striped" id="terms_and_condition_table">
														  	<thead>
														  		<tr class="row">
															      	<th scope="row" class="col-lg-1 col-md-1 col-sm-12">#</th>
														    		<th scope="row" class="col-lg-11 col-md-11 col-sm-12">
														    			<span>Terms & Condition</span>
														    			<span class="float-right">
														    				<!-- <a href="#" data-toggle="modal" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill"
																				data-target="#terms_and_condition" id="terms_condition_button" title="Edit"> <i class="fa fa-edit"></i></a> -->
																		</span>
														    		</th>
														    	</tr>
														  	</thead>
														  	<tbody>
																<c:set var="termsAndConditionIds" scope="page" value=""/>
																<c:forEach items="${termsAndConditionVos}" var="termsAndConditionVo" varStatus="status">
																	<tr class="row" data-terms-item="">
																      	<td class="col-lg-1 col-md-1 col-sm-12" data-terms-index="">
																      		${status.index+1}
																      	</td>
																      	<td class="col-lg-11 col-md-11 col-sm-12" data-terms-name="">${termsAndConditionVo.termsCondition}</td>
															    	</tr>
																</c:forEach>
														  	</tbody>
														</table>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-12">
														<table class="table table-sm m-table  m-table--head-no-border">
														  	<thead>
														  		<tr class="row">
														    		<th scope="row" class="col-lg-12 col-md-12 col-sm-12">Note:</th>
														    	</tr>
														  	</thead>
														  	<tbody>
														    	<tr class="row">
															      	<td class="col-lg-12 col-md-12 col-sm-12">
															      		<span><c:if test="${empty salesVo.note}">No Note.</c:if>${salesVo.note}</span>
															      	</td>
														    	</tr>
														  	</tbody>
														</table>
													</div>
												</div>
											</div>
											
											<div class="tab-pane" id="m_tabs_7_3" role="tabpanel">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														<table class="table table-sm m-table table-striped" id="production_detail_table">	
														 <thead>
															<tr>
																<th>#</th>
																<th>Production No.</th>
																<th>Production Date</th>
															</tr>
														</thead>
														</table>
													</div>
												</div>
											</div>
											<div class="tab-pane" id="m_tabs_7_4" role="tabpanel">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														<table class="table table-sm m-table table-striped" id="delivery_detail_table">	
														<thead>
															<tr>
																<th>#</th>
																<th>Delivery No.</th>
																<th>Delivery Date</th>
															</tr>
														</thead> 
														</table>
													</div>
												</div>
											</div>
											
											<div class="tab-pane" id="m_tabs_7_5" role="tabpanel">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12">
														
														<table class="table" id="sales_item_detail_table">	
															<thead>
																<tr>
																	<th>#</th>
																	<th>Product</th>
																	<th>Order Qty</th>
																	<th>Produced Qty</th>
																	<th>Production Remains Qty</th>
																	<th>Delivered Qty</th>
																	<th>Delivery Remains Qty</th>
																</tr>
															</thead>
															<tbody>
																<c:set var="productIndex" scope="page" value="0"/>
																<c:forEach items="${salesVo.salesItemVos}" var="salesItemVo" varStatus="statusp">
																	<tr>
																      	<td>
																      		${statusp.index+1}
																      	</td>
																      	<td >
																      		${salesItemVo.productVariantVo.productVo.name}
																			${salesItemVo.productVariantVo.variantName}
																		</td>
																		<td>
																      		${salesItemVo.qty}
																		</td>
																		<td>
																      		${salesItemVo.producedQty}
																		</td>
																		<td>
																      		${salesItemVo.qty-salesItemVo.producedQty}
																		</td>
																		<td>
																      		${salesItemVo.deliveredQty}
																		</td>
																		<td>
																      		${salesItemVo.qty-salesItemVo.deliveredQty}
																		</td>
															    	</tr>
																</c:forEach>
															</tbody> 
														</table>
														
													</div>
												</div>
											</div>
											
										</div>
									</div>
									
								</div>
								<!--end::Portlet-->
								
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12" id="additional_charge_div">
										
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Additional Charge
												</h3>
											</div>			
										</div>
									</div>
									<div class="m-portlet__body">
										
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												
												<div class="table-responsive m--margin-top-20">
													<table class="table m-table table-bordered table-hover" id="additional_charge_table">
													    
													    <thead>
													      <tr>
													        <!-- <th>#</th> -->
													        <th style="width: 50px;">#</th>
													        <th style="width: 280px;">Additional Charge</th>
													        <th style="width: 180px;text-align: right;">Amount</th>
													        <th style="width: 180px;text-align: right;">Tax</th>
													        <th style="width: 180px;text-align: right;">Total</th>
													        <th style="width: 40px;"></th>
													      </tr>
													    </thead>
													    <tbody data-charge-list="">
													    	<c:if test='${salesVo.salesAdditionalChargeVos.size() == 0}'>
														    	<tr>
														    		<td colspan="6" class="text-center m--font-bolder">No Additional Charges</td>
														    	</tr>
														    </c:if>
													    	<c:set var="additionalChargeIndex" scope="page" value="0"/>
													    	<c:forEach items="${salesVo.salesAdditionalChargeVos}" var="salesAdditionalChargeVos">
														    	<tr data-charge-item="${additionalChargeIndex}">
														    		<td class="" style="width: 50px;" data-item-index=""></td>
															        <td class="" style="width: 280px;">
															        	<div class="form-group m-form__group p-0">
																			${salesAdditionalChargeVos.additionalChargeVo.additionalCharge}
																		</div>
															        </td>
															        <td class="" style="width: 180px;">
															        	<div class="p-0 text-right m--font-bolder">
															        		${salesAdditionalChargeVos.amount}
															        		<input type="hidden" id="additionalChargeAmount${additionalChargeIndex}" value="${salesAdditionalChargeVos.amount}" />
															        	</div>
															        </td>
															        <td class="" style="width: 180px;">
															        	<div class=" m--font-bolder">
															        		<span class="m--font-info" data-item-tax-name="">${salesAdditionalChargeVos.taxVo.taxName} (${salesAdditionalChargeVos.taxRate} %)</span>
															        		<span class="float-right">Rs. <span data-item-tax-amount="">${salesAdditionalChargeVos.taxAmount}</span></span>
															        	</div>
															        </td>
															        <td class="" style="width: 180px;">
															        	<div class="text-right m--font-bolder" data-item-amount="">
															        	</div>
															        </td>
															        <td class="" style="width: 40px;">
															        	<input type="hidden" name="salesAdditionalChargeVos[${additionalChargeIndex}].taxAmount" id="additionalChargeTaxAmount${additionalChargeIndex}" value="${salesAdditionalChargeVos.taxAmount}"/>
															        	<input type="hidden" name="salesAdditionalChargeVos[${additionalChargeIndex}].taxRate" id="additionalChargeTaxRate${additionalChargeIndex}" value="${salesAdditionalChargeVos.taxRate}"/>
															        	<input type="hidden" name="salesAdditionalChargeVos[${additionalChargeIndex}].taxVo.taxId" id="additionalChargeTaxId${additionalChargeIndex}" value="${salesAdditionalChargeVos.taxVo.taxId}"/>
															        </td>
														    	</tr>
														    	<c:set var="additionalChargeIndex" scope="page" value="${additionalChargeIndex+1}"/>
														    </c:forEach>
														</tbody>
														<tfoot>
															<tr>
																<th></th>
																<th></th>
																<th></th>
																<th></th>
																<th><span class="m--font-boldest float-right" id="additional_charge_sub_total">0.0</span></th>
																<th></th>
													      	</tr>
													   </tfoot>
												  	</table>
												</div>
												
											</div>
											
										</div>
										
									</div>
									
								</div>
								<!--end::Portlet-->
								
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12">
										
								<!--begin::Portlet-->
								<div class="m-portlet" style="margin-top: -2.2rem">
									
									<div class="m-portlet__foot m-portlet__foot--fit">
										
										<div class="m-form__actions m-form__actions--solid m--padding-bottom-15">
											<div class="row m--padding-top-20 m--padding-left-20 m--padding-bottom-0 m--padding-right-20">
										
												<div class="col-lg-4 col-md-4 col-sm-12 ">
													
													<table class="table table-sm m-table table-striped mb-0 m--margin-left-20 collapse" id="tax_summary_table">
													  	<thead>
													  		<tr class="row">
														      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax</th>
													    		<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax Rate</th>
														      	<th scope="row" class="col-lg-4 col-md-4 col-sm-12">Tax Amount</th>
														      	
													    	</tr>
													  	</thead>
													  	<tbody>
													    	
													    	<tr class="row m--hide" data-tax-item="template">
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
														      	<td class="col-lg-4 col-md-4 col-sm-12"></td>
													    	</tr>
													  	</tbody>
													</table>
													
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12">
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12">
													<table class="table m-table text-right mb-0">
													  	<tbody>
													    	<tr class="row ">
														      	<th scope="row" class="col-lg-8 col-md-8 col-sm-12 m--font-info"><a href="#tax_summary_table" data-toggle="collapse" class="m-link m-link--info m-link--state">Tax Amount</a></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="tax_amount">0</h6></td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12"><h6>Total Amount</h6></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="total_amount">0</h6></td>
													    	</tr>
													    	<tr class="row"> 
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12 ">
													      			<h6>Roundoff</h6>
													      		</th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h6 id="round_off">0.0</h6></td>
													    	</tr>
													    	<tr class="row">
													      		<th scope="row" class="col-lg-8 col-md-8 col-sm-12"><h4>Net Amount</h4></th>
														      	<td class="col-lg-4 col-md-4 col-sm-12"><h3 id="net_amount">0</h3></td>
													    	</tr>
													  	</tbody>
													</table>
												</div>
												
											</div>
											<!-- <a href="#" data-toggle="modal"  data-toggel="modal" data-repeater-create="" class="m-link m--font-boldest m--margin-left-30">Show Tax Details</a> -->
										</div>
									</div>
								</div>
								<!--end::Portlet-->
								
							</div>
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		<div class="m--hide" id="targetDiv"></div>
		<form action="<%=request.getContextPath()%>/sales/${Constant.SALES_ORDER}/new" method="post" id="sales_order_form">
			<input type="hidden" name="parentId" value="${salesVo.salesId}"> 
		</form>
		<form action="<%=request.getContextPath()%>/${Constant.PRODUCTION}/new" method="post" id="production_form">
			<input type="hidden" name="parentId" value="${salesVo.salesId}"> 
		</form>
		<form action="<%=request.getContextPath()%>/${Constant.DELIVERY}/new" method="post" id="delivery_form">
			<input type="hidden" name="parentId" value="${salesVo.salesId}"> 
		</form>
		
		<form action="<%=request.getContextPath()%>/sales/${Constant.SALES_INVOICE}/new" method="post" id="sales_invoice_form">
			<input type="hidden" name="parentId" value="${salesVo.salesId}"> 
		</form>
		<div class="modal fade" id="assigned_employee_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							 Assigned Employee
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
					</div>
					<form id="assigned_employee_form" action="/sales/${type}/${salesVo.salesId}/assigned/employee" method="post">
						<div class="modal-body">
							<div class="form-group m-form__group row">
								<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Employee:</label>
								<div class="col-lg-12 col-md-12 col-sm-12">
									<div class="input-group" >
										<select class="form-control m-select2" id="employeeId" name="assignedEmployee" placeholder="Select Employee">
											<option value="">Select Employee</option>
											<c:forEach items="${employeeVos}" var="employeeVo">
												<option value="${employeeVo.employeeId}">${employeeVo.employeeName}</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							<button type="submit"id="saveassignedemployee" class="btn btn-primary">Save</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!---------END OF ASSIGNED NEW MODAL--------------->
		<div class="modal fade" id="modal_send_msg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog " role="document">
				<input type="hidden" name="contactVo.companyMobileno" id="phoneno" value=""/>
			
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							Send SMS
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
					</div>
					<div class="modal-body">
						<p class="col-12 " id="errorMessageCategory" style="color: red;"></p>
						<div class="form-group m-form__group row">
	            			<label class="col-12 col-form-label">Message <span class="required">*</span></label>
	              			<div class="col-12 ">
	               				<textarea name="message" id="offer_message" onkeyup="" class="form-control m-input" maxlength="500" rows="4" placeholder="Message" ></textarea>
	               					<div id="charNum"></div>
	               			</div>
	             		</div>
	             		<c:if test="${type==Constant.SALES_ORDER && salesVo.status == 'Production Completed'}">
		             	<div class="row" id="transportdiv">
							<div class="col-md-12 m-scrollable mCustomScrollbar _mCS_2 mCS-autoHide" data-scrollable="true" data-max-height="300" style="max-height: 300px; height: 300px; overflow: visible;">
								<p id="errormessageSerialnoEdit" style="color: red;"></p>
								
								<table id="invListTable" class="table table-bordered table-hover">
									<thead>
										<tr style="height: 10px;">
											<th data-field="state" class="btSelectItem"><input class="checkbox" type="checkbox" id="select_all_3"></th>
											<th data-field="Tid" class="">Transporters</th>
										</tr>
									</thead>
									<tbody>
												
									</tbody>		
								</table>
								
							</div>
						</div>
						</c:if>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="send_sms_btn" <c:if test="${salesVo.status == 'In Progress' || salesVo.status == 'Production Completed'}"> onclick="sendSMS()" </c:if> >Send</button>
		          		    	<button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/datatable/jquery.spring-friendly.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/script/sales/sales-script.js" type="text/javascript"></script>
	
	
	<script type="text/javascript">
		$(document).ready(function () {
		
			var $salesItem=$("#product_table").find("[data-sales-item]").not(".m--hide");
			
			$salesItem.each(function (){
				setAmount($(this).attr("data-sales-item"));
			});
			
	  		setSrNo();
	  		
			var $additionalChargeItem=$("#additional_charge_table").find("[data-charge-item]").not(".m--hide"), subTotal=0.0;
			
			$additionalChargeItem.each(function (){
				setAdditionalChargeAmount($(this).attr("data-charge-item"));
			});
			
			setAdditionalChargeSrNo();
			
			$("#round_off").text(${salesVo.roundoff});
	  		$("#net_amount").text((parseFloat($("#total_amount").text())+parseFloat(${salesVo.roundoff})).toFixed(2));

	  		$(".m-tabs__link").click(function(){
				if($(this).attr('href') == "#m_tabs_7_3") {
					setProductionDatable();
				} else if($(this).attr('href') == "#m_tabs_7_4") {
					setDeliveryDatable();
				} else if($(this).attr('href') == "#m_tabs_7_5") {
					
				} 
				
			});
	  		
		});
		
		function printSalesReport(){
			
			var iframe = $('<iframe>');
            iframe.attr('id', 'iFramePrint');
            iframe.attr('class', 'm--hide');
            iframe.attr('src', '<%=request.getContextPath()%>/sales/${type}/${salesVo.salesId}/pdf');
			
            $('#targetDiv').html(iframe);
            if($("#targetDiv").html() != '') {
				document.getElementById("iFramePrint").focus();
				document.getElementById("iFramePrint").contentWindow.print();
			}
			
		}
		
		$(document).ready(function(){
			
			$('#assigned_employee_form').formValidation({
				framework: 'bootstrap',
				excluded: ":disabled",
				live:'disabled', 
				button: {
					selector: "#saveassignedemployee",
					disabled: "disabled"
				},
				icon : null,
				fields : {
					assignedEmployee : {
							validators : {
								notEmpty : {
								message : 'please select Employee. '
							}
						}
					},
				}
			});
			
			$('#assigned_employee_modal').on('shown.bs.modal', function() {
				$('#assignedemployee_form').formValidation('resetForm', true);
				$("#employeeId").select2({dropdownParent: $("#employeeId").parent()});
			});
			
			$("#saveassignedemployee").click(function() {
				$('#assigned_employee_form').data('formValidation').validate();
			});

			$("#sales_hold_link").click(function() {
				swal({
			        title: "You sure want to hold Order ?",
			        text: "",
			        type: "warning",
			        confirmButtonText: "<span><i class='la la-thumbs-up'></i><span>Yes</span></span>",
			        confirmButtonClass: "btn btn-info m-btn m-btn--pill m-btn--air m-btn--icon bg-info",
			        showCancelButton: !0,
			        cancelButtonText: "<span><i class='la la-thumbs-down'></i><span>No</span></span>",
			        cancelButtonClass: "btn btn-secondary m-btn m-btn--pill m-btn--icon"
			    }).then(function(e) {
	                
	                if(e.value) {
	                	$.post('/sales/${type}/${salesVo.salesId}/update/status',{
	                		status:"Hold"
	                	},function(data, status){		      
	                		location.reload(true);
            		    });
	                }
			    	
	            })
			});
			
			$("#sales_cancel_link").click(function() {
				swal({
			        title: "You sure want to cancel Order ?",
			        text: "",
			        type: "warning",
			        confirmButtonText: "<span><i class='la la-thumbs-up'></i><span>Yes</span></span>",
			        confirmButtonClass: "btn btn-danger m-btn m-btn--pill m-btn--air m-btn--icon bg-danger",
			        showCancelButton: !0,
			        cancelButtonText: "<span><i class='la la-thumbs-down'></i><span>No</span></span>",
			        cancelButtonClass: "btn btn-secondary m-btn m-btn--pill m-btn--icon"
			    }).then(function(e) {
	                
	                if(e.value) {
	                	if(e.value) {
		                	alert("f");
	                		$.post('/sales/${type}/${salesVo.salesId}/update/status',{
		                		status:"Cancel"
		                	},function(data, status){
		                		location.reload(true);
	            		    });
		                }
	                }
			    	
	            })
			});
		});
		
		function getAllEmployee(){		
			mApp.blockPage({overlayColor:"chocolate",type:"loader",state:"danger",message:"Loading Employee"});
			$.post('/employee/json',		   
			    function(data, status){		      
			        $('#employeeId').empty();
			        
			        $("#employeeId").append($('<option>',{value :'', text :"Select Employee"}));
			        $.each( data, function( key, value) {				        	
		  				$("#employeeId").append($('<option>',{value : value.employeeId, text :value.employeeName,}));
		  				
		  			});
		  			
		  			if($('#employeeId').data("default") != undefined)
		  				$('#employeeId').val($('#employeeId').data("default")).trigger('change') ;
		  			
		  			$("#assigned_employee_modal").modal("show");
		  			
		  			mApp.unblockPage();
		    });
		}


		function setProductionDatable() {
			
			$("#production_detail_table").dataTable().fnDestroy();
			var DatatablesDataSourceAjaxServer = {
				    init: function() {
				        $("#production_detail_table").DataTable({
				            responsive: !0,
				            pageLength: 10,
				            searchDelay: 500,
							processing: !0,
							serverSide: true,
							ajax: {
								url: "<%=request.getContextPath()%>/production/${Constant.SALES_ORDER}/${salesVo.salesId}/datatable",
								type: "POST",
							},
							lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
							  columns: [{
					                data: "productionId"
					            }, {
					                data: "prefix"
					            }, {
					                data: "productionDate"
					            }],
							
							columnDefs: [{
									targets: 0,
									orderable: !1,
									render: function(a, e, t, n) {
										return (n.row+n.settings._iDisplayStart+1);
									}
								},{
					                targets:1,
					                orderable: !1,
					               	render: function(a, e, t, n) {
					               		return '\n  <a href="/production/'+t.productionId+'" target="_blank" class="m-link m--font-bolder" aria-expanded="true">\n '+t.prefix+t.productionNo+'</a>\n '
					             	}
							},{
								targets: 2,
								render: function(a, e, t, n) {
									return moment(a).format('DD/MM/YYYY');
								}
							}/* ,{
								targets: 3,
				                title: "Total",
				                orderable: !1,
				               	/* render: function(a, e, t, n) {
				               		var action = "";
									action += '<a href="${pageContext.request.contextPath}/purchase/${Constant.PURCHASE_DEBIT_NOTE}/'+t.purchaseId+'/edit" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'
									action += '<button data-url="${pageContext.request.contextPath}/purchase/${Constant.PURCHASE_DEBIT_NOTE}/'+t.purchaseId+'/delete" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill delete-btn" title="Delete"> <i class="fa fa-trash"></i></button>'
									action += '<span class="dropdown">\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="fa fa-ellipsis-h"></i>\n </a>\n<div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item" target="_blank" href="${pageContext.request.contextPath}/purchase/${Constant.PURCHASE_DEBIT_NOTE}/'+t.purchaseId+'/pdf"><i class="fa fa-file-pdf"></i> PDF</a>\n</div>\n</span>\n '
									return action;
				                } }*/
				            
				            ],
				            fixedHeader: {
				                header: false,
				                footer: false
				            },
				        })
				    }
				};
			
			DatatablesDataSourceAjaxServer.init();
		}
		
		function setDeliveryDatable() {
			
			$("#delivery_detail_table").dataTable().fnDestroy();
			var DatatablesDataSourceAjaxServer = {
				    init: function() {
				        $("#delivery_detail_table").DataTable({
				            responsive: !0,
				            pageLength: 10,
				            searchDelay: 500,
							processing: !0,
							serverSide: true,
							ajax: {
								url: "<%=request.getContextPath()%>/delivery/${Constant.SALES_ORDER}/${salesVo.salesId}/datatable",
								type: "POST",
							},
							lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
							  columns: [{
					                data: "deliveryId"
					            }, {
					                data: "deliveryNo"
					            }, {
					                data: "deliveryDate"
					            }],
							
							columnDefs: [{
									targets: 0,
									orderable: !1,
									render: function(a, e, t, n) {
										return (n.row+n.settings._iDisplayStart+1);
									}
								},{
					                targets:1,
					                orderable: !1,
					               	render: function(a, e, t, n) {
					               		return '\n  <a href="/delivery/'+t.deliveryId+'" target="_blank" class="m-link m--font-bolder" aria-expanded="true">\n '+t.prefix+t.deliveryNo+'</a>\n '
					             	}
							},{
								targets: 2,
								render: function(a, e, t, n) {
									return moment(a).format('DD/MM/YYYY');
								}
							}/* ,{
								targets: 3,
				                title: "Total",
				                orderable: !1,
				               	/* render: function(a, e, t, n) {
				               		var action = "";
									action += '<a href="${pageContext.request.contextPath}/purchase/${Constant.PURCHASE_DEBIT_NOTE}/'+t.purchaseId+'/edit" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'
									action += '<button data-url="${pageContext.request.contextPath}/purchase/${Constant.PURCHASE_DEBIT_NOTE}/'+t.purchaseId+'/delete" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill delete-btn" title="Delete"> <i class="fa fa-trash"></i></button>'
									action += '<span class="dropdown">\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="fa fa-ellipsis-h"></i>\n </a>\n<div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item" target="_blank" href="${pageContext.request.contextPath}/purchase/${Constant.PURCHASE_DEBIT_NOTE}/'+t.purchaseId+'/pdf"><i class="fa fa-file-pdf"></i> PDF</a>\n</div>\n</span>\n '
									return action;
				                } }*/
				            
				            ],
				            fixedHeader: {
				                header: false,
				                footer: false
				            },
				        })
				    }
				};
			DatatablesDataSourceAjaxServer.init();
		}

		jQuery(document).ready(function() {
			
			$("#smsbuttonfortransporter" ).click(function() {
				if(${type==Constant.SALES_ORDER && salesVo.status == 'Production Completed'}) {
					$.ajax({
			            url: '/sales/order/${salesVo.salesId}/sendsms/deliverydetails',
			            type: 'post',
			            success: function (data) {
			            	var json = $.parseJSON(data);
			            	var index = 0;
			            	$(json).each(function (i, val) {
			            		$('#offer_message').html(val.message);
			            		$('#transportdiv').show();
			            		$("#invListTable").find("tbody").html("")
			            		$.each(val.transportVos, function (k, v) {
			            			var row = '<tr style="height: 10px;"> '
										+'<td data-field="state" class="btSelectItem_3"><input class="checkbox btSelectItem_3" type="checkbox" id="transport'+index+'" name="transportId'+index+'" value="'+v.cMobileNo+'"></td>'
										+'<td data-field="'+v.cMobileNo+'" class="">'+v.cName+'</td>'
										+'</tr>'
			            			
									$("#invListTable").append(row)
			            			//alert(v.cName);
			            	  	});
			            	});
			            }, 
			        });
				}
			});

			$("#smsbuttonforcustomer" ).click(function() {
				
				//$('#transportdiv').hide();
				if(${type==Constant.SALES_ORDER && (salesVo.status == 'Pending')} || ${type==Constant.SALES_QUOTATION && (salesVo.status == 'SO Generated')} ){
					$.ajax({
		            url: '/sales/order/${salesVo.salesId}/sendsms/salesdetails',
			         type: 'post',
			            success: function (data) {
			            $('#offer_message').html(data);
			            $('#transportdiv').hide();
			            }, 
			        });	
				}
				else if(${type==Constant.SALES_ORDER && salesVo.status == 'In Progress'})
				{
					$.ajax({
			            url: '/sales/order/${salesVo.salesId}/sendsms/productiondetails',
			            type: 'post',
			            success: function (data) {
			            $('#offer_message').html(data);
			            $('#transportdiv').hide();
			            }, 
			        });
				}
				else if(${type==Constant.SALES_ORDER && salesVo.status == 'Production Completed'})
				{
					$.ajax({
			            url: '/sales/order/${salesVo.salesId}/sendsms/details',
			            type: 'post',
			            success: function (data) {
			            $('#offer_message').html(data);
			            $('#transportdiv').hide();
			            }, 
			        });
				}
		
			  });
			
			$('#offer_message').keyup(function () {
				  var max = 500;
				  var len = $(this).val().length;
				  if (len >= max) {
				    $('#charNum').text(' you have reached the limit');
				  } else {
				    var char = max - len;
				    $('#charNum').text(char + ' characters left');
				  }
			});	
			
			$("#send_sms_btn" ).click(function() {
		    	if($("#offer_message").val()!=""){
		    		$.post("/sales/order/${salesVo.salesId}/sendsms", { phoneno: $("#phoneno").val(), message: $("#offer_message").val() })
		    		  .done(function( data ) {
		    			  toastr.options = {
		    					  "closeButton": true,
		    					  "debug": false,
		    					  "newestOnTop": false,
		    					  "progressBar": false,
		    					  "positionClass": "toast-top-center",
		    					  "preventDuplicates": true,
		    					  "onclick": null,
		    					  "showDuration": "300",
		    					  "hideDuration": "1000",
		    					  "timeOut": "5000",
		    					  "extendedTimeOut": "1000",
		    					  "showEasing": "swing",
		    					  "hideEasing": "linear",
		    					  "showMethod": "fadeIn",
		    					  "hideMethod": "fadeOut"
		    					  
		    					};
		    				toastr.success("","Message send successfully");
		    				$('#modal_send_msg').modal('hide');
		    		  });
		    	}else{
		    		toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "newestOnTop": false,
							  "progressBar": false,
							  "positionClass": "toast-top-center",
							  "preventDuplicates": true,
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};
							toastr.error("","Please Write Message");
				    	}
				    });
		 
			
			/* For Tab - 3 Inv */
			 $('#select_all_3').click(function () {
				if(this.checked)
				{
					
					$("#invListTable").children('tbody').each(function(){
						$('.btSelectItem_3').prop('checked', true);
					 }); 
				}
				else
				{
					$("#invListTable").children('tbody').each(function(){
						$('.btSelectItem_3').prop('checked', false);
					 });
				}	
			});
			
			
			 $(".btSelectItem_3").click(function(){
				if($(".btSelectItem_3").length == $(".btSelectItem_3:checked").length) 
				{ 
					$("#select_all_3").prop('checked', true);
				} 
				else 
				{   
					$("#select_all_3").prop('checked', false);
				}
			});
			
		});// Document.Ready End
		
		
	function sendSMS() {
			var phoneNo=""
				$("#invListTable").find("tbody").find(".checkbox").each(function(){
				if($(this).prop('checked') == true) {
					if($(this).val()!=""){
						phoneNo=phoneNo+$(this).val()+",";
					}
	  			}
				
	  		});
			$("#phoneno").val(phoneNo);
			 
		}
		

	</script>
	
</body>

<!-- end::Body -->
</html>