<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
    <%@page import="com.croods.bssgroup.constant.Constant" %>
<!DOCTYPE html>
<html>

<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>${displayType}</title>
	<style type="text/css">
		.select2-container{display: block;}
	</style>
	
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
</head>

<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp"%>

			<div class="m-grid__item m-grid__item--fluid m-wrapper">

				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">${displayType}
							</h3>

							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home"><a href="<%=request.getContextPath() %>/dashboard"
									class="m-nav__link m-nav__link--icon"> <i
										class="m-nav__link-icon la la-home"></i>
								</a></li>
							</ul>

						</div>
					</div>
				</div>
				<!-- END: Subheader -->

				<div class="m-content">

					<div class="row">

						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__head">
									<div class="m-portlet__head-caption">
										<div class="m-portlet__head-title">
											<a href="<%=request.getContextPath()%>/sales/${type}/new"
												class="btn btn-primary m-btn m-btn--icon"> <span><i
													class="la la-plus"></i><span>${displayType}</span></span>
											</a>
										</div>
									</div>
									<div class="m-portlet__head-tools">
										<a href="#" id="export_print" class="btn btn-metal m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top" title="Print"><i class="fa fa-print"></i></a>
										<a href="#" id="export_excel" class="btn btn-success m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top"
											title="Excel"> <i class="fa fa-file-excel"></i>
										</a>
										<a href="#" id="export_pdf" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"
											data-skin="dark" data-toggle="m-tooltip" data-placement="top"
											title="PDF"> <i class="fa fa-file-pdf"></i>
										</a>
									</div>
								</div>
								<div class="m-portlet__body">
									<div class="row m--margin-bottom-20">
										<div class="col-lg-4 col-md-4 col-sm-12">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Customer:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<select class="form-control m-select2" id="contactVo" name="contactVo.contactId" placeholder="Select Customer">
													<option value="0">All Customer</option>
													<c:forEach items="${ContactList}" var="ContactList">
														<option value="${ContactList.contactId}">
															${ContactList.code}
														</option>
													</c:forEach>
												</select>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-12">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">From Date:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="input-group date" >
													<input type="text" class="form-control m-input todaybtn-datepicker" name="fromDate" readonly id="fromDate" data-date-format="dd/mm/yyyy" value="<%=session.getAttribute("firstDateFinancialYear")%>"
														data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'/>
													<div class="input-group-append">
														<span class="input-group-text">
															<i class="la la-calendar"></i>
														</span>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-12">
											<label class="col-form-label col-lg-12 col-md-12 col-sm-12">To Date:</label>
											<div class="col-lg-12 col-md-12 col-sm-12">
												<div class="input-group date" >
													<input type="text" class="form-control m-input todaybtn-datepicker" name="toDate" readonly id="toDate" data-date-format="dd/mm/yyyy" value="<%=session.getAttribute("lastDateFinancialYear")%>"
														data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'/>
													<div class="input-group-append">
														<span class="input-group-text">
															<i class="la la-calendar"></i>
														</span>
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="m_datatable"  >
										<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 collapse" id="m_datatable_group_action_form">
											<div class="row align-items-center">
												<div class="col-xl-12">
													<div class="m-form__group m-form__group--inline">
														<div class="m-form__label m-form__label-no-wrap">
															<label class="m--font-bold m--font-danger-">
																Selected <span id="m_datatable_selected_number"></span>
																records:
															</label>
														</div>
														<div class="m-form__control">
															<div class="btn-toolbar">
																<button class="btn btn-info m-btn btn-sm m-btn m-btn--icon" type="button" id="m_datatable_check_all" onclick="sendSMS()" data-target="#modal_send_msg"  data-toggle="modal"><i class="fa fa-send"></i>&nbsp;Send SMS</button>
																&nbsp;&nbsp;&nbsp;	
																<!-- <button class="btn btn-info m-btn btn-sm m-btn m-btn--icon " type="button" id="m_datatable_check_all" onclick="sendMail()" data-target="#modal_send_mail"  data-toggle="modal"><i class="fa fa-envelope"></i>&nbsp;Send Mail</button> -->	
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										
										<table class="table table-striped- table-bordered table-hover table-checkable"
											id="m_table_1">
											<thead>
												<tr>
													<th>#
				  										<!-- <label class="m-checkbox m-checkbox--bold m-checkbox--state-brand m--margin-top-0 m--margin-bottom-0" style="display:initial">
				  											<input type="checkbox" class="checkbox" id="select-all" name="">
				  											<span></span>
				  										</label> -->
				  									</th>
													<th>${displayType} No.</th>
													<th>${displayType} Date</th>
													<th>Customer</th>
													<th>Total Amount</th>
													<c:if test="${type==Constant.SALES_INVOICE}">
				  										<th>Paid Amount</th>
				  										<th>Due Amount</th>
				  									</c:if>
				  									<th>Status</th>
													<th>Actions</th>
												</tr>
											</thead>
											
										</table>
									</div>
								</div>
							</div>
							<!--end::Portlet-->

						</div>

					</div>
				</div>

			</div>

		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		<div class="modal fade" id="modal_send_msg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog " role="document">
				<input type="hidden" name="contactVo.companyMobileno" id="phoneno" value=""/>
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">
							Send SMS
						</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">
								&times;
							</span>
						</button>
					</div>
					<div class="modal-body">
							<p class="col-12 " id="errorMessageCategory" style="color: red;"></p>
							<div class="form-group m-form__group row">
		            			<label class="col-12 col-form-label">Message <span class="required">*</span></label>
		              			<div class="col-12 ">
		               				<textarea name="message" id="offer_message" class="form-control m-input" maxlength="160" rows="4" placeholder="Message" ></textarea>
		               			</div>
		             		</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="send_sms_btn">Send</button>
	          		    	<button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/datatable/jquery.spring-friendly.js" type="text/javascript"></script>
	<script type="text/javascript">
		//https://datatables.net/reference/button/excelHtml5
		//https://github.com/darrachequesne/spring-data-jpa-datatables
		function deleteSales(id,type){
				var u = '/sales/'+type+'/'+id+'/delete';
			   swal({
	                title: "Are you sure?",
	                text: "You won't be able to revert this!",
	                type: "warning",
	                showCancelButton: !0,
	                confirmButtonText: "Yes, delete it!"
	            }).then(function(e) {
	            	e.value && u != ''? window.location.href=u : console.error("CROODS: data-url undefined ")
	            })
		}
	
		var DatatablesDataSourceHtml = {
		    init: function() {
		        $("#m_table_1").DataTable({
		        	order : [[ 1, 'desc' ]],
		            responsive: !0,
		            pageLength: 10,
		            searchDelay: 500,
					processing: !0,
					serverSide: true,
					ajax: {
						url: "<%=request.getContextPath()%>/sales/${type}/datatable",
						type: "POST",
						"data": function ( d ) {
							return $.extend( {}, d, {
								"contactId": $('#contactVo').val(),
								"fromDate": $('#fromDate').val(),
								"toDate": $('#toDate').val()
							});
						}
					},
					lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
					columns: [{
							data: "salesId"
						},{
							data: "salesNo"
						}, {
							data: "salesDate"
						},
						 {
							data: "contactVo.code"
						}, {
							data: "total"
						}, 
						<c:if test="${type==Constant.SALES_INVOICE}">
						{
							data: "paidAmount"
						}, {
							data: "paidAmount"
						}, 
						</c:if>
						{
							data: "status"
						}, 
						{
							data: "salesId"
					}],
					
					columnDefs: [{
						targets: 1,
						//orderable: !1,
						render: function(a, e, t, n) {
							//console.log(a);
							//console.log(e);
							console.log();
							//console.log(n);
							return '<a href="${pageContext.request.contextPath}/sales/'+t.type+'/'+t.salesId+'" class="m-link m--font-bolder">'+t.prefix+t.salesNo+'</a>';
						}
					},{
						targets: 3,
						orderable: !1,
						render: function(a, e, t, n) {
							
							return '<a href="${pageContext.request.contextPath}/contact/'+t.contactVo.type+'/'+t.contactVo.contactId+'" target="_blank" class="m-link m--font-bolder">'+a+'</a>';
						}
					},{
						targets: 0,
						orderable: !1,
						render: function(a, e, t, n) {
							var  c ="";
							/* c+= '<span class="m-checkbox-inline"><label class="m-checkbox m-checkbox--bold m-checkbox--state-brand m--margin-top-0 m--margin-bottom-0" style="display:initial"><input type="checkbox" class="checkbox" id="" name="" data-mobileno="'+t.contactVo.companyMobileno+'" value="'+t.contactVo.companyMobileno+'"><span></span></label></span>'; */
							c+= '<span class="pull-right">'+(n.row+n.settings._iDisplayStart+1)+'</span>';
							return c;
						}
					},
					<c:if test='${type == Constant.SALES_INVOICE}'>
					{
						targets: 6,
						orderable: !1,
						render: function(a, e, t, n) {
							return t.total - t.paidAmount;
						}
					},
					</c:if>
					{
						<c:if test="${type!=Constant.SALES_INVOICE}">
						targets: 6,
						</c:if>
						<c:if test="${type==Constant.SALES_INVOICE}">
						targets: 7,
						</c:if>
						orderable: !1,
						render: function(a, e, t, n) {
							var action = "";
							if(t.status == 'Pending')
							{
								action += '<a href="${pageContext.request.contextPath}/sales/'+t.type+'/'+t.salesId+'/edit" class="m-portlet__nav-link btn m-btn m-btn--hover-info m-btn--icon m-btn--icon-only m-btn--pill" title="Edit"> <i class="fa fa-edit"></i></a>'
								action += "<button  onclick='deleteSales("+t.salesId+",`"+t.type+"`)' class='m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill' title='Delete'> <i class='fa fa-trash'></i></button>"
							}
							//action += '<button onclick="deleteContact("'+t.salesId+'","'+t.type+'")" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill" title="Delete"> <i class="fa fa-trash"></i></button>'
							if(t.type == 'quotation')
								action += '<span class="dropdown">\n<a href="#" class="m-portlet__nav-link btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true">\n<i class="fa fa-ellipsis-h"></i>\n </a>\n<div class="dropdown-menu dropdown-menu-right">\n<a class="dropdown-item" target="_blank" href="${pageContext.request.contextPath}/sales/'+t.type+'/'+t.salesId+'/pdf"><i class="fa fa-file-pdf"></i> PDF</a>\n</div>\n</span>\n '
							return action;
						}
					},{
						targets: 2,
						render: function(a, e, t, n) {
							return moment(a).format('DD/MM/YYYY');
						}
					},{
						targets: 5,
						className: "text-center",
						render: function(a, e, t, n) {
							if(a === 'Pending') {
								return '<span class="m-badge m-badge--warning m-badge--wide">Pending</span>';
							} else if(a === 'SO Generated') {
								return '<span class="m-badge m-badge--primary m-badge--wide">SO Generated</span>';
							} else if(a === 'In Progress') {
								return '<span class="m-badge m-badge--info m-badge--wide">In Progress</span>';
							} else if(a === 'Production Completed') {
								return '<span class="m-badge m-badge--metal m-badge--wide">Production Completed</span>';
							}else if(a === 'Completed') {
								return '<span class="m-badge m-badge--success m-badge--wide">Completed</span>';
							}
						}
					}],


		            //lengthMenu: [[10,25,50,100,-1], [10,25,50,100,"All"]],
		            fixedHeader: {
		                header: false,
		                footer: false
		            },
		            /* buttons: ["print", "copyHtml5", "excelHtml5", "csvHtml5", "pdfHtml5"], */
		            buttons: [
		            {
		            	extend: 'print',
		            	exportOptions: {
				            <c:if test="${type==Constant.SALES_INVOICE}">
			        			columns: [0,1,2,3,4,5,6]
			        		</c:if>
			        		<c:if test="${type!=Constant.SALES_INVOICE}">
			        			columns: [0,1,2,3,4]
			        		</c:if>
		                },
		                title :'${sessionScope.Name}',
		                messageTop: function () {
	                    	return '<span class="sub-heading"><b>${displayType} List </b><br/> ${sessionScope.firstDateFinancialYear} to ${sessionScope.lastDateFinancialYear}</span>';
	                    },
	                    autoPrint: true,
			            customize: function ( win ) {
					    	 $(win.document.body).find('h1').css('text-align', 'center');
					    	$( win.document.body).find("div").css('text-align', 'center');
					    	
					    	var css = '@page { size: auto;margin-top:5mm;margin-left:5mm;margin-right:5mm; }',
		                    head = win.document.head || win.document.getElementsByTagName('head')[0],
		                    style = win.document.createElement('style');
			                style.type = 'text/css';
			                style.media = 'print';
			 
			                if (style.styleSheet)
			                {
			                  style.styleSheet.cssText = css;
			                }
			                else
			                {
			                  style.appendChild(win.document.createTextNode(css));
			                }
			 
			                head.appendChild(style);
					    	
	              		 } 
		            },
		            {
		            	extend: 'pdfHtml5',
		            	//extension: '.pdf'
		            	exportOptions: {
		            		 <c:if test="${type==Constant.SALES_INVOICE}">
			        			columns: [0,1,2,3,4,5,6]
			        		</c:if>
			        		<c:if test="${type!=Constant.SALES_INVOICE}">
			        			columns: [0,1,2,3,4]
			        		</c:if>
		                },
		                pageSize: 'LEGAL',
		                title :'${sessionScope.Name}',
		                messageTop: function () {
	                    	return '${displayType} List \n ${sessionScope.firstDateFinancialYear} to ${sessionScope.lastDateFinancialYear}';
	                    },
	                    customize: function(doc) {
	                    	doc.styles.tableHeader = {
	                            	fillColor:"#88898c",
	                            	color: '#ffffff',
	                            	bold: 'true',
	                        }
	                    
	                    	doc.styles.title = {
	                          fontSize: '18',
	                          alignment: 'center',
	                          bold: 'true'
                        	}
	                        doc.pageMargins = [ 10, 20, 10, 10 ];
                            doc.content.forEach(function(item) {
		                        if (item.table) {
		                        	<c:if test="${type==Constant.SALES_INVOICE}">
		                        	item.table.widths = [ 40, 85, 70, 125, 75, 75, 75];
					        		</c:if>
					        		<c:if test="${type!=Constant.SALES_INVOICE}">
					        		item.table.widths = [ 40, 110, 95, 200, 100];
					        		</c:if>
		                        	
		                        	item.table.body[1][3].alignment = 'right';
		                        	
		                        	for(i = 0; i < item.table.body.length; i++) {
		                        		item.table.body[i][0].alignment = 'center' ;
		                        		item.table.body[i][1].alignment = 'left' ;
		                        		item.table.body[i][2].alignment = 'left' ;
		                        		item.table.body[i][3].alignment = 'left' ;
		                        		item.table.body[i][4].alignment = 'right' ;
		                        		
		                        		<c:if test="${type==Constant.SALES_INVOICE}">
		                        			item.table.body[i][5].alignment = 'right' ;
		                        			item.table.body[i][6].alignment = 'right' ;
		                        			item.table.body[i][6].margin = [ 0, 0, 5, 0 ];
		                        		</c:if>
		                        		<c:if test="${type!=Constant.SALES_INVOICE}">
		                        			item.table.body[i][4].margin = [ 0, 0, 5, 0 ];
		                        		</c:if>
		                        	}
		                        	
		                        }
                            });
                           
	                        doc.styles.message = {
	  	                          fontSize: '12',
		                          alignment: 'center'
	                        	}
	                    }
		            },
		            {
		            	extend: 'excelHtml5',
		            	//extension: '.pdf'
		            	exportOptions: {
		            		 <c:if test="${type==Constant.SALES_INVOICE}">
			        			columns: [0,1,2,3,4,5,6]
			        		</c:if>
			        		<c:if test="${type!=Constant.SALES_INVOICE}">
			        			columns: [0,1,2,3,4]
			        		</c:if>
		                },
		                title :'${sessionScope.Name}',
		                messageTop: function () {
	                    	return '${displayType} List \n ${sessionScope.firstDateFinancialYear} to ${sessionScope.lastDateFinancialYear}';
	                    },
	                     customize: function(doc) {
	                    	var sheet = doc.xl.worksheets['sheet1.xml'];
	                    }
		            }
		        ],
		        }),$("#contactVo,#fromDate,#toDate").on("change", function(t) {
		        	$('#m_table_1').DataTable().draw()
	            }), $("#export_print").on("click", function(e) {
	                e.preventDefault(), $("#m_table_1").DataTable().button(0).trigger()
	            }), $("#export_excel").on("click", function(e) {
	                e.preventDefault(), $("#m_table_1").DataTable().button(2).trigger()
	            }), $("#export_pdf").on("click", function(e) {
	                e.preventDefault(), $("#m_table_1").DataTable().button(1).trigger()
	            })
		    }
		};
		jQuery(document).ready(function() {
		    DatatablesDataSourceHtml.init();
		    
		    $(".dt-buttons").addClass("m--hide");
		    $("#m_table_1").on("click", ".checkbox", function() {
		    	
				var total=$('.checkbox:checked').length;
				var tt=$('.checkbox').length;
				
				if(total>0){
					$('#m_datatable_group_action_form').collapse("show");
					if(total==tt){total=total-1;}
					$('#m_datatable_selected_number').html(total);
				}else{
					$('#m_datatable_group_action_form').collapse("hide");
				}
					
	  		});
			
			$("#select-all" ).click(function() {
	  			if($("#select-all").prop('checked') == true) {
	  				$("#m_table_1").find(".checkbox").prop('checked',true);
	  			}
	  			else{
	  				$("#m_table_1").find(".checkbox").prop('checked',false);
	  			}
	  		});
			

			$("#send_sms_btn" ).click(function() {
		    	
		    	if($("#offer_message").val()!=""){
		    		$.post("/sales/order/sendsms", { phoneno: $("#phoneno").val(), message: $("#offer_message").val() })
		    		  .done(function( data ) {
		    			  toastr.options = {
		    					  "closeButton": true,
		    					  "debug": false,
		    					  "newestOnTop": false,
		    					  "progressBar": false,
		    					  "positionClass": "toast-top-center",
		    					  "preventDuplicates": true,
		    					  "onclick": null,
		    					  "showDuration": "300",
		    					  "hideDuration": "1000",
		    					  "timeOut": "5000",
		    					  "extendedTimeOut": "1000",
		    					  "showEasing": "swing",
		    					  "hideEasing": "linear",
		    					  "showMethod": "fadeIn",
		    					  "hideMethod": "fadeOut"
		    					  
		    					};
		    				toastr.success("","Message send successfully");
		    				$('#modal_send_msg').modal('hide');
		    		  });
		    	}else{
		    		toastr.options = {
							  "closeButton": true,
							  "debug": false,
							  "newestOnTop": false,
							  "progressBar": false,
							  "positionClass": "toast-top-center",
							  "preventDuplicates": true,
							  "onclick": null,
							  "showDuration": "300",
							  "hideDuration": "1000",
							  "timeOut": "5000",
							  "extendedTimeOut": "1000",
							  "showEasing": "swing",
							  "hideEasing": "linear",
							  "showMethod": "fadeIn",
							  "hideMethod": "fadeOut"
							};
							toastr.error("","Please Write Message");
				    	}
				    			    	
				    });
				});
		
		function sendSMS() {
			
			var phoneNo=""
				$("#m_table_1").find("tbody").find(".checkbox").each(function(){
				if($(this).prop('checked') == true) {
					if($(this).val()!=""){
						phoneNo=phoneNo+$(this).val()+",";
						
					}
	  			}
				
	  		});
			$("#phoneno").val(phoneNo);
			 $("#offer_message").html(""); 
		}
	</script>
	
	
</body>

<!-- end::Body -->
</html>


