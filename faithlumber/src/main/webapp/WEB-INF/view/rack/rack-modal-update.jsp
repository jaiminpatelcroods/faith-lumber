<div class="modal fade" id="rack_update_modal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Edit Rack</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true"> &times; </span>
				</button>
			</div>
			<form id="rack_update_form" class="m-form m-form--state m-form--fit">
				<input type="hidden" id="rackId">
				<div class="modal-body">
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Rack Code:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<input type="text" class="form-control m-input" name="rackCode" id="updateRackCode" placeholder="Rack Code" value="">
						</div>
					</div>
					<!-- <div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">No. of Row:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<input type="text" class="form-control m-input" name="noOfRow" id="updateNoOfRow" placeholder="No. of Row" value="">
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">No. of Column:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<input type="text" class="form-control m-input" name="noOfColumn" id="updateNoOfColumn" placeholder="No. of Column" value="">
						</div>
					</div> -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">
						Close
					</button>
					<button type="button" onclick="" id="updaterack" class="btn btn-primary">
						Save
					</button>
					
				</div>
			</form>
		</div>
	</div>
</div>