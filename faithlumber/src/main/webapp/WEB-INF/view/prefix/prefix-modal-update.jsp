<div class="modal fade" id="prefix_update_modal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Edit Prefix</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true"> &times; </span>
				</button>
			</div>
			<form id="prefix_update_form" class="m-form m-form--state m-form--fit">
				<input name="prefixType" id="prefixType" type="hidden">
				<input name="prefixId" id="prefixId" type="hidden">
				<div class="modal-body">
					<div class="m-widget28 m--padding-left-10">
						<div class="m-widget28__container" >	
							<!-- Start:: Content -->
							<div class="m-widget28__tab tab-content">
								<div class="m-widget28__tab-container tab-pane active">
								    <div class="m-widget28__tab-items">
										<div class="m-widget28__tab-item">
											<span class="m--regular-font-size-">Prefix Type:</span>
											<span id="prefix_type"></span>
										</div>
									</div>					      	 		      	
								</div>
							</div>
							<!-- end:: Content --> 	
						</div>				 	 
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Prefix:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<input type="text" class="form-control m-input" name="prefix" id="prefix" placeholder="Prefix" />
						</div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Sequence No:</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<input type="text" class="form-control m-input" name="sequenceNo" id="sequenceNo" placeholder="Sequence No" />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">
						Close
					</button>
					<button type="button" onclick="" id="updateprefix" class="btn btn-primary">
						Save
					</button>
					
				</div>
			</form>
		</div>
	</div>
</div>