<div class="modal fade" id="category_new_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">
					New Category
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="category_new_form">
					<div class="form-group m-form__group">
						<label for="recipient-name" class="form-control-label">
							Category Name:
						</label>
						<input type="text" name="category" class="form-control"  id="categoryNameModal">
					</div>
				 	<div class="form-group m-form__group">
						<label for="message-text" class="form-control-label">
							Description:
						</label>
						<textarea class="form-control m-input" name="description" id="categoryDescriptionModal"></textarea>
					</div> 
				</form>
			</div>
			<div class="modal-footer">
				<button type="button"  class="btn btn-secondary" data-dismiss="modal">Close</button>
				<button type="button" id="savecategory"  class="btn btn-primary">Save</button>
			</div>
		</div>
	</div>
</div>	
						