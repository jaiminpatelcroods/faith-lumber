<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Dashboard</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		input[type=number]::-webkit-inner-spin-button, 
		input[type=number]::-webkit-outer-spin-button 
		{ 
		  -webkit-appearance: none; 
		  margin: 0; 
		}
	</style>
	
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title">Dashboard</h3>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<!-- START Workload Calculation-->
					<div class="m-content m-ht-smsbox" style="padding-bottom: 0px;">
					<div class="row">
						<div class="col-md-12">
							<div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-brand alert-dismissible fade show" role="alert">
								<div class="m-alert__icon">
									<i class="la la-cubes"> ${totalProductionRemainsQty}</i>
								</div>
								<div id="verify" class="m-alert__text ">
								  	<h4 class="m-widget14__title"> Sales Order Item Remains for Production </h4>
								  	<strong><a style="cursor: pointer;" onclick="verifyMobileNo()">Calculate Workload!</a></strong> 		
								</div>
								<div class="m-alert__text m--hide" id="mobileNoDiv">
								  	<form class="m-form m-form--state m-form--fit m-form--label-align-left">
								  		<div class="row">
											<div class="col-lg-7 col-md-7 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Enter Daily Production Quantity:</label>
													<div class="col-lg-9 col-md-9 col-sm-12">
														<input class="form-control m-input" type="number" value="0.0" placeholder="Enter Daily Production" id="verifyMobNo" name="verifyMobNo" min="0" max="${totalProductionRemainsQty}"/>
													</div>
													<div class="col-lg-3 col-md-3 col-sm-12">
														<button type="button" class="btn btn-brand" id="sendOtp">Calculate</button>
													</div>
												</div>
												
											</div>
											<div class="col-lg-5 col-md-5 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Daily Workload:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input m--bold" disabled="disabled" type="number" value="0.0" placeholder="Enter Daily Production" id="workload" style="font-weight: 500"/>
													</div>
												</div>
											</div>
										</div>
								  	</form>
								</div>
							</div>
						</div>
						</div>
					</div>
					<!-- END WORKLOAD -->
				
				
				<div class="m-content" style="padding-top: 10px;">
						
					<div class="row">
					
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__body  m-portlet__body--no-padding">
									<div class="row m-row--no-padding m-row--col-separator-xl">
										<div class="col-md-12 col-lg-6 col-xl-4">
											<div class="m-widget24">
												<div class="m-widget24__item">
													<h4 class="m-widget24__title">Customers</h4> <br> 
													<span class="m-widget24__desc"> Total Active Customers</span> 
													<span class="m-widget24__stats m--font-brand" id="totalCustomers">${totalCustomers}</span>
													<div class="m--space-10"></div>
													<div class="progress m-progress--sm">
														<div class="progress-bar m--bg-brand" role="progressbar"style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
													</div>
													<span class="m-widget24__change"></span> 
													<span class="m-widget24__number"></span>
												</div>
											</div>
										</div>
										<%-- <div class="col-md-12 col-lg-6 col-xl-3">
											<div class="m-widget24">
												<div class="m-widget24__item">
													<h4 class="m-widget24__title">Suppliers</h4> <br> 
													<span class="m-widget24__desc"> Total Active Suppliers</span> 
													<span class="m-widget24__stats m--font-info" id="totalCustomers">${totalSuppliers}</span>
													<div class="m--space-10"></div>
													<div class="progress m-progress--sm">
														<div class="progress-bar m--bg-info" role="progressbar"style="width: 100%;" aria-valuenow="50" aria-valuemin="0"aria-valuemax="100"></div>
													</div>
													<span class="m-widget24__change"></span> 
													<span class="m-widget24__number"></span>
												</div>
											</div>
										</div> --%>
										<div class="col-md-12 col-lg-6 col-xl-4">
											<div class="m-widget24">
												<div class="m-widget24__item">
													<h4 class="m-widget24__title">Product</h4> <br> 
													<span class="m-widget24__desc"> Total Active Product</span> 
													<span class="m-widget24__stats m--font-danger" id="totalProducts">${totalProduct}</span>
													<div class="m--space-10"></div>
													<div class="progress m-progress--sm">
														<div class="progress-bar m--bg-danger" role="progressbar"style="width: 100%;" aria-valuenow="50" aria-valuemin="0"aria-valuemax="100"></div>
													</div>
													<span class="m-widget24__change"></span> 
													<span class="m-widget24__number"></span>
												</div>
											</div>
										</div>
										<div class="col-md-12 col-lg-6 col-xl-4">
											<div class="m-widget24">
												<div class="m-widget24__item">
													<h4 class="m-widget24__title">Available Stock</h4> <br> 
													<span class="m-widget24__desc">In Qty</span> 
													<span class="m-widget24__stats m--font-success" id="totalStock">${currentAvailableStock}</span>
													<div class="m--space-10"></div>
													<div class="progress m-progress--sm">
														<div class="progress-bar m--bg-success" role="progressbar"style="width: 100%;" aria-valuenow="50" aria-valuemin="0"aria-valuemax="100"></div>
													</div>
													<span class="m-widget24__change"></span> 
													<span class="m-widget24__number"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<!--begin::Portlet-->
							<div class="m-portlet m-portlet--bordered-semi">
								<div class="m-portlet__body  m-portlet__body--no-padding">
									<div class="row m-row--no-padding m-row--col-separator-xl">
										<div class="col-md-6 col-lg-6 col-xl-6">
											<div class="m-widget14">
												<div class="m-widget14__header m--margin-bottom-30">
													<h3 class="m-widget14__title">Last 15 Day Sales Quotation</h3>
													<span class="m-widget14__desc">Day Wise Sales Quotation Details</span>
												</div>
												<div class="m-widget14__chart" >
													<canvas  id="daywise_sales"></canvas>
												</div>
											</div>
										</div>
										
										<div class="col-md-6 col-lg-6 col-xl-6">
											<div class="m-widget14">
												<div class="m-widget14__header m--margin-bottom-30">
													<h3 class="m-widget14__title">Last 15 Day Sales Order</h3>
													<span class="m-widget14__desc">Day Wise Sales Order Details</span>
												</div>
												<div class="m-widget14__chart" >
													<canvas  id="daywise_sales_order"></canvas>
												</div>
											</div>
										</div>
										<%-- <div class="col-md-6 col-lg-6 col-xl-6">
											<div class="m-widget14">
												<div class="m-widget14__header m--margin-bottom-30">
													<h3 class="m-widget14__title">Total Customers</h3>
													<span class="m-widget14__desc">Category Wise</span>
												</div>
												<div class="m-widget14__chart" >
													<div class="row  align-items-left">
														<div class="col">
															<div id="contact_category" class="m-widget14__chart1" style="height: 180px">
															</div>
														</div>
														<div class="col">
															<div class="m-widget14__legends">
																<div class="m-widget14__legend">
																	<span class="m-widget14__legend-bullet m--bg-accent"></span>
																	<span class="m-widget14__legend-text">${totalRetailerCustomers}</span>
																	<span class="m-widget14__legend-text">Retailer</span>
																</div>
																<div class="m-widget14__legend">
																	<span class="m-widget14__legend-bullet m--bg-danger"></span>
																	<span class="m-widget14__legend-text">${totalWholesalerCustomers}</span>
																		<span class="m-widget14__legend-text">Wholesaler</span>
																</div>
																<div class="m-widget14__legend">
																	<span class="m-widget14__legend-bullet m--bg-brand"></span>
																	<span class="m-widget14__legend-text">${totalOtherCustomers}</span>
																		<span class="m-widget14__legend-text">Other</span>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div> --%>
									</div>
								</div>
							</div>	
							<!--end::Portlet-->
						</div>
						<!-- <div class="col-lg-12 col-md-12 col-sm-12">
							begin::Portlet
							<div class="m-portlet m-portlet--full-height">
								<div class="m-portlet__body  m-portlet__body--no-padding">
									<div class="row m-row--no-padding m-row--col-separator-xl">
										<div class="col-md-12 col-lg-12 col-xl-12">
											<div class="m-widget14">
												<div class="m-widget14__header m--margin-bottom-30">
													<h3 class="m-widget14__title">Sales V/S Purchase Chart </h3>
													<span class="m-widget14__desc"></span>
												</div>
												<div class="m-widget14__chart">
													<div id="sales_purchase_chart"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>	
							end::Portlet
						</div> -->
					</div>
					
					
					
					
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/datatable/jquery.spring-friendly.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<script type="text/javascript">
		
		jQuery(document).ready(function() {
			
			// Total Customers Category Wise 
			//0 != $("#contact_category").length && Morris.Donut({ element: "contact_category", data: [{ label: "Retailer", value: ${totalRetailerCustomers} }, { label: "Wholesaler", value: ${totalWholesalerCustomers} }, { label: "Other", value: ${totalOtherCustomers} }], colors: [mApp.getColor("accent"), mApp.getColor("danger"), mApp.getColor("brand")] })
			
			
			var e = $("#daywise_sales");
			if (0 != e.length) {
	            var t = { labels: ['${salesDate}'], datasets: [{ backgroundColor: mApp.getColor("primary"), data: [${salesData}] }] };
	            new Chart(e, { type: "bar", data: t, options: { title: { display: !1 }, tooltips: { intersect: !1, mode: "nearest", xPadding: 10, yPadding: 10, caretPadding: 10 }, legend: { display: !1 }, responsive: !0, maintainAspectRatio: !1, barRadius: 4, scales: { xAxes: [{ display: !1, gridLines: !1, stacked: !0 }], yAxes: [{ display: !1, stacked: !0, gridLines: !1 }] }, layout: { padding: { left: 0, right: 0, top: 0, bottom: 0 } } } })
	        }

			var e1 = $("#daywise_sales_order");
			if (0 != e1.length) {
	            var t = { labels: ['${salesDate2}'], datasets: [{ backgroundColor: mApp.getColor("success"), data: [${salesData2}] }] };
	            new Chart(e1, { type: "bar", data: t, options: { title: { display: !1 }, tooltips: { intersect: !1, mode: "nearest", xPadding: 10, yPadding: 10, caretPadding: 10 }, legend: { display: !1 }, responsive: !0, maintainAspectRatio: !1, barRadius: 4, scales: { xAxes: [{ display: !1, gridLines: !1, stacked: !0 }], yAxes: [{ display: !1, stacked: !0, gridLines: !1 }] }, layout: { padding: { left: 0, right: 0, top: 0, bottom: 0 } } } })	
	        }
	        
	        $.post("/dashboard/salesvspurchase", {
				
			}, function (data, status) {
				if(status == 'success') {
					new Morris.Line({
		                element: 'sales_purchase_chart',
		                data: data,
		                xkey: 'y',
		                
		                ykeys: ['a','b'],
		                labels: ['Sales','Purchase'],
		                hideHover : 'auto',
		              	 xLabelAngle : 45,
		    			 padding: 15,
		    			 axes:'y',
		    			 resize: true,
		    			 gridTextWeight: 'bold'
		    				 
		            });
				}
			});
		});




		function verifyMobileNo(){
			$("#verify").addClass("m--hide");
			$("#mobileNoDiv").removeClass("m--hide");
		}
		
		$('#sendOtp').click(function(e) {

			var totalProductionRemainsQty=${totalProductionRemainsQty};
			var dailyProduction=$('#verifyMobNo').val();

			if(dailyProduction>0)
				$('#workload').val((totalProductionRemainsQty/dailyProduction).toFixed(2));	
			else
				$('#workload').val(0.0);
		});
			
	</script>

</body>
<!-- end::Body -->
</html>