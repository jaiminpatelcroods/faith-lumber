<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>New Employee</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container{width: 100% !important;}
		.input-group .select2-container {
			position: relative;
			-webkit-box-flex: 1;
			-ms-flex: 1 1 auto;
			flex: 1 1 auto;
			width: 1% !important;
			
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		.rotate {
		    -moz-transition: all .5s linear;
		    -webkit-transition: all .5s linear;
		    transition: all .5s linear;
		}
		.rotate.down {
		    -moz-transform:rotate(45deg);
		    -webkit-transform:rotate(45deg);
		    transform:rotate(45deg);
		}
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">New Employee</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/employee" class="m-nav__link">
										<span class="m-nav__link-text">Employee</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="employee_form" action="/employee/save" method="post">	
						
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">Basic Details</h3>
											</div>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Employee Name:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="employeeName" placeholder="Employee Name"/>
													</div>
												</div>
												
												<div class="form-group m-form__group row">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">Mobile No.:</label>
														<input type="text" class="form-control m-input" name="employeeMobileno" placeholder="Mobile No." value="">
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">Email:</label>
														<input type="text" class="form-control m-input" name=employeeEmail  placeholder="Email" value="">
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">Alternative Mobile No.:</label>
														<input type="text" class="form-control m-input" name="alternativeMobileno" placeholder="Alternative Mobile No." value="">
													</div>
													<div class="col-lg-6 m-form__group-sub">
														<label class="form-control-label">Alternative Email:</label>
														<input type="text" class="form-control m-input" name=alternativeEmail  placeholder="Alternative Email" value="">
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Pan No.:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="panNo" placeholder="Pan No."/>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">CTC:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="ctc" placeholder="CTC"/>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Select Department:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<select class="form-control m-select2 department-select2" id="departmentId" name="departmentVo" placeholder="Select Department">
															<option value="">Select Department</option>
															<c:forEach items="${departmentVos}" var="departmentVo">
																<option value="${departmentVo.departmentId}">${departmentVo.departmentName}</option>
															</c:forEach>
														</select>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Select Designation:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<select class="form-control m-select2 designation-select2" id="designationId" name="designationVo" placeholder="Select Designation">
															<option value="">Select Designation</option>
															<c:forEach items="${designationVos}" var="designationVo">
																<option value="${designationVo.designationId}">${designationVo.designationName}</option>
															</c:forEach>
														</select>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Date of Birth:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input clearbtn-datepicker" data-date-format="dd/mm/yyyy" name="dateOfBirth" id="dateOfBirth" readonly placeholder="Date Of Birth"/>
															<div class="input-group-append">
																<span class="input-group-text">
																	<i class="la la-calendar"></i>
																</span>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Anniversary Date:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input clearbtn-datepicker" data-date-format="dd/mm/yyyy" readonly name="anniversaryDate" id="anniversaryDate" placeholder="Anniversary Date"/>
															<div class="input-group-append">
																<span class="input-group-text">
																	<i class="la la-calendar"></i>
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div id="contact_other_repeater" class="m--margin-top-20"> 
				                            <div class="form-group  m-form__group row">
				                                <div data-repeater-list="" class="col-lg-12">                               
				                                    <div data-repeater-item class="row m--margin-bottom-10">                                      
				                                        <div class="col-lg-4">
				                                            <div class="input-group">
				                                                <div class="input-group-prepend">
				                                                    <span class="input-group-text">
				                                                        <i class="fa fa-user"></i>
				                                                    </span>
				                                                </div>
				                                                <input type="text" class="form-control form-control-danger" name="employeeContactVos[{index}].name" placeholder="Name">
				                                            </div>
				                                        </div>
				                                        <div class="col-lg-4">
				                                            <div class="input-group">
				                                                <div class="input-group-prepend">
				                                                    <span class="input-group-text">
				                                                        <i class="fa fa-user-friends"></i>
				                                                    </span>
				                                                </div>
				                                                <input type="text" class="form-control form-control-danger" name="employeeContactVos[{index}].relation" placeholder="Relation">
				                                            </div>
				                                        </div> 
				                                        <div class="col-lg-3">
				                                            <div class="input-group">
				                                                <div class="input-group-prepend">
				                                                    <span class="input-group-text">
				                                                        <i class="fa fa-phone"></i>
				                                                    </span>
				                                                </div>
				                                                <input type="text" class="form-control form-control-danger" name="employeeContactVos[{index}].mobileno" placeholder="Mobile No.">
				                                            </div>
				                                        </div>
				                                        <div class="col-lg-1">
				                                            <a href="JavaScript:void(0)" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
				                                                <i class="la la-remove"></i>
				                                            </a>
				                                        </div>                     
				                                    </div>                                                        
				                                </div>                 
				                            </div>
					                        <div class="m-demo-icon mb-0">
												<div class="m-demo-icon__preview">
													<span class=""><i class="flaticon-plus m--font-primary"></i></span>
												</div>
												<div class="m-demo-icon__class">
												<a href="#" data-toggle="modal" data-repeater-create="" class="m-link m--font-boldest">Add More Contact</a>
												</div>
											</div>
										</div>
									</div>
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions m-form__actions--solid m--padding-10">
				                            <div class="form-group  m-form__group row collapse" id="authentication_details">
				                                <div class="col-lg-12">                               
				                                    <div class="row m--margin-bottom-10">                                      
				                                        <div class="col-lg-4">
				                                            <div class="input-group">
				                                                <div class="input-group-prepend">
				                                                    <span class="input-group-text">
				                                                        <i class="fa fa-user"></i>
				                                                    </span>
				                                                </div>
				                                                <input type="text" class="form-control form-control-danger" name="userName" id="userName" placeholder="User Name">
				                                            </div>
				                                        </div>
				                                        <div class="col-lg-3">
				                                            <div class="input-group">
				                                                <div class="input-group-prepend">
				                                                    <span class="input-group-text">
				                                                        <i class="fa fa-user-lock"></i>
				                                                    </span>
				                                                </div>
				                                                <input type="password" class="form-control form-control-danger" name="password" id="password" placeholder="Password">
				                                            </div>
				                                        </div>
				                                        <div class="col-lg-4">
				                                           
															<div class="input-group m-input-group">
																<div class="input-group-prepend"><span class="input-group-text"><i class="la la-exclamation-triangle"></i></span></div>
																<select class="form-control m-select2" name="userRoleId" id="userRoleId" placeholder="Select User Role">
																	<option value="">Select User Role</option>
																	<c:forEach items="${userRoleVos}" var="userRoleVo">
																		<option value="${userRoleVo.userRoleId}">${userRoleVo.userRoleName}</option>
																	</c:forEach>
																</select>
															</div>
				                                        </div>
				                                    </div>                                                        
				                                </div>                 
				                            </div>
					                        <div class="m-demo-icon mb-0">
												<div class="m-demo-icon__preview">
													<span class=""><i class="fa fa-plus-circle  m--font-brand rotate" id="icon"></i></span>
												</div>
												<div class="m-demo-icon__class">
												<a href="#" data-toggle="modal" data-repeater-create="" id="authenticationDetailsBtn" class="m-link m--font-boldest">Add Authentication Details</a>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
								
								<!--begin::Portlet-->
								<div class="m-portlet" data-repeater-item>
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Address Details
												</h3>
											</div>			
										</div>
									</div>
									<div class="m-portlet__body">
										<div class="row">
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row ">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Address Line 1:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="addressLine1" placeholder="Address Line 1" value="">
													</div>
												</div>
												<div class="form-group m-form__group row ">
													<label class="form-control-label col-lg-12 col-md-12 col-sm-12">Address Line 2:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<input type="text" class="form-control m-input" name="addressLine2" placeholder="Address Line 2" value="">
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
														<label class="form-control-label">Select Country:</label>
														<select class="form-control m-select2" id="countriesCode" name="countriesCode" onchange="getAllStateAjax('countriesCode','stateCode')" data-default="${sessionScope.countryCode}" placeholder="Select Country" data-allow-clear="true">
														</select>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
														<label class="form-control-label">Select State:</label>
														<select class="form-control m-select2" id="stateCode" name="stateCode" onchange="getAllCityAjax('stateCode','cityCode')" data-allow-clear="false" placeholder="Select State">
														</select>
													</div>
												</div>
												<div class="form-group m-form__group row">
													<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
														<label class="form-control-label">Select City:</label>
														<select class="form-control m-select2" id="cityCode" name="cityCode" placeholder="Select City" data-allow-clear="true">
														</select>
													</div>
													<div class="col-lg-6 col-md-6 col-sm-12 m-form__group-sub">
														<label class="form-control-label">ZIP/Postal code:</label>
														<input type="text" class="form-control m-input" name="pinCode" placeholder="ZIP/Postal code" value="">
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions--solid m-form__actions--right">
								<button type="submit" class="btn btn-brand" id="save_employee">
									Submit
								</button>
								
								<a href="/employee" class="btn btn-secondary">
									Cancel
								</a>
							</div>
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<%@include file="../department/department-modal-new.jsp" %>
		<%@include file="../designation/designation-modal-new.jsp" %>
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/script/employee/employee-script.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/script/department/department-quick-script.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/script/designation/designation-quick-script.js" type="text/javascript"></script>
	
	<%@include file="../global/location-ajax.jsp" %>
	
</body>
<!-- end::Body -->
</html>