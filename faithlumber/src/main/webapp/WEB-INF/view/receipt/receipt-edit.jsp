<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page import="com.croods.bssgroup.constant.Constant" %>
<html>
<head>
	
	<%@include file="../header/head.jsp" %>
	
	<title>Edit | ${receiptVo.prefix}${receiptVo.receiptNo} | Receipt</title>
	<link href="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.css" rel="stylesheet" type="text/css" />
	<link href="<%=request.getContextPath()%>/assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
	
	<style type="text/css">
		.select2-container{display: block;}
		.select2-container {
			width: 100% !important;
			
		}
		
		.m-table tr.m-table__row--brand a {
			color: #fff;
			border-color:#fff
		}
		.m-table tr.m-table__row--brand i {
			color: #fff !important;
		}
		
		/* .m-table tr.m-table__row--brand span {
			color: #fff !important;
		} */
		
		table .row {
			margin-left: 0px;
			margin-right: 0px;
		}
		table .select2-selection--single {
			border: none !important;
		}
		table .select2-container--open  {
			border: 1px solid #716aca;
		}
		table .select2-container--focus {
			border: 1px solid #716aca;
		}
		table .m-input1{
			border: none;
		}
		table .m-input1:focus {
			border: 1px solid #716aca;
		}
		
		.card-container {
		  cursor: pointer;
		  height: 100%;
		  perspective: 600;
		  position: relative;
		  width: 55px;
		}
		.card {
		  height: 100%;
		  position: absolute;
		  transform-style: preserve-3d;
		  transition: all 1s ease-in-out;
		  width: 100%;
		}
		.card-hover {
		  transform: rotateY(180deg);
		}
		.card-hover .card-btn {
			/* display: none; */		
		}
		.card .side {
		  backface-visibility: hidden;
		  /* border-radius: 6px; */
		  height: 100%;
		  position: absolute;
		  overflow: hidden;
		  width: 100%;
		}
		.card .back {
		   background: #eaeaed !important;
		  /*color: #0087cc;
		  line-height: 150px; */
		  /* text-align: center; */
		  transform: rotateY(180deg);
		}
	</style>
</head>
<!-- begin::Body -->
<body  class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- Include Header -->
		<%@include file="../header/header.jsp" %>
		
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
		
			<!-- Include Navigation -->
			<%@include file="../header/navigation.jsp" %>
			
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
			
				<!-- BEGIN: Subheader -->
				<div class="m-subheader">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title m-subheader__title--separator">Edit Receipt</h3>
							
							<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
								<li class="m-nav__item m-nav__item--home">
									<a href="<%=request.getContextPath() %>/dashboard" class="m-nav__link m-nav__link--icon">
										<i class="m-nav__link-icon la la-home"></i>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/receipt" class="m-nav__link">
										<span class="m-nav__link-text">Receipt</span>
									</a>
								</li>
								<li class="m-nav__separator">-</li>
								<li class="m-nav__item">
									<a href="<%=request.getContextPath()%>/receipt/${receiptVo.receiptId}" class="m-nav__link">
										<span class="m-nav__link-text">${receiptVo.prefix}${receiptVo.receiptNo}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- END: Subheader -->
				
				<div class="m-content">
					<form class="m-form m-form--state m-form--fit m-form--label-align-left" id="receipt_form" action="<%=request.getContextPath()%>/receipt/save" method="post">
						<input type="hidden" name="deleteBillIds" id="deleteBillIds" value=""/>
						<input type="hidden" name="prefix" id="prefix" value="${receiptVo.prefix}"/>
						<input type="hidden" name="receiptNo" id="receiptNo" value="${receiptVo.receiptNo}"/>
						<input type="hidden" name="receiptId" id="receiptId" value="${receiptVo.receiptId}"/> 
						
						<input type="radio" name="type" id="advancePayment" value="AdvancePayment" <c:if test="${receiptVo.type=='AdvancePayment'}">checked="checked"</c:if> class="m--hide" />
						<input type="radio" name="type" id="againstBill" value="AgainstBill" <c:if test="${receiptVo.type=='AgainstBill'}">checked="checked"</c:if> class="m--hide" />
						<input type="radio" name="type" id="onAccount" <c:if test="${receiptVo.type=='OnAccount'}">checked="checked"</c:if> value="OnAccount" class="m--hide"/>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__foot m-portlet__foot--fit">
										<div class="m-form__actions m-form__actions--solid p-2">
											<div class="row  mt-3">
												<div class="col-lg-3 col-md-3 col-sm-12">
													<div class="m-widget4">
														<div class="m-widget4__item">
															<div class="m-widget4__info">
																<span class="m-widget4__ext">
																	<label class="m-radio m-radio--solid m-radio--brand m-widget4__title">
																		<input type="radio" name="paymentMode" id="paymentModeCash" <c:if test="${receiptVo.paymentMode==Constant.ACCOUNT_CASH}">checked="checked"</c:if> value="${Constant.ACCOUNT_CASH}"> Cash
																		<span></span>
																	</label>
																</span>
															</div>
															
														</div>
													</div>
												</div>
												<div class="col-lg-2 col-md-2 col-sm-12">
													<div class="m-widget4">
														<div class="m-widget4__item">
															<div class="m-widget4__info">
																<span class="m-widget4__ext">
																	<label class="m-radio m-radio--solid m-radio--brand m-widget4__title">
																		<input type="radio" name="paymentMode" id="paymentModeBank" <c:if test="${receiptVo.paymentMode==Constant.BANK}">checked="checked"</c:if> value="${Constant.BANK}"> Bank
																		<span></span>
																	</label>
																</span>
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12" id="bank_div">
													<div class="form-group m-form__group row m--margin-top-5">
														<div class="col-lg-12 col-md-12 col-sm-12">
															<div class="input-group">
																<select class="form-control m-select2" id="bankVo" name="bankVo.bankId" data-default="<c:if test="${not empty receiptVo.bankVo}">${receiptVo.bankVo.bankId}</c:if>" placeholder="Select Bank">
																	<option value="">Select Bank</option>
																</select>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Select Customer:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<select class="form-control m-select2" id="contactVo" name="contactVo.contactId" onchange="changeContact()" placeholder="Select Customer">
															<option value="">Select Customer</option>
															<c:forEach items="${contactVos}" var="contactVo">
																<option value="${contactVo.contactId}"
																	<c:if test="${contactVo.contactId == receiptVo.contactVo.contactId}">selected="selected"</c:if>>
																	${contactVo.companyName}
																</option>
															</c:forEach>
														</select>
													</div>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Receipt Date:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group date" >
															<input type="text" class="form-control m-input todaybtn-datepicker" name="receiptDate" readonly id="receiptDate" data-date-format="dd/mm/yyyy"
																data-date-start-date='<%=session.getAttribute("firstDateFinancialYear")%>' data-date-end-date='<%=session.getAttribute("lastDateFinancialYear")%>'
																	value="<fmt:formatDate pattern="dd/MM/yyyy" value="${receiptVo.receiptDate}" />"/>
															<div class="input-group-append">
																<span class="input-group-text">
																	<i class="la la-calendar"></i>
																</span>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-12 col-md-12 col-sm-12 m--margin-top-15">
												<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
												<div class="form-group m-form__group pl-0 pr-0">
													<div class="row">
														<div class="col-lg-4">
															<label class="m-option">
																<span class="m-option__control">
																	<span class="">
																		<c:choose>
																		    <c:when test="${receiptVo.type=='OnAccount'}">
																		       <span class="m-type m-type--bg m--bg-success" style="width: 25px;height: 25px"><span class="m--font-light"><i class="fa fa-check"></i></span></span>
																		    </c:when>    
																		    <c:otherwise>
																		      <span class="m-type m-type--bg m--bg-metal" style="width: 25px;height: 25px"><span class="m--font-light"><i class="fa fa-check"></i></span></span>				       
																		    </c:otherwise>
																		</c:choose>
																		
																	</span>
																</span>
																<span class="m-option__label">
																	<span class="m-option__head">												 
																		<span class="m-option__title m-option__focus">On Account</span>											 
																	</span>
																	<span class="m-option__body">
																		Estimated 14-20 Day Shipping (&nbsp;Duties end taxes may be due upon delivery&nbsp;)
																	</span>
																</span>		
															</label> 
														</div>
														<div class="col-lg-4">
															<label class="m-option">
																<span class="m-option__control">
																	<span class="">
																		<c:choose>
																		    <c:when test="${receiptVo.type=='AdvancePayment'}">
																		       <span class="m-type m-type--bg m--bg-success" style="width: 25px;height: 25px"><span class="m--font-light"><i class="fa fa-check"></i></span></span>
																		    </c:when>    
																		    <c:otherwise>
																		      <span class="m-type m-type--bg m--bg-metal" style="width: 25px;height: 25px"><span class="m--font-light"><i class="fa fa-check"></i></span></span>				       
																		    </c:otherwise>
																		</c:choose>
																		
																	</span>
																</span>
																<span class="m-option__label">
																	<span class="m-option__head">												 
																		<span class="m-option__title m-option__focus">Advance Payment</span>
																	</span>
																	<span class="m-option__body">
																		Estimated 2-5 Day Shipping (&nbsp;Duties end taxes may be due upon delivery&nbsp;)
																	</span>
																</span>		
															</label> 
														</div>
														<div class="col-lg-4">
															<label class="m-option">
																<span class="m-option__control">
																	<span class="">
																		<c:choose>
																		    <c:when test="${receiptVo.type=='AgainstBill'}">
																		       <span class="m-type m-type--bg m--bg-success" style="width: 25px;height: 25px"><span class="m--font-light"><i class="fa fa-check"></i></span></span>
																		    </c:when>    
																		    <c:otherwise>
																		      <span class="m-type m-type--bg m--bg-metal" style="width: 25px;height: 25px"><span class="m--font-light"><i class="fa fa-check"></i></span></span>
																		    </c:otherwise>
																		</c:choose>
																		
																	</span>
																</span>
																<span class="m-option__label">
																	<span class="m-option__head">												 
																		<span class="m-option__title m-option__focus">Against Bill</span>
																	</span>
																	<span class="m-option__body">
																		Estimated 2-5 Day Shipping (&nbsp;Duties end taxes may be due upon delivery&nbsp;)
																	</span>
																</span>		
															</label> 
														</div>
													</div>
													<div class="m-form__help"><!--must use this helper element to display error message for the options--></div>
												</div>
												<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Amount:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<div class="input-group input-group-lg m-input-group m-input-group--air">
															<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="fa 	fa-rupee-sign"></i></span></div>
															<input type="text" class="form-control m-input m-input--air m--font-boldest m--regular-font-size-lg5" onkeydown="check(event,this)" id="totalPayment" value="${receiptVo.totalPayment}" name="totalPayment" placeholder="Amount" aria-describedby="basic-addon1">
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6 col-md-6 col-sm-12">
												<div class="form-group m-form__group row">
													<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Description:</label>
													<div class="col-lg-12 col-md-12 col-sm-12">
														<textarea class="form-control m-input m-input--air" name="description" placeholder="Enter a description">${receiptVo.description}</textarea>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
							<div class="col-lg-12 col-md-12 col-sm-12" id="bill_details">
								<!--begin::Portlet-->
								<div class="m-portlet">
									<div class="m-portlet__head">
										<div class="m-portlet__head-caption">
											<div class="m-portlet__head-title">
												<span class="m-portlet__head-icon">
													<i class="flaticon-cogwheel-2"></i>
												</span>
												<h3 class="m-portlet__head-text m--font-brand">
													Invoice Details
												</h3>
											</div>			
										</div>
									</div>
									<div class="m-portlet__body" >
										<div class="row">
											<div class="table-responsive m--margin-top-20">
												<table class="table m-table table-bordered table-hover" id="bill_table">
												    <thead>
												      <tr>
												        <th>#</th>
												        <th>Invoice No.</th>
												        <th class="text-right">Original Amount</th>
												        <th class="text-right">Paid Amount</th>
												        <th class="text-right">Pending Amount</th>
												        <th class="text-right">Kasar Amount</th>
												        <th class="text-right">Payment</th>
												        <th></th>
												      </tr>
												    </thead>
												    <tbody data-bill-list="">
												    	<c:set var="total" scope="page" value="0"/>
												    	<c:set var="index" scope="page" value="0"/>
												    	<c:forEach items="${receiptVo.receiptBillVos}" var="receiptBillVo" varStatus="status">
													    	<tr data-bill-item="${index}" class="">
													    		<td class="" style="width: 50px;">
													    			<span data-item-index>${index+1}</span>
													    		</td>
														        <td class="p-0" style="width: 280px;">
														        	<div class="form-group m-form__group p-0">
																		 <input type="hidden" class="form-control form-control-sm" name="receiptBillVos[${index}].salesVo.salesId" value="${receiptBillVo.salesVo.salesId}"/>
																		 <select class="form-control form-control-sm m-input1 m-select2" id="bill${index}" disabled="disabled" onchange="getBillInfo(${index})" name="receiptBillVos[${index}].salesVo.salesId"  placeholder="Select Invoice">
																			<option value="">Select Invoice</option>
																				<option value="${receiptBillVo.salesVo.salesId}" selected="selected">
																					${receiptBillVo.salesVo.prefix}${receiptBillVo.salesVo.salesNo} 
																				</option>
																		</select>
																	</div>
														        </td>
														        <td class="" style="width: 180px;">
														        	<div class="p-0 text-right m--font-bolder text-right" data-item-original="">${receiptBillVo.salesVo.total}</div>
														        </td>
														        <td class="" style="width: 180px;">
														        	<div class="p-0 text-right m--font-bolder text-right" data-item-paid="">${receiptBillVo.salesVo.paidAmount}</div>
														        </td>
														        <td class="" style="width: 180px;">
														        	<div class="p-0 text-right m--font-bolder text-right" data-item-pending="">${receiptBillVo.salesVo.total-receiptBillVo.salesVo.paidAmount}</div>
														        </td>
														        <td class="p-0" style="width: 180px;">
														        	<div class="form-group m-form__group p-0">
														        		<input type="text" class="form-control form-control m-input1 text-right" data-item-kasar="" id="kasarAmount${index}" name="receiptBillVos[${index}].kasar" onchange="checkKasar()" placeholder="Kasar" value="${receiptBillVo.kasar}" />
														        	</div> 
														        </td>
														        <td class="p-0" style="width: 180px;">
														        	<div class="form-group m-form__group p-0">
														        		<c:set var="total" scope="page" value="${total + receiptBillVo.payment}" />
														        		<input type="text" class="form-control m-input text-right m-input1" data-item-amount="" id="paymentAmount${index}" name="receiptBillVos[${index}].payment" onchange="checkAmount()" placeholder="Amount" value="${receiptBillVo.payment}" />
														        	</div>
														        </td>
														        <td class="p-0" style="width: 40px;">
														        	<button type="button" data-item-remove="" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill" title="Delete"> <i class="fa fa-times"></i></button>
														        	<input type="hidden" id="receiptBillId${index}" name="receiptBillVos[${index}].receiptBillId" value="${receiptBillVo.receiptBillId}">
														        	<input type="hidden"  data-item-old-payment="" name="receiptBillVos[${index}].oldPyament" value="${receiptBillVo.payment}">
														        	<input type="hidden"  data-item-old-kasar="" name="receiptBillVos[${index}].oldKasar" value="${receiptBillVo.kasar}">
														        </td>
													    	</tr>
													    	<c:set var="index" scope="page" value="${index+1}"/>
												    	</c:forEach>
												    	<tr data-bill-item="template" class="m--hide">
												    		<td class="" style="width: 50px;">
												    			<span data-item-index></span>
												    		</td>
													        <td class="p-0" style="width: 280px;">
													        	<div class="form-group m-form__group p-0">
																	 <select class="form-control form-control-sm m-input1" id="bill{index}" onchange="getBillInfo({index})" name="receiptBillVos[{index}].salesVo.salesId"  placeholder="Additional Charge">
																		<option value="">Select Bill </option>
																	</select>
																</div>
													        </td>
													        <td class="" style="width: 180px;">
													        	<div class="p-0 text-right m--font-bolder text-right" data-item-original="" name=""></div>
													        </td>
													        <td class="" style="width: 180px;">
													        	<div class="p-0 text-right m--font-bolder text-right" data-item-paid=""></div>
													        </td>
													        <td class="" style="width: 180px;">
													        	<div class="p-0 text-right m--font-bolder text-right" data-item-pending=""></div>
													        </td>
													        <td class="p-0" style="width: 180px;">
													        	<div class="form-group m-form__group p-0">
													        		<input type="text" class="form-control form-control m-input1 text-right" data-item-kasar="" id="kasarAmount{index}" name="receiptBillVos[{index}].kasar" onchange=" checkKasar()" placeholder="Kasar" value="">
													        	</div>
													        </td>
													        <td class="p-0" style="width: 180px;">
													        	<div class="form-group m-form__group p-0">
													        		<input type="text" class="form-control m-input text-right m-input1" data-item-amount="" id="paymentAmount{index}" name="receiptBillVos[{index}].payment" onchange=" checkAmount()" placeholder="Amount" value="0">
													        	</div>
													        </td>
													        <td class="p-0" style="width: 40px;">
													        	<button type="button" data-item-remove="" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon  m-btn--pill" title="Delete"> <i class="fa fa-times"></i></button>
													        	<input type="hidden"  data-item-old-payment="" name="receiptBillVos[{index}].oldPyament" value="0">
													        	<input type="hidden"  data-item-old-kasar="" name="receiptBillVos[{index}].oldKasar" value="0">
													        </td>
												    	</tr>
												    	
													</tbody>
													<tfoot>
												      <tr>
												        <th></th>
												        <th>
												        	<div class="m-demo-icon mb-0">
																<div class="m-demo-icon__preview">
																	<span class=""><i class="flaticon-plus m--font-primary"></i></span>
																</div>
																<div class="m-demo-icon__class">
																<a href="Javascript:void(0)" data-toggel="modal" class="m-link m--font-boldest" id="add_Bill"> Add More</a>
																</div>
															</div>
												        </th>
												        <th></th>
												        <th></th>
												        <th></th>
												        <th></th>
												        <th><span class="m--font-boldest float-right" id="payment_sub_total">${total}</span></th>
												        <th></th>
												      </tr>
												    </tfoot>
											  	</table>
											</div>
										</div>
									</div>
								</div>	
								<!--end::Portlet-->
							</div>
						</div>
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions--solid m-form__actions--right">
								<button type="submit" class="btn btn-brand" id="savereceipt">
									Submit
								</button>
								
								<a href="<%=request.getContextPath()%>/receipt" class="btn btn-secondary">
									Cancel
								</a>
							</div>
						</div>
					</form>
				</div>
		
			</div>
	
		</div>
		<!-- end:: Body -->
		
		<!-- Include Footer -->
		<%@include file="../footer/footer.jsp" %>
		
	</div>
	<!-- end:: Page -->
	
	<!--begin::Base Scripts -->
	<script src="<%=request.getContextPath()%>/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/base/scripts.bundle.js" type="text/javascript"></script>
	<!--end::Base Scripts -->
	
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/select2.js" type="text/javascript"></script>
	<script src="<%=request.getContextPath()%>/assets/demo/demo12/custom/crud/forms/widgets/bootstrap-switch.js" type="text/javascript"></script>
	
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/formValidation.min.js"></script>
	<script src="<%=request.getContextPath()%>/assets/vendors/formvalidation/framework/bootstrap.min.js"></script>
	
	<script src="<%=request.getContextPath()%>/script/receipt/receipt-script.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			setBillId(${index});
		});
	</script>
	
</body>
<!-- end::Body -->
</html>