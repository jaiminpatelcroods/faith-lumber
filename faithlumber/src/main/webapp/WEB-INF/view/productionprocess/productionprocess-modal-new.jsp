<div class="modal fade" id="productionprocess_new_modal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog " role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">New Production Process</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true"> &times; </span>
				</button>
			</div>
			<div class="modal-body">
				<form class="m-form m-form--state m-form--fit" id="productionprocess_new_form">
					<div class="form-group m-form__group row ">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Production Type :</label>
						<select class="form-control m-select2" id="newProductionId" name="productionTypeVo.productionTypeId" placeholder="Select Particular">
							<option value="">Production Type</option>
							<c:forEach items="${productionTypeVos}" var="productionTypeVos">
								<option value="${productionTypeVos.productionTypeId}">${productionTypeVos.productionTypeName}</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-group m-form__group row ">
						<label class="col-form-label col-lg-12 col-md-12 col-sm-12">Name :</label>
						<div class="col-lg-12 col-md-12 col-sm-12">
							<input class="form-control m-input" maxlength="50" name="productionProcessName" id="newProductionProcessName" placeholder="Enter Name Eg. Mori-Munda">
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">
					Close</button>
				<button type="button" id="saveProductionProcess" class="btn btn-primary">
					Save</button>
			</div>
		</div>
	</div>
</div>
