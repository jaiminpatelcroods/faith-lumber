package com.croods.bssgroup.repository.sales;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.sales.SalesAdditionalChargeVo;

@Repository
public interface SalesAdditionalChargeRepository extends JpaRepository<SalesAdditionalChargeVo, Long>{

	@Modifying
	@Query("delete from SalesAdditionalChargeVo  where salesAdditionalChargeId in (?1)")
	void deleteSalesAdditionalChargeByIdIn(List<Long> l);

	//List<SalesAdditionalChargeVo> findAdditionalChargeBySalesId(long salesId);

	List<SalesAdditionalChargeVo> findAdditionalChargeBySalesVoSalesId(long salesId);

}
