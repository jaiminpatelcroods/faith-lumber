package com.croods.bssgroup.repository.complain;

import java.util.Date;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.complain.ComplainVo;

@Repository
public interface ComplainRepository extends JpaRepository<ComplainVo, Long>, DataTablesRepository<ComplainVo, Long>{

	@Query(value="select max(complainNo) from ComplainVo where isDeleted=0 and branchId=?1 and prefix=?2")
	String findMaxComplainNo(long branchId, String prefix);

	ComplainVo findByComplainIdAndBranchIdAndIsDeleted(long complainId, long branchId, int isDeleted);

	@Modifying
	@Query(value="UPDATE ComplainVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3  WHERE complainId = ?1")
	void delete(long complainId, long alterBy, String modifiedOn);

	@Modifying
	@Query("UPDATE ComplainVo SET assignedEmployee.employeeId= ?1, status= ?2, alterBy= ?3, modifiedOn= ?4 WHERE complainId= ?5" )
	void updateAssignedEmployee(long employeeId,String status, long alterBy, String modifiedOn ,long complainId);

	@Modifying
	@Query("UPDATE ComplainVo SET  status= ?1, alterBy= ?2, modifiedOn= ?3, correctiveActions=?4, preventiveAction=?5, remark=?6, completedDate=?7 WHERE complainId=?8")
	void complainAssignedEmployee( String status, long alterBy, String modifiedOn, 
			String correctiveActions, String preventiveAction, String remark, Date completedDate, long complainId);
}
