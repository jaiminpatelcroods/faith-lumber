package com.croods.bssgroup.repository.production;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.production.ProductionItemVo;

@Repository
public interface ProductionItemRepository extends JpaRepository<ProductionItemVo, Long>{

	@Modifying
	@Query("delete from ProductionItemVo  where productionItemId in (?1)")
	void deleteProductionItemIds(List<Long> productionItemIds);

	List<ProductionItemVo> findByProductionVoProductionId(long productionId);
}
