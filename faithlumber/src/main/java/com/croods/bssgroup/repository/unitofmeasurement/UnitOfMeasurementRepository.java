package com.croods.bssgroup.repository.unitofmeasurement;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.unitofmeasurement.UnitOfMeasurementVo;

@Repository
public interface UnitOfMeasurementRepository extends JpaRepository<UnitOfMeasurementVo, Long> {

	UnitOfMeasurementVo findByMeasurementId(long measurementId);
	
	@Modifying
	@Query("update UnitOfMeasurementVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where measurementId=?1")
	void delete(long measurementId, long alterBy, String modifiedOn);

	List<UnitOfMeasurementVo> findByCompanyIdAndIsDeletedOrderByMeasurementIdDesc(long companyId, int isDeleted);

	@Modifying
	@Query("update UnitOfMeasurementVo set isDefault=?3 , alterBy=?2, modifiedOn=?4 where companyId=?1")
	void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn);

	@Modifying
	@Query("update UnitOfMeasurementVo set isDefault=?3, alterBy=?2, modifiedOn=?4 where measurementId=?1")
	void updateIsDefaultByMeasurementId(long measurementId, long alterBy, int isDefault, String modifiedOn);
	
	@Query("SELECT measurementId FROM UnitOfMeasurementVo WHERE isDeleted=0 AND companyId =?1 AND measurementName=?2")
	String isExistUnitofmeasurement(long companyId, String Uom);

	@Query("SELECT measurementId FROM UnitOfMeasurementVo WHERE isDeleted=0 AND companyId =?1 AND measurementName=?2 AND measurementId!=?3")
	String isExistUnitofmeasurement(long companyId, String Uom, long measurementId);
	
	UnitOfMeasurementVo findByMeasurementCodeAndCompanyId(String Uom,long companyId);

}
