package com.croods.bssgroup.repository.farmoparticular;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.farmoparticular.FarmoParticularVo;

@Repository
public interface FarmoParticularRepository extends JpaRepository<FarmoParticularVo, Long>{

	FarmoParticularVo findByFarmoParticularId(long termsandConditionId);
	
	List<FarmoParticularVo> findByCompanyIdAndIsDeletedOrderByFarmoParticularIdDesc(long companyId,int isDeleted);
	
	List<FarmoParticularVo> findByCompanyIdAndIsActiveAndIsDeletedOrderByFarmoParticularIdDesc(long companyId, boolean isDefault, int isDeleted);
	
	List<FarmoParticularVo> findByFarmoParticularIdIn(List<Long> termandConditionIds);
	
	@Modifying
	@Query("update FarmoParticularVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where farmoParticularId=?1")
	void delete(long termsandConditionId, long alterBy, String modifiedOn);
}
