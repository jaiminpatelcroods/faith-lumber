package com.croods.bssgroup.repository.deliverychallan;

import java.util.List;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.deliverychallan.DeliveryChallanVo;
import com.croods.bssgroup.vo.purchaserequest.PurchaseRequestVo;

@Repository
public interface DeliveryChallanRepository extends JpaRepository<DeliveryChallanVo, Long>, DataTablesRepository<DeliveryChallanVo, Long>{

	@Query(value="SELECT MAX(deliveryChallanNo) FROM DeliveryChallanVo WHERE isDeleted=0 AND branchId=?1 AND prefix=?2")
	String findMaxDeliveryChallanNo(long branchId, String prefix);

	DeliveryChallanVo findByDeliveryChallanIdAndBranchIdAndIsDeleted(long deliveryChallanId, long branchId, int isDeleted);

	@Modifying
	@Query(value="UPDATE DeliveryChallanVo SET qcBy.employeeId = ?1, status = ?2, modifiedOn = ?3, alterBy = ?4 WHERE branchId=?5 AND deliveryChallanId=?6")
	void updateQCByAndStatusByDeliveryChallanId(long employeeId, String status, String modifiedOn, long userId, long branchId, long deliveryChallanId);

	@Modifying
	@Query("UPDATE DeliveryChallanVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE deliveryChallanId = ?1")
	void delete(long deliveryChallanId, long userId, String modifiedOn);

	List<DeliveryChallanVo> findByContactVoContactIdAndBranchIdAndIsDeleted(long contactId, long branchId, int isDeleted);

	List<DeliveryChallanVo> findByDeliveryChallanIdIn(List<Long> deliveryChallanIds);
}
