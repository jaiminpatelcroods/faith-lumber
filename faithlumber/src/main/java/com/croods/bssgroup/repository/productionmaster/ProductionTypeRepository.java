package com.croods.bssgroup.repository.productionmaster;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.croods.bssgroup.vo.productionmaster.ProductionTypeVo;

public interface ProductionTypeRepository extends JpaRepository<ProductionTypeVo, Long> {
	
	List<ProductionTypeVo> findByIsDeleted(int isDeleted);

}
