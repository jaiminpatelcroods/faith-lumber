package com.croods.bssgroup.repository.farmotype;

import java.util.List;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.farmotype.FarmoTypeVo;

@Repository
public interface FarmoTypeRepository extends JpaRepository<FarmoTypeVo, Long> , DataTablesRepository<FarmoTypeVo, Long> {

	FarmoTypeVo findByFarmoTypeIdAndCompanyId(long farmoTypeId,long companyId);
	
	List<FarmoTypeVo> findByCompanyIdAndIsDeletedOrderByFarmoTypeIdDesc(long companyId,int isDeleted);
	
	@Modifying
	@Query("update FarmoTypeVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where farmoTypeId=?1")
	void delete(long farmoTypeId, long alterBy, String modifiedOn);

	@Modifying
	@Query("delete from FarmoTypeParticularVo  where farmoTypeParticularsId in (?1)")
	void deleteFarmoTypePerticularsIds(List<Long> l);
}
