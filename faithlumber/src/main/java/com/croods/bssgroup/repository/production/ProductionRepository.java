package com.croods.bssgroup.repository.production;

import java.util.List;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.production.ProductionVo;

@Repository
public interface ProductionRepository extends JpaRepository<ProductionVo, Long>, DataTablesRepository<ProductionVo, Long> {

	ProductionVo findByProductionIdAndBranchId(long ProductionId, long branchId);

	@Query(value="select max(productionNo) from ProductionVo where isDeleted=0 and branchId=?1 and prefix=?2 ")
	String findMaxProductionNo(long branchId, String prefix);
	
	@Modifying
	@Query("UPDATE ProductionVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE productionId = ?1")
	void delete(long productionId , long userId, String modifiedOn);

	List<ProductionVo> findByBranchIdAndIsDeleted(long branchId, int isDeleted);
}
