package com.croods.bssgroup.repository.tax;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.tax.TaxVo;

@Repository
public interface TaxRepository extends JpaRepository<TaxVo, Long> {
	
	TaxVo findByTaxId(long id);
	
	@Modifying
	@Query("update TaxVo set isDeleted=1, alterBy = ?2, modifiedOn = ?3 where taxId=?1")
	void delete(long taxId, long alterBy, String modifiedOn) ;
	
	List<TaxVo> findByCompanyIdAndIsDeletedOrderByTaxIdDesc(long companyId, int isDeleted);

	@Modifying
	@Query("update TaxVo set isDefault=?2 where branchId=?1")
	void updateIsDefaultByCompanyId(long companyId, int isDefault);

	@Modifying
	@Query("update TaxVo set isDefault=?2 where taxId=?1")
	void updateIsDefaultByTaxId(long taxId, int isDefault);
	
	@Query("SELECT taxId FROM TaxVo WHERE isDeleted=0 AND companyId =?1 AND taxName=?2")
	String isExistTax(long companyId, String tax);

	@Query("SELECT taxId FROM TaxVo WHERE isDeleted=0 AND companyId =?1 AND taxName=?2 AND taxId!=?3" )
	String isExistTax(long companyId, String tax, long taxId);
}
