package com.croods.bssgroup.repository.journalvoucher;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.journalvoucher.JournalVoucherAccountVo;

@Repository
public interface JournalVoucherAccountRepository extends JpaRepository<JournalVoucherAccountVo, Long> {
	
	@Modifying
	@Query("DELETE FROM JournalVoucherAccountVo where journalVoucherAccountId in (?1)")
	void deleteJournalVoucherAccountByIdIn(List<Long> journalVoucherAccountIds);
}
