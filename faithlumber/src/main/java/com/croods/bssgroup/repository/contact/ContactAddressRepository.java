package com.croods.bssgroup.repository.contact;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.contact.ContactAddressVo;

@Repository
public interface ContactAddressRepository extends JpaRepository<ContactAddressVo, Long> {
	
	public ContactAddressVo findByContactAddressId(long contactAddressId);
	
	@Modifying
	@Query("update ContactAddressVo set isDeleted=1 where contactAddressId in (?1)")
	void deleteContactAddressByIdIn(List<Long> contactAddressIds);

	@Modifying
	@Query("update ContactAddressVo set isDefault=0 where contactVo.contactId = ?1")
	void updateIsDefaultByContactId(long contactId);
	
	@Modifying
	@Query("update ContactAddressVo set isDefault=1 where contactVo.contactId = ?1 AND contactAddressId = ?2")
	void updateIsDefaultByContactIdAndContactAddressId(long contactId, long contactAddressId);
	
}
