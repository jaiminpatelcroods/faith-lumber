package com.croods.bssgroup.repository.sales;

import java.util.Date;
import java.util.List;

import java.util.Map;


import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.sales.SalesVo;

@Repository
public interface SalesRepository extends JpaRepository<SalesVo, Long>, DataTablesRepository<SalesVo, Long> {

	@Query(value="select max(salesNo) from SalesVo where isDeleted=0 and branchId=?1 and type=?2 and prefix=?3")
	String findMaxSalesNo(long branchId, String type, String prefix);

	@Query("SELECT salesId FROM SalesVo WHERE isDeleted = 0 AND branchId = ?1 AND type = ?2 AND prefix = ?3 AND salesNo = ?4")
	String isExistSalesNo(long branchId, String type, String prefix, long salesNo);

	@Query("SELECT salesId FROM SalesVo WHERE isDeleted = 0 AND branchId = ?1 AND type = ?2 AND prefix = ?3 AND salesNo = ?4 AND salesId != ?5")
	String isExistSalesNo(long branchId, String type, String prefix, long salesNo, long salesId);

	SalesVo findBySalesIdAndBranchIdAndIsDeleted(long salesId, long branchId, int i);

	@Modifying
	@Query("update SalesVo set isDeleted=1 where salesId=?1")
	void deleteSales(long salesId);

	@Query(value="SELECT MAX(total) as total,salesDate FROM (select sum(total) as total, sales_date as salesDate from sales where DATE(sales_date)<=current_date and sales_date+0>=current_date-15 and is_deleted=0 and branch_id=?1 and type=?2 group by sales_date UNION ALL select  0 as total ,i\\:\\:date as salesDate from generate_series(current_date-14, current_date, '1 day'\\:\\:interval) i) AS a group by salesDate order by salesDate",nativeQuery=true)
	List<Map<Double, String>> getLast15DaySales(long branchId, String type);
	
	@Query(value="SELECT cast(sales_date as varchar) as y,\n" + 
			"cast (sum(totalsales) as varchar) AS a, \n" + 
			"cast (sum(totalpurchase) as varchar) AS b \n" + 
			"FROM \n" + 
			"( SELECT cast( ?2\\:\\:date + d.date as varchar) AS sales_date, 0 AS \n" + 
			" totalsales , \n" + 
			" 0 AS totalpurchase \n" + 
			" FROM \n" + 
			" generate_series(0, ?3\\:\\:date - ?2\\:\\:date) AS d(date) UNION ALL SELECT  cast(purchase_date as varchar) as purchase_date,\n" + 
			" 0 AS totalsales , \n" + 
			" sum(total) AS totalpurchase \n" + 
			" FROM \n" + 
			" purchase \n" + 
			" WHERE \n" + 
			" is_deleted=0 \n" + 
			" AND purchase_date BETWEEN ?2 AND ?3 AND branch_id=?1 AND type='bill' GROUP BY purchase_date \n" + 
			" UNION ALL \n" + 
			" SELECT cast(sales_date as varchar), sum(total) AS TotalSales , 0 AS totalpurchase FROM sales WHERE is_deleted=0 AND sales_date BETWEEN ?2 AND ?3 AND sales.branch_id=?1 AND sales.type='invoice' GROUP BY sales_date ) AS a GROUP BY sales_date \n" + 
			" ORDER BY sales_date ASC",nativeQuery=true)
	public  List<Map<String, String>> getSalesVSPurchase(long branchId,Date startDate,Date endDate);
	
	@Query(value="from SalesVo where isDeleted = 0 AND contactVo.contactId = ?5 AND type IN (?1) AND branchId = ?2 AND salesDate BETWEEN ?3 AND ?4")
	List<SalesVo> findByTypesAndContactAndCompanyIdAndSalesDateBetween(List<String> salesTypes, long branchId, Date yearStartDate, Date yearEndDate, long contactId);

	@Query("from SalesVo where type=?1 And branchId=?2 And isDeleted=0 And salesDate between ?4 And ?5 And contactVo.contactId=?3 And paidAmount!=total")
	List<SalesVo> findUnpaidSalesByTypeAndBranchIdAndContactIdAndIsDeletedAndSalesDateBetween(String type,
			long branchId, long contactId, Date startDate, Date endDate);

	@Modifying
	@Query("UPDATE SalesVo SET paidAmount = paidAmount + ?2, alterBy = ?3, modifiedOn = ?4 WHERE salesId=?1")
	void addPaidAmountBySalesId(long salesId, double payment, long userId, String modifiedOn);

	@Modifying
	@Query("update SalesVo set paidAmount=paidAmount-?2 where salesId=?1")
	void updateOldPaymentMinus(long salesId, double oldPyament);

	@Modifying
	@Query("UPDATE SalesVo SET paidAmount=?2 WHERE salesId=?1")
	void updatePaidAmount(long salesId, double amount);
	
	@Query(value="SELECT SUM(total) as totalamount,SUM(paidAmount) as paidAmount  FROM SalesVo WHERE isDeleted = 0 AND type IN (?1) AND branchId = ?2 AND salesDate BETWEEN ?3 AND ?4 And contactVo.contactId=?5")
	public List<Map<Double, Double>> getPaidAndTotalAmountByContactId(List<String> types, long branchId,Date startDate, Date endDate,long contactId);
	
	@Query(value="SELECT SUM(total-paidAmount) FROM SalesVo WHERE isDeleted = 0 AND type IN (?1) AND branchId = ?2 AND salesDate BETWEEN ?4 AND ?5 AND contactVo.contactId = ?3")
	public double getDueAmountByContactId(List<String> types, long branchId, long contactId, Date startDate, Date endDate);

	@Modifying
	@Query("UPDATE SalesVo SET assignedEmployee.employeeId = ?1, status = ?2, alterBy = ?4, modifiedOn = ?5 WHERE salesId=?3")
	void updateAssignedEmployeeAndStatusBySalesId(long employeeId, String status, long salesId, long alterBy, String modifiedOn);

	@Modifying
	@Query("UPDATE SalesVo SET status = ?1, alterBy = ?3, modifiedOn = ?4 WHERE salesId=?2")
	void updateStatusBySalesId(String status, long salesId, long alterBy, String modifiedOn);
	
	@Query(value="from SalesVo where isDeleted = 0 AND status IN (?3) AND type =?2 AND branchId = ?1")
	List<SalesVo> findByBranchIdAndTypeAndStatus(long branchId, String type, List<String>  status);
	/*
	 * @Modifying
	 * 
	 * @Query("UPDATE SalesItemVo set producedQty=producedQty+?3 where salesItemId=?1 AND productVariantVo.productVariantId=?2"
	 * ) void updateSalesOrderItem(long salesItemId, long productVariantId, double
	 * productionQty);
	 */
	
	@Modifying
	@Query("UPDATE SalesItemVo set producedQty=((producedQty-?4)+?3) where salesItemId=?1 AND productVariantVo.productVariantId=?2")
	void updateSalesOrderItem(long salesItemId, long productVariantId, double productionQty, double oldProductionQty);

	SalesVo findBySalesId(long salesId);
	
	@Modifying
	@Query("UPDATE SalesItemVo set deliveredQty=((deliveredQty-?4)+?3) where salesItemId=?1 AND productVariantVo.productVariantId=?2")
	void updateSalesOrderItemForDelivery(long salesItemId, long productVariantId, double deliveryQty,
			double oldDeliveredQty);
}
