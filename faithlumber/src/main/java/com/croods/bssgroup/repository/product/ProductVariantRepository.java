package com.croods.bssgroup.repository.product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.product.ProductVariantVo;

@Repository
public interface ProductVariantRepository extends JpaRepository<ProductVariantVo, Long>{

	List<ProductVariantVo> findByCompanyIdAndIsDeletedAndProductVoIsPackageAndProductVoIsDeleted(long companyId, int isDeleted, boolean isPackage, int deleted);

	ProductVariantVo findByProductVariantIdAndCompanyIdAndIsDeleted(long productVariantId, long companyId, int isDeleted);
	
	@Query("from ProductVariantVo where (LOWER(variantName) like LOWER(concat('%', ?1,'%')) OR LOWER(productVo.name) like LOWER(concat('%', ?1,'%')) ) AND productVo.isDeleted=0 AND companyId=?2")
	List<ProductVariantVo> findProductVariantsWithPackage(String searchKey,long companyId);
	
	@Query("from ProductVariantVo where (LOWER(variantName) like LOWER(concat('%', ?1,'%')) OR LOWER(productVo.name) like LOWER(concat('%', ?1,'%')) OR LOWER(productVo.categoryVo.categoryName) like LOWER(concat('%', ?1,'%')) OR LOWER(productVo.brandVo.brandName) like LOWER(concat('%', ?1,'%'))) AND productVo.isPackage = false AND isDeleted=0 AND companyId=?2")
	List<ProductVariantVo> findProductVariants(String searchKey,long companyId);
	
	@Query("SELECT productVariantId FROM ProductVariantVo WHERE isDeleted=0 AND companyId =?1 AND itemCode=?2")
	String isExistItemCode(long companyId, String itemCode);

	@Query("SELECT productVariantId FROM ProductVariantVo WHERE isDeleted=0 AND companyId =?1 AND itemCode=?2 AND productVariantId!=?3")
	String isExistItemCode(long companyId, String itemCode, long productVariantId);
	
	ProductVariantVo findByItemCodeAndCompanyId(String itemCode, long companyId);

	List<ProductVariantVo> findByProductVariantIdIn(List<Long> productVariantIds);
}
