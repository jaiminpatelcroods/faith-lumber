package com.croods.bssgroup.repository.materialrequest;

import java.util.List;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.materialrequest.MaterialRequestVo;

@Repository
public interface MaterialRequestRepository extends JpaRepository<MaterialRequestVo, Long>, DataTablesRepository<MaterialRequestVo, Long> {

	MaterialRequestVo findByMaterialRequestIdAndBranchId(long materialRequestId, long branchId);

	@Query(value="select max(requestNo) from MaterialRequestVo where isDeleted=0 and branchId=?1 and prefix=?2 ")
	String findMaxRequestNo(long branchId, String prefix);
	
	@Modifying
	@Query("UPDATE MaterialRequestVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE materialRequestId = ?1")
	void delete(long materialRequestId , long userId, String modifiedOn);

	List<MaterialRequestVo> findByBranchIdAndIsDeleted(long branchId, int isDeleted);
}
