package com.croods.bssgroup.repository.gate;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.gate.GateVo;

@Repository
public interface GateRepository extends JpaRepository<GateVo, Long>, DataTablesRepository<GateVo, Long> {

	GateVo findByGateIdAndBranchId(long gateId, long branchId);

	@Modifying
	@Query("UPDATE GateVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE gateId = ?1")
	void delete(long id , long userId, String modifiedOn);

	GateVo findByGateId(long gateId);
	
}
