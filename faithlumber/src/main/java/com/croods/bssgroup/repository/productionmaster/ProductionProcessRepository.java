package com.croods.bssgroup.repository.productionmaster;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.croods.bssgroup.vo.productionmaster.ProductionProcessVo;

public interface ProductionProcessRepository extends JpaRepository<ProductionProcessVo, Long> {

	List<ProductionProcessVo> findByCompanyIdAndIsDeleted(long companyId,int isDeleted);

	ProductionProcessVo findByProductionProcessIdAndIsDeleted(long productionProcessId,int isDeleted);

	@Modifying
	@Query("update ProductionProcessVo SET isDeleted = 1, alterBy=?2, modifiedOn=?3 WHERE productionProcessId=?1")
	void deleteProductionProcess(long id, long userId, String currentDate);

	List<ProductionProcessVo> findByCompanyIdAndProductionTypeVoProductionTypeId(long companyId, long productionTypeId);
}
