package com.croods.bssgroup.repository.sms;


import java.util.List;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.sms.SmsVo;

@Repository
public interface SmsRepository extends JpaRepository<SmsVo, Long>, DataTablesRepository<SmsVo, Long> {
	
	List<SmsVo> findByCompanyIdOrderBySmsIdDesc(long companyId);

}
