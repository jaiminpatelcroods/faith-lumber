package com.croods.bssgroup.repository.jobwork;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.jobwork.JobworkPackageVo;

@Repository
public interface JobworkPackageRepository extends JpaRepository<JobworkPackageVo, Long> {

	List<JobworkPackageVo> findByJobworkVoJobworkId(long jobworkId);
}
