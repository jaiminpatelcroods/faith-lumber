package com.croods.bssgroup.repository.lead;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.lead.LeadActivityVo;

@Repository
public interface LeadActivityRepository extends JpaRepository<LeadActivityVo, Long> {
	
	List<LeadActivityVo> findByLeadVoLeadIdAndBranchIdOrderByLeadActivityIdDesc(long leadId, long branchId);

}
