package com.croods.bssgroup.repository.materialissue;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.materialissue.MaterialIssueItemVo;

@Repository
public interface MaterialIssueItemRepository extends JpaRepository<MaterialIssueItemVo, Long>{

	@Modifying
	@Query("delete from MaterialIssueItemVo  where materialIssueItemId in (?1)")
	void deleteMaterialIssueItemIds(List<Long> materialRequestItemIds);
	
	@Modifying
	@Query("UPDATE MaterialIssueItemVo SET returnQty = ?2 where materialIssueItemId = ?1")
	void updateReturnQTY(long materialIssueItemId, double returnQty);
	
}
