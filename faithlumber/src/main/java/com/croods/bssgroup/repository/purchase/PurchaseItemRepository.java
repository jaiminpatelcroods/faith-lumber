package com.croods.bssgroup.repository.purchase;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.purchase.PurchaseItemVo;

@Repository
public interface PurchaseItemRepository extends JpaRepository<PurchaseItemVo, Long> {

	@Modifying
	@Query("delete from PurchaseItemVo  where purchaseItemId in (?1)")
	void deletePurchaseItemByIdIn(List<Long> purchaseItemIds);

	List<PurchaseItemVo> findByPurchaseVoPurchaseId(long purchaseId);

}
