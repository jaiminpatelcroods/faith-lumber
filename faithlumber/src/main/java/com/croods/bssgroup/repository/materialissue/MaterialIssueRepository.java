package com.croods.bssgroup.repository.materialissue;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.materialissue.MaterialIssueVo;

@Repository
public interface MaterialIssueRepository extends JpaRepository<MaterialIssueVo, Long>, DataTablesRepository<MaterialIssueVo, Long> {

	MaterialIssueVo findByMaterialIssueIdAndBranchId(long materialIssueId, long branchId);
	
	@Query(value="select max(issueNo) from MaterialIssueVo where isDeleted=0 and branchId=?1 and prefix=?2 ")
	String findMaxIssueNo(long branchId, String prefix);
	
	@Modifying
	@Query("UPDATE MaterialIssueVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE materialIssueId = ?1")
	void delete(long materialIssueId , long userId, String modifiedOn);
}
