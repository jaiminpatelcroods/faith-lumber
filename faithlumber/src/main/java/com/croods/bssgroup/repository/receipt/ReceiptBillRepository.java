package com.croods.bssgroup.repository.receipt;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.croods.bssgroup.vo.receipt.ReceiptBillVo;

public interface ReceiptBillRepository extends JpaRepository<ReceiptBillVo, Long>{

	@Modifying
	@Query("delete from ReceiptBillVo  where receiptBillId in (?1)")
	void deleteReceiptBill(List<Long> receiptBillIds);

}
