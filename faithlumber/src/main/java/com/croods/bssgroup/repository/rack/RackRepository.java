package com.croods.bssgroup.repository.rack;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.croods.bssgroup.vo.rack.RackVo;

public interface RackRepository extends JpaRepository<RackVo, Long> {

	RackVo findByRackId(long rackId);
	
	@Modifying
	@Query("update RackVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where rackId=?1")
	void delete(long rackId, long alterBy, String modifiedOn);

	List<RackVo> findByBranchIdAndIsDeletedOrderByRackIdDesc(long branchId, int isDeleted);

	@Query("SELECT rackId FROM RackVo WHERE isDeleted=0 AND branchId =?1 AND rackCode=?2")
	String isExistRackCode(long branchId, String rackCode);

	@Query("SELECT rackId FROM RackVo WHERE isDeleted=0 AND branchId =?1 AND rackCode=?2 AND rackId!=?3")
	String isExistRackCode(long branchId, String rackCode, long rackId);
	
}
