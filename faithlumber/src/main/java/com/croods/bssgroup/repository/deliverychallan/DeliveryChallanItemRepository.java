package com.croods.bssgroup.repository.deliverychallan;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.deliverychallan.DeliveryChallanItemVo;

@Repository
public interface DeliveryChallanItemRepository extends JpaRepository<DeliveryChallanItemVo, Long>{

	@Modifying
	@Query("delete from DeliveryChallanItemVo  where deliveryChallanItemId in (?1)")
	void deleteDeliveryChallanItemIds(List<Long> l);

	@Modifying
	@Query("UPDATE DeliveryChallanItemVo SET qcQty = ?2 where deliveryChallanItemId = ?1")
	void updateQCQTY(long deliveryChallanItemId, float qcQty);
	
	List<DeliveryChallanItemVo> findByDeliveryChallanVoDeliveryChallanId(long deliveryChallanId);

}
