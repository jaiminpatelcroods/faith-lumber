package com.croods.bssgroup.repository.designation;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.designation.DesignationVo;

@Repository
public interface DesignationRepository extends JpaRepository<DesignationVo, Long> {

	DesignationVo findByDesignationId(long designationId);
	
	@Modifying
	@Query("update DesignationVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where designationId=?1")
	void deleteDesignation(long designationId, long alterBy, String modifiedOn);

	List<DesignationVo> findByCompanyIdAndIsDeletedOrderByDesignationIdDesc(long companyId, int isDeleted);
	
	@Query("SELECT designationId FROM DesignationVo WHERE isDeleted=0 AND companyId =?1 AND designationName=?2")
	String isExistDesignation(long companyId, String designationName);

	@Query("SELECT designationId FROM DesignationVo WHERE isDeleted=0 AND companyId =?1 AND designationName=?2 AND designationId!=?3")
	String isExistDesignation(long companyId, String designationName, long designationId);
	
}
