package com.croods.bssgroup.repository.account;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.account.CreditNoteAccountingVo;

@Repository
public interface CreditNoteAccountingRepository extends JpaRepository<CreditNoteAccountingVo, Long>{
	
	List<CreditNoteAccountingVo> findByBranchIdAndIsDeletedAndTransactionDateBetween(long branchId, int isDeleted, Date transctionDateStart, Date transctionDateEnd);
	
	CreditNoteAccountingVo findByCreditNoteAccountIdAndBranchIdAndIsDeleted(long creditNoteAccountId, long branchId,int isDeleted);
	
	@Modifying
	@Query("update CreditNoteAccountingVo set isDeleted = 1, alterBy = ?2, modifiedOn = ?3 where creditNoteAccountId=?1")
	void delete(long id, long alterBy, String modifiedOn);

	@Query(value="select max(voucherNo) from CreditNoteAccountingVo where isDeleted=0 and branchId=?1 and prefix=?2 ")
	String findMaxVoucherNo(long branchId, String prefix);
}
