package com.croods.bssgroup.repository.product;

import java.util.List;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.product.ProductVo;

@Repository
public interface ProductRepository extends JpaRepository<ProductVo, Long>, DataTablesRepository<ProductVo, Long> {

	ProductVo findByProductIdAndCompanyIdAndIsDeleted(long productId, long companyId, int isDeleted);

	List<ProductVo> findByCompanyIdAndIsDeleted(long companyId, int i);
	
	List<ProductVo> findByCompanyIdAndIsDeletedAndHaveVariationAndIsPackage(long companyId, int i,int j, boolean isPackage);
	
	ProductVo findByProductVoProductIdAndCompanyIdAndIsDeleted(long productId, long companyId, int isDeleted);

	long countByCompanyIdAndIsPackageAndIsDeleted(long companyId, boolean isPackage, int isDeleted);

	@Modifying
	@Query("UPDATE ProductVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE productId = ?1")
	void deleteProduct(long productId, long userId, String modifiedOn);

	@Modifying
	@Query("UPDATE ProductVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE productVo.productId = ?1")
	void deleteProductByParentId(long parentId, long userId, String modifiedOn);
}
