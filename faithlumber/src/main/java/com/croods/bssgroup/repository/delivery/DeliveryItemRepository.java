package com.croods.bssgroup.repository.delivery;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.delivery.DeliveryItemVo;

@Repository
public interface DeliveryItemRepository extends JpaRepository<DeliveryItemVo, Long>{

	@Modifying
	@Query("delete from DeliveryItemVo  where deliveryItemId in (?1)")
	void deleteDeliveryItemIds(List<Long> deliveryItemIds);

	List<DeliveryItemVo> findByDeliveryVoDeliveryId(long deliveryId);
}
