package com.croods.bssgroup.repository.fabric;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.fabric.FabricVo;

@Repository
public interface FabricRepository extends JpaRepository<FabricVo, Long>{

	FabricVo findByFabricId(long fabricId);
	
	@Modifying
	@Query("update FabricVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where fabricId=?1")
	void delete(long fabricId, long alterBy, String modifiedOn);

	List<FabricVo> findByCompanyIdAndIsDeletedOrderByFabricIdDesc(long companyId, int isDeleted);

	@Query("SELECT fabricId FROM FabricVo WHERE isDeleted=0 AND companyId =?1 AND fabricName=?2")
	String isExistFabric(long companyId, String fabricName);

	@Query("SELECT fabricId FROM FabricVo WHERE isDeleted=0 AND companyId =?1 AND fabricName=?2 AND fabricId!=?3")
	String isExistFabric(long companyId, String fabricName, long fabricId);
}
