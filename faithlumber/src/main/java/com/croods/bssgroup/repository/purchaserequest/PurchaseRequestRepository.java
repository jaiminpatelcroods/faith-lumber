package com.croods.bssgroup.repository.purchaserequest;

import java.util.Date;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.purchaserequest.PurchaseRequestVo;

@Repository
public interface PurchaseRequestRepository extends JpaRepository<PurchaseRequestVo, Long>, DataTablesRepository<PurchaseRequestVo, Long> {

	@Query(value="select max(requestNo) from PurchaseRequestVo where isDeleted=0 and branchId=?1 and prefix=?2 ")
	String findMaxRequestNo(long branchId, String prefix);

	PurchaseRequestVo findByPurchaseRequestIdAndBranchIdAndIsDeleted(long purchaseRequestId, long branchId, int isDeleted);

	@Modifying
	@Query("UPDATE PurchaseRequestVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE purchaseRequestId = ?1")
	void delete(long purchaseRequestId, long userId, String modifiedOn);

	@Modifying
	@Query("UPDATE PurchaseRequestVo SET status = ?1, expectedDate = ?2, alterBy = ?3, modifiedOn = ?4 WHERE purchaseRequestId = ?5")
	void updateStatusAndExpectedDateByPurchaseRequestId(String status, Date deliveryDate, long alterBy, String modifiedOn, long purchaseRequestId);

	@Query(value="select concat(prefix,requestNo) AS purchaseRequestNo from PurchaseRequestVo where purchaseRequestId = ?1")
	String findRequestNoByPurchaseRequestId(long purchaseRequestId);

	@Modifying
	@Query("UPDATE PurchaseRequestVo SET status = ?1, alterBy = ?2, modifiedOn = ?3 WHERE purchaseRequestId IN (SELECT purchaseRequestId FROM PurchaseVo WHERE purchaseId = ?4)")
	void updateStatusByPurchaseId(String status, long alterBy, String modifiedOn, long purchaseId);

}
