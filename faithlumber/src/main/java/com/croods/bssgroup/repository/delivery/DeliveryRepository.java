package com.croods.bssgroup.repository.delivery;

import java.util.List;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.delivery.DeliveryVo;

@Repository
public interface DeliveryRepository extends JpaRepository<DeliveryVo, Long>, DataTablesRepository<DeliveryVo, Long> {

	DeliveryVo findByDeliveryIdAndBranchId(long DeliveryId, long branchId);

	@Query(value="select max(deliveryNo) from DeliveryVo where isDeleted=0 and branchId=?1 and prefix=?2 ")
	String findMaxDeliveryNo(long branchId, String prefix);
	
	@Modifying
	@Query("UPDATE DeliveryVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE deliveryId = ?1")
	void delete(long deliveryId , long userId, String modifiedOn);

	List<DeliveryVo> findByBranchIdAndIsDeleted(long branchId, int isDeleted);
}
