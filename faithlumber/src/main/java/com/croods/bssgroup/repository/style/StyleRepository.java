package com.croods.bssgroup.repository.style;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.style.StyleVo;

@Repository
public interface StyleRepository extends JpaRepository<StyleVo, Long>{

	StyleVo findByStyleId(long styleId);
	
	@Modifying
	@Query("update StyleVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where styleId=?1")
	void delete(long styleId, long alterBy, String modifiedOn);

	List<StyleVo> findByCompanyIdAndIsDeletedOrderByStyleIdDesc(long companyId, int isDeleted);

	@Query("SELECT styleId FROM StyleVo WHERE isDeleted=0 AND companyId =?1 AND styleName=?2")
	String isExistStyle(long companyId, String styleName);

	@Query("SELECT styleId FROM StyleVo WHERE isDeleted=0 AND companyId =?1 AND styleName=?2 AND styleId!=?3")
	String isExistStyle(long companyId, String styleName, long styleId);
}
