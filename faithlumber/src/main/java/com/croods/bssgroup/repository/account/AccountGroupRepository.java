package com.croods.bssgroup.repository.account;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.account.AccountGroupVo;

@Repository
public interface AccountGroupRepository extends JpaRepository<AccountGroupVo, Long> {

}
