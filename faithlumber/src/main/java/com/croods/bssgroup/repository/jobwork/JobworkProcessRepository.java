package com.croods.bssgroup.repository.jobwork;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.jobwork.JobworkProcessVo;

@Repository
public interface JobworkProcessRepository extends JpaRepository<JobworkProcessVo, Long> {

	@Modifying
	@Query("delete from JobworkProcessVo  where jobworkProcessId in (?1)")	
	void deleteJobworkProcessByIdIn(List<Long> jobworkProcessIds);
}
