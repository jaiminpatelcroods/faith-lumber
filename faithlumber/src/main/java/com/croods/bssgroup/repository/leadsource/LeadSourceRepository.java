package com.croods.bssgroup.repository.leadsource;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.leadsource.LeadSourceVo;

@Repository
public interface LeadSourceRepository extends JpaRepository<LeadSourceVo, Long> {
	
	LeadSourceVo findByLeadSourceId(long leadSourceId);
	
	@Modifying
	@Query("update LeadSourceVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where leadSourceId=?1")
	void deleteLeadSource(long leadSourceId, long alterBy, String modifiedOn);

	List<LeadSourceVo> findByCompanyIdAndIsDeletedAndIsEditableOrderByLeadSourceIdDesc(long companyId, int isDeleted, boolean isEditable);
	
	List<LeadSourceVo> findByCompanyIdAndIsDeleted(long companyId, int isDeleted);

	@Query("SELECT leadSourceId FROM LeadSourceVo WHERE isDeleted=0 AND companyId =?1 AND sourceName=?2")
	String isExistLeadSource(long companyId, String sourceName);

	@Query("SELECT leadSourceId FROM LeadSourceVo WHERE isDeleted=0 AND companyId =?1 AND sourceName=?2 AND leadSourceId!=?3")
	String isExistLeadSource(long companyId, String sourceName, long leadSourceId);

}
