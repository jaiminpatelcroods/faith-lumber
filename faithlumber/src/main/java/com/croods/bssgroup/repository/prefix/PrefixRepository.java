package com.croods.bssgroup.repository.prefix;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.prefix.PrefixVo;

@Repository
public interface PrefixRepository extends JpaRepository<PrefixVo, Long>{
	
	List<PrefixVo> findByBranchIdAndIsDeleted(long branchId,int isDeleted);

	PrefixVo findByPrefixId(long prefixId);

	List<PrefixVo> findByPrefixTypeAndBranchId(String prefixType,long branchId);
}
