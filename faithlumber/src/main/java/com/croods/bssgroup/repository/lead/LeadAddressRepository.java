package com.croods.bssgroup.repository.lead;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.lead.LeadAddressVo;

@Repository
public interface LeadAddressRepository extends JpaRepository<LeadAddressVo, Long> {
	
	public LeadAddressVo findByLeadAddressId(long leadAddressId);
	
	@Modifying
	@Query("update LeadAddressVo set isDeleted=1 where leadAddressId in (?1)")
	void deleteLeadAddressByIdIn(List<Long> leadAddressIds);

	@Modifying
	@Query("update LeadAddressVo set isDefault=0 where leadVo.leadId = ?1")
	void updateIsDefaultByLeadId(long leadId);
	
	@Modifying
	@Query("update LeadAddressVo set isDefault=1 where leadVo.leadId = ?1 AND leadAddressId = ?2")
	void updateIsDefaultByLeadIdAndLeadAddressId(long leadId, long leadAddressId);
	
}
