package com.croods.bssgroup.repository.lead;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.lead.LeadVo;

@Repository
public interface LeadRepository extends JpaRepository<LeadVo, Long>, DataTablesRepository<LeadVo, Long> {

	LeadVo findByLeadIdAndBranchIdAndIsDeleted(long leadId, long branchId, int isDeleted);

	List<LeadVo> findByBranchIdAndIsDeletedOrderByLeadIdDesc(long branchId, int isDeleted);
	
	@Modifying
	@Query("UPDATE LeadVo SET isDeleted = 1, alterBy = ?2, modifiedOn = ?3 WHERE leadId = ?1")
	void deleteLead(long leadId, long userId, String modifiedOn);
	
	@Modifying
	@Query("UPDATE LeadVo SET status = ?2, alterBy = ?3, modifiedOn = ?4 WHERE leadId = ?1")
	void updateLeadStatus(long leadId, String status,  long userId, String modifiedOn);
	
	@Modifying
	@Query("UPDATE LeadVo SET status = ?2, alterBy = ?3, modifiedOn = ?4, assignedEmployee.employeeId = ?5 WHERE leadId = ?1")
	void updateLeadStatusAndAssignedEmployee(long leadId, String status,  long userId, String modifiedOn, long employeeId);

	@Modifying
	@Query("UPDATE LeadVo SET status = ?2, completedDate = ?3, remark = ?4, alterBy = ?5, modifiedOn = ?6 WHERE leadId = ?1")
	void updateCompleteDetails(long leadId, String status, Date completedDate, String remark, long alterBy,
			String modifiedOn);
}
