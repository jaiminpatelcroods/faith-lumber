package com.croods.bssgroup.repository.termsandcondition;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.termsandcondition.TermsAndConditionVo;

@Repository
public interface TermsAndConditionRepository extends JpaRepository<TermsAndConditionVo, Long>{

	TermsAndConditionVo findByTermsandConditionId(long termsandConditionId);
	
	List<TermsAndConditionVo> findByCompanyIdAndIsDeletedOrderByTermsandConditionIdDesc(long companyId,int isDeleted);
	
	List<TermsAndConditionVo> findByCompanyIdAndIsDefaultAndIsDeletedOrderByTermsandConditionIdDesc(long companyId, int isDefault, int isDeleted);
	//companyId = ?1 AND isDefault = ?2 AND isDeleted = 0 AND Order By termsandConditionId DESC
	@Query(nativeQuery = true ,value="SELECT * FROM termandcondition_master WHERE string_to_array(modules, ',') && string_to_array(?3, ',') AND company_id = ?1 AND is_default = ?2 AND is_deleted = 0 Order By termandcondition_id DESC")
	List<TermsAndConditionVo> findByCompanyIdAndIsDefaultAndModules(long companyId, int isDefault, String modules);
	
	@Query(nativeQuery = true ,value="SELECT * FROM termandcondition_master WHERE string_to_array(modules, ',') && string_to_array(?2, ',') AND company_id = ?1 AND is_deleted = 0 ORDER BY termandcondition_id DESC")
	List<TermsAndConditionVo> findByCompanyIdAndModules(long companyId, String modules);
	
	//@Query("FROM TermsAndConditionVo WHERE termandconditionId IN (?1)")
	List<TermsAndConditionVo> findByTermsandConditionIdIn(List<Long> termandConditionIds);
	
	@Modifying
	@Query("update TermsAndConditionVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where termsandConditionId=?1")
	void delete(long termsandConditionId, long alterBy, String modifiedOn);
}
