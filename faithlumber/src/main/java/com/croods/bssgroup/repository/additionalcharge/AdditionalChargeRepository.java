package com.croods.bssgroup.repository.additionalcharge;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.additionalcharge.AdditionalChargeVo;

@Repository
public interface AdditionalChargeRepository extends JpaRepository<AdditionalChargeVo, Long> {
	
	AdditionalChargeVo findByAdditionalChargeId(long additionalChargeId);
	
	List<AdditionalChargeVo> findByCompanyIdAndIsDeletedOrderByAdditionalChargeIdDesc(long companyId, int delete);
	
	@Modifying
	@Query("update AdditionalChargeVo set isDeleted=1, alterBy=?2, modifiedOn=?3 where additionalChargeId=?1")
	void delete(long additionalChargeId, long alterBy, String modifiedOn);
}
