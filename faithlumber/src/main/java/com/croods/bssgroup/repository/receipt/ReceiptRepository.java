package com.croods.bssgroup.repository.receipt;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.receipt.ReceiptVo;

@Repository
public interface ReceiptRepository extends DataTablesRepository<ReceiptVo, Long>,JpaRepository<ReceiptVo, Long>{

	@Query(value="SELECT MAX(receiptNo) FROM ReceiptVo WHERE isDeleted=0 AND branchId=?1 AND prefix=?2")
	String findMaxReceiptNo(long branchId, String prefix);

	ReceiptVo findByReceiptIdAndBranchIdAndIsDeleted(long receiptId, long branchId, int i);

}
