package com.croods.bssgroup.repository.stock;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.stock.StockTransactionVo;

@Repository
public interface StockTransactionRepository extends JpaRepository<StockTransactionVo, Long>{
	
	@Modifying
	@Query(value="delete from StockTransactionVo where branchId=?1 and typeId=?2 and type=?3")
	public void deleteStockTransaction(Long branchId, Long typeId, String type);
	
	@Query("select SUM(inQuantity-outQuantity) FROM StockTransactionVo WHERE branchId = ?1 AND productVariantVo.productVo.productId = ?2 AND stockTransactionDate BETWEEN ?3 AND ?4 AND yearInterval = ?5")
	public double getTotalQty(long branchId, long productId, Date startDate, Date endDate, String yearInterval);
	
	@Query("select SUM(inQuantity-outQuantity) FROM StockTransactionVo WHERE branchId = ?1 AND productVariantVo.productVariantId = ?2 AND stockTransactionDate BETWEEN ?3 AND ?4 AND yearInterval = ?5")
	public double getVariantQty(long branchId, long productVariantId, Date startDate, Date endDate, String yearInterval);

	@Query("select baleNo AS baleNo, SUM(inQuantity-outQuantity) AS qty, designNo AS designNo FROM StockTransactionVo WHERE branchId = ?1 AND productVariantVo.productVariantId = ?2 AND stockTransactionDate BETWEEN ?3 AND ?4 AND yearInterval = ?5 GROUP BY designNo, baleNo HAVING sum(inQuantity) - sum(outQuantity) !=0")
	public List<Map<String, String>> findAllBaleNoByBranchIdAndProductVariantId(long branchId, long productVariantId, Date startDate, Date endDate, String yearInterval);
	
	@Query("select baleNo AS baleNo, SUM(inQuantity-outQuantity) AS qty FROM StockTransactionVo WHERE branchId = ?1 AND productVariantVo.productVariantId = ?2 AND designNo = ?3 AND stockTransactionDate BETWEEN ?4 AND ?5 AND yearInterval = ?6 GROUP BY baleNo HAVING sum(inQuantity) - sum(outQuantity) !=0")
	public List<Map<String, String>> findAllBaleNoByBranchIdAndProductVariantIdAndDesignNo(long branchId,
			long productVariantId, String designNo, Date startDate, Date endDate, String yearInterval);
	
	@Query("select designNo AS designNo, SUM(inQuantity-outQuantity) AS qty FROM StockTransactionVo WHERE branchId = ?1 AND productVariantVo.productVariantId = ?2 AND stockTransactionDate BETWEEN ?3 AND ?4 AND yearInterval = ?5 GROUP BY designNo HAVING sum(inQuantity) - sum(outQuantity) != 0")
	public List<Map<String, String>> findAllDesignNoByBranchIdAndProductVariantId(long branchId, long productVariantId,
			Date startDate, Date endDate, String yearInterval);

	@Query("select SUM(inQuantity-outQuantity) FROM StockTransactionVo WHERE branchId = ?1 AND stockTransactionDate BETWEEN ?2 AND ?3 AND yearInterval = ?4")
	public double getAllProductTotalQty(long branchId, Date startDate, Date endDate, String yearInterval);
	
}
