package com.croods.bssgroup.repository.sales;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.sales.SalesItemVo;

@Repository
public interface SalesItemRepository extends JpaRepository<SalesItemVo, Long>{
	
	@Modifying
	@Query("delete from SalesItemVo  where salesItemId in (?1)")
	void deleteSalesItemByIdIn(List<Long> l);

	@Query("from SalesItemVo where salesVo.salesId =?3 AND productVariantVo.companyId=?2 AND productVariantVo.itemCode=?1")
	SalesItemVo findByItemCodeAndCompanyIdAndSalesId(String itemCode, long companyId, long salesId);

	List<SalesItemVo> findBySalesVoSalesId(long salesId);
	
	@Query(value="select qty from SalesItemVo where salesItemId=?1")
	double findQtyBySalesItemId(long salesItemId);

	@Query(value="select producedQty from SalesItemVo where salesItemId=?1")
	double findProducedQtyBySalesItemId(long salesItemId);

	@Query(value="select deliveredQty from SalesItemVo where salesItemId=?1")
	double findDeliveredQtyBySalesItemId(long salesItemId);
	
	@Query(value="select SUM(producedQty-deliveredQty) from SalesItemVo where salesVo.isDeleted=0 and salesVo.type='order' and salesVo.branchId=?1 ")
	double findCurrentAvailableStock(long branchId);

	@Query(value="select SUM(qty-producedQty) from SalesItemVo where salesVo.isDeleted=0 and salesVo.type='order' and salesVo.branchId=?1 ")
	double findRemainsProductionStock(long branchId);

	@Query(value="select SUM(producedQty-deliveredQty) from SalesItemVo where salesVo.isDeleted=0 and salesVo.type='order' and salesVo.branchId=?1 and productVariantVo.productVo.productId = ?2 ")
	double findCurrentAvailableStock(long branchId, long productId);

	@Query(value="select SUM(producedQty-deliveredQty) from SalesItemVo where salesVo.isDeleted=0 and salesVo.type='order' and salesVo.branchId=?1 and productVariantVo.productVariantId = ?2 ")
	double findCurrentProductVariantAvailableStock(long branchId, long productVariantId);
}
