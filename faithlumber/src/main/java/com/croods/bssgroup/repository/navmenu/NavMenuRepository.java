package com.croods.bssgroup.repository.navmenu;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.navmenu.NavMenuVo;

@Repository
public interface NavMenuRepository extends JpaRepository<NavMenuVo, Long> {

	@Query(nativeQuery=true,value="SELECT * FROM nav_menu WHERE nav_menu_id IN (SELECT nav_menu_id FROM nav_menu_permission WHERE user_front_id=?1)  GROUP BY nav_menu_id ORDER BY ordering ASC")
	List<NavMenuVo> findNavMenuByUserFrontId(long userFrontId);

	@Query(nativeQuery=true,value="SELECT * FROM nav_menu WHERE nav_menu_id IN (SELECT nav_menu_id FROM nav_menu_permission WHERE user_role_id=?1 AND status='active')  GROUP BY nav_menu_id ORDER BY ordering ASC")
	List<NavMenuVo> findNavMenuByUserRoleId(long userRoleId);
}
