package com.croods.bssgroup.repository.materialrequest;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.materialrequest.MaterialRequestItemVo;

@Repository
public interface MaterialRequestItemRepository extends JpaRepository<MaterialRequestItemVo, Long>{

	@Modifying
	@Query("delete from MaterialRequestItemVo  where materialRequestItemId in (?1)")
	void deleteMaterialRequestItemIds(List<Long> materialRequestItemIds);

	List<MaterialRequestItemVo> findByMaterialRequestVoMaterialRequestId(long materialRequestId);
}
