package com.croods.bssgroup.repository.jobwork;

import java.util.List;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.jobwork.JobworkVo;

@Repository
public interface JobworkRepository extends JpaRepository<JobworkVo, Long>, DataTablesRepository<JobworkVo, Long> {

	JobworkVo findByJobworkIdAndBranchIdAndIsDeleted(long jobworkId, long branchId, int isDeleted);

	@Query(value="select max(jobworkNo) from JobworkVo where isDeleted=0 and branchId=?1 and prefix=?2")
	String findMaxJobworkNo(long branchId, String prefix);

	List<JobworkVo> findByBranchIdAndStatus(long branchId, String status);

}
