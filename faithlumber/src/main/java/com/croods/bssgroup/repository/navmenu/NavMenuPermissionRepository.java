package com.croods.bssgroup.repository.navmenu;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.navmenu.NavMenuPermissionVo;

@Repository
public interface NavMenuPermissionRepository extends JpaRepository<NavMenuPermissionVo, Long> {
	
	@Query(value="from NavMenuPermissionVo  where userFrontVo.userFrontId=?1")
	List<NavMenuPermissionVo> findByUserfrontId(long userFrontId);
	
	@Query(value="from NavMenuPermissionVo  where userRoleVo.userRoleId=?1")
	List<NavMenuPermissionVo> findByUserRoleId(long userRoleId);
	
	public int deleteByNavMenuPermissionIdIn(List<Long> navMenuPermissionIds);
}
