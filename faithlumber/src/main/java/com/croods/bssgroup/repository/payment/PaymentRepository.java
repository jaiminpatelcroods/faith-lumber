package com.croods.bssgroup.repository.payment;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.payment.PaymentVo;

@Repository
public interface PaymentRepository extends JpaRepository<PaymentVo, Long>, DataTablesRepository<PaymentVo, Long> {

	@Query(value="SELECT MAX(paymentNo) FROM PaymentVo WHERE isDeleted=0 AND branchId=?1 AND prefix=?2")
	String findMaxPaymentNo(long branchId, String prefix);

	PaymentVo findByPaymentIdAndBranchIdAndIsDeleted(long paymentId, long branchId, int i);

}
