package com.croods.bssgroup.repository.location;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.location.StateVo;

@Repository
public interface StateRepository extends JpaRepository<StateVo, Long> {
	
	@Query("from StateVo where countriesCode =:id")
	public List<StateVo> findByCountriesCode(@Param("id")String id);

	@Query("SELECT stateName FROM StateVo WHERE stateCode = ?1")
	public String findStateNameByStateCode(String stateCode);
}
