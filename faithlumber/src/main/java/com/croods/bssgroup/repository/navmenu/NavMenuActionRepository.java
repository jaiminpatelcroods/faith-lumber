package com.croods.bssgroup.repository.navmenu;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.navmenu.NavMenuActionVo;

@Repository
public interface NavMenuActionRepository extends JpaRepository<NavMenuActionVo, Long> {

}
