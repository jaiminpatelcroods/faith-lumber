package com.croods.bssgroup.vo.jobwork;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.croods.bssgroup.vo.product.ProductVariantVo;

import lombok.Data;

@Entity
@Table(name = "jobwork_input")
@DynamicUpdate(value = true)
@Data
public class JobworkInputVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "jobwork_input_id", length = 10)
	private long jobworkInputId;
	
	@ManyToOne
	@JoinColumn(name="product_variant_id",referencedColumnName="product_variant_id")
	private ProductVariantVo productVariantVo;
	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobwork_id", referencedColumnName = "jobwork_id")
	JobworkVo jobworkVo;
	
	@Column(name="bale_no",length=50)
	private String baleNo;
	
	@Column(name="design_no",length=100)
	private String designNo;
	
	@Column(name = "jobwork_qty",length=10)
	private float jobworkQty;
	
	@Column(name = "faulty_qty",length=10)
	private float faultyQty;
}
