package com.croods.bssgroup.vo.lead;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="lead_address")
@Getter @Setter
public class LeadAddressVo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="lead_address_id",length=10)
	private long leadAddressId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="lead_id",referencedColumnName="lead_id")
	private LeadVo leadVo;
	
	@Column(name="compny_name", length=100)
	private String companyName;
	
	@Column(name="address_line_1",length=300,columnDefinition="text")
	private String addressLine1;
	
	@Column(name="address_line_2",length=300,columnDefinition="text")
	private String addressLine2;
	
	@Column(name="countries_code",length=50)
	private String countriesCode;
	
	@Transient
	String countriesName;
	
	@Column(name="state_code",length=50)
	private String stateCode;
	
	@Transient
	String stateName;
	
	@Column(name="city_code",length=50)
	private String cityCode;
	
	@Transient
	String cityName;
	
	@Column(name="pin_code",length=6)
	private String pinCode;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="is_default",length=1,columnDefinition="int default 0")
	private int isDefault;
}
