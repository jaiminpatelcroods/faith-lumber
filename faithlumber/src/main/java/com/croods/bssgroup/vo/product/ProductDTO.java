package com.croods.bssgroup.vo.product;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class ProductDTO {

	String text;
	
	long id;
}
