package com.croods.bssgroup.vo.tax;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.bssgroup.vo.account.AccountCustomVo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;


@Entity
@Table(name="tax_master")
public class TaxVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="tax_id",length=10)
	private long taxId;
	
	@Column(name="tax_name",length=100)
	private String taxName;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="tax_rate",length=6)
	private double taxRate=0;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;

	@JsonProperty(access = Access.WRITE_ONLY)
	@ManyToOne(fetch =FetchType.LAZY)
	@JoinColumn(name = "account_custom_id", referencedColumnName = "account_custom_id")
	private AccountCustomVo accountCustomVo;
	
	@Column(name = "is_default", length = 1, columnDefinition = "int default 0")
	private int isDefault;

	public long getTaxId() {
		return taxId;
	}

	public void setTaxId(long taxId) {
		this.taxId = taxId;
	}

	public String getTaxName() {
		return taxName;
	}

	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public long getAlterBy() {
		return alterBy;
	}

	public void setAlterBy(long alterBy) {
		this.alterBy = alterBy;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public AccountCustomVo getAccountCustomVo() {
		return accountCustomVo;
	}

	public void setAccountCustomVo(AccountCustomVo accountCustomVo) {
		this.accountCustomVo = accountCustomVo;
	}

	public int getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(int isDefault) {
		this.isDefault = isDefault;
	}
	
}