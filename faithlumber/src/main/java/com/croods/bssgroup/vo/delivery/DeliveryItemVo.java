package com.croods.bssgroup.vo.delivery;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.croods.bssgroup.vo.product.ProductVariantVo;

import lombok.Data;

@Entity
@Table(name = "delivery_item")
@Data
public class DeliveryItemVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "delivery_item_id", length = 10)
	private long deliveryItemId;
	
	@Column(name="qty")
	private double qty=0.0;
	
	@ManyToOne
	@JoinColumn(name="product_variant_id",referencedColumnName="product_variant_id")
	private ProductVariantVo productVariantVo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "delivery_id", referencedColumnName = "delivery_id")
	private DeliveryVo deliveryVo;
	
	@Transient
	private double oldDeliveredQty;
	
	@Transient
	private double soTotalQty=0.0;
	
	@Transient
	private double deliveredTotalQty=0.0;
	
	@Column(name="sales_item_id",length=10)
	private long salesItemId;
}
