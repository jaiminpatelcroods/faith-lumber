package com.croods.bssgroup.vo.jobwork;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.bssgroup.vo.farmoparticular.FarmoParticularVo;
import com.croods.bssgroup.vo.farmotype.FarmoTypeVo;

import lombok.Data;

@Entity
@Table(name="jobwork_farmo_type_particulars")
@Data
public class JobworkFarmoTypeParticularVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "jobwork_farmo_type_particulars_id", length = 10)
	private long jobworkFarmoTypeParticularsId;
	
	@ManyToOne
	@JoinColumn(name="farmo_particular_id",referencedColumnName="farmo_particular_id")
	FarmoParticularVo farmoParticularVo;
	
	@Column(name="process",length=100)
	private String process;
	
	@Column(name="farmo_particular_measurement")
	private float farmoParticularMeasurement;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobwork_id", referencedColumnName = "jobwork_id")
	JobworkVo jobworkVo;
	 
	
}
