package com.croods.bssgroup.vo.delivery;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import com.croods.bssgroup.vo.contact.ContactVo;
import com.croods.bssgroup.vo.sales.SalesVo;

import lombok.Data;

@Entity
@Table(name = "delivery")
@Data
@DynamicUpdate(value = true)
public class DeliveryVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "delivery_id", length = 10)
	private long deliveryId;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "delivery_date")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date deliveryDate;
	
	@Column(name = "prefix", length = 50)
	private String prefix;
	
	@Column(name = "delivery_no", length = 30)
	private long deliveryNo;
	
	@Column(name="note",length=300,columnDefinition="text")
	private String note;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="sales_id",referencedColumnName="sales_id")
	private SalesVo salesVo;
	
	@ManyToOne
	@JoinColumn(name="contact_id",referencedColumnName="contact_id")
	private ContactVo contactVo;
	
	@ManyToOne
	@JoinColumn(name="transport_id",referencedColumnName="contact_id")
	private ContactVo contactTransportVo;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "deliveryVo", cascade = CascadeType.ALL)
	private List<DeliveryItemVo> deliveryItemVos;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="vehicle_number",length=50)
	private String vehicleNo;
}
