package com.croods.bssgroup.vo.navmenu;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="nav_sub_menu")
public class NavSubMenuVo {	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "nav_sub_menu_id", length = 10)
	private long navSubMenuId;
	
	@ManyToOne
	@JoinColumn(name="nav_menu_id",referencedColumnName = "nav_menu_id")
	private NavMenuVo navMenuVo;
	
	@Column(name="title",length=50)
	private String title;

	@Column(name="menu_url",length=50)
	private String menuURL;

	@Column(name="icon_class",length=50)
	private String iconClass;

	@Column(name="status",length=50)
	private String status;

	@Column(name="ordering")
	private int ordering;

	@Column(name="type",length=50)
	private String type;
	
	@Column(name="action",length=50)
	private String subMenuAction;
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public long getNavSubMenuId() {
		return navSubMenuId;
	}

	public void setNavSubMenuId(long navSubMenuId) {
		this.navSubMenuId = navSubMenuId;
	}

	public NavMenuVo getNavMenuVo() {
		return navMenuVo;
	}

	public void setNavMenuVo(NavMenuVo navMenuVo) {
		this.navMenuVo = navMenuVo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMenuURL() {
		return menuURL;
	}

	public void setMenuURL(String menuURL) {
		this.menuURL = menuURL;
	}

	public String getIconClass() {
		return iconClass;
	}

	public void setIconClass(String iconClass) {
		this.iconClass = iconClass;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getOrdering() {
		return ordering;
	}

	public void setOrdering(int ordering) {
		this.ordering = ordering;
	}

	public String getSubMenuAction() {
		return subMenuAction;
	}

	public void setSubMenuAction(String subMenuAction) {
		this.subMenuAction = subMenuAction;
	}
	
}
