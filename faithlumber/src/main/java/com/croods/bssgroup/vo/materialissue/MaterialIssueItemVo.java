package com.croods.bssgroup.vo.materialissue;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.bssgroup.vo.product.ProductVariantVo;

import lombok.Data;

@Entity
@Table(name = "material_issue_item")
@Data
public class MaterialIssueItemVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "material_issue_item_id", length = 10)
	private long materialIssueItemId;
	
	@Column(name="qty")
	private double qty=0.0;
	
	@ManyToOne
	@JoinColumn(name="product_variant_id",referencedColumnName="product_variant_id")
	private ProductVariantVo productVariantVo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "material_issue_id", referencedColumnName = "material_issue_id")
	private MaterialIssueVo materialIssueVo;
	
	@Column(name="design_no",length=50)
	private String designNo;
	
	@Column(name="bale_no",length=50)
	private String baleNo;
	
	@Column(name="return_qty")
	private double returnQty=0.0;
}
