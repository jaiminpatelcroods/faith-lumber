	package com.croods.bssgroup.vo.jobwork;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import com.croods.bssgroup.vo.contact.ContactVo;
import com.croods.bssgroup.vo.employee.EmployeeVo;
import com.croods.bssgroup.vo.product.ProductVo;

import lombok.Data;

@Entity
@Table(name = "jobwork")
@DynamicUpdate(value = true)
@Data
public class JobworkVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "jobwork_id", length = 10)
	private long jobworkId;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "jobwork_date")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date jobworkDate;
	
	@Column(name="design_no",length=50)
	private String designNo;
	
	@Column(name="status",length=50)
	private String status;
	
	@Column(name="current_process",length=80)
	private String currentProcess;
	
	@Column(name="previous_process",length=80)
	private String previousProcess;
	
	@Column(name = "prefix", length = 50)
	private String prefix;
	
	@Column(name = "jobwork_no", length = 30)
	private long jobworkNo;
	
	@ManyToOne
	@JoinColumn(name="antri_larring_by",referencedColumnName="employee_id")
	private EmployeeVo antriLarringBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "antri_larring_date")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date antriLarringDate;
	
	@ManyToOne
	@JoinColumn(name="antri_larring_approve_by",referencedColumnName="employee_id")
	private EmployeeVo antriLarringApproveBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	@Column(name = "antri_larring_approve_date")
	private Date antriLarringApproveDate;
	
	@ManyToOne
	@JoinColumn(name="drawing_by",referencedColumnName="employee_id")
	private EmployeeVo drawingBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "drawing_date")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date drawingDate;
	
	@ManyToOne
	@JoinColumn(name="drawing_approve_by",referencedColumnName="employee_id")
	private EmployeeVo drawingApproveBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "drawing_approve_date")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date drawingApproveDate;
	
	@ManyToOne
	@JoinColumn(name="cutting_by",referencedColumnName="employee_id")
	private EmployeeVo cuttingBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cutting_date")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date cuttingDate;
	
	@ManyToOne
	@JoinColumn(name="cutting_approve_by",referencedColumnName="employee_id")
	private EmployeeVo cuttingApproveBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "cutting_approve_date")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date cuttingApproveDate;

	@ManyToOne
	@JoinColumn(name="sort_by",referencedColumnName="employee_id")
	private EmployeeVo sortBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "sort_date")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date sortDate;
	
	@ManyToOne
	@JoinColumn(name="sort_approve_by",referencedColumnName="employee_id")
	private EmployeeVo sortApproveBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "sort_approve_date")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date sortApproveDate;

	//Bal Gaj Related Field
	// In House / Out of Box
	
	@Column(name="bal_gaj_qc",length=50)
	private String balGajQC;
	
	@Column(name="bal_gaj_location",length=50)
	private String balGajLocation;
	
	@Column(name = "bal_gaj_qty", length = 10)
	private double balGajQty;
	
	@ManyToOne
	@JoinColumn(name="bal_gaj_contact_id",referencedColumnName="contact_id")
	private ContactVo balGajContactVo;
	
	@ManyToOne
	@JoinColumn(name="bal_gaj_handover_by",referencedColumnName="employee_id")
	private EmployeeVo balGajHandoverBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bal_gaj_handover_date")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date balGajHandoverDate;
	
	@ManyToOne
	@JoinColumn(name="bal_gaj_received_by",referencedColumnName="employee_id")
	private EmployeeVo balGajReceivedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bal_gaj_received_date")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date balGajReceivedDate;
	
	@Column(name = "bal_gaj_received_qty", length = 10)
	private double balGajReceivedQty;
	
	@ManyToOne
	@JoinColumn(name="bal_gaj_approve_by",referencedColumnName="employee_id")
	private EmployeeVo balGajApproveBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "bal_gaj_approve_date")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date balGajApproveDate;
	
	//Washing Related Field
	
	@Column(name = "washing_qty", length = 10)
	private double washingQty;
	
	@ManyToOne
	@JoinColumn(name="washing_contact_id",referencedColumnName="contact_id")
	private ContactVo washingContactVo;
	
	@ManyToOne
	@JoinColumn(name="washing_handover_by",referencedColumnName="employee_id")
	private EmployeeVo washingHandoverBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "washing_handover_date")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date washingHandoverDate;
	
	@Column(name="washing_receive_status",length=50)
	private String washingReceiveStatus;
	
	@ManyToOne
	@JoinColumn(name="washing_received_by",referencedColumnName="employee_id")
	private EmployeeVo washingReceivedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "washing_received_date")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date washingReceivedDate;
	
	@Column(name = "washing_received_qty", length = 10)
	private double washingReceivedQty;
	
	@Column(name="washing_sample")
	private boolean washingSample;
	
	@ManyToOne
	@JoinColumn(name="washing_sample_received_by",referencedColumnName="employee_id")
	private EmployeeVo washingSampleReceivedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "washing_sample_received_date")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date washingSampleReceivedDate;
	
	@Column(name = "washing_sample_received_qty", length = 10)
	private double washingSampleReceivedQty;
	
	//Pressing
	
	@Column(name = "pressing_qty", length = 10)
	private double pressingQty;
	
	@Column(name="pressing_receive_status",length=50)
	private String pressingReceiveStatus;
	
	@ManyToOne
	@JoinColumn(name="pressing_contact_id",referencedColumnName="contact_id")
	private ContactVo pressingContactVo;
	
	@ManyToOne
	@JoinColumn(name="pressing_handover_by",referencedColumnName="employee_id")
	private EmployeeVo pressingHandoverBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "pressing_handover_date")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date pressingHandoverDate;
	
	@ManyToOne
	@JoinColumn(name="pressing_received_by",referencedColumnName="employee_id")
	private EmployeeVo pressingReceivedBy;
	
	@ManyToOne
	@JoinColumn(name="output_product_id",referencedColumnName="product_id")
	private ProductVo productVo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "pressing_received_date")
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date pressingReceivedDate;
	
	@Column(name = "pressing_received_qty", length = 10)
	private double pressingReceivedQty;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="stitching_qc",length=50)
	private String stitchingQC;

	@Column(name="labeling_finishing_qc",length=50)
	private String labelingFinishingQC;
	
	@Column(name="store_status",length=50)
	private String storeStatus;
	
	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(mappedBy = "jobworkVo", cascade = CascadeType.ALL)
	private List<JobworkInputVo> jobworkInputVos;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(mappedBy = "jobworkVo", cascade = CascadeType.ALL)
	private List<JobworkOutputVo> jobworkOutputVos;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(mappedBy = "jobworkVo",cascade=CascadeType.ALL)
	private List<JobworkFarmoTypeVo> jobworkFarmoTypeVos;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(mappedBy = "jobworkVo", cascade = CascadeType.ALL)
	private List<JobworkFarmoTypeParticularVo> jobworkFarmoTypeParticularVos;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(mappedBy = "jobworkVo", cascade = CascadeType.ALL)
	private List<JobworkProcessVo> jobworkProcessVos;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(mappedBy = "jobworkVo", cascade = CascadeType.ALL)
	private List<JobworkPackageVo> jobworkPackageVos;
}
