package com.croods.bssgroup.vo.sales;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.bssgroup.vo.additionalcharge.AdditionalChargeVo;
import com.croods.bssgroup.vo.tax.TaxVo;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "sales_additional_charge")
@Getter @Setter
public class SalesAdditionalChargeVo {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "sales_additional_charge_id",length=10)
	private long salesAdditionalChargeId;
	
	@Column(name = "amount",length=10)
	private double amount=0.0;
	
	@Column(name = "tax_amount",length=10)
	private double taxAmount=0.0;
	
	@Column(name = "tax_rate",length=10)
	private double taxRate=0.0;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="sales_id",referencedColumnName="sales_id")
	private SalesVo salesVo;
	
	@ManyToOne
	@JoinColumn(name="tax_id",referencedColumnName="tax_id")
	private TaxVo taxVo;
	
	@ManyToOne
	@JoinColumn(name="additional_charge_id",referencedColumnName="additional_charge_id")
	private AdditionalChargeVo additionalChargeVo;
}
