package com.croods.bssgroup.vo.location;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="loc_city")
public class CityVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "city_id", length = 10)
	private long cityId;
	
	@Column(name="city_code",length=20)
	private String cityCode;
	
	@Column(name="city_name",length=50)
	private String cityName;
	
	@Column(name="state_code",length=20)
	private String stateCode;
	
	@Column(name="countries_code",length=20)
	private String countriesCode;

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getCountriesCode() {
		return countriesCode;
	}

	public void setCountriesCode(String countriesCode) {
		this.countriesCode = countriesCode;
	}

	public long getCityId() {
		return cityId;
	}

	public void setCityId(long cityId) {
		this.cityId = cityId;
	}
}
