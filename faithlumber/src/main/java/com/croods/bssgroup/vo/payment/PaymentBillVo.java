package com.croods.bssgroup.vo.payment;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.croods.bssgroup.vo.purchase.PurchaseVo;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "payment_bill")
@Getter @Setter
public class PaymentBillVo {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "payment_bill_id",length=10)
	private long paymentBillId;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="payment_id",referencedColumnName="payment_id")
	private PaymentVo paymentVo;
	
	@ManyToOne
	@JoinColumn(name="purchase_id",referencedColumnName="purchase_id")
	private PurchaseVo purchaseVo;
	
	@Column(name = "payment",length=15)
	private double payment;

	@Column(name = "kasar_amount",length=10)
	private double kasar=0.0;
	
	@Transient
	private double oldPyament;

	@Transient
	private double oldKasar;
}
