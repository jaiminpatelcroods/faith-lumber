package com.croods.bssgroup.vo.productionmaster;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.bssgroup.vo.farmoparticular.FarmoParticularVo;
import com.croods.bssgroup.vo.farmotype.FarmoTypeVo;

import lombok.Data;

@Entity
@Table(name="production_type")
@Data
public class ProductionTypeVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "production_type_id", length = 10)
	private long productionTypeId;
		
	@Column(name="production_type_name")
	private String productionTypeName;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
		
}
