package com.croods.bssgroup.vo.sales;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import com.croods.bssgroup.vo.contact.ContactVo;
import com.croods.bssgroup.vo.employee.EmployeeVo;
import com.croods.bssgroup.vo.paymentterm.PaymentTermVo;

import lombok.Data;

@Entity
@Table(name = "sales")
@DynamicUpdate(value = true)
@Data
public class SalesVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sales_id", length = 10)
	private long salesId;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "sales_date")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date salesDate;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "due_date")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date dueDate;
	
	@Column(name = "prefix", length = 50)
	private String prefix;
	
	@Column(name = "sales_no", length = 30)
	private long salesNo;
	
	@Column(name="sez",columnDefinition="int default 0")
	private int sez;
	
	@Column(name="type",length=50)
	private String type;
	
	@Column(name="tax_type",columnDefinition="int default 0")
	private int taxType;
	
	@Column(name = "total", length = 10)
	private double total;
	
	@Column(name = "roundoff", length = 10, columnDefinition = "float default 0.0")
	private float roundoff;
	
	@Column(name = "paid_amount", length = 10)
	private double paidAmount;
	
	@ManyToOne
	@JoinColumn(name="contact_id",referencedColumnName="contact_id")
	private ContactVo contactVo;
	
	@ManyToOne
	@JoinColumn(name="payment_term_id",referencedColumnName="payment_term_id")
	private PaymentTermVo paymentTermsVo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="parent_id",referencedColumnName="sales_id")
	private SalesVo salesVo;
	
	@Column(name="note",length=300,columnDefinition="text")
	private String note;
	
	@Column(name="status",length=50)
	private String status;
	
	/**Billing Address**/
	
	@Column(name="billing_compny_name")
	private String billingCompanyName;
	
	@Column(name="billing_address_line_1",length=300,columnDefinition="text")
	private String billingAddressLine1;
	
	@Column(name="billing_address_line_2",length=300,columnDefinition="text")
	private String billingAddressLine2;
	
	@Column(name="billing_countries_code",length=50)
	private String billingCountriesCode;
	
	@Transient
	String billingCountriesName;
	
	@Column(name="billing_state_code",length=50)
	private String billingStateCode;
	
	@Transient
	String billingStateName;
	
	@Column(name="billing_city_code",length=50)
	private String billingCityCode;
	
	@Transient
	String billingCityName;
	
	@Column(name="billing_pin_code",length=6)
	private String billingPinCode;
	
	/**Shipping Address**/
	
	@Column(name="shipping_compny_name")
	private String shippingCompanyName;
	
	@Column(name="shipping_address_line_1",length=300,columnDefinition="text")
	private String shippingAddressLine1;
	
	@Column(name="shipping_address_line_2",length=300,columnDefinition="text")
	private String shippingAddressLine2;
	
	@Column(name="shipping_countries_code",length=50)
	private String shippingCountriesCode;
	
	@Transient
	String shippingCountriesName;
	
	@Column(name="shipping_state_code",length=50)
	private String shippingStateCode;
	
	@Transient
	String shippingStateName;
	
	@Column(name="shipping_city_code",length=50)
	private String shippingCityCode;
	
	@Transient
	String shippingCityName;
	
	@Column(name="shipping_pin_code",length=6)
	private String shippingPinCode;
	
	@Column(name="terms_and_condition_ids",length=80)
	String termsAndConditionIds;
	
	@ManyToOne
	@JoinColumn(name="assigned_employee",referencedColumnName="employee_id")
	private EmployeeVo assignedEmployee;
	
	@ManyToOne
	@JoinColumn(name="agent_id",referencedColumnName="contact_id")
	private ContactVo contactAgentVo;
	
	@ManyToOne
	@JoinColumn(name="transport_id",referencedColumnName="contact_id")
	private ContactVo contactTransportVo;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "salesVo", cascade = CascadeType.ALL)
	private List<SalesItemVo> salesItemVos;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "salesVo",cascade = CascadeType.ALL)
	private List<SalesAdditionalChargeVo>  salesAdditionalChargeVos;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
		
	@Column(name="vehicle_number",length=50)
	private String vehicleNo;
	
}
