package com.croods.bssgroup.vo.production;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.croods.bssgroup.vo.product.ProductVariantVo;

import lombok.Data;

@Entity
@Table(name = "production_item")
@Data
public class ProductionItemVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "production_item_id", length = 10)
	private long productionItemId;
	
	@Column(name="qty")
	private double qty=0.0;
	
	@ManyToOne
	@JoinColumn(name="product_variant_id",referencedColumnName="product_variant_id")
	private ProductVariantVo productVariantVo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "production_id", referencedColumnName = "production_id")
	private ProductionVo productionVo;
	
	@Transient
	private double oldProductionQty;
	
	@Transient
	private double soTotalQty=0.0;
	
	@Transient
	private double producedTotalQty=0.0;
	
	@Column(name="sales_item_id",length=10)
	private long salesItemId;
}
