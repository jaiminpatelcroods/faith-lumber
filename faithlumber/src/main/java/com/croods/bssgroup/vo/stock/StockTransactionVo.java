package com.croods.bssgroup.vo.stock;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.bssgroup.vo.product.ProductVariantVo;

import lombok.Data;

@Entity
@Table(name="stock_transaction")
@Data
public class StockTransactionVo {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "stock_transaction_id", length = 10)
	private long stockTransactionId;
	
	@Column(name="stock_transaction_date")
	Date stockTransactionDate;

	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="type",length=100)
	private String type;
	
	@Column(name="type_id",length=100)
	private long typeId;
	
	@Column(name="in_quantity",length=50)
	private double inQuantity;
	
	@Column(name="out_quantity",length=50)
	private double outQuantity;
	
	@ManyToOne
	@JoinColumn(name="product_variant_id",referencedColumnName="product_variant_id")
	private ProductVariantVo productVariantVo;
	
	@Column(name="year_interval",length=100)
	private String yearInterval;
	
	@Column(name="product_price",length=50)
	private double productPrice;

	@Column(name="description",length=100)
	private String description;
	
	@Column(name="design_no",length=100)
	private String designNo;
	
	@Column(name="bale_no",length=50)
	private String baleNo;
	
}
