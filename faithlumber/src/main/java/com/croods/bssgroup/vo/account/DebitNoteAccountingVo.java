package com.croods.bssgroup.vo.account;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "debit_note_accounting")
public class DebitNoteAccountingVo  implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "debit_note_account_id", length = 10)
	private long debitNoteAccountId;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name = "transaction_date")
	private Date transactionDate;
	
	@Column(name = "amount", length =30)
	private double amount;
	
	@Column(name="description",length=300,columnDefinition="text")
	private String description;
	
	@Column(name = "voucher_no", length = 30)
	private long voucherNo;
	
	@Column(name = "prefix", length = 50)
	private String prefix;
	
	@ManyToOne
	@JoinColumn(name = "from_account_id", referencedColumnName = "account_custom_id")
	private AccountCustomVo fromAccountCustomVo;
	
	@ManyToOne
	@JoinColumn(name = "to_account_id", referencedColumnName = "account_custom_id")
	private AccountCustomVo toAccountCustomVo;
		
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public long getDebitNoteAccountId() {
		return debitNoteAccountId;
	}

	public void setDebitNoteAccountId(long debitNoteAccountId) {
		this.debitNoteAccountId = debitNoteAccountId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getVoucherNo() {
		return voucherNo;
	}

	public void setVoucherNo(long voucherNo) {
		this.voucherNo = voucherNo;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}


	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getAlterBy() {
		return alterBy;
	}

	public void setAlterBy(long alterBy) {
		this.alterBy = alterBy;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public AccountCustomVo getFromAccountCustomVo() {
		return fromAccountCustomVo;
	}

	public void setFromAccountCustomVo(AccountCustomVo fromAccountCustomVo) {
		this.fromAccountCustomVo = fromAccountCustomVo;
	}

	public AccountCustomVo getToAccountCustomVo() {
		return toAccountCustomVo;
	}

	public void setToAccountCustomVo(AccountCustomVo toAccountCustomVo) {
		this.toAccountCustomVo = toAccountCustomVo;
	}
	
	
}

