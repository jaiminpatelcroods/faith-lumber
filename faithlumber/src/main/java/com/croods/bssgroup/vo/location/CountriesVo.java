package com.croods.bssgroup.vo.location;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="loc_countries")
public class CountriesVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "countries_id", length = 10)
	private long countriesId;
	
	@Column(name="countries_code",length=20)
	private String countriesCode;
	
	@Column(name="countries_name",length=50)
	private String countriesName;

	@Column(name="phone_code",length=20)
	private String phoneCode;

	public long getCountriesId() {
		return countriesId;
	}

	public void setCountriesId(long countriesId) {
		this.countriesId = countriesId;
	}

	public String getCountriesCode() {
		return countriesCode;
	}

	public void setCountriesCode(String countriesCode) {
		this.countriesCode = countriesCode;
	}

	public String getCountriesName() {
		return countriesName;
	}

	public void setCountriesName(String countriesName) {
		this.countriesName = countriesName;
	}

	public String getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}
	
}
