package com.croods.bssgroup.vo.jobwork;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.croods.bssgroup.vo.employee.EmployeeVo;
import com.croods.bssgroup.vo.product.ProductVariantVo;
import com.croods.bssgroup.vo.productionmaster.ProductionProcessVo;

import lombok.Data;

@Entity
@Table(name = "jobwork_process")
@DynamicUpdate(value = true)
@Data
public class JobworkProcessVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "jobwork_process_id", length = 10)
	private long jobworkProcessId;
	
	@ManyToOne
	@JoinColumn(name="production_process_id",referencedColumnName="production_process_id")
	private ProductionProcessVo productionProcessVo;
	
	@Column(name="process",length=50)
	private String process;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobwork_id", referencedColumnName = "jobwork_id")
	JobworkVo jobworkVo;
	
	@ManyToOne
	@JoinColumn(name="employee_id",referencedColumnName="employee_id")
	private EmployeeVo employeeVo;
	
	@Column(name = "qty")
	private float qty;
	
	@Column(name = "rate")
	private double rate;
	
	@Column(name = "qc_qty")
	private float qcQty;
	
	@Column(name = "qc_pass_qty")
	private float qcPassQty;
	
	@Column(name = "qc_fail_qty")
	private float qcFailQty;
	
	@Column(name="remark", length=100)
	private String remark;
	
	@ManyToOne
	@JoinColumn(name="qc_by",referencedColumnName="employee_id")
	private EmployeeVo qcBy;

}
