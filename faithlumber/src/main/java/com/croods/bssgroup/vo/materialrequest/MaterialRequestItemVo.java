package com.croods.bssgroup.vo.materialrequest;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.bssgroup.vo.product.ProductVariantVo;

import lombok.Data;

@Entity
@Table(name = "material_request_item")
@Data
public class MaterialRequestItemVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "material_request_item_id", length = 10)
	private long materialRequestItemId;
	
	@Column(name="qty")
	private double qty=0.0;
	
	@ManyToOne
	@JoinColumn(name="product_variant_id",referencedColumnName="product_variant_id")
	private ProductVariantVo productVariantVo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "material_request_id", referencedColumnName = "material_request_id")
	private MaterialRequestVo materialRequestVo;
}
