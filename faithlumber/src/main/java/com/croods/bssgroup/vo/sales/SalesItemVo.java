package com.croods.bssgroup.vo.sales;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.bssgroup.vo.product.ProductVariantVo;
import com.croods.bssgroup.vo.tax.TaxVo;

import lombok.Data;

@Entity
@Table(name = "sales_item")
@Data
public class SalesItemVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sales_item_id", length = 10)
	private long salesItemId;
	
	@ManyToOne
	@JoinColumn(name="product_variant_id",referencedColumnName="product_variant_id")
	private ProductVariantVo productVariantVo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sales_id", referencedColumnName = "sales_id")
	private SalesVo salesVo;
	
	@Column(name = "qty",length=10)
	private float qty;
	
	@Column(name = "product_description",length=50)
	private String productDescription;
	
	@Column(name = "price",length=10)
	private double price = 0.0;
	
	@Column(name = "tax_amount",length=10)
	private double taxAmount=0.0;
	
	@Column(name = "tax_rate",length=10)
	private double taxRate=0.0;
	
	@ManyToOne
	@JoinColumn(name="tax_id",referencedColumnName="tax_id")
	private TaxVo taxVo;
	
	@Column(name="discount",length=6)
	private double discount=0.0;
	
	@Column(name="discount_type",length=20)
	String discountType;
	
	@Column(name="design_no",length=50)
	private String designNo;
	
	@Column(name="bale_no",length=50)
	private String baleNo;
	
	@Column(name = "produced_qty",length=10)
	private double producedQty=0.0;
	
	@Column(name = "delivered_qty",length=10)
	private double deliveredQty=0.0;
}
