package com.croods.bssgroup.vo.lead;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.croods.bssgroup.vo.employee.EmployeeVo;

import lombok.Data;

@Entity
@Table(name="lead_activity")
@Data
public class LeadActivityVo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="lead_activity_id",length=10)
	private long leadActivityId;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="mode_of_communication",length=50)
	private String modeOfCommunication;
	
	@Column(name="remark",length=500)
	private String remark;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "next_schedule_date")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date nextScheduleDate;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="lead_id",referencedColumnName="lead_id")
	private LeadVo leadVo;
}
