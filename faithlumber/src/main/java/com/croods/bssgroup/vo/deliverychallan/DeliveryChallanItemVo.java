package com.croods.bssgroup.vo.deliverychallan;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.bssgroup.vo.product.ProductVariantVo;

import lombok.Data;

@Entity
@Table(name = "delivery_challan_item")
@Data
public class DeliveryChallanItemVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "delivery_challan_item_id", length = 10)
	private long deliveryChallanItemId;
	
	@ManyToOne
	@JoinColumn(name="product_variant_id",referencedColumnName="product_variant_id")
	private ProductVariantVo productVariantVo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "delivery_challan_id", referencedColumnName = "delivery_challan_id")
	private DeliveryChallanVo deliveryChallanVo;
	
	@Column(name = "pack")
	private float pack;
	
	@Column(name = "qty",length=10)
	private double qty;
	
	@Column(name = "qc_qty",length=10, columnDefinition="float default 0.0")
	private float qcQty;
	
	@Column(name="design_no",length=50)
	private String designNo;
	
	@Column(name="bale_no",length=50)
	private String baleNo;
}
