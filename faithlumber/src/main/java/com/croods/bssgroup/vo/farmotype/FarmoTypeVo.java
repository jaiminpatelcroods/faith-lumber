package com.croods.bssgroup.vo.farmotype;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.croods.bssgroup.vo.farmoparticular.FarmoParticularVo;
import com.croods.bssgroup.vo.purchase.PurchaseItemVo;

import lombok.Data;

@Entity
@Table(name="farmo_type")
@Data
public class FarmoTypeVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "farmo_type_id", length = 10)
	private long farmoTypeId;
	
	@Column(name="farmo_type_name")
	private String farmoTypeName;
	
	@Column(name="front_length",columnDefinition = "float default 0.0")
	private float frontLength;
	
	@Column(name="front_seat",columnDefinition = "float default 0.0")
	private float frontSeat;
	
	@Column(name="front_thai",columnDefinition = "float default 0.0")
	private float frontThai;
	
	@Column(name="front_langot",columnDefinition = "float default 0.0")
	private float frontLangot;
	
	@Column(name="front_knee",columnDefinition = "float default 0.0")
	private float frontKnee;
	
	@Column(name="front_mori_munda",columnDefinition = "float default 0.0")
	private float frontMoriMunda;
	
	@Column(name="front_jati",columnDefinition = "float default 0.0")
	private float frontJati;
	
	@Column(name="back_length",columnDefinition = "float default 0.0")
	private float backLength;
	
	@Column(name="back_seat",columnDefinition = "float default 0.0")
	private float backSeat;
	
	@Column(name="back_thai",columnDefinition = "float default 0.0")
	private float backThai;
	
	@Column(name="back_langot",columnDefinition = "float default 0.0")
	private float backLangot;
	
	@Column(name="back_knee",columnDefinition = "float default 0.0")
	private float backKnee;
	
	@Column(name="back_mori_munda",columnDefinition = "float default 0.0")
	private float backMoriMunda;
	
	@Column(name="back_jati",columnDefinition = "float default 0.0")
	private float backJati;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "farmoTypeVo", cascade = CascadeType.ALL)
	private List<FarmoTypeParticularVo> farmoTypeParticularVos;
	 
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
}
