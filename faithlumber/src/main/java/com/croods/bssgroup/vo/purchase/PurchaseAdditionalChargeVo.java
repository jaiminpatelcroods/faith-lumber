package com.croods.bssgroup.vo.purchase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.bssgroup.vo.additionalcharge.AdditionalChargeVo;
import com.croods.bssgroup.vo.tax.TaxVo;

@Entity
@Table(name = "purchase_additional_charge")
public class PurchaseAdditionalChargeVo {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "purchase_additional_charge_id",length=10)
	private long purchaseAdditionalChargeId;
	
	@Column(name = "amount",length=10)
	private double amount=0.0;
	
	@Column(name = "tax_amount",length=10)
	private double taxAmount=0.0;
	
	@Column(name = "tax_rate",length=10)
	private double taxRate=0.0;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="purchase_id",referencedColumnName="purchase_id")
	private PurchaseVo purchaseVo;
	
	@ManyToOne
	@JoinColumn(name="tax_id",referencedColumnName="tax_id")
	private TaxVo taxVo;
	
	@ManyToOne
	@JoinColumn(name="additional_charge_id",referencedColumnName="additional_charge_id")
	private AdditionalChargeVo additionalChargeVo;

	public long getPurchaseAdditionalChargeId() {
		return purchaseAdditionalChargeId;
	}

	public void setPurchaseAdditionalChargeId(long purchaseAdditionalChargeId) {
		this.purchaseAdditionalChargeId = purchaseAdditionalChargeId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getTaxAmount() {
		return taxAmount;
	}

	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public TaxVo getTaxVo() {
		return taxVo;
	}

	public void setTaxVo(TaxVo taxVo) {
		this.taxVo = taxVo;
	}

	public AdditionalChargeVo getAdditionalChargeVo() {
		return additionalChargeVo;
	}

	public void setAdditionalChargeVo(AdditionalChargeVo additionalChargeVo) {
		this.additionalChargeVo = additionalChargeVo;
	}

	public PurchaseVo getPurchaseVo() {
		return purchaseVo;
	}

	public void setPurchaseVo(PurchaseVo purchaseVo) {
		this.purchaseVo = purchaseVo;
	}
	
}
