package com.croods.bssgroup.vo.receipt;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.croods.bssgroup.vo.sales.SalesVo;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "receipt_bill")
@Getter @Setter
public class ReceiptBillVo {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "receipt_bill_id", length = 10)
	private long receiptBillId;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "receipt_id", referencedColumnName = "receipt_id")
	private ReceiptVo receiptVo;

	@ManyToOne
	@JoinColumn(name = "sales_id", referencedColumnName = "sales_id")
	private SalesVo salesVo;

	@Column(name = "payment", length = 15)
	private double payment;

	@Column(name = "kasar_amount", length = 10)
	private double kasar = 0.0;
	
	@Transient
	private double oldPyament;
	
	@Transient
	private double oldKasar;
}
