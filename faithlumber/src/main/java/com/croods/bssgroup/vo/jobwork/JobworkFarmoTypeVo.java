package com.croods.bssgroup.vo.jobwork;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.croods.bssgroup.vo.farmotype.FarmoTypeVo;

import lombok.Data;

@Entity
@Table(name = "jobwork_farmo_type")
@DynamicUpdate(value = true)
@Data
public class JobworkFarmoTypeVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "jobwork_farmo_type_id", length = 10)
	private long jobworkFarmoTypeId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobwork_id", referencedColumnName = "jobwork_id")
	JobworkVo jobworkVo;
	
	@Column(name="process",length=100)
	private String process;
	
	@Column(name="front_length",columnDefinition = "float default 0.0")
	private float frontLength;
	
	@Column(name="front_seat",columnDefinition = "float default 0.0")
	private float frontSeat;
	
	@Column(name="front_thai",columnDefinition = "float default 0.0")
	private float frontThai;
	
	@Column(name="front_langot",columnDefinition = "float default 0.0")
	private float frontLangot;
	
	@Column(name="front_knee",columnDefinition = "float default 0.0")
	private float frontKnee;
	
	@Column(name="front_mori_munda",columnDefinition = "float default 0.0")
	private float frontMoriMunda;
	
	@Column(name="front_jati",columnDefinition = "float default 0.0")
	private float frontJati;
	
	@Column(name="back_length",columnDefinition = "float default 0.0")
	private float backLength;
	
	@Column(name="back_seat",columnDefinition = "float default 0.0")
	private float backSeat;
	
	@Column(name="back_thai",columnDefinition = "float default 0.0")
	private float backThai;
	
	@Column(name="back_langot",columnDefinition = "float default 0.0")
	private float backLangot;
	
	@Column(name="back_knee",columnDefinition = "float default 0.0")
	private float backKnee;
	
	@Column(name="back_mori_munda",columnDefinition = "float default 0.0")
	private float backMoriMunda;
	
	@Column(name="back_jati",columnDefinition = "float default 0.0")
	private float backJati;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "farmo_type_id", referencedColumnName = "farmo_type_id")
	private FarmoTypeVo farmoTypeVo;
}
