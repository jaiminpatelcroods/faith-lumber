package com.croods.bssgroup.vo.transaction;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.croods.bssgroup.vo.account.AccountCustomVo;
import com.croods.bssgroup.vo.account.AccountGroupVo;

@Entity
@Table(name = "transaction")
public class TransactionVo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "transaction_id",length=10)
	private long transactionId;
	
	@Column(name="transaction_date")
	Date transactionDate;
	
	@ManyToOne
	@JoinColumn(name="account_group_id",referencedColumnName="account_group_id")
	private AccountGroupVo accountGroupVo;
	
	@ManyToOne
	@JoinColumn(name = "account_custom_id", referencedColumnName = "account_custom_id")
	private AccountCustomVo accountCustomVo;
	
	@Column(name="voucher_type",length=100)
	private String voucherType;
	
	@Column(name="voucher_id",length=10)
	private long voucherId;
	
	@Column(name="voucher_no",length=100)
	private String voucherNo;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="debit_amount",length=50)
	private double debitAmount;
	
	@Column(name="credit_amount",length=50)
	private double creditAmount;

	@Column(name="description",length=100)
	private String description;

	@Transient
	double closingAmount = 0.0;

	public long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(long transactionId) {
		this.transactionId = transactionId;
	}

	public Date getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	public AccountGroupVo getAccountGroupVo() {
		return accountGroupVo;
	}

	public void setAccountGroupVo(AccountGroupVo accountGroupVo) {
		this.accountGroupVo = accountGroupVo;
	}

	public AccountCustomVo getAccountCustomVo() {
		return accountCustomVo;
	}

	public void setAccountCustomVo(AccountCustomVo accountCustomVo) {
		this.accountCustomVo = accountCustomVo;
	}

	public String getVoucherType() {
		return voucherType;
	}

	public void setVoucherType(String voucherType) {
		this.voucherType = voucherType;
	}

	public long getVoucherId() {
		return voucherId;
	}

	public void setVoucherId(long voucherId) {
		this.voucherId = voucherId;
	}

	public String getVoucherNo() {
		return voucherNo;
	}

	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}

	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public double getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(double debitAmount) {
		this.debitAmount = debitAmount;
	}

	public double getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getClosingAmount() {
		return closingAmount;
	}

	public void setClosingAmount(double closingAmount) {
		this.closingAmount = closingAmount;
	}
	
}
