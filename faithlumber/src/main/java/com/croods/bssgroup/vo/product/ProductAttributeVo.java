package com.croods.bssgroup.vo.product;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "product_attribute")
@Data
public class ProductAttributeVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_attribute_id", length = 10)
	private long productAttributeId;
	
	@ManyToOne(fetch =FetchType.LAZY)
	@JoinColumn(name = "product_id", referencedColumnName = "product_id")
	private ProductVo productVo;
	
	@Column(name = "option_name", length = 150)
	private String optionName;
	
	@Column(name = "option_values", length = 150)
	private String optionValues;
	
	@Column(name = "position", length = 150)
	private String position;
}
