package com.croods.bssgroup.vo.gate;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicUpdate;
import org.springframework.format.annotation.DateTimeFormat;

import com.croods.bssgroup.vo.contact.ContactVo;
import com.croods.bssgroup.vo.sales.SalesVo;

import lombok.Data;

@Entity
@Table(name = "gate")
@DynamicUpdate(value = true)
@Data
public class GateVo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "gate_id", length = 10)
	private long gateId;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "gate_date")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date gateDate;
	
	@ManyToOne
	@JoinColumn(name="transport_id",referencedColumnName="contact_id")
	private ContactVo contactTransportVo;
	
	@Column(name="vehicle_number",length=50)
	private String vehicleno;
	
	@ManyToOne
	@JoinColumn(name="sales_id",referencedColumnName="sales_id")
	private SalesVo salesVo;
	
	@Column(name="remark",length=500)
	private String remark;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;

	
}
