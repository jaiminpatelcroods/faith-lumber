package com.croods.bssgroup.vo.employee;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="employee_contact")
@Getter @Setter
public class EmployeeContactVo {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="employee_contact_id",length=10)
	private long employeeContactId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="employee_id",referencedColumnName="employee_id")
	private EmployeeVo employeeVo;
	
	@Column(name="name",length=100)
	private String name;
	
	@Column(name="mobileno", length=12)
	private String mobileno;
	
	@Column(name="relation", length=50)
	private String relation;

}
