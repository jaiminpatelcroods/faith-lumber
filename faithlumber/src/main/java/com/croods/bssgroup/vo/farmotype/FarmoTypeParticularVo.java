package com.croods.bssgroup.vo.farmotype;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.croods.bssgroup.vo.farmoparticular.FarmoParticularVo;
import com.croods.bssgroup.vo.purchase.PurchaseVo;

import lombok.Data;

@Entity
@Table(name="farmo_type_particulars")
@Data
public class FarmoTypeParticularVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "farmo_type_particulars_id", length = 10)
	private long farmoTypeParticularsId;
	
	@ManyToOne
	@JoinColumn(name="farmo_particular_id",referencedColumnName="farmo_particular_id")
	FarmoParticularVo farmoParticularVo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "farmo_type_id", referencedColumnName = "farmo_type_id")
	private FarmoTypeVo farmoTypeVo;
	
	@Column(name="farmo_particular_measurement")
	private float farmoParticularMeasurement;
	 
	
}
