package com.croods.bssgroup.vo.sms;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;


@Entity
@Table(name = "sms")
@DynamicUpdate(value = true)
@Data
public class SmsVo {
	/*
	 * public SmsVo(String reciever, String message2, String senderId) {
	 * this.mobileno = reciever; this.message=message2; }
	 */

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sms_id", length = 10)
	private long smsId;
	
	@Column(name="mobileno", length=1000)
	private String mobileno;
	
	@Column(name="message",length=900)
	private String message;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="created_on",updatable=false)
	@DateTimeFormat(pattern="dd/MM/yyyy hh:mm a")
	private Date createdOn;


}