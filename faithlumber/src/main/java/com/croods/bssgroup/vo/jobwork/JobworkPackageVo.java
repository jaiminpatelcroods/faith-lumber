package com.croods.bssgroup.vo.jobwork;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import com.croods.bssgroup.vo.product.ProductVariantVo;

import lombok.Data;

@Entity
@Table(name = "jobwork_package")
@DynamicUpdate(value = true)
@Data
public class JobworkPackageVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "jobwork_package_id", length = 10)
	private long jobworkPackageId;
	
	@Column(name="no_of_pack")
	private long noOfPack;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jobwork_id", referencedColumnName = "jobwork_id")
	JobworkVo jobworkVo;
	
	@ManyToOne
	@JoinColumn(name="package_variant_id",referencedColumnName="product_variant_id")
	private ProductVariantVo productVariantVo;
	
	@Column(name = "parent_variant_ids") //package variants ',' separated
	private String parentVariantIds;
	
	@Column(name = "variant_quantites") //package variants qty ',' separated
	private String variantQuantites;
	
}
