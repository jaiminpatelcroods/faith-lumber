package com.croods.bssgroup.vo.materialissue;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import com.croods.bssgroup.repository.materialrequest.MaterialRequestItemRepository;
import com.croods.bssgroup.vo.employee.EmployeeVo;
import com.croods.bssgroup.vo.jobwork.JobworkVo;
import com.croods.bssgroup.vo.materialrequest.MaterialRequestVo;

import lombok.Data;

@Entity
@Table(name = "material_issue")
@Data
@DynamicUpdate(value = true)
public class MaterialIssueVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "material_issue_id", length = 10)
	private long materialIssueId;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "issue_date")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date issueDate;
	
	@Column(name = "prefix", length = 50)
	private String prefix;
	
	@Column(name = "issue_no", length = 30)
	private long issueNo;
	
	@Column(name="note",length=300,columnDefinition="text")
	private String note;
	
	@ManyToOne
	@JoinColumn(name="receivedby_id",referencedColumnName="employee_id")
	private EmployeeVo receivedBy;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="jobwork_id",referencedColumnName="jobwork_id")
	private JobworkVo jobworkVo;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="material_request_id",referencedColumnName="material_request_id")
	private MaterialRequestVo materialRequestVo;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "materialIssueVo", cascade = CascadeType.ALL)
	private List<MaterialIssueItemVo> materialIssueItemVos;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
}
