package com.croods.bssgroup.vo.journalvoucher;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "journal_voucher")
public class JournalVoucherVo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "journal_voucher_id", length = 10)
	private long journalVoucherId;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "voucher_date")
	private Date voucherDate;
	
	@Column(name = "total", length =30)
	private double total;
	
	@Column(name="description",length=300,columnDefinition="text")
	private String description;
	
	@Column(name = "prefix", length = 50)
	private String prefix;
	
	@Column(name = "voucher_no", length = 30)
	private long voucherNo;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "journalVoucherVo", cascade = CascadeType.ALL)
	private List<JournalVoucherAccountVo> journalVoucherAccountVos;

	public long getJournalVoucherId() {
		return journalVoucherId;
	}

	public void setJournalVoucherId(long journalVoucherId) {
		this.journalVoucherId = journalVoucherId;
	}

	public Date getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public long getVoucherNo() {
		return voucherNo;
	}

	public void setVoucherNo(long voucherNo) {
		this.voucherNo = voucherNo;
	}

	public long getBranchId() {
		return branchId;
	}

	public void setBranchId(long branchId) {
		this.branchId = branchId;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getAlterBy() {
		return alterBy;
	}

	public void setAlterBy(long alterBy) {
		this.alterBy = alterBy;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public int getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

	public List<JournalVoucherAccountVo> getJournalVoucherAccountVos() {
		return journalVoucherAccountVos;
	}

	public void setJournalVoucherAccountVos(List<JournalVoucherAccountVo> journalVoucherAccountVos) {
		this.journalVoucherAccountVos = journalVoucherAccountVos;
	}
}
