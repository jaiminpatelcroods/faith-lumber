package com.croods.bssgroup.vo.receipt;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import com.croods.bssgroup.vo.bank.BankVo;
import com.croods.bssgroup.vo.contact.ContactVo;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "receipt")
@DynamicUpdate(value = true)
@Getter @Setter
public class ReceiptVo {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "receipt_id",length=10)
	private long receiptId;
	
	@ManyToOne
	@JoinColumn(name="contact_id",referencedColumnName="contact_id")
	private ContactVo contactVo;
	
	@ManyToOne
	@JoinColumn(name="bank_id",referencedColumnName="bank_id")
	private BankVo bankVo;
	
	@Column(name="payment_mode",length=50)
	private String paymentMode;
	
	@Column(name="type",length=50)
	private String type;
	
	@Column(name="description",length=300,columnDefinition="text")
	private String description;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "receipt_date")
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date receiptDate;
	
	@Column(name = "receipt_no",length=10)
	private long receiptNo;
		
	@Column(name = "total_payment",length=10)
	private double totalPayment;
	
	@Column(name = "prefix",length=50)
	private String prefix;
	
	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany(mappedBy = "receiptVo", cascade = CascadeType.ALL)
	List<ReceiptBillVo> receiptBillVos;
	
	@Column(name="company_id",length=10,updatable=false)
	private long companyId;	
	
	@Column(name="branch_id",length=10,updatable=false)
	private long branchId;	
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
}
