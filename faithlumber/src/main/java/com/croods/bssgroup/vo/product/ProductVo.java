package com.croods.bssgroup.vo.product;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.croods.bssgroup.vo.brand.BrandVo;
import com.croods.bssgroup.vo.category.CategoryVo;
import com.croods.bssgroup.vo.tax.TaxVo;
import com.croods.bssgroup.vo.unitofmeasurement.UnitOfMeasurementVo;

import lombok.Data;

@Entity
@Table(name = "product")
@Data
public class ProductVo implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_id", length = 10)
	private long productId;
	
	@Column(name = "have_variation", length = 1, columnDefinition = "int default 0")
	private int haveVariation;
	
	@Column(name = "have_design_no", columnDefinition = "int default 0")
	private int haveDesignno;
	
	@Column(name = "have_bale_no", columnDefinition = "int default 0")
	private int haveBaleno;
	
	@Column(name = "name", length = 150)
	private String name;
	
	@Column(name = "display_name", length = 150)
	private String displayName;
	
	@Column(name = "description", length = 300, columnDefinition = "text")
	private String description;	
	
	@ManyToOne
	@JoinColumn(name="category_id",referencedColumnName="category_id")
	private CategoryVo categoryVo;	
	
	@ManyToOne
	@JoinColumn(name="brand_id",referencedColumnName="brand_id")
	private BrandVo brandVo;
	
	@ManyToOne
	@JoinColumn(name = "sales_tax_id", referencedColumnName = "tax_id")
	private TaxVo salesTaxVo;
	
	@ManyToOne
	@JoinColumn(name = "purchase_tax_id", referencedColumnName = "tax_id")
	private TaxVo purchaseTaxVo;
	
	@Column(name = "sales_tax_included", columnDefinition = "int default 0")
	private int salesTaxIncluded;
	
	@Column(name = "purchase_tax_included", columnDefinition = "int default 0")
	private int purchaseTaxIncluded;
	
	@ManyToOne
	@JoinColumn(name = "measurement_id", referencedColumnName = "measurement_id")
	private UnitOfMeasurementVo unitOfMeasurementVo;
	
	@Column(name = "hsn_code", length = 100)
	private String hsnCode;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(mappedBy = "productVo",cascade = CascadeType.ALL)
	private List<ProductVariantVo> productVariantVos;
	
	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(mappedBy = "productVo",cascade = CascadeType.ALL)
	private List<ProductAttributeVo> productAttributeVos;
	
	@Column(name="branch_id",length=10)
	private long branchId;
	
	@Column(name="company_id",length=10)
	private long companyId;
	
	@Column(name="alterby_id",length=10)
	private long alterBy;
	
	@Column(name="createdby_id",length=10,updatable=false)
	private long createdBy;
	
	@Column(name="is_deleted",length=1,columnDefinition="int default 0")
	private int isDeleted;
	
	@Column(name="created_on",length=50,updatable=false)
	private String createdOn;

	@Column(name="modified_on",length=50)
	private String modifiedOn;
	
	@ManyToOne(fetch =FetchType.LAZY)
	@JoinColumn(name = "parent_id", referencedColumnName = "product_id")
	private ProductVo productVo;
	
	@Column(name="is_package")
	boolean isPackage;
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
