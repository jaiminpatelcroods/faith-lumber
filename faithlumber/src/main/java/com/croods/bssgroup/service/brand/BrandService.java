package com.croods.bssgroup.service.brand;

import java.util.List;

import com.croods.bssgroup.vo.brand.BrandVo;

public interface BrandService {
	
	public void save(BrandVo brandVo);
	
	public BrandVo findByBrandId(long brandId);
	
	public void deleteBrand(long brandId, long alterBy, String modifiedOn);
	
	public List<BrandVo> findByCompanyId(long companyId);

	public void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn);

	public void updateIsDefaultByBrandId(long brandId, long alterBy, int isDefault, String modifiedOn);
	
	boolean isExistBrand(long companyId, String brandName);

	boolean isExistBrand(long companyId, String brandName, long brandId);

}
