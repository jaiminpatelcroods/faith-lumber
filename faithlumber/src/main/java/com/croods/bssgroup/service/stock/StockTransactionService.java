package com.croods.bssgroup.service.stock;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.croods.bssgroup.vo.deliverychallan.DeliveryChallanItemVo;
import com.croods.bssgroup.vo.jobwork.JobworkInputVo;
import com.croods.bssgroup.vo.jobwork.JobworkOutputVo;
import com.croods.bssgroup.vo.jobwork.JobworkPackageVo;
import com.croods.bssgroup.vo.materialissue.MaterialIssueItemVo;
import com.croods.bssgroup.vo.purchase.PurchaseItemVo;
import com.croods.bssgroup.vo.sales.SalesItemVo;

public interface StockTransactionService {

	public void deleteStockTransaction(long branchId, long typeId, String type);
	
	public double getTotalQty(long branchId, long productId, Date startDate, Date endDate, String yearInterval);
	
	public double getVariantQty(long branchId, long productVariantId, Date startDate, Date endDate, String yearInterval);
	
	public void saveStockFromDeliveryChallan(List<DeliveryChallanItemVo> deliveryChallanItemVos, String yearInterval);
	
	public List<Map<String, String>> findAllBaleNoByBranchIdAndProductVariantId(long branchId, long productVariantId, Date startDate, Date endDate, String yearInterval);

	public void saveStockFromJobworkProcessStore(List<JobworkOutputVo> jobworkOutputVos, List<JobworkPackageVo> jobworkPackageVos,
			String yearInterval);

	public void saveStockFromJobworkAntriAndLarring(List<JobworkInputVo> jobworkInputVos, String yearInterval);
	
	public List<Map<String, String>> findAllDesignNoByBranchIdAndProductVariantId(long parseLong, long productVariantId,
			Date parse, Date parse2, String string);
	
	public void saveStockTransactionFromPurchaseBill(List<PurchaseItemVo> purchaseItemVos, String yearInterval);

	public void saveStockTransactionFromPurchaseDebitNote(List<PurchaseItemVo> purchaseItemVos, String yearInterval);

	public double getAllProductTotalQty(long branchId, Date startDate, Date endDate, String yearInterval);

	public void saveStockFromSales(List<SalesItemVo> salesItemVos, String yearInterval);

	public void saveStockFromSalesReturn(List<SalesItemVo> salesItemVos, String yearInterval);

	public List<Map<String, String>> findAllBaleNoByBranchIdAndProductVariantIdAndDesignNo(long branchId,
			long productVariantId, String designNo, Date startDate, Date endDate, String yearInterval);	
	
	public void saveStockFromMaterialIssue(List<MaterialIssueItemVo> materialIssueItemVos, String yearInterval);
	
	public void saveStockFromMaterialReturn(List<MaterialIssueItemVo> materialIssueItemVos, String yearInterval);

}
