package com.croods.bssgroup.service.jobwork;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.repository.jobwork.JobworkPackageRepository;
import com.croods.bssgroup.repository.jobwork.JobworkProcessRepository;
import com.croods.bssgroup.repository.jobwork.JobworkRepository;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.service.stock.StockTransactionService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.jobwork.JobworkPackageVo;
import com.croods.bssgroup.vo.jobwork.JobworkVo;
import com.croods.bssgroup.vo.prefix.PrefixVo;

@Service
@Transactional
public class JobworkServiceImpl implements JobworkService {

	@Autowired
	JobworkRepository jobworkRepository; 
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	JobworkProcessRepository jobworkProcessRepository;
	
	@Autowired
	JobworkPackageRepository jobworkPackageRepository;
	
	@Autowired
	StockTransactionService stockTransactionService;
	
	@Override
	public JobworkVo findByJobworkIdAndBranchId(long jobworkId, long branchId) {
		
		return jobworkRepository.findByJobworkIdAndBranchIdAndIsDeleted(jobworkId,branchId,0);
	}

	@Override
	public void save(JobworkVo jobworkVo) {
		jobworkRepository.save(jobworkVo);
		
	}

	@Override
	public long findMaxJobworkNo(long companyId, long branchId, long userId, String type, String prefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(prefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = jobworkRepository.findMaxJobworkNo(branchId, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
	}

	@Override
	public void deleteJobworkProcessByIdIn(String jobworkProcessIds) {
		jobworkProcessIds=jobworkProcessIds.substring(0, jobworkProcessIds.length()-1);
		List<Long> l= Arrays.asList(jobworkProcessIds.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
		jobworkProcessRepository.deleteJobworkProcessByIdIn(l);
	}

	@Override
	public List<JobworkPackageVo> findJobworkPackageByJobworkId(long jobworkId) {
		return jobworkPackageRepository.findByJobworkVoJobworkId(jobworkId);
	}

	@Override
	public void saveStockTransaction(JobworkVo jobworkVo, String yearInterval, String fromProcess) {
		
		if(fromProcess.equals(Constant.JOBWORK_PROCESS_ANTRI_AND_LARRING)) {
			stockTransactionService.deleteStockTransaction(jobworkVo.getBranchId(), jobworkVo.getJobworkId(), Constant.JOBWORK);
			stockTransactionService.saveStockFromJobworkAntriAndLarring(jobworkVo.getJobworkInputVos(), yearInterval);
		} else if(fromProcess.equals(Constant.JOBWORK_PROCESS_STORE)) {
			stockTransactionService.saveStockFromJobworkProcessStore(jobworkVo.getJobworkOutputVos(), jobworkVo.getJobworkPackageVos(), yearInterval);
		}
			
	}

	@Override
	public DataTablesOutput<JobworkVo> findAll(DataTablesInput input,
			Specification<JobworkVo> additionalSpecification, Specification<JobworkVo> specification) {
		return jobworkRepository.findAll(input, additionalSpecification, specification);
	}

	@Override
	public List<JobworkVo> findByBranchIdAndStatus(long branchId, String status) {
		return jobworkRepository.findByBranchIdAndStatus(branchId, status);
	}

}
