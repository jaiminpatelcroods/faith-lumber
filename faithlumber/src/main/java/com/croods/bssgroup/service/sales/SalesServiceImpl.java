package com.croods.bssgroup.service.sales;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.tomcat.util.bcel.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.repository.sales.SalesAdditionalChargeRepository;
import com.croods.bssgroup.repository.sales.SalesItemRepository;
import com.croods.bssgroup.repository.sales.SalesRepository;
import com.croods.bssgroup.service.account.AccountService;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.service.stock.StockTransactionService;
import com.croods.bssgroup.service.transaction.TransactionService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.util.TransactionAmount;
import com.croods.bssgroup.vo.account.AccountCustomVo;
import com.croods.bssgroup.vo.account.AccountGroupVo;
import com.croods.bssgroup.vo.prefix.PrefixVo;
import com.croods.bssgroup.vo.sales.SalesAdditionalChargeVo;
import com.croods.bssgroup.vo.sales.SalesItemVo;
import com.croods.bssgroup.vo.sales.SalesVo;
import com.croods.bssgroup.vo.transaction.TransactionVo;

@Service
@Transactional
public class SalesServiceImpl implements SalesService {

	@Autowired
	SalesRepository salesRepository;

	@Autowired
	PrefixService prefixService;
	
	@Autowired
	SalesItemRepository salesItemRepository;
	
	@Autowired
	SalesAdditionalChargeRepository salesAdditionalChargeRepository;
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired
	TransactionService transactionService;
	
	@Autowired
	StockTransactionService stockTransactionService;
	
	@Autowired
	AccountService accountCustomService;
	
	@Override
	public DataTablesOutput<SalesVo> findAll(@Valid DataTablesInput input, Object object,
			Specification<SalesVo> specification) {
		return salesRepository.findAll(input, null,specification);
	}

	@Override
	public long findMaxSalesNo(long companyId, long branchId, String type, String defaultPrefix, long userId) {
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(defaultPrefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = salesRepository.findMaxSalesNo(branchId, type, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
	}

	@Override
	public boolean isExistSalesNo(long branchId, String type, String prefix, long salesNo) {
		if(salesRepository.isExistSalesNo(branchId, type, prefix, salesNo) == null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isExistSalesNo(long branchId, String type, String prefix, long salesNo, long salesId) {
		if(salesRepository.isExistSalesNo(branchId, type, prefix, salesNo, salesId) == null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public SalesVo findBySalesIdAndBranchId(long salesId, long branchId) {
		return salesRepository.findBySalesIdAndBranchIdAndIsDeleted(salesId, branchId, 0);
	}

	@Override
	public void deleteSalesItemByIdIn(List<Long> l) {
		salesItemRepository.deleteSalesItemByIdIn(l);
	}

	@Override
	public void deleteSalesAdditionalChargeByIdIn(List<Long> l) {
		salesAdditionalChargeRepository.deleteSalesAdditionalChargeByIdIn(l);
	}

	@Override
	public SalesVo save(SalesVo salesVo) {
		salesVo = salesRepository.saveAndFlush(salesVo);
		entityManager.refresh(salesVo);
		return salesVo;
	}

	@Override
	public void deleteSales(long branchId, long salesId, String type) {
		
		if(type.equals(Constant.SALES_INVOICE) || type.equals(Constant.SALES_CREDIT_NOTE)) {
			stockTransactionService.deleteStockTransaction(branchId, salesId, "sales");
			transactionService.deleteTransaction(branchId, salesId, type);
		}
		
		salesRepository.deleteSales(salesId);
	}

	@Override
	public List<Map<Double, String>> getLast15DaySales(long branchId, String type) {
		return salesRepository.getLast15DaySales(branchId, type);
	}

	@Override
	public List<Map<String, String>> getSalesVSPurchase(long branchId, Date startDate, Date endDate) {
		return salesRepository.getSalesVSPurchase(branchId, startDate, endDate);
	}
	
	@Override
	public List<SalesVo> findByTypesAndContactAndBranchIdAndSalesDateBetween(List<String> salesTypes, long branchId,
			Date yearStartDate, Date yearEndDate, long contactId) {

		return salesRepository.findByTypesAndContactAndCompanyIdAndSalesDateBetween(salesTypes, branchId, yearStartDate, yearEndDate, contactId);
	}

	@Override
	public SalesItemVo findByItemCodeAndCompanyIdAndSalesId(String itemCode, long companyId, long salesId) {
		// TODO Auto-generated method stub
		return salesItemRepository.findByItemCodeAndCompanyIdAndSalesId(itemCode, companyId, salesId);
	}

	@Override
	public List<SalesVo> findUnpaidSalesByTypeAndBranchIdAndContactIdAndSalesDateBetween(String type, long branchId,
			long contactId, Date startDate, Date endDate) {
		return salesRepository.findUnpaidSalesByTypeAndBranchIdAndContactIdAndIsDeletedAndSalesDateBetween(type, branchId, contactId, startDate, endDate);
		
	}

	@Override
	public void updateOldPaymentMinus(long salesId, double oldPyament) {
		// TODO Auto-generated method stub
		salesRepository.updateOldPaymentMinus(salesId,oldPyament);
	}

	@Override
	public void addPaidAmountBySalesId(long salesId, double payment, long userId, String modifiedOn) {
		// TODO Auto-generated method stub
		salesRepository.addPaidAmountBySalesId(salesId, payment, userId, modifiedOn);
	}

	@Override
	public List<SalesItemVo> findItemBySalesId(long salesId) {
		return salesItemRepository.findBySalesVoSalesId(salesId);
	}

	@Override
	public void insertSalesTransaction(SalesVo salesVo, String yearInterval) {
		
		AccountCustomVo accountCustomVo;
		TransactionVo transactionVo;
		AccountGroupVo accountGroupVo;
		
		//Update Stock
		stockTransactionService.deleteStockTransaction(salesVo.getBranchId(), salesVo.getSalesId(), "sales");
		stockTransactionService.saveStockFromSales(salesVo.getSalesItemVos(), yearInterval);
		//End Update Stock
		
		transactionService.deleteTransaction(salesVo.getBranchId(), salesVo.getSalesId(), salesVo.getType());
		
		double taxableAmount = salesVo.getSalesItemVos().stream()
				.mapToDouble(o -> (o.getQty() * o.getPrice()) - (o.getDiscountType().equals("amount") ? o.getDiscount()
						: (o.getQty() * o.getPrice()) * (o.getDiscount() / 100)))
				.sum();
		
		double totalDiscount = salesVo.getSalesItemVos().stream().mapToDouble(d -> d.getDiscountType().equals("amount") ? d.getDiscount()
				: (d.getQty() * d.getPrice()) * (d.getDiscount() / 100)).sum();
		
		transactionVo = new TransactionVo();

		//Customer Entry
		
		if(salesVo.getType().equals(Constant.SALES_POS) && (salesVo.getContactVo() == null || salesVo.getContactVo().getContactId() == 0)) {
			accountCustomVo = accountCustomService.findByAccountNameAndBranchId("N/A", salesVo.getBranchId());
			transactionVo.setAccountCustomVo(accountCustomVo);
			transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
		} else {
			transactionVo.setAccountCustomVo(salesVo.getContactVo().getAccountCustomVo());
			transactionVo.setAccountGroupVo(salesVo.getContactVo().getAccountCustomVo().getGroup());
		}
		
		transactionVo.setBranchId(salesVo.getBranchId());
		transactionVo.setCompanyId(salesVo.getCompanyId());
		transactionVo.setDescription(salesVo.getPrefix()+salesVo.getSalesNo());
		transactionVo.setTransactionDate(salesVo.getSalesDate());
		transactionVo.setVoucherId(salesVo.getSalesId());
		transactionVo.setVoucherNo(String.valueOf(salesVo.getSalesNo()));
		transactionVo.setVoucherType(salesVo.getType());
		
		transactionVo.setTransactionId(0);
		transactionService.save(transactionVo, salesVo.getTotal(), TransactionAmount.DEBIT);
		//End Customer Entry
		
		//Sales Account Entry
		
		transactionVo = new TransactionVo();
		transactionVo.setBranchId(salesVo.getBranchId());
		transactionVo.setCompanyId(salesVo.getCompanyId());
		transactionVo.setDescription(salesVo.getPrefix()+salesVo.getSalesNo());
		transactionVo.setTransactionDate(salesVo.getSalesDate());
		transactionVo.setVoucherId(salesVo.getSalesId());
		transactionVo.setVoucherNo(String.valueOf(salesVo.getSalesNo()));
		transactionVo.setVoucherType(salesVo.getType());
		
		accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_SALES, salesVo.getBranchId());
		
		transactionVo.setAccountCustomVo(accountCustomVo);
		transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
		
		transactionService.save(transactionVo, taxableAmount + totalDiscount, TransactionAmount.CREDIT);
		//End Sales Account Entry
		
		
		//Tax Entry
		Map<Long, Double> taxes =  
			    salesVo.getSalesItemVos().stream().collect(Collectors.groupingBy(x -> x.getTaxVo().getTaxId() , Collectors.summingDouble(SalesItemVo::getTaxAmount)));
		
		taxes.forEach((key, value) -> {
			AccountCustomVo ac = salesVo.getSalesItemVos().stream().filter(c -> c.getTaxVo().getTaxId() == key)
					.collect(Collectors.toList()).get(0).getTaxVo().getAccountCustomVo();
			
			TransactionVo transactionVo2 = new TransactionVo();
			transactionVo2.setBranchId(salesVo.getBranchId());
			transactionVo2.setCompanyId(salesVo.getCompanyId());
			transactionVo2.setDescription(salesVo.getPrefix()+salesVo.getSalesNo());
			transactionVo2.setTransactionDate(salesVo.getSalesDate());
			transactionVo2.setVoucherId(salesVo.getSalesId());
			transactionVo2.setVoucherNo(String.valueOf(salesVo.getSalesNo()));
			transactionVo2.setVoucherType(salesVo.getType());
			
			transactionVo2.setAccountCustomVo(ac);
			transactionVo2.setAccountGroupVo(ac.getGroup());
			
			transactionService.save(transactionVo2, value, TransactionAmount.CREDIT);
		});
		//End Tax Entry
		
		//Discount Entry
		if(totalDiscount != 0.0) {
			transactionVo = new TransactionVo();
			transactionVo.setBranchId(salesVo.getBranchId());
			transactionVo.setCompanyId(salesVo.getCompanyId());
			transactionVo.setDescription(salesVo.getPrefix()+salesVo.getSalesNo());
			transactionVo.setTransactionDate(salesVo.getSalesDate());
			transactionVo.setVoucherId(salesVo.getSalesId());
			transactionVo.setVoucherNo(String.valueOf(salesVo.getSalesNo()));
			transactionVo.setVoucherType(salesVo.getType());
			
			accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_DISCOUNT_GIVEN, salesVo.getBranchId());
			
			transactionVo.setAccountCustomVo(accountCustomVo);
			transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
			
			transactionService.save(transactionVo, totalDiscount, TransactionAmount.DEBIT);
		}
		//End Discount Entry
		
		//Roundoff Entry
	
		transactionVo = new TransactionVo();
		transactionVo.setBranchId(salesVo.getBranchId());
		transactionVo.setCompanyId(salesVo.getCompanyId());
		transactionVo.setDescription(salesVo.getPrefix()+salesVo.getSalesNo());
		transactionVo.setTransactionDate(salesVo.getSalesDate());
		transactionVo.setVoucherId(salesVo.getSalesId());
		transactionVo.setVoucherNo(String.valueOf(salesVo.getSalesNo()));
		transactionVo.setVoucherType(salesVo.getType());
		
		if(salesVo.getRoundoff() > 0) {
			accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_ROUNDOFF_INCOME, salesVo.getBranchId());
			transactionVo.setAccountCustomVo(accountCustomVo);
			transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
			
			transactionService.save(transactionVo, salesVo.getRoundoff(), TransactionAmount.CREDIT);
		} else if(salesVo.getRoundoff() < 0) {
			accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_ROUNDOFF_EXPENSE, salesVo.getBranchId());
			transactionVo.setAccountCustomVo(accountCustomVo);
			transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
			transactionService.save(transactionVo, salesVo.getRoundoff(), TransactionAmount.DEBIT);
		}
		
		//End Roundoff Entry
		
		//Additional Charge Entry
		if(salesVo.getSalesAdditionalChargeVos() != null) {
			for (SalesAdditionalChargeVo charge : salesVo.getSalesAdditionalChargeVos()) {
				
				transactionVo = new TransactionVo();
				transactionVo.setBranchId(salesVo.getBranchId());
				transactionVo.setCompanyId(salesVo.getCompanyId());
				transactionVo.setDescription(salesVo.getPrefix()+salesVo.getSalesNo());
				transactionVo.setTransactionDate(salesVo.getSalesDate());
				transactionVo.setVoucherId(salesVo.getSalesId());
				transactionVo.setVoucherNo(String.valueOf(salesVo.getSalesNo()));
				transactionVo.setVoucherType(salesVo.getType());
				
				transactionVo.setAccountCustomVo(charge.getAdditionalChargeVo().getAccountCustomVo());
				transactionVo.setAccountGroupVo(charge.getAdditionalChargeVo().getAccountCustomVo().getGroup());
				
				transactionService.save(transactionVo, charge.getAmount(), TransactionAmount.CREDIT);
				
				transactionVo = new TransactionVo();
				transactionVo.setBranchId(salesVo.getBranchId());
				transactionVo.setCompanyId(salesVo.getCompanyId());
				transactionVo.setDescription(salesVo.getPrefix()+salesVo.getSalesNo());
				transactionVo.setTransactionDate(salesVo.getSalesDate());
				transactionVo.setVoucherId(salesVo.getSalesId());
				transactionVo.setVoucherNo(String.valueOf(salesVo.getSalesNo()));
				transactionVo.setVoucherType(salesVo.getType());
				
				transactionVo.setAccountCustomVo(charge.getTaxVo().getAccountCustomVo());
				transactionVo.setAccountGroupVo(charge.getTaxVo().getAccountCustomVo().getGroup());
				
				transactionService.save(transactionVo, charge.getTaxAmount(), TransactionAmount.CREDIT);
			}
		}
		//End Additional Charge Entry
	}

	@Override
	public void insertSalesReturnTransaction(SalesVo salesVo, String yearInterval) {
		AccountCustomVo accountCustomVo;
		TransactionVo transactionVo;
		AccountGroupVo accountGroupVo;
		
		//List<TransactionVo> transactionVos = new ArrayList<TransactionVo>();
		
		//Update Stock
		stockTransactionService.deleteStockTransaction(salesVo.getBranchId(), salesVo.getSalesId(), "sales_return");
		stockTransactionService.saveStockFromSalesReturn(salesVo.getSalesItemVos(), yearInterval);
		//End Update Stock
		transactionService.deleteTransaction(salesVo.getBranchId(), salesVo.getSalesId(), salesVo.getType());
		
		double taxableAmount = salesVo.getSalesItemVos().stream()
				.mapToDouble(o -> (o.getQty() * o.getPrice()) - (o.getDiscountType().equals("amount") ? o.getDiscount()
						: (o.getQty() * o.getPrice()) * (o.getDiscount() / 100)))
				.sum();
		
		double totalDiscount = salesVo.getSalesItemVos().stream().mapToDouble(d -> d.getDiscountType().equals("amount") ? d.getDiscount()
				: (d.getQty() * d.getPrice()) * (d.getDiscount() / 100)).sum();
		
		transactionVo = new TransactionVo();

		//Customer Entry
		transactionVo.setAccountCustomVo(salesVo.getContactVo().getAccountCustomVo());
		
		transactionVo.setAccountGroupVo(salesVo.getContactVo().getAccountCustomVo().getGroup());
		
		transactionVo.setBranchId(salesVo.getBranchId());
		transactionVo.setCompanyId(salesVo.getCompanyId());
		transactionVo.setDescription(salesVo.getPrefix()+salesVo.getSalesNo());
		transactionVo.setTransactionDate(salesVo.getSalesDate());
		transactionVo.setVoucherId(salesVo.getSalesId());
		transactionVo.setVoucherNo(String.valueOf(salesVo.getSalesNo()));
		transactionVo.setVoucherType(salesVo.getType());
		
		transactionService.save(transactionVo, salesVo.getTotal(), TransactionAmount.CREDIT);
		//End Customer Entry
		
		//Sales Account Entry
		
		transactionVo = new TransactionVo();
		transactionVo.setBranchId(salesVo.getBranchId());
		transactionVo.setCompanyId(salesVo.getCompanyId());
		transactionVo.setDescription(salesVo.getPrefix()+salesVo.getSalesNo());
		transactionVo.setTransactionDate(salesVo.getSalesDate());
		transactionVo.setVoucherId(salesVo.getSalesId());
		transactionVo.setVoucherNo(String.valueOf(salesVo.getSalesNo()));
		transactionVo.setVoucherType(salesVo.getType());
		
		accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_SALES_RETURN, salesVo.getBranchId());
		
		transactionVo.setAccountCustomVo(accountCustomVo);
		transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
		
		transactionService.save(transactionVo, taxableAmount + totalDiscount, TransactionAmount.DEBIT);
		//End Sales Account Entry
		
		
		//Tax Entry
		Map<Long, Double> taxes =  
			    salesVo.getSalesItemVos().stream().collect(Collectors.groupingBy(x -> x.getTaxVo().getTaxId() , Collectors.summingDouble(SalesItemVo::getTaxAmount)));
		
		taxes.forEach((key, value) -> {
			AccountCustomVo ac = salesVo.getSalesItemVos().stream().filter(c -> c.getTaxVo().getTaxId() == key)
					.collect(Collectors.toList()).get(0).getTaxVo().getAccountCustomVo();
			
			TransactionVo transactionVo2 = new TransactionVo();
			transactionVo2.setBranchId(salesVo.getBranchId());
			transactionVo2.setCompanyId(salesVo.getCompanyId());
			transactionVo2.setDescription(salesVo.getPrefix()+salesVo.getSalesNo());
			transactionVo2.setTransactionDate(salesVo.getSalesDate());
			transactionVo2.setVoucherId(salesVo.getSalesId());
			transactionVo2.setVoucherNo(String.valueOf(salesVo.getSalesNo()));
			transactionVo2.setVoucherType(salesVo.getType());
			
			transactionVo2.setAccountCustomVo(ac);
			transactionVo2.setAccountGroupVo(ac.getGroup());
			
			transactionService.save(transactionVo2, value, TransactionAmount.DEBIT);
		});
		//End Tax Entry
		
		//Discount Entry
		if(totalDiscount != 0.0) {
			transactionVo = new TransactionVo();
			transactionVo.setBranchId(salesVo.getBranchId());
			transactionVo.setCompanyId(salesVo.getCompanyId());
			transactionVo.setDescription(salesVo.getPrefix()+salesVo.getSalesNo());
			transactionVo.setTransactionDate(salesVo.getSalesDate());
			transactionVo.setVoucherId(salesVo.getSalesId());
			transactionVo.setVoucherNo(String.valueOf(salesVo.getSalesNo()));
			transactionVo.setVoucherType(salesVo.getType());
			
			accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_DISCOUNT_RECEIVED, salesVo.getBranchId());
			
			transactionVo.setAccountCustomVo(accountCustomVo);
			transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
			
			transactionService.save(transactionVo, totalDiscount, TransactionAmount.CREDIT);
		}
		//End Discount Entry
		
		//Roundoff Entry
	
		transactionVo = new TransactionVo();
		transactionVo.setBranchId(salesVo.getBranchId());
		transactionVo.setCompanyId(salesVo.getCompanyId());
		transactionVo.setDescription(salesVo.getPrefix()+salesVo.getSalesNo());
		transactionVo.setTransactionDate(salesVo.getSalesDate());
		transactionVo.setVoucherId(salesVo.getSalesId());
		transactionVo.setVoucherNo(String.valueOf(salesVo.getSalesNo()));
		transactionVo.setVoucherType(salesVo.getType());
		
		if(salesVo.getRoundoff() > 0) {
			accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_ROUNDOFF_EXPENSE, salesVo.getBranchId());
			transactionVo.setAccountCustomVo(accountCustomVo);
			transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
			
			transactionService.save(transactionVo, salesVo.getRoundoff(), TransactionAmount.DEBIT);
		} else if(salesVo.getRoundoff() < 0) {
			accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_ROUNDOFF_INCOME, salesVo.getBranchId());
			transactionVo.setAccountCustomVo(accountCustomVo);
			transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
			transactionService.save(transactionVo, salesVo.getRoundoff(), TransactionAmount.CREDIT);
		}
		
		//End Roundoff Entry
		
		//Additional Charge Entry
		if(salesVo.getSalesAdditionalChargeVos() != null) {
			for (SalesAdditionalChargeVo charge : salesVo.getSalesAdditionalChargeVos()) {
				
				transactionVo = new TransactionVo();
				transactionVo.setBranchId(salesVo.getBranchId());
				transactionVo.setCompanyId(salesVo.getCompanyId());
				transactionVo.setDescription(salesVo.getPrefix()+salesVo.getSalesNo());
				transactionVo.setTransactionDate(salesVo.getSalesDate());
				transactionVo.setVoucherId(salesVo.getSalesId());
				transactionVo.setVoucherNo(String.valueOf(salesVo.getSalesNo()));
				transactionVo.setVoucherType(salesVo.getType());
				
				transactionVo.setAccountCustomVo(charge.getAdditionalChargeVo().getAccountCustomVo());
				transactionVo.setAccountGroupVo(charge.getAdditionalChargeVo().getAccountCustomVo().getGroup());
				
				transactionService.save(transactionVo, charge.getAmount(), TransactionAmount.DEBIT);
				
				transactionVo = new TransactionVo();
				transactionVo.setBranchId(salesVo.getBranchId());
				transactionVo.setCompanyId(salesVo.getCompanyId());
				transactionVo.setDescription(salesVo.getPrefix()+salesVo.getSalesNo());
				transactionVo.setTransactionDate(salesVo.getSalesDate());
				transactionVo.setVoucherId(salesVo.getSalesId());
				transactionVo.setVoucherNo(String.valueOf(salesVo.getSalesNo()));
				transactionVo.setVoucherType(salesVo.getType());
				
				transactionVo.setAccountCustomVo(charge.getTaxVo().getAccountCustomVo());
				transactionVo.setAccountGroupVo(charge.getTaxVo().getAccountCustomVo().getGroup());
				
				transactionService.save(transactionVo, charge.getTaxAmount(), TransactionAmount.DEBIT);
			}
		}
		//End Additional Charge Entry
	}

	@Override
	public List<Map<Double, Double>> getPaidAndTotalAmountByContactId(List<String> salesTypes, long branchId,
			Date startDate, Date endDate, long contactId) {
		return salesRepository.getPaidAndTotalAmountByContactId(salesTypes, branchId, startDate, endDate, contactId);
	}

	@Override
	public double getDueAmountByContactId(List<String> types, long branchId, long contactId, Date startDate, Date endDate) {
		try {
			return salesRepository.getDueAmountByContactId(types, branchId, contactId, startDate, endDate);
		} catch(Exception e) {
			return 0;
		}
		
	}

	@Override
	public void updateAssignedEmployeeAndStatusBySalesId(long employeeId, String status, long salesId, long alterBy, String modifiedOn) {
		salesRepository.updateAssignedEmployeeAndStatusBySalesId(employeeId, status, salesId, alterBy, modifiedOn);
	}
	
	@Override
	public void updateStatusBySalesId(String status, long salesId, long alterBy, String modifiedOn) {
		salesRepository.updateStatusBySalesId(status, salesId, alterBy, modifiedOn);
	}

	@Override
	public List<SalesAdditionalChargeVo> findAdditionalChargeBySalesId(long salesId) {
		return salesAdditionalChargeRepository.findAdditionalChargeBySalesVoSalesId(salesId);
	}
	
	@Override
	public List<SalesVo> findByBranchIdAndTypeAndStatus(long branchId, String type, List<String>  status) {
		return salesRepository.findByBranchIdAndTypeAndStatus(branchId, type, status);
	}
	
	@Override
	public void updateSalesOrderItem(long salesItemId, long productVariantId, double productionQty, double oldProductionQty) {
		salesRepository.updateSalesOrderItem(salesItemId,productVariantId,productionQty,oldProductionQty);
	}
	
	@Override
	public SalesVo findBySalesId(long salesId) {
		return salesRepository.findBySalesId(salesId);
	}
	
	@Override
	public double findQtyBySalesItemId(long salesItemId) {
		return salesItemRepository.findQtyBySalesItemId(salesItemId);
	}
	
	@Override
	public double findProducedQtyBySalesItemId(long salesItemId) {
		return salesItemRepository.findProducedQtyBySalesItemId(salesItemId);
	}
	
	@Override
	public void updateSalesOrderItemForDelivery(long salesItemId, long productVariantId, double deliveryQty, double oldDeliveredQty) {
		salesRepository.updateSalesOrderItemForDelivery(salesItemId,productVariantId,deliveryQty,oldDeliveredQty);
	}
	
	@Override
	public double findDeliveredQtyBySalesItemId(long salesItemId) {
		return salesItemRepository.findDeliveredQtyBySalesItemId(salesItemId);
	}

	@Override
	public double getAllProducedTotalQty(long branchId, Date fromDate, Date toDate) {
		try {
			return salesItemRepository.findCurrentAvailableStock(branchId);
		}catch (Exception e) {
			return 0;
		}
	}

	@Override
	public double getRemainsProductionQty(long branchId) {
		try {
			return salesItemRepository.findRemainsProductionStock(branchId);
		}catch (Exception e) {
			return 0;
		}
	}

	@Override
	public double getTotalAvailableQty(long branchId, long productId, Date fromDate, Date toDate) {
		try {
			return salesItemRepository.findCurrentAvailableStock(branchId,productId);
		}catch (Exception e) {
			return 0;
		}
	}

	@Override
	public double getVariantQty(long branchId, long productVariantId, Date fromDate, Date toDate) {
		try {
			return salesItemRepository.findCurrentProductVariantAvailableStock(branchId,productVariantId);
		}catch (Exception e) {
			return 0;
		}
	}
}
