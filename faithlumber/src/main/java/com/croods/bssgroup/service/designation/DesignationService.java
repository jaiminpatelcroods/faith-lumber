package com.croods.bssgroup.service.designation;

import java.util.List;

import com.croods.bssgroup.vo.designation.DesignationVo;

public interface DesignationService {

	public void save(DesignationVo designationVo);
	
	DesignationVo findByDesignationId(long designationId);
	
	void deleteDesignation(long designationId, long alterBy, String modifiedOn);

	List<DesignationVo> findByCompanyId(long companyId);
	
	public boolean isExistDesignation(long companyId, String designationName);

	public boolean isExistDesignation(long companyId, String designationName, long designationId);
	
}
