package com.croods.bssgroup.service.production;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.production.ProductionItemRepository;
import com.croods.bssgroup.repository.production.ProductionRepository;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.production.ProductionItemVo;
import com.croods.bssgroup.vo.production.ProductionVo;
import com.croods.bssgroup.vo.prefix.PrefixVo;

@Service
@Transactional
public class ProductionServiceImpl implements ProductionService {

	@Autowired
	ProductionRepository productionRepository;
	
	@Autowired
	ProductionItemRepository productionItemRepository;
	
	@Autowired
	PrefixService prefixService;
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public DataTablesOutput<ProductionVo> findAll(DataTablesInput input,
			Specification<ProductionVo> additionalSpecification, Specification<ProductionVo> specification) {
		
		return productionRepository.findAll(input, additionalSpecification, specification);
	}

	@Override
	public ProductionVo save(ProductionVo productionVo) {
		productionVo = productionRepository.saveAndFlush(productionVo);
		entityManager.refresh(productionVo);
		return productionVo;
		
		//productionRepository.save(productionVo);
	}
	
	@Override
	public void deleteProductionItemIds(List<Long> productionItemIds) {
		productionItemRepository.deleteProductionItemIds(productionItemIds);
	}

	@Override
	public ProductionVo findByProductionIdAndBranchId(long productionId, long branchId) {
		return productionRepository.findByProductionIdAndBranchId(productionId, branchId);
	}

	@Override
	public long findMaxProductionNo(String type, long companyId, long branchId, long userId,
			String defaultPrefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(defaultPrefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = productionRepository.findMaxProductionNo(branchId, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
		
	}

	@Override
	public void delete(long productionId, long userId, String modifiedOn) {
		productionRepository.delete(productionId, userId, modifiedOn);
	}

	@Override
	public List<ProductionVo> findByBranchId(long branchId) {
		return productionRepository.findByBranchIdAndIsDeleted(branchId, 0);
	}

	@Override
	public List<ProductionItemVo> findItemByProductionId(long productionId) {
		return productionItemRepository.findByProductionVoProductionId(productionId);
	}

}
