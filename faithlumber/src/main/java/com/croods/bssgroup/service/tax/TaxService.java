package com.croods.bssgroup.service.tax;


import java.util.List;

import com.croods.bssgroup.vo.tax.TaxVo;

public interface TaxService {
	
	public TaxVo findByTaxId(long id);
	
	public void taxSave(TaxVo taxVo);
	
	public void delete(long taxId, long alterBy, String modifiedOn);

	public List<TaxVo> findByCompanyId(long companyId);

	public void updateIsDefaultByCompanyId(long companyId, int isDefault);
	
	public void updateIsDefaultByTaxId(long taxId, int isDefault);
	
	public boolean isExistTax(long companyId, String tax);

	public boolean isExistTax(long companyId, String tax, long taxId);
}
