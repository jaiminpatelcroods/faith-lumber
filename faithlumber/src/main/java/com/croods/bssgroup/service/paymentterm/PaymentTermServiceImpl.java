package com.croods.bssgroup.service.paymentterm;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.paymentterm.PaymentTermRepository;
import com.croods.bssgroup.vo.paymentterm.PaymentTermVo;

@Service
@Transactional
public class PaymentTermServiceImpl implements PaymentTermService {

	@Autowired
	PaymentTermRepository paymentTermRepository;
	
	@Override
	public void save(PaymentTermVo paymentTermVo) {
		paymentTermRepository.save(paymentTermVo);
		
	}

	@Override
	public List<PaymentTermVo> findByBranchId(long branchid) {
		return paymentTermRepository.findByBranchIdAndIsDeletedOrderByPaymentTermIdDesc(branchid, 0);
	}

	@Override
	public PaymentTermVo findByPaymentTermId(long id) {
		return paymentTermRepository.findByPaymentTermId(id);
	}

	@Override
	public void delete(long id, long alterBy, String modifiedOn) {
		paymentTermRepository.delete(id, alterBy, modifiedOn);
	}

}
