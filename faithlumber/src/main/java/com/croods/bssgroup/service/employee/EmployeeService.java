package com.croods.bssgroup.service.employee;

import java.util.List;

import com.croods.bssgroup.vo.employee.EmployeeVo;

public interface EmployeeService {
	
	public EmployeeVo findByEmployeeIdAndBranchId(long employeeId, long branchId);
	
	public List<EmployeeVo> findByBranchId(long branchId);
	
	public void deleteEmployee(long employeeId, long userId, String modifiedOn);

	public void save(EmployeeVo employeeVo);

	public void deleteEmployeeContactByIdIn(List<Long> employeeContactIds);
}
