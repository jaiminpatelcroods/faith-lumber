package com.croods.bssgroup.service.financial;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.financial.FinancialMonthRepository;
import com.croods.bssgroup.repository.financial.FinancialYearRepository;
import com.croods.bssgroup.repository.financial.YearWiseOpeningBalanceRepository;
import com.croods.bssgroup.vo.financial.FinancialMonth;
import com.croods.bssgroup.vo.financial.FinancialYear;
import com.croods.bssgroup.vo.financial.YearWiseOpeningBalanceVo;

@Service
@Transactional
public class FinancialServiceImpl  implements FinancialService{

	@Autowired
	FinancialMonthRepository financialMonthRepository;
	
	@Autowired
	FinancialYearRepository financialYearRepository;
	
	@Autowired
	YearWiseOpeningBalanceRepository yearWiseOpeningBalanceRepository;

	//MonthInterval Method implementation
	@Override
	public String findByMonthInterval(String monthInterval) {
		return financialMonthRepository.findByMonthInterval(monthInterval);
	}

	@Override
	public List<FinancialMonth> findAllFinancialMonth() {
		return financialMonthRepository.findAll();
	}

	//FinancialYear Method implementation
	@Override
	public List<FinancialYear> findAllFinancialYear() {
		return financialYearRepository.findAll();
	}

	//YearWiseOpeningBalanceVo Method implementation
	@Override
	public YearWiseOpeningBalanceVo findByBranchIdAndAccountCustomIdAndYearInterval(long branchId,
			long accountCustomId, String yearInterval) {
		return yearWiseOpeningBalanceRepository.findByBranchIdAndAccountCustomVoAccountCustomIdAndYearInterval(branchId, accountCustomId, yearInterval);
	}

	@Override
	public YearWiseOpeningBalanceVo findByOpeningBalanceId(long Id) {
		return yearWiseOpeningBalanceRepository.findByOpeningBalanceId(Id);
	}

	@Override
	public YearWiseOpeningBalanceVo saveYearWiseOpeningBalance(YearWiseOpeningBalanceVo yearWiseOpeningBalanceVo) {
		return yearWiseOpeningBalanceRepository.save(yearWiseOpeningBalanceVo);
	}
}
