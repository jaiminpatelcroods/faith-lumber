package com.croods.bssgroup.service.receipt;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.repository.receipt.ReceiptBillRepository;
import com.croods.bssgroup.repository.receipt.ReceiptRepository;
import com.croods.bssgroup.repository.sales.SalesRepository;
import com.croods.bssgroup.service.account.AccountService;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.service.transaction.TransactionService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.util.TransactionAmount;
import com.croods.bssgroup.vo.account.AccountCustomVo;
import com.croods.bssgroup.vo.prefix.PrefixVo;
import com.croods.bssgroup.vo.receipt.ReceiptBillVo;
import com.croods.bssgroup.vo.receipt.ReceiptVo;
import com.croods.bssgroup.vo.transaction.TransactionVo;

@Service
@Transactional
public class ReceiptServiceImpl implements ReceiptService{

	@Autowired
	ReceiptRepository receiptRepository;

	@Autowired
	PrefixService prefixService;

	@Autowired
	ReceiptBillRepository receiptBillRepository;
	
	@Autowired
	SalesRepository salesRepository; 
	
	@Autowired
	TransactionService transactionService;
	
	@Autowired
	AccountService accountCustomService;
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public DataTablesOutput<ReceiptVo> findAll(@Valid DataTablesInput input, Specification<ReceiptVo> additionalSpecification,
			Specification<ReceiptVo> specification) {
		
		return receiptRepository.findAll(input, additionalSpecification, specification);
	}

	@Override
	public long findMaxReceiptNo(long companyId, long branchId, long userId, String type, String prefix) {
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(prefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = receiptRepository.findMaxReceiptNo(branchId, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
	}

	@Override
	public ReceiptVo save(ReceiptVo receiptVo) {
		receiptVo = receiptRepository.saveAndFlush(receiptVo);
		entityManager.refresh(receiptVo);
		return receiptVo;
	}

	@Override
	public void deleteReceiptBillVoByIn(List<Long> receiptBillIds) {
		
		List<ReceiptBillVo> receiptBillVo=receiptBillRepository.findAllById(receiptBillIds);
		
		for(ReceiptBillVo billVo:receiptBillVo)
		{	
			double minus=billVo.getSalesVo().getPaidAmount()-billVo.getPayment();
			
			salesRepository.updatePaidAmount(billVo.getSalesVo().getSalesId(),minus);
		}
		
		receiptBillRepository.deleteReceiptBill(receiptBillIds);
	}

	@Override
	public ReceiptVo findByReceiptIdAndBranchId(long receiptId, long branchId) {
		// TODO Auto-generated method stub
		return receiptRepository.findByReceiptIdAndBranchIdAndIsDeleted(receiptId, branchId, 0);
	}

	@Override
	public void transation(ReceiptVo receiptVo, double totalKasar) {
		
		TransactionVo transactionVo;
		AccountCustomVo accountCustomVo = new AccountCustomVo();

		transactionVo = new TransactionVo();

		transactionVo.setTransactionDate(receiptVo.getReceiptDate());
		transactionVo.setBranchId(receiptVo.getBranchId());
		transactionVo.setCompanyId(receiptVo.getCompanyId());
		transactionVo.setDescription("Receipt No - " + receiptVo.getReceiptNo());
		transactionVo.setVoucherType(Constant.RECEIPT);
		transactionVo.setVoucherId(receiptVo.getReceiptId());
		transactionVo.setVoucherNo("" + receiptVo.getReceiptNo());

		transactionService.deleteTransaction(receiptVo.getBranchId(), receiptVo.getReceiptId(), receiptVo.getType());

		if (receiptVo.getContactVo() == null || receiptVo.getContactVo().getContactId() == 0) {
			accountCustomVo = accountCustomService.findByAccountNameAndBranchId("N/A", receiptVo.getBranchId());
			transactionVo.setAccountCustomVo(accountCustomVo);
			transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
		} else {
			transactionVo.setAccountGroupVo(receiptVo.getContactVo().getAccountCustomVo().getGroup());
			transactionVo.setAccountCustomVo(receiptVo.getContactVo().getAccountCustomVo());
		}

		transactionService.save(transactionVo, receiptVo.getTotalPayment(), TransactionAmount.CREDIT);

		if (receiptVo.getPaymentMode().equalsIgnoreCase("cash")) {
			transactionVo = new TransactionVo();

			transactionVo.setTransactionDate(receiptVo.getReceiptDate());
			transactionVo.setBranchId(receiptVo.getBranchId());
			transactionVo.setCompanyId(receiptVo.getCompanyId());
			transactionVo.setDescription("Receipt No - " + receiptVo.getReceiptNo());
			transactionVo.setVoucherType(Constant.RECEIPT);
			transactionVo.setVoucherId(receiptVo.getReceiptId());
			transactionVo.setVoucherNo("" + receiptVo.getReceiptNo());

			accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_CASH,
					receiptVo.getBranchId());
			transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
			transactionVo.setAccountCustomVo(accountCustomVo);

			transactionService.save(transactionVo, receiptVo.getTotalPayment(), TransactionAmount.DEBIT);

		} else {
			transactionVo = new TransactionVo();

			transactionVo.setTransactionDate(receiptVo.getReceiptDate());
			transactionVo.setBranchId(receiptVo.getBranchId());
			transactionVo.setCompanyId(receiptVo.getCompanyId());
			transactionVo.setDescription("Receipt No - " + receiptVo.getReceiptNo());
			transactionVo.setVoucherType(Constant.RECEIPT);
			transactionVo.setVoucherId(receiptVo.getReceiptId());
			transactionVo.setVoucherNo("" + receiptVo.getReceiptNo());

			transactionVo.setAccountGroupVo(receiptVo.getBankVo().getAccountCustomVo().getGroup());
			transactionVo.setAccountCustomVo(receiptVo.getBankVo().getAccountCustomVo());

			transactionService.save(transactionVo, receiptVo.getTotalPayment(), TransactionAmount.DEBIT);
		}

		if (totalKasar != 0) {
			transactionVo = new TransactionVo();

			transactionVo.setTransactionDate(receiptVo.getReceiptDate());
			transactionVo.setBranchId(receiptVo.getBranchId());
			transactionVo.setCompanyId(receiptVo.getCompanyId());
			transactionVo.setDescription("Receipt No - " + receiptVo.getReceiptNo());
			transactionVo.setVoucherType(Constant.RECEIPT);
			transactionVo.setVoucherId(receiptVo.getReceiptId());
			transactionVo.setVoucherNo("" + receiptVo.getReceiptNo());

			accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_KASAR,
					receiptVo.getBranchId());
			transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
			transactionVo.setAccountCustomVo(accountCustomVo);

			if (totalKasar > 0) {
				transactionService.save(transactionVo, totalKasar, TransactionAmount.DEBIT);
			} else {
				transactionService.save(transactionVo, totalKasar * (-1), TransactionAmount.CREDIT);
			}

		}
	}

}
