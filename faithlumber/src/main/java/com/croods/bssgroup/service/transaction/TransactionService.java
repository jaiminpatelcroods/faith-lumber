package com.croods.bssgroup.service.transaction;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.util.TransactionAmount;
import com.croods.bssgroup.vo.transaction.TransactionVo;

public interface TransactionService {

	public TransactionVo save(TransactionVo transactionVo,double amount, TransactionAmount transactionAmount);

	public void deleteTransaction(long branchId, long purchaseId, String type);

	List<TransactionVo> saveAll(List<TransactionVo> transactionVos);
	
	public double getOpeningBalance(long branchId, long accountCustomId, Date startDate, Date fromDate, String year);
	
	DataTablesOutput<TransactionVo> findAll(DataTablesInput input,
			Specification<TransactionVo> additionalSpecification, Specification<TransactionVo> specification);
	
	public double getOpeningBalancePageable(long branchId, long accountCustomId, Date startDate, Date fromDate, int start, int limit,String yearInterval);
}
