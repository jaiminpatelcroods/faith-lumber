package com.croods.bssgroup.service.production;

import java.util.List;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.vo.production.ProductionItemVo;
import com.croods.bssgroup.vo.production.ProductionVo;

public interface ProductionService {

	public DataTablesOutput<ProductionVo> findAll(DataTablesInput input,
			Specification<ProductionVo> additionalSpecification, Specification<ProductionVo> specification);

	public void deleteProductionItemIds(List<Long> productionItemIds);

	public ProductionVo save(ProductionVo productionVo);

	public ProductionVo findByProductionIdAndBranchId(long productionId, long branchId);

	public long findMaxProductionNo(String type, long companyId, long branchId, long userId, String defaultPrefix);

	public void delete(long productionId, long userId, String modifiedOn);

	public List<ProductionVo> findByBranchId(long branchId);

	public List<ProductionItemVo> findItemByProductionId(long productionId);
}
