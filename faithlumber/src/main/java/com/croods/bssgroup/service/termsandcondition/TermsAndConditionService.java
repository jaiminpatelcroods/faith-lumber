package com.croods.bssgroup.service.termsandcondition;

import java.util.List;

import com.croods.bssgroup.vo.termsandcondition.TermsAndConditionVo;

public interface TermsAndConditionService {

	public void save(TermsAndConditionVo termsAndConditionVo);
	
	TermsAndConditionVo findByTermsandConditionId(long termsandConditionId);
	
	List<TermsAndConditionVo> findByCompanyId(long companyId);
	
	List<TermsAndConditionVo> findByCompanyIdAndIsDefault(long companyId, int isDefault);
	
	List<TermsAndConditionVo> findByCompanyIdAndIsDefaultAndModules(long companyId, int isDefault, String modules);
	
	List<TermsAndConditionVo> findByCompanyIdAndModules(long companyId, String modules);
	
	List<TermsAndConditionVo> findByTermsandConditionIdIn(List<Long> termsandConditionIds);
	
	void delete(long termsandConditionId, long alterBy, String modifiedOn);
}
