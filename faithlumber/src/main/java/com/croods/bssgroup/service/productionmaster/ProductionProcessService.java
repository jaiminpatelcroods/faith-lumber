package com.croods.bssgroup.service.productionmaster;

import java.util.List;

import com.croods.bssgroup.vo.productionmaster.ProductionProcessVo;

public interface ProductionProcessService {

	List<ProductionProcessVo> findByCompanyId(long companyId);

	void save(ProductionProcessVo productionProcessVo);

	ProductionProcessVo findByProductionProcessId(long productionProcessId);
	
	List<ProductionProcessVo> findByCompanyIdAndProductionTypeId(long companyId, long productionTypeId);
	
	void delete(long id, long userId, String currentDate);
}
