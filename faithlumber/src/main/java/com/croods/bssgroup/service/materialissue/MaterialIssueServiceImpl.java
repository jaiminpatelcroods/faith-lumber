package com.croods.bssgroup.service.materialissue;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.repository.materialissue.MaterialIssueItemRepository;
import com.croods.bssgroup.repository.materialissue.MaterialIssueRepository;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.service.stock.StockTransactionService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.materialissue.MaterialIssueVo;
import com.croods.bssgroup.vo.prefix.PrefixVo;

@Service
@Transactional
public class MaterialIssueServiceImpl implements MaterialIssueService {

	@Autowired
	MaterialIssueRepository materialIssueRepository;
	
	@Autowired
	MaterialIssueItemRepository materialIssueItemRepository;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	StockTransactionService stockTransactionService;
	
	@Override
	public DataTablesOutput<MaterialIssueVo> findAll(DataTablesInput input,
			Specification<MaterialIssueVo> additionalSpecification, Specification<MaterialIssueVo> specification) {
		return materialIssueRepository.findAll(input, additionalSpecification, specification);
	}

	@Override
	public void deleteMaterialIssueItemIds(List<Long> materialIssueItemIds) {
		materialIssueItemRepository.deleteMaterialIssueItemIds(materialIssueItemIds);
	}

	@Override
	public void save(MaterialIssueVo materialIssueVo) {
		materialIssueRepository.save(materialIssueVo);
	}

	@Override
	public void saveStockTransaction(MaterialIssueVo materialIssueVo, String yearInterval) {
		stockTransactionService.deleteStockTransaction(materialIssueVo.getBranchId(), materialIssueVo.getMaterialIssueId(), Constant.MATERIAL_ISSUE);
		stockTransactionService.saveStockFromMaterialIssue(materialIssueVo.getMaterialIssueItemVos(), yearInterval);
	}
	
	@Override
	public MaterialIssueVo findByMaterialIssueIdAndBranchId(long materialIssueId, long branchId) {
		return materialIssueRepository.findByMaterialIssueIdAndBranchId(materialIssueId, branchId);
	}

	@Override
	public long findMaxIssueNo(String type, long companyId, long branchId, long userId, String defaultPrefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(defaultPrefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = materialIssueRepository.findMaxIssueNo(branchId, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
	}

	@Override
	public void delete(long materialIssueId, long branchId, long userId, String modifiedOn) {
		stockTransactionService.deleteStockTransaction(branchId, materialIssueId, Constant.MATERIAL_ISSUE);
		stockTransactionService.deleteStockTransaction(branchId, materialIssueId, Constant.MATERIAL_RETURN);
		materialIssueRepository.delete(materialIssueId, userId, modifiedOn);
		
	}

	@Override
	public void materialReturn(MaterialIssueVo materialIssueVo, String yearInterval) {
		
		materialIssueVo.getMaterialIssueItemVos()
			.forEach(p -> materialIssueItemRepository.updateReturnQTY(p.getMaterialIssueItemId(), p.getReturnQty()));
		
		materialIssueVo = materialIssueRepository.findByMaterialIssueIdAndBranchId(materialIssueVo.getMaterialIssueId(), materialIssueVo.getBranchId());
		
		stockTransactionService.saveStockFromMaterialReturn(materialIssueVo.getMaterialIssueItemVos(), yearInterval);
	}

}
