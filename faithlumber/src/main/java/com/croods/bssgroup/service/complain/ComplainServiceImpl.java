package com.croods.bssgroup.service.complain;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.complain.ComplainRepository;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.complain.ComplainVo;
import com.croods.bssgroup.vo.prefix.PrefixVo;

@Service
@Transactional
public class ComplainServiceImpl implements ComplainService {

	@Autowired
	ComplainRepository complainRepository;

	@Autowired
	PrefixService prefixService;
	
	@Override
	public ComplainVo save(ComplainVo complainVo) {
		return complainRepository.save(complainVo);
	}

	@Override
	public long findMaxComplainNo(long companyId, long branchId, long userId, String type, String prefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(prefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxComplainNo = complainRepository.findMaxComplainNo(branchId, prefixVo.getPrefix());
		
		if(maxComplainNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long complainNo = Long.parseLong(maxComplainNo);
			complainNo++;
			return complainNo;
		}
	}

	@Override
	public ComplainVo findByComplainIdAndBranchId(long complainId, long branchId) {
		return complainRepository.findByComplainIdAndBranchIdAndIsDeleted(complainId, branchId, 0);
	}

	@Override
	public DataTablesOutput<ComplainVo> findAll(DataTablesInput input,
			Specification<ComplainVo> additionalSpecification, Specification<ComplainVo> specification) {
		return complainRepository.findAll(input, additionalSpecification, specification);
	}

	@Override
	public void delete(long complainId, long alterBy, String modifiedOn) {
		complainRepository.delete(complainId, alterBy, modifiedOn);
	}

	@Override
	public void updateAssignedEmployee(long employeeId, long complainId, String status, long alterBy,
			String modifiedOn) {
		complainRepository.updateAssignedEmployee(employeeId, status, alterBy, modifiedOn, complainId);
	}

	@Override
	public void complainAssignedEmployee(long complainId, String status, String correctiveActions,
			String preventiveAction, String remark, Date completedDate, long alterBy, String modifiedOn) {
		
		complainRepository.complainAssignedEmployee(status, alterBy, modifiedOn, correctiveActions, preventiveAction, remark, completedDate, complainId);
	}
}
