package com.croods.bssgroup.service.bank;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.repository.bank.BankRepository;
import com.croods.bssgroup.repository.bank.BankTransactionRepository;
import com.croods.bssgroup.service.account.AccountService;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.service.transaction.TransactionService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.util.TransactionAmount;
import com.croods.bssgroup.vo.account.AccountCustomVo;
import com.croods.bssgroup.vo.bank.BankTransactionVo;
import com.croods.bssgroup.vo.bank.BankVo;
import com.croods.bssgroup.vo.prefix.PrefixVo;
import com.croods.bssgroup.vo.transaction.TransactionVo;

@Service
@Transactional
public class BankServiceImpl implements BankService {

	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired
	BankRepository bankRepository;
	
	@Autowired
	BankTransactionRepository bankTransactionRepository;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	TransactionService transactionService;
	
	@Autowired
	AccountService accountService;
	
	//Bank Method Implementation
	@Override
	public List<BankVo> findByBranchId(long branchId) {
		return bankRepository.findByBranchIdAndIsDeletedOrderByBankIdDesc(branchId, 0);
	}

	@Override
	public BankVo findByBankIdAndBranchId(long bankId, long branchId) {
		return bankRepository.findByBankIdAndBranchIdAndIsDeleted(bankId, branchId, 0);
	}

	@Override
	public void deleteBank(long bankId, long alterBy, String modifiedOn) {
		bankRepository.delete(bankId, alterBy, modifiedOn);
	}

	@Override
	public BankVo findByBankId(long bankId) {
		return bankRepository.findByBankId(bankId);
	}

	@Override
	public BankVo saveBank(BankVo bankVo) {
		return bankRepository.save(bankVo);
	}
	
	@Override
	public
	AccountCustomVo findAccountCustomVoByBankId(long bankId) {
		return bankRepository.findAccountCustomVoByBankId(bankId);
	}
	
	//BankTransactionVo Method Implementation
	@Override
	public List<BankTransactionVo> findByBranchIdAndTransactionDateBetween(long branchId, Date transactionDateStart,
			Date transactionDateEnd) {
		return bankTransactionRepository.findByBranchIdAndIsDeletedAndTransactionDateBetween(branchId, 0, transactionDateStart, transactionDateEnd);
	}

	@Override
	public BankTransactionVo findByBankTransactionIdAndBranchId(long bankTransactionId, long branchId) {
		return bankTransactionRepository.findByBankTransactionIdAndBranchIdAndIsDeleted(bankTransactionId, branchId, 0);
	}

	@Override
	public void deleteBankTransaction(long bankTransactionId, long branchId, long alterBy, String modifiedOn) {
		
		BankTransactionVo bankTransactionVo = bankTransactionRepository.findByBankTransactionIdAndBranchIdAndIsDeleted(bankTransactionId, branchId, 0);
		transactionService.deleteTransaction(bankTransactionVo.getBranchId(), bankTransactionId, bankTransactionVo.getTransactionType());
		
		bankTransactionRepository.delete(bankTransactionId, alterBy, modifiedOn);
	}

	@Override
	public long findMaxVoucherNo(String type, long companyId, long branchId, long userId, String defaultPrefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(defaultPrefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = bankTransactionRepository.findMaxVoucherNo(branchId, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
	}
	
	@Override
	public BankTransactionVo saveBankTransaction(BankTransactionVo bankTransactionVo) {
		
		bankTransactionVo = bankTransactionRepository.saveAndFlush(bankTransactionVo);
		entityManager.refresh(bankTransactionVo);
		
		return bankTransactionVo;
	}

	@Override
	public void saveTransaction(BankTransactionVo bankTransactionVo) {
		
		TransactionVo transactionVo;
		AccountCustomVo accountCustomVo;
		
		transactionService.deleteTransaction(bankTransactionVo.getBranchId(), bankTransactionVo.getBankTransactionId(), bankTransactionVo.getTransactionType());
		
		if(bankTransactionVo.getTransactionType().equals(Constant.BANK_WITHDRAW))
		{
			// Bank Entry
			transactionVo = new TransactionVo();
			
			transactionVo.setTransactionDate(bankTransactionVo.getTransactionDate());
			transactionVo.setBranchId(bankTransactionVo.getBranchId());
			transactionVo.setCompanyId(bankTransactionVo.getCompanyId());
			transactionVo.setVoucherType(bankTransactionVo.getTransactionType());
			transactionVo.setVoucherId(bankTransactionVo.getBankTransactionId());
			transactionVo.setVoucherNo(""+bankTransactionVo.getVoucherNo());
			transactionVo.setVoucherType(Constant.BANK_WITHDRAW);
			transactionVo.setDescription("Withdraw");
			
			transactionVo.setAccountCustomVo(bankTransactionVo.getFromBankVo().getAccountCustomVo());
			transactionVo.setAccountGroupVo(bankTransactionVo.getFromBankVo().getAccountCustomVo().getGroup());
			transactionService.save(transactionVo, bankTransactionVo.getDebitAmount(),TransactionAmount.CREDIT);
			//End Bank Entry
			
			
			// Cash Entry
			transactionVo = new TransactionVo();
			
			transactionVo.setTransactionDate(bankTransactionVo.getTransactionDate());
			transactionVo.setBranchId(bankTransactionVo.getBranchId());
			transactionVo.setCompanyId(bankTransactionVo.getCompanyId());
			transactionVo.setVoucherType(bankTransactionVo.getTransactionType());
			transactionVo.setVoucherId(bankTransactionVo.getBankTransactionId());
			transactionVo.setVoucherNo(""+bankTransactionVo.getVoucherNo());
			transactionVo.setVoucherType(Constant.BANK_WITHDRAW);
			transactionVo.setDescription("Withdraw");
			
			accountCustomVo = new AccountCustomVo();
			accountCustomVo = accountService.findByAccountNameAndBranchId(Constant.ACCOUNT_CASH, bankTransactionVo.getBranchId());
			
			transactionVo.setAccountCustomVo(accountCustomVo);
			transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
			transactionService.save(transactionVo, bankTransactionVo.getDebitAmount(), TransactionAmount.DEBIT);
			// End Cash Entry
		}
		else if(bankTransactionVo.getTransactionType().equals(Constant.BANK_DEPOSIT))
		{
			// Bank Entry
			transactionVo = new TransactionVo();
			
			transactionVo.setTransactionDate(bankTransactionVo.getTransactionDate());
			transactionVo.setBranchId(bankTransactionVo.getBranchId());
			transactionVo.setCompanyId(bankTransactionVo.getCompanyId());
			transactionVo.setVoucherType(bankTransactionVo.getTransactionType());
			transactionVo.setVoucherId(bankTransactionVo.getBankTransactionId());
			transactionVo.setVoucherNo(""+bankTransactionVo.getVoucherNo());
			transactionVo.setVoucherType(Constant.BANK_DEPOSIT);
			transactionVo.setDescription("Deposite");
			
			transactionVo.setAccountCustomVo(bankTransactionVo.getToBankVo().getAccountCustomVo());
			transactionVo.setAccountGroupVo(bankTransactionVo.getToBankVo().getAccountCustomVo().getGroup());
			transactionService.save(transactionVo, bankTransactionVo.getCreditAmount(), TransactionAmount.DEBIT);
			// End Bank Entry
			
			// Cash Entry
			transactionVo = new TransactionVo();
			
			transactionVo.setTransactionDate(bankTransactionVo.getTransactionDate());
			transactionVo.setBranchId(bankTransactionVo.getBranchId());
			transactionVo.setCompanyId(bankTransactionVo.getCompanyId());
			transactionVo.setVoucherType(bankTransactionVo.getTransactionType());
			transactionVo.setVoucherId(bankTransactionVo.getBankTransactionId());
			transactionVo.setVoucherNo(""+bankTransactionVo.getVoucherNo());
			transactionVo.setVoucherType(Constant.BANK_DEPOSIT);
			transactionVo.setDescription("Deposite");
			
			accountCustomVo = accountService.findByAccountNameAndBranchId(Constant.ACCOUNT_CASH, bankTransactionVo.getBranchId());
			transactionVo.setAccountCustomVo(accountCustomVo);
			transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
			transactionService.save(transactionVo, bankTransactionVo.getCreditAmount(), TransactionAmount.CREDIT);
			// End Cash Entry
		}
		else if(bankTransactionVo.getTransactionType().equals(Constant.BANK_TRANSFER))
		{
			// From Bank Entry
			transactionVo = new TransactionVo();
			
			transactionVo.setTransactionDate(bankTransactionVo.getTransactionDate());
			transactionVo.setBranchId(bankTransactionVo.getBranchId());
			transactionVo.setCompanyId(bankTransactionVo.getCompanyId());
			transactionVo.setVoucherType(bankTransactionVo.getTransactionType());
			transactionVo.setVoucherId(bankTransactionVo.getBankTransactionId());
			transactionVo.setVoucherNo(""+bankTransactionVo.getVoucherNo());
			transactionVo.setVoucherType(Constant.BANK_TRANSFER);
			transactionVo.setDescription("Transfer");
			
			transactionVo.setAccountCustomVo(bankTransactionVo.getFromBankVo().getAccountCustomVo());
			transactionVo.setAccountGroupVo(bankTransactionVo.getFromBankVo().getAccountCustomVo().getGroup());
			transactionService.save(transactionVo, bankTransactionVo.getDebitAmount(), TransactionAmount.CREDIT);
			// End From Bank Entry

			// To Bank Entry
			transactionVo = new TransactionVo();
			
			transactionVo.setTransactionDate(bankTransactionVo.getTransactionDate());
			transactionVo.setBranchId(bankTransactionVo.getBranchId());
			transactionVo.setCompanyId(bankTransactionVo.getCompanyId());
			transactionVo.setVoucherType(bankTransactionVo.getTransactionType());
			transactionVo.setVoucherId(bankTransactionVo.getBankTransactionId());
			transactionVo.setVoucherNo(""+bankTransactionVo.getVoucherNo());
			transactionVo.setVoucherType(Constant.BANK_TRANSFER);
			transactionVo.setDescription("Transfer");
			
			transactionVo.setAccountCustomVo(bankTransactionVo.getToBankVo().getAccountCustomVo());
			transactionVo.setAccountGroupVo(bankTransactionVo.getToBankVo().getAccountCustomVo().getGroup());
			transactionService.save(transactionVo, bankTransactionVo.getCreditAmount(), TransactionAmount.DEBIT);
			// End To Bank Entry
		}
		
	}

}
