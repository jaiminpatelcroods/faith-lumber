package com.croods.bssgroup.service.complain;

import java.util.Date;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.vo.complain.ComplainVo;

public interface ComplainService {

	public ComplainVo save(ComplainVo complainVo);
	
	long findMaxComplainNo(long companyId, long branchId, long userId, String type, String prefix);

	public ComplainVo findByComplainIdAndBranchId(long complainId, long branchId);
	
	public DataTablesOutput<ComplainVo> findAll(DataTablesInput input,
			Specification<ComplainVo> additionalSpecification, Specification<ComplainVo> specification);

	public void delete(long complainId, long alterBy, String modifiedOn);
	
	public void updateAssignedEmployee(long employeeId, long complainId, String status, long alterBy, String modifiedOn);

	public void complainAssignedEmployee( long complainId, String status, String correctiveActions,
			String preventiveAction, String remark, Date completedDate, long alterBy, String modifiedOn);
}
