package com.croods.bssgroup.service.product;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.apache.commons.lang.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.product.ProductRepository;
import com.croods.bssgroup.repository.product.ProductVariantRepository;
import com.croods.bssgroup.service.unitofmeasurement.UnitOfMeasurementService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.product.ProductVariantVo;
import com.croods.bssgroup.vo.product.ProductVo;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;
	
	@Autowired
	ProductVariantRepository productVariantRepository;
	
	@Autowired
	UnitOfMeasurementService unitOfMeasurementService;
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public ProductVo saveProduct(ProductVo productVo) {
		return productRepository.save(productVo);
	}
	
	@Override
	public ProductVo findByProductIdAndCompanyId(long productId, long companyId) {
		return productRepository.findByProductIdAndCompanyIdAndIsDeleted(productId, companyId, 0);
	}

	@Override
	public DataTablesOutput<ProductVo> findAll(DataTablesInput input, Specification<ProductVo> additionalSpecification,
			Specification<ProductVo> specification) {
		
		return productRepository.findAll(input, additionalSpecification, specification);
	}

	@Override
	public List<ProductVo> findByCompanyId(long companyId) {
		return productRepository.findByCompanyIdAndIsDeleted(companyId, 0);
	}
	
	@Override
	public List<ProductVo> getProductWithVariation(long companyId, boolean ispackage) {
		return productRepository.findByCompanyIdAndIsDeletedAndHaveVariationAndIsPackage(companyId, 0, 1,ispackage);
	}
	
	@Override
	public List<ProductVariantVo> findProductVariantByCompanyId(long companyId) {
		return productVariantRepository.findByCompanyIdAndIsDeletedAndProductVoIsPackageAndProductVoIsDeleted(companyId, 0, false, 0);
	}

	@Override
	public ProductVariantVo findProductVariantByIdAndCompanyId(long productVariantId, long companyId) {
		return productVariantRepository.findByProductVariantIdAndCompanyIdAndIsDeleted(productVariantId, companyId, 0);
	}

	@Override
	public List<ProductVariantVo> findProductVariants(String searchKey, long companyId) {
		return productVariantRepository.findProductVariants(searchKey, companyId);
	}
	
	@Override
	public List<ProductVariantVo> findProductVariantsWithPackage(String searchKey, long companyId) {
		return productVariantRepository.findProductVariantsWithPackage(searchKey, companyId);
	}

	@Override
	public long saveProductPackage(HttpSession session, Map<String, String> allRequestParams, ProductVo productVo) {
		long productId =0;
		ProductVo productVoOld = productRepository.findByProductIdAndCompanyIdAndIsDeleted(Long.parseLong(allRequestParams.get("productVo.productId")),Long.parseLong(session.getAttribute("companyId").toString()),0);
		
		ProductVo parentproductVo=new ProductVo();
		parentproductVo.setProductId(Long.parseLong(allRequestParams.get("productVo.productId")));
		
		ProductVo clone;
		try {
			clone = (ProductVo) productVoOld.clone();
		
		
			//entityManager.find(ProductVo.class, productVoOld.getProductId());
			//entityManager.detach(clone);
			clone.setProductId(productVo.getProductId());
			clone.setName(productVo.getName());
			clone.setProductVo(parentproductVo);
			clone.setDisplayName(productVo.getName());
			clone.setPackage(true);
			clone.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
			clone.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
			clone.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
			clone.setModifiedOn(CurrentDateTime.getCurrentDate());
			clone.setDescription("");
			clone.setUnitOfMeasurementVo(unitOfMeasurementService.findByMeasurementCodeAndCompanyId("PKG", Long.parseLong(session.getAttribute("companyId").toString())));
			
			clone.setProductVariantVos(productVo.getProductVariantVos());
			clone.setProductAttributeVos(productVo.getProductAttributeVos());
			clone.getProductVariantVos().forEach(p->{
				p.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
				p.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
				p.setPosition("0");
				p.setProductVo(clone);
			});
			
			//entityManager.persist(clone);
			
			productRepository.saveAndFlush(clone);
			//	entityManager.refresh(enquiryVo);
			productId = clone.getProductId();
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return  productId;
		//return productRepository.save(clone);
	}

	@Override
	public ProductVo findByParantId(long productId, long companyId) {
		return productRepository.findByProductVoProductIdAndCompanyIdAndIsDeleted(productId,companyId,0);
	}
	
	@Override
	public boolean isExistItemCode(long companyId, String itemCode) {
		if(productVariantRepository.isExistItemCode(companyId, itemCode) == null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isExistItemCode(long companyId, String itemCode, long productVariantId) {
		if(productVariantRepository.isExistItemCode(companyId, itemCode,productVariantId ) == null) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public ProductVariantVo findByItemCodeAndCompanyId(String itemCode, long companyId) {
		// TODO Auto-generated method stub
		return productVariantRepository.findByItemCodeAndCompanyId(itemCode, companyId);
	}

	@Override
	public long countProductByCompanyId(long companyId) {
		return productRepository.countByCompanyIdAndIsPackageAndIsDeleted(companyId, false,0);
	}

	@Override
	public List<ProductVariantVo> findProductVariantByIdIn(List<Long> productVariantIds) {
		return productVariantRepository.findByProductVariantIdIn(productVariantIds);
	}

	@Override
	public void deleteProduct(long productId, long userId, String modifiedOn) {
		productRepository.deleteProduct(productId, userId, modifiedOn);
		productRepository.deleteProductByParentId(productId, userId, modifiedOn);
	}
}
