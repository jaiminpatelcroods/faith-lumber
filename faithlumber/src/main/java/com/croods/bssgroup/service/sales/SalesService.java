package com.croods.bssgroup.service.sales;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.vo.sales.SalesAdditionalChargeVo;
import com.croods.bssgroup.vo.sales.SalesItemVo;
import com.croods.bssgroup.vo.sales.SalesVo;

public interface SalesService {

	DataTablesOutput<SalesVo> findAll(@Valid DataTablesInput input, Object object,
			Specification<SalesVo> specification);

	long findMaxSalesNo(long companyId, long branchId, String type, String defaultPrefix, long userId);

	boolean isExistSalesNo(long branchId, String type, String prefix, long salesNo);

	boolean isExistSalesNo(long branchId, String type, String prefix, long salesNo, long salesId);

	SalesVo findBySalesIdAndBranchId(long salesId, long branchId);

	void deleteSalesItemByIdIn(List<Long> l);

	void deleteSalesAdditionalChargeByIdIn(List<Long> l);

	SalesVo save(SalesVo salesVo);

	void deleteSales(long branchId, long salesId, String type);

	List<Map<Double, String>> getLast15DaySales(long branchId, String type);
	
	public  List<Map<String, String>> getSalesVSPurchase(long branchId,Date startDate,Date endDate);

	List<SalesVo> findByTypesAndContactAndBranchIdAndSalesDateBetween(List<String> salesTypes, long branchId,
			Date yearStartDate, Date yearEndDate, long contactId);

	SalesItemVo findByItemCodeAndCompanyIdAndSalesId(String itemCode, long companyId, long salesId);


	List<SalesVo> findUnpaidSalesByTypeAndBranchIdAndContactIdAndSalesDateBetween(String type, long parseLong,
			long contactId, Date parse, Date parse2);

	void updateOldPaymentMinus(long salesId, double oldPyament);

	void addPaidAmountBySalesId(long salesId, double payment, long userId, String modifiedOn);

	List<SalesItemVo> findItemBySalesId(long salesId);

	void insertSalesTransaction(SalesVo salesVo, String yearInterval);

	void insertSalesReturnTransaction(SalesVo salesVo, String yearInterval);

	List<Map<Double, Double>> getPaidAndTotalAmountByContactId(List<String> salesTypes, long branchId, Date startDate,
			Date endDate, long contactId);
	
	public double getDueAmountByContactId(List<String> types, long branchId, long contactId, Date startDate, Date endDate);

	public void updateAssignedEmployeeAndStatusBySalesId(long employeeId, String status, long salesId, long alterBy, String modifiedOn);

	public void updateStatusBySalesId(String status, long salesId, long parseLong, String currentDate);

	List<SalesAdditionalChargeVo> findAdditionalChargeBySalesId(long salesId);

	void updateSalesOrderItem(long salesItemId,long productVariantId, double productionQty, double oldProductionQty);
	
	SalesVo findBySalesId(long salesId);
	
	List<SalesVo> findByBranchIdAndTypeAndStatus(long branchId, String type, List<String>  status);
	
	double findQtyBySalesItemId(long salesItemId);
	
	double findProducedQtyBySalesItemId(long salesItemId);
	
	void updateSalesOrderItemForDelivery(long salesItemId,long productVariantId, double deliveryQty, double oldDeliveredQty);

	double findDeliveredQtyBySalesItemId(long salesItemId);

	double getAllProducedTotalQty(long branchId, Date fromDate, Date toDate);

	double getRemainsProductionQty(long branchId);

	double getTotalAvailableQty(long branchId, long productId, Date fromDate, Date toDate);

	double getVariantQty(long branchId, long productVariantId, Date fromDate, Date toDate);

}
