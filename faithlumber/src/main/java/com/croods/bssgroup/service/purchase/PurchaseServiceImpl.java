package com.croods.bssgroup.service.purchase;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.repository.purchase.PurchaseAdditionalChargeRepository;
import com.croods.bssgroup.repository.purchase.PurchaseItemRepository;
import com.croods.bssgroup.repository.purchase.PurchaseRepository;
import com.croods.bssgroup.service.account.AccountService;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.service.stock.StockTransactionService;
import com.croods.bssgroup.service.transaction.TransactionService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.util.TransactionAmount;
import com.croods.bssgroup.vo.account.AccountCustomVo;
import com.croods.bssgroup.vo.account.AccountGroupVo;
import com.croods.bssgroup.vo.prefix.PrefixVo;
import com.croods.bssgroup.vo.purchase.PurchaseAdditionalChargeVo;
import com.croods.bssgroup.vo.purchase.PurchaseItemVo;
import com.croods.bssgroup.vo.purchase.PurchaseVo;
import com.croods.bssgroup.vo.transaction.TransactionVo;

@Service
@Transactional
public class PurchaseServiceImpl implements PurchaseService {

	@Autowired
	PurchaseRepository purchaseRepository;
	
	@Autowired
	PurchaseItemRepository purchaseItemRepository;
	
	@Autowired
	PurchaseAdditionalChargeRepository purchaseAdditionalChargeRepository;
	
	@Autowired
	PrefixService prefixService;
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired
	TransactionService transactionService;
	
	@Autowired
	StockTransactionService stockTransactionService;
	
	@Autowired
	AccountService accountCustomService;
	
	@Override
	public long findMaxPurchaseNo(long companyId, long branchId, long userId, String type, String defaultPrefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(defaultPrefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = purchaseRepository.findMaxPurchaseNo(branchId, type, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
		
	}

	@Override
	public PurchaseVo findByPurchaseIdAndBranchId(long purchaseId, long branchId) {
		return purchaseRepository.findByPurchaseIdAndBranchIdAndIsDeleted(purchaseId, branchId, 0);
	}

	@Override
	public void delete(long purchaseId, long branchId, long userId, String type, String modifiedOn) {
		
		if(type.equals(Constant.PURCHASE_BILL)) {
			transactionService.deleteTransaction(branchId, purchaseId, type);
		} else if(type.equals(Constant.PURCHASE_DEBIT_NOTE)) {
			stockTransactionService.deleteStockTransaction(branchId, purchaseId, Constant.PURCHASE_DEBIT_NOTE);
			transactionService.deleteTransaction(branchId, purchaseId, Constant.PURCHASE_DEBIT_NOTE);
		}
		purchaseRepository.delete(purchaseId, userId, modifiedOn);
	}

	@Override
	public void deletePurchaseItemByIdIn(List<Long> purchaseItemIds) {
		purchaseItemRepository.deletePurchaseItemByIdIn(purchaseItemIds);
	}

	@Override
	public DataTablesOutput<PurchaseVo> findAll(@Valid DataTablesInput input,
			Specification<PurchaseVo> additionalSpecification, Specification<PurchaseVo> preFilteringSpecification) {
		return purchaseRepository.findAll(input, null, preFilteringSpecification);
	}

	@Override
	public boolean isExistPurchaesNo(long branchId, String type, String prefix, long purchaseNo) {
		
		if(purchaseRepository.isExistPurchaesNo(branchId, type, prefix, purchaseNo) == null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isExistPurchaesNo(long branchId, String type, String prefix, long purchaseNo, long purchaseId) {
		
		if(purchaseRepository.isExistPurchaesNo(branchId, type, prefix, purchaseNo, purchaseId) == null) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void deletePurchaseAdditionalChargeByIdIn(List<Long> purchaseAdditionalChargeIds) {
		purchaseAdditionalChargeRepository.deletePurchaseAdditionalChargeByIdIn(purchaseAdditionalChargeIds);
	}

	@Override
	public PurchaseVo save(PurchaseVo purchaseVo) {
		purchaseVo = purchaseRepository.saveAndFlush(purchaseVo);
		entityManager.refresh(purchaseVo);
		return purchaseVo;
	}

	@Override
	public List<PurchaseVo> findUnpaidPurchaseByTypeAndBranchIdAndContactIdAndPurchaseDateBetween(String type, long branchId,
			long contactId, Date startDate, Date endDate) {
		return purchaseRepository.findUnpaidPurchaseByTypeAndBranchIdAndContactIdAndIsDeletedAndPurchaseDateBetween(type, branchId, contactId, startDate, endDate);
	}

	@Override
	public void addPaidAmountByPurchaseId(long purchaseId, double payment, long userId, String modifiedOn) {
		purchaseRepository.addPaidAmountByPurchaseId(purchaseId, payment, userId, modifiedOn);
	}

	@Override
	public void insertPurchaseBillTransaction(PurchaseVo purchaseVo, String yearInterval) {

		
		PurchaseVo purchaseVo3= purchaseRepository.findByPurchaseIdAndBranchIdAndIsDeleted(purchaseVo.getPurchaseId(), purchaseVo.getBranchId(),0);
		purchaseVo3.getContactVo().getAccountCustomVo().getGroup();
		
		AccountCustomVo accountCustomVo;
		TransactionVo transactionVo;
		AccountGroupVo accountGroupVo;
		purchaseVo.getContactVo().getAccountCustomVo().getGroup();
		//List<TransactionVo> transactionVos = new ArrayList<TransactionVo>();
		
		//Update Stock
		//stockTransactionService.deleteStockTransaction(purchaseVo.getBranchId(), purchaseVo.getPurchaseId(), Constant.PURCHASE_BILL);
		//stockTransactionService.saveStockTransactionFromPurchaseBill(purchaseVo.getPurchaseItemVos(), yearInterval);
		//End Update Stock
		
		transactionService.deleteTransaction(purchaseVo.getBranchId(), purchaseVo.getPurchaseId(), purchaseVo.getType());
		
		double taxableAmount = purchaseVo.getPurchaseItemVos().stream()
				.mapToDouble(o -> (o.getQty() * o.getPrice()) - (o.getDiscountType().equals("amount") ? o.getDiscount()
						: (o.getQty() * o.getPrice()) * (o.getDiscount() / 100)))
				.sum();
		
		double totalDiscount = purchaseVo.getPurchaseItemVos().stream().mapToDouble(d -> d.getDiscountType().equals("amount") ? d.getDiscount()
				: (d.getQty() * d.getPrice()) * (d.getDiscount() / 100)).sum();
		
		transactionVo = new TransactionVo();
		
		//Supplier Entry
		transactionVo.setAccountCustomVo(purchaseVo.getContactVo().getAccountCustomVo());
		
		transactionVo.setAccountGroupVo(purchaseVo.getContactVo().getAccountCustomVo().getGroup());
		
		transactionVo.setBranchId(purchaseVo.getBranchId());
		transactionVo.setCompanyId(purchaseVo.getCompanyId());
		transactionVo.setDescription(purchaseVo.getBillNo());
		transactionVo.setTransactionDate(purchaseVo.getPurchaseDate());
		transactionVo.setVoucherId(purchaseVo.getPurchaseId());
		transactionVo.setVoucherNo(purchaseVo.getBillNo());
		transactionVo.setVoucherType(purchaseVo.getType());
		
		transactionVo.setTransactionId(0);
		transactionService.save(transactionVo, purchaseVo.getTotal(), TransactionAmount.CREDIT);
		//End Supplier Entry
		
		//Purchase Account Entry
		
		transactionVo = new TransactionVo();
		transactionVo.setBranchId(purchaseVo.getBranchId());
		transactionVo.setCompanyId(purchaseVo.getCompanyId());
		transactionVo.setDescription(purchaseVo.getBillNo());
		transactionVo.setTransactionDate(purchaseVo.getPurchaseDate());
		transactionVo.setVoucherId(purchaseVo.getPurchaseId());
		transactionVo.setVoucherNo(purchaseVo.getBillNo());
		transactionVo.setVoucherType(purchaseVo.getType());
		
		accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_PURCHASE, purchaseVo.getBranchId());
		
		transactionVo.setAccountCustomVo(accountCustomVo);
		transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
		
		transactionService.save(transactionVo, taxableAmount + totalDiscount, TransactionAmount.DEBIT);
		//End Purchase Account Entry
		
		
		//Tax Entry
		Map<Long, Double> taxes =  
			    purchaseVo.getPurchaseItemVos().stream().collect(Collectors.groupingBy(x -> x.getTaxVo().getTaxId() , Collectors.summingDouble(PurchaseItemVo::getTaxAmount)));
		
		taxes.forEach((key, value) -> {
			AccountCustomVo ac = purchaseVo.getPurchaseItemVos().stream().filter(c -> c.getTaxVo().getTaxId() == key)
					.collect(Collectors.toList()).get(0).getTaxVo().getAccountCustomVo();
			
			TransactionVo transactionVo2 = new TransactionVo();
			transactionVo2.setBranchId(purchaseVo.getBranchId());
			transactionVo2.setCompanyId(purchaseVo.getCompanyId());
			transactionVo2.setDescription(purchaseVo.getBillNo());
			transactionVo2.setTransactionDate(purchaseVo.getPurchaseDate());
			transactionVo2.setVoucherId(purchaseVo.getPurchaseId());
			transactionVo2.setVoucherNo(purchaseVo.getBillNo());
			transactionVo2.setVoucherType(purchaseVo.getType());
			
			transactionVo2.setAccountCustomVo(ac);
			transactionVo2.setAccountGroupVo(ac.getGroup());
			
			transactionService.save(transactionVo2, value, TransactionAmount.DEBIT);
		});
		//End Tax Entry
		
		//Discount Entry
		if(totalDiscount != 0.0) {
			transactionVo = new TransactionVo();
			transactionVo.setBranchId(purchaseVo.getBranchId());
			transactionVo.setCompanyId(purchaseVo.getCompanyId());
			transactionVo.setDescription(purchaseVo.getBillNo());
			transactionVo.setTransactionDate(purchaseVo.getPurchaseDate());
			transactionVo.setVoucherId(purchaseVo.getPurchaseId());
			transactionVo.setVoucherNo(purchaseVo.getBillNo());
			transactionVo.setVoucherType(purchaseVo.getType());
			
			accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_DISCOUNT_RECEIVED, purchaseVo.getBranchId());
			
			transactionVo.setAccountCustomVo(accountCustomVo);
			transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
			
			transactionService.save(transactionVo, totalDiscount, TransactionAmount.CREDIT	);
		}
		//End Discount Entry
		
		//Roundoff Entry
		if(purchaseVo.getRoundoff() != 0.0) {
			transactionVo = new TransactionVo();
			transactionVo.setBranchId(purchaseVo.getBranchId());
			transactionVo.setCompanyId(purchaseVo.getCompanyId());
			transactionVo.setDescription(purchaseVo.getBillNo());
			transactionVo.setTransactionDate(purchaseVo.getPurchaseDate());
			transactionVo.setVoucherId(purchaseVo.getPurchaseId());
			transactionVo.setVoucherNo(purchaseVo.getBillNo());
			transactionVo.setVoucherType(purchaseVo.getType());
			
			if(purchaseVo.getRoundoff() > 0) {
				accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_ROUNDOFF_EXPENSE, purchaseVo.getBranchId());
				
				transactionVo.setAccountCustomVo(accountCustomVo);
				transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
				transactionService.save(transactionVo, totalDiscount, TransactionAmount.DEBIT);
			} else if(purchaseVo.getRoundoff() > 0) {
				accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_ROUNDOFF_INCOME, purchaseVo.getBranchId());
				
				transactionVo.setAccountCustomVo(accountCustomVo);
				transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
				transactionService.save(transactionVo, totalDiscount, TransactionAmount.CREDIT);
			}
		}
		//End Roundoff Entry
		
		//Additional Charge Entry
		if(purchaseVo.getPurchaseAdditionalChargeVos() != null)
			for (PurchaseAdditionalChargeVo charge : purchaseVo.getPurchaseAdditionalChargeVos()) {
				
				transactionVo = new TransactionVo();
				transactionVo.setBranchId(purchaseVo.getBranchId());
				transactionVo.setCompanyId(purchaseVo.getCompanyId());
				transactionVo.setDescription(purchaseVo.getBillNo());
				transactionVo.setTransactionDate(purchaseVo.getPurchaseDate());
				transactionVo.setVoucherId(purchaseVo.getPurchaseId());
				transactionVo.setVoucherNo(purchaseVo.getBillNo());
				transactionVo.setVoucherType(purchaseVo.getType());
				
				transactionVo.setAccountCustomVo(charge.getAdditionalChargeVo().getAccountCustomVo());
				transactionVo.setAccountGroupVo(charge.getAdditionalChargeVo().getAccountCustomVo().getGroup());
				
				transactionService.save(transactionVo, charge.getAmount(), TransactionAmount.DEBIT);
				
				transactionVo = new TransactionVo();
				transactionVo.setBranchId(purchaseVo.getBranchId());
				transactionVo.setCompanyId(purchaseVo.getCompanyId());
				transactionVo.setDescription(purchaseVo.getBillNo());
				transactionVo.setTransactionDate(purchaseVo.getPurchaseDate());
				transactionVo.setVoucherId(purchaseVo.getPurchaseId());
				transactionVo.setVoucherNo(purchaseVo.getBillNo());
				transactionVo.setVoucherType(purchaseVo.getType());
				
				transactionVo.setAccountCustomVo(charge.getTaxVo().getAccountCustomVo());
				transactionVo.setAccountGroupVo(charge.getTaxVo().getAccountCustomVo().getGroup());
				
				transactionService.save(transactionVo, charge.getTaxAmount(), TransactionAmount.DEBIT);
			}
		//End Additional Charge Entry
	}

	@Override
	public void insertPurchaseDebitNoteTransaction(PurchaseVo purchaseVo, String yearInterval) {
		// TODO Auto-generated method stub
		
		AccountCustomVo accountCustomVo;
		TransactionVo transactionVo;
		AccountGroupVo accountGroupVo;
		
		//List<TransactionVo> transactionVos = new ArrayList<TransactionVo>();
		//Update Stock
		stockTransactionService.deleteStockTransaction(purchaseVo.getBranchId(), purchaseVo.getPurchaseId(), purchaseVo.getType());
		stockTransactionService.saveStockTransactionFromPurchaseDebitNote(purchaseVo.getPurchaseItemVos(), yearInterval);
		//End Update Stock
		transactionService.deleteTransaction(purchaseVo.getBranchId(), purchaseVo.getPurchaseId(), purchaseVo.getType());
		
		double taxableAmount = purchaseVo.getPurchaseItemVos().stream()
				.mapToDouble(o -> (o.getQty() * o.getPrice()) - (o.getDiscountType().equals("amount") ? o.getDiscount()
						: (o.getQty() * o.getPrice()) * (o.getDiscount() / 100)))
				.sum();
		
		double totalDiscount = purchaseVo.getPurchaseItemVos().stream().mapToDouble(d -> d.getDiscountType().equals("amount") ? d.getDiscount()
				: (d.getQty() * d.getPrice()) * (d.getDiscount() / 100)).sum();
		
		transactionVo = new TransactionVo();

		//Supplier Entry
		transactionVo.setAccountCustomVo(purchaseVo.getContactVo().getAccountCustomVo());
		
		transactionVo.setAccountGroupVo(purchaseVo.getContactVo().getAccountCustomVo().getGroup());
		
		transactionVo.setBranchId(purchaseVo.getBranchId());
		transactionVo.setCompanyId(purchaseVo.getCompanyId());
		transactionVo.setDescription(purchaseVo.getBillNo());
		transactionVo.setTransactionDate(purchaseVo.getPurchaseDate());
		transactionVo.setVoucherId(purchaseVo.getPurchaseId());
		transactionVo.setVoucherNo(purchaseVo.getBillNo());
		transactionVo.setVoucherType(purchaseVo.getType());
		
		transactionVo.setTransactionId(0);
		transactionService.save(transactionVo, purchaseVo.getTotal(), TransactionAmount.DEBIT);
		//End Supplier Entry
		
		//Purchase Account Entry
		
		transactionVo = new TransactionVo();
		transactionVo.setBranchId(purchaseVo.getBranchId());
		transactionVo.setCompanyId(purchaseVo.getCompanyId());
		transactionVo.setDescription(purchaseVo.getBillNo());
		transactionVo.setTransactionDate(purchaseVo.getPurchaseDate());
		transactionVo.setVoucherId(purchaseVo.getPurchaseId());
		transactionVo.setVoucherNo(purchaseVo.getBillNo());
		transactionVo.setVoucherType(purchaseVo.getType());
		
		accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_PURCHASE_RETURN, purchaseVo.getBranchId());
		
		transactionVo.setAccountCustomVo(accountCustomVo);
		transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
		
		transactionService.save(transactionVo, taxableAmount + totalDiscount, TransactionAmount.CREDIT);
		//End Purchase Account Entry
		
		
		//Tax Entry
		Map<Long, Double> taxes =  
			    purchaseVo.getPurchaseItemVos().stream().collect(Collectors.groupingBy(x -> x.getTaxVo().getTaxId() , Collectors.summingDouble(PurchaseItemVo::getTaxAmount)));
		
		taxes.forEach((key, value) -> {
			AccountCustomVo ac = purchaseVo.getPurchaseItemVos().stream().filter(c -> c.getTaxVo().getTaxId() == key)
					.collect(Collectors.toList()).get(0).getTaxVo().getAccountCustomVo();
			
			TransactionVo transactionVo2 = new TransactionVo();
			transactionVo2.setBranchId(purchaseVo.getBranchId());
			transactionVo2.setCompanyId(purchaseVo.getCompanyId());
			transactionVo2.setDescription(purchaseVo.getBillNo());
			transactionVo2.setTransactionDate(purchaseVo.getPurchaseDate());
			transactionVo2.setVoucherId(purchaseVo.getPurchaseId());
			transactionVo2.setVoucherNo(purchaseVo.getBillNo());
			transactionVo2.setVoucherType(purchaseVo.getType());
			transactionVo2.setAccountCustomVo(ac);
			transactionVo2.setAccountGroupVo(ac.getGroup());
			
			transactionService.save(transactionVo2, value, TransactionAmount.CREDIT);
		});
		//End Tax Entry
		
		//Discount Entry
		if(totalDiscount != 0.0) {
			transactionVo = new TransactionVo();
			transactionVo.setBranchId(purchaseVo.getBranchId());
			transactionVo.setCompanyId(purchaseVo.getCompanyId());
			transactionVo.setDescription(purchaseVo.getBillNo());
			transactionVo.setTransactionDate(purchaseVo.getPurchaseDate());
			transactionVo.setVoucherId(purchaseVo.getPurchaseId());
			transactionVo.setVoucherNo(purchaseVo.getBillNo());
			transactionVo.setVoucherType(purchaseVo.getType());
			
			accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_DISCOUNT_GIVEN, purchaseVo.getBranchId());
			
			transactionVo.setAccountCustomVo(accountCustomVo);
			transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
			
			transactionService.save(transactionVo, totalDiscount, TransactionAmount.DEBIT);
		}
		//End Discount Entry
		
		//Roundoff Entry
		if(purchaseVo.getRoundoff() != 0.0) {
			transactionVo = new TransactionVo();
			transactionVo.setBranchId(purchaseVo.getBranchId());
			transactionVo.setCompanyId(purchaseVo.getCompanyId());
			transactionVo.setDescription(purchaseVo.getBillNo());
			transactionVo.setTransactionDate(purchaseVo.getPurchaseDate());
			transactionVo.setVoucherId(purchaseVo.getPurchaseId());
			transactionVo.setVoucherNo(purchaseVo.getBillNo());
			transactionVo.setVoucherType(purchaseVo.getType());
			
			if(purchaseVo.getRoundoff() > 0) {
				accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_ROUNDOFF_INCOME, purchaseVo.getBranchId());
				
				transactionVo.setAccountCustomVo(accountCustomVo);
				transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
				transactionService.save(transactionVo, totalDiscount, TransactionAmount.CREDIT);
			} else if(purchaseVo.getRoundoff() > 0) {////baki
				accountCustomVo = accountCustomService.findByAccountNameAndBranchId(Constant.ACCOUNT_ROUNDOFF_EXPENSE, purchaseVo.getBranchId());
				
				transactionVo.setAccountCustomVo(accountCustomVo);
				transactionVo.setAccountGroupVo(accountCustomVo.getGroup());
				transactionService.save(transactionVo, totalDiscount, TransactionAmount.DEBIT);
			}
		}
		//End Roundoff Entry
		
		//Additional Charge Entry
		if(purchaseVo.getPurchaseAdditionalChargeVos() != null)
			for (PurchaseAdditionalChargeVo charge : purchaseVo.getPurchaseAdditionalChargeVos()) {
				
				transactionVo = new TransactionVo();
				transactionVo.setBranchId(purchaseVo.getBranchId());
				transactionVo.setCompanyId(purchaseVo.getCompanyId());
				transactionVo.setDescription(purchaseVo.getBillNo());
				transactionVo.setTransactionDate(purchaseVo.getPurchaseDate());
				transactionVo.setVoucherId(purchaseVo.getPurchaseId());
				transactionVo.setVoucherNo(purchaseVo.getBillNo());
				transactionVo.setVoucherType(purchaseVo.getType());
				
				transactionVo.setAccountCustomVo(charge.getAdditionalChargeVo().getAccountCustomVo());
				transactionVo.setAccountGroupVo(charge.getAdditionalChargeVo().getAccountCustomVo().getGroup());
				
				transactionService.save(transactionVo, charge.getAmount(), TransactionAmount.CREDIT);
				
				transactionVo = new TransactionVo();
				transactionVo.setBranchId(purchaseVo.getBranchId());
				transactionVo.setCompanyId(purchaseVo.getCompanyId());
				transactionVo.setDescription(purchaseVo.getBillNo());
				transactionVo.setTransactionDate(purchaseVo.getPurchaseDate());
				transactionVo.setVoucherId(purchaseVo.getPurchaseId());
				transactionVo.setVoucherNo(purchaseVo.getBillNo());
				transactionVo.setVoucherType(purchaseVo.getType());
				
				transactionVo.setAccountCustomVo(charge.getTaxVo().getAccountCustomVo());
				transactionVo.setAccountGroupVo(charge.getTaxVo().getAccountCustomVo().getGroup());
				
				transactionService.save(transactionVo, charge.getTaxAmount(), TransactionAmount.CREDIT);
			}
		//End Additional Charge Entry
	}

	@Override
	public void updateOldPaymentMinus(long purchaseId, double oldPyament) {
		purchaseRepository.updateOldPaymentMinus(purchaseId,oldPyament);
		
	}

	@Override
	public List<PurchaseVo> findByTypeAndBranchIdAndContactId(String type, long branchId, long contactId,
			Date startDate, Date endDate) {
		return purchaseRepository.findByTypeAndBranchIdAndContactId(type, branchId, contactId, startDate, endDate);
	}

	@Override
	public List<PurchaseItemVo> findItemByPurchaseId(long purchaseId) {
		return purchaseItemRepository.findByPurchaseVoPurchaseId(purchaseId);
	}

	@Override
	public List<Map<Double, Double>> getPaidAndTotalAmountByContactId(List<String> purchaseTypes, long branchId,
			Date startDate, Date endDate, long contactId) {
		return purchaseRepository.getPaidAndTotalAmountByContactId(purchaseTypes, branchId, startDate, endDate, contactId);
	}

	@Override
	public double getDueAmountByContactId(List<String> purchaseTypes, long branchId, long contactId, Date startDate,
			Date endDate) {
		try {
			return purchaseRepository.getDueAmountByContactId(purchaseTypes, branchId, contactId, startDate, endDate);
		} catch(Exception e) {
			return 0;
		}
	}

	@Override
	public String findPurchaseNoByPurchaseId(long purchaseId) {
		return purchaseRepository.findPurchaseNoByPurchaseId(purchaseId);
	}

}
