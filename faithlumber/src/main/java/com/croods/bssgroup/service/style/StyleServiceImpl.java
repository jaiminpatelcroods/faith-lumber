package com.croods.bssgroup.service.style;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.style.StyleRepository;
import com.croods.bssgroup.vo.style.StyleVo;

@Service
@Transactional
public class StyleServiceImpl implements StyleService {

	@Autowired
	StyleRepository styleRepository;
	
	@Override
	public void save(StyleVo styleVo) {
		styleRepository.save(styleVo);
	}

	@Override
	public StyleVo findByStyleId(long styleId) {
		return styleRepository.findByStyleId(styleId);
	}

	@Override
	public void delete(long styleId, long alterBy, String modifiedOn) { 
		styleRepository.delete(styleId, alterBy, modifiedOn);
	}

	@Override
	public List<StyleVo> findByCompanyId(long companyId) {
		return styleRepository.findByCompanyIdAndIsDeletedOrderByStyleIdDesc(companyId, 0);
	}

	@Override
	public boolean isExistStyle(long companyId, String styleName) {
		if(styleRepository.isExistStyle(companyId, styleName) == null) {
			return true;
		} else {
			return false;
		}
	}	
		


	@Override
	public boolean isExistStyle(long companyId, String styleName, long styleId) {
		if(styleRepository.isExistStyle(companyId, styleName, styleId) == null) {
			return true;
		} else {
			return false;
		}
	}	

}
