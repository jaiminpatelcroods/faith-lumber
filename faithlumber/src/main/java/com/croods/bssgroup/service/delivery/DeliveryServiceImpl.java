package com.croods.bssgroup.service.delivery;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.delivery.DeliveryItemRepository;
import com.croods.bssgroup.repository.delivery.DeliveryRepository;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.delivery.DeliveryItemVo;
import com.croods.bssgroup.vo.delivery.DeliveryVo;
import com.croods.bssgroup.vo.prefix.PrefixVo;

@Service
@Transactional
public class DeliveryServiceImpl implements DeliveryService {

	@Autowired
	DeliveryRepository deliveryRepository;
	
	@Autowired
	DeliveryItemRepository deliveryItemRepository;
	
	@Autowired
	PrefixService prefixService;
	
	@PersistenceContext
	EntityManager entityManager;

	@Override
	public DataTablesOutput<DeliveryVo> findAll(DataTablesInput input,
			Specification<DeliveryVo> additionalSpecification, Specification<DeliveryVo> specification) {
		
		return deliveryRepository.findAll(input, additionalSpecification, specification);
	}

	@Override
	public DeliveryVo save(DeliveryVo deliveryVo) {
		deliveryVo = deliveryRepository.saveAndFlush(deliveryVo);
		entityManager.refresh(deliveryVo);
		return deliveryVo;
	}
	
	@Override
	public void deleteDeliveryItemIds(List<Long> deliveryItemIds) {
		deliveryItemRepository.deleteDeliveryItemIds(deliveryItemIds);
	}

	@Override
	public DeliveryVo findByDeliveryIdAndBranchId(long deliveryId, long branchId) {
		return deliveryRepository.findByDeliveryIdAndBranchId(deliveryId, branchId);
	}

	@Override
	public long findMaxDeliveryNo(String type, long companyId, long branchId, long userId,
			String defaultPrefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(defaultPrefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = deliveryRepository.findMaxDeliveryNo(branchId, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
		
	}

	@Override
	public void delete(long deliveryId, long userId, String modifiedOn) {
		deliveryRepository.delete(deliveryId, userId, modifiedOn);
	}

	@Override
	public List<DeliveryVo> findByBranchId(long branchId) {
		return deliveryRepository.findByBranchIdAndIsDeleted(branchId, 0);
	}

	@Override
	public List<DeliveryItemVo> findItemByDeliveryId(long deliveryId) {
		return deliveryItemRepository.findByDeliveryVoDeliveryId(deliveryId);
	}

}
