package com.croods.bssgroup.service.department;

import java.util.List;

import com.croods.bssgroup.vo.department.DepartmentVo;

public interface DepartmentService {

	public void save(DepartmentVo departmentVo);
	
	public DepartmentVo findByDepartmentId(long departmentId);
	
	public void deleteDepartment(long departmentId, long alterBy, String modifiedOn);

	public List<DepartmentVo> findByCompanyId(long companyId);
	
	public boolean isExistDepartment(long companyId, String departmentName);

	public boolean isExistDepartment(long companyId, String departmentName, long departmentId);
	
}
