package com.croods.bssgroup.service.sms;


import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.vo.sms.SmsVo;

public interface SmsService {

	public void smsSave(SmsVo smsVo);
	
	public List<SmsVo> findByCompanyId(long companyId);

	DataTablesOutput<SmsVo> findAll(@Valid DataTablesInput input, Object object,
			Specification<SmsVo> specification);
}
