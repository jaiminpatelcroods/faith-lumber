package com.croods.bssgroup.service.purchaserequest;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.vo.purchaserequest.PurchaseRequestItemVo;
import com.croods.bssgroup.vo.purchaserequest.PurchaseRequestVo;

public interface PurchaseRequestService {

	public long findMaxRequestNo(String purchaseRequest, long companyId, long branchId, long userId, String prefix);

	public void save(PurchaseRequestVo purchaseRequestVo);

	public PurchaseRequestVo findByPurchaseRequestIdAndBranchId(long purchaseRequestId, long branchId);

	public void deletePurchaseRequestItemByIdIn(List<Long> purchaseRequestItemIds);

	public void delete(long purchaseRequestId, long userId, String modifiedOn);

	public DataTablesOutput<PurchaseRequestVo> findAll(DataTablesInput input,
			Specification<PurchaseRequestVo> additionalSpecification, Specification<PurchaseRequestVo> specification);

	public List<PurchaseRequestItemVo> findItemByPurchaseRequestId(long purchaseRequestId);

	public void updateStatusAndExpectedDateByPurchaseRequestId(String status, Date deliveryDate, long alterBy, String modifiedOn,
			long purchaseRequestId);

	public String findRequestNoByPurchaseRequestId(long purchaseRequestId);

	public void updateStatusByPurchaseId(String status, long alterBy, String modifiedOn, long purchaseId);

}
