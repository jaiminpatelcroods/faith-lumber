package com.croods.bssgroup.service.prefix;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.prefix.PrefixRepository;
import com.croods.bssgroup.vo.prefix.PrefixVo;

@Service
@Transactional
public class PrefixServiceImpl implements PrefixService {

	@Autowired
	PrefixRepository prefixRepository;
	
	@Override
	public List<PrefixVo> findByBranchId(long branchId) {
		return prefixRepository.findByBranchIdAndIsDeleted(branchId, 0);
	}

	@Override
	public PrefixVo findByPrefixId(long prefixId) {
		return prefixRepository.findByPrefixId(prefixId);
	}

	@Override
	public List<PrefixVo> findByPrefixTypeAndBranchId(String prefixType, long branchId) {
		return prefixRepository.findByPrefixTypeAndBranchId(prefixType, branchId);
	}

	@Override
	public void save(PrefixVo prefixVo) {
		prefixRepository.save(prefixVo);
	}
	
}
