package com.croods.bssgroup.service.purchase;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.vo.purchase.PurchaseItemVo;
import com.croods.bssgroup.vo.purchase.PurchaseVo;

public interface PurchaseService {
	
	long findMaxPurchaseNo(long companyId, long branchId, long userId, String type, String prefix);

	PurchaseVo findByPurchaseIdAndBranchId(long purchaseId, long branchId);
	
	void delete(long purchaseId, long branchId, long userId, String type, String modifiedOn);
	
	void deletePurchaseItemByIdIn(List<Long> purchaseItemIds);

	DataTablesOutput<PurchaseVo> findAll(@Valid DataTablesInput input, Specification<PurchaseVo> additionalSpecification,
			Specification<PurchaseVo> preFilteringSpecification);

	boolean isExistPurchaesNo(long branchId, String type, String prefix, long purchaseNo);

	boolean isExistPurchaesNo(long branchId, String type, String prefix, long purchaseNo, long purchaseId);
	
	void deletePurchaseAdditionalChargeByIdIn(List<Long> purchaseAdditionalChargeIds);

	public PurchaseVo save(PurchaseVo purchaseVo);

	List<PurchaseVo> findUnpaidPurchaseByTypeAndBranchIdAndContactIdAndPurchaseDateBetween(String type, long branchId,
			long contactId, Date startDate, Date endDate);
	
	List<PurchaseVo> findByTypeAndBranchIdAndContactId(String type, long branchId, long contactId, Date startDate, Date endDate);
	
	void addPaidAmountByPurchaseId(long purchaseId, double payment, long userId, String modifiedOn);
	
	void insertPurchaseBillTransaction(PurchaseVo purchaseVo, String yearInterval);
	
	void insertPurchaseDebitNoteTransaction(PurchaseVo purchaseVo, String yearInterval);

	void updateOldPaymentMinus(long purchaseId, double oldPyament);

	List<PurchaseItemVo> findItemByPurchaseId(long purchaseId);

	List<Map<Double, Double>> getPaidAndTotalAmountByContactId(List<String> purchaseTypes, long branchId, Date startDate,
			Date endDate, long contactId);

	double getDueAmountByContactId(List<String> purchaseTypes, long branchId, long contactId, Date startDate, Date endDate);

	String findPurchaseNoByPurchaseId(long purchaseId);
}
