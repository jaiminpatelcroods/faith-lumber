package com.croods.bssgroup.service.leadsource;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.croods.bssgroup.repository.leadsource.LeadSourceRepository;
import com.croods.bssgroup.vo.leadsource.LeadSourceVo;

@Service
@Transactional
public class LeadSourceServiceImpl implements LeadSourceService{

	@Autowired
	LeadSourceService leadSourceService;
	
	@Autowired
	LeadSourceRepository leadSourceRepository;
	
	@Override
	public LeadSourceVo findByLeadSourceId(long leadSourceId) {
		return leadSourceRepository.findByLeadSourceId(leadSourceId);
	}

	
	@Override
	public void deleteLeadSource(long leadSourceId, long alterBy, String modifiedOn) {
		leadSourceRepository.deleteLeadSource(leadSourceId, alterBy, modifiedOn);
		
	}

	@Override
	public List<LeadSourceVo> findByCompanyId(long companyId) {
		return leadSourceRepository.findByCompanyIdAndIsDeleted(companyId,0);
	}

	@Override
	public List<LeadSourceVo> findByCompanyIdAndIsEditable(long companyId) {
		return leadSourceRepository.findByCompanyIdAndIsDeletedAndIsEditableOrderByLeadSourceIdDesc(companyId, 0, true);
	}
	@Override
	public void save(LeadSourceVo leadSourceVo) {
		leadSourceRepository.save(leadSourceVo);
	}


	@Override
	public boolean isExistLeadSource(long companyId, String sourceName) {
		if(leadSourceRepository.isExistLeadSource(companyId, sourceName) == null) {
			return true;
		} else {
			return false;
		}
	}
		@Override
	public boolean isExistLeadSource(long companyId, String sourceName, long leadSourceId) {
		if(leadSourceRepository.isExistLeadSource(companyId, sourceName, leadSourceId) == null) {
			return true;
		} else {
			return false;
		}
	}
	

}

	
	
