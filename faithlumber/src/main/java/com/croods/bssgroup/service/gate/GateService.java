package com.croods.bssgroup.service.gate;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.vo.gate.GateVo;

public interface GateService {

	public GateVo save(GateVo gateVo);

	public DataTablesOutput<GateVo> findAll( DataTablesInput input, Specification<GateVo> additionalSpecification,
			Specification<GateVo> specification);

	public GateVo findByGateIdAndBranchId(long gateId, long branchId);

	public void delete(long id, long userId, String modifiedOn);

	public GateVo findByGateId(long gateId);

}
