package com.croods.bssgroup.service.materialrequest;

import java.util.List;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.vo.materialrequest.MaterialRequestItemVo;
import com.croods.bssgroup.vo.materialrequest.MaterialRequestVo;

public interface MaterialRequestService {

	public DataTablesOutput<MaterialRequestVo> findAll(DataTablesInput input,
			Specification<MaterialRequestVo> additionalSpecification, Specification<MaterialRequestVo> specification);

	public void deleteMaterialRequestItemIds(List<Long> materialRequestItemIds);

	public void save(MaterialRequestVo materialRequestVo);

	public MaterialRequestVo findByMaterialRequestIdAndBranchId(long materialRequestId, long branchId);

	public long findMaxRequestNo(String type, long companyId, long branchId, long userId, String defaultPrefix);

	public void delete(long materialRequestId, long userId, String modifiedOn);

	public List<MaterialRequestVo> findByBranchId(long branchId);

	public List<MaterialRequestItemVo> findItemByMaterialRequestId(long materialRequestId);
}
