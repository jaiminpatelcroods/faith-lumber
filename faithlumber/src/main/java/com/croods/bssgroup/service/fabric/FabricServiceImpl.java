package com.croods.bssgroup.service.fabric;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.fabric.FabricRepository;
import com.croods.bssgroup.vo.fabric.FabricVo;

@Service
@Transactional
public class FabricServiceImpl implements FabricService {

	@Autowired
	FabricRepository fabricRepository;
	
	@Override
	public void save(FabricVo fabricVo) {
		fabricRepository.save(fabricVo);
	}

	@Override
	public FabricVo findByFabricId(long fabricId) {
		return fabricRepository.findByFabricId(fabricId);
	}

	@Override
	public void delete(long fabricId, long alterBy, String modifiedOn) { 
		fabricRepository.delete(fabricId, alterBy, modifiedOn);
	}

	@Override
	public List<FabricVo> findByCompanyId(long companyId) {
		return fabricRepository.findByCompanyIdAndIsDeletedOrderByFabricIdDesc(companyId, 0);
	}

	@Override
	public boolean isExistFabric(long companyId, String fabricName) {
		if(fabricRepository.isExistFabric(companyId, fabricName) == null) {
			return true;
		} else {
			return false;
		}
	}	

	@Override
	public boolean isExistFabric(long companyId, String fabricName, long fabricId) {
		if(fabricRepository.isExistFabric(companyId, fabricName, fabricId) == null) {
			return true;
		} else {
			return false;
		}
	}
}
