package com.croods.bssgroup.service.contact;

import java.util.List;

import com.croods.bssgroup.vo.account.AccountCustomVo;
import com.croods.bssgroup.vo.contact.ContactAddressVo;
import com.croods.bssgroup.vo.contact.ContactVo;

public interface ContactService {

	public void saveContact(ContactVo contact);
	
	public List<ContactVo> findByBranchIdAndType(long branchId, String type);
	
	public ContactVo findByContactIdAndBranchIdAndType(long contactId, long branchId, String type);
	
	public AccountCustomVo findAccountCustomVoByContactId(long contactId);

	public void deleteContactAddressByIdIn(List<Long> contactAddressIds);
	
	public void deleteContactOtherByIdIn(List<Long> contactOtherIds);

	public void deleteContact(long contactId, long userId, String modifiedOn);

	public void changeDefaultAddress(long contactId, long contactAddressId);
	
	public ContactAddressVo findAddressByContactAddressId(long contactAddressId);

	public long countByBranchIdAndType(long branchId, String type);

	public long countByBranchIdAndTypeAndContactType(long branchId, String type, String contactType);
	
	public ContactVo findByContactId(long contactId);
}
