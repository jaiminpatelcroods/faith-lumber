package com.croods.bssgroup.service.rack;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.rack.RackRepository;
import com.croods.bssgroup.vo.rack.RackVo;

@Service
@Transactional
public class RackServiceImpl implements RackService {

	@Autowired
	RackRepository rackRepository;
	
	@Override
	public void save(RackVo rackVo) {
		rackRepository.save(rackVo);
	}

	@Override
	public RackVo findByRackId(long rackId) {
		return rackRepository.findByRackId(rackId);
	}

	@Override
	public void delete(long rackId, long alterBy, String modifiedOn) {
		rackRepository.delete(rackId, alterBy, modifiedOn);
	}

	@Override
	public List<RackVo> findByBranchId(long branchId) {
		return rackRepository.findByBranchIdAndIsDeletedOrderByRackIdDesc(branchId, 0);
	}

	@Override
	public boolean isExistRackCode(long branchId, String rackCode) {
		
		if(rackRepository.isExistRackCode(branchId, rackCode) == null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isExistRackCode(long branchId, String rackCode, long rackId) {
		
		if(rackRepository.isExistRackCode(branchId, rackCode, rackId) == null) {
			return true;
		} else {
			return false;
		}
	}

}
