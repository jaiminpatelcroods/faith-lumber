package com.croods.bssgroup.service.userfront;

import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.userfront.UserFrontRepository;
import com.croods.bssgroup.vo.userfront.UserFrontVo;
import com.croods.bssgroup.vo.userrole.UserRoleVo;

@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserFrontRepository userFrontRepository;
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		UserFrontVo userFrontVo = userFrontRepository.findByUserName(username);
		
		logger.info("loadUserByUsername username="+username);
		
		if (userFrontVo == null) 
			throw new UsernameNotFoundException(username +" not found");
		
		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		
		for (UserRoleVo userRoleVo: userFrontVo.getRoles()) {
			grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_"+userRoleVo.getUserRoleId()));
		}
		
		return new User(username, userFrontVo.getPassword(), grantedAuthorities);
	}

}
