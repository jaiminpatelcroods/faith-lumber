package com.croods.bssgroup.service.productionmaster;

import java.util.List;

import com.croods.bssgroup.vo.productionmaster.ProductionTypeVo;

public interface ProductionTypeService {

	List<ProductionTypeVo> findAll();

}
