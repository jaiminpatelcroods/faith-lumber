package com.croods.bssgroup.service.additionalcharge;

import java.util.List;

import com.croods.bssgroup.vo.additionalcharge.AdditionalChargeVo;

public interface AdditionalChargeService {
	
	public void save(AdditionalChargeVo additionalChargeVo);
	
	AdditionalChargeVo findByAdditionalChargeId(long additionalChargeId);
	
	List<AdditionalChargeVo> findByCompanyId(long companyId);
	
	void delete(long additionalChargeId, long alterBy, String modifiedOn);
	
}
