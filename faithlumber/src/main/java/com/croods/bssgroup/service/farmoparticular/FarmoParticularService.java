package com.croods.bssgroup.service.farmoparticular;

import java.util.List;

import com.croods.bssgroup.vo.farmoparticular.FarmoParticularVo;

public interface FarmoParticularService {

	public void save(FarmoParticularVo farmoParticularVo);
	
	FarmoParticularVo findByFarmoParticularId(long farmoParticularId);
	
	List<FarmoParticularVo> findByCompanyId(long companyId);
	
	/*List<FarmoParticularVo> findByCompanyIdAndIsActive(long companyId, int isDefault);*/
	
	/*List<FarmoParticularVo> findByCompanyIdAndIsDefaultAndModules(long companyId, int isDefault, String modules);*/
	
	List<FarmoParticularVo> findByCompanyIdAndActive(long companyId, boolean isActive);
	
	List<FarmoParticularVo> findByFarmoParticularIdIn(List<Long> farmoParticularIds);
	
	void delete(long farmoParticularId, long alterBy, String modifiedOn);
}
