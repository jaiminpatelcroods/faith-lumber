package com.croods.bssgroup.service.farmotype;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.farmotype.FarmoTypeRepository;
import com.croods.bssgroup.vo.farmoparticular.FarmoParticularVo;
import com.croods.bssgroup.vo.farmotype.FarmoTypeVo;

@Service
@Transactional
public class FarmoTypeServiceImpl implements FarmoTypeService {

	@Autowired
	FarmoTypeRepository farmoTypeRepository;

	@Override
	public void save(FarmoTypeVo farmoTypeVo) {
		farmoTypeRepository.save(farmoTypeVo);
	}

	@Override
	public FarmoTypeVo findByFarmoTypeId(long farmoTypeId,long companyId) {
		return farmoTypeRepository.findByFarmoTypeIdAndCompanyId(farmoTypeId,companyId);
	}

	@Override
	public List<FarmoTypeVo> findByCompanyId(long companyId) {
		return farmoTypeRepository.findByCompanyIdAndIsDeletedOrderByFarmoTypeIdDesc(companyId, 0);
	}

	@Override
	public void delete(long farmoTypeId, long alterBy, String modifiedOn) {
		farmoTypeRepository.delete(farmoTypeId, alterBy, modifiedOn);
		
	}

	@Override
	public DataTablesOutput<FarmoTypeVo> findAll(@Valid DataTablesInput input,
			Specification<FarmoTypeVo> additionalSpecification, Specification<FarmoTypeVo> preFilteringSpecification) {
		return farmoTypeRepository.findAll(input, null,preFilteringSpecification);
	}

	@Override
	public void deleteFarmoTypePerticularsIds(List<Long> l) {
		farmoTypeRepository.deleteFarmoTypePerticularsIds(l);
		
	}

}
