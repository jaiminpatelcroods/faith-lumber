package com.croods.bssgroup.service.category;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.croods.bssgroup.vo.category.CategoryVo;

public interface CategoryService {
	
	public void save(CategoryVo categoryVo);
	
	public CategoryVo findByCategoryId(long categoryid);
	
	public void deleteCategory(long categoryId, long alterBy, String modifiedOn);

	public List<CategoryVo> findByCompanyId(long companyId);
	
	public void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn);
	
	public 	void updateIsDefaultByCategoryId(long categoryId, long alterBy, int isDefault, String modifiedOn);

	boolean isExistCategory(long companyId, String categoryName);

	boolean isExistCategory(long companyId, String categoryName, long categoryId);
}
