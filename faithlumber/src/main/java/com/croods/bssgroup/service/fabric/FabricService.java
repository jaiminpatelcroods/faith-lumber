package com.croods.bssgroup.service.fabric;

import java.util.List;

import com.croods.bssgroup.vo.fabric.FabricVo;

public interface FabricService {
	
	public void save(FabricVo fabricVo);
	
	public FabricVo findByFabricId(long fabricId);
	
	public void delete(long fabricId, long alterBy, String modifiedOn);

	public List<FabricVo> findByCompanyId(long companyId);
	
	public boolean isExistFabric(long companyId, String fabricName);

	public boolean isExistFabric(long companyId, String fabricName, long fabricId);
}
