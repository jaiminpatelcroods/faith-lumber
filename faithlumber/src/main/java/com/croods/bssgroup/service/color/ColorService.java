package com.croods.bssgroup.service.color;

import java.util.List;

import com.croods.bssgroup.vo.color.ColorVo;

public interface ColorService {
	
	public void save(ColorVo colorVo);
	
	ColorVo findByColorId(long colorId);
	
	void delete(long colorId, long alterBy, String modifiedOn);

	List<ColorVo> findByCompanyId(long companyId);
}
