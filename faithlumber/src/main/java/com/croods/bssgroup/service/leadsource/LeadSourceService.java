package com.croods.bssgroup.service.leadsource;

import java.util.List;

import com.croods.bssgroup.vo.leadsource.LeadSourceVo;

public interface LeadSourceService {

	public void save(LeadSourceVo leadSourceVo);
	
	LeadSourceVo findByLeadSourceId(long leadSourceId);
	
	void deleteLeadSource(long leadSourceId, long alterBy, String modifiedOn);

	List<LeadSourceVo> findByCompanyId(long companyId);

	List<LeadSourceVo> findByCompanyIdAndIsEditable(long companyId);
	
	public boolean isExistLeadSource(long companyId, String sourceName);

	public boolean isExistLeadSource(long companyId, String sourceName, long leadSourceId);
	
}
