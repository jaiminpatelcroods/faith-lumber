package com.croods.bssgroup.service.userfront;

import com.croods.bssgroup.vo.userfront.UserFrontVo;

public interface UserFrontService {

	public void save(UserFrontVo userFrontVo);
	
	public UserFrontVo findByUserFrontId(long userFrontId);
	
	public void setDefaultYearInterval(long userFrontId, String yearInterval, String modifiedOn);
}
