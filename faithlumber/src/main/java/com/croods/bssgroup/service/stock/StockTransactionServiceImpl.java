package com.croods.bssgroup.service.stock;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.repository.stock.StockTransactionRepository;
import com.croods.bssgroup.vo.deliverychallan.DeliveryChallanItemVo;
import com.croods.bssgroup.vo.jobwork.JobworkInputVo;
import com.croods.bssgroup.vo.jobwork.JobworkOutputVo;
import com.croods.bssgroup.vo.jobwork.JobworkPackageVo;
import com.croods.bssgroup.vo.materialissue.MaterialIssueItemVo;
import com.croods.bssgroup.vo.purchase.PurchaseItemVo;
import com.croods.bssgroup.vo.sales.SalesItemVo;
import com.croods.bssgroup.vo.stock.StockTransactionVo;

@Service
@Transactional
public class StockTransactionServiceImpl implements StockTransactionService {

	@Autowired
	StockTransactionRepository stockTransactionRepository;
	
	@Override
	public void deleteStockTransaction(long branchId, long typeId, String type) {
		stockTransactionRepository.deleteStockTransaction(branchId, typeId, type);
	}

	@Override
	public double getTotalQty(long branchId, long productId, Date startDate, Date endDate, String yearInterval) {
		
		try {
			return stockTransactionRepository.getTotalQty(branchId, productId, startDate, endDate, yearInterval);
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public double getVariantQty(long branchId, long productVariantId, Date startDate, Date endDate,
			String yearInterval) {
		try {
			return stockTransactionRepository.getVariantQty(branchId, productVariantId, startDate, endDate, yearInterval);
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public void saveStockFromDeliveryChallan(List<DeliveryChallanItemVo> deliveryChallanItemVos, String yearInterval) {
		
		StockTransactionVo stockTransactionVo;
		
		for (DeliveryChallanItemVo deliveryChallanItemVo : deliveryChallanItemVos) {
			stockTransactionVo = new StockTransactionVo();
			
			stockTransactionVo.setBranchId(deliveryChallanItemVo.getDeliveryChallanVo().getBranchId());
			stockTransactionVo.setCompanyId(deliveryChallanItemVo.getDeliveryChallanVo().getCompanyId());
			stockTransactionVo.setDescription(deliveryChallanItemVo.getDeliveryChallanVo().getPrefix()+deliveryChallanItemVo.getDeliveryChallanVo().getDeliveryChallanNo());
			
			stockTransactionVo.setInQuantity(deliveryChallanItemVo.getQty());
			stockTransactionVo.setOutQuantity(0);
			stockTransactionVo.setProductPrice(0);
			stockTransactionVo.setProductVariantVo(deliveryChallanItemVo.getProductVariantVo());
			stockTransactionVo.setStockTransactionDate(deliveryChallanItemVo.getDeliveryChallanVo().getDeliveryChallanDate());
			stockTransactionVo.setType(Constant.PURCHASE_DELIVERY_CHALLAN);
			stockTransactionVo.setTypeId(deliveryChallanItemVo.getDeliveryChallanVo().getDeliveryChallanId());
			stockTransactionVo.setYearInterval(yearInterval);
			
			if(deliveryChallanItemVo.getBaleNo() != null) {
				stockTransactionVo.setBaleNo(deliveryChallanItemVo.getBaleNo());
			}
			
			if(deliveryChallanItemVo.getDesignNo() != null) {
				stockTransactionVo.setDesignNo(deliveryChallanItemVo.getDesignNo());
			}
			
			stockTransactionRepository.save(stockTransactionVo);
		}
		
		System.out.println("done");
	}

	@Override
	public List<Map<String, String>> findAllBaleNoByBranchIdAndProductVariantId(long branchId, long productVariantId,
			Date startDate, Date endDate, String yearInterval) {
		
		return stockTransactionRepository.findAllBaleNoByBranchIdAndProductVariantId(branchId, productVariantId, startDate, endDate, yearInterval);
	}
	
	@Override
	public List<Map<String, String>> findAllBaleNoByBranchIdAndProductVariantIdAndDesignNo(long branchId,
			long productVariantId, String designNo, Date startDate, Date endDate, String yearInterval) {
		return stockTransactionRepository.findAllBaleNoByBranchIdAndProductVariantIdAndDesignNo(branchId, productVariantId, designNo, startDate, endDate, yearInterval);
	}

	@Override
	public void saveStockFromJobworkProcessStore(List<JobworkOutputVo> jobworkOutputVos, List<JobworkPackageVo> jobworkPackageVos,
			String yearInterval) {
		
		StockTransactionVo stockTransactionVo;
		
		for (JobworkOutputVo jobworkOutputVo : jobworkOutputVos) {
			
			if(jobworkOutputVo.getFinalQty() != 0 ) {
				stockTransactionVo = new StockTransactionVo();
				
				stockTransactionVo.setBaleNo("");
				stockTransactionVo.setBranchId(jobworkOutputVo.getJobworkVo().getBranchId());
				stockTransactionVo.setCompanyId(jobworkOutputVo.getJobworkVo().getCompanyId());
				stockTransactionVo.setDescription(jobworkOutputVo.getJobworkVo().getPrefix()+jobworkOutputVo.getJobworkVo().getJobworkNo());
				stockTransactionVo.setDesignNo(jobworkOutputVo.getJobworkVo().getDesignNo());
				stockTransactionVo.setInQuantity(jobworkOutputVo.getFinalQty());
				stockTransactionVo.setOutQuantity(0);
				stockTransactionVo.setProductPrice(0);
				stockTransactionVo.setProductVariantVo(jobworkOutputVo.getProductVariantVo());
				stockTransactionVo.setStockTransactionDate(new Date());
				stockTransactionVo.setType(Constant.JOBWORK);
				stockTransactionVo.setTypeId(jobworkOutputVo.getJobworkVo().getJobworkId());
				stockTransactionVo.setYearInterval(yearInterval);
				
				stockTransactionRepository.save(stockTransactionVo);
			}
		}
		
		for (JobworkPackageVo jobworkPackageVo : jobworkPackageVos) {
			stockTransactionVo = new StockTransactionVo();
			
			stockTransactionVo.setBaleNo("");
			stockTransactionVo.setBranchId(jobworkPackageVo.getJobworkVo().getBranchId());
			stockTransactionVo.setCompanyId(jobworkPackageVo.getJobworkVo().getCompanyId());
			stockTransactionVo.setDescription(jobworkPackageVo.getJobworkVo().getPrefix()+jobworkPackageVo.getJobworkVo().getJobworkNo());
			stockTransactionVo.setDesignNo(jobworkPackageVo.getJobworkVo().getDesignNo());
			stockTransactionVo.setInQuantity(jobworkPackageVo.getNoOfPack());
			stockTransactionVo.setOutQuantity(0);
			stockTransactionVo.setProductPrice(0);
			stockTransactionVo.setProductVariantVo(jobworkPackageVo.getProductVariantVo());
			stockTransactionVo.setStockTransactionDate(new Date());
			stockTransactionVo.setType(Constant.JOBWORK);
			stockTransactionVo.setTypeId(jobworkPackageVo.getJobworkVo().getJobworkId());
			stockTransactionVo.setYearInterval(yearInterval);
			
			stockTransactionRepository.save(stockTransactionVo);
		}
		
	}

	@Override
	public void saveStockFromJobworkAntriAndLarring(List<JobworkInputVo> jobworkInputVos, String yearInterval) {
		StockTransactionVo stockTransactionVo;
		
		for (JobworkInputVo jobworkInputVo : jobworkInputVos) {
			stockTransactionVo = new StockTransactionVo();
			
			stockTransactionVo.setBaleNo(jobworkInputVo.getBaleNo());
			stockTransactionVo.setDesignNo(jobworkInputVo.getDesignNo());
			stockTransactionVo.setBranchId(jobworkInputVo.getJobworkVo().getBranchId());
			stockTransactionVo.setCompanyId(jobworkInputVo.getJobworkVo().getCompanyId());
			stockTransactionVo.setDescription(jobworkInputVo.getJobworkVo().getPrefix()+jobworkInputVo.getJobworkVo().getJobworkNo());
			stockTransactionVo.setDesignNo(jobworkInputVo.getJobworkVo().getDesignNo());
			stockTransactionVo.setInQuantity(0);
			stockTransactionVo.setOutQuantity(jobworkInputVo.getJobworkQty());
			stockTransactionVo.setProductPrice(0);
			stockTransactionVo.setProductVariantVo(jobworkInputVo.getProductVariantVo());
			stockTransactionVo.setStockTransactionDate(new Date());
			stockTransactionVo.setType(Constant.JOBWORK);
			stockTransactionVo.setTypeId(jobworkInputVo.getJobworkVo().getJobworkId());
			stockTransactionVo.setYearInterval(yearInterval);
			
			stockTransactionRepository.save(stockTransactionVo);
		}
	}

	@Override
	public List<Map<String, String>> findAllDesignNoByBranchIdAndProductVariantId(long branchId, long productVariantId,
			Date startDate, Date endDate, String yearInterval) {

		return stockTransactionRepository.findAllDesignNoByBranchIdAndProductVariantId(branchId, productVariantId, startDate, endDate, yearInterval);
	}
	
	public void saveStockTransactionFromPurchaseBill(List<PurchaseItemVo> purchaseItemVos, String yearInterval) {
		
		StockTransactionVo stockTransactionVo;
		
		for (PurchaseItemVo purchaseItemVo : purchaseItemVos) {
			stockTransactionVo = new StockTransactionVo();
			
			
			stockTransactionVo.setBranchId(purchaseItemVo.getPurchaseVo().getBranchId());
			stockTransactionVo.setCompanyId(purchaseItemVo.getPurchaseVo().getCompanyId());
			stockTransactionVo.setDescription(purchaseItemVo.getPurchaseVo().getPrefix()+purchaseItemVo.getPurchaseVo().getPurchaseNo());
			stockTransactionVo.setInQuantity(purchaseItemVo.getQty());
			stockTransactionVo.setOutQuantity(0);
			stockTransactionVo.setProductPrice(purchaseItemVo.getPrice());
			stockTransactionVo.setProductVariantVo(purchaseItemVo.getProductVariantVo());
			stockTransactionVo.setStockTransactionDate(purchaseItemVo.getPurchaseVo().getPurchaseDate());
			stockTransactionVo.setType(Constant.PURCHASE_BILL);
			stockTransactionVo.setTypeId(purchaseItemVo.getPurchaseVo().getPurchaseId());
			stockTransactionVo.setYearInterval(yearInterval);
			
			if(purchaseItemVo.getBaleNo() != null) {
				stockTransactionVo.setBaleNo(purchaseItemVo.getBaleNo());
			}
			
			if(purchaseItemVo.getDesignNo() != null) {
				stockTransactionVo.setDesignNo(purchaseItemVo.getDesignNo());
			}
			
			stockTransactionRepository.save(stockTransactionVo);
		}
		
		System.out.println("done");
	}

	@Override
	public void saveStockTransactionFromPurchaseDebitNote(List<PurchaseItemVo> purchaseItemVos, String yearInterval) {
		StockTransactionVo stockTransactionVo;
		
		for (PurchaseItemVo purchaseItemVo : purchaseItemVos) {
			stockTransactionVo = new StockTransactionVo();
			
			stockTransactionVo.setBranchId(purchaseItemVo.getPurchaseVo().getBranchId());
			stockTransactionVo.setCompanyId(purchaseItemVo.getPurchaseVo().getCompanyId());
			stockTransactionVo.setDescription(purchaseItemVo.getPurchaseVo().getPrefix()+purchaseItemVo.getPurchaseVo().getPurchaseNo());
			stockTransactionVo.setInQuantity(0);
			stockTransactionVo.setOutQuantity(purchaseItemVo.getQty());
			stockTransactionVo.setProductPrice(purchaseItemVo.getPrice());
			stockTransactionVo.setProductVariantVo(purchaseItemVo.getProductVariantVo());
			stockTransactionVo.setStockTransactionDate(purchaseItemVo.getPurchaseVo().getPurchaseDate());
			stockTransactionVo.setType(Constant.PURCHASE_DEBIT_NOTE);
			stockTransactionVo.setTypeId(purchaseItemVo.getPurchaseVo().getPurchaseId());
			stockTransactionVo.setYearInterval(yearInterval);
			
			if(purchaseItemVo.getBaleNo() != null) {
				stockTransactionVo.setBaleNo(purchaseItemVo.getBaleNo());
			}
			
			if(purchaseItemVo.getDesignNo() != null) {
				stockTransactionVo.setDesignNo(purchaseItemVo.getDesignNo());
			}
			
			stockTransactionRepository.save(stockTransactionVo);
		}
		
		System.out.println("done");
		
	}

	@Override
	public double getAllProductTotalQty(long branchId, Date startDate, Date endDate, String yearInterval) {
		
		try {
			return stockTransactionRepository.getAllProductTotalQty(branchId, startDate, endDate, yearInterval);
		} catch (Exception e) {
			return 0;
		}
		
	}

	@Override
	public void saveStockFromSales(List<SalesItemVo> salesItemVos, String yearInterval) {
		
		StockTransactionVo stockTransactionVo;
		
		for (SalesItemVo salesItemVo : salesItemVos) {
			
			stockTransactionVo =new StockTransactionVo();
			
			stockTransactionVo.setBranchId(salesItemVo.getSalesVo().getBranchId());
			stockTransactionVo.setCompanyId(salesItemVo.getSalesVo().getCompanyId());
			stockTransactionVo.setDescription(salesItemVo.getSalesVo().getPrefix()+""+salesItemVo.getSalesVo().getSalesNo());
			stockTransactionVo.setInQuantity(0);
			stockTransactionVo.setOutQuantity(salesItemVo.getQty());
			stockTransactionVo.setProductPrice(salesItemVo.getPrice());
			stockTransactionVo.setType("sales");
			stockTransactionVo.setTypeId(salesItemVo.getSalesVo().getSalesId());
			stockTransactionVo.setStockTransactionDate(salesItemVo.getSalesVo().getSalesDate());
			stockTransactionVo.setYearInterval(yearInterval);
			stockTransactionVo.setProductVariantVo(salesItemVo.getProductVariantVo());
			
			if(salesItemVo.getBaleNo() != null) {
				stockTransactionVo.setBaleNo(salesItemVo.getBaleNo());
			}
			
			if(salesItemVo.getDesignNo() != null) {
				stockTransactionVo.setDesignNo(salesItemVo.getDesignNo());
			}
			
			stockTransactionRepository.save(stockTransactionVo);
		}
	}

	@Override
	public void saveStockFromSalesReturn(List<SalesItemVo> salesItemVos, String yearInterval) {
		
		StockTransactionVo stockTransactionVo;
		
		for (SalesItemVo salesItemVo : salesItemVos) {
			
			stockTransactionVo =new StockTransactionVo();
			
			stockTransactionVo.setBranchId(salesItemVo.getSalesVo().getBranchId());
			stockTransactionVo.setCompanyId(salesItemVo.getSalesVo().getCompanyId());
			stockTransactionVo.setDescription(salesItemVo.getSalesVo().getPrefix()+""+salesItemVo.getSalesVo().getSalesNo());
			stockTransactionVo.setInQuantity(salesItemVo.getQty());
			stockTransactionVo.setOutQuantity(0);
			stockTransactionVo.setProductPrice(salesItemVo.getPrice());
			stockTransactionVo.setType("sales_return");
			stockTransactionVo.setTypeId(salesItemVo.getSalesVo().getSalesId());
			stockTransactionVo.setStockTransactionDate(salesItemVo.getSalesVo().getSalesDate());
			stockTransactionVo.setYearInterval(yearInterval);
			stockTransactionVo.setProductVariantVo(salesItemVo.getProductVariantVo());
			
			if(salesItemVo.getBaleNo() != null) {
				stockTransactionVo.setBaleNo(salesItemVo.getBaleNo());
			}
			
			if(salesItemVo.getDesignNo() != null) {
				stockTransactionVo.setDesignNo(salesItemVo.getDesignNo());
			}
			
			stockTransactionRepository.save(stockTransactionVo);
		}
	}
	
	@Override
	public void saveStockFromMaterialIssue(List<MaterialIssueItemVo> materialIssueItemVos, String yearInterval) {
		
		StockTransactionVo stockTransactionVo;
		
		for (MaterialIssueItemVo materialIssueItemVo : materialIssueItemVos) {
			stockTransactionVo = new StockTransactionVo();
			
			stockTransactionVo.setBranchId(materialIssueItemVo.getMaterialIssueVo().getBranchId());
			stockTransactionVo.setCompanyId(materialIssueItemVo.getMaterialIssueVo().getCompanyId());
			stockTransactionVo.setDescription(materialIssueItemVo.getMaterialIssueVo().getPrefix()+materialIssueItemVo.getMaterialIssueVo().getIssueNo());
			stockTransactionVo.setInQuantity(0);
			stockTransactionVo.setOutQuantity(materialIssueItemVo.getQty());
			stockTransactionVo.setProductPrice(0);
			stockTransactionVo.setProductVariantVo(materialIssueItemVo.getProductVariantVo());
			stockTransactionVo.setStockTransactionDate(materialIssueItemVo.getMaterialIssueVo().getIssueDate());
			stockTransactionVo.setType(Constant.MATERIAL_ISSUE);
			stockTransactionVo.setTypeId(materialIssueItemVo.getMaterialIssueVo().getMaterialIssueId());
			stockTransactionVo.setYearInterval(yearInterval);
			
			if(materialIssueItemVo.getBaleNo() != null) {
				stockTransactionVo.setBaleNo(materialIssueItemVo.getBaleNo());
			}
			
			if(materialIssueItemVo.getDesignNo() != null) {
				stockTransactionVo.setDesignNo(materialIssueItemVo.getDesignNo());
			}
			
			stockTransactionRepository.save(stockTransactionVo);
		}
	}
	
	@Override
	public void saveStockFromMaterialReturn(List<MaterialIssueItemVo> materialIssueItemVos, String yearInterval) {
		
		StockTransactionVo stockTransactionVo;
		
		try {
			materialIssueItemVos.removeIf(m -> m.getReturnQty() <= 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for (MaterialIssueItemVo materialIssueItemVo : materialIssueItemVos) {
			stockTransactionVo = new StockTransactionVo();
			
			stockTransactionVo.setBranchId(materialIssueItemVo.getMaterialIssueVo().getBranchId());
			stockTransactionVo.setCompanyId(materialIssueItemVo.getMaterialIssueVo().getCompanyId());
			stockTransactionVo.setDescription(materialIssueItemVo.getMaterialIssueVo().getPrefix()+materialIssueItemVo.getMaterialIssueVo().getIssueNo());
			stockTransactionVo.setInQuantity(materialIssueItemVo.getReturnQty());
			stockTransactionVo.setOutQuantity(0);
			stockTransactionVo.setProductPrice(0);
			stockTransactionVo.setProductVariantVo(materialIssueItemVo.getProductVariantVo());
			stockTransactionVo.setStockTransactionDate(materialIssueItemVo.getMaterialIssueVo().getIssueDate());
			stockTransactionVo.setType(Constant.MATERIAL_RETURN);
			stockTransactionVo.setTypeId(materialIssueItemVo.getMaterialIssueVo().getMaterialIssueId());
			stockTransactionVo.setYearInterval(yearInterval);
			
			if(materialIssueItemVo.getBaleNo() != null) {
				stockTransactionVo.setBaleNo(materialIssueItemVo.getBaleNo());
			}
			
			if(materialIssueItemVo.getDesignNo() != null) {
				stockTransactionVo.setDesignNo(materialIssueItemVo.getDesignNo());
			}
			
			stockTransactionRepository.save(stockTransactionVo);
		}
	}
}
