package com.croods.bssgroup.service.navmenu;

import java.util.List;

import com.croods.bssgroup.vo.navmenu.NavMenuActionVo;
import com.croods.bssgroup.vo.navmenu.NavMenuPermissionVo;
import com.croods.bssgroup.vo.navmenu.NavMenuVo;
import com.croods.bssgroup.vo.navmenu.NavSubMenuVo;

public interface NavMenuService {
	
	public List<NavMenuVo> findNavMenuByUserFrontId(long userFrontId);
	
	public List<NavSubMenuVo> findNavSubMenuByUserFrontId(long userFrontId);
	
	public List<NavMenuVo> findNavMenuByUserRoleId(long userFrontId);
	
	public List<NavSubMenuVo> findNavSubMenuByUserRoleId(long userFrontId);
	
	public NavSubMenuVo findByNavSubMenuId(long navSubMenuId);
	
	public List<NavMenuActionVo> findAllNavMenuAction();
	
	public List<NavMenuPermissionVo> findByUserfrontId(long userFrontId);
	
	public int deleteByNavMenuPermissionIdIn(List<Long> navMenuPermissionIds);
	
	public void saveNavMenuPermission(NavMenuPermissionVo navMenuPermissionVo);
	
	public List<NavMenuPermissionVo> findByUserRoleId(long userRoleId);
	
	public void saveNavSubMenu(NavSubMenuVo navSubMenuVo);
	
	public void saveNavMenu(NavMenuVo navMenuVo);
	
	public List<NavMenuVo> findAllNavMenu();
	
	public List<NavSubMenuVo> findAllNavSubMenu();
}
