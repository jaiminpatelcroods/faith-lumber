package com.croods.bssgroup.service.materialissue;

import java.util.List;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.vo.materialissue.MaterialIssueVo;

public interface MaterialIssueService {

	public DataTablesOutput<MaterialIssueVo> findAll(DataTablesInput input,
			Specification<MaterialIssueVo> additionalSpecification, Specification<MaterialIssueVo> specification);

	public void deleteMaterialIssueItemIds(List<Long> materialIssueItemIds);

	public void save(MaterialIssueVo materialIssueVo);

	public MaterialIssueVo findByMaterialIssueIdAndBranchId(long materialIssueId, long branchId);

	public long findMaxIssueNo(String type, long companyId, long branchId, long userId, String defaultPrefix);

	public void delete(long materialIssueId, long branchId, long userId, String modifiedOn);

	public void materialReturn(MaterialIssueVo materialIssueVo, String yearInterval);
	
	public void saveStockTransaction(MaterialIssueVo materialIssueVo, String yearInterval);
}
