package com.croods.bssgroup.service.rack;

import java.util.List;

import com.croods.bssgroup.vo.rack.RackVo;

public interface RackService {

	public void save(RackVo rackVo);
	
	public RackVo findByRackId(long rackId);
	
	public void delete(long rackId, long alterBy, String modifiedOn);

	public List<RackVo> findByBranchId(long branchId);

	public boolean isExistRackCode(long branchId, String rackCode);

	public boolean isExistRackCode(long branchId, String rackCode, long rackId);
}
