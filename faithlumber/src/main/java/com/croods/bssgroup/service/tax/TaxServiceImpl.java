package com.croods.bssgroup.service.tax;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.tax.TaxRepository;
import com.croods.bssgroup.vo.tax.TaxVo;

@Service
@Transactional
public class TaxServiceImpl implements TaxService {

	@Autowired
	TaxRepository taxRepository;
	
	@Override
	public TaxVo findByTaxId(long id) {
		return taxRepository.findByTaxId(id);
	}
	
	@Override
	public void taxSave(TaxVo taxVo) {
		taxRepository.save(taxVo);
	}

	@Override
	public void delete(long taxId, long alterBy, String modifiedOn) {
		taxRepository.delete(taxId, alterBy, modifiedOn);
	}

	@Override
	public List<TaxVo> findByCompanyId(long companyId) {
		return taxRepository.findByCompanyIdAndIsDeletedOrderByTaxIdDesc(companyId, 0);
	}

	@Override
	public void updateIsDefaultByCompanyId(long companyId, int isDefault) {
		taxRepository.updateIsDefaultByCompanyId(companyId, isDefault);
	}

	@Override
	public void updateIsDefaultByTaxId(long taxId, int isDefault) {
		taxRepository.updateIsDefaultByTaxId(taxId, isDefault);
	}
	
	@Override
	public boolean isExistTax(long companyId, String tax) {
		if(taxRepository.isExistTax(companyId, tax) == null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isExistTax(long companyId, String tax, long taxId) {
		if(taxRepository.isExistTax(companyId, tax, taxId) == null) {
			return true;
		} else {
			return false;
		}
	}
	
}
