package com.croods.bssgroup.service.purchaserequest;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.purchaserequest.PurchaseRequestItemRepository;
import com.croods.bssgroup.repository.purchaserequest.PurchaseRequestRepository;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.prefix.PrefixVo;
import com.croods.bssgroup.vo.purchaserequest.PurchaseRequestItemVo;
import com.croods.bssgroup.vo.purchaserequest.PurchaseRequestVo;

@Service
@Transactional
public class PurchaseRequestServiceImpl implements PurchaseRequestService {

	@Autowired
	PurchaseRequestRepository purchaseRequestRepository;
	
	@Autowired
	PurchaseRequestItemRepository purchaseRequestItemRepository;
	
	@Autowired
	PrefixService prefixService;

	@Override
	public long findMaxRequestNo(String type, long companyId, long branchId, long userId, String defaultPrefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(defaultPrefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = purchaseRequestRepository.findMaxRequestNo(branchId, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
		
	}

	@Override
	public void save(PurchaseRequestVo purchaseRequestVo) {
		purchaseRequestRepository.save(purchaseRequestVo);
	}

	@Override
	public PurchaseRequestVo findByPurchaseRequestIdAndBranchId(long purchaseRequestId, long branchId) {
		return purchaseRequestRepository.findByPurchaseRequestIdAndBranchIdAndIsDeleted(purchaseRequestId, branchId, 0);
	}

	@Override
	public void deletePurchaseRequestItemByIdIn(List<Long> purchaseRequestItemIds) {
		purchaseRequestItemRepository.deletePurchaseRequestItemByIdIn(purchaseRequestItemIds);
	}

	@Override
	public void delete(long purchaseRequestId, long userId, String modifiedOn) {
		purchaseRequestRepository.delete(purchaseRequestId, userId, modifiedOn);
	}

	@Override
	public DataTablesOutput<PurchaseRequestVo> findAll(DataTablesInput input,
			Specification<PurchaseRequestVo> additionalSpecification, Specification<PurchaseRequestVo> specification) {
		return purchaseRequestRepository.findAll(input, additionalSpecification, specification);
	}

	@Override
	public List<PurchaseRequestItemVo> findItemByPurchaseRequestId(long purchaseRequestId) {
		return purchaseRequestItemRepository.findByPurchaseRequestVoPurchaseRequestId(purchaseRequestId);
	}

	@Override
	public void updateStatusAndExpectedDateByPurchaseRequestId(String status, Date deliveryDate, long alterBy, String modifiedOn,
			long purchaseRequestId) {
		purchaseRequestRepository.updateStatusAndExpectedDateByPurchaseRequestId(status, deliveryDate, alterBy, modifiedOn, purchaseRequestId);
	}

	@Override
	public String findRequestNoByPurchaseRequestId(long purchaseRequestId) {
		return purchaseRequestRepository.findRequestNoByPurchaseRequestId(purchaseRequestId);
	}

	@Override
	public void updateStatusByPurchaseId(String status, long alterBy, String modifiedOn, long purchaseId) {
		purchaseRequestRepository.updateStatusByPurchaseId(status, alterBy, modifiedOn, purchaseId);
	}
	
}
