package com.croods.bssgroup.service.delivery;

import java.util.List;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.vo.delivery.DeliveryItemVo;
import com.croods.bssgroup.vo.delivery.DeliveryVo;

public interface DeliveryService {

	public DataTablesOutput<DeliveryVo> findAll(DataTablesInput input,
			Specification<DeliveryVo> additionalSpecification, Specification<DeliveryVo> specification);

	public void deleteDeliveryItemIds(List<Long> deliveryItemIds);

	public DeliveryVo save(DeliveryVo deliveryVo);

	public DeliveryVo findByDeliveryIdAndBranchId(long deliveryId, long branchId);

	public long findMaxDeliveryNo(String type, long companyId, long branchId, long userId, String defaultPrefix);

	public void delete(long deliveryId, long userId, String modifiedOn);

	public List<DeliveryVo> findByBranchId(long branchId);

	public List<DeliveryItemVo> findItemByDeliveryId(long deliveryId);
}
