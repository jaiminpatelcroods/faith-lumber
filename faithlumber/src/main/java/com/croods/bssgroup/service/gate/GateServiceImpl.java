package com.croods.bssgroup.service.gate;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.gate.GateRepository;
import com.croods.bssgroup.vo.gate.GateVo;

@Service
@Transactional
public class GateServiceImpl implements GateService {

	@Autowired
	GateService gateService;
	
	@Autowired
	GateRepository gateRepository;

	@Autowired
	EntityManager entityManager;
	
	@Override
	public GateVo save(GateVo gateVo) {
		gateVo=gateRepository.saveAndFlush(gateVo);
		entityManager.refresh(gateVo);
		return gateVo;
		
		//return gateRepository.save(gateVo);
	}

	@Override
	public DataTablesOutput<GateVo> findAll(DataTablesInput input, Specification<GateVo> additionalSpecification,
	Specification<GateVo> specification) {
	return gateRepository.findAll(input, additionalSpecification, specification);
	
		}

	@Override
	public GateVo findByGateIdAndBranchId(long gateId, long branchId) {
		return gateRepository.findByGateIdAndBranchId(gateId, branchId);
		
		}

	@Override
	public void delete(long id, long userId, String modifiedOn) {
		gateRepository.delete(id, userId, modifiedOn);
		}

	@Override
	public GateVo findByGateId(long gateId) {
		return gateRepository.findByGateId(gateId);
		
	}
		

	}
