package com.croods.bssgroup.service.deliverychallan;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.repository.deliverychallan.DeliveryChallanItemRepository;
import com.croods.bssgroup.repository.deliverychallan.DeliveryChallanRepository;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.service.stock.StockTransactionService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.deliverychallan.DeliveryChallanVo;
import com.croods.bssgroup.vo.prefix.PrefixVo;

@Service
@Transactional
public class DeliveryChallanServiceImpl implements DeliveryChallanService {

	@Autowired
	DeliveryChallanRepository deliveryChallanRepository;
	
	@Autowired
	DeliveryChallanItemRepository deliveryChallanItemRepository;
	
	@Autowired
	PrefixService prefixService;
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Autowired
	StockTransactionService stockTransactionService;
	
	@Override
	public long findMaxDeliveryChallanNo(long companyId, long branchId, long userId, String type, String prefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(prefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = deliveryChallanRepository.findMaxDeliveryChallanNo(branchId, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
	}

	@Override
	public DeliveryChallanVo save(DeliveryChallanVo deliveryChallanVo) {
		return deliveryChallanRepository.save(deliveryChallanVo);
	}

	@Override
	public DeliveryChallanVo findByDeliveryChallanIdAndBranchId(long deliveryChallanId, long branchId) {
		return deliveryChallanRepository.findByDeliveryChallanIdAndBranchIdAndIsDeleted(deliveryChallanId, branchId, 0);
	}

	@Override
	public void deleteDeliveryChallanItemIds(List<Long> l) {
		deliveryChallanItemRepository.deleteDeliveryChallanItemIds(l);
		
	}

	@Override
	public void updateQC(DeliveryChallanVo deliveryChallanVo, String yearInterval) {
		
		deliveryChallanVo.getDeliveryChallanItemVos()
				.forEach(p -> deliveryChallanItemRepository.updateQCQTY(p.getDeliveryChallanItemId(), p.getQcQty()));
		
		deliveryChallanRepository.updateQCByAndStatusByDeliveryChallanId(deliveryChallanVo.getQcBy().getEmployeeId(), 
				deliveryChallanVo.getStatus(), 
				deliveryChallanVo.getModifiedOn(),
				deliveryChallanVo.getAlterBy(),
				deliveryChallanVo.getBranchId(),
				deliveryChallanVo.getDeliveryChallanId());
		
		stockTransactionService.deleteStockTransaction(deliveryChallanVo.getBranchId(), deliveryChallanVo.getDeliveryChallanId(), Constant.PURCHASE_DELIVERY_CHALLAN);
		stockTransactionService.saveStockFromDeliveryChallan(
				deliveryChallanItemRepository.findByDeliveryChallanVoDeliveryChallanId(deliveryChallanVo.getDeliveryChallanId()),
				yearInterval);
	}

	@Override
	public DataTablesOutput<DeliveryChallanVo> findAll(DataTablesInput input,
			Specification<DeliveryChallanVo> additionalSpecification, Specification<DeliveryChallanVo> specification) {
		return deliveryChallanRepository.findAll(input, additionalSpecification, specification);
	}
	
	@Override
	public void delete(long deliveryChallanId, long userId, String modifiedOn) {
		deliveryChallanRepository.delete(deliveryChallanId, userId, modifiedOn);
	}

	@Override
	public List<DeliveryChallanVo> findByContactIdAndBranchId(long contactId, long branchId) {
		return deliveryChallanRepository.findByContactVoContactIdAndBranchIdAndIsDeleted(contactId, branchId, 0);
	}

	@Override
	public List<DeliveryChallanVo> findByDeliveryChallanIdIn(List<Long> deliveryChallanIds) {
		return deliveryChallanRepository.findByDeliveryChallanIdIn(deliveryChallanIds);
	}
}
