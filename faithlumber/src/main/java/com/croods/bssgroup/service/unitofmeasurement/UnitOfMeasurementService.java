package com.croods.bssgroup.service.unitofmeasurement;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.croods.bssgroup.vo.unitofmeasurement.UnitOfMeasurementVo;

public interface UnitOfMeasurementService {

	public UnitOfMeasurementVo findByMeasurementId(long measurementId);
	
	public List<UnitOfMeasurementVo> findByCompanyId(long companyId);

	public void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn);

	public void updateIsDefaultByMeasurementId(long measurementId, long alterBy, int isDefault, String modifiedOn);
	
	public void save(UnitOfMeasurementVo unitOfMeasurementVo);
	
	public void delete(long measurementId, long alterBy, String modifiedOn);

	public boolean isExistUnitofmeasurement(long companyId, String Uom);

	public boolean isExistUnitofmeasurement(long companyId, String Uom, long measurementId);
	
	public UnitOfMeasurementVo findByMeasurementCodeAndCompanyId(String Uom,long companyId);
}
