package com.croods.bssgroup.service.sms;


import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.sms.SmsRepository;
import com.croods.bssgroup.vo.sms.SmsVo;

@Service
@Transactional
public class SmsServiceImpl implements SmsService {

	@Autowired
	SmsRepository smsRepository;
	
	
	@Override
	public void smsSave(SmsVo smsVo) {
		smsRepository.save(smsVo);
	}
	
	@Override
	public List<SmsVo> findByCompanyId(long companyId) {
		return smsRepository.findByCompanyIdOrderBySmsIdDesc(companyId);
	}

	@Override
	public DataTablesOutput<SmsVo> findAll(@Valid DataTablesInput input, Object object,
			Specification<SmsVo> specification) {
		return smsRepository.findAll(input, null,specification);
	}

	
}
