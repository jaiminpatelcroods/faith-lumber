package com.croods.bssgroup.service.farmoparticular;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.farmoparticular.FarmoParticularRepository;
import com.croods.bssgroup.vo.farmoparticular.FarmoParticularVo;

@Service
@Transactional
public class FarmoParticularServiceImpl implements FarmoParticularService {

	@Autowired
	FarmoParticularRepository farmoParticularRepository;

	@Override
	public void save(FarmoParticularVo farmoParticularVo) {
		farmoParticularRepository.save(farmoParticularVo);
	}

	@Override
	public FarmoParticularVo findByFarmoParticularId(long farmoParticularId) {
		return farmoParticularRepository.findByFarmoParticularId(farmoParticularId);
	}

	@Override
	public List<FarmoParticularVo> findByCompanyId(long companyId) {
		return farmoParticularRepository.findByCompanyIdAndIsDeletedOrderByFarmoParticularIdDesc(companyId, 0);
	}

	@Override
	public List<FarmoParticularVo> findByCompanyIdAndActive(long companyId, boolean isDefault) {
		return farmoParticularRepository.findByCompanyIdAndIsActiveAndIsDeletedOrderByFarmoParticularIdDesc(companyId, isDefault, 0);
	}

	@Override
	public List<FarmoParticularVo> findByFarmoParticularIdIn(List<Long> farmoParticularIds) {
		return farmoParticularRepository.findByFarmoParticularIdIn(farmoParticularIds);
	}

	@Override
	public void delete(long farmoParticularId, long alterBy, String modifiedOn) {
		farmoParticularRepository.delete(farmoParticularId, alterBy, modifiedOn);
		
	}
	
	/*@Override
	public List<FarmoParticularVo> findByCompanyIdAndIsActive(long companyId, int isDefault, String modules) {
		return farmoParticularRepository.findByCompanyIdAndIsDefaultAndModules(companyId, isDefault, modules);
	}*/
	
	/*@Override
	public List<FarmoParticularVo> findByCompanyIdAndModules(long companyId, String modules) {
		return farmoParticularRepository.findByCompanyIdAndModules(companyId, modules);
	}*/

}
