package com.croods.bssgroup.service.userrole;

import java.util.List;

import com.croods.bssgroup.vo.userrole.UserRoleVo;

public interface UserRoleService {

	public void save(UserRoleVo  userRoleVo);
	
	public UserRoleVo findByUserRoleId(long userRoleId);
	
	public List<UserRoleVo> findByBranchId(long branchId);
}
