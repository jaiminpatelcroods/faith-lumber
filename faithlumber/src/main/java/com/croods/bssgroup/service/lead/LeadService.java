package com.croods.bssgroup.service.lead;

import java.util.Date;
import java.util.List;

import com.croods.bssgroup.vo.lead.LeadActivityVo;
import com.croods.bssgroup.vo.lead.LeadAddressVo;
import com.croods.bssgroup.vo.lead.LeadVo;

public interface LeadService {

	public void saveLead(LeadVo leadVo);
	
	public List<LeadVo> findByBranchId(long branchId);
	
	public LeadVo findByLeadIdAndBranchId(long leadId, long branchId);
	
	public void deleteLead(long leadId, long userId, String modifiedOn);
	
	public void deleteLeadAddressByIdIn(List<Long> leadAddressIds);
	
	public void deleteLeadContactByIdIn(List<Long> leadContactIds);

	public void changeDefaultAddress(long leadId, long leadAddressId);
	
	public LeadAddressVo findAddressByLeadAddressId(long leadAddressId);

	public void saveLeadActivity(LeadActivityVo leadActivityVo);
	
	public void updateLeadStatus(long leadId, String status,  long userId, String modifiedOn);
	
	public void updateLeadStatusAndAssignedEmployee(long leadId, String status,  long userId, String modifiedOn, long employeeId);

	public List<LeadActivityVo> findActivityByLeadIdAndBranchId(long leadId, long branchId);

	public void updateCompleteDetails(long leadId, String status, Date completedDae, String remark, long alterBy, String modifiedOn);
}
