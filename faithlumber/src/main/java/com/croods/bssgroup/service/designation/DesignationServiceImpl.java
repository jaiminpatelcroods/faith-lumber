package com.croods.bssgroup.service.designation;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.designation.DesignationRepository;
import com.croods.bssgroup.vo.designation.DesignationVo;

@Service
@Transactional
public class DesignationServiceImpl implements DesignationService{

	@Autowired
	DesignationRepository designationRepository;
	
	@Override
	public DesignationVo findByDesignationId(long designationId) {
		return designationRepository.findByDesignationId(designationId);
	}

	
	@Override
	public void deleteDesignation(long designationId, long alterBy, String modifiedOn) {
		designationRepository.deleteDesignation(designationId, alterBy, modifiedOn);
		
	}

	@Override
	public List<DesignationVo> findByCompanyId(long companyId) {
		return designationRepository.findByCompanyIdAndIsDeletedOrderByDesignationIdDesc(companyId, 0);
	}

	@Override
	public void save(DesignationVo designationVo) {
		designationRepository.save(designationVo);
	}
	
	@Override
	public boolean isExistDesignation(long companyId, String designationName) {
		if(designationRepository.isExistDesignation(companyId, designationName) == null) {
			return true;
		} else {
			return false;
		}
	}
	


	@Override
	public boolean isExistDesignation(long companyId, String designationName, long designationId) {
		if(designationRepository.isExistDesignation(companyId, designationName, designationId) == null) {
			return true;
		} else {
			return false;
		}
	}
}
