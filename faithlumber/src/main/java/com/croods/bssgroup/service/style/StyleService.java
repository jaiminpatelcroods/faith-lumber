package com.croods.bssgroup.service.style;

import java.util.List;

import com.croods.bssgroup.vo.style.StyleVo;

public interface StyleService {
	
	public void save(StyleVo styleVo);
	
	public StyleVo findByStyleId(long styleId);
	
	public void delete(long styleId, long alterBy, String modifiedOn);

	public List<StyleVo> findByCompanyId(long companyId);
	
	public boolean isExistStyle(long companyId, String styleName);

	public boolean isExistStyle(long companyId, String styleName, long styleId);
}
