package com.croods.bssgroup.service.jobwork;

import java.util.List;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.vo.jobwork.JobworkPackageVo;
import com.croods.bssgroup.vo.jobwork.JobworkVo;

public interface JobworkService {

	public JobworkVo findByJobworkIdAndBranchId(long jobworkId, long branchId);

	public List<JobworkVo> findByBranchIdAndStatus(long branchId, String status);
	
	public void save(JobworkVo jobworkVo);
	
	public long findMaxJobworkNo(long companyId, long branchId, long userId, String type, String prefix);

	// Jobwork Process
	public void deleteJobworkProcessByIdIn(String jobworkProcessIds);

	public List<JobworkPackageVo> findJobworkPackageByJobworkId(long jobworkId);
	
	public void saveStockTransaction(JobworkVo jobworkVo, String yearInterval, String fromProcess);

	public DataTablesOutput<JobworkVo> findAll(DataTablesInput input, Specification<JobworkVo> additionalSpecification,
			Specification<JobworkVo> specification);
	
}
