package com.croods.bssgroup.service.unitofmeasurement;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.unitofmeasurement.UnitOfMeasurementRepository;
import com.croods.bssgroup.vo.unitofmeasurement.UnitOfMeasurementVo;

@Service
@Transactional
public class UnitOfMeasurementServiceImpl implements UnitOfMeasurementService {

	@Autowired
	UnitOfMeasurementRepository unitOfMeasurementRepository;
	
	@Override
	public UnitOfMeasurementVo findByMeasurementId(long measurementId) {
		return unitOfMeasurementRepository.findByMeasurementId(measurementId);
	}

	@Override
	public List<UnitOfMeasurementVo> findByCompanyId(long companyId) {
		return unitOfMeasurementRepository.findByCompanyIdAndIsDeletedOrderByMeasurementIdDesc(companyId, 0);
	}

	@Override
	public void updateIsDefaultByCompanyId(long companyId, long alterBy, int isDefault, String modifiedOn) {
		unitOfMeasurementRepository.updateIsDefaultByCompanyId(companyId, alterBy, isDefault, modifiedOn);
		
	}

	@Override
	public void updateIsDefaultByMeasurementId(long measurementId, long alterBy, int isDefault, String modifiedOn) {
		unitOfMeasurementRepository.updateIsDefaultByMeasurementId(measurementId, alterBy, isDefault, modifiedOn);
		
	}
	
	@Override
	public void save(UnitOfMeasurementVo unitOfMeasurementVo) {
		unitOfMeasurementRepository.save(unitOfMeasurementVo);
	}
	
	@Override
	public void delete(long measurementId, long alterBy, String modifiedOn) {
		unitOfMeasurementRepository.delete(measurementId, alterBy, modifiedOn);
	}
	
	@Override
	public boolean isExistUnitofmeasurement(long companyId, String Uom) {
		if(unitOfMeasurementRepository.isExistUnitofmeasurement(companyId, Uom) == null) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isExistUnitofmeasurement(long companyId, String Uom, long measurementId) {
		if(unitOfMeasurementRepository.isExistUnitofmeasurement(companyId, Uom, measurementId) == null) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public UnitOfMeasurementVo findByMeasurementCodeAndCompanyId(String Uom,long companyId) {
		return unitOfMeasurementRepository.findByMeasurementCodeAndCompanyId(Uom,companyId);

	}
}
