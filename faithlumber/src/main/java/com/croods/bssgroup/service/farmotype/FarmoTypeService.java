package com.croods.bssgroup.service.farmotype;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.vo.farmotype.FarmoTypeVo;

public interface FarmoTypeService {

	DataTablesOutput<FarmoTypeVo> findAll(@Valid DataTablesInput input, Specification<FarmoTypeVo> additionalSpecification,
			Specification<FarmoTypeVo> preFilteringSpecification);
	
	public void save(FarmoTypeVo farmoParticularVo);
	
	FarmoTypeVo findByFarmoTypeId(long farmoParticularId,long companyId);
	
	List<FarmoTypeVo> findByCompanyId(long companyId);
	
	void delete(long farmoParticularId, long alterBy, String modifiedOn);

	void deleteFarmoTypePerticularsIds(List<Long> l);

}
