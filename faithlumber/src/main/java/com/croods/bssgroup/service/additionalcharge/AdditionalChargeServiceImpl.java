package com.croods.bssgroup.service.additionalcharge;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.additionalcharge.AdditionalChargeRepository;
import com.croods.bssgroup.vo.additionalcharge.AdditionalChargeVo;

@Service
@Transactional
public class AdditionalChargeServiceImpl implements AdditionalChargeService {

	@Autowired
	AdditionalChargeRepository additionalChargeRepository;
	
	@Override
	public void save(AdditionalChargeVo additionalChargeVo) {
		additionalChargeRepository.save(additionalChargeVo);
		
	}
	
	@Override
	public AdditionalChargeVo findByAdditionalChargeId(long additionalChargeId) {
		return additionalChargeRepository.findByAdditionalChargeId(additionalChargeId);
	}

	@Override
	public List<AdditionalChargeVo> findByCompanyId(long companyId) {
		return additionalChargeRepository.findByCompanyIdAndIsDeletedOrderByAdditionalChargeIdDesc(companyId, 0);
	}

	@Override
	public void delete(long additionalChargeId, long alterBy, String modifiedOn) {
		additionalChargeRepository.delete(additionalChargeId, alterBy, modifiedOn);

	}

}
