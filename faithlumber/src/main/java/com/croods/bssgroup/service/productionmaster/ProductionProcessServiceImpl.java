package com.croods.bssgroup.service.productionmaster;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.productionmaster.ProductionProcessRepository;
import com.croods.bssgroup.vo.productionmaster.ProductionProcessVo;

@Service
@Transactional
public class ProductionProcessServiceImpl implements ProductionProcessService {

	@Autowired
	ProductionProcessRepository productionProcessRepository;
	
	@Override
	public List<ProductionProcessVo> findByCompanyId(long companyId) {
		return productionProcessRepository.findByCompanyIdAndIsDeleted(companyId,0);
	}

	@Override
	public void save(ProductionProcessVo productionProcessVo) {
		productionProcessRepository.save(productionProcessVo);
		
	}

	@Override
	public ProductionProcessVo findByProductionProcessId(long productionProcessId) {
		// TODO Auto-generated method stub
		return productionProcessRepository.findByProductionProcessIdAndIsDeleted(productionProcessId,0);
	}

	@Override
	public void delete(long id, long userId, String currentDate) {
		productionProcessRepository.deleteProductionProcess(id,userId,currentDate);
		
	}

	@Override
	public List<ProductionProcessVo> findByCompanyIdAndProductionTypeId(long companyId, long productionTypeId) {
		return productionProcessRepository.findByCompanyIdAndProductionTypeVoProductionTypeId(companyId, productionTypeId);
	}

}
