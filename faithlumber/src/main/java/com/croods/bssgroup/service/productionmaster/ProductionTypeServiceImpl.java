package com.croods.bssgroup.service.productionmaster;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.productionmaster.ProductionTypeRepository;
import com.croods.bssgroup.vo.productionmaster.ProductionTypeVo;

@Service
@Transactional
public class ProductionTypeServiceImpl implements ProductionTypeService {

	@Autowired
	ProductionTypeRepository productionTypeRepository;
	
	@Override
	public List<ProductionTypeVo> findAll() {
		return productionTypeRepository.findByIsDeleted(0);
	}

}
