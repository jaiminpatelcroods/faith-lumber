package com.croods.bssgroup.service.lead;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.lead.LeadActivityRepository;
import com.croods.bssgroup.repository.lead.LeadAddressRepository;
import com.croods.bssgroup.repository.lead.LeadContactRepository;
import com.croods.bssgroup.repository.lead.LeadRepository;
import com.croods.bssgroup.vo.lead.LeadActivityVo;
import com.croods.bssgroup.vo.lead.LeadAddressVo;
import com.croods.bssgroup.vo.lead.LeadVo;

@Service
@Transactional
public class LeadServiceImpl implements LeadService {

	@Autowired
	LeadRepository leadRepository;
	
	@Autowired
	LeadActivityRepository leadActivityRepository;
	
	@Autowired
	LeadAddressRepository leadAddressRepository;
	
	@Autowired
	LeadContactRepository leadContactRepository;

	@Override
	public void saveLead(LeadVo leadVo) {
		leadRepository.save(leadVo);
	}

	@Override
	public List<LeadVo> findByBranchId(long branchId) {
		return leadRepository.findByBranchIdAndIsDeletedOrderByLeadIdDesc(branchId, 0);
	}

	@Override
	public LeadVo findByLeadIdAndBranchId(long leadId, long branchId) {
		return leadRepository.findByLeadIdAndBranchIdAndIsDeleted(leadId, branchId, 0);
	}

	@Override
	public void deleteLead(long leadId, long userId, String modifiedOn) {
		leadRepository.deleteLead(leadId, userId, modifiedOn);
	}

	@Override
	public void deleteLeadAddressByIdIn(List<Long> leadAddressIds) {
		leadAddressRepository.deleteLeadAddressByIdIn(leadAddressIds);
	}

	@Override
	public void deleteLeadContactByIdIn(List<Long> leadContactIds) {
		leadContactRepository.deleteLeadContactByIdIn(leadContactIds);
	}

	@Override
	public void changeDefaultAddress(long leadId, long leadAddressId) {
		leadAddressRepository.updateIsDefaultByLeadId(leadId);
		leadAddressRepository.updateIsDefaultByLeadIdAndLeadAddressId(leadId, leadAddressId);
	}

	@Override
	public LeadAddressVo findAddressByLeadAddressId(long leadAddressId) {
		return leadAddressRepository.findByLeadAddressId(leadAddressId);
	}

	@Override
	public void saveLeadActivity(LeadActivityVo leadActivityVo) {
		leadActivityRepository.save(leadActivityVo);
	}
	
	@Override
	public void updateLeadStatus(long leadId, String status,  long userId, String modifiedOn) {
		leadRepository.updateLeadStatus(leadId, status, userId, modifiedOn);
	}
	
	@Override
	public void updateLeadStatusAndAssignedEmployee(long leadId, String status,  long userId, String modifiedOn, long employeeId) {
		leadRepository.updateLeadStatusAndAssignedEmployee(leadId, status, userId, modifiedOn, employeeId);
	}

	@Override
	public List<LeadActivityVo> findActivityByLeadIdAndBranchId(long leadId, long branchId) {
		return leadActivityRepository.findByLeadVoLeadIdAndBranchIdOrderByLeadActivityIdDesc(leadId, branchId);
	}

	@Override
	public void updateCompleteDetails(long leadId, String status, Date completedDate, String remark, long alterBy,
			String modifiedOn) {
		leadRepository.updateCompleteDetails(leadId, status, completedDate, remark, alterBy, modifiedOn);
		
	}
}
