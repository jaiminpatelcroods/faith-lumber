package com.croods.bssgroup.service.deliverychallan;

import java.util.List;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.vo.deliverychallan.DeliveryChallanVo;

public interface DeliveryChallanService {

	public long findMaxDeliveryChallanNo(long companyId, long branchId, long userId, String type, String prefix);

	public DeliveryChallanVo save(DeliveryChallanVo deliveryChallanVo);

	public DeliveryChallanVo findByDeliveryChallanIdAndBranchId(long deliveryChallanId, long branchId);

	public void deleteDeliveryChallanItemIds(List<Long> l);

	public void updateQC(DeliveryChallanVo deliveryChallanVo, String yearInterval);
	
	public DataTablesOutput<DeliveryChallanVo> findAll(DataTablesInput input,
			Specification<DeliveryChallanVo> additionalSpecification, Specification<DeliveryChallanVo> specification);
	
	public void delete(long deliveryChallanId, long userId, String modifiedOn);

	public List<DeliveryChallanVo> findByContactIdAndBranchId(long contactId, long branchId);

	public List<DeliveryChallanVo> findByDeliveryChallanIdIn(List<Long> deliveryChallanIds);
}
