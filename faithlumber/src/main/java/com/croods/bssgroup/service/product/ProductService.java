package com.croods.bssgroup.service.product;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.vo.product.ProductVariantVo;
import com.croods.bssgroup.vo.product.ProductVo;

public interface ProductService {
	
	public ProductVo saveProduct(ProductVo productVo);

	public ProductVo findByProductIdAndCompanyId(long productId, long companyId);
	
	public DataTablesOutput<ProductVo> findAll(DataTablesInput input,
			Specification<ProductVo> additionalSpecification, Specification<ProductVo> specification);

	public List<ProductVo> findByCompanyId(long companyId);
	
	public List<ProductVariantVo> findProductVariantByCompanyId(long companyId);

	public ProductVariantVo findProductVariantByIdAndCompanyId(long productVariantId, long companyId);

	public List<ProductVariantVo> findProductVariants(String searchKey, long companyId);
	
	public List<ProductVariantVo> findProductVariantsWithPackage(String searchKey, long companyId);
	
	public List<ProductVo> getProductWithVariation(long companyId, boolean ispackage);

	public long saveProductPackage(HttpSession session, Map<String, String> allRequestParams,ProductVo productVo);

	public ProductVo findByParantId(long productId, long parseLong);

	public boolean isExistItemCode(long companyId, String itemCode);

	public boolean isExistItemCode(long companyId, String itemCode, long productVariantId);
	
	public ProductVariantVo findByItemCodeAndCompanyId(String itemCode, long parseLong);

	public long countProductByCompanyId(long companyId);
	
	public List<ProductVariantVo> findProductVariantByIdIn(List<Long> productVariantIds);

	public void deleteProduct(long productId, long userId, String modifiedOn);
}
