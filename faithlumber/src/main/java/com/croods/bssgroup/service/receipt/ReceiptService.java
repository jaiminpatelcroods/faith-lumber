package com.croods.bssgroup.service.receipt;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;

import com.croods.bssgroup.vo.receipt.ReceiptVo;

public interface ReceiptService {

	DataTablesOutput<ReceiptVo> findAll(@Valid DataTablesInput input,Specification<ReceiptVo> additionalSpecification,
			Specification<ReceiptVo> specification);

	long findMaxReceiptNo(long companyId, long branchId, long userId, String type, String prefix);

	ReceiptVo save(ReceiptVo receiptVo);

	void deleteReceiptBillVoByIn(List<Long> billIds);

	ReceiptVo findByReceiptIdAndBranchId(long receiptId, long parseLong);

	void transation(ReceiptVo receiptVo, double totalKasar);

}
