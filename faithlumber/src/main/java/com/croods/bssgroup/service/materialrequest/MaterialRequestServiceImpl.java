package com.croods.bssgroup.service.materialrequest;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.croods.bssgroup.repository.materialrequest.MaterialRequestItemRepository;
import com.croods.bssgroup.repository.materialrequest.MaterialRequestRepository;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.materialrequest.MaterialRequestItemVo;
import com.croods.bssgroup.vo.materialrequest.MaterialRequestVo;
import com.croods.bssgroup.vo.prefix.PrefixVo;

@Service
@Transactional
public class MaterialRequestServiceImpl implements MaterialRequestService {

	@Autowired
	MaterialRequestRepository materialRequestRepository;
	
	@Autowired
	MaterialRequestItemRepository materialRequestItemRepository;
	
	@Autowired
	PrefixService prefixService;

	@Override
	public DataTablesOutput<MaterialRequestVo> findAll(DataTablesInput input,
			Specification<MaterialRequestVo> additionalSpecification, Specification<MaterialRequestVo> specification) {
		
		return materialRequestRepository.findAll(input, additionalSpecification, specification);
	}

	@Override
	public void save(MaterialRequestVo materialRequestVo) {
		materialRequestRepository.save(materialRequestVo);
	}
	
	@Override
	public void deleteMaterialRequestItemIds(List<Long> materialRequestItemIds) {
		materialRequestItemRepository.deleteMaterialRequestItemIds(materialRequestItemIds);
	}

	@Override
	public MaterialRequestVo findByMaterialRequestIdAndBranchId(long materialRequestId, long branchId) {
		return materialRequestRepository.findByMaterialRequestIdAndBranchId(materialRequestId, branchId);
	}

	@Override
	public long findMaxRequestNo(String type, long companyId, long branchId, long userId,
			String defaultPrefix) {
		
		List<PrefixVo> prefixVos= prefixService.findByPrefixTypeAndBranchId(type, branchId);
		PrefixVo prefixVo;
		if(prefixVos == null || prefixVos.size()==0)
		{
			prefixVo=new PrefixVo();
			prefixVo.setAlterBy(userId);
			prefixVo.setCreatedBy(userId);
			prefixVo.setBranchId(branchId);
			prefixVo.setCompanyId(companyId);
			prefixVo.setPrefix(defaultPrefix);
			prefixVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			prefixVo.setSequenceNo(1L);
			prefixVo.setPrefixType(type);
			prefixService.save(prefixVo);
		}
		else
		{
			prefixVo=prefixVos.get(0);
			
		}
		
		String maxVoucherNo = materialRequestRepository.findMaxRequestNo(branchId, prefixVo.getPrefix());
		
		if(maxVoucherNo == null || prefixVo.getIsChangeSequnce()==1)
		{
			return prefixVo.getSequenceNo();
		}
		else
		{
			long voucherNo = Long.parseLong(maxVoucherNo);
			voucherNo++;
			return voucherNo;
		}
		
	}

	@Override
	public void delete(long materialRequestId, long userId, String modifiedOn) {
		materialRequestRepository.delete(materialRequestId, userId, modifiedOn);
	}

	@Override
	public List<MaterialRequestVo> findByBranchId(long branchId) {
		
		return materialRequestRepository.findByBranchIdAndIsDeleted(branchId, 0);
	}

	@Override
	public List<MaterialRequestItemVo> findItemByMaterialRequestId(long materialRequestId) {
		return materialRequestItemRepository.findByMaterialRequestVoMaterialRequestId(materialRequestId);
	}

}
