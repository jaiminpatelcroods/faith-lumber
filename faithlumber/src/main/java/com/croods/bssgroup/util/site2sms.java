package com.croods.bssgroup.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;

import com.croods.bssgroup.repository.sms.SmsRepository;
import com.croods.bssgroup.service.sms.SmsService;
import com.croods.bssgroup.vo.sms.SmsVo;

import net.bytebuddy.implementation.bind.annotation.Super;

@Configuration
public class site2sms extends HttpServlet implements ApplicationContextAware {
	
	private static ApplicationContext context;
	
	//private static SmsService smsService;
	
	private static SmsRepository smsRepo;
	
	// Replace your site2sms username and password below
	static final String _userName = "croodscon";
	static final String _password = "crood@12345";

	static HttpSession httpSession;
	static final String _url = "http://ip.shreesms.net/smsserver/SMS200N.aspx";

	static final String charset = "UTF-8";
	
	static long userId = 3;//Long.parseLong(httpSession.getAttribute("userId").toString());
	static long companyId =3; //Long.parseLong(httpSession.getAttribute("companyId").toString());
	static long branchId = 3;//Long.parseLong(httpSession.getAttribute("branchId").toString());
	// to build the query string that will send a message
	private static String buildRequestString(String targetPhoneNo, String message, String senderId) {
		String query = null;
		
		try {
			String[] params = new String[5];
			params[0] = _userName;
			params[1] = _password;
			params[2] = targetPhoneNo;
			params[3] = message;
			params[4] = senderId;

			query = String.format("Userid=%s&UserPassword=%s&PhoneNumber=%s&Text=%s&GSM=%s",
					URLEncoder.encode(params[0], charset), URLEncoder.encode(params[1], charset),
					URLEncoder.encode(params[2], charset), URLEncoder.encode(params[3], charset),
					URLEncoder.encode(params[4], charset));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return query;
	}
	
	
	public static void sendMessage(String reciever, String message, String senderId) {

		try {
			System.out.println("hi!hello");
			// To establish the connection and perform the post request
			URLConnection connection = new URL(_url + "?" + buildRequestString(reciever, message, senderId))
					.openConnection();
			System.out.println(_url + "?" + buildRequestString(reciever, message, senderId));
			// connection.setRequestProperty("Accept-Charset", charset);

			// This automatically fires the request and we can use it to
			// determine the response status
			InputStream response = connection.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(response));
			// System.out.println(br);
			System.out.println("Jaimin SMS------------>");	
			//System.out.println(br.readLine());
			
			System.err.println("Created ON :::-->"+CurrentDateTime.getCurrentDate());
			System.err.println("MOBILE NO. :::-->"+reciever);
			System.err.println("MESSAGE    :::-->"+message);
			System.err.println("GSM SNID   :::-->"+senderId);
			System.err.println("RESPONSE   :::-->"+br.readLine());
			
			
			saveSmsLog(reciever, message, senderId);
			
			
			br.close();	
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	private static void saveSmsLog(String reciever, String message, String senderId) {
		SmsVo smsVo = new SmsVo();
		smsVo.setCreatedBy(userId);
		smsVo.setCompanyId(companyId);
		smsVo.setBranchId(branchId);
		//smsVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		smsVo.setMobileno(reciever);
		smsVo.setMessage(message);
		
		smsRepo = context.getBean(SmsRepository.class);
		smsRepo.save(smsVo);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		context = applicationContext;

	}

	/*
	 * public static void main(String[] args) { try {
	 * System.out.println("enter Mobile No:"); Scanner scanIn = new
	 * Scanner(System.in);
	 * 
	 * String testPhoneNo = scanIn.nextLine(); scanIn.close(); String senderId = "";
	 * String testMessage = "this is demo";
	 * sendMessage(testPhoneNo,testMessage,senderId);
	 * System.out.println("------------"); } catch (Exception e) {
	 * System.out.println("inn main---" + e); } }
	 */
}