package com.croods.bssgroup.util;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

public class CurrentDateTime {

	static Date todayDate;
	static DateFormat dateFormat;
	
	
	public static String getCurrentDate()
	{
		todayDate=new Date();
		dateFormat=new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		return dateFormat.format(todayDate);
	}
	
	public static String getTodayDate()
	{
		todayDate=new Date();
		dateFormat=new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
		return dateFormat.format(todayDate);
	}
	
	public static String getFirstDate(String monthInterval, String defaultYearInterval) {
		// TODO Auto-generated method stub
		System.out.println(defaultYearInterval+"---------"+monthInterval);
		String[] yearList = defaultYearInterval.split("-");
		String[] monthList = monthInterval.split("-");
		
		YearMonth yearMonth = YearMonth.of( Integer.parseInt(yearList[0]),Integer.parseInt(monthList[0]) ); 
		LocalDate firstOfMonth = yearMonth.atDay( 1 );
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		
		return firstOfMonth.format(formatter);
	}

	public static String getLastDate(String monthInterval, String defaultYearInterval) {
		// TODO Auto-generated method stub
		String[] yearList = defaultYearInterval.split("-");
		String[] monthList = monthInterval.split("-");
		
		YearMonth yearMonth = YearMonth.of( Integer.parseInt(yearList[1]),Integer.parseInt(monthList[1]) );
		LocalDate last = yearMonth.atEndOfMonth();
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		
		return last.format(formatter);
	}


	public static java.sql.Date getSqlDate(String string) {
		// TODO Auto-generated method stub
		
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy");
    	
		try {
			return new java.sql.Date((sdf1.parse(string)).getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
