package com.croods.bssgroup.controller.accounting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.repository.account.AccountCustomRepository;
import com.croods.bssgroup.service.account.AccountService;
import com.croods.bssgroup.service.financial.FinancialService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.account.AccountCustomVo;
import com.croods.bssgroup.vo.account.AccountGroupVo;
import com.croods.bssgroup.vo.account.AccountVo;
import com.croods.bssgroup.vo.financial.YearWiseOpeningBalanceVo;

@Controller
@RequestMapping("/account")
public class AccountController {

	@Autowired
	AccountService accountService;
	
	@Autowired
	FinancialService financialService;
	
	@GetMapping("")
	public ModelAndView account(HttpSession session) {
		ModelAndView view = new ModelAndView("accounting/account/account");
		view.addObject("accountCustomVos",accountService.findByBranchIdAndAccounType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.ACCOUNT_CUSTOM));
		view.addObject("accountGroupVos",accountService.findAllAccountGroup());
		return view;
	}
	
	@PostMapping("save")
	@ResponseBody
	public AccountCustomVo saveAccount(@RequestParam(value="accountName") String accountName, @RequestParam(value="group") String accountGroup, @RequestParam(value="debit") String debit, @RequestParam(value="credit") String credit, HttpSession session) {
		
		AccountCustomVo accountCustomVo = new AccountCustomVo();
		
		accountCustomVo.setAccountName(accountName);
		accountCustomVo.setAccounType(Constant.ACCOUNT_CUSTOM);
		accountCustomVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		accountCustomVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		accountCustomVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		accountCustomVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		accountCustomVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		accountCustomVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		accountCustomVo.setIsUpdatable(0);
		
		AccountGroupVo accountGroupVo = new AccountGroupVo();
		accountGroupVo.setAccountGroupId(Long.parseLong(accountGroup));
		accountCustomVo.setGroup(accountGroupVo);
		accountService.insertAccount(accountCustomVo);
		
		return accountCustomVo;
	}
	
	@PostMapping("update")
	@ResponseBody
	public AccountCustomVo updateAccount(@RequestParam(value="accountName") String accountName, @RequestParam(value="group") String accountGroup, @RequestParam(value="accountCustomId") String accountCustomId, HttpSession session) {
		AccountCustomVo accountCustomVo=accountService.findAccountCustomById(Long.parseLong(accountCustomId));
		
		accountCustomVo.setAccountName(accountName);
		accountCustomVo.setAccounType(Constant.ACCOUNT_CUSTOM);
		accountCustomVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		accountCustomVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		AccountGroupVo accountGroupVo = new AccountGroupVo();
		accountGroupVo.setAccountGroupId(Long.parseLong(accountGroup));
		accountCustomVo.setGroup(accountGroupVo);
		
		accountService.insertAccount(accountCustomVo);
		
		return accountCustomVo;
	}
	
	@GetMapping("insertdefault")
	@ResponseBody
	public String accountDefalult(HttpSession session) {
		List<AccountVo> accountCustomVos = accountService.findAllAccount();
		
		for (AccountVo accountVo : accountCustomVos) {
			AccountCustomVo accountCustomVo =new AccountCustomVo();
			
			accountCustomVo.setAccountName(accountVo.getAccountName());
			accountCustomVo.setAccounType(Constant.ACCOUNT_CUSTOM);
			accountCustomVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
			accountCustomVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			accountCustomVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
			accountCustomVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
			accountCustomVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			accountCustomVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			accountCustomVo.setGroup(accountVo.getGroup());
			accountCustomVo.setIsUpdatable(1);
			accountCustomVo.setIsDeleted(0);
			
			accountService.insertAccount(accountCustomVo);
		}
 		return "Success";
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(HttpSession session,@RequestParam(value="id") long id)
	{

		accountService.deleteAccountCustom(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "success";
	}
	
	@PostMapping("typenotin/json")
	@ResponseBody
	public List<AccountCustomVo> accountTypeNotInJSON(HttpSession session) {
		String fromTypes = Constant.TAX + "," + Constant.ADDITIONAL_CHARGE + "," + Constant.BANK;
		
		return  accountService.findByBranchIdAndAccounTypesNotIn(
				Long.parseLong(session.getAttribute("branchId").toString()), Arrays.asList(fromTypes.split(",")));
	}
	
	@GetMapping("openingbalance")
	public ModelAndView accountOpeningBalance(HttpSession session) {
		ModelAndView view = new ModelAndView("accounting/openingbalance/openingbalance");
		view.addObject("financialYearVos",financialService.findAllFinancialYear());
		return view;
	}
	
	@PostMapping("openingbalance/datatable")
	@ResponseBody
	public DataTablesOutput<AccountCustomVo> accountOpeningBalanceDatatable(@Valid DataTablesInput input,@RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		String yearInterval = allRequestParams.get("yearInterval");
		System.out.println(yearInterval);
		
		Specification<AccountCustomVo>  specification = new Specification<AccountCustomVo>() {
			
			@Override
			public Predicate toPredicate(Root<AccountCustomVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<AccountCustomVo> dataTablesOutput = accountService.findAllAccountDatatable(input, null, specification);
		
		dataTablesOutput.getData().forEach(a -> {
			YearWiseOpeningBalanceVo yearWiseOpeningBalanceVo = financialService.findByBranchIdAndAccountCustomIdAndYearInterval(branchId, a.getAccountCustomId(), yearInterval);
			
			if(yearWiseOpeningBalanceVo != null) {
				
				a.setCreditAmount(yearWiseOpeningBalanceVo.getCreditAmount());
				a.setDebitAmount(yearWiseOpeningBalanceVo.getDebitAmount());
			}
		});
		
		return dataTablesOutput;
	}
	
	@PostMapping("/openingbalance/update")
	@ResponseBody
	public String accountOpeningBalanceUpdate(@RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		YearWiseOpeningBalanceVo yearWiseOpeningBalanceVo = financialService.findByBranchIdAndAccountCustomIdAndYearInterval(
				Long.parseLong(session.getAttribute("branchId").toString()),
				Long.parseLong(allRequestParams.get("accountCustomId")),
				allRequestParams.get("yearInterval"));
		if(yearWiseOpeningBalanceVo != null) {
			yearWiseOpeningBalanceVo.setCreditAmount(Double.parseDouble(allRequestParams.get("creditAmount")));
			yearWiseOpeningBalanceVo.setDebitAmount(Double.parseDouble(allRequestParams.get("debitAmount")));
		} else {
			yearWiseOpeningBalanceVo = new YearWiseOpeningBalanceVo();
			AccountCustomVo accountCustomVo = new AccountCustomVo();
			accountCustomVo.setAccountCustomId(Long.parseLong(allRequestParams.get("accountCustomId")));
			yearWiseOpeningBalanceVo.setAccountCustomVo(accountCustomVo);
			yearWiseOpeningBalanceVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
			yearWiseOpeningBalanceVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
			yearWiseOpeningBalanceVo.setYearInterval(allRequestParams.get("yearInterval"));
			yearWiseOpeningBalanceVo.setCreditAmount(Double.parseDouble(allRequestParams.get("creditAmount")));
			yearWiseOpeningBalanceVo.setDebitAmount(Double.parseDouble(allRequestParams.get("debitAmount")));
		}
		
		financialService.saveYearWiseOpeningBalance(yearWiseOpeningBalanceVo);
		
		return "";
	}
}
