package com.croods.bssgroup.controller.rack;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.service.rack.RackService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.rack.RackVo;

@Controller
@RequestMapping("/rack")
public class RackController {

	@Autowired
	RackService rackService;
	
	@GetMapping("")
	public ModelAndView rack(HttpSession session)
 	{
		ModelAndView view=new ModelAndView("rack/rack");
 	
 		view.addObject("rackVos",rackService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
 		return view;			
	}
	
	@PostMapping("save")
	@ResponseBody
	public RackVo saveRack(@RequestParam(value="rackCode") String rackCode, HttpSession session)
 	{
		RackVo rackVo = new RackVo();
		
		rackVo.setRackCode(rackCode);
		rackVo.setNoOfColumn(0);
		rackVo.setNoOfRow(0);
		rackVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		rackVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		rackVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		rackVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		
		rackVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		rackVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		rackService.save(rackVo);
		
		return rackVo;
	}
	
	@PostMapping("update")
	@ResponseBody
	public RackVo updateRack(@RequestParam(value="rackId") long rackId, @RequestParam(value="rackCode") String rackCode, HttpSession session)
	{
		RackVo rackVo = rackService.findByRackId(rackId);
		
		rackVo.setRackCode(rackCode);
		rackVo.setNoOfColumn(0);
		rackVo.setNoOfRow(0);
		rackVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		rackVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		rackService.save(rackVo);
		
		return rackVo;
	}
	
	@PostMapping("delete")
	@ResponseBody
	public String deleteRack(@RequestParam(value="id") long id, HttpSession session)
	{
		rackService.delete(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "Sucess";
	}
	
	@PostMapping("verify")
	@ResponseBody
	public String IsRackExist(@RequestParam("rackCode") String rackCode, @RequestParam(value="rackId",defaultValue="0") String rackId, HttpSession session) {
		boolean isExist = false;
		if(rackId.equals("0")) {
			isExist = rackService.isExistRackCode(Long.parseLong(session.getAttribute("branchId").toString()), rackCode);
		} else {
			isExist = rackService.isExistRackCode(Long.parseLong(session.getAttribute("branchId").toString()), rackCode, Long.parseLong(rackId));
		}
		
		return "{ \"valid\": "+isExist+" }";
	}
}
