package com.croods.bssgroup.controller.receipt;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.service.contact.ContactService;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.service.receipt.ReceiptService;
import com.croods.bssgroup.service.sales.SalesService;
import com.croods.bssgroup.service.transaction.TransactionService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.util.JasperExporter;
import com.croods.bssgroup.util.NumberToWord;
import com.croods.bssgroup.vo.receipt.ReceiptBillVo;
import com.croods.bssgroup.vo.receipt.ReceiptVo;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;


@Controller
@RequestMapping("/receipt")
public class ReceiptController {

	@Autowired
	ReceiptService receiptService;
	
	@Autowired
	ContactService contactService;
	
	@Autowired
	SalesService salesService; 
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	TransactionService transactionService;
	
	JasperReport jasperReport;
	JasperPrint jasperPrint;
	OutputStream outputStream;
	HashMap jasperParameter;
	
	@GetMapping(value = { "", "/", "/list", "list" })
	public ModelAndView receipt(HttpSession session) {
		
		ModelAndView view = new ModelAndView("receipt/receipt");
		
		return view;
	}
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<ReceiptVo> receiptDatatable(@Valid DataTablesInput input, @RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<ReceiptVo>  specification =new Specification<ReceiptVo>() {
			
			@Override
			public Predicate toPredicate(Root<ReceiptVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				query.orderBy(criteriaBuilder.desc(root.get("receiptDate")));
				query.orderBy(criteriaBuilder.desc(root.get("receiptId")));
				try {
					predicates.add(criteriaBuilder.between(root.get("receiptDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<ReceiptVo> dataTablesOutput=receiptService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> {
			p.setReceiptBillVos(null);
			p.setBankVo(null);
			p.getContactVo().setContactAddressVos(null);
			p.getContactVo().setContactOtherVos(null);
			p.getContactVo().setAccountCustomVo(null);
		});
		return dataTablesOutput;
	}
	
	@PostMapping("contact/{contactId}/datatable")
	@ResponseBody
	public DataTablesOutput<ReceiptVo> receiptByContactDatatable(@PathVariable(value="contactId") long contactId, @Valid DataTablesInput input, @RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<ReceiptVo>  specification =new Specification<ReceiptVo>() {
			
			@Override
			public Predicate toPredicate(Root<ReceiptVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				predicates.add(criteriaBuilder.equal(root.get("contactVo").get("contactId"), contactId));
				query.orderBy(criteriaBuilder.desc(root.get("receiptDate")),criteriaBuilder.desc(root.get("receiptId")));
				try {
					predicates.add(criteriaBuilder.between(root.get("receiptDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<ReceiptVo> dataTablesOutput=receiptService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> {
			p.setReceiptBillVos(null);
			p.setBankVo(null);
			p.setContactVo(null);
		});
		return dataTablesOutput;
	}
	
	@RequestMapping("/new")
	public ModelAndView newReceipt(HttpSession session, @RequestParam(value = "contactId", defaultValue="0") String contactId) {
		
		ModelAndView view = new ModelAndView("receipt/receipt-new");
		
		view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_CUSTOMER));
		
		if(!contactId.equals("0")) {
			view.addObject("contactId", contactId);
		}
		
		return view;
	}
	
	@PostMapping("/save")
	public String saveReceipt(@RequestParam Map<String,String> allRequestParams,@ModelAttribute("ReceiptVo") ReceiptVo receiptVo, HttpSession session ) {
		
		double totalKasar=0;
	
		if(receiptVo.getReceiptBillVos()!=null) {
			receiptVo.getReceiptBillVos().removeIf(pay -> pay.getSalesVo() == null);
			receiptVo.getReceiptBillVos().forEach(pay->pay.setReceiptVo(receiptVo));
			totalKasar=receiptVo.getReceiptBillVos().stream().mapToDouble(pay->pay.getKasar()).sum();
			for(ReceiptBillVo BillVo:receiptVo.getReceiptBillVos()){	
				salesService.updateOldPaymentMinus(BillVo.getSalesVo().getSalesId(),BillVo.getOldPyament()+BillVo.getOldKasar());
			}
		}
		
		if(receiptVo.getReceiptId()==0l) {
			receiptVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			receiptVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			receiptVo.setReceiptNo(receiptService.findMaxReceiptNo(
					Long.parseLong(session.getAttribute("companyId").toString()),
					Long.parseLong(session.getAttribute("branchId").toString()),
					Long.parseLong(session.getAttribute("userId").toString()),Constant.RECEIPT, "PAY"));
			receiptVo.setPrefix(prefixService.findByPrefixTypeAndBranchId(Constant.RECEIPT, Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
		}
		
		receiptVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		receiptVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		receiptVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		receiptVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		ReceiptVo receiptVo2=receiptService.save(receiptVo);
		
		if(allRequestParams.get("deleteBillIds") != null && !allRequestParams.get("deleteBillIds").equals("")) {
			String deletedBillIds = allRequestParams.get("deleteBillIds").substring(0, allRequestParams.get("deleteBillIds").length()-1);
			List<Long> billIds= Arrays.asList(deletedBillIds.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());	
			receiptService.deleteReceiptBillVoByIn(billIds);
			
		}
		
		if(receiptVo2.getReceiptBillVos() != null)
			for(ReceiptBillVo receiptBillVo:receiptVo2.getReceiptBillVos()){	
				salesService.addPaidAmountBySalesId(receiptBillVo.getSalesVo().getSalesId(),receiptBillVo.getPayment()+receiptBillVo.getKasar(), Long.parseLong(session.getAttribute("userId").toString()),CurrentDateTime.getCurrentDate());
		}
		
		receiptService.transation(receiptService.findByReceiptIdAndBranchId(receiptVo2.getBranchId(), Long.parseLong(session.getAttribute("branchId").toString())),totalKasar);
		return "redirect:/receipt/"+receiptVo2.getReceiptId();
	}
	
	@GetMapping("/{id}")
	public ModelAndView receiptView(@PathVariable(value="id") long receiptId, HttpSession session) {
		ModelAndView view=new ModelAndView("receipt/receipt-view");
		view.addObject("receiptVo", receiptService.findByReceiptIdAndBranchId(receiptId,Long.parseLong(session.getAttribute("branchId").toString())));
		return view;
	}
	
	@GetMapping("/{id}/edit")
	public ModelAndView receiptEdit(@PathVariable(value="id") long receiptId, HttpSession session) {
		ModelAndView view=new ModelAndView("receipt/receipt-edit");
		
		view.addObject("receiptVo", receiptService.findByReceiptIdAndBranchId(receiptId,Long.parseLong(session.getAttribute("branchId").toString())));
		view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_CUSTOMER));
		return view;
	}
	
	@GetMapping("/{id}/delete")
	public String receiptDelete(@PathVariable(value = "id") long receiptId, HttpSession session)
			throws NumberFormatException, ParseException {
		
		ReceiptVo receiptVo = receiptService.findByReceiptIdAndBranchId(receiptId,Long.parseLong(session.getAttribute("branchId").toString()));
		receiptVo.setAlterBy(Long.parseLong(session.getAttribute("branchId").toString()));
		receiptVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		receiptVo.setIsDeleted(1);
		
		receiptService.save(receiptVo);
		
		List<Long> l = new ArrayList<>();
		
		for(ReceiptBillVo BillVo:receiptVo.getReceiptBillVos()){	
			salesService.updateOldPaymentMinus(BillVo.getSalesVo().getSalesId(),BillVo.getPayment()+BillVo.getKasar());
		}
		transactionService.deleteTransaction(receiptVo.getBranchId(),receiptVo.getReceiptId(), Constant.RECEIPT);
		return "redirect:/receipt";
	}
	
	@GetMapping("{id}/pdf")
	public void receiptPDF(@PathVariable("id") long receiptId, HttpSession session, HttpServletRequest request, HttpServletResponse response) 
	{
		ReceiptVo receiptVo=receiptService.findByReceiptIdAndBranchId(receiptId, Long.parseLong(session.getAttribute("branchId").toString()));
		jasperParameter = new HashMap();
		jasperParameter.put("receipt_invoice_id",receiptId);
		jasperParameter.put("user_front_id",Long.parseLong(session.getAttribute("branchId").toString()));
		jasperParameter.put("display_title","Receipt");
		jasperParameter.put("path", request.getServletContext().getRealPath("/") + "report" + System.getProperty("file.separator"));
		jasperParameter.put("amount_in_word",new NumberToWord().getNumberToWord(receiptVo.getTotalPayment()));
		JasperExporter jasperExporter = new JasperExporter(); 
		try 
		{			
				jasperExporter.jasperExporterPDF(jasperParameter,
						request.getServletContext().getRealPath("/") + "report/receipt"
								+ System.getProperty("file.separator") +"/receipt.jrxml","Certificate-"+receiptId+".pdf", response);					
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
