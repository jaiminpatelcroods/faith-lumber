package com.croods.bssgroup.controller.userrole;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.service.navmenu.NavMenuService;
import com.croods.bssgroup.service.userrole.UserRoleService;
import com.croods.bssgroup.vo.navmenu.NavMenuActionVo;
import com.croods.bssgroup.vo.navmenu.NavMenuPermissionVo;
import com.croods.bssgroup.vo.navmenu.NavSubMenuVo;
import com.croods.bssgroup.vo.userrole.UserRoleVo;

@Controller
@RequestMapping("userrole")
public class UserRoleController {

	@Autowired
	UserRoleService userRoleService;
	
	@Autowired
	NavMenuService navMenuService;
	
	@GetMapping("")
	public ModelAndView userRole(HttpSession session) {
		ModelAndView view = new ModelAndView("userrole/user-role");
		view.addObject("userRoleVos", userRoleService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		return view;
	}
	
	@GetMapping("new")
	public ModelAndView userRoleNew(HttpSession session) {
		ModelAndView view = new ModelAndView("userrole/user-role-new");
		
		view.addObject("navMenuPermission",navMenuService.findNavMenuByUserRoleId(Long.parseLong(session.getAttribute("userType").toString())));
		view.addObject("navSubMenuPermission",navMenuService.findNavSubMenuByUserRoleId(Long.parseLong(session.getAttribute("userType").toString())));
		view.addObject("navMenuAction", navMenuService.findAllNavMenuAction());
		return view;
	}
	
	@PostMapping("save")
	public String userRoleSave(@RequestParam Map<String, String> allRequestParam,  HttpSession session) {
		
		UserRoleVo userRoleVo = new UserRoleVo();
		
		userRoleVo.setUserRoleName(allRequestParam.get("userRoleName"));
		userRoleVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		
		if(allRequestParam.get("userRoleId") != null) {
			userRoleVo.setUserRoleId(Long.parseLong(allRequestParam.get("userRoleId").toString()));
		}
		userRoleService.save(userRoleVo);
		
		List<NavSubMenuVo> navSubMenuVos= navMenuService.findNavSubMenuByUserRoleId(Long.parseLong(session.getAttribute("userType").toString()));
		List<NavMenuActionVo> navMenuActionVos= navMenuService.findAllNavMenuAction();
		
		List<Long> deletePermissionIds =new ArrayList<Long>();
		
		for (NavSubMenuVo navSubMenuVo : navSubMenuVos) {

			for (NavMenuActionVo actionVo : navMenuActionVos) {

				if (allRequestParam.get(actionVo.getActionName() + "-" + navSubMenuVo.getType()) != null &&
						allRequestParam.get(actionVo.getActionName() + "-" + navSubMenuVo.getType() + "-permission") == null) {
					
					NavMenuPermissionVo navMenuPermissionVo = new NavMenuPermissionVo();
					navMenuPermissionVo.setNavMenuActionVo(actionVo);
					navMenuPermissionVo.setNavMenuVo(navSubMenuVo.getNavMenuVo());
					navMenuPermissionVo.setNavSubMenuVo(navSubMenuVo);
					navMenuPermissionVo.setStatus("active");
					navMenuPermissionVo.setUserRoleVo(userRoleVo);
					navMenuService.saveNavMenuPermission(navMenuPermissionVo);
				} else if (allRequestParam.get(actionVo.getActionName() + "-" + navSubMenuVo.getType()) == null &&
						allRequestParam.get(actionVo.getActionName() + "-" + navSubMenuVo.getType() + "-permission") != null) {
					deletePermissionIds.add(Long.parseLong(allRequestParam.get(actionVo.getActionName() + "-" + navSubMenuVo.getType() + "-permission")));
				}
			}
		}
		
		if(deletePermissionIds.size() != 0) {
			navMenuService.deleteByNavMenuPermissionIdIn(deletePermissionIds);
		}
		
		return "redirect:/userrole/"+userRoleVo.getUserRoleId();
	}
	
	@GetMapping("{id}")
	public ModelAndView userRoleView(@PathVariable("id") long userRoleId, HttpSession session) {
		ModelAndView view = new ModelAndView("userrole/user-role-view");
		
		view.addObject("navMenuPermission",navMenuService.findNavMenuByUserRoleId(Long.parseLong(session.getAttribute("userType").toString())));
		view.addObject("navSubMenuPermission",navMenuService.findNavSubMenuByUserRoleId(Long.parseLong(session.getAttribute("userType").toString())));
		view.addObject("navMenuAction", navMenuService.findAllNavMenuAction());
		view.addObject("userRoleVo", userRoleService.findByUserRoleId(userRoleId));
		view.addObject("userNavMenuPermission",navMenuService.findByUserRoleId(userRoleId));
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView userRoleEdit(@PathVariable("id") long userRoleId, HttpSession session) {
		ModelAndView view = new ModelAndView("userrole/user-role-edit");
		
		view.addObject("navMenuPermission",navMenuService.findNavMenuByUserRoleId(Long.parseLong(session.getAttribute("userType").toString())));
		view.addObject("navSubMenuPermission",navMenuService.findNavSubMenuByUserRoleId(Long.parseLong(session.getAttribute("userType").toString())));
		view.addObject("navMenuAction", navMenuService.findAllNavMenuAction());
		view.addObject("userRoleVo", userRoleService.findByUserRoleId(userRoleId));
		view.addObject("userNavMenuPermission",navMenuService.findByUserRoleId(userRoleId));
		return view;
	}
}
