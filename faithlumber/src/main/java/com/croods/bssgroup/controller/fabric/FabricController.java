package com.croods.bssgroup.controller.fabric;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.service.fabric.FabricService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.fabric.FabricVo;

@Controller
@RequestMapping("/fabric")
public class FabricController {

	@Autowired
	FabricService fabricService;
	
	@GetMapping("")
	public ModelAndView fabric(HttpSession session)
 	{
		ModelAndView view=new ModelAndView("fabric/fabric");
 	
 		view.addObject("fabricVos",fabricService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		return view;			
	}
	
	@PostMapping("save")
	@ResponseBody
	public FabricVo saveFabric(@RequestParam(value="fabricName") String fabricName, HttpSession session)
 	{
		FabricVo fabricVo = new FabricVo();
		
		fabricVo.setFabricName(fabricName);
		
		fabricVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		fabricVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		fabricVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		fabricVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		
		fabricVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		fabricVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		fabricService.save(fabricVo);
		
		return fabricVo;
	}
	
	@PostMapping("update")
	@ResponseBody
	public FabricVo updateFabric(@RequestParam(value="fabricId") long fabricId, @RequestParam(value="fabricName") String fabricName, HttpSession session)
	{
		FabricVo fabricVo = fabricService.findByFabricId(fabricId);
		
		fabricVo.setFabricName(fabricName);
		
		fabricVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		fabricVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		fabricService.save(fabricVo);
		
		return fabricVo;
	}
	
	@PostMapping("delete")
	@ResponseBody
	public String deleteFabric(@RequestParam(value="id") long id, HttpSession session)
	{
		fabricService.delete(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "Sucess";
	}
	
	@PostMapping("fabric/verify")
	@ResponseBody
	public String isExistFabric(@RequestParam("fabricName") String fabricName, @RequestParam(value="fabricId",defaultValue="0") String fabricId, HttpSession session) {
		boolean isExist = false;
		if(fabricId.equals("0")) {
			isExist = fabricService.isExistFabric(Long.parseLong(session.getAttribute("companyId").toString()), fabricName);
		} else {
			
			isExist = fabricService.isExistFabric(Long.parseLong(session.getAttribute("companyId").toString()), fabricName, Long.parseLong(fabricId));
			
		}
		System.out.println(fabricId +"////"  +isExist);
		return "{ \"valid\": "+isExist+" }";
	}
}
