	package com.croods.bssgroup.controller.production;
	
	import java.text.DateFormat;
	import java.text.ParseException;
	import java.text.SimpleDateFormat;
	import java.util.ArrayList;
	import java.util.Arrays;
	import java.util.List;
	import java.util.Map;
	import java.util.stream.Collectors;
	
	import javax.persistence.criteria.CriteriaBuilder;
	import javax.persistence.criteria.CriteriaQuery;
	import javax.persistence.criteria.Predicate;
	import javax.persistence.criteria.Root;
	import javax.servlet.http.HttpSession;
	import javax.validation.Valid;
	
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
	import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
	import org.springframework.data.jpa.domain.Specification;
	import org.springframework.stereotype.Controller;
	import org.springframework.ui.Model;
	import org.springframework.web.bind.annotation.GetMapping;
	import org.springframework.web.bind.annotation.ModelAttribute;
	import org.springframework.web.bind.annotation.PathVariable;
	import org.springframework.web.bind.annotation.PostMapping;
	import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RequestParam;
	import org.springframework.web.bind.annotation.ResponseBody;
	import org.springframework.web.servlet.ModelAndView;
	
	import com.croods.bssgroup.constant.Constant;
	import com.croods.bssgroup.service.employee.EmployeeService;
	import com.croods.bssgroup.service.jobwork.JobworkService;
	import com.croods.bssgroup.service.prefix.PrefixService;
	import com.croods.bssgroup.service.production.ProductionService;
	import com.croods.bssgroup.service.sales.SalesService;
	import com.croods.bssgroup.util.CurrentDateTime;
	import com.croods.bssgroup.util.site2sms;
	import com.croods.bssgroup.vo.production.ProductionItemVo;
	import com.croods.bssgroup.vo.production.ProductionVo;
	import com.croods.bssgroup.vo.sales.SalesItemVo;
	import com.croods.bssgroup.vo.sales.SalesVo;
	
	@Controller
	@RequestMapping("/production")
	public class ProductionController {
	
		@Autowired
		ProductionService productionService;
		
		@Autowired
		EmployeeService employeeService;
		
		@Autowired
		PrefixService prefixService;
		
		@Autowired
		JobworkService jobworkService;
		
		@Autowired
		SalesService salesService;
		
		@GetMapping("")
		public ModelAndView purchaseRequest(HttpSession session) {
			ModelAndView view = new ModelAndView("production/production");
			
			return view;
		}
		
		@PostMapping("datatable")
		@ResponseBody
		public DataTablesOutput<ProductionVo> productionDatatable(@Valid DataTablesInput input,@RequestParam Map<String, String> allRequestParams, HttpSession session) {
			
			long branchId = Long.parseLong(session.getAttribute("branchId").toString());
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	
			Specification<ProductionVo>  specification =new Specification<ProductionVo>() {
				
				@Override
				public Predicate toPredicate(Root<ProductionVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<Predicate>();
					//predicates.add(criteriaBuilder.equal(root.get("type"), type));
					predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
					predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
					query.orderBy(criteriaBuilder.desc(root.get("productionDate")));
					query.orderBy(criteriaBuilder.desc(root.get("productionId")));
					try {
						predicates.add(criteriaBuilder.between(root.get("productionDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
								dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
					} catch (ParseException e) {
						e.printStackTrace();
					}
					
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			
			DataTablesOutput<ProductionVo> dataTablesOutput=productionService.findAll(input, null,specification);
			
			dataTablesOutput.getData().forEach(p -> {
				p.setProductionItemVos(null);
				
				p.getSalesVo().getContactVo().setContactOtherVos(null);
				p.getSalesVo().getContactVo().setAccountCustomVo(null);
				p.getSalesVo().getContactVo().setContactAddressVos(null);
				
				p.getSalesVo().setSalesItemVos(null);
				p.getSalesVo().setContactAgentVo(null);
				p.getSalesVo().setSalesVo(null);
				p.getSalesVo().setSalesAdditionalChargeVos(null);
				p.getSalesVo().setAssignedEmployee(null);
				p.getSalesVo().setContactTransportVo(null);
				//p.getSalesVo().setContactVo(null);
				p.getSalesVo().setPaymentTermsVo(null);
			});
			return dataTablesOutput;
		}
		
		@PostMapping("/new")
		public String productionNew(HttpSession session, @RequestParam(value = "contactId", defaultValue="0") String contactId, @RequestParam(value = "parentId", defaultValue="0") String parentId,Model view ) {
			
			view.addAttribute("parentId", parentId);
			List<SalesItemVo> salesItemVos = salesService.findItemBySalesId(Long.parseLong(parentId));
			view.addAttribute("salesItemVos",salesItemVos);
			view.addAttribute("salesOrderNo",salesItemVos.get(0).getSalesVo().getPrefix()+""+salesItemVos.get(0).getSalesVo().getSalesNo());
			System.err.println("salesOrderNo ::::::::"+salesItemVos.get(0).getSalesVo().getPrefix()+""+salesItemVos.get(0).getSalesVo().getSalesNo());
			return "production/production-new";
		}
		
		@PostMapping("save")
		public String saveProduction(@RequestParam Map<String,String> allRequestParams, @ModelAttribute("productionVo") ProductionVo productionVo,HttpSession session) {
			
			productionVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
			productionVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			productionVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
			productionVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
			
			if(productionVo.getProductionId() == 0) {
				productionVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
				productionVo.setCreatedOn(CurrentDateTime.getCurrentDate());
				
				productionVo.setProductionNo(productionService.findMaxProductionNo(Constant.PRODUCTION, 
						Long.parseLong(session.getAttribute("companyId").toString()), 
						Long.parseLong(session.getAttribute("branchId").toString()), 
						Long.parseLong(session.getAttribute("userId").toString()), "PRO"));
				
				productionVo.setPrefix(prefixService.findByPrefixTypeAndBranchId(Constant.PRODUCTION,
						Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
			}
			
			if(productionVo.getProductionItemVos() != null) {
				productionVo.getProductionItemVos().removeIf( rm -> rm.getProductVariantVo() == null);
				productionVo.getProductionItemVos().forEach( item -> item.setProductionVo(productionVo));
			}
			
			if(allRequestParams.get("deleteProductionIds") != null &&
					!allRequestParams.get("deleteProductionIds").equals("")) {
				String address=allRequestParams.get("deleteProductionIds").substring(0, allRequestParams.get("deleteProductionIds").length()-1);
				List<Long> l= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
				
				productionService.deleteProductionItemIds(l);
			}
			
			
			if(productionVo.getSalesVo()!=null && productionVo.getProductionItemVos() != null)
			{
				productionVo.getProductionItemVos().removeIf( rm -> rm.getProductVariantVo() == null);
				productionVo.getProductionItemVos().forEach(item -> {
					if(item.getQty()>=0 && item.getOldProductionQty()>=0) 
					{
						salesService.updateSalesOrderItem(item.getSalesItemId(),item.getProductVariantVo().getProductVariantId(),item.getQty(),item.getOldProductionQty());
					}
				});
			}
			
			ProductionVo productionVo2 = productionService.save(productionVo);
			if(productionVo2.getSalesVo() != null && productionVo2.getSalesVo().getType().equals(Constant.SALES_ORDER)) {
				salesService.updateStatusBySalesId(allRequestParams.get("updateStatus"), productionVo2.getSalesVo().getSalesId(), Long.parseLong(session.getAttribute("userId").toString()),CurrentDateTime.getCurrentDate());
			}
			
			ProductionVo productionVo3 = productionService.findByProductionIdAndBranchId(productionVo2.getProductionId(), Long.parseLong(session.getAttribute("branchId").toString()));
			if(productionVo3.getSalesVo().getStatus().equals("In Progress")) 
			{
				String name=productionVo3.getSalesVo().getContactVo().getCompanyName();
				StringBuffer message= new StringBuffer();
					message.append("Dear"+" "+ name +","+"\n"+"Production Update:"+"\n");
				productionVo3.getProductionItemVos().forEach(m -> {
					message.append(m.getQty()+" ");
					message.append(m.getProductVariantVo().getProductVo().getUnitOfMeasurementVo().getMeasurementCode()+" "+m.getProductVariantVo().getVariantName());
					message.append(" ready out of"+" ");
					message.append(m.getProductionVo().getSalesVo().getSalesItemVos().stream().filter(si -> 
						si.getProductVariantVo().getProductVariantId() == m.getProductVariantVo().getProductVariantId()).collect(Collectors.toList()).get(0).getQty() + " "+
						m.getProductVariantVo().getProductVo().getUnitOfMeasurementVo().getMeasurementCode() +" "+
						m.getProductVariantVo().getVariantName()+" ");
						
					message.append(",");
					});
					message.deleteCharAt(message.length()-1);
					message.append(" order.");
				System.err.println(""+message);
				//site2sms.sendMessage(productionVo3.getSalesVo().getContactVo().getCompanyMobileno(), message+"\n", "VSYERP");
			}
			else if(productionVo3.getSalesVo().getStatus().equals("Production Completed")) 
			{
				StringBuffer message= new StringBuffer();
						message.append("Dear Transporter" +","+" "+"we require a truck as per below given details: ");
						
			    	productionVo3.getSalesVo().getSalesItemVos().forEach(m -> {
			    		message.append("Item:" + m.getProductVariantVo().getProductVo().getName() +" "+ m.getProductVariantVo().getVariantName() + "\n");
						message.append("Qty:" + m.getQty()+" "+"\n");
			    		});
			    			
			    	productionVo3.getSalesVo().getContactVo().getContactAddressVos().forEach(c -> {
			    		message.append("Destination:"+c.getCityName()+"\n");
			    		});
			    			
			    	productionVo3.getSalesVo().getSalesItemVos().forEach(m -> {
			        	message.append("Truck required for:" +" "+ m.getQty()+" "+ m.getProductVariantVo().getProductVo().getUnitOfMeasurementVo().getMeasurementCode() + m.getProductVariantVo().getProductVo().getName() +" "+ m.getProductVariantVo().getVariantName());
			    		});
			        		
			    			message.deleteCharAt(message.length()-2);
				System.err.println(""+message);
	
				//site2sms.sendMessage(productionVo3.getSalesVo().getContactVo().getCompanyMobileno(), message+"\n", "VSYERP");
		    }
			
			return "redirect:/production/"+productionVo3.getProductionId();
		}
		
		@GetMapping("{id}")
		public ModelAndView viewProduction(@PathVariable("id") long productionId, HttpSession session) {
			
			ModelAndView view = new ModelAndView("production/production-view");
			
			ProductionVo productionVo = productionService.findByProductionIdAndBranchId(productionId, Long.parseLong(session.getAttribute("branchId").toString()));
			
			view.addObject("productionVo", productionVo);
			
			return view;
		}
		
		@GetMapping("{id}/edit")
		public ModelAndView productionNew(@PathVariable("id") long productionId, HttpSession session) {
			ModelAndView view = new ModelAndView("production/production-edit");
			
			ProductionVo productionVo = productionService.findByProductionIdAndBranchId(productionId, Long.parseLong(session.getAttribute("branchId").toString()));
			view.addObject("productionVo", productionVo);
			
			List<ProductionItemVo> productionItemVos = productionService.findItemByProductionId(productionId);
			productionItemVos.forEach(m -> {
				m.setSoTotalQty(salesService.findQtyBySalesItemId(m.getSalesItemId()));
				m.setProducedTotalQty(salesService.findProducedQtyBySalesItemId(m.getSalesItemId()));
			});
			view.addObject("productionItemVos", productionItemVos);
			return view;
		}
		
		@GetMapping("{id}/delete")
		public String productionDelete(@PathVariable("id") long productionId, HttpSession session) {
			
			ProductionVo productionVo = productionService.findByProductionIdAndBranchId(productionId, Long.parseLong(session.getAttribute("branchId").toString()));
			if(productionVo.getProductionItemVos() != null) {
				productionVo.getProductionItemVos().removeIf( rm -> rm.getProductVariantVo() == null);
				productionVo.getProductionItemVos().forEach(item -> {
					if(item.getQty()>=0) 
					{
						//For Delete Produced qty from SalesItem 
						salesService.updateSalesOrderItem(item.getSalesItemId(),item.getProductVariantVo().getProductVariantId(),0,item.getQty());
						
						//Check If All Produced equals 0 then change status to 'Pending'
						item.getProductionVo().getSalesVo().getSalesItemVos().forEach(si -> {
							if(si.getProducedQty()==0)
							{
								salesService.updateStatusBySalesId("Pending", item.getProductionVo().getSalesVo().getSalesId(), Long.parseLong(session.getAttribute("userId").toString()),CurrentDateTime.getCurrentDate());
							}
							else
							{
								salesService.updateStatusBySalesId("In Progress", item.getProductionVo().getSalesVo().getSalesId(), Long.parseLong(session.getAttribute("userId").toString()),CurrentDateTime.getCurrentDate());
							}
							System.err.println("Successfully UPDATED Sales Order STATUS ON DELETE PRODUCTION");
						});
					}
				});
			}
			
			productionService.delete(productionId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
			return "redirect:/production";
		}
		
		@PostMapping("{id}/item/json")
		@ResponseBody
		public List<ProductionItemVo> productionItemJSON(@PathVariable("id") long productionId, HttpSession session) {
			
			List<ProductionItemVo> productionItemVos = productionService.findItemByProductionId(productionId);
			
			productionItemVos.forEach(m -> {
				m.setProductionVo(null);
				m.getProductVariantVo().getProductVo().setProductVariantVos(null);
				m.getProductVariantVo().getProductVo().setProductAttributeVos(null);
				m.getProductVariantVo().getProductVo().setProductVo(null);
			});
			return productionItemVos;
		}
		
		@RequestMapping("/{type}/{salesId}/datatable")
		@ResponseBody
		public DataTablesOutput<ProductionVo> salesByContactDatatable(@PathVariable(value="salesId") long salesId, @PathVariable String type, @Valid DataTablesInput input, @RequestParam Map<String,String> allRequestParams,HttpSession session) throws NumberFormatException, ParseException {
			
			long branchId = Long.parseLong(session.getAttribute("branchId").toString());
			
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			
			Specification<ProductionVo>  specification =new Specification<ProductionVo>() {
				
				@Override
				public Predicate toPredicate(Root<ProductionVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
					List<Predicate> predicates = new ArrayList<Predicate>();
					predicates.add(criteriaBuilder.equal(root.get("salesVo").get("type"), type));
					predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
					predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
					predicates.add(criteriaBuilder.equal(root.get("salesVo").get("salesId"), salesId));
					
					query.orderBy(criteriaBuilder.desc(root.get("productionDate")),criteriaBuilder.desc(root.get("productionId")));
					
					/*try {
						predicates.add(criteriaBuilder.between(root.get("salesDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
								dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
					} catch (ParseException e) {
						e.printStackTrace();
					}*/
					
					return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
				}
			};
			
			DataTablesOutput<ProductionVo> dataTablesOutput=productionService.findAll(input, null,specification);
			
			dataTablesOutput.getData().forEach( x -> { 
				  x.setProductionItemVos(null);
				  x.setSalesVo(null);
				});
			 
			System.out.println(dataTablesOutput);
			return dataTablesOutput;
		}
		@RequestMapping("{id}/sendsms/updateproduction")
		@ResponseBody
		public StringBuffer sendsms6(@PathVariable ("id") long productionId, HttpSession session) {
		
			ProductionVo productionVo = productionService.findByProductionIdAndBranchId(productionId, Long.parseLong(session.getAttribute("branchId").toString()));
		
			String name=productionVo.getSalesVo().getContactVo().getCompanyName();
			StringBuffer message= new StringBuffer();
				message.append("Dear"+" "+ name +","+"\n"+"Production Update:"+"\n");
				
				productionVo.getProductionItemVos().forEach(m -> {
				message.append(m.getQty()+" ");
				message.append(m.getProductVariantVo().getProductVo().getUnitOfMeasurementVo().getMeasurementCode()+" "+m.getProductVariantVo().getVariantName());
				message.append("ready out of"+" ");
				message.append(m.getProductionVo().getSalesVo().getSalesItemVos().stream().filter(si -> 
					si.getProductVariantVo().getProductVariantId() == m.getProductVariantVo().getProductVariantId()).collect(Collectors.toList()).get(0).getQty() + " "+
					m.getProductVariantVo().getProductVo().getUnitOfMeasurementVo().getMeasurementCode() +" "+
					m.getProductVariantVo().getVariantName()+" ");
					
				message.append(",");
				});
				message.deleteCharAt(message.length()-1);
				message.append(" order.");
			System.err.println(""+message);
			return message;
			
		}
		
		
		
		//for message shown in modal edit and send
		@RequestMapping("{id}/sendsms")
		@ResponseBody
		public String sendsms1(@PathVariable ("id") long productionId,@RequestParam("phoneno") String phoneno, @RequestParam("message") String message, HttpSession session){
	    	
			ProductionVo productionVo = productionService.findByProductionIdAndBranchId(productionId, Long.parseLong(session.getAttribute("branchId").toString()));
			System.out.println("phoneNo" +phoneno);
			site2sms.sendMessage(productionVo.getSalesVo().getContactVo().getCompanyMobileno(), message, "VSYERP");
			System.err.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+message);
			return "Sucess";	
		}
		
		/*
		 * @RequestMapping("/sendSms")
		 * 
		 * @ResponseBody public void sendsms(@PathVariable("id") long
		 * productionId, @RequestParam("phoneno")String phoneno ,HttpSession session){
		 * 
		 * ProductionVo productionVo =
		 * productionService.findByProductionIdAndBranchId(productionId,
		 * Long.parseLong(session.getAttribute("branchId").toString())); StringBuffer
		 * message= new StringBuffer();
		 * 
		 * List<ProductionItemVo> productionItemVos =
		 * productionService.findItemByProductionId(productionId);
		 * message.append("Dear Sir/Madam,"
		 * +"\n"+"The following Products are selected in your invoice :"+"\n");
		 * productionItemVos.forEach(m -> {
		 * message.append(m.getProductVariantVo().getProductVo().getName()+" ");
		 * message.append(m.getProductVariantVo().getVariantName()+" | ");
		 * message.append(m.getQty()+", "); });
		 * 
		 * message.deleteCharAt(message.length()-2); System.err.println(""+message);
		 * 
		 * //List<String> phonnos = (List<String>)
		 * Arrays.asList(phoneno.split("\\s*,\\s*")).stream().map(String::valueOf).
		 * collect(Collectors.toList()); //for (String s : phonnos) {
		 * //System.out.println(message+"\n"+session.getAttribute("domainName").toString
		 * ()+"/quotation/"+session.getAttribute("branchId").toString()+"/");
		 * //site2sms.sendMessage(productionVo.getSalesVo().getContactVo().
		 * getCompanyMobileno(), // message, // "VSYERP");
		 * 
		 * //return message; }
		 */
		
		//FOR PRODUCTION LIST BUTTON MULTIPLE SELECT CHECKBOX
			@RequestMapping("/sendsms")	
			@ResponseBody
		    public String  sendsms(@RequestParam("phoneno") String phoneno, @RequestParam("message") String message, HttpSession session){
		    	
				
				System.out.println("phoneNo" +phoneno);
				if(String.valueOf(phoneno.charAt(phoneno.length() - 1)).equals(",")) {
					phoneno = phoneno.substring(0, phoneno.length() -1);
				}
				
				site2sms.sendMessage(phoneno, message, "VSYERP");
				
				return "Sucess";	
		    }
		
	}
