package com.croods.bssgroup.controller.sms;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.service.sms.SmsService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.sms.SmsVo;

@Controller
@RequestMapping("/sms")
public class SmsController {

	@Autowired
	SmsService smsService;
	
	@GetMapping("")
	public ModelAndView tax(HttpSession session) {
		ModelAndView view = new ModelAndView("sms/sms");
		//view.addObject("smsVos", smsService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		return view;
	}
	
	
	@RequestMapping("/datatable")
	@ResponseBody
	public DataTablesOutput<SmsVo> smsDatatable(@Valid DataTablesInput input, @RequestParam Map<String,String> allRequestParams,HttpSession session) throws NumberFormatException, ParseException {
		
		System.err.println("At DATATABLE METHOD");
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		Specification<SmsVo>  specification =new Specification<SmsVo>() {
			
			@Override
			public Predicate toPredicate(Root<SmsVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				//predicates.add((Predicate) criteriaBuilder.desc(root.get("salesDate")));
				query.orderBy(criteriaBuilder.desc(root.get("createdOn")),criteriaBuilder.desc(root.get("smsId")));
				
				
				
				try {
					predicates.add(criteriaBuilder.between(root.get("createdOn"), dateFormat.parse(allRequestParams.get("fromDate")),
							dateFormat.parse(allRequestParams.get("toDate"))));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<SmsVo> dataTablesOutput=smsService.findAll(input, null,specification);
		
		return dataTablesOutput;
	}
	
	
}
