package com.croods.bssgroup.controller.deliverychallan;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.service.contact.ContactService;
import com.croods.bssgroup.service.deliverychallan.DeliveryChallanService;
import com.croods.bssgroup.service.employee.EmployeeService;
import com.croods.bssgroup.service.location.LocationService;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.service.product.ProductService;
import com.croods.bssgroup.service.purchase.PurchaseService;
import com.croods.bssgroup.service.purchaserequest.PurchaseRequestService;
import com.croods.bssgroup.service.rack.RackService;
import com.croods.bssgroup.service.stock.StockTransactionService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.deliverychallan.DeliveryChallanItemVo;
import com.croods.bssgroup.vo.deliverychallan.DeliveryChallanVo;
import com.croods.bssgroup.vo.purchase.PurchaseItemVo;
import com.croods.bssgroup.vo.purchase.PurchaseVo;
import com.croods.bssgroup.vo.purchaserequest.PurchaseRequestItemVo;
import com.croods.bssgroup.vo.purchaserequest.PurchaseRequestVo;

@Controller
@RequestMapping("deliverychallan")
public class DeliveryChallanController {

	@Autowired
	ContactService contactService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	DeliveryChallanService deliveryChallanService;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	LocationService locationService;
	
	@Autowired
	PurchaseService purchaseService;
	
	@Autowired
	PurchaseRequestService purchaseRequestService;
	
	@Autowired
	RackService rackService;
	
	@RequestMapping("new")
	public ModelAndView newDeliveryChallan(HttpSession session, @RequestParam(value = "purchaseId", defaultValue="0") String purchaseId) {
		
		ModelAndView view = new ModelAndView("deliverychallan/deliverychallan-new");
		
		long newChallanNo = deliveryChallanService.findMaxDeliveryChallanNo(
				Long.parseLong(session.getAttribute("companyId").toString()),
				Long.parseLong(session.getAttribute("branchId").toString()),
				Long.parseLong(session.getAttribute("userId").toString()), Constant.PURCHASE_DELIVERY_CHALLAN, "DC");
		
		view.addObject("deliveryChallanNo", newChallanNo);
		
		view.addObject("prefix",prefixService.findByPrefixTypeAndBranchId(Constant.PURCHASE_DELIVERY_CHALLAN, Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
		view.addObject("employeeVos", employeeService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
		view.addObject("productVariantVos", productService.findProductVariantByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		
		if(!purchaseId.equals("0")) {
			
			List<PurchaseItemVo> purchaseItemVos = purchaseService.findItemByPurchaseId(Long.parseLong(purchaseId));
			
			if(purchaseItemVos != null && purchaseItemVos.size() != 0) {
				
				List<DeliveryChallanItemVo> deliveryChallanItemVos = new ArrayList<DeliveryChallanItemVo>();
				for (PurchaseItemVo purchaseItemVo : purchaseItemVos) {
					
					DeliveryChallanItemVo deliveryChallanItemVo = new DeliveryChallanItemVo();
					
					deliveryChallanItemVo.setQty(purchaseItemVo.getQty());
					deliveryChallanItemVo.setProductVariantVo(purchaseItemVo.getProductVariantVo());
					
					deliveryChallanItemVos.add(deliveryChallanItemVo);
					
				}
				
				view.addObject("deliveryChallanItemVos",deliveryChallanItemVos);
				view.addObject("purchaseId", purchaseId);
				view.addObject("purchaseNo", purchaseService.findPurchaseNoByPurchaseId(Long.parseLong(purchaseId)));
				
				view.addObject("contactId", purchaseItemVos.get(0).getPurchaseVo().getContactVo().getContactId());
			}
		}
		return view;
	}
	
	@GetMapping("")
	public ModelAndView purchaseRequest(HttpSession session) {
		ModelAndView view = new ModelAndView("deliverychallan/deliverychallan");
		
		return view;
	}
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<DeliveryChallanVo> deliveryChallanDatatable(@Valid DataTablesInput input,@RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<DeliveryChallanVo>  specification =new Specification<DeliveryChallanVo>() {
			
			@Override
			public Predicate toPredicate(Root<DeliveryChallanVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				query.orderBy(criteriaBuilder.desc(root.get("deliveryChallanDate")));
				query.orderBy(criteriaBuilder.desc(root.get("deliveryChallanId")));
				
				try {
					predicates.add(criteriaBuilder.between(root.get("deliveryChallanDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<DeliveryChallanVo> dataTablesOutput=deliveryChallanService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> {
			p.setDeliveryChallanItemVos(null);
			p.getContactVo().setContactAddressVos(null);
			p.getContactVo().setContactOtherVos(null);
			p.getReceivedBy().setEmployeeContactVos(null);
			if(p.getQcBy() != null) {
				p.getQcBy().setEmployeeContactVos(null);
			}
		});
		return dataTablesOutput;
	}
	
	@PostMapping("save")
	public String saveDeliveryChallan(@RequestParam Map<String,String> allRequestParams, @ModelAttribute("deliveryChallanVo") DeliveryChallanVo deliveryChallanVo,HttpSession session) {
		
		deliveryChallanVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		deliveryChallanVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		deliveryChallanVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		deliveryChallanVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		if(deliveryChallanVo.getDeliveryChallanId() == 0) {
			deliveryChallanVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			deliveryChallanVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		}
		deliveryChallanVo.setStatus("Pending");
		if(deliveryChallanVo.getDeliveryChallanItemVos() != null) {
			deliveryChallanVo.getDeliveryChallanItemVos().removeIf( rm -> rm.getProductVariantVo() == null);
			deliveryChallanVo.getDeliveryChallanItemVos().forEach( item -> item.setDeliveryChallanVo(deliveryChallanVo));
		}
		
		if(allRequestParams.get("deleteDeliveryChallanItemIds") != null &&
				!allRequestParams.get("deleteDeliveryChallanItemIds").equals("")) {
			String address=allRequestParams.get("deleteDeliveryChallanItemIds").substring(0, allRequestParams.get("deleteDeliveryChallanItemIds").length()-1);
			List<Long> l= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
			
			deliveryChallanService.deleteDeliveryChallanItemIds(l);
		}
		
		deliveryChallanService.save(deliveryChallanVo);
		
		if(deliveryChallanVo.getPurchaseId() != 0) {
			purchaseRequestService.updateStatusByPurchaseId("Completed", deliveryChallanVo.getAlterBy(), CurrentDateTime.getCurrentDate(), deliveryChallanVo.getPurchaseId());
		}
		return "redirect:/deliverychallan/"+deliveryChallanVo.getDeliveryChallanId();
	}
	
	@GetMapping("{id}")
	public ModelAndView viewDeliveryChallan(@PathVariable("id") long deliveryChallanId, HttpSession session) {
		
		DeliveryChallanVo deliveryChallanVo = deliveryChallanService.findByDeliveryChallanIdAndBranchId(deliveryChallanId, Long.parseLong(session.getAttribute("branchId").toString()));
		
		ModelAndView view = new ModelAndView("deliverychallan/deliverychallan-view");
		
		deliveryChallanVo.getContactVo().getContactAddressVos().removeIf( pp -> pp.getIsDefault() == 0);
		
		deliveryChallanVo.getContactVo().getContactAddressVos().forEach(p -> {
			p.setCountriesName(locationService.findCountriesNameByCountriesCode(p.getCountriesCode()));
			p.setStateName(locationService.findStateNameByStateCode(p.getStateCode()));
			p.setCityName(locationService.findCityNameByCityCode(p.getCityCode()));
		});
		
		view.addObject("employeeVos", employeeService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		
		if(deliveryChallanVo.getPurchaseId() != 0) {
			view.addObject("purchaseNo", purchaseService.findPurchaseNoByPurchaseId(deliveryChallanVo.getPurchaseId()));
		}
		
		view.addObject("deliveryChallanVo", deliveryChallanVo);
		view.addObject("rackVos",rackService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView editDeliveryChallan(@PathVariable("id") long deliveryChallanId, HttpSession session) {
		
		DeliveryChallanVo deliveryChallanVo = deliveryChallanService.findByDeliveryChallanIdAndBranchId(deliveryChallanId, Long.parseLong(session.getAttribute("branchId").toString()));
		
		ModelAndView view = new ModelAndView();
		
		deliveryChallanVo.getContactVo().getContactAddressVos().removeIf( pp -> pp.getIsDefault() == 0);
		
		deliveryChallanVo.getContactVo().getContactAddressVos().forEach(p -> {
			p.setCountriesName(locationService.findCountriesNameByCountriesCode(p.getCountriesCode()));
			p.setStateName(locationService.findStateNameByStateCode(p.getStateCode()));
			p.setCityName(locationService.findCityNameByCityCode(p.getCityCode()));
		});
		
		view.addObject("deliveryChallanVo", deliveryChallanVo);
		
		if(deliveryChallanVo.getPurchaseId() != 0) {
			view.addObject("purchaseNo", purchaseService.findPurchaseNoByPurchaseId(deliveryChallanVo.getPurchaseId()));
		}
		
		if(deliveryChallanVo.getStatus().equals("Pending")) {
			view.addObject("employeeVos", employeeService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
			view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
			view.addObject("productVariantVos", productService.findProductVariantByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
			view.setViewName("deliverychallan/deliverychallan-edit");
		} else {
			view.setViewName("deliverychallan/deliverychallan-view");
		}
		
		return view;
	}
	
	@PostMapping("{id}/qc/save")
	public String qcDeliveryChallan(@PathVariable("id") long deliveryChallanId, @ModelAttribute("deliveryChallanVo") DeliveryChallanVo deliveryChallanVo, HttpSession session) {
		
		deliveryChallanVo.setDeliveryChallanId(deliveryChallanId);
		
		deliveryChallanVo.setStatus("Complete");
		deliveryChallanVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		deliveryChallanVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		deliveryChallanVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		
		deliveryChallanService.updateQC(deliveryChallanVo, session.getAttribute("financialYear").toString());
		
		return "redirect:/deliverychallan/"+deliveryChallanVo.getDeliveryChallanId();
	}
	
	@GetMapping("{id}/delete")
	public String deletedeliveryChallan(@PathVariable("id") long deliveryChallanId, HttpSession session) {
		deliveryChallanService.delete(deliveryChallanId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "redirect:/deliverychallan";
	}
	
	@PostMapping("contact/{contactId}/json")
	@ResponseBody
	public List<DeliveryChallanVo> deliveryChallanJsonByContact(@PathVariable("contactId") long contactId, HttpSession session) {
		List<DeliveryChallanVo> deliveryChallanVos = deliveryChallanService.findByContactIdAndBranchId(contactId, Long.parseLong(session.getAttribute("branchId").toString()));
		
		deliveryChallanVos.forEach(d -> {
			d.setContactVo(null);
			d.setDeliveryChallanItemVos(null);
			d.setQcBy(null);
			d.setReceivedBy(null);
		});
		return deliveryChallanVos;
	}
}
