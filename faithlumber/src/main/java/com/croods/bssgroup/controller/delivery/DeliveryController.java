package com.croods.bssgroup.controller.delivery;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.service.contact.ContactService;
import com.croods.bssgroup.service.delivery.DeliveryService;
import com.croods.bssgroup.service.employee.EmployeeService;
import com.croods.bssgroup.service.jobwork.JobworkService;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.service.sales.SalesService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.util.site2sms;
import com.croods.bssgroup.vo.delivery.DeliveryItemVo;
import com.croods.bssgroup.vo.delivery.DeliveryVo;
import com.croods.bssgroup.vo.production.ProductionItemVo;
import com.croods.bssgroup.vo.production.ProductionVo;
import com.croods.bssgroup.vo.sales.SalesItemVo;

@Controller
@RequestMapping("/delivery")
public class DeliveryController {

	@Autowired
	DeliveryService deliveryService;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	JobworkService jobworkService;
	
	@Autowired
	SalesService salesService;
	
	@Autowired
	ContactService contactService;
	
	@GetMapping("")
	public ModelAndView purchaseRequest(HttpSession session) {
		ModelAndView view = new ModelAndView("delivery/delivery");
		
		return view;
	}
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<DeliveryVo> deliveryDatatable(@Valid DataTablesInput input,@RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<DeliveryVo>  specification =new Specification<DeliveryVo>() {
			
			@Override
			public Predicate toPredicate(Root<DeliveryVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				query.orderBy(criteriaBuilder.desc(root.get("deliveryDate")));
				//query.orderBy(criteriaBuilder.desc(root.get("deliveryId")));
				try {
					predicates.add(criteriaBuilder.between(root.get("deliveryDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<DeliveryVo> dataTablesOutput=deliveryService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> {
			p.setDeliveryItemVos(null);
			p.setContactVo(null);
			p.setContactTransportVo(null);
			//For Sales Order
			p.getSalesVo().getContactVo().setContactOtherVos(null);
			p.getSalesVo().getContactVo().setAccountCustomVo(null);
			p.getSalesVo().getContactVo().setContactAddressVos(null);
			p.getSalesVo().setSalesItemVos(null);
			p.getSalesVo().setContactAgentVo(null);
			p.getSalesVo().setSalesVo(null);
			p.getSalesVo().setSalesAdditionalChargeVos(null);
			
		});
		return dataTablesOutput;
	}
	
	@PostMapping("/new")
	public String deliveryNew(HttpSession session, @RequestParam(value = "contactId", defaultValue="0") String contactId, @RequestParam(value = "parentId", defaultValue="0") String parentId,Model view ) {
		
		view.addAttribute("parentId", parentId);
		view.addAttribute("contactTransportVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_TRANSPORT));
		List<SalesItemVo> salesItemVos = salesService.findItemBySalesId(Long.parseLong(parentId));
		view.addAttribute("salesItemVos",salesItemVos);
		view.addAttribute("salesOrderNo",salesItemVos.get(0).getSalesVo().getPrefix()+""+salesItemVos.get(0).getSalesVo().getSalesNo());
		System.err.println("salesOrderNo ::::::::"+salesItemVos.get(0).getSalesVo().getPrefix()+""+salesItemVos.get(0).getSalesVo().getSalesNo());
		
		view.addAttribute("status",salesItemVos.get(0).getSalesVo().getStatus());
		return "delivery/delivery-new";
	}
	
	@PostMapping("save")
	public String saveDelivery(@RequestParam Map<String,String> allRequestParams, @ModelAttribute("deliveryVo") DeliveryVo deliveryVo,HttpSession session) {
		
		deliveryVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		deliveryVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		deliveryVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		deliveryVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		if(deliveryVo.getDeliveryId() == 0) {
			deliveryVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			deliveryVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			
			deliveryVo.setDeliveryNo(deliveryService.findMaxDeliveryNo(Constant.DELIVERY, 
					Long.parseLong(session.getAttribute("companyId").toString()), 
					Long.parseLong(session.getAttribute("branchId").toString()), 
					Long.parseLong(session.getAttribute("userId").toString()), "DLV"));
			
			deliveryVo.setPrefix(prefixService.findByPrefixTypeAndBranchId(Constant.DELIVERY,
					Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
		}
		
		if(deliveryVo.getDeliveryItemVos() != null) {
			deliveryVo.getDeliveryItemVos().removeIf( rm -> rm.getProductVariantVo() == null);
			deliveryVo.getDeliveryItemVos().forEach( item -> item.setDeliveryVo(deliveryVo));
		}
		
		if(allRequestParams.get("deleteDeliveryIds") != null &&
				!allRequestParams.get("deleteDeliveryIds").equals("")) {
			String address=allRequestParams.get("deleteDeliveryIds").substring(0, allRequestParams.get("deleteDeliveryIds").length()-1);
			List<Long> l= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
			
			deliveryService.deleteDeliveryItemIds(l);
		}
		
		
		if(deliveryVo.getSalesVo()!=null && deliveryVo.getDeliveryItemVos() != null)
		{
			deliveryVo.getDeliveryItemVos().removeIf( rm -> rm.getProductVariantVo() == null);
			deliveryVo.getDeliveryItemVos().forEach(item -> {
				if(item.getQty()>=0 && item.getOldDeliveredQty()>=0) 
				{
					salesService.updateSalesOrderItemForDelivery(item.getSalesItemId(),item.getProductVariantVo().getProductVariantId(),item.getQty(),item.getOldDeliveredQty());
				}
			});
		}
		
		DeliveryVo deliveryVo2 = deliveryService.save(deliveryVo);
		if(deliveryVo2.getSalesVo() != null && deliveryVo2.getSalesVo().getType().equals(Constant.SALES_ORDER)) {
			salesService.updateStatusBySalesId(allRequestParams.get("updateStatus"), deliveryVo2.getSalesVo().getSalesId(), Long.parseLong(session.getAttribute("userId").toString()),CurrentDateTime.getCurrentDate());
		}
		
		DeliveryVo deliveryVo3 = deliveryService.findByDeliveryIdAndBranchId(deliveryVo2.getDeliveryId(), Long.parseLong(session.getAttribute("branchId").toString()));
		//SMS Remains for Delivery...
		
		return "redirect:/delivery/"+deliveryVo3.getDeliveryId();
	}
	
	@GetMapping("{id}")
	public ModelAndView viewDelivery(@PathVariable("id") long deliveryId, HttpSession session) {
		ModelAndView view = new ModelAndView("delivery/delivery-view");
		DeliveryVo deliveryVo = deliveryService.findByDeliveryIdAndBranchId(deliveryId, Long.parseLong(session.getAttribute("branchId").toString()));
		view.addObject("deliveryVo", deliveryVo);
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView deliveryNew(@PathVariable("id") long deliveryId, HttpSession session) {
		ModelAndView view = new ModelAndView("delivery/delivery-edit");
		
		DeliveryVo deliveryVo = deliveryService.findByDeliveryIdAndBranchId(deliveryId, Long.parseLong(session.getAttribute("branchId").toString()));
		view.addObject("deliveryVo", deliveryVo);
		
		List<DeliveryItemVo> deliveryItemVos = deliveryService.findItemByDeliveryId(deliveryId);
		deliveryItemVos.forEach(m -> {
			m.setSoTotalQty(salesService.findQtyBySalesItemId(m.getSalesItemId()));
			m.setDeliveredTotalQty(salesService.findDeliveredQtyBySalesItemId(m.getSalesItemId()));
		});
		view.addObject("deliveryItemVos", deliveryItemVos);
		
		view.addObject("contactTransportVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_TRANSPORT));
		return view;
	}
	
	@GetMapping("{id}/delete")
	public String deliveryDelete(@PathVariable("id") long deliveryId, HttpSession session) {
		
		DeliveryVo deliveryVo = deliveryService.findByDeliveryIdAndBranchId(deliveryId, Long.parseLong(session.getAttribute("branchId").toString()));
		if(deliveryVo.getDeliveryItemVos() != null) {
			deliveryVo.getDeliveryItemVos().removeIf( rm -> rm.getProductVariantVo() == null);
			deliveryVo.getDeliveryItemVos().forEach(item -> {
				if(item.getQty()>=0) 
				{
					//For Delete Delivered qty from SalesItem 
					salesService.updateSalesOrderItemForDelivery(item.getSalesItemId(),item.getProductVariantVo().getProductVariantId(),0,item.getQty());
					
					//Check If All Delivered equals 0 then change status to 'Pending'
					item.getDeliveryVo().getSalesVo().getSalesItemVos().forEach(si -> {
						if(si.getDeliveredQty()==0 && si.getProducedQty()==0)
						{
							salesService.updateStatusBySalesId("Pending", item.getDeliveryVo().getSalesVo().getSalesId(), Long.parseLong(session.getAttribute("userId").toString()),CurrentDateTime.getCurrentDate());
						}
						else if(si.getDeliveredQty()==0 && si.getProducedQty()!=0)
						{
							salesService.updateStatusBySalesId("In Progress", item.getDeliveryVo().getSalesVo().getSalesId(), Long.parseLong(session.getAttribute("userId").toString()),CurrentDateTime.getCurrentDate());
						}
						else if(si.getQty()==si.getProducedQty())
						{
							salesService.updateStatusBySalesId("Production Completed", item.getDeliveryVo().getSalesVo().getSalesId(), Long.parseLong(session.getAttribute("userId").toString()),CurrentDateTime.getCurrentDate());
						}
							
						System.err.println("Successfully UPDATED Sales Order STATUS ON DELETE DELIVERY");
					});
				}
			});
		}
		deliveryService.delete(deliveryId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "redirect:/delivery";
	}
	
	@PostMapping("{id}/item/json")
	@ResponseBody
	public List<DeliveryItemVo> deliveryItemJSON(@PathVariable("id") long deliveryId, HttpSession session) {
		
		List<DeliveryItemVo> deliveryItemVos = deliveryService.findItemByDeliveryId(deliveryId);
		
		deliveryItemVos.forEach(m -> {
			m.setDeliveryVo(null);
			m.getProductVariantVo().getProductVo().setProductVariantVos(null);
			m.getProductVariantVo().getProductVo().setProductAttributeVos(null);
			m.getProductVariantVo().getProductVo().setProductVo(null);
		});
		return deliveryItemVos;
	}
	
	@RequestMapping("/{type}/{salesId}/datatable")
	@ResponseBody
	public DataTablesOutput<DeliveryVo> salesByContactDatatable(@PathVariable(value="salesId") long salesId, @PathVariable String type, @Valid DataTablesInput input, @RequestParam Map<String,String> allRequestParams,HttpSession session) throws NumberFormatException, ParseException {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		Specification<DeliveryVo>  specification =new Specification<DeliveryVo>() {
			
			@Override
			public Predicate toPredicate(Root<DeliveryVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				predicates.add(criteriaBuilder.equal(root.get("salesVo").get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				predicates.add(criteriaBuilder.equal(root.get("salesVo").get("salesId"), salesId));
				
				query.orderBy(criteriaBuilder.desc(root.get("deliveryDate")),criteriaBuilder.desc(root.get("deliveryId")));
				
				/*try {
					predicates.add(criteriaBuilder.between(root.get("salesDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}*/
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<DeliveryVo> dataTablesOutput=deliveryService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach( x -> { 
			  x.setDeliveryItemVos(null);
			  x.setSalesVo(null);
			  x.setContactVo(null);
			  x.setContactTransportVo(null);
			 
		  });
		 
		System.out.println(dataTablesOutput);
		return dataTablesOutput;
	}
	
	
	  /*@RequestMapping("/sendsms")
	  @ResponseBody
	  public String sendsms(@RequestParam("phoneno")String phoneno, @RequestParam("message")String message,HttpSession session){
	  
	  List<String> phonnos = (List<String>)
	  Arrays.asList(phoneno.split("\\s*,\\s*")).stream().map(String::valueOf).
	  collect(Collectors.toList());
	  
	  for (String s : phonnos) {
		  
		  if(String.valueOf(phoneno.charAt(phoneno.length() - 1)).equals(",")) {
				phoneno = phoneno.substring(0, phoneno.length() -1);
			}
			
			site2sms.sendMessage(phoneno, message, "VSYERP");
	  	}
	  	return "Sucess";
	  
	  }*/
	 
	
	@RequestMapping("/sendsms")	
	@ResponseBody
    public String  sendsms(@RequestParam("phoneno") String phoneno, @RequestParam("message") String message, HttpSession session){
    	
		
		System.out.println("phoneNo" +phoneno);
		if(String.valueOf(phoneno.charAt(phoneno.length() - 1)).equals(",")) {
			phoneno = phoneno.substring(0, phoneno.length() -1);
		}
		
		site2sms.sendMessage(phoneno, message, "VSYERP");
		
		return "Sucess";	
    }
}
