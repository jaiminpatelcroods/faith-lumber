package com.croods.bssgroup.controller.style;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.service.style.StyleService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.style.StyleVo;

@Controller
@RequestMapping("/style")
public class StyleController {

	@Autowired
	StyleService styleService;
	
	@GetMapping("")
	public ModelAndView style(HttpSession session)
 	{
		ModelAndView view=new ModelAndView("style/style");
 	
 		view.addObject("styleVos",styleService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		return view;			
	}
	
	@PostMapping("save")
	@ResponseBody
	public StyleVo saveStyle(@RequestParam(value="styleName") String styleName, HttpSession session)
 	{
		StyleVo styleVo = new StyleVo();
		
		styleVo.setStyleName(styleName);
		
		styleVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		styleVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		styleVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		styleVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		
		styleVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		styleVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		styleService.save(styleVo);
		
		return styleVo;
	}
	
	@PostMapping("update")
	@ResponseBody
	public StyleVo updateStyle(@RequestParam(value="styleId") long styleId, @RequestParam(value="styleName") String styleName, HttpSession session)
	{
		StyleVo styleVo = styleService.findByStyleId(styleId);
		
		styleVo.setStyleName(styleName);
		
		styleVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		styleVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		styleService.save(styleVo);
		
		return styleVo;
	}
	
	@PostMapping("delete")
	@ResponseBody
	public String deleteStyle(@RequestParam(value="id") long id, HttpSession session)
	{
		styleService.delete(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "Sucess";
	}
	
	@PostMapping("style/verify")
	@ResponseBody
	public String isExistStyle(@RequestParam("styleName") String styleName, @RequestParam(value="styleId",defaultValue="0") String styleId, HttpSession session) {
		boolean isExist = false;
		if(styleId.equals("0")) {
			isExist = styleService.isExistStyle(Long.parseLong(session.getAttribute("companyId").toString()), styleName);
		} else {
			
			isExist = styleService.isExistStyle(Long.parseLong(session.getAttribute("companyId").toString()), styleName, Long.parseLong(styleId));
			
		}
		System.out.println(styleId +"////"  +isExist);
		return "{ \"valid\": "+isExist+" }";
	}
}
