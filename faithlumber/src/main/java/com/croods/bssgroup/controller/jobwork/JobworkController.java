package com.croods.bssgroup.controller.jobwork;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.service.contact.ContactService;
import com.croods.bssgroup.service.employee.EmployeeService;
import com.croods.bssgroup.service.farmotype.FarmoTypeService;
import com.croods.bssgroup.service.jobwork.JobworkService;
import com.croods.bssgroup.service.location.LocationService;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.service.product.ProductService;
import com.croods.bssgroup.service.productionmaster.ProductionProcessService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.deliverychallan.DeliveryChallanVo;
import com.croods.bssgroup.vo.jobwork.JobworkFarmoTypeParticularVo;
import com.croods.bssgroup.vo.jobwork.JobworkFarmoTypeVo;
import com.croods.bssgroup.vo.jobwork.JobworkPackageVo;
import com.croods.bssgroup.vo.jobwork.JobworkVo;
import com.croods.bssgroup.vo.product.ProductVo;
import com.croods.bssgroup.vo.productionmaster.ProductionProcessVo;

@Controller
@RequestMapping("jobwork")
public class JobworkController {
	
	@Autowired
	ContactService contactService;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	JobworkService jobworkService;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	FarmoTypeService farmoTypeService;
	
	@Autowired
	ProductionProcessService productionProcessService;
	
	@Autowired
	LocationService locationService;
	
	@GetMapping("")
	public ModelAndView jobwork(HttpSession session) {
		ModelAndView view = new ModelAndView("jobwork/jobwork");
		return view;
	}
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<JobworkVo> jobworkDatatable(@Valid DataTablesInput input,@RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<JobworkVo>  specification =new Specification<JobworkVo>() {
			
			@Override
			public Predicate toPredicate(Root<JobworkVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				query.orderBy(criteriaBuilder.desc(root.get("jobworkDate")));
				query.orderBy(criteriaBuilder.desc(root.get("jobworkId")));
				
				try {
					predicates.add(criteriaBuilder.between(root.get("jobworkDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<JobworkVo> dataTablesOutput=jobworkService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> {
			p.setJobworkFarmoTypeParticularVos(null);
			p.setJobworkFarmoTypeVos(null);
			p.setJobworkInputVos(null);
			p.setJobworkOutputVos(null);
			p.setJobworkPackageVos(null);
			p.setJobworkProcessVos(null);
			p.setAntriLarringApproveBy(null);
			p.setAntriLarringBy(null);
			p.setBalGajApproveBy(null);
			p.setBalGajContactVo(null);
			p.setBalGajHandoverBy(null);
			p.setBalGajReceivedBy(null);
			p.setCuttingApproveBy(null);
			p.setCuttingBy(null);
			p.setDrawingApproveBy(null);
			p.setDrawingBy(null);
			p.setPressingContactVo(null);
			p.setPressingHandoverBy(null);
			p.setPressingReceivedBy(null);
			p.setProductVo(null);
			p.setSortApproveBy(null);
			p.setSortBy(null);
			p.setWashingContactVo(null);
			p.setWashingHandoverBy(null);
			p.setWashingReceivedBy(null);
			p.setWashingSampleReceivedBy(null);
		});
		return dataTablesOutput;
	}
	
	@RequestMapping("new")
	public ModelAndView jobworkNew(HttpSession session) {
		ModelAndView view = new ModelAndView("jobwork/jobwork-step1-antri-larring");
		view.addObject("employeeVos", employeeService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		view.addObject("productVos", productService.getProductWithVariation(Long.parseLong(session.getAttribute("companyId").toString()),false));
		view.addObject("productVariantVos", productService.findProductVariantByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		JobworkVo jobworkVo = new JobworkVo();
		jobworkVo.setJobworkDate(new Date());
		view.addObject("jobworkVo", jobworkVo);
		view.addObject("isEditable", true);
		return view;
	}
	
	@PostMapping("save")
	public String jobworkSave(@ModelAttribute("jobworkVo") JobworkVo jobworkVo, @RequestParam Map<String,String> allRequestParams, HttpSession session) {
		
		JobworkVo jobworkVo2 = null;
		if(jobworkVo.getJobworkId() == 0) {
			jobworkVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			jobworkVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			jobworkVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
			jobworkVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
			jobworkVo.setStatus("progress");
			jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_FARMO_TYPE);
			jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_ANTRI_AND_LARRING);
			jobworkVo.setJobworkNo(jobworkService.findMaxJobworkNo(
					Long.parseLong(session.getAttribute("companyId").toString()), 
					Long.parseLong(session.getAttribute("branchId").toString()), 
					Long.parseLong(session.getAttribute("userId").toString()), Constant.JOBWORK, "JBW"));
			
			jobworkVo.setPrefix(prefixService.findByPrefixTypeAndBranchId(Constant.JOBWORK, Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
		} else {
			jobworkVo2 = jobworkService.findByJobworkIdAndBranchId(jobworkVo.getJobworkId(), Long.parseLong(session.getAttribute("branchId").toString()));
			
			compareJobworks(jobworkVo, jobworkVo2);
		}
		
		jobworkVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		jobworkVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		
		if(allRequestParams.get("process").equals(Constant.JOBWORK_PROCESS_ANTRI_AND_LARRING)) {
			
			if(jobworkVo.getJobworkInputVos() != null) {
				jobworkVo.getJobworkInputVos().forEach(j->j.setJobworkVo(jobworkVo));
			}
			
			if(jobworkVo.getJobworkOutputVos() != null) {
				jobworkVo.getJobworkOutputVos().forEach(j -> j.setJobworkVo(jobworkVo));
			}
			
			jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_FARMO_TYPE);
			jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_ANTRI_AND_LARRING);
			
		} else if(allRequestParams.get("process").equals(Constant.JOBWORK_PROCESS_FARMO_TYPE)) {
			
			if(jobworkVo.getJobworkFarmoTypeVos() != null) {
				jobworkVo.getJobworkFarmoTypeVos().forEach(j-> {
					j.setJobworkVo(jobworkVo);
					j.setProcess(Constant.JOBWORK_PROCESS_FARMO_TYPE);
				});			
			}
			
			if(jobworkVo.getJobworkFarmoTypeParticularVos() != null) {
				jobworkVo.getJobworkFarmoTypeParticularVos().removeIf(rm -> rm.getFarmoParticularVo()==null);
				jobworkVo.getJobworkFarmoTypeParticularVos().forEach(p->{
					p.setJobworkVo(jobworkVo);
					p.setProcess(Constant.JOBWORK_PROCESS_FARMO_TYPE);
				});
			}
			
			jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_STITCHING);
			jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_FARMO_TYPE);
			
		} else if(allRequestParams.get("process").equals(Constant.JOBWORK_PROCESS_STITCHING)) {
			
			if(jobworkVo.getJobworkProcessVos() != null) {
				jobworkVo.getJobworkProcessVos().removeIf(j -> j.getProductionProcessVo() == null);
				jobworkVo.getJobworkProcessVos().forEach(j -> {
					j.setJobworkVo(jobworkVo);
					j.setProcess(Constant.JOBWORK_PROCESS_STITCHING);
				});
			}
			
			if(allRequestParams.get("stitchingQC") == null) {
				jobworkVo.setStitchingQC("pending");
				jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_STITCHING);
				jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_FARMO_TYPE);
			} else {
				jobworkVo.setStitchingQC("complete");
				jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1);
				jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_STITCHING);
			}
			
			if(allRequestParams.get("deleteJobworkProcessVos") != null && !allRequestParams.get("deleteJobworkProcessVos").equals("")) {
				jobworkService.deleteJobworkProcessByIdIn(allRequestParams.get("deleteJobworkProcessVos"));
			}
			
		} else if(allRequestParams.get("process").equals(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1)) {
			
			if(jobworkVo.getJobworkFarmoTypeVos() != null) {
				jobworkVo.getJobworkFarmoTypeVos().forEach(j-> {
					j.setJobworkVo(jobworkVo);
					j.setProcess(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1);
				});			
			}
			
			if(jobworkVo.getJobworkFarmoTypeParticularVos() != null) {
				jobworkVo.getJobworkFarmoTypeParticularVos().removeIf(rm -> rm.getFarmoParticularVo()==null);
				jobworkVo.getJobworkFarmoTypeParticularVos().forEach(p->{
					p.setJobworkVo(jobworkVo);
					p.setProcess(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1);
				});
			}
			
			jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_BAL_GAJ);
			jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1);
			
		} else if(allRequestParams.get("process").equals(Constant.JOBWORK_PROCESS_BAL_GAJ)) {
			
			if(jobworkVo.getBalGajLocation().equals("in_house")) {
				if(jobworkVo.getJobworkProcessVos() != null) {
					jobworkVo.getJobworkProcessVos().removeIf(j -> j.getProductionProcessVo() == null);
					jobworkVo.getJobworkProcessVos().forEach(j -> {
						j.setJobworkVo(jobworkVo);
						j.setProcess(Constant.JOBWORK_PROCESS_BAL_GAJ);
					});
				}
				
				if(allRequestParams.get("balGajQC") == null) {
					jobworkVo.setBalGajQC("pending");
					jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_BAL_GAJ);
					jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1);
				} else {
					jobworkVo.setBalGajQC("complete");
					jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_WASHING);
					jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_BAL_GAJ);
				}
			} else {
				if(jobworkVo.getBalGajQC().equals("pending")){
					jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_BAL_GAJ);
					jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1);
				} else {
					jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_WASHING);
					jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_BAL_GAJ);
				}
			}
			
			
		} else if(allRequestParams.get("process").equals(Constant.JOBWORK_PROCESS_WASHING)) {
			
			if(jobworkVo.getWashingReceiveStatus().equals("pending") || jobworkVo.getWashingReceiveStatus().equals("sample")) {
				jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_WASHING);
				jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_BAL_GAJ);
			} else {
				jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_MEASUREMENT_QC2);
				jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_WASHING);
			}
			
			if(jobworkVo.getWashingReceiveStatus().equals("pending")) {
				
				if(allRequestParams.get("washingSample") == null) {
					jobworkVo.setWashingSample(false);
				} else {
					jobworkVo.setWashingSample(true);
				}
			}
			
		} else if(allRequestParams.get("process").equals(Constant.JOBWORK_PROCESS_MEASUREMENT_QC2)) {
			if(jobworkVo.getJobworkFarmoTypeVos() != null) {
				jobworkVo.getJobworkFarmoTypeVos().forEach(j-> {
					j.setJobworkVo(jobworkVo);
					j.setProcess(Constant.JOBWORK_PROCESS_MEASUREMENT_QC2);
				});			
			}
			if(jobworkVo.getJobworkFarmoTypeParticularVos() != null) {
				jobworkVo.getJobworkFarmoTypeParticularVos().removeIf(rm -> rm.getFarmoParticularVo()==null);
				jobworkVo.getJobworkFarmoTypeParticularVos().forEach(p->{
					p.setJobworkVo(jobworkVo);
					p.setProcess(Constant.JOBWORK_PROCESS_MEASUREMENT_QC2);
				});
			}
			jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_PRESSING);
			jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_MEASUREMENT_QC2);
			
		} else if(allRequestParams.get("process").equals(Constant.JOBWORK_PROCESS_PRESSING)) {
			if(jobworkVo.getPressingReceiveStatus().equals("pending")){
				jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_PRESSING);
				jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_MEASUREMENT_QC2);
			} else {
				jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_LABELING_FINISHING);
				jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_PRESSING);
			}
		} else if(allRequestParams.get("process").equals(Constant.JOBWORK_PROCESS_LABELING_FINISHING)) {
			
			if(jobworkVo.getJobworkProcessVos() != null) {
				jobworkVo.getJobworkProcessVos().removeIf(j -> j.getProductionProcessVo() == null);
				jobworkVo.getJobworkProcessVos().forEach(j -> {
					j.setJobworkVo(jobworkVo);
					j.setProcess(Constant.JOBWORK_PROCESS_LABELING_FINISHING);
				});
			}
			
			if(allRequestParams.get("labelingFinishingQC") == null) {
				jobworkVo.setLabelingFinishingQC("pending");
				jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_LABELING_FINISHING);
				jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_PRESSING);
			} else {
				jobworkVo.setLabelingFinishingQC("complete");
				jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_STORE);
				jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_LABELING_FINISHING);
			}
			
			if(allRequestParams.get("deleteJobworkProcessVos") != null && !allRequestParams.get("deleteJobworkProcessVos").equals("")) {
				jobworkService.deleteJobworkProcessByIdIn(allRequestParams.get("deleteJobworkProcessVos"));
			}
			
		} else if(allRequestParams.get("process").equals(Constant.JOBWORK_PROCESS_STORE)){
			
			if(jobworkVo.getStoreStatus().equals("pending")) {
				if(jobworkVo.getJobworkOutputVos() != null) {
					jobworkVo.getJobworkOutputVos().forEach(j -> j.setJobworkVo(jobworkVo));
				}
			} else {
				jobworkVo.setStatus("complete");
				if(jobworkVo.getJobworkPackageVos() != null) {
					jobworkVo.getJobworkPackageVos().forEach(j -> j.setJobworkVo(jobworkVo));
				}
				
				if(jobworkVo.getJobworkOutputVos() != null) {
					jobworkVo.getJobworkOutputVos().forEach(j -> j.setJobworkVo(jobworkVo));
				}
				
			}
			
			jobworkVo.setCurrentProcess(Constant.JOBWORK_PROCESS_STORE);
			jobworkVo.setPreviousProcess(Constant.JOBWORK_PROCESS_STORE);
		}
		
		jobworkService.save(jobworkVo);
		if(allRequestParams.get("process").equals(Constant.JOBWORK_PROCESS_ANTRI_AND_LARRING)) {
			jobworkService.saveStockTransaction(jobworkVo, session.getAttribute("financialYear").toString(), Constant.JOBWORK_PROCESS_ANTRI_AND_LARRING);
		}
		else if(allRequestParams.get("process").equals(Constant.JOBWORK_PROCESS_STORE)) {
			if(jobworkVo.getStoreStatus().equals("package")) {
				jobworkService.saveStockTransaction(jobworkVo, session.getAttribute("financialYear").toString(), Constant.JOBWORK_PROCESS_STORE);
			}
		}
		
		
		return "redirect:/jobwork/"+jobworkVo.getJobworkId();
	}
	
	@RequestMapping("{id}")
	public ModelAndView jobworkView(@PathVariable("id") long jobworkId, @RequestParam(value="previousProcess", defaultValue="none") String previousProcess, HttpSession session) {
		ModelAndView view = new ModelAndView();
		
		JobworkVo jobworkVo = jobworkService.findByJobworkIdAndBranchId(jobworkId, Long.parseLong(session.getAttribute("branchId").toString()));
		boolean isEditable = false;
		System.out.println(previousProcess);
		System.out.println(jobworkVo.getCurrentProcess());
		
		List listA = new ArrayList();
		listA.add(Constant.JOBWORK_PROCESS_ANTRI_AND_LARRING);
		listA.add(Constant.JOBWORK_PROCESS_FARMO_TYPE);
		listA.add(Constant.JOBWORK_PROCESS_STITCHING);
		listA.add(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1);
		listA.add(Constant.JOBWORK_PROCESS_BAL_GAJ);
		listA.add(Constant.JOBWORK_PROCESS_WASHING);
		listA.add(Constant.JOBWORK_PROCESS_MEASUREMENT_QC2);
		listA.add(Constant.JOBWORK_PROCESS_PRESSING);
		listA.add(Constant.JOBWORK_PROCESS_LABELING_FINISHING);
		listA.add(Constant.JOBWORK_PROCESS_STORE);
		
		
		System.err.println(listA.indexOf(previousProcess)+"---PP-----");
		System.err.println(listA.indexOf(jobworkVo.getCurrentProcess())+"----CP----");
		if(listA.indexOf(previousProcess)>listA.indexOf(jobworkVo.getCurrentProcess())){
			previousProcess = jobworkVo.getCurrentProcess();
		}
		
		if(previousProcess.equals(Constant.JOBWORK_PROCESS_ANTRI_AND_LARRING) || jobworkVo.getCurrentProcess().equals(Constant.JOBWORK_PROCESS_ANTRI_AND_LARRING)) {
			
			view.setViewName("jobwork/jobwork-step1-antri-larring");
			
			view.addObject("productVos", productService.getProductWithVariation(Long.parseLong(session.getAttribute("companyId").toString()),false));
			view.addObject("productVariantVos", productService.findProductVariantByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
			
			if(Constant.JOBWORK_PROCESS_ANTRI_AND_LARRING.equals(jobworkVo.getPreviousProcess()) ||
					Constant.JOBWORK_PROCESS_ANTRI_AND_LARRING.equals(jobworkVo.getCurrentProcess())) {
				isEditable = true;
			}
			
		} else if(previousProcess.equals(Constant.JOBWORK_PROCESS_FARMO_TYPE) || jobworkVo.getCurrentProcess().equals(Constant.JOBWORK_PROCESS_FARMO_TYPE)) {
			
			view.setViewName("jobwork/jobwork-step2-farmo-type");
			
			if(Constant.JOBWORK_PROCESS_FARMO_TYPE.equals(jobworkVo.getPreviousProcess()) ||
					Constant.JOBWORK_PROCESS_FARMO_TYPE.equals(jobworkVo.getCurrentProcess())) {
				
				if(jobworkVo.getStitchingQC() == null || jobworkVo.getStitchingQC().equals("pending")) {
					isEditable = true;
				}
			}
			
			view.addObject("farmoTypeVos",farmoTypeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		} else if(previousProcess.equals(Constant.JOBWORK_PROCESS_STITCHING) || jobworkVo.getCurrentProcess().equals(Constant.JOBWORK_PROCESS_STITCHING)) {
			
			view.setViewName("jobwork/jobwork-step3-stitching");
			
			view.addObject("productionProcessVos", productionProcessService.findByCompanyIdAndProductionTypeId(Long.parseLong(session.getAttribute("companyId").toString()),1));
			
			if(Constant.JOBWORK_PROCESS_STITCHING.equals(jobworkVo.getPreviousProcess()) ||
					Constant.JOBWORK_PROCESS_STITCHING.equals(jobworkVo.getCurrentProcess())) {
				if(jobworkVo.getStitchingQC() == null || jobworkVo.getStitchingQC().equals("pending")) {
					isEditable = true;
				}
			}
			
			jobworkVo.getJobworkProcessVos().removeIf(j -> !j.getProcess().equals(Constant.JOBWORK_PROCESS_STITCHING));
			
		} else if(previousProcess.equals(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1) || jobworkVo.getCurrentProcess().equals(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1)) {
			
			view.setViewName("jobwork/jobwork-step4-measurment-qc1");
			
			if(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1.equals(jobworkVo.getPreviousProcess()) ||
					Constant.JOBWORK_PROCESS_MEASUREMENT_QC1.equals(jobworkVo.getCurrentProcess())) {
				isEditable = true;
			}
			
			if(jobworkVo.getJobworkFarmoTypeVos().stream().filter(p->p.getProcess().equals(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1)).collect(Collectors.toList()).size() == 0) {
				JobworkFarmoTypeVo jobworkFarmoTypeVo= new JobworkFarmoTypeVo();
				jobworkFarmoTypeVo.setProcess(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1);
				jobworkVo.getJobworkFarmoTypeVos().add(jobworkFarmoTypeVo);
			}
			if(jobworkVo.getJobworkFarmoTypeParticularVos().stream().filter(p->p.getProcess().equals(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1)).collect(Collectors.toList()).size() == 0) {
				for(int i=0; i<jobworkVo.getJobworkFarmoTypeParticularVos().stream().filter(dd->dd.getProcess().equals(Constant.JOBWORK_PROCESS_FARMO_TYPE)).collect(Collectors.toList()).size();i++){
					JobworkFarmoTypeParticularVo jobworkFarmoTypeParticularVo= new JobworkFarmoTypeParticularVo();
					jobworkFarmoTypeParticularVo.setProcess(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1);
					jobworkVo.getJobworkFarmoTypeParticularVos().add(jobworkFarmoTypeParticularVo);
				}
			}
			
			view.addObject("farmoTypeVos",farmoTypeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		} else if(previousProcess.equals(Constant.JOBWORK_PROCESS_BAL_GAJ) || jobworkVo.getCurrentProcess().equals(Constant.JOBWORK_PROCESS_BAL_GAJ)) {
			
			view.setViewName("jobwork/jobwork-step5-bal-gaj");
			
			if(Constant.JOBWORK_PROCESS_BAL_GAJ.equals(jobworkVo.getPreviousProcess()) ||
					Constant.JOBWORK_PROCESS_BAL_GAJ.equals(jobworkVo.getCurrentProcess())) {
				if(jobworkVo.getBalGajQC() == null || jobworkVo.getBalGajQC().equals("pending")) {
					isEditable = true;
				}
			}
			
			if(jobworkVo.getBalGajLocation() == null || jobworkVo.getBalGajLocation().equals("out_source")) {
				view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
				
				if(jobworkVo.getBalGajContactVo() != null) {
					jobworkVo.getBalGajContactVo().getContactAddressVos().removeIf( pp -> pp.getIsDefault() == 0);
					
					jobworkVo.getBalGajContactVo().getContactAddressVos().forEach(p -> {
						p.setCountriesName(locationService.findCountriesNameByCountriesCode(p.getCountriesCode()));
						p.setStateName(locationService.findStateNameByStateCode(p.getStateCode()));
						p.setCityName(locationService.findCityNameByCityCode(p.getCityCode()));
					});
				}
			}
			jobworkVo.getJobworkProcessVos().removeIf(j -> !j.getProcess().equals(Constant.JOBWORK_PROCESS_BAL_GAJ));
			
			if(jobworkVo.getBalGajLocation() == null || jobworkVo.getBalGajLocation().equals("in_house")) {
				
				List<ProductionProcessVo> productionProcessVos = productionProcessService.findByCompanyIdAndProductionTypeId(Long.parseLong(session.getAttribute("companyId").toString()),2);
				view.addObject("productionProcessVos", productionProcessVos);
			}
			
			
		} else if(previousProcess.equals(Constant.JOBWORK_PROCESS_WASHING) || jobworkVo.getCurrentProcess().equals(Constant.JOBWORK_PROCESS_WASHING)) {
			
			view.setViewName("jobwork/jobwork-step6-washing");
			
			if(Constant.JOBWORK_PROCESS_WASHING.equals(jobworkVo.getPreviousProcess()) ||
					Constant.JOBWORK_PROCESS_WASHING.equals(jobworkVo.getCurrentProcess())) {
				if(jobworkVo.getWashingReceiveStatus() == null || !jobworkVo.getWashingReceiveStatus().equals("complete"))
				isEditable = true;
			}
			
			view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
			
			if(jobworkVo.getWashingContactVo() != null) {
				jobworkVo.getWashingContactVo().getContactAddressVos().removeIf( pp -> pp.getIsDefault() == 0);
				
				jobworkVo.getWashingContactVo().getContactAddressVos().forEach(p -> {
					p.setCountriesName(locationService.findCountriesNameByCountriesCode(p.getCountriesCode()));
					p.setStateName(locationService.findStateNameByStateCode(p.getStateCode()));
					p.setCityName(locationService.findCityNameByCityCode(p.getCityCode()));
				});
			}
		} else if(previousProcess.equals(Constant.JOBWORK_PROCESS_MEASUREMENT_QC2) || jobworkVo.getCurrentProcess().equals(Constant.JOBWORK_PROCESS_MEASUREMENT_QC2)) {
			
			view.setViewName("jobwork/jobwork-step7-measurment-qc2");
			if(Constant.JOBWORK_PROCESS_MEASUREMENT_QC2.equals(jobworkVo.getPreviousProcess()) ||
					Constant.JOBWORK_PROCESS_MEASUREMENT_QC2.equals(jobworkVo.getCurrentProcess())) {
				isEditable = true;
			}
			
			if(jobworkVo.getJobworkFarmoTypeVos().stream().filter(p->p.getProcess().equals(Constant.JOBWORK_PROCESS_MEASUREMENT_QC2)).collect(Collectors.toList()).size() == 0) {
				JobworkFarmoTypeVo jobworkFarmoTypeVo= new JobworkFarmoTypeVo();
				jobworkFarmoTypeVo.setProcess(Constant.JOBWORK_PROCESS_MEASUREMENT_QC2);
				jobworkVo.getJobworkFarmoTypeVos().add(jobworkFarmoTypeVo);
			}
			if(jobworkVo.getJobworkFarmoTypeParticularVos().stream().filter(p->p.getProcess().equals(Constant.JOBWORK_PROCESS_MEASUREMENT_QC2)).collect(Collectors.toList()).size() == 0) {
				for(int i=0; i<jobworkVo.getJobworkFarmoTypeParticularVos().stream().filter(dd->dd.getProcess().equals(Constant.JOBWORK_PROCESS_MEASUREMENT_QC1)).collect(Collectors.toList()).size();i++){
					JobworkFarmoTypeParticularVo jobworkFarmoTypeParticularVo= new JobworkFarmoTypeParticularVo();
					jobworkFarmoTypeParticularVo.setProcess(Constant.JOBWORK_PROCESS_MEASUREMENT_QC2);
					jobworkVo.getJobworkFarmoTypeParticularVos().add(jobworkFarmoTypeParticularVo);
				}
			}
			
			view.addObject("farmoTypeVos",farmoTypeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		} else if(previousProcess.equals(Constant.JOBWORK_PROCESS_PRESSING) || jobworkVo.getCurrentProcess().equals(Constant.JOBWORK_PROCESS_PRESSING)) {
			
			view.setViewName("jobwork/jobwork-step8-pressing");
			
			if(Constant.JOBWORK_PROCESS_PRESSING.equals(jobworkVo.getPreviousProcess()) ||
					Constant.JOBWORK_PROCESS_PRESSING.equals(jobworkVo.getCurrentProcess())) {
				if(jobworkVo.getPressingReceiveStatus() == null || jobworkVo.getPressingReceiveStatus().equals("pending")){
					isEditable = true;
				}
				
			}
			
			if(jobworkVo.getPressingContactVo() != null) {
				jobworkVo.getPressingContactVo().getContactAddressVos().removeIf( pp -> pp.getIsDefault() == 0);
				
				jobworkVo.getPressingContactVo().getContactAddressVos().forEach(p -> {
					p.setCountriesName(locationService.findCountriesNameByCountriesCode(p.getCountriesCode()));
					p.setStateName(locationService.findStateNameByStateCode(p.getStateCode()));
					p.setCityName(locationService.findCityNameByCityCode(p.getCityCode()));
				});
			}
			
			view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
		} else if(previousProcess.equals(Constant.JOBWORK_PROCESS_LABELING_FINISHING) || jobworkVo.getCurrentProcess().equals(Constant.JOBWORK_PROCESS_LABELING_FINISHING)) {
			
			view.setViewName("jobwork/jobwork-step9-labeling-finishing");
			
			view.addObject("productionProcessVos", productionProcessService.findByCompanyIdAndProductionTypeId(Long.parseLong(session.getAttribute("companyId").toString()),3));
			
			if(Constant.JOBWORK_PROCESS_LABELING_FINISHING.equals(jobworkVo.getPreviousProcess()) ||
					Constant.JOBWORK_PROCESS_LABELING_FINISHING.equals(jobworkVo.getCurrentProcess())) {
				if(jobworkVo.getLabelingFinishingQC() == null || jobworkVo.getLabelingFinishingQC().equals("pending")) {
					isEditable = true;
				}
			}
			jobworkVo.getJobworkProcessVos().removeIf(j -> !j.getProcess().equals(Constant.JOBWORK_PROCESS_LABELING_FINISHING));
		} else if(previousProcess.equals(Constant.JOBWORK_PROCESS_STORE) || jobworkVo.getCurrentProcess().equals(Constant.JOBWORK_PROCESS_STORE)) {
			
			view.setViewName("jobwork/jobwork-step10-store");
			if(jobworkVo.getStoreStatus() == null || !jobworkVo.getStoreStatus().equals("complete")) {
				isEditable = true;
			}
			view.addObject("productVariantVos", productService.findProductVariantByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		}
		
		view.addObject("isEditable", isEditable);
		view.addObject("jobworkVo", jobworkVo);
		view.addObject("employeeVos", employeeService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		return view;
	}
	
	@PostMapping("package/{id}/{productId}/json")
	@ResponseBody
	public Map<String, Object> productPackageJSON(@PathVariable("id") long jobworkId,@PathVariable("productId") long productId, HttpSession session) {
		ProductVo productVo = productService.findByParantId(productId, Long.parseLong(session.getAttribute("companyId").toString()));
		//productVo.getProductVariantVos().forEach(p -> p.setProductVo(null));
		//productVo.getProductVo().getProductVariantVos().forEach(p -> p.setProductVo(null));
		//productVo.getProductVo().setProductAttributeVos(null);
		//productVo.setProductAttributeVos(null);
		Map<String, Object> response = new HashMap<String, Object>();
		
		if(productVo != null) {
		
			List<JobworkPackageVo> jobworkPackageVos = jobworkService.findJobworkPackageByJobworkId(jobworkId);
			
			jobworkPackageVos.forEach(j -> {
				j.setJobworkVo(null);
				j.getProductVariantVo().setProductVo(null);
			});
			response.put("productVo", productVo);
			response.put("jobworkPackageVos", jobworkPackageVos);
			((ProductVo)response.get("productVo")).getProductVariantVos().forEach(p->p.setProductVo(null));
			((ProductVo)response.get("productVo")).getProductVo().getProductVariantVos().forEach(p->p.setProductVo(null));
			((ProductVo)response.get("productVo")).getProductVo().setProductAttributeVos(null);
			
		} else {
			response.put("error", "no packages created");
		}
		return response;
	}
	
	public JobworkVo compareJobworks(JobworkVo jspJobworkVo,JobworkVo dbJobworkVo) {
		if(dbJobworkVo.getJobworkDate() != null && jspJobworkVo.getJobworkDate() == null){  // Jobwork Date
			jspJobworkVo.setJobworkDate(dbJobworkVo.getJobworkDate());
		} if(dbJobworkVo.getDesignNo() != null && jspJobworkVo.getDesignNo() == null) { // Design Number
			jspJobworkVo.setDesignNo(dbJobworkVo.getDesignNo());
		} if(dbJobworkVo.getStatus() != null && jspJobworkVo.getStatus() == null) { // Status
			jspJobworkVo.setStatus(dbJobworkVo.getStatus());
		} if(dbJobworkVo.getPrefix() != null && jspJobworkVo.getPrefix() == null) { // Prefix
			jspJobworkVo.setPrefix(dbJobworkVo.getPrefix());
		} if(dbJobworkVo.getJobworkNo() != 0L && jspJobworkVo.getJobworkNo() == 0L ) { // Jobwork Number
			jspJobworkVo.setJobworkNo(dbJobworkVo.getJobworkNo());
		} if(dbJobworkVo.getAntriLarringBy() != null && jspJobworkVo.getAntriLarringBy() == null ) { // AntriLarring By
			jspJobworkVo.setAntriLarringBy(dbJobworkVo.getAntriLarringBy());
		} if(dbJobworkVo.getAntriLarringDate() != null && jspJobworkVo.getAntriLarringDate() == null) { // AntriLarring Date
			jspJobworkVo.setAntriLarringDate(dbJobworkVo.getAntriLarringDate());
		} if(dbJobworkVo.getAntriLarringApproveBy() != null && jspJobworkVo.getAntriLarringApproveBy() == null) { // AntriLarring Approve Date
			jspJobworkVo.setAntriLarringApproveBy(dbJobworkVo.getAntriLarringApproveBy());
		} if(dbJobworkVo.getAntriLarringApproveDate() != null && jspJobworkVo.getAntriLarringApproveDate() == null) { // AntriLarring Approve Date
			jspJobworkVo.setAntriLarringApproveDate(dbJobworkVo.getAntriLarringApproveDate());
		} if(dbJobworkVo.getJobworkFarmoTypeVos() != null && jspJobworkVo.getJobworkFarmoTypeVos() == null) { // Jobwork Farmo Type
			jspJobworkVo.setJobworkFarmoTypeVos(dbJobworkVo.getJobworkFarmoTypeVos());
		} if(dbJobworkVo.getJobworkFarmoTypeParticularVos() != null && jspJobworkVo.getJobworkFarmoTypeParticularVos() == null) { // Jobwork Farmo Type Particular List
			jspJobworkVo.setJobworkFarmoTypeParticularVos(dbJobworkVo.getJobworkFarmoTypeParticularVos());
		} if(dbJobworkVo.getDrawingBy() != null && jspJobworkVo.getDrawingBy() == null) { //  Drawing By
			jspJobworkVo.setDrawingBy(dbJobworkVo.getDrawingBy());
		} if(dbJobworkVo.getDrawingDate() != null && jspJobworkVo.getDrawingDate() == null) { // Drawing Date
			jspJobworkVo.setDrawingDate(dbJobworkVo.getDrawingDate());
		} if(dbJobworkVo.getDrawingApproveBy() != null && jspJobworkVo.getDrawingApproveBy() == null) { // Drawing Approve By
			jspJobworkVo.setDrawingApproveBy(dbJobworkVo.getDrawingApproveBy());
		} if(dbJobworkVo.getDrawingApproveDate() != null && jspJobworkVo.getDrawingApproveDate() == null) { // Drawing Approve Date
			jspJobworkVo.setDrawingApproveDate(dbJobworkVo.getDrawingApproveDate());
		} if(dbJobworkVo.getCuttingBy() != null && jspJobworkVo.getCuttingBy() == null) { //  Cutting By
			jspJobworkVo.setCuttingBy(dbJobworkVo.getCuttingBy());
		} if(dbJobworkVo.getCuttingDate() != null && jspJobworkVo.getCuttingDate() == null) { // Cutting Date
			jspJobworkVo.setCuttingDate(dbJobworkVo.getCuttingDate());
		} if(dbJobworkVo.getCuttingApproveBy() != null && jspJobworkVo.getCuttingApproveBy() == null) { // Cutting Approve By
			jspJobworkVo.setCuttingApproveBy(dbJobworkVo.getCuttingApproveBy());
		} if(dbJobworkVo.getCuttingApproveDate() != null && jspJobworkVo.getCuttingApproveDate() == null) { // Cutting Approve Date
			jspJobworkVo.setCuttingApproveDate(dbJobworkVo.getCuttingApproveDate());
		} if(dbJobworkVo.getSortBy() != null && jspJobworkVo.getSortBy() == null) { //  Sort By
			jspJobworkVo.setSortBy(dbJobworkVo.getSortBy());
		} if(dbJobworkVo.getSortDate() != null && jspJobworkVo.getSortDate() == null) { // Sort Date
			jspJobworkVo.setSortDate(dbJobworkVo.getSortDate());
		} if(dbJobworkVo.getSortApproveBy() != null && jspJobworkVo.getSortApproveBy() == null) { // Sort Approve By
			jspJobworkVo.setSortApproveBy(dbJobworkVo.getSortApproveBy());
		} if(dbJobworkVo.getSortApproveDate() != null && jspJobworkVo.getSortApproveDate() == null) { // Sort Approve Date
			jspJobworkVo.setSortApproveDate(dbJobworkVo.getSortApproveDate());
		} if(dbJobworkVo.getBalGajLocation() != null && jspJobworkVo.getBalGajLocation() == null) { // BalGaj Location
			jspJobworkVo.setBalGajLocation(dbJobworkVo.getBalGajLocation());
		} if(dbJobworkVo.getBalGajQty() != 0l && jspJobworkVo.getBalGajQty() == 0L) { // BalGaj Qty
			jspJobworkVo.setBalGajQty(dbJobworkVo.getBalGajQty());
		} if(dbJobworkVo.getBalGajHandoverBy() != null && jspJobworkVo.getBalGajHandoverBy() == null) { // BalGaj Handover By
			jspJobworkVo.setBalGajHandoverBy(dbJobworkVo.getBalGajHandoverBy());
		} if(dbJobworkVo.getBalGajHandoverDate() != null && jspJobworkVo.getBalGajHandoverDate() == null) { // BalGaj Handover Date
			jspJobworkVo.setBalGajHandoverDate(dbJobworkVo.getBalGajHandoverDate());
		} if(dbJobworkVo.getBalGajReceivedBy() != null && jspJobworkVo.getBalGajReceivedBy() == null) { // BalGaj Received By
			jspJobworkVo.setBalGajReceivedBy(dbJobworkVo.getBalGajReceivedBy());
		} if(dbJobworkVo.getBalGajReceivedDate() != null && jspJobworkVo.getBalGajReceivedDate() == null) { // BalGaj Received Date
			jspJobworkVo.setBalGajReceivedDate(dbJobworkVo.getBalGajReceivedDate());
		} if(dbJobworkVo.getBalGajReceivedQty() != 0L && jspJobworkVo.getBalGajReceivedQty() == 0L) { // BalGaj Received Qty
			jspJobworkVo.setBalGajReceivedQty(dbJobworkVo.getBalGajReceivedQty());
		} if(dbJobworkVo.getBalGajApproveBy() != null && jspJobworkVo.getBalGajApproveBy() == null) { // BalGaj Approve By
			jspJobworkVo.setBalGajApproveBy(dbJobworkVo.getBalGajApproveBy());
		} if(dbJobworkVo.getBalGajApproveDate() != null && jspJobworkVo.getBalGajApproveDate() == null) { // BalGaj Approve Date
			jspJobworkVo.setBalGajApproveDate(dbJobworkVo.getBalGajApproveDate());
		} if(dbJobworkVo.getBalGajContactVo() != null && jspJobworkVo.getBalGajContactVo() == null) { // Washing Qty
			jspJobworkVo.setBalGajContactVo(dbJobworkVo.getBalGajContactVo());
		} if(dbJobworkVo.getWashingQty() != 0L && jspJobworkVo.getWashingQty() == 0L) { // Washing Qty
			jspJobworkVo.setWashingQty(dbJobworkVo.getWashingQty());
		} if(dbJobworkVo.getWashingContactVo() != null && jspJobworkVo.getWashingContactVo() == null) { // Washing ContactVo
			jspJobworkVo.setWashingContactVo(dbJobworkVo.getWashingContactVo());
		} if(dbJobworkVo.getWashingHandoverBy() != null && jspJobworkVo.getWashingHandoverBy() == null) { // Washing Handover By
			jspJobworkVo.setWashingHandoverBy(dbJobworkVo.getWashingHandoverBy());
		} if(dbJobworkVo.getWashingHandoverDate() != null && jspJobworkVo.getWashingHandoverDate() == null) { // WashingHandover Date
			jspJobworkVo.setWashingHandoverDate(dbJobworkVo.getWashingHandoverDate());
		} if(dbJobworkVo.getWashingReceivedBy() != null && jspJobworkVo.getWashingReceivedBy() == null) { // Washing Received By
			jspJobworkVo.setWashingReceivedBy(dbJobworkVo.getWashingReceivedBy());
		} if(dbJobworkVo.getWashingReceivedDate() != null && jspJobworkVo.getWashingReceivedDate() == null) { // WashingReceived Date
			jspJobworkVo.setWashingReceivedDate(dbJobworkVo.getWashingReceivedDate());
		} if(dbJobworkVo.getWashingReceivedQty() != 0L && jspJobworkVo.getWashingReceivedQty() == 0L) { // Washing Received Qty
			jspJobworkVo.setWashingReceivedQty(dbJobworkVo.getWashingReceivedQty());
		} if(dbJobworkVo.isWashingSample() != false && jspJobworkVo.isWashingSample() == false) { // WashingReceived Sample
			jspJobworkVo.setWashingSample(dbJobworkVo.isWashingSample());
		} if(dbJobworkVo.getWashingSampleReceivedBy() != null && jspJobworkVo.getWashingSampleReceivedBy() == null) { // Washing Sample Received By
			jspJobworkVo.setWashingSampleReceivedBy(dbJobworkVo.getWashingSampleReceivedBy());
		} if(dbJobworkVo.getWashingSampleReceivedDate() != null && jspJobworkVo.getWashingSampleReceivedDate() == null) { // Washing Sample Received Date
			jspJobworkVo.setWashingSampleReceivedDate(dbJobworkVo.getWashingSampleReceivedDate());
		} if(dbJobworkVo.getWashingSampleReceivedQty() != 0L && jspJobworkVo.getWashingSampleReceivedQty() == 0L) { // Washing Sample Received Qty
			jspJobworkVo.setWashingSampleReceivedQty(dbJobworkVo.getWashingSampleReceivedQty());
		} if(dbJobworkVo.getPressingQty() != 0L && jspJobworkVo.getPressingQty() == 0L) { // Pressing Date
			jspJobworkVo.setPressingQty(dbJobworkVo.getPressingQty());
		} if(dbJobworkVo.getPressingContactVo() != null && jspJobworkVo.getPressingContactVo() == null) { // Pressing ContactVo
			jspJobworkVo.setPressingContactVo(dbJobworkVo.getPressingContactVo());
		} if(dbJobworkVo.getPressingHandoverBy() != null && jspJobworkVo.getPressingHandoverBy() == null) { // Pressing Handover By
			jspJobworkVo.setPressingHandoverBy(dbJobworkVo.getPressingHandoverBy());
		} if(dbJobworkVo.getPressingHandoverDate() != null && jspJobworkVo.getPressingHandoverDate() == null) { // Pressing Handover Date
			jspJobworkVo.setPressingHandoverDate(dbJobworkVo.getPressingHandoverDate());
		} if(dbJobworkVo.getPressingReceivedBy() != null && jspJobworkVo.getPressingReceivedBy() == null) { // Pressing Received By
			jspJobworkVo.setPressingReceivedBy(dbJobworkVo.getPressingReceivedBy());
		} if(dbJobworkVo.getPressingReceivedDate() != null && jspJobworkVo.getPressingReceivedDate() == null) { // Pressing Received Date
			jspJobworkVo.setPressingReceivedDate(dbJobworkVo.getPressingReceivedDate());
		} if(dbJobworkVo.getPressingReceivedQty() != 0L && jspJobworkVo.getPressingReceivedQty() == 0L) { // Pressing Received Qty
			jspJobworkVo.setPressingReceivedQty(dbJobworkVo.getPressingReceivedQty());
		} if(dbJobworkVo.getJobworkInputVos() != null && jspJobworkVo.getJobworkInputVos() == null) {
			jspJobworkVo.setJobworkInputVos(dbJobworkVo.getJobworkInputVos());
		} if(dbJobworkVo.getJobworkOutputVos() != null && jspJobworkVo.getJobworkOutputVos() == null) {
			jspJobworkVo.setJobworkOutputVos(dbJobworkVo.getJobworkOutputVos());
		} if(dbJobworkVo.getJobworkProcessVos()!= null && jspJobworkVo.getJobworkProcessVos() == null) {
			jspJobworkVo.setJobworkProcessVos(dbJobworkVo.getJobworkProcessVos());
		} if(dbJobworkVo.getStitchingQC() != null && jspJobworkVo.getStitchingQC() == null) {
			jspJobworkVo.setStitchingQC(dbJobworkVo.getStitchingQC());
		} if(dbJobworkVo.getBalGajQC() != null && jspJobworkVo.getBalGajQC() == null) {
			jspJobworkVo.setBalGajQC(dbJobworkVo.getBalGajQC());
		} if(dbJobworkVo.getWashingReceiveStatus() != null && jspJobworkVo.getWashingReceiveStatus() == null) {
			jspJobworkVo.setWashingReceiveStatus(dbJobworkVo.getWashingReceiveStatus());
		} if(dbJobworkVo.getPressingReceiveStatus() != null && jspJobworkVo.getPressingReceiveStatus() == null) {
			jspJobworkVo.setPressingReceiveStatus(dbJobworkVo.getPressingReceiveStatus());
		} if(dbJobworkVo.getLabelingFinishingQC() != null && jspJobworkVo.getLabelingFinishingQC() == null) {
			jspJobworkVo.setLabelingFinishingQC(dbJobworkVo.getLabelingFinishingQC());
		} if(dbJobworkVo.getProductVo() != null && jspJobworkVo.getProductVo() == null) {
			jspJobworkVo.setProductVo(dbJobworkVo.getProductVo());
		} if(dbJobworkVo.getStoreStatus() != null && jspJobworkVo.getStoreStatus() == null) {
			jspJobworkVo.setStoreStatus(dbJobworkVo.getStoreStatus());
		} if(dbJobworkVo.getJobworkPackageVos() != null && jspJobworkVo.getJobworkPackageVos() == null) {
			jspJobworkVo.setJobworkPackageVos(dbJobworkVo.getJobworkPackageVos());
		}
		
		jspJobworkVo.setBranchId(dbJobworkVo.getBranchId());
		jspJobworkVo.setCompanyId(dbJobworkVo.getCompanyId());
		
		return jspJobworkVo;
	}
}
