package com.croods.bssgroup.controller.materialissue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.service.employee.EmployeeService;
import com.croods.bssgroup.service.materialissue.MaterialIssueService;
import com.croods.bssgroup.service.materialrequest.MaterialRequestService;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.deliverychallan.DeliveryChallanVo;
import com.croods.bssgroup.vo.materialissue.MaterialIssueVo;

@Controller
@RequestMapping("materialissue")
public class MaterialIssueController {

	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	MaterialIssueService materialIssueService;
	
	@Autowired
	MaterialRequestService materialRequestService;
	
	@GetMapping("")
	public ModelAndView materialIsue(HttpSession session) {
		ModelAndView view = new ModelAndView("materialissue/materialissue");
		
		return view;
	}
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<MaterialIssueVo> materialIssueDatatable(@Valid DataTablesInput input,@RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<MaterialIssueVo>  specification =new Specification<MaterialIssueVo>() {
			
			@Override
			public Predicate toPredicate(Root<MaterialIssueVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				query.orderBy(criteriaBuilder.desc(root.get("issueDate")));
				query.orderBy(criteriaBuilder.desc(root.get("materialIssueId")));
				try {
					predicates.add(criteriaBuilder.between(root.get("issueDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<MaterialIssueVo> dataTablesOutput=materialIssueService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> {
			p.setMaterialIssueItemVos(null);
			p.setJobworkVo(null);
			p.getReceivedBy().setEmployeeContactVos(null);
			p.getReceivedBy().setUserFrontVo(null);
			p.setMaterialRequestVo(null);
		});
		return dataTablesOutput;
	}
	
	@GetMapping("new")
	public ModelAndView materialIssueNew(HttpSession session) {
		ModelAndView view = new ModelAndView("materialissue/materialissue-new");
		
		view.addObject("employeeVos", employeeService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		view.addObject("materialRequestVos", materialRequestService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		return view;
	}
	
	@PostMapping("save")
	public String saveMaterialIssue(@RequestParam Map<String,String> allRequestParams, @ModelAttribute("materialIssueVo") MaterialIssueVo materialIssueVo,HttpSession session) {
		
		materialIssueVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		materialIssueVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		materialIssueVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		materialIssueVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		if(materialIssueVo.getMaterialIssueId() == 0) {
			materialIssueVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			materialIssueVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			
			materialIssueVo.setIssueNo(materialIssueService.findMaxIssueNo(Constant.MATERIAL_ISSUE, 
					Long.parseLong(session.getAttribute("companyId").toString()), 
					Long.parseLong(session.getAttribute("branchId").toString()), 
					Long.parseLong(session.getAttribute("userId").toString()), "MIS"));
			
			materialIssueVo.setPrefix(prefixService.findByPrefixTypeAndBranchId(Constant.MATERIAL_ISSUE,
					Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
		}
		
		if(materialIssueVo.getMaterialIssueItemVos() != null) {
			materialIssueVo.getMaterialIssueItemVos().removeIf( rm -> rm.getProductVariantVo() == null);
			materialIssueVo.getMaterialIssueItemVos().forEach( item -> item.setMaterialIssueVo(materialIssueVo));
		}
		
		if(allRequestParams.get("deleteMaterialIssueIds") != null &&
				!allRequestParams.get("deleteMaterialIssueIds").equals("")) {
			String address=allRequestParams.get("deleteMaterialIssueIds").substring(0, allRequestParams.get("deleteMaterialIssueIds").length()-1);
			List<Long> l= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
			
			materialIssueService.deleteMaterialIssueItemIds(l);
		}
		
		materialIssueService.save(materialIssueVo);
		
		materialIssueService.saveStockTransaction(materialIssueVo, session.getAttribute("financialYear").toString());
		
		return "redirect:/materialissue/"+materialIssueVo.getMaterialIssueId();
	}
	
	@GetMapping("{id}")
	public ModelAndView viewMaterialIssue(@PathVariable("id") long materialIssueId, HttpSession session) {
		
		ModelAndView view = new ModelAndView("materialissue/materialissue-view");
		
		MaterialIssueVo materialIssueVo = materialIssueService.findByMaterialIssueIdAndBranchId(materialIssueId, Long.parseLong(session.getAttribute("branchId").toString()));
		
		view.addObject("materialIssueVo", materialIssueVo);
		
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView materialIssueNew(@PathVariable("id") long materialIssueId, HttpSession session) {
		ModelAndView view = new ModelAndView("materialissue/materialissue-edit");
		
		MaterialIssueVo materialIssueVo = materialIssueService.findByMaterialIssueIdAndBranchId(materialIssueId, Long.parseLong(session.getAttribute("branchId").toString()));
		view.addObject("materialIssueVo", materialIssueVo);
		view.addObject("employeeVos", employeeService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		
		return view;
	}
	
	@GetMapping("{id}/delete")
	public String materialIssueDelete(@PathVariable("id") long materialIssueId, HttpSession session) {
		
		materialIssueService.delete(materialIssueId, Long.parseLong(session.getAttribute("branchId").toString()), Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "redirect:/materialissue";
	}
	
	@PostMapping("{id}/return/save")
	public String materialReturn(@PathVariable("id") long materialIssueId, @ModelAttribute("materialIssueVo") MaterialIssueVo materialIssueVo, HttpSession session) {
		
		materialIssueVo.setMaterialIssueId(materialIssueId);
		
		materialIssueVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		materialIssueVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		materialIssueVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		
		materialIssueService.materialReturn(materialIssueVo, session.getAttribute("financialYear").toString());
		
		return "redirect:/materialissue/"+materialIssueVo.getMaterialIssueId();
	}
}
