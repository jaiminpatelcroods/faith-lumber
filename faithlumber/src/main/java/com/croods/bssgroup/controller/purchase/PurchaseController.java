package com.croods.bssgroup.controller.purchase;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.repository.purchase.PurchaseRepository;
import com.croods.bssgroup.service.additionalcharge.AdditionalChargeService;
import com.croods.bssgroup.service.contact.ContactService;
import com.croods.bssgroup.service.deliverychallan.DeliveryChallanService;
import com.croods.bssgroup.service.location.LocationService;
import com.croods.bssgroup.service.paymentterm.PaymentTermService;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.service.product.ProductService;
import com.croods.bssgroup.service.purchase.PurchaseService;
import com.croods.bssgroup.service.purchaserequest.PurchaseRequestService;
import com.croods.bssgroup.service.termsandcondition.TermsAndConditionService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.util.JasperExporter;
import com.croods.bssgroup.util.NumberToWord;
import com.croods.bssgroup.vo.contact.ContactAddressVo;
import com.croods.bssgroup.vo.purchase.PurchaseItemVo;
import com.croods.bssgroup.vo.purchase.PurchaseVo;
import com.croods.bssgroup.vo.purchaserequest.PurchaseRequestItemVo;
import com.croods.bssgroup.vo.purchaserequest.PurchaseRequestVo;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@Controller
@RequestMapping("/purchase/{type}")
public class PurchaseController {

	@Autowired
	ContactService contactService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	PurchaseRepository purchaseRepository;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	PurchaseService purchaseService;
	
	@Autowired
	PaymentTermService paymentTermService;
	
	@Autowired
	LocationService locationService;

	@Autowired
	TermsAndConditionService termsAndConditionService;
	
	@Autowired
	AdditionalChargeService additionalChargeService;
	
	@Autowired
	DeliveryChallanService deliveryChallanService;
	
	@Autowired
	PurchaseRequestService purchaseRequestService;
	
	JasperReport jasperReport;
	JasperPrint jasperPrint;
	OutputStream outputStream;
	HashMap jasperParameter;
	
	@GetMapping("")
	public String purchaseList(HttpSession session, @PathVariable(value="type") String type, Model view) {
		
		view.addAttribute("type",type);
		
		if(type.equals(Constant.PURCHASE_BILL)) {
			view.addAttribute("displayType", "Supplier Bill");
		} else if(type.equals(Constant.PURCHASE_ORDER)) {
			view.addAttribute("displayType", "Purchase Order");
		}else if(type.equals(Constant.PURCHASE_DEBIT_NOTE)) {
			view.addAttribute("displayType", "Debit Note");
		} else {
			return "404";
		}
		
		view.addAttribute("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
		
		return "purchase/purchase";
	}
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<PurchaseVo> purchaseRequestDatatable(@Valid DataTablesInput input, @PathVariable(value="type") String type, @RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<PurchaseVo>  specification =new Specification<PurchaseVo>() {
			
			@Override
			public Predicate toPredicate(Root<PurchaseVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				predicates.add(criteriaBuilder.equal(root.get("type"),type));
				query.orderBy(criteriaBuilder.desc(root.get("purchaseDate")));
				query.orderBy(criteriaBuilder.desc(root.get("purchaseId")));
				
				try {
					predicates.add(criteriaBuilder.between(root.get("purchaseDate"), dateFormat.parse(allRequestParams.get("fromDate")),
							dateFormat.parse(allRequestParams.get("toDate"))));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				if(!allRequestParams.get("contactId").equals("0")) {
	    			predicates.add(criteriaBuilder.equal(root.get("contactVo").get("contactId"), Long.parseLong(allRequestParams.get("contactId"))));
	    		}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<PurchaseVo> dataTablesOutput=purchaseService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> {
			p.setPurchaseItemVos(null);
			p.getContactVo().setContactOtherVos(null);
			p.getContactVo().setContactAddressVos(null);
			p.setContactAgentVo(null);
			p.setContactTransportVo(null);
			p.setPurchaseVo(null);
			p.setPurchaseAdditionalChargeVos(null);
		});
		return dataTablesOutput;
	}
	
	@PostMapping("contact/{contactId}/datatable")
	@ResponseBody
	public DataTablesOutput<PurchaseVo> purchaseByContactDatatable(@Valid DataTablesInput input, @PathVariable(value="contactId") long contactId, @PathVariable(value="type") String type, @RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<PurchaseVo>  specification =new Specification<PurchaseVo>() {
			
			@Override
			public Predicate toPredicate(Root<PurchaseVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				predicates.add(criteriaBuilder.equal(root.get("type"),type));
				predicates.add(criteriaBuilder.equal(root.get("contactVo").get("contactId"), contactId));
				query.orderBy(criteriaBuilder.desc(root.get("purchaseDate")));
				query.orderBy(criteriaBuilder.desc(root.get("purchaseId")));
				
				try {
					predicates.add(criteriaBuilder.between(root.get("purchaseDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<PurchaseVo> dataTablesOutput = purchaseService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> {
			p.setPurchaseItemVos(null);
			p.setContactVo(null);
			p.setContactAgentVo(null);
			p.setContactTransportVo(null);
			p.setPurchaseVo(null);
			p.setPurchaseAdditionalChargeVos(null);
		});
		return dataTablesOutput;
	}
	
	@RequestMapping("/new")
	public String newPurchase(HttpSession session, @PathVariable(value="type")String type, @RequestParam(value = "contactId", defaultValue="0") String contactId, @RequestParam(value = "purchaseRequestId", defaultValue="0") String purchaseRequestId, Model view) {
		
		view.addAttribute("type",type);
		
		String defaultPrefix ="";
		
		if(type.equals(Constant.PURCHASE_BILL)) {
			view.addAttribute("displayType", "Supplier Bill");
			defaultPrefix = "BIL";
		} else if(type.equals(Constant.PURCHASE_ORDER)) {
			view.addAttribute("displayType", "Purchase Order");
			defaultPrefix = "PO";
		}else if(type.equals(Constant.PURCHASE_DEBIT_NOTE)) {
			view.addAttribute("displayType", "Debit Note");
			defaultPrefix = "DBNT";
		} else {
			return "404";
		}
		
		long newPurchaseNo = purchaseService.findMaxPurchaseNo(
				Long.parseLong(session.getAttribute("companyId").toString()),
				Long.parseLong(session.getAttribute("branchId").toString()),
				Long.parseLong(session.getAttribute("userId").toString()), type, "BIL");
		
		view.addAttribute("purchaseNo", newPurchaseNo);
		
		view.addAttribute("prefix",prefixService.findByPrefixTypeAndBranchId(type, Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
		
		view.addAttribute("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
		view.addAttribute("contactAgentVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_AGENT));
		view.addAttribute("contactTransportVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_TRANSPORT));
		view.addAttribute("productVariantVos", productService.findProductVariantByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addAttribute("termsAndConditionVos", termsAndConditionService.findByCompanyIdAndIsDefaultAndModules(Long.parseLong(session.getAttribute("companyId").toString()), 1, type));
		view.addAttribute("paymentTermVos", paymentTermService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		
		if(!purchaseRequestId.equals("0")) {
			List<PurchaseRequestItemVo> purchaseRequestItemVos = purchaseRequestService.findItemByPurchaseRequestId(Long.parseLong(purchaseRequestId));
			
			
			if(purchaseRequestItemVos != null && purchaseRequestItemVos.size() != 0) {
				List<PurchaseItemVo> purchaseItemVos = new ArrayList<PurchaseItemVo>();
				
				for (PurchaseRequestItemVo purchaseRequestItemVo : purchaseRequestItemVos) {
					PurchaseItemVo purchaseItemVo = new PurchaseItemVo();
					
					purchaseItemVo.setDiscountType("percentage");
					purchaseItemVo.setMrp(purchaseRequestItemVo.getProductVariantVo().getPurchasePrice());
					purchaseItemVo.setPrice(purchaseRequestItemVo.getProductVariantVo().getPurchasePrice());
					purchaseItemVo.setProductVariantVo(purchaseRequestItemVo.getProductVariantVo());
					purchaseItemVo.setQty(purchaseRequestItemVo.getQty());
					purchaseItemVo.setTaxRate(purchaseRequestItemVo.getProductVariantVo().getProductVo().getPurchaseTaxVo().getTaxRate());
					purchaseItemVo.setTaxVo(purchaseRequestItemVo.getProductVariantVo().getProductVo().getPurchaseTaxVo());
					
					purchaseItemVos.add(purchaseItemVo);
					
				}
				
				view.addAttribute("purchaseItemVos",purchaseItemVos);
				view.addAttribute("purchaseRequestId", purchaseRequestId);
				
				view.addAttribute("purchaseRequestNo", purchaseRequestService.findRequestNoByPurchaseRequestId(Long.parseLong(purchaseRequestId)));
			}
		}
		
		if(!contactId.equals("0")) {
			view.addAttribute("contactId", contactId);
		}
		return "purchase/purchase-new";
	}
	
	@PostMapping("purchaseno/verify")
	@ResponseBody
	public String verifyPurchaseNo(@PathVariable(value="type")String type, @RequestParam Map<String,String> allRequestParams, HttpSession session) {
		boolean isExist = false;
		if(allRequestParams.get("purchaseId") == null) {
			isExist = purchaseService.isExistPurchaesNo(Long.parseLong(session.getAttribute("branchId").toString()), type, allRequestParams.get("prefix"), Long.parseLong(allRequestParams.get("purchaseNo")));
		} else {
			isExist = purchaseService.isExistPurchaesNo(Long.parseLong(session.getAttribute("branchId").toString()), type, allRequestParams.get("prefix"), Long.parseLong(allRequestParams.get("purchaseNo")), Long.parseLong(allRequestParams.get("purchaseId")));
		}
		return "{ \"valid\": "+isExist+" }";
	}
	
	@PostMapping("/save")
	public String savePurchase(@RequestParam Map<String,String> allRequestParams, @PathVariable(value="type") String type, @ModelAttribute("purchaseVo") PurchaseVo purchaseVo,HttpSession session) {
		
		ContactAddressVo contactAddressVo;
		
		purchaseVo.setType(type);
		purchaseVo.setStatus("open");
		purchaseVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		purchaseVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		purchaseVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		purchaseVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		PurchaseVo purchaseVo2 = null;
		
		if(allRequestParams.get("billingAddressId").equals("0") || allRequestParams.get("shippingAddressId").equals("0")) {
			purchaseVo2 = purchaseService.findByPurchaseIdAndBranchId(purchaseVo.getPurchaseId(), purchaseVo.getBranchId());
		}
		
		//--------------Set Billing Address Details ------------------------
		if(!allRequestParams.get("billingAddressId").equals("0")) {
			contactAddressVo = contactService.findAddressByContactAddressId(Long.parseLong(allRequestParams.get("billingAddressId")));
			
			purchaseVo.setBillingAddressLine1(contactAddressVo.getAddressLine1());
			purchaseVo.setBillingAddressLine2(contactAddressVo.getAddressLine2());
			purchaseVo.setBillingCityCode(contactAddressVo.getCityCode());
			purchaseVo.setBillingCompanyName(contactAddressVo.getCompanyName());
			purchaseVo.setBillingCountriesCode(contactAddressVo.getCountriesCode());
			purchaseVo.setBillingPinCode(contactAddressVo.getPinCode());
			purchaseVo.setBillingStateCode(contactAddressVo.getStateCode());
		} else if (purchaseVo2 != null){
			purchaseVo.setBillingAddressLine1(purchaseVo2.getBillingAddressLine1());
			purchaseVo.setBillingAddressLine2(purchaseVo2.getBillingAddressLine2());
			purchaseVo.setBillingCityCode(purchaseVo2.getBillingCityCode());
			purchaseVo.setBillingCompanyName(purchaseVo2.getBillingCompanyName());
			purchaseVo.setBillingCountriesCode(purchaseVo2.getBillingCountriesCode());
			purchaseVo.setBillingPinCode(purchaseVo2.getBillingPinCode());
			purchaseVo.setBillingStateCode(purchaseVo2.getBillingStateCode());
		}
		
		
		//--------------Set Shipping Address Details ------------------------
		if(!allRequestParams.get("shippingAddressId").equals("0")) {
			contactAddressVo = contactService.findAddressByContactAddressId(Long.parseLong(allRequestParams.get("shippingAddressId")));
			
			purchaseVo.setShippingAddressLine1(contactAddressVo.getAddressLine1());
			purchaseVo.setShippingAddressLine2(contactAddressVo.getAddressLine2());
			purchaseVo.setShippingCityCode(contactAddressVo.getCityCode());
			purchaseVo.setShippingCompanyName(contactAddressVo.getCompanyName());
			purchaseVo.setShippingCountriesCode(contactAddressVo.getCountriesCode());
			purchaseVo.setShippingPinCode(contactAddressVo.getPinCode());
			purchaseVo.setShippingStateCode(contactAddressVo.getStateCode());
		} else if (purchaseVo2 != null){
			purchaseVo.setShippingAddressLine1(purchaseVo2.getShippingAddressLine1());
			purchaseVo.setShippingAddressLine2(purchaseVo2.getShippingAddressLine2());
			purchaseVo.setShippingCityCode(purchaseVo2.getShippingCityCode());
			purchaseVo.setShippingCompanyName(purchaseVo2.getShippingCompanyName());
			purchaseVo.setShippingCountriesCode(purchaseVo2.getShippingCountriesCode());
			purchaseVo.setShippingPinCode(purchaseVo2.getShippingPinCode());
			purchaseVo.setShippingStateCode(purchaseVo2.getShippingStateCode());
		}
		
		if(purchaseVo.getPurchaseId() == 0) {
			purchaseVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			purchaseVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			purchaseVo.setPaidAmount(0.0);
		}
		
		if(purchaseVo.getPurchaseAdditionalChargeVos() != null) { 
			purchaseVo.getPurchaseAdditionalChargeVos().removeIf( rm -> rm.getAdditionalChargeVo() == null);
			purchaseVo.getPurchaseAdditionalChargeVos().forEach(item1 -> item1.setPurchaseVo(purchaseVo));
		}
		
		if(purchaseVo.getPurchaseItemVos() != null) {
			purchaseVo.getPurchaseItemVos().removeIf( rm -> rm.getProductVariantVo() == null);
			purchaseVo.getPurchaseItemVos().forEach( item -> item.setPurchaseVo(purchaseVo));
		}
		
		if(allRequestParams.get("deletePurchaseItemIds") != null &&
				!allRequestParams.get("deletePurchaseItemIds").equals("")) {
			String address=allRequestParams.get("deletePurchaseItemIds").substring(0, allRequestParams.get("deletePurchaseItemIds").length()-1);
			List<Long> l= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
			
			purchaseService.deletePurchaseItemByIdIn(l);
		}
		
		if(allRequestParams.get("deleteAdditionalChargeIds") != null &&
				!allRequestParams.get("deleteAdditionalChargeIds").equals("")) {
			
			String address=allRequestParams.get("deleteAdditionalChargeIds").substring(0, allRequestParams.get("deleteAdditionalChargeIds").length()-1);
			List<Long> l= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
			
			purchaseService.deletePurchaseAdditionalChargeByIdIn(l);
		}
		
		try {
			if(!purchaseVo.getTermsAndConditionIds().equals("")) {
				
				if(String.valueOf(purchaseVo.getTermsAndConditionIds().charAt(purchaseVo.getTermsAndConditionIds().length() - 1)).equals(",")) {
					purchaseVo.setTermsAndConditionIds(purchaseVo.getTermsAndConditionIds().substring(0, purchaseVo.getTermsAndConditionIds().length() - 1));
				}
				
			}
		} catch(Exception e) {}
		
		PurchaseVo purchaseVo3 = purchaseService.save(purchaseVo);
		
		if(purchaseVo.getType().equals(Constant.PURCHASE_BILL)) {
			purchaseService.insertPurchaseBillTransaction(purchaseVo3,session.getAttribute("financialYear").toString());
		} else if(purchaseVo.getType().equals(Constant.PURCHASE_DEBIT_NOTE)) {
			purchaseService.insertPurchaseDebitNoteTransaction(purchaseVo3,session.getAttribute("financialYear").toString());
		
		}
		
		if(type.equals(Constant.PURCHASE_ORDER) && purchaseVo.getPurchaseRequestId() != 0) {
			purchaseRequestService.updateStatusAndExpectedDateByPurchaseRequestId("In Progress", purchaseVo.getDeliveryDate(), purchaseVo.getAlterBy(), CurrentDateTime.getCurrentDate(), purchaseVo.getPurchaseRequestId());
		}
		return "redirect:/purchase/"+type+"/"+purchaseVo.getPurchaseId();
	}
	
	@GetMapping("{id}")
	public String purchaseDetails(@PathVariable String type, @PathVariable long id, HttpSession session, Model view) {
		
		view.addAttribute("type",type);
		
		if(type.equals(Constant.PURCHASE_BILL)) {
			view.addAttribute("displayType", "Supplier Bill");
		} else if(type.equals(Constant.PURCHASE_ORDER)) {
			view.addAttribute("displayType", "Purchase Order");
		}else if(type.equals(Constant.PURCHASE_DEBIT_NOTE)) {
			view.addAttribute("displayType", "Debit Note");
		} else {
			return "404";
		}
		
		
		PurchaseVo purchaseVo=purchaseService.findByPurchaseIdAndBranchId(id, Long.parseLong(session.getAttribute("branchId").toString()));
		
		purchaseVo.setShippingCountriesName(locationService.findCountriesNameByCountriesCode(purchaseVo.getShippingCountriesCode()));
		purchaseVo.setShippingStateName(locationService.findStateNameByStateCode(purchaseVo.getShippingStateCode()));
		purchaseVo.setShippingCityName(locationService.findCityNameByCityCode(purchaseVo.getShippingCityCode()));
		
		purchaseVo.setBillingCountriesName(locationService.findCountriesNameByCountriesCode(purchaseVo.getBillingCountriesCode()));
		purchaseVo.setBillingStateName(locationService.findStateNameByStateCode(purchaseVo.getBillingStateCode()));
		purchaseVo.setBillingCityName(locationService.findCityNameByCityCode(purchaseVo.getBillingCityCode()));
		
		if(type.equals(Constant.PURCHASE_ORDER) && purchaseVo.getPurchaseRequestId() != 0) {
			view.addAttribute("purchaseRequestNo", purchaseRequestService.findRequestNoByPurchaseRequestId(purchaseVo.getPurchaseRequestId()));
		}
		
		view.addAttribute("purchaseVo", purchaseVo);
		
		if(!purchaseVo.getTermsAndConditionIds().equals("")) {
			List<Long> termandconditionIds = (List<Long>) Arrays.asList(purchaseVo.getTermsAndConditionIds().split("\\s*,\\s*")).stream().map(Long::parseLong).collect(Collectors.toList());
		
			view.addAttribute("termsAndConditionVos", termsAndConditionService.findByTermsandConditionIdIn(termandconditionIds));
		}
		
		if(type.equals(Constant.PURCHASE_BILL)) {
			
			if(purchaseVo.getDeliveryChallanIds() != null && !purchaseVo.getDeliveryChallanIds().equals("")) {
				List<Long> deliveryChallanIds = Arrays.asList(purchaseVo.getDeliveryChallanIds().split("\\s*,\\s*")).stream().map(Long::parseLong).collect(Collectors.toList());
				
				view.addAttribute("deliveryChallanVos", deliveryChallanService.findByDeliveryChallanIdIn(deliveryChallanIds));
			}
		}
		
		return "purchase/purchase-view";
	}
	
	@GetMapping("{id}/edit")
	public String editPurchase(@PathVariable(value="id") long purchaseId, @PathVariable(value="type") String type, HttpSession session, Model view) {
		
		view.addAttribute("type",type);
		
		if(type.equals(Constant.PURCHASE_BILL)) {
			view.addAttribute("displayType", "Supplier Bill");
		} else if(type.equals(Constant.PURCHASE_ORDER)) {
			view.addAttribute("displayType", "Purchase Order");
		} else if(type.equals(Constant.PURCHASE_DEBIT_NOTE)) {
			view.addAttribute("displayType", "Debit Note");
		} else {
			return "404";
		}
		
		view.addAttribute("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
		view.addAttribute("contactAgentVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_AGENT));
		view.addAttribute("contactTransportVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_TRANSPORT));
		view.addAttribute("productVariantVos", productService.findProductVariantByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		
		view.addAttribute("paymentTermVos", paymentTermService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		
		PurchaseVo purchaseVo=purchaseService.findByPurchaseIdAndBranchId(purchaseId, Long.parseLong(session.getAttribute("branchId").toString()));
		
		purchaseVo.setShippingCountriesName(locationService.findCountriesNameByCountriesCode(purchaseVo.getShippingCountriesCode()));
		purchaseVo.setShippingStateName(locationService.findStateNameByStateCode(purchaseVo.getShippingStateCode()));
		purchaseVo.setShippingCityName(locationService.findCityNameByCityCode(purchaseVo.getShippingCityCode()));
		
		purchaseVo.setBillingCountriesName(locationService.findCountriesNameByCountriesCode(purchaseVo.getBillingCountriesCode()));
		purchaseVo.setBillingStateName(locationService.findStateNameByStateCode(purchaseVo.getBillingStateCode()));
		purchaseVo.setBillingCityName(locationService.findCityNameByCityCode(purchaseVo.getBillingCityCode()));
		
		if(type.equals(Constant.PURCHASE_ORDER) && purchaseVo.getPurchaseRequestId() != 0) {
			view.addAttribute("purchaseRequestNo", purchaseRequestService.findRequestNoByPurchaseRequestId(purchaseVo.getPurchaseRequestId()));
		}
		
		view.addAttribute("purchaseVo", purchaseVo);
		
		if(purchaseVo.getPurchaseAdditionalChargeVos().size() > 0) {
			view.addAttribute("additionalChargeVos", additionalChargeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		}
		
		if(!purchaseVo.getTermsAndConditionIds().equals("")) {
			List<Long> termandconditionIds = (List<Long>) Arrays.asList(purchaseVo.getTermsAndConditionIds().split("\\s*,\\s*")).stream().map(Long::parseLong).collect(Collectors.toList());;
		
			view.addAttribute("termsAndConditionVos", termsAndConditionService.findByTermsandConditionIdIn(termandconditionIds));
		}
		
		return "purchase/purchase-edit";
	}
	
	@PostMapping("{contactId}/pending/json")
	@ResponseBody
	public List<PurchaseVo> purchasePendingListJson(@PathVariable("type") String type, @PathVariable("contactId") long contactId, HttpSession session) throws NumberFormatException, ParseException {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		List<PurchaseVo> purchasevos=purchaseService.findUnpaidPurchaseByTypeAndBranchIdAndContactIdAndPurchaseDateBetween(type, 
				Long.parseLong(session.getAttribute("branchId").toString()),
				contactId,
				dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
				dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()));
		
		purchasevos.forEach(p-> {
			p.setContactVo(null);
			p.setPurchaseItemVos(null);
			p.setPurchaseAdditionalChargeVos(null);
			p.setContactTransportVo(null);
			p.setContactAgentVo(null);
		});
		
		return purchasevos;
	}
	
	@PostMapping("contact/{contactId}/json")
	@ResponseBody
	public List<PurchaseVo> purchaseListByContactJson(@PathVariable("type") String type, @PathVariable("contactId") long contactId, HttpSession session) throws NumberFormatException, ParseException {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		List<PurchaseVo> purchasevos=purchaseService.findByTypeAndBranchIdAndContactId(type, 
				Long.parseLong(session.getAttribute("branchId").toString()),
				contactId,
				dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
				dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()));
		
		purchasevos.forEach(p->p.setContactVo(null));
		purchasevos.forEach(pr->pr.setPurchaseItemVos(null));
		purchasevos.forEach(prc->prc.setPurchaseAdditionalChargeVos(null));
		
		return purchasevos;
	}
	
	@PostMapping("{id}/json")
	@ResponseBody
	public PurchaseVo purchaseDetailsJson(@PathVariable long id, @PathVariable("type") String type, HttpSession session) throws NumberFormatException, ParseException {
		PurchaseVo purchaseVo=purchaseService.findByPurchaseIdAndBranchId(id,Long.parseLong(session.getAttribute("branchId").toString()));
		purchaseVo.getContactVo().getContactAddressVos().forEach(ad->ad.setContactVo(null));
		purchaseVo.setPurchaseVo(null);
		purchaseVo.setPurchaseItemVos(null);
		purchaseVo.setPurchaseAdditionalChargeVos(null);
		purchaseVo.setContactTransportVo(null);
		purchaseVo.setContactAgentVo(null);
		return purchaseVo;
	}
	
	@PostMapping("{id}/item/json")
	@ResponseBody
	public List<PurchaseItemVo> purchaseItemJson(@PathVariable long id, @PathVariable("type") String type, HttpSession session) throws NumberFormatException, ParseException {
		List<PurchaseItemVo> purchaseItemVos = purchaseService.findItemByPurchaseId(id);
		
		purchaseItemVos.forEach(p -> {
			p.setPurchaseVo(null);
			p.getProductVariantVo().getProductVo().setProductVariantVos(null);
			p.getProductVariantVo().getProductVo().setProductAttributeVos(null);
			p.getProductVariantVo().getProductVo().setProductVo(null);
		});
		return purchaseItemVos;
	}
	
	@GetMapping("{id}/pdf")
	public void purchasePDF(@PathVariable("id") long purchaseId, @PathVariable("type") String type, HttpSession session, HttpServletRequest request, HttpServletResponse response) 
	{
		PurchaseVo purchaseVo=purchaseService.findByPurchaseIdAndBranchId(purchaseId, Long.parseLong(session.getAttribute("branchId").toString()));
	    jasperParameter = new HashMap();
		jasperParameter.put("purchase_id",purchaseId);
		jasperParameter.put("contact_id",purchaseVo.getContactVo().getContactId());
		jasperParameter.put("user_front_id",Long.parseLong(session.getAttribute("userId").toString()));
		jasperParameter.put("contact_type",purchaseVo.getContactVo().getType());
		
		jasperParameter.put("path", request.getServletContext().getRealPath("/") + "report" + System.getProperty("file.separator"));
		jasperParameter.put("amount_in_word",new NumberToWord().getNumberToWord(purchaseVo.getTotal()));
		JasperExporter jasperExporter = new JasperExporter(); 
		
		try 	
		{
			if(type.equals(Constant.PURCHASE_BILL)) {
					
				jasperParameter.put("display_title", "Supplier Bill");
				jasperExporter.jasperExporterPDF(jasperParameter,
							request.getServletContext().getRealPath("/") + "report/purchase"
									+ System.getProperty("file.separator") +"/Purchase-1.jrxml","Certificate-"+purchaseId+".pdf", response);					
			} else if (type.equals(Constant.PURCHASE_DEBIT_NOTE)) {
				jasperParameter.put("display_title", "Debit Note");
				jasperExporter.jasperExporterPDF(jasperParameter,
						request.getServletContext().getRealPath("/") + "report/debitnote"
								+ System.getProperty("file.separator") +"/debitnote-1.jrxml","Certificate-"+purchaseId+".pdf", response);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	

	@GetMapping("{id}/delete")
	public String deletePurchase(@PathVariable("id") long purchaseId, @PathVariable("type") String type, HttpSession session, HttpServletRequest request, HttpServletResponse response) 
	{
		purchaseService.delete(purchaseId, Long.parseLong(session.getAttribute("branchId").toString()), Long.parseLong(session.getAttribute("userId").toString()), type, CurrentDateTime.getCurrentDate());
		
		return "redirect:/purchase/"+type;
	}
}
