package com.croods.bssgroup.controller.product;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.support.ConcurrentExecutorAdapter;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.service.brand.BrandService;
import com.croods.bssgroup.service.category.CategoryService;
import com.croods.bssgroup.service.product.ProductService;
import com.croods.bssgroup.service.sales.SalesService;
import com.croods.bssgroup.service.stock.StockTransactionService;
import com.croods.bssgroup.service.tax.TaxService;
import com.croods.bssgroup.service.unitofmeasurement.UnitOfMeasurementService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.product.ProductAttributeVo;
import com.croods.bssgroup.vo.product.ProductDTO;
import com.croods.bssgroup.vo.product.ProductVariantVo;
import com.croods.bssgroup.vo.product.ProductVo;



@Controller
@RequestMapping("product")
public class ProductController {

	@Autowired
	CategoryService categoryService;
	
	@Autowired
	BrandService brandService;
	
	@Autowired
	TaxService taxService;
	
	@Autowired
	UnitOfMeasurementService unitOfMeasurementService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	StockTransactionService stockTransactionService;
	
	JSONArray jsonArray;
	JSONObject jsonObject;
	
	@Autowired
	SalesService salesService;
	
	@GetMapping("")
	public ModelAndView product(HttpSession session) {
		ModelAndView view =new ModelAndView("product/product");
		view.addObject("categoryVos",categoryService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("brandVos",brandService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		return view;
	}
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<ProductVo> productDatatable(@Valid DataTablesInput input,@RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long companyId = Long.parseLong(session.getAttribute("companyId").toString());

		Specification<ProductVo>  specification =new Specification<ProductVo>() {
			
			@Override
			public Predicate toPredicate(Root<ProductVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("companyId"), companyId));
				
				/*if(!allRequestParams.get("category").equals("")) {
					predicates.add(criteriaBuilder.equal(root.get("categoryVo").get("categoryId"), Long.parseLong(allRequestParams.get("category").toString())));
				}
				if(!allRequestParams.get("brand").equals("")) {
					predicates.add(criteriaBuilder.equal(root.get("brandVo").get("brandId"), Long.parseLong(allRequestParams.get("brand").toString())));
				}*/
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<ProductVo> dataTablesOutput=productService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach( x -> {
			x.getProductVariantVos().forEach(y->y.setProductVo(null));
			x.setProductAttributeVos(null);
		});
		
		return dataTablesOutput;
	}
	
	@GetMapping("new")
	public ModelAndView productNew(HttpSession session) {
		ModelAndView view =new ModelAndView("product/product-new");
		
		view.addObject("categoryVos",categoryService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("brandVos",brandService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("taxVos", taxService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("unitOfMeasurementVos", unitOfMeasurementService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		
		return view;
	}
	
	@PostMapping("/save")
	public String saveProduct(HttpSession session,@RequestParam Map<String,String> allRequestParams, @ModelAttribute("product") ProductVo product, BindingResult bindingResult, Model model)
 	{			
		product.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		product.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
    	product.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
    	product.setModifiedOn(CurrentDateTime.getCurrentDate());
    	
		for(int i=0;i<product.getProductVariantVos().size();i++)
		{
			ProductVariantVo productVariantVo=product.getProductVariantVos().get(i);
			productVariantVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
			productVariantVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
			productVariantVo.setProductVo(product);			
		}

		if(product.getHaveVariation() == 1) {
			for(int i=0;i<product.getProductAttributeVos().size();i++)		
			{
				ProductAttributeVo productAttributeVo=product.getProductAttributeVos().get(i);
				productAttributeVo.setProductVo(product);			
			}
		}
		
		if(product.getProductId() == 0) {
			product.setCreatedOn(CurrentDateTime.getCurrentDate());
	    	product.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		}
		productService.saveProduct(product);
		
		
		return  "redirect:/product/"+product.getProductId();
	}
	
	@GetMapping("{id}")
	public ModelAndView productView(@PathVariable("id") long productId, HttpSession session) throws NumberFormatException, ParseException {
		ModelAndView view =new ModelAndView("product/product-view");
		
		ProductVo productVo = productService.findByProductIdAndCompanyId(productId,Long.parseLong(session.getAttribute("companyId").toString()));
		
		double totalQty = 0;
    	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    	
//		totalQty = stockTransactionService.getTotalQty(Long.parseLong(session.getAttribute("branchId").toString()),
//				productId, 
//				dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()), 
//				dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()),
//				session.getAttribute("financialYear").toString());
//	
//		productVo.getProductVariantVos().forEach( v -> {
//			try {
//				v.setQty(stockTransactionService.getVariantQty(
//					Long.parseLong(session.getAttribute("branchId").toString()),
//					v.getProductVariantId(),
//					dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()), 
//					dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()),
//					session.getAttribute("financialYear").toString()));
//			} catch (NumberFormatException | ParseException e) {
//				
//				e.printStackTrace();
//			}
//		});
    	
    	try {
			totalQty = salesService.getTotalAvailableQty(Long.parseLong(session.getAttribute("branchId").toString()),
					productId, 
					dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()), dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()));
		}catch (Exception e) {
			e.printStackTrace();
			totalQty = 0;
		}
		
		System.err.println("CurrentAvailableStock---Main Product--->"+totalQty);
	
		productVo.getProductVariantVos().forEach( v -> {
			try {
				
				System.err.println("Variant Product Id--->"+v.getProductVariantId());
				
				v.setQty(salesService.getVariantQty(
						Long.parseLong(session.getAttribute("branchId").toString()),
						v.getProductVariantId(),
						dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()), 
						dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())
					)
				);
				
				
			} catch (NumberFormatException | ParseException e) {
				e.printStackTrace();
				v.setQty(0);
			}
		});
	
		view.addObject("productVo", productVo);
		view.addObject("totalQty", totalQty);
		return view;
	}
	
	@PostMapping("{id}/json")
	@ResponseBody
	public ProductVo productViewJSON(@PathVariable("id") long productId, HttpSession session) {
		
		ProductVo productVo = productService.findByProductIdAndCompanyId(productId, Long.parseLong(session.getAttribute("companyId").toString()));
		productVo.getProductVariantVos().forEach(p -> p.setProductVo(null));
		productVo.setProductAttributeVos(null);
		
		return productVo;
	}
	
	@PostMapping("variant/{id}/json")
	@ResponseBody
	public ProductVariantVo productVariantJSON(@PathVariable("id") long productVariantId, HttpSession session) {
		
		ProductVariantVo productVariantVo = productService.findProductVariantByIdAndCompanyId(productVariantId, Long.parseLong(session.getAttribute("companyId").toString()));
		productVariantVo.getProductVo().setProductVariantVos(null);
		productVariantVo.getProductVo().setProductAttributeVos(null);
		
		if(productVariantVo.getProductVo().getProductVo() != null) {
			productVariantVo.getProductVo().getProductVo().setProductVariantVos(null);
			productVariantVo.getProductVo().getProductVo().setProductAttributeVos(null);
		}
		
		return productVariantVo;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView productEdit(@PathVariable("id") long productId, HttpSession session) {
		ModelAndView view =new ModelAndView("product/product-edit");
		
		ProductVo productVo = productService.findByProductIdAndCompanyId(productId,Long.parseLong(session.getAttribute("companyId").toString()));
		view.addObject("categoryVos",categoryService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("brandVos",brandService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("taxVos", taxService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("unitOfMeasurementVos", unitOfMeasurementService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("productVo", productVo);
		return view;
	}
	
	@GetMapping("{id}/delete")
	public String productDelete(@PathVariable("id") long productId, HttpSession session) {
		
		productService.deleteProduct(productId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "redirect:/product";
	}
	
	@GetMapping("variant/json")
	@ResponseBody
	public String productVariantMultiSelectJSON(@RequestParam Map<String,String> allRequestParams, HttpSession session) {
		jsonArray = new JSONArray();
		jsonObject = new JSONObject();
		List<ProductVariantVo> productVariantVos = productService.findProductVariantsWithPackage(allRequestParams.get("q"), Long.parseLong(session.getAttribute("companyId").toString()));
		
		productVariantVos.forEach(p-> {
			JSONObject json1 = new JSONObject();
			try{
			json1.put("id", p.getProductVariantId());
			json1.put("text", p.getProductVo().getName()+" "+p.getVariantName());
			}catch(Exception e){}
			jsonArray.put(json1);
		});
		try {
			jsonObject.put("total_count", productVariantVos.size());
			jsonObject.put("incomplete_results",true);
			jsonObject.put("items",jsonArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}
	
	@GetMapping("variant/select/json")
	@ResponseBody
	public String productVariantSelectJSON(@RequestParam Map<String,String> allRequestParams, HttpSession session) {
		jsonArray = new JSONArray();
		jsonObject = new JSONObject();
		List<ProductVariantVo> productVariantVos = productService.findProductVariants(allRequestParams.get("q"), Long.parseLong(session.getAttribute("companyId").toString()));
		// Model Mapper
		
		/*ModelMapper modelMapper = new ModelMapper();
		
		PropertyMap<ProductVariantVo, ProductDTO> propertyMap = new PropertyMap<ProductVariantVo, ProductDTO>() {
			
			@Override
			protected void configure() {
				destination.setText(source.getProductVo().getBrandVo().getBrandName());
				destination.setId(source.getProductVariantId());
			}
		};
		
		Converter<ProductVariantVo, ProductDTO> convertCategory2Name = new Converter<ProductVariantVo, ProductDTO>() {
			
			@Override
			public ProductDTO convert(MappingContext<ProductVariantVo, ProductDTO> context) {
				
				context.getDestination().setText(String.valueOf(context.getSource().getPurchasePrice()));
				context.getDestination().setId(context.getSource().getProductVariantId());
				return context.getDestination();
			}
		};
		//modelMapper.addConverter(convertCategory2Name);
		
		modelMapper.addMappings( new PropertyMap<ProductVariantVo, ProductDTO>() {
			
			@Override
			protected void configure() {
				using(convertCategory2Name).map();
			}
		});
		Converter<ProductVariantVo, ProductDTO> convertCategory2Name12 = context -> context.getSource() == null ? null :
            context.getSource().stream().map(p -> p.).collect(Collectors.toList());
		
		ProductDTO pr =  modelMapper.map(productVariantVos.get(0), ProductDTO.class);
		
		System.out.println(pr.getText());*/
		//End Model Mapper
		
		productVariantVos.forEach(p-> {
			JSONObject json1 = new JSONObject();
			try{
			json1.put("id", p.getProductVariantId());
			json1.put("text", p.getProductVo().getCategoryVo().getCategoryName()+" "+ p.getProductVo().getName()+" "+p.getVariantName());
			}catch(Exception e){}
			jsonArray.put(json1);
		});
		try {
			jsonObject.put("total_count", productVariantVos.size());
			jsonObject.put("incomplete_results",true);
			jsonObject.put("items",jsonArray);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}
	
	@PostMapping("{id}/baleno/json")
	@ResponseBody
	public Map<String, Object> productBalenoJSON(@PathVariable("id") long productVariantId, HttpSession session) throws NumberFormatException, ParseException {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Map<String, Object> response = new HashMap<String, Object>();
		List<Map<String, String>> maps = stockTransactionService.findAllBaleNoByBranchIdAndProductVariantId(Long.parseLong(session.getAttribute("branchId").toString()), 
			productVariantId, dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
			dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()), session.getAttribute("financialYear").toString());
		
		ProductVariantVo productVariantVo = productService.findProductVariantByIdAndCompanyId(productVariantId, Long.parseLong(session.getAttribute("companyId").toString()));
		productVariantVo.getProductVo().setProductVariantVos(null);
		productVariantVo.getProductVo().setProductAttributeVos(null);
		response.put("measurementCode", productVariantVo.getProductVo().getUnitOfMeasurementVo().getMeasurementCode());
		response.put("balenoList", maps);
		return response;
	}
	
	@PostMapping("{id}/{designNo}/baleno/json")
	@ResponseBody
	public List<Map<String, String>> getBalenoByDesignNoJSON(@PathVariable("id") long productVariantId,@PathVariable("designNo") String designNo, HttpSession session) throws NumberFormatException, ParseException {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		List<Map<String, String>> maps = stockTransactionService.findAllBaleNoByBranchIdAndProductVariantIdAndDesignNo(Long.parseLong(session.getAttribute("branchId").toString()), 
			productVariantId, designNo, dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
			dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()), session.getAttribute("financialYear").toString());
		
		return maps;
	}
	
	@PostMapping("itemcode/verify")
	@ResponseBody
	public String isExistItemCode(@RequestParam Map<String, String> allRequestParam, @RequestParam(value="productVariantId",defaultValue="0") String productVariantId, HttpSession session) {
		
		
		String itemCode = allRequestParam.get(allRequestParam.keySet().stream().filter(s -> s.endsWith("itemCode")).collect(Collectors.toSet()).iterator().next());
		System.err.println(itemCode);
		boolean isExist = false;
		if(productVariantId.equals("0")) {
			isExist = productService.isExistItemCode(Long.parseLong(session.getAttribute("companyId").toString()), itemCode);
		} else {
			
			isExist = productService.isExistItemCode(Long.parseLong(session.getAttribute("companyId").toString()),itemCode);
			
		}
		System.out.println(productVariantId +"////"  +isExist);
		return "{ \"valid\": "+isExist+" }";
	}
	
	@PostMapping("variant/barcode/{itemCode}/json")
	@ResponseBody
	public ProductVariantVo productBarcodeJSON(@PathVariable("itemCode") String itemCode, HttpSession session) throws NumberFormatException, ParseException {
		
		Map<String, Object> response = new HashMap<String, Object>();
		
		
		ProductVariantVo productVariantVo = productService.findByItemCodeAndCompanyId(itemCode, Long.parseLong(session.getAttribute("companyId").toString()));
		
		if(productVariantVo == null) {
			return null;
		}
		else {
			productVariantVo.getProductVo().setProductVariantVos(null);
			productVariantVo.getProductVo().setProductAttributeVos(null);
			return productVariantVo;	
		}
		
	}
	
	@PostMapping("{id}/designno/json")
	@ResponseBody
	public Map<String, Object> productDesignNoJSON(@PathVariable("id") long productVariantId, HttpSession session) throws NumberFormatException, ParseException {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Map<String, Object> response = new HashMap<String, Object>();
		List<Map<String, String>> maps = stockTransactionService.findAllDesignNoByBranchIdAndProductVariantId(Long.parseLong(session.getAttribute("branchId").toString()), 
			productVariantId, dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
			dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()), session.getAttribute("financialYear").toString());
		
		response.put("designNoList", maps);
		return response;
	}
	
	@PostMapping("designno/json")
	@ResponseBody
	public List<Map<String, Object>> productDesignNoByIdsJSON(@RequestParam("productVariantIds") String productVariantIds, HttpSession session) throws NumberFormatException, ParseException {
		
		List<Map<String, Object>> response = new ArrayList<Map<String, Object>>();
		
		Map<String, Object> designNo;
		System.out.println("productVariantIds="+ productVariantIds);
		if(!productVariantIds.equals("")) {
			System.out.println("productVariantIds="+ productVariantIds);
			String[] productVariantId = productVariantIds.split(",");
			
			for(int i =0; i < productVariantId.length; i++) {
				System.out.println("productVariantId="+productVariantId[i]);
				DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				
				designNo = new HashMap<String, Object>();
				
				List<Map<String, String>> maps = stockTransactionService.findAllDesignNoByBranchIdAndProductVariantId(Long.parseLong(session.getAttribute("branchId").toString()), 
					Long.parseLong(productVariantId[i]), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
					dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()), session.getAttribute("financialYear").toString());
				
				designNo.put("productVariantId", productVariantId[i]);
				designNo.put("designNoList", maps);
				
				response.add(designNo);
			}
			
		}
		System.out.println("size---"+response.size());
		return response;
	}
}
