package com.croods.bssgroup.controller.productpackage;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.service.product.ProductService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.product.ProductVo;

@Controller
@RequestMapping("productpackage")
public class ProductPackageController {

	@Autowired
	ProductService productService;
	
	@GetMapping("")
	public ModelAndView productPackage(HttpSession session) {
		ModelAndView view =new ModelAndView("productpackage/product-package");
			view.addObject("productVos", productService.getProductWithVariation(Long.parseLong(session.getAttribute("companyId").toString()), true));
 		return view;
	}
	
	@GetMapping("new")
	public ModelAndView productNew(HttpSession session) {
		ModelAndView view =new ModelAndView("productpackage/product-package-new");
			view.addObject("productVos", productService.getProductWithVariation(Long.parseLong(session.getAttribute("companyId").toString()), false));
 		return view;
	}
	
	@PostMapping("/save")
	public String saveProduct(HttpSession session,@RequestParam Map<String,String> allRequestParams, @ModelAttribute("product") ProductVo productVo, BindingResult bindingResult, Model model)
 	{			
		long productId= productService.saveProductPackage(session,allRequestParams,productVo);
		
		return  "redirect:/productpackage/"+productId;
	}
	
	@GetMapping("{id}")
	public ModelAndView productView(@PathVariable("id") long productId, HttpSession session) {
		ModelAndView view =new ModelAndView("productpackage/product-package-view");
		
		ProductVo productVo = productService.findByProductIdAndCompanyId(productId,Long.parseLong(session.getAttribute("companyId").toString()));
		
		view.addObject("productVo", productVo);
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView productEdit(@PathVariable("id") long productId, HttpSession session) {
		ModelAndView view =new ModelAndView("productpackage/product-package-edit");
		ProductVo productVo = productService.findByProductIdAndCompanyId(productId,Long.parseLong(session.getAttribute("companyId").toString()));
		view.addObject("productVo", productVo);
		return view;
	}
	
	@PostMapping("{id}/json")
	@ResponseBody
	public ProductVo productPackageJSON(@PathVariable("id") long productId, HttpSession session) {
		ProductVo productVo = productService.findByParantId(productId, Long.parseLong(session.getAttribute("companyId").toString()));
		productVo.getProductVariantVos().forEach(p -> p.setProductVo(null));
		productVo.getProductVo().getProductVariantVos().forEach(p -> p.setProductVo(null));
		productVo.getProductVo().setProductAttributeVos(null);
		productVo.setProductAttributeVos(null);
		return productVo;
	}
	
	@GetMapping("{id}/delete")
	public String productPackageDelete(@PathVariable("id") long productId, HttpSession session) {
		
		productService.deleteProduct(productId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "redirect:/productpackage";
	}
}
