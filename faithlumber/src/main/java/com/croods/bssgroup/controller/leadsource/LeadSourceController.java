package com.croods.bssgroup.controller.leadsource;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.service.leadsource.LeadSourceService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.leadsource.LeadSourceVo;

@Controller
@RequestMapping("/leadsource")
public class LeadSourceController {
	
	@Autowired
	LeadSourceService leadSourceService;
	
	@GetMapping("")
	public ModelAndView leadSource(HttpSession session)
 	{
		ModelAndView view=new ModelAndView("leadsource/leadsource");
 		view.addObject("leadSourceVos",leadSourceService.findByCompanyIdAndIsEditable(Long.parseLong(session.getAttribute("companyId").toString())));
 		return view;			
	}
 
	@PostMapping("save")
	@ResponseBody
	public LeadSourceVo saveLeadSource(@RequestParam(value="sourceName") String sourceName, HttpSession session)
 	{
		LeadSourceVo leadSourceVo=new LeadSourceVo();
		leadSourceVo.setSourceName(sourceName);
		
		leadSourceVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		leadSourceVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		leadSourceVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		leadSourceVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		leadSourceVo.setEditable(true);
		leadSourceVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		leadSourceVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		leadSourceService.save(leadSourceVo);
		
		return leadSourceVo;
 	}
	
	
	@PostMapping("update")
	@ResponseBody
	public LeadSourceVo updateLeadSource(@RequestParam(value="sourceName") String sourceName, @RequestParam(value="id") long id, HttpSession session)
	{
		LeadSourceVo leadSourceVo=new LeadSourceVo();
		leadSourceVo=leadSourceService.findByLeadSourceId(id);
		
		leadSourceVo.setSourceName(sourceName);
		leadSourceVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		leadSourceVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		leadSourceVo.setEditable(true);
		leadSourceService.save(leadSourceVo);
		return leadSourceVo;
	}
	
	@PostMapping("delete")
	@ResponseBody
	public String deleteLeadSource(@RequestParam(value="id") long id, HttpSession session)
	{
		leadSourceService.deleteLeadSource(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "Sucess";
	}
	
	@PostMapping("verify")
	@ResponseBody
	public String isExistLeadSource(@RequestParam("sourceName") String sourceName, @RequestParam(value="leadSourceId",defaultValue="0") String leadSourceId, HttpSession session) {
		boolean isExist = false;
		if(leadSourceId.equals("0")) {
			isExist = leadSourceService.isExistLeadSource(Long.parseLong(session.getAttribute("companyId").toString()), sourceName);
		} else {
			
			isExist = leadSourceService.isExistLeadSource(Long.parseLong(session.getAttribute("companyId").toString()), sourceName, Long.parseLong(leadSourceId));
			
		}
		System.out.println(leadSourceId +"////"  +isExist);
		return "{ \"valid\": "+isExist+" }";
	}

}
