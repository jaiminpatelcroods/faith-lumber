package com.croods.bssgroup.controller.farmoparticular;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.service.farmoparticular.FarmoParticularService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.farmoparticular.FarmoParticularVo;

@Controller
@RequestMapping("farmoparticular")
public class  FarmoParticularController {

	@Autowired
	FarmoParticularService farmoParticularService;
	
	@GetMapping("")
	public ModelAndView farmoParticular(HttpSession session)
	{
		ModelAndView view =new ModelAndView("farmoparticular/farmoparticular");
		view.addObject("farmoParticularVos",farmoParticularService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		return view;	
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public FarmoParticularVo save(@RequestParam(value = "name") String name,
			@RequestParam(value = "isActive") String isActive, HttpSession session) {
		
		FarmoParticularVo farmoParticularVo =  new FarmoParticularVo();
		farmoParticularVo.setName(name);
		farmoParticularVo.setActive(isActive.equals("0")?false:true);
		farmoParticularVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		farmoParticularVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		farmoParticularVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		farmoParticularVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		farmoParticularVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		farmoParticularVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		farmoParticularService.save(farmoParticularVo);

		return farmoParticularVo;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public FarmoParticularVo update(@RequestParam(value="name") String name,@RequestParam(value="isActive") String isActive,@RequestParam(value="farmoParticularId") long farmoParticularId, HttpSession session)
	{	
		FarmoParticularVo farmoParticularVo=farmoParticularService.findByFarmoParticularId(farmoParticularId);
		farmoParticularVo.setName(name);
		farmoParticularVo.setActive(isActive.equals("0")?false:true);
		farmoParticularVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		farmoParticularVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		farmoParticularService.save(farmoParticularVo);

		return farmoParticularVo;	
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(@RequestParam(value="id") long id,HttpSession session)
	{	
		farmoParticularService.delete(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "Success";	
	}
	
	@GetMapping("{type}/json")
	@ResponseBody
	public List<FarmoParticularVo> farmoParticularJSON(@PathVariable("type") String type, HttpSession session)
	{
		return farmoParticularService.findByCompanyIdAndActive(Long.parseLong(session.getAttribute("companyId").toString()), type.equals("0")?false:true);	
	}
}
