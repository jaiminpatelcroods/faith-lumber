package com.croods.bssgroup.controller.materialrequest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.service.employee.EmployeeService;
import com.croods.bssgroup.service.jobwork.JobworkService;
import com.croods.bssgroup.service.materialrequest.MaterialRequestService;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.deliverychallan.DeliveryChallanVo;
import com.croods.bssgroup.vo.jobwork.JobworkVo;
import com.croods.bssgroup.vo.materialrequest.MaterialRequestItemVo;
import com.croods.bssgroup.vo.materialrequest.MaterialRequestVo;

@Controller
@RequestMapping("materialrequest")
public class MaterialRequestController {

	@Autowired
	MaterialRequestService materialRequestService;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	JobworkService jobworkService;
	
	@GetMapping("")
	public ModelAndView purchaseRequest(HttpSession session) {
		ModelAndView view = new ModelAndView("materialrequest/materialrequest");
		
		return view;
	}
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<MaterialRequestVo> materialRequestDatatable(@Valid DataTablesInput input,@RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<MaterialRequestVo>  specification =new Specification<MaterialRequestVo>() {
			
			@Override
			public Predicate toPredicate(Root<MaterialRequestVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				query.orderBy(criteriaBuilder.desc(root.get("requestDate")));
				query.orderBy(criteriaBuilder.desc(root.get("materialRequestId")));
				try {
					predicates.add(criteriaBuilder.between(root.get("requestDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<MaterialRequestVo> dataTablesOutput=materialRequestService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> {
			p.setMaterialRequestItemVos(null);
			p.setJobworkVo(null);
			p.getRequestedBy().setEmployeeContactVos(null);
			p.getRequestedBy().setUserFrontVo(null);
		});
		return dataTablesOutput;
	}
	
	@GetMapping("new")
	public ModelAndView materialRequestNew(HttpSession session) {
		ModelAndView view = new ModelAndView("materialrequest/materialrequest-new");
		
		view.addObject("employeeVos", employeeService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		
		view.addObject("jobworkVos", jobworkService.findByBranchIdAndStatus(Long.parseLong(session.getAttribute("branchId").toString()), "active"));
		
		return view;
	}
	
	@PostMapping("save")
	public String saveMaterialRequest(@RequestParam Map<String,String> allRequestParams, @ModelAttribute("materialRequestVo") MaterialRequestVo materialRequestVo,HttpSession session) {
		
		materialRequestVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		materialRequestVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		materialRequestVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		materialRequestVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		if(materialRequestVo.getMaterialRequestId() == 0) {
			materialRequestVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			materialRequestVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			
			materialRequestVo.setRequestNo(materialRequestService.findMaxRequestNo(Constant.MATERIAL_REQUEST, 
					Long.parseLong(session.getAttribute("companyId").toString()), 
					Long.parseLong(session.getAttribute("branchId").toString()), 
					Long.parseLong(session.getAttribute("userId").toString()), "MRQ"));
			
			materialRequestVo.setPrefix(prefixService.findByPrefixTypeAndBranchId(Constant.MATERIAL_REQUEST,
					Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
		}
		
		if(materialRequestVo.getMaterialRequestItemVos() != null) {
			materialRequestVo.getMaterialRequestItemVos().removeIf( rm -> rm.getProductVariantVo() == null);
			materialRequestVo.getMaterialRequestItemVos().forEach( item -> item.setMaterialRequestVo(materialRequestVo));
		}
		
		if(allRequestParams.get("deleteMaterialRequestIds") != null &&
				!allRequestParams.get("deleteMaterialRequestIds").equals("")) {
			String address=allRequestParams.get("deleteMaterialRequestIds").substring(0, allRequestParams.get("deleteMaterialRequestIds").length()-1);
			List<Long> l= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
			
			materialRequestService.deleteMaterialRequestItemIds(l);
		}
		
		materialRequestService.save(materialRequestVo);
		
		return "redirect:/materialrequest/"+materialRequestVo.getMaterialRequestId();
	}
	
	@GetMapping("{id}")
	public ModelAndView viewMaterialRequest(@PathVariable("id") long materialRequestId, HttpSession session) {
		
		ModelAndView view = new ModelAndView("materialrequest/materialrequest-view");
		
		MaterialRequestVo materialRequestVo = materialRequestService.findByMaterialRequestIdAndBranchId(materialRequestId, Long.parseLong(session.getAttribute("branchId").toString()));
		
		view.addObject("materialRequestVo", materialRequestVo);
		
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView materialRequestNew(@PathVariable("id") long materialRequestId, HttpSession session) {
		ModelAndView view = new ModelAndView("materialrequest/materialrequest-edit");
		
		MaterialRequestVo materialRequestVo = materialRequestService.findByMaterialRequestIdAndBranchId(materialRequestId, Long.parseLong(session.getAttribute("branchId").toString()));
		view.addObject("materialRequestVo", materialRequestVo);
		view.addObject("employeeVos", employeeService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		view.addObject("jobworkVos", jobworkService.findByBranchIdAndStatus(Long.parseLong(session.getAttribute("branchId").toString()), "active"));
		return view;
	}
	
	@GetMapping("{id}/delete")
	public String materialRequestDelete(@PathVariable("id") long materialRequestId, HttpSession session) {
		
		materialRequestService.delete(materialRequestId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "redirect:/materialrequest";
	}
	
	@PostMapping("{id}/item/json")
	@ResponseBody
	public List<MaterialRequestItemVo> materialRequestItemJSON(@PathVariable("id") long materialRequestId, HttpSession session) {
		
		List<MaterialRequestItemVo> materialRequestItemVos = materialRequestService.findItemByMaterialRequestId(materialRequestId);
		
		materialRequestItemVos.forEach(m -> {
			m.setMaterialRequestVo(null);
			m.getProductVariantVo().getProductVo().setProductVariantVos(null);
			m.getProductVariantVo().getProductVo().setProductAttributeVos(null);
			m.getProductVariantVo().getProductVo().setProductVo(null);
		});
		return materialRequestItemVos;
	}
}
