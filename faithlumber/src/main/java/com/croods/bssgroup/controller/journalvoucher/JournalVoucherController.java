package com.croods.bssgroup.controller.journalvoucher;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.service.account.AccountService;
import com.croods.bssgroup.service.journalvoucher.JournalVoucherService;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.util.JasperExporter;
import com.croods.bssgroup.util.NumberToWord;
import com.croods.bssgroup.vo.journalvoucher.JournalVoucherVo;
import com.fasterxml.jackson.core.JsonProcessingException;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@Controller
@RequestMapping("/account/journalvoucher")
public class JournalVoucherController {

	@Autowired
	JournalVoucherService journalVoucherService;
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	PrefixService prefixService;
	
	JasperReport jasperReport;
	JasperPrint jasperPrint;
	OutputStream outputStream;
	HashMap jasperParameter;
	
	@GetMapping("")
	public ModelAndView journalVoucher(HttpSession session) throws NumberFormatException, ParseException {
		ModelAndView view = new ModelAndView("accounting/journalvoucher/journalvoucher");
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		view.addObject("journalVoucherVos",journalVoucherService.findByBranchIdAndVoucherDateBetween(Long.parseLong(session.getAttribute("branchId").toString()), 
				dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
				dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
		
		return view;
	}
	
	@GetMapping("new")
	public ModelAndView journalVoucherNew(HttpSession session) throws NumberFormatException, ParseException, JsonProcessingException {
		ModelAndView view = new ModelAndView("accounting/journalvoucher/journalvoucher-new");
		
		return view;
	}
	
	@PostMapping("save")
	public String journalVoucherSave(@RequestParam Map<String,String> allRequestParams, HttpSession session, @ModelAttribute("journalVoucherVo") JournalVoucherVo journalVoucherVo) throws NumberFormatException, ParseException {
		
		journalVoucherVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		journalVoucherVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		journalVoucherVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		journalVoucherVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		if(journalVoucherVo.getJournalVoucherId() == 0) {
			
			long newVoucherNo = journalVoucherService.findMaxVoucherNo(Constant.ACCOUNTING_JOURNAL, 
					Long.parseLong(session.getAttribute("companyId").toString()), 
					Long.parseLong(session.getAttribute("branchId").toString()), 
					Long.parseLong(session.getAttribute("userId").toString()), "JNV");
			
			journalVoucherVo.setPrefix(prefixService.findByPrefixTypeAndBranchId(Constant.ACCOUNTING_JOURNAL,
					Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
			journalVoucherVo.setVoucherNo(newVoucherNo);
			
			journalVoucherVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			journalVoucherVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		}
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		try {
			journalVoucherVo.setVoucherDate(dateFormat.parse(allRequestParams.get("voucherDate")));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		if(journalVoucherVo.getJournalVoucherAccountVos().size() != 0) {
			journalVoucherVo.getJournalVoucherAccountVos().removeIf( j -> j.getFromAccountCustomVo() == null);
			journalVoucherVo.getJournalVoucherAccountVos().forEach(j -> j.setJournalVoucherVo(journalVoucherVo));
		}
		journalVoucherService.save(journalVoucherVo);
		
		return "redirect:/account/journalvoucher/"+journalVoucherVo.getJournalVoucherId();
	}
	
	@GetMapping("{id}")
	public ModelAndView journalVoucherView(@PathVariable("id") long journalVoucherId, HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("accounting/journalvoucher/journalvoucher-view");
		
		view.addObject("journalVoucherVo", journalVoucherService.findByJournalVoucherIdAndBranchId(journalVoucherId, Long.parseLong(session.getAttribute("branchId").toString())));
		
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView journalVoucherEdit(@PathVariable("id") long journalVoucherId, HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("accounting/journalvoucher/journalvoucher-edit");
		
		view.addObject("journalVoucherVo", journalVoucherService.findByJournalVoucherIdAndBranchId(journalVoucherId, Long.parseLong(session.getAttribute("branchId").toString())));
		
		String fromTypes = Constant.TAX + "," + Constant.ADDITIONAL_CHARGE + "," + Constant.BANK;
		
		view.addObject("accountCustomVos", accountService.findByBranchIdAndAccounTypesNotIn(
				Long.parseLong(session.getAttribute("branchId").toString()), Arrays.asList(fromTypes.split(","))));
		
		return view;
	}
	
	@GetMapping("{id}/delete")
	public String creditNoteAccountingDelete(@PathVariable("id") long journalVoucherId, HttpSession session) throws NumberFormatException, ParseException {
		
		journalVoucherService.delete(journalVoucherId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "redirect:/account/journalvoucher";
	}
	
	@GetMapping("{id}/pdf")
	public void journalVoucherPDF(@PathVariable("id") long journalVoucherId, HttpSession session, HttpServletRequest request, HttpServletResponse response) 
	{
		JournalVoucherVo journalVoucherVo=journalVoucherService.findByJournalVoucherIdAndBranchId(journalVoucherId, Long.parseLong(session.getAttribute("branchId").toString()));
		jasperParameter = new HashMap();
		jasperParameter.put("account_id",journalVoucherId);
		jasperParameter.put("user_front_id",Long.parseLong(session.getAttribute("branchId").toString()));
		jasperParameter.put("display_title","Journal Voucher");
		jasperParameter.put("path", request.getServletContext().getRealPath("/") + "report" + System.getProperty("file.separator"));
		jasperParameter.put("amount_in_word",new NumberToWord().getNumberToWord(journalVoucherVo.getTotal()));
		JasperExporter jasperExporter = new JasperExporter(); 
		try 
		{			
				jasperExporter.jasperExporterPDF(jasperParameter, request.getServletContext().getRealPath("/") + "report/account"
								+ System.getProperty("file.separator") +"/journal_vouchar.jrxml","Certificate-"+journalVoucherId+".pdf", response);					
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
