package com.croods.bssgroup.controller.productionmaster;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.service.productionmaster.ProductionProcessService;
import com.croods.bssgroup.service.productionmaster.ProductionTypeService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.farmoparticular.FarmoParticularVo;
import com.croods.bssgroup.vo.productionmaster.ProductionProcessVo;
import com.croods.bssgroup.vo.productionmaster.ProductionTypeVo;

@Controller
@RequestMapping("production/process")
public class  ProductionProcessController {

	@Autowired
	ProductionTypeService productionTypeService;
	
	@Autowired
	ProductionProcessService productionProcessService;
	
	@GetMapping("")
	public ModelAndView farmoParticular(HttpSession session)
	{
		ModelAndView view =new ModelAndView("productionprocess/productionprocess");
		view.addObject("productionTypeVos",productionTypeService.findAll());
		view.addObject("productionProcessVos",productionProcessService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		return view;	
	}
	
	@RequestMapping("/save")
	@ResponseBody
	public ProductionProcessVo save(@RequestParam(value = "productionTypeId") String productionTypeId,@RequestParam(value = "productionProcessName") String productionProcessName, HttpSession session) {
		ProductionTypeVo productionTypeVo = new ProductionTypeVo();
		productionTypeVo.setProductionTypeId(Long.parseLong(productionTypeId));
		
		ProductionProcessVo productionProcessVo =  new ProductionProcessVo();
		productionProcessVo.setProductionTypeVo(productionTypeVo);
		productionProcessVo.setProductionProcessName(productionProcessName);
		productionProcessVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		productionProcessVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		productionProcessVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		productionProcessVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		productionProcessVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		productionProcessVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		productionProcessService.save(productionProcessVo);

		return productionProcessVo;
	}
	
	@RequestMapping("/update")
	@ResponseBody
	public ProductionProcessVo update(@RequestParam(value = "productionTypeId") String productionTypeId,@RequestParam(value = "productionProcessName") String productionProcessName,@RequestParam(value="productionProcessId") long productionProcessId, HttpSession session)
	{	
		ProductionTypeVo productionTypeVo = new ProductionTypeVo();
		productionTypeVo.setProductionTypeId(Long.parseLong(productionTypeId));
		
		ProductionProcessVo productionProcessVo=productionProcessService.findByProductionProcessId(productionProcessId);
		productionProcessVo.setProductionTypeVo(productionTypeVo);
		productionProcessVo.setProductionProcessName(productionProcessName);
		productionProcessVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		productionProcessVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		productionProcessService.save(productionProcessVo);

		return productionProcessVo;	
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(@RequestParam(value="id") long id,HttpSession session)
	{	
		productionProcessService.delete(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "Success";	
	}
	
	/*@GetMapping("{type}/json")
	@ResponseBody
	public List<FarmoParticularVo> farmoParticularJSON(@PathVariable("type") String type, HttpSession session)
	{
		return farmoParticularService.findByCompanyIdAndActive(Long.parseLong(session.getAttribute("companyId").toString()), type.equals("0"));	
	}*/
}
