package com.croods.bssgroup.controller.categoryandbrand;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.service.brand.BrandService;
import com.croods.bssgroup.service.category.CategoryService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.brand.BrandVo;
import com.croods.bssgroup.vo.category.CategoryVo;

@Controller
@RequestMapping("/categorybrand")
public class CategoryAndBrandController {
	
	@Autowired
	CategoryService categoryService;
	
	@Autowired
	BrandService brandService;
	
	@GetMapping("")
	public ModelAndView newContact(HttpSession session)
 	{
		ModelAndView view=new ModelAndView("category-brand/category-brand");
 	
 		view.addObject("categoryVos",categoryService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		view.addObject("brandVos",brandService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
 		return view;			
	}
	
	@PostMapping("category/save")
	@ResponseBody
	public CategoryVo saveCategory(@RequestParam(value="name") String name,@RequestParam(value="description") String description,HttpSession session)
 	{
		CategoryVo cat=new CategoryVo();
		cat.setCategoryName(name);
		cat.setCategoryDescription(description);
		cat.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		cat.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		cat.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		cat.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		
		cat.setCreatedOn(CurrentDateTime.getCurrentDate());
		cat.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		categoryService.save(cat);
		
		return cat;
	}
	
	@PostMapping("brand/save")
	@ResponseBody
	public BrandVo saveBrand(@RequestParam(value="name") String name,@RequestParam(value="description") String description,HttpSession session)
	{
		BrandVo brand=new BrandVo();
		brand.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		brand.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		brand.setBrandName(name);
		brand.setBrandDescription(description);
		
		brand.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		brand.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		
		brand.setCreatedOn(CurrentDateTime.getCurrentDate());
		brand.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		brandService.save(brand);
		
		return brand;
	}
	
	@PostMapping("category/update")
	@ResponseBody
	public CategoryVo updateUategory(@RequestParam(value="name") String name,@RequestParam(value="description") String description,@RequestParam(value="id") long id, HttpSession session)
	{
		CategoryVo cat=new CategoryVo();
		cat=categoryService.findByCategoryId(id);
		
		cat.setCategoryName(name);
		cat.setCategoryDescription(description);
		
		cat.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		cat.setModifiedOn(CurrentDateTime.getCurrentDate());
		categoryService.save(cat);
		
		return cat;
	}
	
	@PostMapping("brand/update")
	@ResponseBody
	public BrandVo updateBrand(@RequestParam(value="name") String name,@RequestParam(value="description") String description,@RequestParam(value="id") long id, HttpSession session)
	{
		BrandVo brand=new BrandVo();
		brand=brandService.findByBrandId(id);
		
		brand.setBrandName(name);
		brand.setBrandDescription(description);
		
		brand.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		brand.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		brandService.save(brand);
		
		return brand;
	}
	
	@PostMapping("category/delete")
	@ResponseBody
	public String deleteCategory(@RequestParam(value="id") long id, HttpSession session)
	{
	
		categoryService.deleteCategory(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "Sucess";
	}
	
	@PostMapping("brand/delete")
	@ResponseBody
	public String deleteBrand(@RequestParam(value="id") long id, HttpSession session)
	{
	
		brandService.deleteBrand(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "Sucess";
	}
	
	@PostMapping("category/verify")
	@ResponseBody
	public String IsCategoryExist(@RequestParam("category") String categoryName, @RequestParam(value="categoryId",defaultValue="0") String categoryId, HttpSession session) {
		boolean isExist = false;
		if(categoryId.equals("0")) {
			isExist = categoryService.isExistCategory(Long.parseLong(session.getAttribute("companyId").toString()), categoryName);
		} else {
			isExist = categoryService.isExistCategory(Long.parseLong(session.getAttribute("companyId").toString()), categoryName, Long.parseLong(categoryId));
		}
		
		return "{ \"valid\": "+isExist+" }";
	}
	
	@PostMapping("brand/verify")
	@ResponseBody
	public String isExistBrand(@RequestParam("brandname") String brandName, @RequestParam(value="brandId",defaultValue="0") String brandId, HttpSession session) {
		boolean isExist = false;
		if(brandId.equals("0")) {
			isExist = brandService.isExistBrand(Long.parseLong(session.getAttribute("companyId").toString()), brandName);
		} else {
			isExist = brandService.isExistBrand(Long.parseLong(session.getAttribute("companyId").toString()), brandName, Long.parseLong(brandId));
		}
		
		return "{ \"valid\": "+isExist+" }";
	}
	
}
