package com.croods.bssgroup.controller.farmotype;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.service.farmoparticular.FarmoParticularService;
import com.croods.bssgroup.service.farmotype.FarmoTypeService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.deliverychallan.DeliveryChallanVo;
import com.croods.bssgroup.vo.farmotype.FarmoTypeVo;
import com.croods.bssgroup.vo.purchase.PurchaseVo;

@Controller
@RequestMapping("farmotype")
public class  FarmoTypeController {

	@Autowired
	FarmoTypeService farmoTypeService;
	
	@Autowired
	FarmoParticularService farmoParticularService;
	
	@GetMapping("")
	public ModelAndView farmoType(HttpSession session)
	{
		ModelAndView view =new ModelAndView("farmotype/farmotype");
		view.addObject("farmoTypeVos",farmoTypeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		return view;	
	}
	
	@GetMapping("/new")
	public ModelAndView newPurchase(HttpSession session) {
		ModelAndView view = new ModelAndView("farmotype/farmotype-new");
		view.addObject("farmoPerticularVos", farmoParticularService.findByCompanyIdAndActive(Long.parseLong(session.getAttribute("companyId").toString()), true));
		return view;
	}
	
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<FarmoTypeVo> purchaseRequestDatatable(@Valid DataTablesInput input, @RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long companyId = Long.parseLong(session.getAttribute("companyId").toString());

		Specification<FarmoTypeVo>  specification =new Specification<FarmoTypeVo>() {
			
			@Override
			public Predicate toPredicate(Root<FarmoTypeVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("companyId"), companyId));
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<FarmoTypeVo> dataTablesOutput=farmoTypeService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> {
			p.setFarmoTypeParticularVos(null);
		});
		
		return dataTablesOutput;
	}
	
	@PostMapping("/save")
	public String save(@RequestParam Map<String,String> allRequestParams,@ModelAttribute("farmoTypeVo") FarmoTypeVo farmoTypeVo, HttpSession session) {
		
		farmoTypeVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		farmoTypeVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		farmoTypeVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		farmoTypeVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		farmoTypeVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		farmoTypeVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		if(farmoTypeVo.getFarmoTypeParticularVos() != null) {
			farmoTypeVo.getFarmoTypeParticularVos().removeIf(rm -> rm.getFarmoParticularVo()==null);
			farmoTypeVo.getFarmoTypeParticularVos().forEach(p->p.setFarmoTypeVo(farmoTypeVo));
		}
		
		if(allRequestParams.get("deleteFarmoTypeIds") != null &&
				!allRequestParams.get("deleteFarmoTypeIds").equals("")) {
			String address=allRequestParams.get("deleteFarmoTypeIds").substring(0, allRequestParams.get("deleteFarmoTypeIds").length()-1);
			List<Long> l= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
			
			farmoTypeService.deleteFarmoTypePerticularsIds(l);
		}
		
		farmoTypeService.save(farmoTypeVo);

		return "redirect:/farmotype/"+farmoTypeVo.getFarmoTypeId();
	}
	
	@GetMapping("/{id}")
	public ModelAndView viewDeliveryChallan(@PathVariable("id") long farmoTypeId, HttpSession session) {
		
		FarmoTypeVo farmoTypeVo = farmoTypeService.findByFarmoTypeId(farmoTypeId, Long.parseLong(session.getAttribute("companyId").toString()));
		ModelAndView view = new ModelAndView("farmotype/farmotype-view");
		view.addObject("farmoTypeVo", farmoTypeVo);
		return view;
	}
	
	@GetMapping("/{id}/edit")
	public ModelAndView editDeliveryChallan(@PathVariable("id") long farmoTypeId, HttpSession session) {
		
		FarmoTypeVo farmoTypeVo = farmoTypeService.findByFarmoTypeId(farmoTypeId, Long.parseLong(session.getAttribute("companyId").toString()));
		ModelAndView view = new ModelAndView("farmotype/farmotype-edit");
		view.addObject("farmoTypeVo", farmoTypeVo);
		view.addObject("farmoPerticularVos", farmoParticularService.findByCompanyIdAndActive(Long.parseLong(session.getAttribute("companyId").toString()), true));
		return view;
	}
	
	
	@RequestMapping("/{id}/delete")
	public String delete(@PathVariable("id") long farmoTypeId, HttpSession session)
	{	
		farmoTypeService.delete(farmoTypeId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "redirect:/farmotype";	
	}
	
	@GetMapping("json")
	@ResponseBody
	public FarmoTypeVo farmoTypeJSON(@RequestParam(value="farmoTypeId") long farmoTypeId, HttpSession session){	
		FarmoTypeVo farmoTypeVo= farmoTypeService.findByFarmoTypeId(farmoTypeId,Long.parseLong(session.getAttribute("companyId").toString())); 
		farmoTypeVo.getFarmoTypeParticularVos().forEach(p->p.setFarmoTypeVo(null));
		return 	farmoTypeVo;
	}
}
