package com.croods.bssgroup.controller.contact;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.service.account.AccountService;
import com.croods.bssgroup.service.contact.ContactService;
import com.croods.bssgroup.service.location.LocationService;
import com.croods.bssgroup.service.product.ProductService;
import com.croods.bssgroup.service.purchase.PurchaseService;
import com.croods.bssgroup.service.sales.SalesService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.util.JasperExporter;
import com.croods.bssgroup.util.site2sms;
import com.croods.bssgroup.vo.account.AccountCustomVo;
import com.croods.bssgroup.vo.account.AccountGroupVo;
import com.croods.bssgroup.vo.contact.ContactAddressVo;
import com.croods.bssgroup.vo.contact.ContactVo;

@Controller
@RequestMapping("/contact/{type}")
public class ContactController {

	@Autowired
	ContactService contactService;
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	LocationService locationService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	SalesService salesService;
	
	@Autowired
	PurchaseService purchaseService;
	
	ContactVo contactVo;
	
	OutputStream outputStream;
	HashMap jasperParameter;
	
	@GetMapping("")
	public String contact(@PathVariable(value="type") String type, HttpSession session, Model view)
 	{	 
		
		if(type.equals(Constant.CONTACT_CUSTOMER)) {
			view.addAttribute("displayContactType","Customer");
			view.addAttribute("type","customers");
		} else if(type.equals(Constant.CONTACT_SUPPLIER))  {
			view.addAttribute("displayContactType","Supplier");
			view.addAttribute("type","suppliers");
		} else if(type.equals(Constant.CONTACT_AGENT))  {
			view.addAttribute("displayContactType","Agent");
			view.addAttribute("type","agent");
		} else if(type.equals(Constant.CONTACT_TRANSPORT))  {
			view.addAttribute("displayContactType","Transport");
			view.addAttribute("type","transport");
		} else {
			return "404";
		}
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		List<ContactVo> contactVos = contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), type);
		
		List<String> type1=new ArrayList();
		
		if (type.equals(Constant.CONTACT_CUSTOMER)) {
			type1.add(Constant.SALES_INVOICE);
			type1.add(Constant.SALES_POS);
		} else if (type.equals(Constant.CONTACT_SUPPLIER)) {
			type1.add(Constant.PURCHASE_BILL);
		}
		
		contactVos.forEach(c -> {
			c.getContactAddressVos().removeIf( a ->
				a.getIsDefault() ==0 || a.getIsDeleted() == 1);
			
			c.getContactAddressVos().forEach( a -> {
				a.setCityName(locationService.findCityNameByCityCode(a.getCityCode()));
			});
			
			if (type.equals(Constant.CONTACT_CUSTOMER)) {
				
				try {
					c.setDueAmount(salesService.getDueAmountByContactId(type1, 
							Long.parseLong(session.getAttribute("branchId").toString()), c.getContactId(), 
							dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()), 
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
					
				} catch (NumberFormatException | ParseException e) {
					e.printStackTrace();
				}
			} else if (type.equals(Constant.CONTACT_SUPPLIER)) {
				
				try {
					c.setDueAmount(purchaseService.getDueAmountByContactId(type1, 
							Long.parseLong(session.getAttribute("branchId").toString()), c.getContactId(), 
							dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()), 
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
					
				} catch (NumberFormatException | ParseException e) {
					e.printStackTrace();
				}
			}
		});
		
		view.addAttribute("contactVos", contactVos);
		
		return "contact/contact";
	}
	
	@RequestMapping("new")
	public String newContact(HttpSession session,@PathVariable(value="type") String type, Model view)
 	{	
		
		if(type.equals(Constant.CONTACT_CUSTOMER)) {
			view.addAttribute("displayContactType","Customer");
			view.addAttribute("type","customers");
		} else if(type.equals(Constant.CONTACT_SUPPLIER))  {
			view.addAttribute("displayContactType","Supplier");
			view.addAttribute("type","suppliers");
		} else if(type.equals(Constant.CONTACT_AGENT))  {
			view.addAttribute("displayContactType","Agent");
			view.addAttribute("type","agent");
		} else if(type.equals(Constant.CONTACT_TRANSPORT))  {
			view.addAttribute("displayContactType","Transport");
			view.addAttribute("type","transport");
		} else {
			return "404";
		}
		
		//For Agent dropdown in customers Start
		view.addAttribute("contactAgentList", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_AGENT));
		
		return "contact/contact-new";
	}
	
	@PostMapping("/save")
	public String save(HttpSession session,@PathVariable(value="type")String type,@RequestParam Map<String,String> allRequestParams, @ModelAttribute("contactVo") ContactVo contactVo, BindingResult bindingResult) {
		
    	if(contactVo.getContactAddressVos() != null && contactVo.getContactAddressVos().size() != 0) {
    		contactVo.getContactAddressVos().forEach(c -> c.setContactVo(contactVo));
    	}
    	if(contactVo.getContactOtherVos() != null && contactVo.getContactOtherVos().size() != 0) {
    		contactVo.getContactOtherVos().forEach(c -> c.setContactVo(contactVo));
    	}
    	
    	AccountCustomVo accountCustomVo = new AccountCustomVo();
    	
    	if(contactVo.getContactId() == 0) {
	    	
    		contactVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
        	contactVo.setCreatedOn(CurrentDateTime.getCurrentDate());
    	}
    	
    	if(type.equals(Constant.CONTACT_CUSTOMER) ||
    			type.equals(Constant.CONTACT_SUPPLIER)) {
    		
    		if(contactVo.getContactId() == 0) {
    			accountCustomVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
    	    	accountCustomVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
    	    	accountCustomVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
    	    	accountCustomVo.setCreatedOn(CurrentDateTime.getCurrentDate());
    	    	
    	    	AccountGroupVo accountGroupVo = new AccountGroupVo();
    	    	if (type.equals(Constant.CONTACT_CUSTOMER)) {			
    				accountGroupVo.setAccountGroupId(Constant.ACCOUNT_GROUP_SUNDRY_DEBTORS);			
    				accountCustomVo.setAccounType(Constant.CONTACT_CUSTOMER);
    			} else if (type.equals(Constant.CONTACT_SUPPLIER)) {
    				accountGroupVo.setAccountGroupId(Constant.ACCOUNT_GROUP_SUNDRY_CREDITORS);
    				accountCustomVo.setAccounType(Constant.CONTACT_SUPPLIER);
    			}
    	    	
    	    	accountCustomVo.setGroup(accountGroupVo);
    		} else {
    			accountCustomVo = contactService.findAccountCustomVoByContactId(contactVo.getContactId());
    		}
    		
    		accountCustomVo.setAccountName(contactVo.getCompanyName());
        	accountCustomVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
        	accountCustomVo.setModifiedOn(CurrentDateTime.getCurrentDate());
        	
        	accountService.insertAccount(accountCustomVo);
        	
        	contactVo.setAccountCustomVo(accountCustomVo);
    	}
    	
    	contactVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
    	contactVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
    	contactVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
    	contactVo.setModifiedOn(CurrentDateTime.getCurrentDate());
    	contactVo.setType(type);
    	
    	contactService.saveContact(contactVo);
    	
    	if(allRequestParams.containsKey("deleteAddress") == true) {		
			if(!allRequestParams.get("deleteAddress").equals("")) {
				String address=allRequestParams.get("deleteAddress").substring(0, allRequestParams.get("deleteAddress").length()-1);
				List<Long> contactAddressIds= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
				contactService.deleteContactAddressByIdIn(contactAddressIds);
			}
			
		}
    	
    	if(allRequestParams.containsKey("deletedContactOther") == true) {		
			if(!allRequestParams.get("deletedContactOther").equals("")) {
				String address=allRequestParams.get("deletedContactOther").substring(0, allRequestParams.get("deletedContactOther").length()-1);
				List<Long> contactOtherIds= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
				contactService.deleteContactOtherByIdIn(contactOtherIds);
			}
			
		}
		return "redirect:/contact/"+type+"/"+contactVo.getContactId();
	}
	
	@PostMapping("/save/json")
	@ResponseBody
	public ContactVo saveJSON(HttpSession session,@PathVariable(value="type")String type,@RequestParam Map<String,String> allRequestParams, @ModelAttribute("contactVo") ContactVo contactVo, BindingResult bindingResult) {
		
    	if(contactVo.getContactAddressVos() != null && contactVo.getContactAddressVos().size() != 0) {
    		contactVo.getContactAddressVos().forEach(c -> {
    			c.setContactVo(contactVo);
    			c.setCompanyName(contactVo.getCompanyName());
    		});
    	}
    	
    	AccountCustomVo accountCustomVo = new AccountCustomVo();
    	
    	if(contactVo.getContactId() == 0) {
	    	
    		contactVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
        	contactVo.setCreatedOn(CurrentDateTime.getCurrentDate());
    	}
    	
    	if(type.equals(Constant.CONTACT_CUSTOMER) ||
    			type.equals(Constant.CONTACT_SUPPLIER)) {
    		
    		contactVo.setContactType(Constant.CONTACT_CATEGORY_RETAILER);
    		if(contactVo.getContactId() == 0) {
    			accountCustomVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
    	    	accountCustomVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
    	    	accountCustomVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
    	    	accountCustomVo.setCreatedOn(CurrentDateTime.getCurrentDate());
    	    	
    	    	AccountGroupVo accountGroupVo = new AccountGroupVo();
    	    	if (type.equals(Constant.CONTACT_CUSTOMER)) {			
    				accountGroupVo.setAccountGroupId(Constant.ACCOUNT_GROUP_SUNDRY_DEBTORS);			
    				accountCustomVo.setAccounType(Constant.CONTACT_CUSTOMER);
    			} else if (type.equals(Constant.CONTACT_SUPPLIER)) {
    				accountGroupVo.setAccountGroupId(Constant.ACCOUNT_GROUP_SUNDRY_CREDITORS);
    				accountCustomVo.setAccounType(Constant.CONTACT_SUPPLIER);
    			}
    	    	
    	    	accountCustomVo.setGroup(accountGroupVo);
    		} else {
    			accountCustomVo = contactService.findAccountCustomVoByContactId(contactVo.getContactId());
    		}
    		
    		accountCustomVo.setAccountName(contactVo.getCompanyName());
        	accountCustomVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
        	accountCustomVo.setModifiedOn(CurrentDateTime.getCurrentDate());
        	
        	accountService.insertAccount(accountCustomVo);
        	
        	contactVo.setAccountCustomVo(accountCustomVo);
    	}
    	
    	contactVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
    	contactVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
    	contactVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
    	contactVo.setModifiedOn(CurrentDateTime.getCurrentDate());
    	contactVo.setType(type);
    	
    	contactService.saveContact(contactVo);
    	ContactVo contactVo2 = new ContactVo();
    	
    	contactVo2.setContactId(contactVo.getContactId());
    	contactVo2.setCompanyName(contactVo.getCompanyName());
		return contactVo2;
	}
	
	@GetMapping("{id}")
	public String contactView(@PathVariable("id") long contactId, @PathVariable("type") String type, HttpSession session, Model view) throws NumberFormatException, ParseException {
		
		/*ContactVo contactVo1 = contactRepository.findCompanyNameByContactId(contactId);
		System.out.println(contactVo1.getCompanyName());
		
		System.out.println(contactVo1.getGstin());*/
		//System.out.println(contactVo1.getContactAddressVos().size());
		/*System.out.println(contactVo1.getGstin());
		System.out.println(contactVo1.getContactAddressVos().size());*/
		
		if(type.equals(Constant.CONTACT_CUSTOMER)) {
			view.addAttribute("displayContactType","Customer");
			view.addAttribute("type","customers");
		} else if(type.equals(Constant.CONTACT_SUPPLIER))  {
			view.addAttribute("displayContactType","Supplier");
			view.addAttribute("type","suppliers");
		} else if(type.equals(Constant.CONTACT_AGENT))  {
			view.addAttribute("displayContactType","Agent");
			view.addAttribute("type","agent");
		} else if(type.equals(Constant.CONTACT_TRANSPORT))  {
			view.addAttribute("displayContactType","Transport");
			view.addAttribute("type","transport");
		} else {
			return "404";
		}
		
		ContactVo contactVo = contactService.findByContactIdAndBranchIdAndType(contactId, Long.parseLong(session.getAttribute("branchId").toString()), type);
		
		contactVo.getContactAddressVos().removeIf( c -> c.getIsDeleted()==1);
		
		try {
			contactVo.getContactAddressVos().forEach( c -> {
				c.setCountriesName(locationService.findCountriesNameByCountriesCode(c.getCountriesCode()));
				c.setStateName(locationService.findStateNameByStateCode(c.getStateCode()));
				c.setCityName(locationService.findCityNameByCityCode(c.getCityCode()));
			});
		} catch(Exception e) {
			
		}
		
		view.addAttribute("contactVo",contactVo);
		
		/*Customer Intrested Product */
		if(contactVo.getInterestedProductIds() != null && !contactVo.getInterestedProductIds().equals("")) {
			List<Long> productVariantIds = (List<Long>) Arrays.asList(contactVo.getInterestedProductIds().split("\\s*,\\s*")).stream().map(Long::parseLong).collect(Collectors.toList());
		
			view.addAttribute("productVariantVos", productService.findProductVariantByIdIn(productVariantIds));
		}
		
		/*Agent dropdown in customers*/
		view.addAttribute("contactAgentList", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_AGENT));
		
		if(type.equals(Constant.CONTACT_CUSTOMER) || type.equals(Constant.CONTACT_SUPPLIER)) {
			
		
			List<Map<Double,Double>> totalAndPaidAmount=null;
			
			List<String> type1=new ArrayList();
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			
			if(type.equals(Constant.CONTACT_CUSTOMER)) {
				
				type1.add(Constant.SALES_POS);
				type1.add(Constant.SALES_INVOICE);
				
				totalAndPaidAmount=salesService.getPaidAndTotalAmountByContactId(type1, 
						Long.parseLong(session.getAttribute("branchId").toString()),
						dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
						dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()),contactId);
				
			} else if(type.equals(Constant.CONTACT_SUPPLIER)) {
				
				type1.add(Constant.PURCHASE_BILL);
				
				totalAndPaidAmount=purchaseService.getPaidAndTotalAmountByContactId(type1, 
						Long.parseLong(session.getAttribute("branchId").toString()),
						dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
						dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()),contactId);
			}
			
			view.addAttribute("totalAndPaidAmount", totalAndPaidAmount);
		}
		
		return "contact/contact-view";
	}
	
	@GetMapping("{id}/edit")
	public String contactEdit(@PathVariable("id") long contactId, @PathVariable("type") String type, HttpSession session, Model view) throws NumberFormatException, ParseException {
		
		if(type.equals(Constant.CONTACT_CUSTOMER)) {
			view.addAttribute("displayContactType","Customer");
			view.addAttribute("type","customers");
		} else if(type.equals(Constant.CONTACT_SUPPLIER))  {
			view.addAttribute("displayContactType","Supplier");
			view.addAttribute("type","suppliers");
		} else if(type.equals(Constant.CONTACT_AGENT))  {
			view.addAttribute("displayContactType","Agent");
			view.addAttribute("type","agent");
		} else if(type.equals(Constant.CONTACT_TRANSPORT))  {
			view.addAttribute("displayContactType","Transport");
			view.addAttribute("type","transport");
		} else {
			return "404";
		}
		
		ContactVo contactVo = contactService.findByContactIdAndBranchIdAndType(contactId, Long.parseLong(session.getAttribute("branchId").toString()), type);
		
		contactVo.getContactAddressVos().removeIf( c -> c.getIsDeleted()==1);
		
		view.addAttribute("contactVo",contactVo);
		
		/*Customer Intrested Product */
		if(contactVo.getInterestedProductIds() != null && !contactVo.getInterestedProductIds().equals("")) {
			List<Long> productVariantIds = (List<Long>) Arrays.asList(contactVo.getInterestedProductIds().split("\\s*,\\s*")).stream().map(Long::parseLong).collect(Collectors.toList());
		
			view.addAttribute("productVariantVos", productService.findProductVariantByIdIn(productVariantIds));
		}
		
		/*Agent dropdown in customers*/
		view.addAttribute("contactAgentList", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_AGENT));
		
		return "contact/contact-edit";
	}
	
	@PostMapping("{id}/address/changedefault/{contactAddressId}")
	@ResponseBody
	public String changeDefaultAddress(@PathVariable("id") long contactId,@PathVariable("contactAddressId") long contactAddressId, @PathVariable("type") String type, HttpSession session) throws NumberFormatException, ParseException {
		
		contactService.changeDefaultAddress(contactId, contactAddressId);
		
		return "success";
	}
	
	@RequestMapping("/{id}/delete")
    public String deleteContact(@PathVariable long id,@PathVariable(value="type")String type, HttpSession session){
		
		contactService.deleteContact(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		if(type.equals(Constant.CONTACT_CUSTOMER) || type.equals(Constant.CONTACT_SUPPLIER)) {
			AccountCustomVo accountCustomVo = contactService.findAccountCustomVoByContactId(id);
			accountService.deleteAccountCustom(accountCustomVo.getAccountCustomId(), Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		}
		
		return  "redirect:/contact/"+type;
    }
	
	@RequestMapping("/{id}/json")
	@ResponseBody
    public ContactVo contactViewJSON(@PathVariable(value="id") long contactId, @PathVariable(value="type") String type, HttpSession session){
		
		ContactVo contactVo = contactService.findByContactIdAndBranchIdAndType(contactId, Long.parseLong(session.getAttribute("branchId").toString()), type);
		
    	contactVo.getContactAddressVos().removeIf( c -> c.getIsDeleted()==1);
    	Collections.sort(contactVo.getContactAddressVos(), Comparator.comparingInt(ContactAddressVo::getIsDefault).reversed());
    	
		contactVo.getContactAddressVos().forEach(p -> {
			try {
				p.setCountriesName(locationService.findCountriesNameByCountriesCode(p.getCountriesCode()));
			} catch (Exception e) {}
			
			try {
				p.setStateName(locationService.findStateNameByStateCode(p.getStateCode()));
			} catch (Exception e) {}
			
			try {
				p.setCityName(locationService.findCityNameByCityCode(p.getCityCode()));
			} catch (Exception e) {}
			
			p.setContactVo(null);
		});
    	contactVo.setContactOtherVos(null);
    	/*return json.use(JsonView.with(contactVos)
    			.onClass(ContactVo.class, Match.match().exclude("contactAddressVos.contact")))
    			.returnValue().get(0);*/
		contactVo.setAccountCustomVo(null);
		
		return contactVo;
	}
	
	@GetMapping("{id}/pdf")
	public void contactPDF(@PathVariable("id") long contactId, @PathVariable ("type") String type, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
		ContactVo contactVo=contactService.findByContactIdAndBranchIdAndType(contactId, Long.parseLong(session.getAttribute("branchId").toString()),type);
		jasperParameter = new HashMap();
		jasperParameter.put("contact_id",contactId);
		jasperParameter.put("company_id",Long.parseLong(session.getAttribute("companyId").toString()));
		jasperParameter.put("contact_type", type);
		jasperParameter.put("path", request.getServletContext().getRealPath("/") + "report" + System.getProperty("file.separator"));
		
		JasperExporter jasperExporter = new JasperExporter(); 
		try 	
		{			
				jasperExporter.jasperExporterPDF(jasperParameter,
						request.getServletContext().getRealPath("/") + "report/contact"
								+ System.getProperty("file.separator") +"/contact.jrxml","contact-"+contactId+".pdf", response);					
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
	
	@RequestMapping(value="/upload/excel")
	@ResponseBody
	public String  ImportCustomertodb(@PathVariable ("type") String type, HttpSession session,HttpServletRequest request) throws IOException 
	{
		
		/*File fb2 = ImageResize.convert(file);
		System.out.println(fb2.getName());
		System.out.println(fb2.getAbsolutePath());
		String filepath = fb2.getAbsolutePath();*/
		
		File fb = null;
		
		if(type.equals(Constant.CONTACT_SUPPLIER)) {
			fb=new File(request.getServletContext().getRealPath("/") + "import-data" + System.getProperty("file.separator")+"Supplier.xlsx");
		} else {
			fb=new File(request.getServletContext().getRealPath("/") + "import-data" + System.getProperty("file.separator")+"Transport.xlsx");
		}
		
		InputStream in=new FileInputStream(fb);
		
		//Create Workbook instance holding reference to .xlsx file
        XSSFWorkbook workbook = new XSSFWorkbook(in);        
        //Get first/desired sheet from the workbook
        XSSFSheet sheet = workbook.getSheetAt(0);
        
        //Iterate through each rows one by one
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next();
        rowIterator.next();
        while(rowIterator.hasNext())
        {
        	Row row=rowIterator.next();
            //For each row, iterate through all the columns
            Iterator<Cell> cellIterator = row.cellIterator();
       
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				// Check the cell type and format accordingly
				if (cell.getColumnIndex() != 2 && cell.getColumnIndex() != 3) {
					cell.setCellType(Cell.CELL_TYPE_STRING);
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_BOOLEAN:
						System.out.println("boolean===>>>" + cell.getBooleanCellValue() + "\t");
						break;
					case Cell.CELL_TYPE_NUMERIC:

						break;
					case Cell.CELL_TYPE_STRING:

						// list.add(cell.getStringCellValue().trim());
						break;

					}
				}
			}
            
            DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD");
            
            ContactVo contactVo = new ContactVo();
            
            {
            	contactVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
            	contactVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
            	contactVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
            	contactVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
            	contactVo.setModifiedOn(CurrentDateTime.getCurrentDate());
            	contactVo.setCreatedOn(CurrentDateTime.getCurrentDate());
            	contactVo.setType(type);
            	
            	try {
            		contactVo.setOwnerName(row.getCell(0).getStringCellValue().trim());
            	} catch (Exception e) {
            		contactVo.setOwnerName("");
            	}
        	   
            	try {
        		   contactVo.setOwnerMobileno(row.getCell(1).getStringCellValue().trim());
            	} catch (Exception e) {
        		   contactVo.setOwnerMobileno("");
            	}
            	
            	try {
            		contactVo.setDateOfBirth(row.getCell(2).getDateCellValue());
         	   	} catch (Exception e) {
         		   //e.printStackTrace();
         	   	}
            	
            	try {
         		   contactVo.setConcernPersonName(row.getCell(4).getStringCellValue().trim());
             	} catch (Exception e) {
             		contactVo.setConcernPersonName("");
             	}
            	
            	try {
            		contactVo.setConcernPersonMobileno(row.getCell(5).getStringCellValue().trim());
              	} catch (Exception e) {
              		contactVo.setConcernPersonMobileno("");
              	}
            	
            	try {
           		   contactVo.setCompanyName(row.getCell(6).getStringCellValue().trim());
               	} catch (Exception e) {
               		contactVo.setCompanyName("");
               	}
            	
            	try {
            		contactVo.setCompanyMobileno(row.getCell(7).getStringCellValue().trim());
            	} catch (Exception e) {
            		contactVo.setCompanyMobileno("");
            	}
            	
            	try {
         		   contactVo.setCompanyEmail(row.getCell(8).getStringCellValue().trim());
             	} catch (Exception e) {
             		contactVo.setCompanyEmail("");
             	}
            	
            	try {
          		   contactVo.setCompanyTelephone(row.getCell(9).getStringCellValue().trim());
              	} catch (Exception e) {
              		contactVo.setCompanyTelephone("");
              	}
            	
            	try {
            		if(type.equals(Constant.CONTACT_SUPPLIER)) {
            			contactVo.setGstType("Registered");
            		} else {
            			contactVo.setGstType("UnRegistered");
            		}
            		
            		//contactVo.setGstType(row.getCell(10).getStringCellValue().trim());
               	} catch (Exception e) {
               		contactVo.setGstType("UnRegistered");
               	}
            	
            	try {
           		   contactVo.setGstin(row.getCell(11).getStringCellValue().trim());
               	} catch (Exception e) {
               		contactVo.setGstin("");
               	}
            	
            	try {
        		   contactVo.setPanNo(row.getCell(12).getStringCellValue().trim());
            	} catch (Exception e) {
            		contactVo.setPanNo("");
            	}
            	
            	ContactAddressVo contactAddressVo = new ContactAddressVo();
            	
            	contactAddressVo.setCompanyName(contactVo.getCompanyName());
            	contactAddressVo.setIsDefault(1);
            	contactAddressVo.setContactVo(contactVo);
            	if(type.equals(Constant.CONTACT_SUPPLIER)) {
            		
            		contactVo.setPaymentType("Credit");
            		contactVo.setCreditDays(0);
            		contactVo.setMaximumCreditLimit(0);
            		
            		try {
    					contactVo.setBankName(row.getCell(16).getStringCellValue().trim());
    				} catch (Exception e) {
    					contactVo.setBankName("");
    				}
            		
            		try {
    					contactVo.setBankBranch(row.getCell(17).getStringCellValue().trim());
    				} catch (Exception e) {
    					contactVo.setBankBranch("");
    				}
            		
            		try {
    					contactVo.setBankAccountNo(row.getCell(18).getStringCellValue().trim());
    				} catch (Exception e) {
    					contactVo.setBankAccountNo("");
    				}
            		
            		try {
    					contactVo.setBankIfsc(row.getCell(18).getStringCellValue().trim());
    				} catch (Exception e) {
    					contactVo.setBankIfsc("");
    				}
            		
            		try {
    					contactAddressVo.setAddressLine1(row.getCell(20).getStringCellValue().trim());
    				} catch (Exception e) {
    					contactAddressVo.setAddressLine1("");
    				}
    				
    				try {
    					contactAddressVo.setAddressLine2("");
    				} catch (Exception e) {
    					contactAddressVo.setAddressLine2("");
    				}
    				   
    				try {
    					contactAddressVo.setPinCode(row.getCell(23).getStringCellValue().trim());
    				} catch (Exception e) {
    				
    				}
       
    				try {
    					contactAddressVo.setCityCode(row.getCell(24).getStringCellValue().trim());
    				} catch (Exception e) {
    				
    				}
       
    				try {
    					contactAddressVo.setStateCode(row.getCell(25).getStringCellValue().trim());
    				} catch (Exception e) {
    				
    				}
    				
    				contactAddressVo.setCountriesCode("IN");
    				
    				AccountCustomVo accountCustomVo = new AccountCustomVo();
    				
	    			accountCustomVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
	    	    	accountCustomVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
	    	    	accountCustomVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
	    	    	accountCustomVo.setCreatedOn(CurrentDateTime.getCurrentDate());
	    	    	
	    	    	AccountGroupVo accountGroupVo = new AccountGroupVo();
	    	    	
	    	    	accountGroupVo.setAccountGroupId(Constant.ACCOUNT_GROUP_SUNDRY_CREDITORS);
    				accountCustomVo.setAccounType(Constant.CONTACT_SUPPLIER);
    				
	    	    	accountCustomVo.setGroup(accountGroupVo);
    	    		
    	    		accountCustomVo.setAccountName(contactVo.getCompanyName());
    	        	accountCustomVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
    	        	accountCustomVo.setModifiedOn(CurrentDateTime.getCurrentDate());
    	        	
    	        	accountService.insertAccount(accountCustomVo);
    	        	
    	        	contactVo.setAccountCustomVo(accountCustomVo);
    	        	
            	} else {
            		
            		try {
              		   contactVo.setWorkingTime(row.getCell(13).getStringCellValue().trim());
                  	} catch (Exception e) {
                  		contactVo.setWorkingTime("");
                  	}
                 	
                 	try {
              		   contactVo.setServiceAvailable(row.getCell(14).getStringCellValue().trim());
                  	} catch (Exception e) {
                  		contactVo.setServiceAvailable("");
                  	}
                 	
                 	try {
    					contactAddressVo.setAddressLine1(row.getCell(16).getStringCellValue().trim());
    				} catch (Exception e) {
    					contactAddressVo.setAddressLine1("");
    				}
    				
    				try {
    					contactAddressVo.setAddressLine2(row.getCell(17).getStringCellValue().trim());
    				} catch (Exception e) {
    					contactAddressVo.setAddressLine2("");
    				}
    				   
    				try {
    					contactAddressVo.setPinCode(row.getCell(18).getStringCellValue().trim());
    				} catch (Exception e) {
    				
    				}
       
    				try {
    					contactAddressVo.setCityCode(row.getCell(19).getStringCellValue().trim());
    				} catch (Exception e) {
    				
    				}
       
    				try {
    					contactAddressVo.setStateCode(row.getCell(20).getStringCellValue().trim());
    				} catch (Exception e) {
    				
    				}
    				
    				contactAddressVo.setCountriesCode("IN");
            	}
            	
				List<ContactAddressVo> contactAddressVos = new ArrayList<ContactAddressVo>();
				contactAddressVos.add(contactAddressVo);
				
				contactVo.setContactAddressVos(contactAddressVos);
				
				contactService.saveContact(contactVo);
        	  
           }
          
		}
        in.close();
        workbook.cloneSheet(0);
        workbook.close();
		return "success";
	}
	
	@RequestMapping("/sendsms")	
	@ResponseBody
    public String  sendsms(@RequestParam("phoneno") String phoneNo, @RequestParam("message") String message, HttpSession session){
    	
		if(String.valueOf(phoneNo.charAt(phoneNo.length() - 1)).equals(",")) {
			phoneNo = phoneNo.substring(0, phoneNo.length() -1);	
		}
		
		site2sms.sendMessage(phoneNo, message, "VSYERP");
		
		return "Sucess";	
    }
}
