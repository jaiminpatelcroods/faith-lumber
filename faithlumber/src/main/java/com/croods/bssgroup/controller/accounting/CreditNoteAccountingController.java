package com.croods.bssgroup.controller.accounting;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.service.account.AccountService;
import com.croods.bssgroup.service.account.CreditNoteAccountingService;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.util.JasperExporter;
import com.croods.bssgroup.util.NumberToWord;
import com.croods.bssgroup.vo.account.CreditNoteAccountingVo;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@Controller
@RequestMapping("account/creditnote")
public class CreditNoteAccountingController {

	@Autowired
	CreditNoteAccountingService creditNoteAccountingService;
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	PrefixService prefixService;
	
	JasperReport jasperReport;
	JasperPrint jasperPrint;
	OutputStream outputStream;
	HashMap jasperParameter;
	
	@GetMapping("")
	public ModelAndView creditNoteAccounting(HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("accounting/credit-note/credit-note");
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		view.addObject("creditNoteAccountingVos",creditNoteAccountingService.findByBranchIdAndTransactionDateBetween(
				Long.parseLong(session.getAttribute("branchId").toString()), 
				dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
				dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())
			));
				
		return view;
	}
	
	@GetMapping("new")
	public ModelAndView creditNoteAccountingNew(HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("accounting/credit-note/credit-note-new");
		String fromTypes = Constant.CONTACT_CUSTOMER + "," + Constant.CONTACT_SUPPLIER + "," + Constant.ACCOUNT_CUSTOM;
		String toTypes = Constant.CONTACT_CUSTOMER + "," + Constant.CONTACT_SUPPLIER;
		
		view.addObject("fromAccountCustomVos", accountService.findByBranchIdAndAccounTypes(
				Long.parseLong(session.getAttribute("branchId").toString()), Arrays.asList(fromTypes.split(","))));
		view.addObject("toAccountCustomVos", accountService.findByBranchIdAndAccounTypes(
				Long.parseLong(session.getAttribute("branchId").toString()), Arrays.asList(toTypes.split(","))));
		return view;
	}
	
	@PostMapping("save")
	public String creditNoteAccountingSave(@RequestParam Map<String,String> allRequestParams, HttpSession session, @ModelAttribute("creditNoteAccountingVo") CreditNoteAccountingVo creditNoteAccountingVo) throws NumberFormatException, ParseException {
		
		creditNoteAccountingVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		creditNoteAccountingVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		creditNoteAccountingVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		creditNoteAccountingVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		
		if(creditNoteAccountingVo.getCreditNoteAccountId() == 0) {
			
			long newVoucherNo = creditNoteAccountingService.findMaxVoucherNo(Constant.ACCOUNTING_CREDIT_NOTE, 
					Long.parseLong(session.getAttribute("companyId").toString()), 
					Long.parseLong(session.getAttribute("branchId").toString()), 
					Long.parseLong(session.getAttribute("userId").toString()), "CDT");
			
			creditNoteAccountingVo.setPrefix(prefixService.findByPrefixTypeAndBranchId(Constant.ACCOUNTING_CREDIT_NOTE,
					Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
			creditNoteAccountingVo.setVoucherNo(newVoucherNo);
			
			creditNoteAccountingVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			creditNoteAccountingVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		}
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		try {
			creditNoteAccountingVo.setTransactionDate(dateFormat.parse(allRequestParams.get("transactionDate")));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		creditNoteAccountingVo = creditNoteAccountingService.save(creditNoteAccountingVo);
		
		creditNoteAccountingService.saveTransation(creditNoteAccountingVo);
		
		return "redirect:/account/creditnote/"+creditNoteAccountingVo.getCreditNoteAccountId();
	}
	
	@GetMapping("{id}")
	public ModelAndView creditNoteAccountingView(@PathVariable("id") long creditNoteAccountId, HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("accounting/credit-note/credit-note-view");
		
		view.addObject("creditNoteAccountingVo", creditNoteAccountingService.findByCreditNoteAccountIdAndBranchId(creditNoteAccountId, Long.parseLong(session.getAttribute("branchId").toString())));
		
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView creditNoteAccountingEdit(@PathVariable("id") long creditNoteAccountId, HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("accounting/credit-note/credit-note-edit");
		
		view.addObject("creditNoteAccountingVo", creditNoteAccountingService.findByCreditNoteAccountIdAndBranchId(creditNoteAccountId, Long.parseLong(session.getAttribute("branchId").toString())));
		
		String fromTypes = Constant.CONTACT_CUSTOMER + "," + Constant.CONTACT_SUPPLIER + "," + Constant.ACCOUNT_CUSTOM;
		String toTypes = Constant.CONTACT_CUSTOMER + "," + Constant.CONTACT_SUPPLIER;
		
		view.addObject("fromAccountCustomVos", accountService.findByBranchIdAndAccounTypes(
				Long.parseLong(session.getAttribute("branchId").toString()), Arrays.asList(fromTypes.split(","))));
		view.addObject("toAccountCustomVos", accountService.findByBranchIdAndAccounTypes(
				Long.parseLong(session.getAttribute("branchId").toString()), Arrays.asList(toTypes.split(","))));
		
		return view;
	}
	
	@GetMapping("{id}/delete")
	public String creditNoteAccountingDelete(@PathVariable("id") long creditNoteAccountId, HttpSession session) throws NumberFormatException, ParseException {
		
		creditNoteAccountingService.delete(creditNoteAccountId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "redirect:/account/creditnote";
	}
	
	@GetMapping("{id}/pdf")
	public void creditNotePDF(@PathVariable("id") long creditNoteAccountId, HttpSession session, HttpServletRequest request, HttpServletResponse response) 
	{
		CreditNoteAccountingVo creditNoteAccountingVo=creditNoteAccountingService.findByCreditNoteAccountIdAndBranchId(creditNoteAccountId, Long.parseLong(session.getAttribute("branchId").toString()));
		jasperParameter = new HashMap();
		jasperParameter.put("account_id",creditNoteAccountId);
		jasperParameter.put("user_front_id",Long.parseLong(session.getAttribute("branchId").toString()));
		jasperParameter.put("display_title","Credit Note");
		jasperParameter.put("path", request.getServletContext().getRealPath("/") + "report" + System.getProperty("file.separator"));
		jasperParameter.put("amount_in_word",new NumberToWord().getNumberToWord(creditNoteAccountingVo.getAmount()));
		JasperExporter jasperExporter = new JasperExporter(); 
		try 
		{			
				jasperExporter.jasperExporterPDF(jasperParameter,
						request.getServletContext().getRealPath("/") + "report/account"
								+ System.getProperty("file.separator") +"/creditnote.jrxml","Certificate-"+creditNoteAccountId+".pdf", response);					
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
