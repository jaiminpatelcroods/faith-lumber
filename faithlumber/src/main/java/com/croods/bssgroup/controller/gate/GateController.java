package com.croods.bssgroup.controller.gate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.service.contact.ContactService;
import com.croods.bssgroup.service.gate.GateService;
import com.croods.bssgroup.service.sales.SalesService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.util.site2sms;
import com.croods.bssgroup.vo.contact.ContactVo;
import com.croods.bssgroup.vo.delivery.DeliveryVo;
import com.croods.bssgroup.vo.gate.GateVo;
import com.croods.bssgroup.vo.leadsource.LeadSourceVo;
import com.croods.bssgroup.vo.production.ProductionVo;
import com.croods.bssgroup.vo.sales.SalesVo;

@Controller
@RequestMapping("gate")
public class GateController {
	
	@Autowired
	ContactService contactService;
	
	@Autowired
	SalesService salesService;
	
	@Autowired
	GateService gateService;
	
	@GetMapping("")
	public ModelAndView complainNew(HttpSession session) throws NumberFormatException, ParseException {
		
		ModelAndView view = new ModelAndView("gate/gate");
		view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_TRANSPORT));
		
		List<String> status =new ArrayList<>();
		status.add("In Progress");
		status.add("Production Completed");
		
		List<SalesVo> salesVos = salesService.findByBranchIdAndTypeAndStatus(Long.parseLong(session.getAttribute("branchId").toString()), Constant.SALES_ORDER, status);
		
		salesVos.stream().forEach(x -> {
			  x.getContactVo().setContactAddressVos(null);
			  x.getContactVo().setContactOtherVos(null);
			  x.setContactAgentVo(null);
			  x.setContactTransportVo(null);
			  x.setSalesItemVos(null);
			  x.setSalesAdditionalChargeVos(null);
			  x.setSalesVo(null);
			  x.setAssignedEmployee(null);
			  x.setPaymentTermsVo(null);
		});
		view.addObject("salesVos",salesVos);
		
		return view;
	}
	
	@PostMapping("/save")
	@ResponseBody
	public String saveGateIn(@ModelAttribute("gateVo") GateVo gateVo, HttpSession session)
 	{
		gateVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		gateVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		gateVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
		gateVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
	
		gateVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		gateVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		gateService.save(gateVo);
		
		return "success";
 	}
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<GateVo> gateDatatable(@Valid DataTablesInput input,@RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<GateVo>  specification =new Specification<GateVo>() {
			
			@Override
			public Predicate toPredicate(Root<GateVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), Constant.SALES_ORDER));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				query.orderBy(criteriaBuilder.desc(root.get("gateDate")),criteriaBuilder.desc(root.get("gateId")));
				
				try {
					predicates.add(criteriaBuilder.between(root.get("gateDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<GateVo> dataTablesOutput=gateService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> {
			
			p.getContactTransportVo().setAccountCustomVo(null);
			p.getContactTransportVo().setContactAddressVos(null);
			p.getContactTransportVo().setContactOtherVos(null);
			
			p.getSalesVo().setAssignedEmployee(null);
			p.getSalesVo().setContactAgentVo(null);
			p.getSalesVo().setSalesItemVos(null);
			p.getSalesVo().setSalesAdditionalChargeVos(null); 
			p.getSalesVo().setPaymentTermsVo(null);
			p.getSalesVo().setSalesVo(null);
			p.getSalesVo().setContactTransportVo(null);
			p.getSalesVo().setContactVo(null);
			
			
		});
		return dataTablesOutput;
	}
	
	@PostMapping("update")
	@ResponseBody
	public String updateGate(@RequestParam(value="gateId") long gateId, @RequestParam(value="salesId") long salesId, @RequestParam(value="contactId") long contactId, @RequestParam(value="gateDate") String gateDate, @RequestParam(value="vehicleno") String vehicleno, @RequestParam (value="remark") String remark, HttpSession session) throws ParseException
	{
		ContactVo contactVo = contactService.findByContactId(contactId);
		SalesVo salesVo=salesService.findBySalesId(salesId);
		GateVo gateVo=gateService.findByGateId(gateId);
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		gateVo.setGateDate(dateFormat.parse(gateDate));
		gateVo.setVehicleno(vehicleno);
		gateVo.setRemark(remark);
		gateVo.setContactTransportVo(contactVo);
		
		gateVo.setSalesVo(salesVo);
		
		gateVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		gateVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		gateVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		gateVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		gateService.save(gateVo);
		
		return "success";
	}
	
	@PostMapping("delete")
	@ResponseBody
	public String deleteGate(@RequestParam(value="id") long id, HttpSession session)
	{
		gateService.delete(id, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
		return "Sucess";
	}
	//for message shown in modal
	@RequestMapping("{id}/sendsms/gatein")
	@ResponseBody
	public  StringBuffer sendsms5(@PathVariable("id") long gateId, HttpSession session){
		
		GateVo gateVo=gateService.findByGateId(gateId);
		StringBuffer message5= new StringBuffer();
		String customername = gateVo.getSalesVo().getContactVo().getCompanyName();
	
		message5.append("Dear"+" "+customername +","+"transportation arrangement has been finalized."+"\n");
		message5.append("Pls find the details:"+"\n");
		message5.append("Transport Name: "+gateVo.getContactTransportVo().getCompanyName()+"\n");
		message5.append("Transport Contact:"+gateVo.getContactTransportVo().getCompanyMobileno()+"\n");
		message5.append("Truck No:"+gateVo.getVehicleno()+"\n");
		message5.append("Guarantee in Tons:"+"\n");
		message5.append("Driver No:"+"\n");
		message5.append("ETD:"+"\n");
		message5.append("ETA:"+"\n");
		message5.append("Guarantee in Tons:"+"\n");
		message5.append("Qty:"+"\n");
		System.err.println(message5);
		return message5;
		
	}
	//for message shown in modal edit and send
	@RequestMapping("{id}/sendsms")
	@ResponseBody
	public String sendsms1(@PathVariable ("id") long gateId,@RequestParam("phoneno") String phoneno, @RequestParam("message") String message, HttpSession session){
		GateVo gateVo=gateService.findByGateId(gateId);
		System.out.println("phoneNo" +phoneno);
		site2sms.sendMessage(gateVo.getSalesVo().getContactVo().getCompanyMobileno(), message, "VSYERP");
		System.err.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+message);
		return "Sucess";	
	}
}
