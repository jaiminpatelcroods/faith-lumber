package com.croods.bssgroup.controller.userfront;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.service.financial.FinancialService;
import com.croods.bssgroup.service.userfront.UserFrontService;
import com.croods.bssgroup.vo.userfront.UserFrontVo;

@Controller
public class UserFrontController {

	@Autowired
	UserFrontService userFrontService;
	
	@Autowired
	FinancialService financialService;
	
	@GetMapping("profile/edit")
	public ModelAndView profileEdit(HttpSession session) {
		ModelAndView view = new ModelAndView("profile/profile-edit");
		view.addObject("financialYearVos", financialService.findAllFinancialYear());
		view.addObject("financialMonthVos", financialService.findAllFinancialMonth());
		view.addObject("userFrontVo", userFrontService.findByUserFrontId(Long.parseLong(session.getAttribute("userId").toString())));
		return view;
	}
	
	@PostMapping("profile/update")
	public String profileUpdate(HttpSession session, @ModelAttribute("userFrontVo") UserFrontVo userFrontVo, BindingResult bindingResult) {
		
		UserFrontVo userFrontVo2 = new UserFrontVo();
		
		userFrontVo2 = userFrontService.findByUserFrontId(Long.parseLong(session.getAttribute("userId").toString()));
		
		userFrontVo.setUserFrontId(Long.parseLong(session.getAttribute("userId").toString()));
		userFrontVo.setCreatedOn(userFrontVo2.getCreatedOn());
		userFrontVo.setIsDeleted(userFrontVo2.getIsDeleted());
		userFrontVo.setPassword(userFrontVo2.getPassword());
		userFrontVo.setRoles(userFrontVo2.getRoles());
		userFrontVo.setSenderId(userFrontVo2.getSenderId());
		userFrontVo.setStatus(userFrontVo2.getStatus());
		
		if(userFrontVo2.getUserFrontVo() != null) {
			userFrontVo.setUserFrontVo(userFrontVo2.getUserFrontVo());
		}
		
		userFrontService.save(userFrontVo);
		
		return "redirect:/setting";
	}
}
