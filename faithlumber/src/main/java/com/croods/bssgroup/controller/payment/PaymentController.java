package com.croods.bssgroup.controller.payment;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.service.contact.ContactService;
import com.croods.bssgroup.service.payment.PaymentService;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.service.purchase.PurchaseService;
import com.croods.bssgroup.service.transaction.TransactionService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.util.JasperExporter;
import com.croods.bssgroup.util.NumberToWord;
import com.croods.bssgroup.vo.payment.PaymentBillVo;
import com.croods.bssgroup.vo.payment.PaymentVo;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

@Controller
@RequestMapping("/payment")
public class PaymentController {

	@Autowired
	PaymentService paymentService;

	@Autowired
	ContactService contactService;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	PurchaseService purchaseService;
	
	@Autowired
	TransactionService transactionService;
	
	JasperReport jasperReport;
	JasperPrint jasperPrint;
	OutputStream outputStream;
	HashMap jasperParameter;
	
	@GetMapping("")
	public ModelAndView payment(HttpSession session) {
		
		ModelAndView view = new ModelAndView("payment/payment");
		
		return view;
	}
	
	@PostMapping("datatable")
	@ResponseBody
	public DataTablesOutput<PaymentVo> paymentDatatable(@Valid DataTablesInput input, @RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<PaymentVo>  specification =new Specification<PaymentVo>() {
			
			@Override
			public Predicate toPredicate(Root<PaymentVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				query.orderBy(criteriaBuilder.desc(root.get("paymentDate")));
				query.orderBy(criteriaBuilder.desc(root.get("paymentId")));
				try {
					predicates.add(criteriaBuilder.between(root.get("paymentDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<PaymentVo> dataTablesOutput=paymentService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> {
			p.setPaymentBillVos(null);
			p.setBankVo(null);
			p.getContactVo().setContactAddressVos(null);
			p.getContactVo().setContactOtherVos(null);
			p.getContactVo().setAccountCustomVo(null);
		});
		return dataTablesOutput;
	}
	
	@PostMapping("contact/{contactId}/datatable")
	@ResponseBody
	public DataTablesOutput<PaymentVo> paymentByContactDatatable(@Valid DataTablesInput input, @PathVariable(value="contactId") long contactId, @RequestParam Map<String, String> allRequestParams, HttpSession session) {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		Specification<PaymentVo>  specification =new Specification<PaymentVo>() {
			
			@Override
			public Predicate toPredicate(Root<PaymentVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				//predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				predicates.add(criteriaBuilder.equal(root.get("contactVo").get("contactId"), contactId));
				query.orderBy(criteriaBuilder.desc(root.get("paymentDate")));
				query.orderBy(criteriaBuilder.desc(root.get("paymentId")));
				try {
					predicates.add(criteriaBuilder.between(root.get("paymentDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<PaymentVo> dataTablesOutput=paymentService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach(p -> {
			p.setPaymentBillVos(null);
			p.setBankVo(null);
			p.setContactVo(null);
		});
		return dataTablesOutput;
	}
	
	@RequestMapping("/new")
	public ModelAndView newPayment(HttpSession session, @RequestParam(value = "contactId", defaultValue="0") String contactId) {
		
		ModelAndView view = new ModelAndView("payment/payment-new");
		
		view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
		
		if(!contactId.equals("0")) {
			view.addObject("contactId", contactId);
		}
		
		return view;
	}
	
	@PostMapping("/save")
	public String savePayment(@RequestParam Map<String,String> allRequestParams,@ModelAttribute("PaymentVo") PaymentVo paymentVo, HttpSession session ) {
		
		double totalKasar=0;
	
		if(paymentVo.getPaymentBillVos()!=null) {
			paymentVo.getPaymentBillVos().removeIf(pay -> pay.getPurchaseVo() == null);
			paymentVo.getPaymentBillVos().forEach(pay->pay.setPaymentVo(paymentVo));
			
			totalKasar = paymentVo.getPaymentBillVos().stream().mapToDouble(pay->pay.getKasar()).sum();
			
			for(PaymentBillVo BillVo:paymentVo.getPaymentBillVos()){	
				purchaseService.updateOldPaymentMinus(BillVo.getPurchaseVo().getPurchaseId(),BillVo.getOldPyament()+BillVo.getOldKasar());
			}
		}
		
		if(paymentVo.getPaymentId()==0l) {
			paymentVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			paymentVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			paymentVo.setPaymentNo(paymentService.findMaxPaymentNo(
					Long.parseLong(session.getAttribute("companyId").toString()),
					Long.parseLong(session.getAttribute("branchId").toString()),
					Long.parseLong(session.getAttribute("userId").toString()),Constant.PAYMENT, "PAY"));
			paymentVo.setPrefix(prefixService.findByPrefixTypeAndBranchId(Constant.PAYMENT, Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
		}
		
		paymentVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		paymentVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		paymentVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		paymentVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		
		PaymentVo paymentVo2=paymentService.save(paymentVo);
		
		if(allRequestParams.get("deleteBillIds") != null && !allRequestParams.get("deleteBillIds").equals("")) {
			
			String deletedBillIds = allRequestParams.get("deleteBillIds").substring(0, allRequestParams.get("deleteBillIds").length()-1);
			
			List<Long> billIds= Arrays.asList(deletedBillIds.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());	
			paymentService.deletePaymentBillVoByIn(billIds);
			
		}
		
		if(paymentVo2.getPaymentBillVos() != null)
			for(PaymentBillVo paymentBillVo:paymentVo2.getPaymentBillVos()){	
				purchaseService.addPaidAmountByPurchaseId(paymentBillVo.getPurchaseVo().getPurchaseId(),paymentBillVo.getPayment()+paymentBillVo.getKasar(), Long.parseLong(session.getAttribute("userId").toString()),CurrentDateTime.getCurrentDate());
		}
		
		paymentService.transation(paymentService.findByPaymentIdAndBranchId(paymentVo2.getPaymentId(), Long.parseLong(session.getAttribute("branchId").toString())),totalKasar);
		
		return "redirect:/payment/"+paymentVo2.getPaymentId();
	}
	
	@GetMapping("/{id}")
	public ModelAndView paymentView(@PathVariable(value="id") long paymentId, HttpSession session) {
		ModelAndView view=new ModelAndView("payment/payment-view");
		
		view.addObject("paymentVo", paymentService.findByPaymentIdAndBranchId(paymentId,Long.parseLong(session.getAttribute("branchId").toString())));
		
		return view;
	}
	
	@GetMapping("/{id}/edit")
	public ModelAndView paymentEdit(@PathVariable(value="id") long paymentId, HttpSession session) {
		ModelAndView view=new ModelAndView("payment/payment-edit");
		
		view.addObject("paymentVo", paymentService.findByPaymentIdAndBranchId(paymentId,Long.parseLong(session.getAttribute("branchId").toString())));
		view.addObject("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_SUPPLIER));
		return view;
	}
	
	@GetMapping("/{id}/delete")
	public String paymentDelete(@PathVariable(value = "id") long receiptId, HttpSession session)
			throws NumberFormatException, ParseException {
		
		PaymentVo paymentVo = paymentService.findByPaymentIdAndBranchId(receiptId,Long.parseLong(session.getAttribute("branchId").toString()));
		paymentVo.setAlterBy(Long.parseLong(session.getAttribute("branchId").toString()));
		paymentVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		paymentVo.setIsDeleted(1);
		
		paymentService.save(paymentVo);
		
		List<Long> l = new ArrayList<>();
		
		for(PaymentBillVo BillVo:paymentVo.getPaymentBillVos()){	
			purchaseService.updateOldPaymentMinus(BillVo.getPurchaseVo().getPurchaseId(),BillVo.getPayment()+BillVo.getKasar());
		}
		
		transactionService.deleteTransaction(paymentVo.getBranchId(),paymentVo.getPaymentId(), Constant.PAYMENT);
		
		return "redirect:/payment";
	}
	
	@GetMapping("{id}/pdf")
	public void paymentPDF(@PathVariable("id") long paymentId, HttpSession session, HttpServletRequest request, HttpServletResponse response) 
	{
		PaymentVo paymentVo=paymentService.findByPaymentIdAndBranchId(paymentId, Long.parseLong(session.getAttribute("branchId").toString()));
		jasperParameter = new HashMap();
		jasperParameter.put("payment_bill_id",paymentId);
		jasperParameter.put("user_front_id",Long.parseLong(session.getAttribute("branchId").toString()));
		jasperParameter.put("display_title","Payment");
		jasperParameter.put("path", request.getServletContext().getRealPath("/") + "report" + System.getProperty("file.separator"));
		jasperParameter.put("amount_in_word",new NumberToWord().getNumberToWord(paymentVo.getTotalPayment()));
		JasperExporter jasperExporter = new JasperExporter(); 
		
		System.out.println("dgffrjfdmk");
	try	{			
			jasperExporter.jasperExporterPDF(jasperParameter,
					request.getServletContext().getRealPath("/") + "report/payment"
							+ System.getProperty("file.separator") +"/payment.jrxml","Certificate-"+paymentId+".pdf", response);					
	
	}catch  (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
