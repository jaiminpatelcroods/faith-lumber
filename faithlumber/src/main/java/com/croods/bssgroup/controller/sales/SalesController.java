package com.croods.bssgroup.controller.sales;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amazonaws.services.route53domains.model.ContactType;
import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.service.additionalcharge.AdditionalChargeService;
import com.croods.bssgroup.service.contact.ContactService;
import com.croods.bssgroup.service.gate.GateService;
import com.croods.bssgroup.service.location.LocationService;
import com.croods.bssgroup.service.paymentterm.PaymentTermService;
import com.croods.bssgroup.service.prefix.PrefixService;
import com.croods.bssgroup.service.sales.SalesService;
import com.croods.bssgroup.service.termsandcondition.TermsAndConditionService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.util.JasperExporter;
import com.croods.bssgroup.util.NumberToWord;
import com.croods.bssgroup.util.site2sms;
import com.croods.bssgroup.vo.contact.ContactAddressVo;
import com.croods.bssgroup.vo.contact.ContactVo;
import com.croods.bssgroup.vo.gate.GateVo;
import com.croods.bssgroup.vo.location.CityVo;
import com.croods.bssgroup.vo.production.ProductionVo;
import com.croods.bssgroup.vo.sales.SalesAdditionalChargeVo;
import com.croods.bssgroup.vo.sales.SalesItemVo;
import com.croods.bssgroup.vo.sales.SalesVo;

import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;



@Controller
@RequestMapping("/sales/{type}")
public class SalesController {

	@Autowired
	ContactService contactService;
	
	@Autowired
	SalesService salesService;
	
	@Autowired
	PrefixService prefixService;
	
	@Autowired
	PaymentTermService paymentTermService;
	
	@Autowired
	LocationService locationService;

	@Autowired
	TermsAndConditionService termsAndConditionService;
	
	@Autowired
	AdditionalChargeService additionalChargeService;
	
	@Autowired
	GateService gateService;
	
	JasperReport jasperReport;
	JasperPrint jasperPrint;
	OutputStream outputStream;
	HashMap jasperParameter;
	JSONArray jsonArray;
	JSONObject jsonObject;
	
	@GetMapping("")
	public String salesList(HttpSession session, @PathVariable(value="type")String type, Model view) {
		
		view.addAttribute("type",type);
		
		if (type.equals(Constant.SALES_INVOICE)) {
			view.addAttribute("displayType", "Invoice");
		}  else if (type.equals(Constant.SALES_QUOTATION)) {
        	view.addAttribute("displayType", "Sales Quotation");
        } else if (type.equals(Constant.SALES_ORDER)) {
        	view.addAttribute("displayType", "Sales Order");
		} else if (type.equals(Constant.SALES_CREDIT_NOTE)) {
        	view.addAttribute("displayType", "Credit Note");
		} else {
        	return "404";
		}
		view.addAttribute("ContactList", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_CUSTOMER));
		return "sales/sales";
	}
	
	@RequestMapping("/datatable")
	@ResponseBody
	public DataTablesOutput<SalesVo> salesDatatable(@PathVariable String type, @Valid DataTablesInput input, @RequestParam Map<String,String> allRequestParams,HttpSession session) throws NumberFormatException, ParseException {
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		Specification<SalesVo>  specification =new Specification<SalesVo>() {
			
			@Override
			public Predicate toPredicate(Root<SalesVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				//predicates.add((Predicate) criteriaBuilder.desc(root.get("salesDate")));
				query.orderBy(criteriaBuilder.desc(root.get("salesDate")),criteriaBuilder.desc(root.get("salesId")));
				
				if(!allRequestParams.get("contactId").equals("0")) {
					ContactVo contactVo = new ContactVo();
					contactVo.setContactId(Long.parseLong(allRequestParams.get("contactId")));
					predicates.add(criteriaBuilder.equal(root.get("contactVo"), contactVo));
				}
				
				try {
					predicates.add(criteriaBuilder.between(root.get("salesDate"), dateFormat.parse(allRequestParams.get("fromDate")),
							dateFormat.parse(allRequestParams.get("toDate"))));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<SalesVo> dataTablesOutput=salesService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach( x -> {
			  x.getContactVo().setContactAddressVos(null);
			  x.getContactVo().setAccountCustomVo(null);
			  x.getContactVo().setContactOtherVos(null);
			  x.setContactAgentVo(null);
			  x.setContactTransportVo(null);
			  x.setSalesItemVos(null);
			  x.setSalesAdditionalChargeVos(null);
			  x.setSalesVo(null);
			  x.setAssignedEmployee(null);
			  x.setPaymentTermsVo(null);
			  
		  });
		 
		
		return dataTablesOutput;
	}
	
	@RequestMapping("contact/{contactId}/datatable")
	@ResponseBody
	public DataTablesOutput<SalesVo> salesByContactDatatable(@PathVariable(value="contactId") long contactId, @PathVariable String type, @Valid DataTablesInput input, @RequestParam Map<String,String> allRequestParams,HttpSession session) throws NumberFormatException, ParseException {
		
		
		long branchId = Long.parseLong(session.getAttribute("branchId").toString());
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		
		Specification<SalesVo>  specification =new Specification<SalesVo>() {
			
			@Override
			public Predicate toPredicate(Root<SalesVo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<Predicate>();
				predicates.add(criteriaBuilder.equal(root.get("type"), type));
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), 0));
				predicates.add(criteriaBuilder.equal(root.get("branchId"), branchId));
				predicates.add(criteriaBuilder.equal(root.get("contactVo").get("contactId"), contactId));
				query.orderBy(criteriaBuilder.desc(root.get("salesDate")),criteriaBuilder.desc(root.get("salesId")));
				
				try {
					predicates.add(criteriaBuilder.between(root.get("salesDate"), dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
							dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString())));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
			}
		};
		
		DataTablesOutput<SalesVo> dataTablesOutput=salesService.findAll(input, null,specification);
		
		dataTablesOutput.getData().forEach( x -> { 
			  x.setContactVo(null);
			  x.setContactAgentVo(null);
			  x.setContactTransportVo(null);
			  x.setSalesItemVos(null);
			  x.setSalesAdditionalChargeVos(null);
			  x.setSalesVo(null);
			  x.setAssignedEmployee(null);
		  });
		 
		
		return dataTablesOutput;
	}
	
	@RequestMapping("/new")
	public String newSales(HttpSession session, @PathVariable(value="type")String type, @RequestParam(value = "contactId", defaultValue="0") String contactId, @RequestParam(value = "parentId", defaultValue="0") String parentId, Model view) {
		
		String defaultPrefix = "";
		
		if (type.equals(Constant.SALES_INVOICE)) {
			view.addAttribute("displayType", "Invoice");
			defaultPrefix = "INV";
        } else if (type.equals(Constant.SALES_QUOTATION)) {
        	view.addAttribute("displayType", "Sales Quotation");
        	defaultPrefix = "QUO";
        } else if (type.equals(Constant.SALES_ORDER)) {
        	view.addAttribute("displayType", "Sales Order");
        	defaultPrefix = "ORD";
        } else if (type.equals(Constant.SALES_CREDIT_NOTE)) {
        	view.addAttribute("displayType", "Credit Note");
        	defaultPrefix = "CRD";
        } else {
        	return "404";
		}
		
		view.addAttribute("type",type);
		
		long newSalesNo = salesService.findMaxSalesNo(Long.parseLong(session.getAttribute("companyId").toString()),
				Long.parseLong(session.getAttribute("branchId").toString()), type, defaultPrefix,
				Long.parseLong(session.getAttribute("userId").toString()));
		view.addAttribute("newSalesNo", newSalesNo);
		
		
		view.addAttribute("prefix",prefixService.findByPrefixTypeAndBranchId(type, Long.parseLong(session.getAttribute("branchId").toString())).get(0).getPrefix());
		
		view.addAttribute("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_CUSTOMER));
		view.addAttribute("contactAgentVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_AGENT));
		view.addAttribute("contactTransportVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_TRANSPORT));
		view.addAttribute("termsAndConditionVos", termsAndConditionService.findByCompanyIdAndIsDefaultAndModules(Long.parseLong(session.getAttribute("companyId").toString()), 1, type));
		view.addAttribute("paymentTermVos", paymentTermService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		
		if(!parentId.equals("0")) {
			
			List<SalesItemVo> salesItemVos = salesService.findItemBySalesId(Long.parseLong(parentId));
			view.addAttribute("salesItemVos",salesItemVos);
			List<SalesAdditionalChargeVo> salesAdditionalChargeVos = salesService.findAdditionalChargeBySalesId(Long.parseLong(parentId));
			view.addAttribute("salesAdditionalChargeVos",salesAdditionalChargeVos);
			view.addAttribute("parentId", parentId);
			
			contactId = String.valueOf(salesItemVos.get(0).getSalesVo().getContactVo().getContactId());
			view.addAttribute("paymentTermId", salesItemVos.get(0).getSalesVo().getPaymentTermsVo().getPaymentTermId());
			view.addAttribute("salesVo", salesItemVos.get(0).getSalesVo());
		}
		if(!contactId.equals("0")) {
			view.addAttribute("contactId", contactId);
		}
		return "sales/sales-new";
	}
	
	@PostMapping("salesno/verify")
	@ResponseBody
	public String verifySalesNo(@PathVariable(value="type")String type, @RequestParam Map<String,String> allRequestParams, HttpSession session) {
		boolean isExist = false;
		if(allRequestParams.get("salesId") == null || allRequestParams.get("salesId") == "") {
			isExist = salesService.isExistSalesNo(Long.parseLong(session.getAttribute("branchId").toString()), type, allRequestParams.get("prefix"), Long.parseLong(allRequestParams.get("salesNo")));
		} else {
			isExist = salesService.isExistSalesNo(Long.parseLong(session.getAttribute("branchId").toString()), type, allRequestParams.get("prefix"), Long.parseLong(allRequestParams.get("salesNo")), Long.parseLong(allRequestParams.get("salesId")));
		}
		return "{ \"valid\": "+isExist+" }";
	}
	
	@PostMapping("/save")
	public String saveSales(@RequestParam Map<String,String> allRequestParams, @PathVariable(value="type") String type, @ModelAttribute("salesVo") SalesVo salesVo,HttpSession session) {
		
		ContactAddressVo contactAddressVo;
		
		salesVo.setType(type);
		salesVo.setStatus("Pending");
		salesVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		salesVo.setModifiedOn(CurrentDateTime.getCurrentDate());
		salesVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		salesVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		
		SalesVo salesVo2 = null;
		
		if(allRequestParams.get("billingAddressId").equals("0") || allRequestParams.get("shippingAddressId").equals("0")) {
			salesVo2 = salesService.findBySalesIdAndBranchId(salesVo.getSalesId(), salesVo.getBranchId());
		}

		//--------------Set Billing Address Details ------------------------
		if(!allRequestParams.get("billingAddressId").equals("0")) {
			contactAddressVo = contactService.findAddressByContactAddressId(Long.parseLong(allRequestParams.get("billingAddressId")));
			
			salesVo.setBillingAddressLine1(contactAddressVo.getAddressLine1());
			salesVo.setBillingAddressLine2(contactAddressVo.getAddressLine2());
			salesVo.setBillingCityCode(contactAddressVo.getCityCode());
			salesVo.setBillingCompanyName(contactAddressVo.getCompanyName());
			salesVo.setBillingCountriesCode(contactAddressVo.getCountriesCode());
			salesVo.setBillingPinCode(contactAddressVo.getPinCode());
			salesVo.setBillingStateCode(contactAddressVo.getStateCode());
		} else if (salesVo2 != null){
			salesVo.setBillingAddressLine1(salesVo2.getBillingAddressLine1());
			salesVo.setBillingAddressLine2(salesVo2.getBillingAddressLine2());
			salesVo.setBillingCityCode(salesVo2.getBillingCityCode());
			salesVo.setBillingCompanyName(salesVo2.getBillingCompanyName());
			salesVo.setBillingCountriesCode(salesVo2.getBillingCountriesCode());
			salesVo.setBillingPinCode(salesVo2.getBillingPinCode());
			salesVo.setBillingStateCode(salesVo2.getBillingStateCode());
		}
		
		
		//--------------Set Shipping Address Details ------------------------
		if(!allRequestParams.get("shippingAddressId").equals("0")) {
			contactAddressVo = contactService.findAddressByContactAddressId(Long.parseLong(allRequestParams.get("shippingAddressId")));
			
			salesVo.setShippingAddressLine1(contactAddressVo.getAddressLine1());
			salesVo.setShippingAddressLine2(contactAddressVo.getAddressLine2());
			salesVo.setShippingCityCode(contactAddressVo.getCityCode());
			salesVo.setShippingCompanyName(contactAddressVo.getCompanyName());
			salesVo.setShippingCountriesCode(contactAddressVo.getCountriesCode());
			salesVo.setShippingPinCode(contactAddressVo.getPinCode());
			salesVo.setShippingStateCode(contactAddressVo.getStateCode());
		} else if (salesVo2 != null){
			salesVo.setShippingAddressLine1(salesVo2.getShippingAddressLine1());
			salesVo.setShippingAddressLine2(salesVo2.getShippingAddressLine2());
			salesVo.setShippingCityCode(salesVo2.getShippingCityCode());
			salesVo.setShippingCompanyName(salesVo2.getShippingCompanyName());
			salesVo.setShippingCountriesCode(salesVo2.getShippingCountriesCode());
			salesVo.setShippingPinCode(salesVo2.getShippingPinCode());
			salesVo.setShippingStateCode(salesVo2.getShippingStateCode());
		}
		
		if(salesVo.getSalesId() == 0) {
			salesVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			salesVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			salesVo.setPaidAmount(0.0);
		}
		
		if(salesVo.getSalesAdditionalChargeVos() != null) { 
			System.err.println("additional charges--->"+salesVo.getSalesAdditionalChargeVos().size());
			salesVo.getSalesAdditionalChargeVos().removeIf( rm -> rm.getAdditionalChargeVo() == null);
			salesVo.getSalesAdditionalChargeVos().forEach(item1 -> item1.setSalesVo(salesVo));
		}
		
		if(salesVo.getSalesItemVos() != null) {
			salesVo.getSalesItemVos().removeIf( rm -> rm.getProductVariantVo() == null);
			salesVo.getSalesItemVos().forEach( item -> item.setSalesVo(salesVo));
		}
		
		if(allRequestParams.get("deleteSalesItemIds") != null &&
				!allRequestParams.get("deleteSalesItemIds").equals("")) {
			String address=allRequestParams.get("deleteSalesItemIds").substring(0, allRequestParams.get("deleteSalesItemIds").length()-1);
			List<Long> l= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
			
			salesService.deleteSalesItemByIdIn(l);
		}
		
		if(allRequestParams.get("deleteAdditionalChargeIds") != null &&
				!allRequestParams.get("deleteAdditionalChargeIds").equals("")) {
			
			String address=allRequestParams.get("deleteAdditionalChargeIds").substring(0, allRequestParams.get("deleteAdditionalChargeIds").length()-1);
			List<Long> l= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
			
			salesService.deleteSalesAdditionalChargeByIdIn(l);
		}
		
		try {
			if(!salesVo.getTermsAndConditionIds().equals("")) {
				System.out.println(salesVo.getTermsAndConditionIds().charAt(salesVo.getTermsAndConditionIds().length() - 1));
				
				if(String.valueOf(salesVo.getTermsAndConditionIds().charAt(salesVo.getTermsAndConditionIds().length() - 1)).equals(",")) {
					salesVo.setTermsAndConditionIds(salesVo.getTermsAndConditionIds().substring(0, salesVo.getTermsAndConditionIds().length() - 1));
				}
			}
		} catch(Exception e) {}
		
		SalesVo salesVo3 = salesService.save(salesVo);
		
		if(salesVo3.getType().equals(Constant.SALES_INVOICE)) {
			salesService.insertSalesTransaction(salesVo3,session.getAttribute("financialYear").toString());
		} else if(salesVo3.getType().equals(Constant.SALES_CREDIT_NOTE)) {
			salesService.insertSalesReturnTransaction(salesVo3,session.getAttribute("financialYear").toString()); 
		}
		
		if(salesVo3.getSalesVo() != null && salesVo3.getSalesVo().getType().equals(Constant.SALES_QUOTATION)) {
			salesService.updateStatusBySalesId("SO Generated", salesVo3.getSalesVo().getSalesId(), Long.parseLong(session.getAttribute("userId").toString()),CurrentDateTime.getCurrentDate());
		}
		
		
		//Ritiksha's SMS --- 2019-04-17
		
		//SalesVo salesVo4 = salesService.findBySalesIdAndBranchId(salesVo3.getSalesVo().getSalesId(), Long.parseLong(session.getAttribute("branchId").toString()));
		if(salesVo3.getType().equals(Constant.SALES_ORDER))
		{
			StringBuffer message= new StringBuffer();
			
					message.append("Welcome"+" "+ salesVo3.getContactVo().getCompanyName() +","
								+"\n"+"Appreciate your patronage and your order for ");
				
				salesVo3.getSalesItemVos().forEach(m -> {
					message.append(m.getQty()+" ");
					message.append(m.getProductVariantVo().getProductVo().getUnitOfMeasurementVo().getMeasurementCode()+" ");
					message.append(m.getProductVariantVo().getProductVo().getName() +" "+ m.getProductVariantVo().getVariantName() + ", ");
						});
					message.deleteCharAt(message.length()-2);
					message.append("has been booked with us." 
									+ "\n" + "You shall be updated regularly on the same." + "\n");
					
			System.err.println(""+message);
	
			//site2sms.sendMessage(salesVo3.getContactVo().getCompanyMobileno(), message+"\n", "VSYERP");
		}
		return "redirect:/sales/"+type+"/"+salesVo3.getSalesId();
	}
	
	@GetMapping("{id}")
	public String salesDetails(@PathVariable String type, @PathVariable long id, HttpSession session, Model view) {
		
		view.addAttribute("type",type);
		
		if (type.equals(Constant.SALES_INVOICE)) {
			view.addAttribute("displayType", "Invoice");
        } else if (type.equals(Constant.SALES_QUOTATION)) {
        	view.addAttribute("displayType", "Sales Quotation");
        } else if (type.equals(Constant.SALES_ORDER)) {
        	view.addAttribute("displayType", "Sales Order");
        }  else if (type.equals(Constant.SALES_CREDIT_NOTE)) {
        	view.addAttribute("displayType", "Credit Note");
        } else {
        	return "404";
		}
		
		SalesVo salesVo = salesService.findBySalesIdAndBranchId(id, Long.parseLong(session.getAttribute("branchId").toString())); 
		
		salesVo.setShippingCountriesName(locationService.findCountriesNameByCountriesCode(salesVo.getShippingCountriesCode()));
		salesVo.setShippingStateName(locationService.findStateNameByStateCode(salesVo.getShippingStateCode()));
		salesVo.setShippingCityName(locationService.findCityNameByCityCode(salesVo.getShippingCityCode()));
		
		salesVo.setBillingCountriesName(locationService.findCountriesNameByCountriesCode(salesVo.getBillingCountriesCode()));
		salesVo.setBillingStateName(locationService.findStateNameByStateCode(salesVo.getBillingStateCode()));
		salesVo.setBillingCityName(locationService.findCityNameByCityCode(salesVo.getBillingCityCode()));
		
		view.addAttribute("salesVo", salesVo);
		
		if(!salesVo.getTermsAndConditionIds().equals("")) {
			List<Long> termandconditionIds = (List<Long>) Arrays.asList(salesVo.getTermsAndConditionIds().split("\\s*,\\s*")).stream().map(Long::parseLong).collect(Collectors.toList());
		
			view.addAttribute("termsAndConditionVos", termsAndConditionService.findByTermsandConditionIdIn(termandconditionIds));
		}
		
		return "sales/sales-view";
	}
	
	@GetMapping("{id}/edit")
	public String editSales(@PathVariable(value="id") long id, @PathVariable(value="type")String type, HttpSession session, Model view) {
		
		view.addAttribute("type",type);
		
		if (type.equals(Constant.SALES_INVOICE)) {
			view.addAttribute("displayType", "Invoice");
        } else if (type.equals(Constant.SALES_QUOTATION)) {
        	view.addAttribute("displayType", "Sales Quotation");
        } else if (type.equals(Constant.SALES_ORDER)) {
        	view.addAttribute("displayType", "Sales Order");
        }  else if (type.equals(Constant.SALES_CREDIT_NOTE)) {
        	view.addAttribute("displayType", "Credit Note");
        } else {
        	return "404";
		}
		
		view.addAttribute("contactVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_CUSTOMER));
		view.addAttribute("contactAgentVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_AGENT));
		view.addAttribute("contactTransportVos", contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_TRANSPORT));
		view.addAttribute("paymentTermVos", paymentTermService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		
		SalesVo salesVo = salesService.findBySalesIdAndBranchId(id, Long.parseLong(session.getAttribute("branchId").toString())); 
		
		salesVo.setShippingCountriesName(locationService.findCountriesNameByCountriesCode(salesVo.getShippingCountriesCode()));
		salesVo.setShippingStateName(locationService.findStateNameByStateCode(salesVo.getShippingStateCode()));
		salesVo.setShippingCityName(locationService.findCityNameByCityCode(salesVo.getShippingCityCode()));
		
		salesVo.setBillingCountriesName(locationService.findCountriesNameByCountriesCode(salesVo.getBillingCountriesCode()));
		salesVo.setBillingStateName(locationService.findStateNameByStateCode(salesVo.getBillingStateCode()));
		salesVo.setBillingCityName(locationService.findCityNameByCityCode(salesVo.getBillingCityCode()));
		
		view.addAttribute("salesVo", salesVo);
		
		if(!salesVo.getTermsAndConditionIds().equals("")) {
			List<Long> termandconditionIds = (List<Long>) Arrays.asList(salesVo.getTermsAndConditionIds().split("\\s*,\\s*")).stream().map(Long::parseLong).collect(Collectors.toList());;
		
			view.addAttribute("termsAndConditionVos", termsAndConditionService.findByTermsandConditionIdIn(termandconditionIds));
		}
		
		if(salesVo.getSalesAdditionalChargeVos().size() > 0) {
			view.addAttribute("additionalChargeVos", additionalChargeService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		}
		
		
		return "sales/sales-edit";
	}
	
	@GetMapping("{id}/delete")
	public String salesDelete(@PathVariable String type, @PathVariable long id, HttpSession session) {
		
		salesService.deleteSales(Long.parseLong(session.getAttribute("branchId").toString()), id, type);
		
		return "redirect:/sales/" + type;
	}
	
	@GetMapping("/contact/{id}/json")
	@ResponseBody
	public List<SalesVo> salesListJSON(HttpSession session, @PathVariable(value = "type") String type,
			@PathVariable(value = "id") long contactId) {

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

		List<SalesVo> salesVos = new ArrayList<SalesVo>();

		try {
			
			List<String> salesTypes = new ArrayList<String>();
			if (type.equals(Constant.SALES_ESTIMATE)) {
				salesTypes.add(Constant.SALES_ESTIMATE);
			} else {
				salesTypes.add(Constant.SALES_INVOICE);
				salesTypes.add(Constant.SALES_BILL_OF_SUPPLY);
				salesTypes.add(Constant.SALES_POS);
			}
			
			salesVos = salesService.findByTypesAndContactAndBranchIdAndSalesDateBetween(salesTypes,
					Long.parseLong(session.getAttribute("branchId").toString()),
					dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
					dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()), contactId);

			Collections.reverse(salesVos);

			salesVos.forEach(salesItem -> {
				salesItem.setSalesItemVos(null);
				salesItem.setSalesAdditionalChargeVos(null);
				salesItem.setContactVo(null);
				salesItem.setSalesVo(null);
				salesItem.setContactTransportVo(null);
				salesItem.setContactAgentVo(null);
				salesItem.setAssignedEmployee(null);
			});
			  
		} catch (NumberFormatException | ParseException e) {
			e.printStackTrace();
		}

		return salesVos;
	}
	
	@PostMapping("{id}/productbarcode/{itemCode}/json")
	@ResponseBody
	public SalesItemVo productBarcodeJSON(@PathVariable("id") long salesId, @PathVariable("itemCode") String itemCode, HttpSession session) throws NumberFormatException, ParseException {
		
		Map<String, Object> response = new HashMap<String, Object>();
		
		
		SalesItemVo salesItemVo = salesService.findByItemCodeAndCompanyIdAndSalesId(itemCode, Long.parseLong(session.getAttribute("companyId").toString()),salesId);
		
		if(salesItemVo == null) {
			return null;
		}
		else {
			salesItemVo.getProductVariantVo().getProductVo().setProductVariantVos(null);
			salesItemVo.getProductVariantVo().getProductVo().setProductAttributeVos(null);
			salesItemVo.setSalesVo(null);
			
			return salesItemVo;
		}
	}	

	@PostMapping("{contactId}/pending/json")
	@ResponseBody
	public List<SalesVo> salesPendingListJson(@PathVariable("type") String type, @PathVariable("contactId") long contactId, HttpSession session) throws NumberFormatException, ParseException {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		List<SalesVo> salesVos= salesService.findUnpaidSalesByTypeAndBranchIdAndContactIdAndSalesDateBetween(type, 
				Long.parseLong(session.getAttribute("branchId").toString()),
				contactId,
				dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()),
				dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()));
		
			salesVos.forEach(p-> {
			p.setContactVo(null);
			p.setSalesItemVos(null);
			p.setSalesAdditionalChargeVos(null);
			p.setContactTransportVo(null);
			p.setContactAgentVo(null);
			p.setSalesVo(null);
			p.setAssignedEmployee(null);
		});
		return salesVos;
	}

	@PostMapping("{id}/item/json")
	@ResponseBody
	public List<SalesItemVo> salesItemJSON(@PathVariable("id") long salesId, HttpSession session) throws NumberFormatException, ParseException {
		
		List<SalesItemVo> salesItemVos = salesService.findItemBySalesId(salesId);
		
		salesItemVos.forEach(p -> {
			p.setSalesVo(null);
			p.getProductVariantVo().getProductVo().setProductVariantVos(null);
			p.getProductVariantVo().getProductVo().setProductAttributeVos(null);
			p.getProductVariantVo().getProductVo().setProductVo(null);
		});
		
		return salesItemVos;

	}
	

	@PostMapping("{id}/json")
	@ResponseBody
	public SalesVo salesDetailsJson(@PathVariable long id, HttpSession session) throws NumberFormatException, ParseException {
		SalesVo salesVo=salesService.findBySalesIdAndBranchId(id,Long.parseLong(session.getAttribute("branchId").toString()));
		salesVo.getContactVo().getContactAddressVos().forEach(ad->ad.setContactVo(null));
		salesVo.getContactVo().setContactOtherVos(null);
		salesVo.setSalesVo(null);
		salesVo.setSalesItemVos(null);
		salesVo.setSalesAdditionalChargeVos(null);
		salesVo.setContactTransportVo(null);
		salesVo.setContactAgentVo(null);
		return salesVo;
	}
	
	@GetMapping("{id}/pdf")
	public void salesPDF(@PathVariable("id") long salesId, @PathVariable("type") String type, HttpSession session, HttpServletRequest request, HttpServletResponse response) 
	{
		SalesVo salesVo=salesService.findBySalesIdAndBranchId(salesId, Long.parseLong(session.getAttribute("branchId").toString()));
	    jasperParameter = new HashMap();
		jasperParameter.put("sales_id",salesId);
		jasperParameter.put("user_front_id",Long.parseLong(session.getAttribute("branchId").toString()));
		
		jasperParameter.put("path", request.getServletContext().getRealPath("/") + "report" + System.getProperty("file.separator"));
		jasperParameter.put("amount_in_word",new NumberToWord().getNumberToWord(salesVo.getTotal()));
		JasperExporter jasperExporter = new JasperExporter(); 
		
		try 	
		{
			if(type.equals(Constant.SALES_INVOICE)) {
				jasperParameter.put("display_title", "Invoice");
				jasperExporter.jasperExporterPDF(jasperParameter,
						request.getServletContext().getRealPath("/") + "report/invoice"
								+ System.getProperty("file.separator") +"/invoice-1.jrxml","Invoice-"+salesId+".pdf", response);
			} else if(type.equals(Constant.SALES_QUOTATION)) {
				jasperParameter.put("display_title", "Sales Quotation");
				jasperExporter.jasperExporterPDF(jasperParameter,
						request.getServletContext().getRealPath("/") + "report/invoice"
								+ System.getProperty("file.separator") +"/invoice-1.jrxml","Quotation-"+salesId+".pdf", response);
			} else if(type.equals(Constant.SALES_ORDER)) {
					jasperParameter.put("display_title", "Sales Order");
					jasperExporter.jasperExporterPDF(jasperParameter,
							request.getServletContext().getRealPath("/") + "report/invoice"
									+ System.getProperty("file.separator") +"/invoice-1.jrxml","Order-"+salesId+".pdf", response);
			} else if(type.equals(Constant.SALES_CREDIT_NOTE)) {
				jasperParameter.put("display_title", "Credit Note");
				jasperExporter.jasperExporterPDF(jasperParameter,
						request.getServletContext().getRealPath("/") + "report/invoice"
								+ System.getProperty("file.separator") +"/creditnote-1.jrxml","Credit-Note-"+salesId+".pdf", response);
		}
								
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	@PostMapping("{id}/assigned/employee")
	public String assignedEmployee(@PathVariable("id") long salesId, @PathVariable("type") String type, @RequestParam("assignedEmployee") long employeeId, HttpSession session ) {
		
		salesService.updateAssignedEmployeeAndStatusBySalesId(employeeId, "Employee Assigned", salesId, Long.parseLong(session.getAttribute("userId").toString()),CurrentDateTime.getCurrentDate());
		
		return "redirect:/sales/" + type+"/"+salesId;
	}
	
	@PostMapping("{id}/update/status")
	@ResponseBody
	public String updateStatus(@PathVariable("id") long salesId, @PathVariable("type") String type, @RequestParam("status") String status, HttpSession session ) {
		
		salesService.updateStatusBySalesId(status, salesId, Long.parseLong(session.getAttribute("userId").toString()),CurrentDateTime.getCurrentDate());
		
		return "success";
	}
	//for multiple select
	@RequestMapping("/sendsms")	
	@ResponseBody
    public String  sendsms(@RequestParam("phoneno") String phoneNo, @RequestParam("message") String message, HttpSession session){
    	
		if(String.valueOf(phoneNo.charAt(phoneNo.length() - 1)).equals(",")) {
			phoneNo = phoneNo.substring(0, phoneNo.length() -1);	
		}
		
		site2sms.sendMessage(phoneNo, message, "VSYERP");
		
		return "Sucess";	
    }
    
	
	//	for ajax to call the message into modal
	//for status pending
	@RequestMapping("{id}/sendsms/salesdetails")
	@ResponseBody
	public StringBuffer sendsmstoparticularnew(@PathVariable ("id") long salesId, HttpSession session) {
		
		SalesVo salesVo=salesService.findBySalesId(salesId);
		if(salesVo.getType().equals(Constant.SALES_ORDER) && salesVo.getStatus().equals("Pending")|| salesVo.getType().equals(Constant.SALES_QUOTATION) && salesVo.getStatus().equals("SO Generated")) {
		
		String name=salesVo.getContactVo().getCompanyName();
				
		StringBuffer message1= new StringBuffer();
		
			message1.append("Welcome"+" "+ name +"."+"\n"+"Appreciate your patronage and your order for ");
		
			salesVo.getSalesItemVos().forEach(m -> {
			message1.append(m.getQty()+" ");
			message1.append(m.getProductVariantVo().getProductVo().getUnitOfMeasurementVo().getMeasurementCode()+" ");
			message1.append(m.getProductVariantVo().getProductVo().getName() +" "+ m.getProductVariantVo().getVariantName() + ", ");
				});
			message1.deleteCharAt(message1.length()-2);
			message1.append("has been booked with us." + "\n" + "You shall be updated regularly on the same." + "\n");
			
			System.err.println(""+message1);
			return message1;
		}
		return null;
		
	}
	//for status inprogress
	@RequestMapping("{id}/sendsms/productiondetails")
	@ResponseBody
	public  StringBuffer sendsms(@PathVariable("id") long salesId,HttpSession session){
	
		SalesVo salesVo=salesService.findBySalesId(salesId);
		if(salesVo.getType().equals(Constant.SALES_ORDER) && salesVo.getStatus().equals("In Progress")) {
		
		StringBuffer message2= new StringBuffer();
		String name = salesVo.getContactVo().getCompanyName();
	
		message2.append("Dear"+" "+ name +","+"\n"+"Production Update:"+"\n");
	
		salesVo.getSalesItemVos().forEach(x -> {
		message2.append(x.getProducedQty()+" ");
		message2.append(x.getProductVariantVo().getProductVo().getUnitOfMeasurementVo().getMeasurementCode()+" ");
		message2.append("ready out of"+" ");
		
		message2.append(salesVo.getSalesItemVos().stream().filter(si -> 
		si.getProductVariantVo().getProductVariantId() == x.getProductVariantVo().getProductVariantId()).collect(Collectors.toList()).get(0).getQty() + " "+
		x.getProductVariantVo().getProductVo().getUnitOfMeasurementVo().getMeasurementCode() +" " + "order");
		message2.append(",");
		});
		message2.deleteCharAt(message2.length()-1);
		System.err.println(""+message2);
		return message2;
		}
		return null;
		
	}
	//for status production completed and this msg shud go to transporter
	@RequestMapping("{id}/sendsms/deliverydetails")
	@ResponseBody
	public  String sendsms2(@PathVariable("id") long salesId, HttpSession session){
		SalesVo salesVo=salesService.findBySalesId(salesId);
		if(salesVo.getType().equals(Constant.SALES_ORDER) && salesVo.getStatus().equals("Production Completed")) {	
		jsonObject = new JSONObject();
		jsonArray = new JSONArray();
		
			StringBuffer message3= new StringBuffer();
			message3.append("Dear Transporter" +","+"\n"+"we require a truck as per below given details: "+"\n");
			
			salesVo.getSalesItemVos().forEach(m -> {
    		message3.append("Item:" + m.getProductVariantVo().getProductVo().getName() +" "+ m.getProductVariantVo().getVariantName() + "\n");
			message3.append("Qty:" + m.getQty()+" "+"\n");
    		});
    			
			salesVo.getContactVo().getContactAddressVos().forEach(c -> {
    		message3.append("Destination:"+locationService.findCityNameByCityCode(c.getCityCode())+"\n");
    		});
    		
			salesVo.getSalesItemVos().forEach(m -> {
        	message3.append("Truck required for:" +" "+ m.getQty()+" "+ m.getProductVariantVo().getProductVo().getUnitOfMeasurementVo().getMeasurementCode() + m.getProductVariantVo().getProductVo().getName() +" "+ m.getProductVariantVo().getVariantName());
    		});
	
			message3.deleteCharAt(message3.length()-2);
			System.err.println(""+message3);
	
			List<ContactVo> tansportVos=contactService.findByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()), Constant.CONTACT_TRANSPORT);
			
			
			tansportVos.forEach(ct -> {
				JSONObject json1 = new JSONObject();
				try{
				json1.put("id", ct.getContactId());
				json1.put("cName",ct.getCompanyName());
				json1.put("cMobileNo", ct.getCompanyMobileno());
				}catch(Exception e){}
				jsonArray.put(json1);
			});
			
			//message3.append("====="+tansportVos.toString());
			jsonObject.put("message",message3);
			jsonObject.put("transportVos",jsonArray);
			System.err.println(tansportVos);
			return jsonObject.toString();
		}	
		return null;
	}
	
	//for status production completed and this msg shud go to customer
	@RequestMapping("{id}/sendsms/details")
	@ResponseBody
	public  StringBuffer sendsms3(@PathVariable("id") long salesId,HttpSession session){
		SalesVo salesVo=salesService.findBySalesId(salesId);
		if(salesVo.getType().equals(Constant.SALES_ORDER) && salesVo.getStatus().equals("Production Completed")) {
			
			StringBuffer message4= new StringBuffer();
			String name = salesVo.getContactVo().getCompanyName();
			
			message4.append("Dear"+" "+ name +","+"\n"+"Production Update:"+"\n");
			message4.append("Your Order is completed."+ "\n");
			salesVo.getSalesItemVos().forEach(s -> {
			message4.append(s.getQty()+" ");
			message4.append(s.getProductVariantVo().getProductVo().getUnitOfMeasurementVo().getMeasurementCode()+" ");
			
			message4.append("ready out of"+" ");
			message4.append(salesVo.getSalesItemVos().stream().filter(si -> 
			si.getProductVariantVo().getProductVariantId() == s.getProductVariantVo().getProductVariantId()).collect(Collectors.toList()).get(0).getQty() + " "+
			s.getProductVariantVo().getProductVo().getUnitOfMeasurementVo().getMeasurementCode());
			message4.append(",");
			
			});
			message4.deleteCharAt(message4.length()-1);
			message4.append(" "+"order."+"\n");
			
			message4.append("Request you to pls place truck after confirming with the Unit head."+"\n");
			
			System.err.println(""+message4);
		return message4;
		}
		return null;
	}
	
	//for message shown in modal edit and send
	@RequestMapping("{id}/sendsms")
	@ResponseBody
	public String sendsms1(@PathVariable ("id") long salesId,@RequestParam("phoneno") String phoneno, @RequestParam("message") String message, HttpSession session){
    	
		SalesVo salesVo=salesService.findBySalesId(salesId);
		System.out.println("phoneNo" +phoneno);
		site2sms.sendMessage(salesVo.getContactVo().getCompanyMobileno(), message, "VSYERP");
		System.err.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+message);
		return "Sucess";	
	}
}
