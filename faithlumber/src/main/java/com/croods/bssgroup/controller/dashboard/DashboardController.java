package com.croods.bssgroup.controller.dashboard;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.service.contact.ContactService;
import com.croods.bssgroup.service.product.ProductService;
import com.croods.bssgroup.service.sales.SalesService;
import com.croods.bssgroup.service.stock.StockTransactionService;

@Controller
@RequestMapping("dashboard")
public class DashboardController {

	@Autowired
	ContactService contactService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	StockTransactionService stockTransactionService;
	
	@Autowired
	SalesService salesService;
	
	@GetMapping("")
	public ModelAndView dashboard(HttpSession session) {
		ModelAndView view = new ModelAndView("dashboard/dashboard");
		
		view.addObject("totalCustomers",contactService.countByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()),Constant.CONTACT_CUSTOMER));
		view.addObject("totalSuppliers",contactService.countByBranchIdAndType(Long.parseLong(session.getAttribute("branchId").toString()),Constant.CONTACT_SUPPLIER));
		view.addObject("totalProduct",productService.countProductByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		
		view.addObject("totalRetailerCustomers",contactService.countByBranchIdAndTypeAndContactType(Long.parseLong(session.getAttribute("branchId").toString()),Constant.CONTACT_CUSTOMER,Constant.CONTACT_CATEGORY_RETAILER));
		view.addObject("totalWholesalerCustomers",contactService.countByBranchIdAndTypeAndContactType(Long.parseLong(session.getAttribute("branchId").toString()),Constant.CONTACT_CUSTOMER,Constant.CONTACT_CATEGORY_WHOLESALER));
		view.addObject("totalOtherCustomers",contactService.countByBranchIdAndTypeAndContactType(Long.parseLong(session.getAttribute("branchId").toString()),Constant.CONTACT_CUSTOMER,Constant.CONTACT_CATEGORY_OTHER));
		
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date startDate, endDate;
		
		double totalQty = 0 ;
		double totalProductionRemainsQty = 0 ;
		
//		try {
//			totalQty = stockTransactionService.getAllProductTotalQty(Long.parseLong(session.getAttribute("branchId").toString()), 
//					dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()), dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()), 
//					session.getAttribute("financialYear").toString());
//		} catch (NumberFormatException | ParseException e) {
//			e.printStackTrace();
//			totalQty = 0;
//			
//		}
		try {
			totalQty = salesService.getAllProducedTotalQty(Long.parseLong(session.getAttribute("branchId").toString()), 
					dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString()), dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString()));
		}catch (Exception e) {
			e.printStackTrace();
			totalQty = 0;
		}
		
		System.err.println("CurrentAvailableStock"+totalQty);
		view.addObject("currentAvailableStock", totalQty);
		
		
		try {
			totalProductionRemainsQty = salesService.getRemainsProductionQty(Long.parseLong(session.getAttribute("branchId").toString()));
		}catch (Exception e) {
			e.printStackTrace();
			totalProductionRemainsQty = 0;
		}
		
		System.err.println("totalProductionRemainsQty"+totalProductionRemainsQty);
		view.addObject("totalProductionRemainsQty", totalProductionRemainsQty);
		
		// Last 15 Days Sales
		
		List<Map<Double, String>> list = salesService.getLast15DaySales(Long.parseLong(session.getAttribute("branchId").toString()), Constant.SALES_QUOTATION);
		String salesDate=list.stream().map(a -> String.valueOf(a.get("salesdate"))).collect(Collectors.joining("','"));
		String salesData=list.stream().map(a -> String.valueOf(a.get("total"))).collect(Collectors.joining(","));
		System.err.println(salesDate+" Sales Date");
		System.err.println(salesData+" Sales Data");
		view.addObject("salesDate",salesDate);
		view.addObject("salesData",salesData);
		
		List<Map<Double, String>> list2 = salesService.getLast15DaySales(Long.parseLong(session.getAttribute("branchId").toString()), Constant.SALES_ORDER);
		String salesDate2=list2.stream().map(a -> String.valueOf(a.get("salesdate"))).collect(Collectors.joining("','"));
		String salesData2=list2.stream().map(a -> String.valueOf(a.get("total"))).collect(Collectors.joining(","));
		System.err.println(salesDate2+" Sales Date");
		System.err.println(salesData2+" Sales Data");
		view.addObject("salesDate2",salesDate2);
		view.addObject("salesData2",salesData2);
		
		return view;
	}
	
	@PostMapping("/salesvspurchase")
	@ResponseBody
	public List<Map<String, String>> salesvsPurchase(HttpSession session) throws ParseException{
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date startDate, endDate;
		
    	startDate = dateFormat.parse(session.getAttribute("firstDateFinancialYear").toString());
    	endDate =dateFormat.parse(session.getAttribute("lastDateFinancialYear").toString());
		
		return salesService.getSalesVSPurchase(Long.parseLong(session.getAttribute("branchId").toString()),startDate,endDate);
	}
}

