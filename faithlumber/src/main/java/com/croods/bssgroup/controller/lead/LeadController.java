package com.croods.bssgroup.controller.lead;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.service.employee.EmployeeService;
import com.croods.bssgroup.service.lead.LeadService;
import com.croods.bssgroup.service.leadsource.LeadSourceService;
import com.croods.bssgroup.service.location.LocationService;
import com.croods.bssgroup.service.product.ProductService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.lead.LeadActivityVo;
import com.croods.bssgroup.vo.lead.LeadVo;

@Controller
@RequestMapping("/lead")
public class LeadController {

	@Autowired
	LocationService locationService;
	
	@Autowired
	LeadService leadService;
	
	@Autowired
	LeadSourceService leadSourceService;
	
	@Autowired
	ProductService productService;
	
	@Autowired
	EmployeeService employeeService;
	
	@GetMapping("")
	public ModelAndView lead(HttpSession session)
 	{	
		ModelAndView view=new ModelAndView();
		view.setViewName("lead/lead");
		
		List<LeadVo> leadVos = leadService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		
		leadVos.forEach(c -> {
			c.getLeadAddressVos().removeIf( a ->
				a.getIsDefault() ==0 || a.getIsDeleted() == 1);
			
			c.getLeadAddressVos().forEach( a -> {
				a.setCityName(locationService.findCityNameByCityCode(a.getCityCode()));
			});
		});
		
		view.addObject("leadVos", leadVos);
		
		return view;
	}
	
	@RequestMapping("new")
	public ModelAndView newContact(HttpSession session)
 	{	
		ModelAndView view=new ModelAndView();
		view.addObject("leadSourceVos",leadSourceService.findByCompanyIdAndIsEditable(Long.parseLong(session.getAttribute("companyId").toString())));
		view.setViewName("lead/lead-new");
		return view;
	}
	
	@PostMapping("/save")
	public String save(HttpSession session, @RequestParam Map<String,String> allRequestParams, @ModelAttribute("leadVo") LeadVo leadVo, BindingResult bindingResult) {
		
    	if(leadVo.getLeadAddressVos() != null && leadVo.getLeadAddressVos().size() != 0) {
    		leadVo.getLeadAddressVos().forEach(c -> c.setLeadVo(leadVo));
    	}
    	if(leadVo.getLeadContactVos() != null && leadVo.getLeadContactVos().size() != 0) {
    		leadVo.getLeadContactVos().forEach(c -> c.setLeadVo(leadVo));
    	}
    	
    	if(leadVo.getLeadId() == 0) {
	    	
    		leadVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
        	leadVo.setCreatedOn(CurrentDateTime.getCurrentDate());
    	}
    	
    	leadVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
    	leadVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
    	leadVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
    	leadVo.setModifiedOn(CurrentDateTime.getCurrentDate());
    	leadVo.setStatus("Pending");
    	leadService.saveLead(leadVo);
    	
    	if(allRequestParams.containsKey("deleteAddress") == true) {		
			if(!allRequestParams.get("deleteAddress").equals("")) {
				String address=allRequestParams.get("deleteAddress").substring(0, allRequestParams.get("deleteAddress").length()-1);
				List<Long> leadAddressIds= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
				leadService.deleteLeadAddressByIdIn(leadAddressIds);
			}
			
		}
    	
    	if(allRequestParams.containsKey("deletedLeadContact") == true) {		
			if(!allRequestParams.get("deletedLeadContact").equals("")) {
				String address=allRequestParams.get("deletedLeadContact").substring(0, allRequestParams.get("deletedLeadContact").length()-1);
				List<Long> leadContactIds= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
				leadService.deleteLeadContactByIdIn(leadContactIds);
			}
			
		}
		return "redirect:/lead/"+leadVo.getLeadId();
	}
	
	@GetMapping("{id}")
	public ModelAndView leadView(@PathVariable("id") long leadId, HttpSession session) throws NumberFormatException, ParseException {
		ModelAndView view = new ModelAndView("lead/lead-view");
		System.out.println(leadId);
		LeadVo leadVo = leadService.findByLeadIdAndBranchId(leadId, Long.parseLong(session.getAttribute("branchId").toString()));
		System.out.println(leadVo.getCompanyName());
		leadVo.getLeadAddressVos().removeIf( c -> c.getIsDeleted()==1);
		
		try {
			leadVo.getLeadAddressVos().forEach( c -> {
				c.setCountriesName(locationService.findCountriesNameByCountriesCode(c.getCountriesCode()));
				c.setStateName(locationService.findStateNameByStateCode(c.getStateCode()));
				c.setCityName(locationService.findCityNameByCityCode(c.getCityCode()));
			});
		} catch(Exception e) {
			
		}
		
		if(leadVo.getInterestedProductIds() != null && !leadVo.getInterestedProductIds().equals("")) {
			List<Long> productVariantIds = (List<Long>) Arrays.asList(leadVo.getInterestedProductIds().split("\\s*,\\s*")).stream().map(Long::parseLong).collect(Collectors.toList());
		
			view.addObject("productVariantVos", productService.findProductVariantByIdIn(productVariantIds));
		}
		
		view.addObject("leadVo",leadVo);
		view.addObject("leadActivityVos", leadService.findActivityByLeadIdAndBranchId(leadId, Long.parseLong(session.getAttribute("branchId").toString())));
		view.addObject("employeeVos", employeeService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView leadEdit(@PathVariable("id") long leadId, HttpSession session) throws NumberFormatException, ParseException {
		ModelAndView view = new ModelAndView("lead/lead-edit");
		
		LeadVo leadVo = leadService.findByLeadIdAndBranchId(leadId, Long.parseLong(session.getAttribute("branchId").toString()));
		
		leadVo.getLeadAddressVos().removeIf( c -> c.getIsDeleted()==1);
		
		if(leadVo.getInterestedProductIds() != null && !leadVo.getInterestedProductIds().equals("")) {
			List<Long> productVariantIds = (List<Long>) Arrays.asList(leadVo.getInterestedProductIds().split("\\s*,\\s*")).stream().map(Long::parseLong).collect(Collectors.toList());
		
			view.addObject("productVariantVos", productService.findProductVariantByIdIn(productVariantIds));
		}
		
		view.addObject("leadSourceVos",leadSourceService.findByCompanyIdAndIsEditable(Long.parseLong(session.getAttribute("companyId").toString())));
		
		view.addObject("leadVo",leadVo);
		return view;
	}
	
	@GetMapping("{id}/delete")
    public String deleteLead(@PathVariable("id") long leadId, HttpSession session) {
		
		leadService.deleteLead(leadId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		
        return "redirect:/lead";
    }
	
	@PostMapping("/{id}/activity/save")
	public String saveActivity(@PathVariable("id") long leadId, HttpSession session, @RequestParam Map<String,String> allRequestParams, @ModelAttribute("leadActivityVo") LeadActivityVo leadActivityVo, BindingResult bindingResult) {
		
		if(leadActivityVo.getLeadActivityId()== 0) {
	    	
			leadActivityVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			leadActivityVo.setCreatedOn(CurrentDateTime.getCurrentDate());
    	}
    	
		leadActivityVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		leadActivityVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
		leadActivityVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
		leadActivityVo.setModifiedOn(CurrentDateTime.getCurrentDate());
    	
		LeadVo leadVo = new LeadVo();
		leadVo.setLeadId(leadId);
		
		leadActivityVo.setLeadVo(leadVo);
		
		//leadVo.setStatus("Pending");
    	leadService.saveLeadActivity(leadActivityVo);
    	
    	if(allRequestParams.get("assignedEmployee.employeeId") == null) {
    		leadService.updateLeadStatus(leadId, "In Progress", Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
    	} else {
    		leadService.updateLeadStatusAndAssignedEmployee(leadId, "In Progress", Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate(), Long.parseLong(allRequestParams.get("assignedEmployee.employeeId").toString()));
    	}
    	
    	System.out.println("leadId"+leadId);
		return "redirect:/lead/"+leadId;
	}
	
	@PostMapping("/{id}/completed/save")
	public String saveLeadComplete(@PathVariable("id") long leadId, HttpSession session, @RequestParam Map<String,String> allRequestParams) {
		
		try{
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			
			leadService.updateCompleteDetails(leadId, "Completed", dateFormat.parse(allRequestParams.get("completedDate")), allRequestParams.get("remark"), Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
			
		}catch (Exception e) { 
			e.printStackTrace();
		}
		
    	
		return "redirect:/lead/"+leadId;
	}
}
