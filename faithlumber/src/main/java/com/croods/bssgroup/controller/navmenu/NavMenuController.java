package com.croods.bssgroup.controller.navmenu;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.amazonaws.services.chime.model.Account;
import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.service.account.AccountService;
import com.croods.bssgroup.service.navmenu.NavMenuService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.account.AccountCustomVo;
import com.croods.bssgroup.vo.account.AccountVo;
import com.croods.bssgroup.vo.navmenu.NavMenuActionVo;
import com.croods.bssgroup.vo.navmenu.NavMenuPermissionVo;
import com.croods.bssgroup.vo.navmenu.NavMenuVo;
import com.croods.bssgroup.vo.navmenu.NavSubMenuVo;
import com.croods.bssgroup.vo.userfront.UserFrontVo;
import com.croods.bssgroup.vo.userrole.UserRoleVo;

@Controller
@RequestMapping("allpermit")
public class NavMenuController {
	
	@Autowired
	NavMenuService navMenuService;
	
	@Autowired
	AccountService accountService;
	
	@GetMapping("nav")
	public ModelAndView nav() { 
		ModelAndView view = new ModelAndView("navmenu/navmenu");
		
		view.addObject("navMenuVos", navMenuService.findAllNavMenu());
		
		return view;
	}
	
	@GetMapping("navsub")
	public ModelAndView navsub() { 
		ModelAndView view = new ModelAndView("navmenu/navsubmenu");
		
		view.addObject("navMenuVos", navMenuService.findAllNavMenu());
		
		return view;
	}
	
	@PostMapping("navmenu/save")
	public String saveNav(@RequestParam Map<String,String> allRequestParams) {
		NavMenuVo navMenuVo = new  NavMenuVo();
		
		navMenuVo.setIconClass("m-menu__link-icon flaticon-line-graph");
		navMenuVo.setMenuURL(allRequestParams.get("menuURL"));
		navMenuVo.setOrdering(Integer.parseInt(allRequestParams.get("ordering")));
		navMenuVo.setStatus("active");
		navMenuVo.setTitle(allRequestParams.get("title"));
		
		navMenuService.saveNavMenu(navMenuVo);
		
		return "redirect:/nav";
	}
	
	@PostMapping("navsubmenu/save")
	public String saveNavSub(@RequestParam Map<String,String> allRequestParams) {
		
		for(int i=0; i<10; i++) {
			System.out.println(allRequestParams.get("menuId"+i) != "");
			if(!allRequestParams.get("menuId"+i).equals("khali")) {
				NavSubMenuVo navSubMenuVo = new NavSubMenuVo();
				
				navSubMenuVo.setIconClass("m-menu__link-icon flaticon-line-graph");
				navSubMenuVo.setMenuURL(allRequestParams.get("menuURL"+i));
				navSubMenuVo.setOrdering(Integer.parseInt(allRequestParams.get("ordering"+i)));
				navSubMenuVo.setStatus("active");
				navSubMenuVo.setSubMenuAction(allRequestParams.get("action"+i));
				navSubMenuVo.setTitle(allRequestParams.get("title"+i));
				navSubMenuVo.setType(allRequestParams.get("type"+i));
				
				NavMenuVo navMenuVo = new  NavMenuVo();
				navMenuVo.setNavMenuId(Long.parseLong(allRequestParams.get("menuId"+i)));
				navSubMenuVo.setNavMenuVo(navMenuVo);
				
				navMenuService.saveNavSubMenu(navSubMenuVo);
			}
		}
		
		return "redirect:/navsub";
	}
	
	@GetMapping("/{companyId}/permission")
	@ResponseBody
	public String menuPermission(@PathVariable("companyId") long companyId, HttpServletRequest req)
 	{
		UserFrontVo userFrontVo =new UserFrontVo();
		userFrontVo.setUserFrontId(companyId);
		
		UserRoleVo roleVo =new UserRoleVo();
		roleVo.setUserRoleId(Constant.URID_COMPANY);
		
		List<NavSubMenuVo> navSubMenuVos = navMenuService.findAllNavSubMenu();
		for (NavSubMenuVo navSubMenuVo : navSubMenuVos) {
			String actions[] = navSubMenuVo.getSubMenuAction().split(",");
			
			for (int j=0;j<actions.length;j++) {
				if(!actions[j].equals("")) {
					NavMenuActionVo actionVo =new NavMenuActionVo();
					actionVo.setNavMenuActionId(Long.parseLong(actions[j]));
					
					NavMenuPermissionVo navMenuPermissionVo =new NavMenuPermissionVo();
					
					navMenuPermissionVo.setNavMenuVo(navSubMenuVo.getNavMenuVo());
					navMenuPermissionVo.setNavSubMenuVo(navSubMenuVo);
					navMenuPermissionVo.setStatus("active");
					navMenuPermissionVo.setUserRoleVo(roleVo);
					navMenuPermissionVo.setNavMenuActionVo(actionVo);
					
					navMenuService.saveNavMenuPermission(navMenuPermissionVo);
				}
			}
			
			
		}
		
		return "SuCCESS ";
	}
	
	@GetMapping("/{companyId}/account")
	@ResponseBody
	public String defaultAccount(@PathVariable("companyId") long companyId, HttpServletRequest req) {
		
		List<AccountVo> accountVos = accountService.findAllAccount();

		for (AccountVo accountVo : accountVos) {
			AccountCustomVo accountCustomVo = new AccountCustomVo();
			accountCustomVo.setAccountName(accountVo.getAccountName());
			accountCustomVo.setAccounType(Constant.ACCOUNT_CUSTOM);
			accountCustomVo.setAlterBy(companyId);
			accountCustomVo.setBranchId(companyId);
			accountCustomVo.setCompanyId(companyId);
			accountCustomVo.setCreatedBy(companyId);
			accountCustomVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			accountCustomVo.setGroup(accountVo.getGroup());
			accountCustomVo.setIsDeleted(0);
			accountCustomVo.setIsUpdatable(0);
			accountCustomVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			accountService.insertAccount(accountCustomVo);
		}
		
		return "Done";
	}
}
