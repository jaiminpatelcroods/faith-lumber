package com.croods.bssgroup.controller.employee;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.croods.bssgroup.service.department.DepartmentService;
import com.croods.bssgroup.service.designation.DesignationService;
import com.croods.bssgroup.service.employee.EmployeeService;
import com.croods.bssgroup.service.location.LocationService;
import com.croods.bssgroup.service.userfront.UserFrontService;
import com.croods.bssgroup.service.userrole.UserRoleService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.department.DepartmentVo;
import com.croods.bssgroup.vo.designation.DesignationVo;
import com.croods.bssgroup.vo.employee.EmployeeVo;
import com.croods.bssgroup.vo.userfront.UserFrontVo;
import com.croods.bssgroup.vo.userrole.UserRoleVo;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	DesignationService designationService;
	
	@Autowired
	DepartmentService departmentService;
	
	@Autowired
	LocationService locationService;
	
	@Autowired
	UserRoleService userRoleService;
	
	@Autowired
	UserFrontService userFrontService;
	
	@GetMapping("")
	public ModelAndView employee(HttpSession session) {
		ModelAndView view=new ModelAndView("employee/employee");
		
		List<EmployeeVo> employeeVos = employeeService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		
		/*employeeVos.forEach(e -> {
			e.setCountriesName(locationService.findCountriesNameByCountriesCode(e.getCountriesCode()));
	    	e.setStateName(locationService.findStateNameByStateCode(e.getStateCode()));
	    	e.setCityName(locationService.findCityNameByCityCode(e.getCityCode()));
		});*/
		
		view.addObject("employeeVos", employeeVos);
		return view;
	}
	
	@GetMapping("new")
	public ModelAndView newEmployee(HttpSession session) {
		ModelAndView view=new ModelAndView("employee/employee-new");
		
		view.addObject("departmentVos",departmentService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("designationVos",designationService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("userRoleVos", userRoleService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		return view;
	}
	
	@PostMapping("/save")
	public String saveEmployee(@RequestParam Map<String,String> allRequestParams, HttpSession session, @ModelAttribute("employeeVo") EmployeeVo employeeVo, BindingResult bindingResult) {
		
		UserFrontVo userFrontVo = new UserFrontVo();
		
		if(employeeVo.getEmployeeId() == 0) {
			employeeVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			employeeVo.setCreatedOn(CurrentDateTime.getCurrentDate());
		} else {
			if(employeeVo.getUserFrontVo() != null) {
				userFrontVo = userFrontService.findByUserFrontId(employeeVo.getUserFrontVo().getUserFrontId());
				userFrontVo = employeeVo.getUserFrontVo();
			}
		}
		
		if(employeeVo.getEmployeeContactVos() != null && employeeVo.getEmployeeContactVos().size() != 0) {
			employeeVo.getEmployeeContactVos().forEach(c -> c.setEmployeeVo(employeeVo));
    	}
		
		employeeVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
    	employeeVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
    	employeeVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
    	employeeVo.setModifiedOn(CurrentDateTime.getCurrentDate());
    	
    	DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    	
    	try {
			if(allRequestParams.get("anniversaryDate") != null)
				employeeVo.setAnniversaryDate(dateFormat.parse(allRequestParams.get("anniversaryDate")));
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	
    	try {
			if(allRequestParams.get("dateOfBirth") != null)
				employeeVo.setDateOfBirth(dateFormat.parse(allRequestParams.get("dateOfBirth")));
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	
    	if(allRequestParams.get("userRoleId") != "") {
    		
    		userFrontVo.setName(employeeVo.getEmployeeName());
    		userFrontVo.setCityCode(employeeVo.getCityCode());
    		userFrontVo.setStateCode(employeeVo.getStateCode());
    		userFrontVo.setCountriesCode(employeeVo.getCountriesCode());
    		userFrontVo.setContactNo(employeeVo.getEmployeeMobileno());
    		
    		userFrontVo.setUserName(allRequestParams.get("userName"));
    		
    		if(employeeVo.getUserFrontVo() == null) {
    			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        		userFrontVo.setPassword(passwordEncoder.encode(allRequestParams.get("password")));
        		userFrontVo.setCreatedOn(CurrentDateTime.getCurrentDate());
    		}
    		
    		userFrontVo.setModifiedOn(CurrentDateTime.getCurrentDate());
    		UserRoleVo userRoleVo = new UserRoleVo();
    		userRoleVo.setUserRoleId(Long.parseLong(allRequestParams.get("userRoleId")));
    		List<UserRoleVo> userRoleVos = new ArrayList<UserRoleVo>();
    		userRoleVos.add(userRoleVo);
    		
    		userFrontVo.setRoles(userRoleVos);
    		
    		UserFrontVo parentUserFront =new UserFrontVo();
    		parentUserFront.setUserFrontId(Long.parseLong(session.getAttribute("branchId").toString()));
    		
    		userFrontVo.setUserFrontVo(parentUserFront);
    		
    		userFrontVo.setStatus("active");
    		
    		userFrontService.save(userFrontVo);
    		
    		employeeVo.setUserFrontVo(userFrontVo);
    	}
    	employeeService.save(employeeVo);
    	
    	if(allRequestParams.containsKey("deletedContactOther") == true) {		
			if(!allRequestParams.get("deletedContactOther").equals("")) {
				String address=allRequestParams.get("deletedContactOther").substring(0, allRequestParams.get("deletedContactOther").length()-1);
				List<Long> employeeContactIds= Arrays.asList(address.split(",")).stream().map(Long::parseLong).collect(Collectors.toList());
				employeeService.deleteEmployeeContactByIdIn(employeeContactIds);
			}
			
		}
		return "redirect:/employee/"+employeeVo.getEmployeeId();
	}
	@GetMapping("{id}")
	public ModelAndView viewEmployee(@PathVariable("id") long employeeId, HttpSession session) {
		
		ModelAndView view=new ModelAndView("employee/employee-view");
		EmployeeVo employeeVo = employeeService.findByEmployeeIdAndBranchId(employeeId, Long.parseLong(session.getAttribute("branchId").toString()));
		employeeVo.setCountriesName(locationService.findCountriesNameByCountriesCode(employeeVo.getCountriesCode()));
    	employeeVo.setStateName(locationService.findStateNameByStateCode(employeeVo.getStateCode()));
    	employeeVo.setCityName(locationService.findCityNameByCityCode(employeeVo.getCityCode()));
		view.addObject("employeeVo", employeeVo);
		
		return view;
	}
	
	@GetMapping("{id}/edit")
	public ModelAndView editEmployee(@PathVariable("id") long employeeId, HttpSession session) {
		
		ModelAndView view=new ModelAndView("employee/employee-edit");
		view.addObject("employeeVo", employeeService.findByEmployeeIdAndBranchId(employeeId, Long.parseLong(session.getAttribute("branchId").toString())));
		view.addObject("departmentVos",departmentService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("designationVos",designationService.findByCompanyId(Long.parseLong(session.getAttribute("companyId").toString())));
		view.addObject("userRoleVos", userRoleService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString())));
		
		return view;
	}
	
	@GetMapping("{id}/delete")
	public String deleteEmployee(@PathVariable("id") long employeeId, HttpSession session) {
		employeeService.deleteEmployee(employeeId, Long.parseLong(session.getAttribute("userId").toString()), CurrentDateTime.getCurrentDate());
		return "redirect:/employee";
	}
	
	@PostMapping("json")
	@ResponseBody
	public List<EmployeeVo> employeeJSON(HttpSession session) {
		
		List<EmployeeVo> employeeVos = employeeService.findByBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
		
		employeeVos.forEach(e -> {
	    	e.setUserFrontVo(null);
	    	e.setDepartmentVo(null);
	    	e.setDesignationVo(null);
	    	e.setEmployeeContactVos(null);
		});
		
		return employeeVos;
	}
	
	@RequestMapping(value="/upload/excel")
	@ResponseBody
	public String  ImportCustomertodb(HttpSession session,HttpServletRequest request) throws IOException 
	{
		
		/*File fb2 = ImageResize.convert(file);
		System.out.println(fb2.getName());
		System.out.println(fb2.getAbsolutePath());
		String filepath = fb2.getAbsolutePath();*/
		
		File fb=new File(request.getServletContext().getRealPath("/") + "import-data" + System.getProperty("file.separator")+"Employee.xlsx");
		InputStream in=new FileInputStream(fb);
		
		//Create Workbook instance holding reference to .xlsx file
        XSSFWorkbook workbook = new XSSFWorkbook(in);        
        //Get first/desired sheet from the workbook
        XSSFSheet sheet = workbook.getSheetAt(0);
        
        //Iterate through each rows one by one
        Iterator<Row> rowIterator = sheet.iterator();
        rowIterator.next();
        while(rowIterator.hasNext())
        {
        	Row row=rowIterator.next();
            //For each row, iterate through all the columns
            Iterator<Cell> cellIterator = row.cellIterator();
       
			while (cellIterator.hasNext()) {
				Cell cell = cellIterator.next();
				// Check the cell type and format accordingly
				if (cell.getColumnIndex() != 5 && cell.getColumnIndex() != 6) {
					cell.setCellType(Cell.CELL_TYPE_STRING);
					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_BOOLEAN:

						System.out.println("boolean===>>>" + cell.getBooleanCellValue() + "\t");
						break;
					case Cell.CELL_TYPE_NUMERIC:

						break;
					case Cell.CELL_TYPE_STRING:

						// list.add(cell.getStringCellValue().trim());
						break;

					}
				}
			}
            
            DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD");
            
            EmployeeVo employeeVo = new EmployeeVo();
            {
            	employeeVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
            	employeeVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
            	employeeVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
            	employeeVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
            	employeeVo.setModifiedOn(CurrentDateTime.getCurrentDate());
            	employeeVo.setCreatedOn(CurrentDateTime.getCurrentDate());
            	
            	try {
            		employeeVo.setEmployeeName(row.getCell(0).getStringCellValue().trim());
            	} catch (Exception e) {
            		employeeVo.setEmployeeName("");
            	}
        	   
            	try {
        		   employeeVo.setEmployeeMobileno(row.getCell(1).getStringCellValue().trim());
        	   } catch (Exception e) {
        		   employeeVo.setEmployeeMobileno("");
        	   }
        	   
        	   try {
        		   employeeVo.setEmployeeEmail(row.getCell(2).getStringCellValue().trim());
        	   } catch (Exception e) {
        		   employeeVo.setEmployeeEmail("");
        	   }
        	   

        	   try {
        		   DesignationVo designationVo = new DesignationVo();
        		   designationVo.setDesignationId(Long.parseLong(row.getCell(3).getStringCellValue().trim()));
        		   employeeVo.setDesignationVo(designationVo);
        	   } catch (Exception e) {
        		  // e.printStackTrace();
        	   }
        	   
        	   try {
        		   DepartmentVo departmentVo = new DepartmentVo();
        		   
        		   departmentVo.setDepartmentId(Long.parseLong(row.getCell(4).getStringCellValue().trim()));
        		   employeeVo.setDepartmentVo(departmentVo);
        	   } catch (Exception e) {
        		   
        	   }
        	   
        	   try {
        		   employeeVo.setDateOfBirth(row.getCell(5).getDateCellValue());
        	   } catch (Exception e) {
        		   //e.printStackTrace();
        	   }
        	   
        	   try {
        		   employeeVo.setPanNo(row.getCell(7).getStringCellValue().trim());
        	   } catch (Exception e) {
        		   employeeVo.setPanNo("");
        	   }
        	   
        	   try {
        		   employeeVo.setCtc(Double.valueOf((row.getCell(8).getStringCellValue().trim())));
        	   } catch (Exception e) {
        		   employeeVo.setCtc(0.0);
        	   }
        	   
        	   try {
        		   employeeVo.setAddressLine1(row.getCell(9).getStringCellValue().trim());
        	   } catch (Exception e) {
        		   employeeVo.setAddressLine1("");
        	   }
        	   
        	   
        	   
        	   try {
        		   employeeVo.setAddressLine2(row.getCell(10).getStringCellValue().trim());
        	   } catch (Exception e) {
        		   employeeVo.setAddressLine2("");
        	   }
        	   
        	   try {
        		   employeeVo.setPinCode(row.getCell(11).getStringCellValue().trim());
        	   } catch (Exception e) {
        		   
        	   }
        	   
        	   try {
        		   employeeVo.setCityCode(row.getCell(12).getStringCellValue().trim());
        	   } catch (Exception e) {
        	   }
        	   
        	   try {
        		   employeeVo.setStateCode(row.getCell(13).getStringCellValue().trim());
        	   } catch (Exception e) {
        		   
        	   }
        	   
        	   employeeVo.setCountriesCode("IN");
        	   
        	   employeeService.save(employeeVo);
        	  
           }
          
		}
        in.close();
        workbook.cloneSheet(0);
        workbook.close();
		return "success";
	}
	
	@RequestMapping(value="/upload/department")
	@ResponseBody
	public String  saveDepart(HttpSession session) throws IOException {
		
		String department[] = {"Marketing", "Dispatch", "Packing", "Manufacturing", "Management"};
		
		String designation[] = {"Marketing Representative", "Packing", "Manager", "Labour", "Helper", "Master", "Cutting Master", "Worker/ Karigar", "Management", "Partner", "Stiching Department"};
		
		for(int i=0;i<department.length;i++) {
			
			DepartmentVo departmentVo = new DepartmentVo();
			
			departmentVo.setDepartmentName(department[i]);
			departmentVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			departmentVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			departmentVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
			departmentVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
			departmentVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
			departmentVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			
			departmentService.save(departmentVo);
		}
		
		for(int i=0;i<designation.length;i++) {
			
			DesignationVo designationVo = new DesignationVo();
			
			designationVo.setDesignationName(designation[i]);
			designationVo.setCreatedOn(CurrentDateTime.getCurrentDate());
			designationVo.setModifiedOn(CurrentDateTime.getCurrentDate());
			designationVo.setBranchId(Long.parseLong(session.getAttribute("branchId").toString()));
			designationVo.setCompanyId(Long.parseLong(session.getAttribute("companyId").toString()));
			designationVo.setAlterBy(Long.parseLong(session.getAttribute("userId").toString()));
			designationVo.setCreatedBy(Long.parseLong(session.getAttribute("userId").toString()));
			
			designationService.save(designationVo);
		}
		return "Done";
	}
}
