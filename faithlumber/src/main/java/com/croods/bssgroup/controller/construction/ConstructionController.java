package com.croods.bssgroup.controller.construction;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ConstructionController {

	@GetMapping("/construction")
	public ModelAndView construction() { 
		ModelAndView view = new ModelAndView("construction/construction");
		
		return view;
	}
}
