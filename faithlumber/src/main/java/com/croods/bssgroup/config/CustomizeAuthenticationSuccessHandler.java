package com.croods.bssgroup.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.croods.bssgroup.constant.Constant;
import com.croods.bssgroup.repository.userfront.UserFrontRepository;
import com.croods.bssgroup.service.financial.FinancialService;
import com.croods.bssgroup.service.navmenu.NavMenuService;
import com.croods.bssgroup.util.CurrentDateTime;
import com.croods.bssgroup.vo.userfront.UserFrontVo;

@Component
public class CustomizeAuthenticationSuccessHandler implements AuthenticationSuccessHandler{

	@Autowired
	UserFrontRepository userFrontRepository;
	
	@Autowired
	FinancialService financialService;
	
	@Autowired
	NavMenuService navMenuService;
	
	UserFrontVo userFrontVo;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		
		System.out.println(authentication.getName());
		userFrontVo = userFrontRepository.findByUserName(authentication.getName());
		
		HttpSession session = request.getSession();
		
		session.setAttribute("userName", userFrontVo.getUserName());
		session.setAttribute("name", userFrontVo.getName());
		session.setAttribute("userId", userFrontVo.getUserFrontId());
		userFrontVo.getRoles();
		
		for (GrantedAuthority authority: authentication.getAuthorities()) {
			
			long roleId = Long.parseLong(authority.getAuthority().replace("ROLE_", ""));
			
			if(Constant.URID_CORPORATE == roleId) {
				
				session.setAttribute("corporateId", userFrontVo.getUserFrontId());
				session.setAttribute("userType", Constant.URID_CORPORATE);
				
				
			} else if(Constant.URID_COMPANY == roleId) {
				
				session.setAttribute("corporateId", userFrontVo.getUserFrontVo().getUserFrontId());
				session.setAttribute("branchId", userFrontVo.getUserFrontId());
				session.setAttribute("companyId", userFrontVo.getUserFrontId());
				session.setAttribute("userType", Constant.URID_COMPANY);
				
			} else if(Constant.URID_BRANCH == roleId) {
				
				session.setAttribute("corporateId", userFrontVo.getUserFrontVo().getUserFrontVo().getUserFrontId());
				session.setAttribute("branchId", userFrontVo.getUserFrontId());
				session.setAttribute("companyId", userFrontVo.getUserFrontVo().getUserFrontId());
				session.setAttribute("userType", Constant.URID_BRANCH);
				
			} else if(Constant.URID_USER == roleId) {
				//coding for user as per requirement
				
				/*session.setAttribute("corporateId", userFrontVo.getUserFrontVo().getUserFrontVo().getUserFrontId());
				session.setAttribute("branchId", userFrontVo.getUserFrontId());
				session.setAttribute("companyId", userFrontVo.getUserFrontVo().getUserFrontId());
				session.setAttribute("userType", Constant.URID_USER);
				
				if(Constant.URID_COMPANY == roleId) {
					
					session.setAttribute("corporateId", userFrontVo.getUserFrontVo().getUserFrontId());
					session.setAttribute("branchId", userFrontVo.getUserFrontId());
					session.setAttribute("companyId", userFrontVo.getUserFrontId());
					session.setAttribute("userType", Constant.URID_COMPANY);
					
				} else if(Constant.URID_BRANCH == roleId) {
					
					session.setAttribute("corporateId", userFrontVo.getUserFrontVo().getUserFrontVo().getUserFrontId());
					session.setAttribute("branchId", userFrontVo.getUserFrontId());
					session.setAttribute("companyId", userFrontVo.getUserFrontVo().getUserFrontId());
					session.setAttribute("userType", Constant.URID_BRANCH);
					
				} */
			}
			System.out.println(roleId);
			//set financialYear and monthInterval
			if(Constant.URID_CORPORATE != roleId) {
				String financialYear,monthInterval;
				
				if(roleId > 3) {
					financialYear=userFrontVo.getUserFrontVo().getDefaultYearInterval();
					monthInterval=userFrontVo.getUserFrontVo().getMonthInterval();
				} else {
					financialYear=userFrontVo.getDefaultYearInterval();
					monthInterval=userFrontVo.getMonthInterval();
				}
				
				session.setAttribute("financialYear", financialYear);
				session.setAttribute("monthInterval", monthInterval);
				
				session.setAttribute("firstDateFinancialYear",CurrentDateTime.getFirstDate(financialService.findByMonthInterval(monthInterval),financialYear));
				session.setAttribute("lastDateFinancialYear",CurrentDateTime.getLastDate(financialService.findByMonthInterval(monthInterval),financialYear));
				
				if(Constant.URID_USER == roleId) {
					session.setAttribute("stateCode", userFrontVo.getUserFrontVo().getStateCode());
					session.setAttribute("countryCode", userFrontVo.getUserFrontVo().getCountriesCode());
				} else {
					session.setAttribute("stateCode", userFrontVo.getStateCode());
					session.setAttribute("countryCode", userFrontVo.getCountriesCode());
				}
			}
			
			session.setAttribute("NavMenuPermission",navMenuService.findNavMenuByUserRoleId(roleId));
			session.setAttribute("NavSubMenuPermission",navMenuService.findNavSubMenuByUserRoleId(roleId));
		}
		
		response.sendRedirect("/dashboard");
	}

}
